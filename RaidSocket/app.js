const express = require('express');
const http = require('http');
const path = require('path');
const cookieParser = require('cookie-parser');
const logger = require('morgan');

const indexRouter = require('./routes/index');
const usersRouter = require('./routes/users');

const app = express();

app.use(logger('dev'));
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(cookieParser());
app.use(express.static(path.join(__dirname, 'public')));

app.use('/', indexRouter);

const server = http.createServer(app);
const io = require('socket.io')(server);

const _ = require("lodash");
const fs = require('fs')

// 챗 필터
const chatFilter = fs.readFileSync('./chatfilter.txt', 'utf-8');
let filterList = chatFilter.split(',');


// 클라이언트에서 connect 에 성공 시 호출.
io.on('connection', async function (socket) {
    //console.log("welcome");

    socket.join("raid");

    // 기본 방이 아닌 모든 방을 탈퇴시킨다.
    await leaveRoom();

    // 내 정보를 업데이트 하는 구문이 호출되면
    socket.on('CLIENT_UPDATE_USER_INFO', function (obj) {
        socket.data = {
            inDate: obj.inDate,
            nickName: obj.nickName,
            rank: obj.rank,
            missile: obj.missile,
            bazooka: obj.bazooka,
            costume: obj.costume,
            relic: obj.relic,
            dRelic: obj.dRelic,
            stone: obj.stone,
            soldier: obj.soldier,
            princess: obj.princess,
        };

        socket.emit("SERVER_UPDATE_USER_INFO", true);
    })

    // [생성] 소켓의 방 생성 요청
    socket.on('CLIENT_CREATE_ROOM', async function (obj) {
        // 기본 데이터도 셋팅하지 않고 게임을 진행할 수는 없음.
        if (socket.data === undefined) {
            socket.emit("SOCKET_INTERNAL_ERROR");
            return;
        }

        const raidRoomNum = obj.raidRoomNum;

        // 방이 이미 있는 경우임.
        if (io.sockets.adapter.rooms.has(raidRoomNum)) {
            // 방이 이미 존재한다는 오류 출력
            socket.emit("SERVER_CREATE_ROOM", false, null);
            return;
        }

        // 없는 방이니, 일단 방 생성
        socket.join(raidRoomNum)

        // 방장 설정
        let room = io.sockets.adapter.rooms.get(raidRoomNum);
        room.owner = socket.id;
        room.difficulty = obj.difficulty

        // 통지
        let roomInfo = getRoomInfo(room, raidRoomNum);
        socket.emit("SERVER_CREATE_ROOM", true, roomInfo)
    })

    socket.on('CLIENT_LOCK_ROOM', function (obj) {
        // 클라에게 콜백 처리하지 않음
        // 잠김이 걸리면 좋고, 안 걸려도 상관없음

        const raidRoomNum = obj.raidRoomNum;

        let foundRoom = io.sockets.adapter.rooms.get(raidRoomNum)

        // 찾을 수 없는 방
        if (foundRoom === undefined) return

        foundRoom.isLocked = obj.isLocked
    })

    socket.on('CLIENT_JOIN_ROOM', function (obj) {

        let raidRoomNum = obj.raidRoomNum;

        // 방 번호 안 넘어왔을 경우, 랜덤 방 찾기
        let foundRoom

        let randomJoin = raidRoomNum === undefined || raidRoomNum === null || raidRoomNum === "";
        if (randomJoin) {
            let foundRoomId = findRandomRoomId()

            foundRoom = io.sockets.adapter.rooms.get(foundRoomId)

            // 랜덤 방이 없다면, 실패 처리
            if (foundRoom === undefined) {
                socket.emit('SERVER_JOIN_ROOM', false, null, "notFoundAnyRoom");
                return;
            }

            // 밑에서 처리될 수 있도록 대입
            raidRoomNum = foundRoomId
        } else {
            foundRoom = io.sockets.adapter.rooms.get(raidRoomNum)

            // 찾을 수 없는 방
            if (foundRoom === undefined) {
                socket.emit('SERVER_JOIN_ROOM', false, null, "notFound");
                return
            }
            // 방 인원 제한
            else if (foundRoom.size >= 20) {
                socket.emit('SERVER_JOIN_ROOM', false, null, "full");
                return
            }
            // 시작방 체크
            else if (foundRoom.isStarted) {
                socket.emit('SERVER_JOIN_ROOM', false, null, "alreadyStarted");
                return
            }
            // 잠금방 체크
            else if (foundRoom.isLocked) {
                // 잠금 무시 못한다면, 입장 못함
                const ignoreLocking = obj.ignoreLocking
                if (ignoreLocking !== true) {
                    socket.emit('SERVER_JOIN_ROOM', false, null, "alreadyStarted");
                    return
                }
            }
        }

        // 소켓을 해당 방에 가입시키고
        socket.join(raidRoomNum);

        // 나에게 해당 방 유저들의 데이터 전달 (내 데이터 포함)
        let roomInfo = getRoomInfo(foundRoom, raidRoomNum);

        socket.emit("SERVER_JOIN_ROOM", true, roomInfo, "success")

        // 해당 방의 인원들에게 신규 유저가 접속했음을 알림. (나 빼고)
        socket.to(raidRoomNum).emit("SERVER_ROOM_NEW_USER_JOINED", raidRoomNum, socket.id, socket.data);
    });

    socket.on('CLIENT_REQUEST_ROOM_INFO', function (obj) {
        let raidRoomNum = obj.raidRoomNum;

        let foundRoom = io.sockets.adapter.rooms.get(raidRoomNum)

        // 찾을 수 없는 방
        if (foundRoom === undefined) {
            socket.emit('SERVER_REQUEST_ROOM_INFO', false, null);
            return
        }

        // 보내주기
        let roomInfo = getRoomInfo(foundRoom, raidRoomNum);

        socket.emit("SERVER_REQUEST_ROOM_INFO", true, roomInfo)
    });

    socket.on('CLIENT_REQUEST_ROOM_LIST', function (obj) {
        // 방 리스트 반환.
        const rooms = Array.from(io.sockets.adapter.rooms);
        // 본인 소켓방 제외시킴
        const filtered = rooms.filter(room => !room[1].has(room[0]))
        const availableRooms = filtered.map(room => {
            let roomId = room[0];
            const roomSocket = io.sockets.adapter.rooms.get(roomId)
            return getRoomInfo(roomSocket, roomId);
        });

        const result = {"roomList": availableRooms};

        // [호출한 유저에게만] room_list 를 날림.
        socket.emit("SERVER_REQUEST_ROOM_LIST", result);
    })

    // [진행] 방장의 게임 시작 요청.
    socket.on('CLIENT_START_ROOM', async function (obj) {

        // 방 찾기
        const roomNum = obj.roomNum;
        const room = io.sockets.adapter.rooms.get(roomNum);

        // 해당 방 없을 시
        if (room === undefined) {
            // 실패
            socket.emit("SERVER_START_ROOM", false, "notFoundRoom")
            return;
        }

        const isOwner = room.owner === socket.id

        // 방장 아니면
        if (!isOwner) {
            // 방 시작 실패
            socket.emit("SERVER_START_ROOM", false, "isNotOwner")
            return;
        }

        // 더 이상 이 방에 접속 못하게 플래깅
        room.isStarted = true

        // 연계할 방
        const result = {
            "roomNum": obj.roomNum,
            "nextRoomNum": obj.nextRoomNum,
            "difficulty": obj.difficulty,
            "victory": obj.victory,
            "hpDownRatioPerSec": obj.hpDownRatioPerSec,
        }

        // 방 시작 성공 메시지
        socket.emit("SERVER_START_ROOM", true, "success")

        // 방 전체에게 게임을 시작하라는 메세지 보냄
        io.to(roomNum).emit("SERVER_START_ROOM_ALL", result)
    });

    socket.on('CLIENT_CHAT', function (obj) {
        let msg = obj.message;
        _.forEach(filterList, (curString, idx) => {
            if (msg.includes(curString)) {
                msg = msg.replaceAll(curString, _.repeat('*', curString.length));
            }
        });

        const roomNum = obj.roomNum;
        io.to(roomNum).emit("SERVER_CHAT", roomNum, socket.id, msg)
    })

    socket.on('CLIENT_CHANGE_DIFFICULTY', function (obj) {
        const roomNum = obj.roomNum;
        let room = io.sockets.adapter.rooms.get(roomNum);

        // 해당 방 없을 시, 실패
        if (room === undefined) return;

        // 방장 아니면, 실패
        const isOwner = room.owner === socket.id
        if (!isOwner) return;

        // 따로 실패 이벤트를 보내진 않음
        // 채팅처럼 빈번히 호출되는 이벤트이고, 방 시작 때 반영된 Difficulty도 다시 내려보내줌

        // 방 난이도 변경
        const difficulty = obj.difficulty;
        room.difficulty = difficulty;

        // 통지 (나는 빼고 보냄)
        socket.to(roomNum).emit("SERVER_CHANGE_DIFFICULTY", roomNum, difficulty)
    })

    socket.on('CLIENT_LEAVE_ROOM', async function (obj) {
        changeOwnerIfCan();
        leaveRoom();
    });

    // 나갈때 (disconnect 전에 호출됨_ 현재 내 방의 정보를 가져올 수 있음.)
    socket.on("disconnecting", async (reason) => {
        // 여기서의 처리는 async로 비동기 처리되면 안됨
        // 두 번째 async 부턴 socket의 disconnect가 되서 넘어오게 됨
        changeOwnerIfCan();
        leaveRoom();
    });

    socket.on('disconnect', function () {
        //console.log('server disconnected');
    });

    function findRandomRoomId() {
        const rooms = Array.from(io.sockets.adapter.rooms);
        // 본인 소켓방 제외시킴
        const filteredRooms = rooms.filter(room => !room[1].has(room[0]))
        for (const filteredRoom of filteredRooms) {
            let room = io.sockets.adapter.rooms.get(filteredRoom[0]);

            // 풀방 체크
            if (room.size >= 20) continue

            // 시작 방 제외
            if (room.isStarted) continue

            // 잠금 방 제외
            if (room.isLocked) continue

            // 방 번호 반환
            return filteredRoom[0];
        }
        return null
    }

    function getRoomInfo(room, roomNum) {
        let userList = [];
        for (const userId of room) {
            const userSocket = io.sockets.sockets.get(userId);
            userList.push({
                "socketId": userId,
                "data": userSocket.data
            });
        }

        return {
            "owner": room.owner,
            "difficulty": room.difficulty,
            "roomNum": roomNum,
            "isLocked": room.isLocked,
            "isStarted": room.isStarted,
            "users": userList
        };
    }

    function leaveRoom() {
        // 기본 방이 아닌 모든 방을 탈퇴시킴.
        const userSocketRooms = socket.rooms;

        for (const room of userSocketRooms) {
            // 소켓 입장시 자기 자신이 존재하는 방이 생성되기 때문에, room === socket.id 제외
            if (room === socket.id) continue;

            // 방 나가기
            socket.leave(room);

            // 이 유저가 나갔다는 걸 방에 알리기 (나 빼고)
            io.to(room).emit("SERVER_ROOM_USER_LEFT", room, socket.id);
        }

        // 통지 (나만)
        socket.emit("SERVER_LEAVE_ROOM", true);
    }

    // 방에 나가기 전에 호출돼야 함
    function changeOwnerIfCan() {
        const userRooms = socket.rooms;
        for (const userRoom of userRooms) {
            if (userRoom === socket.id) continue;

            // 접속한 레이드 방만 처리
            const room = io.sockets.adapter.rooms.get(userRoom);

            if (room.owner === undefined) continue;

            // 방장아닌 방 스킵
            if (room.owner !== socket.id) continue;

            // 유저가 단 한 명이라도 있어야 함 (나 제외하고)
            if (room.size <= 1) continue;

            // 방에 유저가 있을때, 이 유저를 방장으로 교체해야 한다.
            let userList = [];
            for (const roomUserId of room) {
                // 나는 제외하고, 유저 리스트를 만듬
                if (roomUserId === socket.id) continue;
                const roomUserSocket = io.sockets.sockets.get(roomUserId);
                userList.push({
                    "id": roomUserId,
                    "data": roomUserSocket.data
                });
            }

            // 선정한 유저들 중 한 명을 방장으로 선정
            const randomOwner = userList[0];
            room.owner = randomOwner.id;

            // 니가 방장이다 라고 통보 (나한텐 통지 안 함)
            socket.to(userRoom).emit("SERVER_ROOM_OWNER_CHANGED", userRoom, randomOwner.id);
        }
    }
})


// 소켓 통신을위해 포트를 지정합니다.
const PORT = 7243;
server.listen(PORT, async function () {
});

module.exports = app;

