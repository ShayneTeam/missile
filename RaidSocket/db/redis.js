
const redis = require('redis');

/**
 * TODO: db connecter
 */
const options = {
  redisApiConn: null,
  redisApiConnect: () => { // NOTE: API redis
    if (!options.redisApiConn) options.redisApiConn = redis.createClient({
      port : 6379,
      host: 'localhost',
    });
    return options.redisApiConn;
  },
};

class Redis {
  /**
   * TODO: redis connect init
   */
  constructor() {
    this.redisConnect = options.redisApiConnect();
  }

  health() {
    this.redisConnect.on('connect', () => {
      console.log('api용 redis connected');
    });
    this.redisConnect.on('error', (err) => {
      console.log('api용 redis connect err', err);
    });
    return true;
  }

  /**
   * TODO: redis strings set
   * @description strings 구조에서 한 쌍의 key, value 저장 하기.
   * @param {String} key
   * @param {*} value 저장할 데이터
   * @param {*} expire 만료
   */
  set(key, value, expire) {
    return new Promise((resolve, reject) => {
      // // if (!util.paramCheck(key)) return reject(new Error('redis set param check'));
      return this.redisConnect.set(key, JSON.stringify(value), (err) => {
        if (err) return reject(err);
        if (expire) {
          this.redisConnect.expire(key, expire);
          return resolve();
        }
        return resolve();
      });
    });
  }

  /**
   * TODO: redis strings get
   * @description strings 구조에서 해당하는 key의 value 가져오기.
   * @param {String} key
   */
  get(key) {
    return new Promise((resolve, reject) => {
      // // if (!util.paramCheck(key)) return reject(new Error('redis get param check'));
      return this.redisConnect.get(key, (err, data) => {
        if (err) return reject(err);
        if (!data) return resolve();
        return resolve(JSON.parse(data));
      });
    });
  }

  /**
   * TODO: redis multi get
   * @param {Array} keys
   */
  mget(keys) {
    return new Promise((resolve, reject) => {
      if (!Array.isArray(keys)) return reject(new Error('redis mget param check'));
      return this.redisConnect.mget(keys, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis hash length
   * @param {*} key
   */
  hlen(key) {
    return new Promise((resolve, reject) => {
      // // if (!util.paramCheck(key)) return reject(new Error('redis hlen param check'));
      return this.redisConnect.hlen(key, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis hashes set
   * @description hashes 구조에서 해당하는 key에 한 쌍의 hash key, value 저장
   * @param {String} key 키
   * @param {String} hashKey 해시 키
   * @param {*} value 저장할 데이터
   */
  hset(key, hashKey, value) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key, hashKey)) return reject(new Error('redis hset param check'));
      return this.redisConnect.hset(key, hashKey, JSON.stringify(value), (err) => {
        if (err) return reject(err);
        return resolve();
      });
    });
  }

  /**
   * TODO: redis hashes get
   * @description hashes 구조에서 해당하는 key, hash key의 데이터 가지고 오기.
   * @param {*} key 키
   * @param {*} hashKey 해시 키
   */
  hget(key, hashKey) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key, hashKey)) return reject(new Error('redis hget param check'));
      return this.redisConnect.hget(key, hashKey, (err, data) => {
        if (err) return reject(err);
        if (!data) return resolve();
        return resolve(JSON.parse(data));
      });
    });
  }

  /**
   * TODO: JSON 타입이 아닐때 hget
   * @param {*} key
   * @param {*} hashKey
   */
  hgetNoneParse(key, hashKey) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key, hashKey)) return reject(new Error('redis hget param check'));
      return this.redisConnect.hget(key, hashKey, (err, data) => {
        if (err) return reject(err);
        if (!data) return resolve();
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis hash delete
   * @param {*} key
   * @param {*} hashKey
   */
  hdel(key, hashKey) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key, hashKey)) return reject(new Error('redis hget param check'));
      return this.redisConnect.hdel(key, hashKey, (err) => {
        if (err) return reject(err);
        return resolve();
      });
    });
  }

  /**
   * TODO: redis hashes multiple get
   * @description hashes 구조에서 해당하는 key, hash key들의 데이터 모두 가지고 오기.
   * @param {String} key 키
   * @param  {Array} hashKey 해시 키들
   */
  hmget(key, hashKey) {
    return new Promise((resolve, reject) => {
      // // if (!util.paramCheck(key)) return reject(new Error('redis hmget param check'));
      if (!Array.isArray(hashKey)) return reject(new Error('redis hmget param check'));
      return this.redisConnect.hmget(key, hashKey, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis hashes get all
   * @description hashes 구조에서 해당하는 key에 hashKey, value 모두 가지고 오기.
   * @param {String} key 키
   */
  hgetall(key) {
    return new Promise((resolve, reject) => {
      // // if (!util.paramCheck(key)) return reject(new Error('redis hgetall param check'));
      return this.redisConnect.hgetall(key, (err, data) => {
        if (err) return reject(err);
        if (!data) return resolve();
        return resolve(data);
      });
    });
  }

  /**
   * redis hashes scan
   * @param {*} key
   * @param {*} pattern
   */
  hscan(key, pattern) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key, pattern)) return reject(new Error('redis hscan param check'));
      return this.redisConnect.hscan(key, 0, 'match', pattern, (err, data) => {
        if (err) return reject(err);
        if (!data) return resolve();
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis hashes get all hashkey
   * @description 모든 hashkey 가지고옴.
   * @returns {Array}
   * @param {*} key
   */
  hkeys(key) {
    return new Promise((resolve, reject) => {
      // // if (!util.paramCheck(key)) return reject(new Error('redis hkeys param check'));
      return this.redisConnect.hkeys(key, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis hashes get all values
   * @description 해당 key의 모든 value만 가지고 오기
   * @returns {Array}
   * @param {*} key
   */
  hvals(key) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key)) return reject(new Error('redis hvals param check'));
      return this.redisConnect.hvals(key, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis hashes incry value
   * @description 해당 해시 키에 value만큼 +
   * @param {String} key 키
   * @param {String} hashKey 해시 키
   * @param {Number} value 값
   */
  hincrby(key, hashKey, value) {
    // console.log('hincrby', 'key', key, 'hashKey', hashKey, 'value', value);
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key)) return reject(new Error('redis hincrby param check'));
      return this.redisConnect.hincrby(key, hashKey, value, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis hashes key의 필드가 존재하는지 체크
   * @param {String} key 키
   * @param {String} hashKey 해시 키
   */
  hexists(key, hashKey) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key, hashKey)) return reject(new Error('redis hexists param check'));
      return this.redisConnect.hexists(key, hashKey, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis hashes multiple key, value set
   * @param {*} key 키
   * @param {Array} hashKeyAndValue [hashkey, value ...]
   */
  hmset(key, hashKeyAndValue) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key, hashKeyAndValue)) return reject(new Error('redis hmset param check'));
      return this.redisConnect.hmset(key, hashKeyAndValue, (err) => {
        if (err) {
          return reject(err);
        }
        return resolve();
      });
    });
  }

  /**
   * TODO: redis delete key
   * @param {*} key
   */
  del(key) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key)) return reject(new Error('redis del param check'));
      return this.redisConnect.del(key, (err) => {
        if (err) return reject(err);
        return resolve();
      });
    });
  }

  /**
   * TODO: redis list push
   * @param {*} key
   * @param {*} data
   */
  lpush(key, data) {
    // console.log('lpush', 'key', key, 'data', data);
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key)) return reject(new Error('redis lpush param check'));
      return this.redisConnect.lpush(key, data, (err) => {
        if (err) return reject(err);
        return resolve();
      });
    });
  }

  /**
   * TODO: redis list 범위별 조회
   * @param {*} key
   * @param {*} start 시작점
   * @param {*} end 끝점
   */
  lrange(key, start, end) {
    // console.log('lrange', 'key', key, 'start', start, 'end', end);
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key)) return reject(new Error('redis lpush param check'));
      return this.redisConnect.lrange(key, start, end, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis sets add
   * @description 중복 저장 안됨.
   * @param {*} key
   * @param {*} id
   */
  sadd(key, id) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key)) return reject(new Error('redis sadd param check'));
      return this.redisConnect.sadd(key, id, (err) => {
        if (err) return reject(err);
        return resolve();
      });
    });
  }

  /**
   * TODO: redis set list
   * @param {*} key
   */
  smembers(key) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key)) return reject(new Error('redis smembers param check'));
      return this.redisConnect.smembers(key, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis set ismember
   * @param {*} key
   * @param {*} value
   */
  sismember(key, value) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(key, value)) return reject(new Error('redis sismember param check'));
      return this.redisConnect.sismember(key, value, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }

  /**
   * TODO: redis scan
   * @param {*} pattern
   * @param {*} count
   */
  scan(pattern, count) {
    return new Promise((resolve, reject) => {
      // if (!util.paramCheck(pattern, count)) return reject(new Error('redis scan param check'));
      return this.redisConnect.scan(0, 'match', pattern, 'count', count, (err, data) => {
        if (err) return reject(err);
        return resolve(data);
      });
    });
  }
}

module.exports = Redis;
