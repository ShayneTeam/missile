Introduction
	In large-scale development, the number of Asset increases, I think that it may become stressful to look for the target Asset.
	We developed it as efficiently selecting the target Asset by going back to the selections.

Change log
	- 2021/01
		- bugfix
	- 2021/01
		- Add Features
			- Support for multiple selection
			- Add right-click menu
		- Add Settings
			- Show Favorite icon
			- Add Language Japanese
	- 2020/01
		- Add Features
			- Drag and Drop
			- Extension filter
			- Open Asset
			- Supports prefab mode
		- Changed icon
		- Add Settings
			- Without Hierarchy object
			- Scroll only history
			- Update when selected history
	- 2018/10
		- New Release!!

Features
　- The selection history of Hierarchy and Project is made
　- Favorite
　- NEW Support for multiple selection
　- NEW Right-click menu
　- NEW Language switching (English, Japanese)
　- Drag and Drop
　- Extension filter
　- Open Asset
　- Shortcuts
　- Change display name of favorite
　- Supports prefab mode

Usage
　- The selection history of Hierarchy and Project is made
　　clicking a button of the history, it's selection.

　- Multiple selection
　　The file name will be prefixed with [X] and the number of selections.

　- Right-click menu
　　You can choose to "Favorite Add, Delete", "Select" or "Open".

　- Favorite
　　Star is clicked, you can register with a favorite.
　　Cross is clicked, it's eliminated from a favorite.

　- Drag and Drop
　- Extension filter

　- Open Asset
　　it's clicked while pressing a Ctrl key, Asset is opened.

　- Shortcuts
　　- backward: Ctrl + Shift + Alt + , （Mac Cmd + Shift + Alt + ,） 
　　- forward: Ctrl + Shift + Alt + . （Mac Cmd + Shift + Alt + .） 
　　- Select Favorite: Ctrl + Shift + Alt + [0-9], （Mac Cmd + Shift + Alt + ,） 

- Settings
　- Auto remove same file history
　　The old history of the same file is deleted automatically.
　- Without Hierarchy object
　　Selection of Hierarchy isn't included in the history any more.
　- Scroll only history
　- Update when selected history
　　When clicking a history button, it's added to the history newly.
　- Show favorite icon
　　Show favorite icons
　- Langage
　　Language switching (English, Japanese)
　- Change display name of favorite


Site
	https://sites.google.com/view/transsoftsite/Home/eng-simple-selection-history

Connect us
	t.onaka0108@gmail.com
