はじめに
大規模開発では、アセットの数が増えてくると、目的のアセットを探すのがストレスになることもあると思います。
そこで、選択に立ち返って効率的に対象資産を選択することを目的として開発しました。

Change log
	- 2021/01
		- バグ修正
	- 2021/01
		- 機能追加
			- 複数選択の対応
			- 右クリックメニュー
		- 設定追加
			- お気に入りアイコン表示OnOff
			- 日本語追加
	- 2020/01
		- 機能追加
			- ドラッグ＆ドロップ
			- 拡張子フィルター
			- アセットを開く
			- プレハブモードの対応
		- アイコンを変更
		- 設定追加
			- Hierarchy objectを選択しない
			- 履歴のみスクロールさせる
			- 履歴を選択した時にも選択を更新する
	- 2018/10
		- New Release!!

機能
- Hierarchy と Project の選択履歴を作成 made
- お気に入り
- 複数選択対応
- 右クリックメニュー
- 日本語言語切替
- ドラッグ＆ドロップ
- 拡張子フィルター
- アセットを開く
- ショートカット
- お気に入りの表示名変更
- Prefab mode に対応


使い方

- Hierarchy と Project の選択履歴を作成
　履歴のボタンをクリックすると選択

- 複数選択
　ファイル名の頭に[X]と選択の個数が入ります

- 右クリックメニュー
　「お気に入りに登録, 削除」「選択」「開く」を選択できます

- お気に入り
　星をクリックしてお気に入りに登録
　バツをクリックしてお気に入りから削除

- ドラッグ＆ドロップ
- 拡張子フィルター

- アセットを開く
　Ctrlキーを押しながら履歴をクリックするとAssetを開く

- ショートカット
　- 戻る: Ctrl + Shift + Alt + , （Mac Cmd + Shift + Alt + ,） 
　- 進む: Ctrl + Shift + Alt + . （Mac Cmd + Shift + Alt + .） 
　- お気に入りの選択: Ctrl + Shift + Alt + [0-9], （Mac Cmd + Shift + Alt + ,） 

- Settings
　- Auto remove same file history
　　同じファイルの履歴の古い方を自動で削除します

　- Without Hierarchy object
　　Hierarchy の選択は履歴に追加しなくなる

　- Scroll only history
　　hisotryのみスクロールさせる

　- Update when selected history
　　履歴をクリックしたときにも履歴に追加します

　- Show favorite icon
　　お気に入りアイコンを表示する

　- Langage
　　言語を英語と日本語切り替える

　- お気に入りの表示名を変更できます

詳細ホームページ
https://sites.google.com/view/transsoftsite/Home/simple-selection-history

連絡
	t.onaka0108@gmail.com
