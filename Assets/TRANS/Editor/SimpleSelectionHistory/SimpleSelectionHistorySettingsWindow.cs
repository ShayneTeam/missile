﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEditor;

namespace SHNameSpace
{
	// --------------------------------------------------------------
	// Window
	// --------------------------------------------------------------
	public class SimpleSelectionHistorySettingsWindow :EditorWindow
	{
		static SimpleSelectionHistorySettingsWindow window = null;

		Vector2 m_scroll;
		GUIStyle m_ButtonStyle = null;
		IMECompositionMode prevMode;
		// bool m_bFileNameOnly = true;

		// --------------------------------------------------------------
		// Back
		[MenuItem("Tools/Simple Selection History/Settings",false, StaticSimpleSelectionMain.Priority + 20)]
		public static void Open()
		{
			EditorWindow.GetWindow<SimpleSelectionHistorySettingsWindow>(true,"Simple Selection History Settings"); 
		}

		// --------------------------------------------------------------
		void Awake()
		{
			window = this;
			prevMode = Input.imeCompositionMode;
			Input.imeCompositionMode = IMECompositionMode.On;
		}
		// --------------------------------------------------------------
		void OnDestroy()
		{
			window = null;
			Input.imeCompositionMode = prevMode;
		}

		// --------------------------------------------------------------
		static public void Refresh()
		{
			if( window != null )
				window.Repaint();
		}


		// --------------------------------------------------------------
		void OnGUI()
		{
			// if( m_ButtonStyle == null )
			{
				// m_ButtonStyle = new GUIStyle( EditorStyles.miniButtonLeft );
				m_ButtonStyle = new GUIStyle( GUI.skin.button );
				// m_ButtonStyle.fixedHeight = 30;
				m_ButtonStyle.alignment = TextAnchor.MiddleCenter;
				m_ButtonStyle.alignment = TextAnchor.MiddleRight;
			}

			var favorite = StaticSimpleSelectionMain.HistoryData.favorite;
			bool needSave = false;

			// --------------------------------------------------------------
			EditorGUILayout.BeginHorizontal();
			GUILayout.Label( GetString(LangString.LangCount) );
			int count = EditorGUILayout.IntSlider( StaticSimpleSelectionMain.HistoryData.historycount, 100 , 1000 );
			if( count !=  StaticSimpleSelectionMain.HistoryData.historycount ){
				needSave = true;
				StaticSimpleSelectionMain.HistoryData.HistoryCount = count;
			}
			EditorGUILayout.EndHorizontal();


			// --------------------------------------------------------------
			bool isAutoRemove = GUILayout.Toggle( StaticSimpleSelectionMain.HistoryData.isAutoRemoveSameFile , GetString(LangString.AutoRemoveHistory) );
			if( isAutoRemove != StaticSimpleSelectionMain.HistoryData.isAutoRemoveSameFile ){
				StaticSimpleSelectionMain.HistoryData.isAutoRemoveSameFile = isAutoRemove;
				needSave = true;
			}

			// --------------------------------------------------------------
			bool withoutHierarchy = GUILayout.Toggle( StaticSimpleSelectionMain.HistoryData.WithoutHierarchy , GetString(LangString.WithoutHierarcy) );
			if( withoutHierarchy != StaticSimpleSelectionMain.HistoryData.WithoutHierarchy ){
				StaticSimpleSelectionMain.HistoryData.WithoutHierarchy = withoutHierarchy;
				needSave = true;
			}

			// --------------------------------------------------------------
			bool ScrollOnlyHistroy = GUILayout.Toggle( StaticSimpleSelectionMain.HistoryData.ScrollOnlyHistroy , GetString(LangString.ScrollOnlyHitory) );
			if( ScrollOnlyHistroy != StaticSimpleSelectionMain.HistoryData.ScrollOnlyHistroy ){
				StaticSimpleSelectionMain.HistoryData.ScrollOnlyHistroy = ScrollOnlyHistroy;
				needSave = true;
			}

			// --------------------------------------------------------------
			bool UpdateWhenSelected = GUILayout.Toggle( StaticSimpleSelectionMain.HistoryData.UpdateWhenSelected , GetString(LangString.UpdateWhenSelected) );
			if( UpdateWhenSelected != StaticSimpleSelectionMain.HistoryData.UpdateWhenSelected ){
				StaticSimpleSelectionMain.HistoryData.UpdateWhenSelected = UpdateWhenSelected;
				needSave = true;
			}

			// --------------------------------------------------------------
			bool showFavoriteIcon = GUILayout.Toggle( StaticSimpleSelectionMain.HistoryData.ShowFavoriteIcon , GetString(LangString.ShowFavoriteIcon) );
			if( showFavoriteIcon != StaticSimpleSelectionMain.HistoryData.ShowFavoriteIcon ){
				StaticSimpleSelectionMain.HistoryData.ShowFavoriteIcon = showFavoriteIcon;
				needSave = true;
			}

			// --------------------------------------------------------------
			LangageType lang = (LangageType)EditorGUILayout.EnumPopup( GetString(LangString.LangageType) , StaticSimpleSelectionMain.HistoryData.Langage );
			if( lang != StaticSimpleSelectionMain.HistoryData.Langage ){
				StaticSimpleSelectionMain.HistoryData.Langage = lang;
				needSave = true;
				Repaint();
			}

			// --------------------------------------------------------------
			EditorGUILayout.LabelField("");
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.LabelField(GetString(LangString.Favorite), EditorStyles.boldLabel);
			EditorGUILayout.EndHorizontal();

			// EditorGUILayout.BeginHorizontal();
			// // toggle view
			// if( GUILayout.Button(m_bFileNameOnly ? "Full Path" : "File Name" ) ){
			// 	m_bFileNameOnly = !m_bFileNameOnly;
			// }
			// EditorGUILayout.EndHorizontal();

			// --------------------------------------------------------------
			m_scroll = EditorGUILayout.BeginScrollView(m_scroll);
			GUIContent cont = new GUIContent( StaticSimpleSelectionMain.GetIcon( IconType.Star ) );
			// --------------------------------------------------------------
			for( int i = 0; i < favorite.Count; ++i )
			{
				EditorGUILayout.BeginHorizontal();
				var data = favorite[i];
				if( data == null )
					continue;
				string path = data.GetPath( true );
				string displayName = data.displayName;
				GUILayout.Label( i < 10 ? ((i+1)%10).ToString() : "" , EditorStyles.miniLabel , GUILayout.Width(10) );

				cont.text = path;
				cont.image = data.Icon;
				GUILayout.Label(cont,GUILayout.Height(20));

				// Display name Edit
				string strInput = GUILayout.TextField( displayName, GUILayout.Width(150) );
				if( strInput != displayName ){
					data.displayName = strInput;
					needSave = true;
				}
				// move up
				if( GUILayout.Button( StaticSimpleSelectionMain.GetIcon( IconType.Up ) , EditorStyles.miniLabel, new GUILayoutOption[]{ GUILayout.Width(20) , GUILayout.Height(20)}) 
					&& i-1 >= 0 ){
					favorite.RemoveAt(i);
					favorite.Insert( i-1 , data );
					needSave = true;
				}
				// move down
				if( GUILayout.Button( StaticSimpleSelectionMain.GetIcon( IconType.Down ) , EditorStyles.miniLabel, new GUILayoutOption[]{ GUILayout.Width(20) , GUILayout.Height(20)}) 
					&& i+1 < favorite.Count ){
					favorite.RemoveAt(i);
					favorite.Insert( i+1 , data );
					needSave = true;
				}
				// remove
				if( GUILayout.Button( StaticSimpleSelectionMain.GetIcon( IconType.Delete ) , EditorStyles.miniLabel, new GUILayoutOption[]{ GUILayout.Width(20) , GUILayout.Height(20)}) ){
					favorite.RemoveAt(i);
					needSave = true;
				}
				EditorGUILayout.EndHorizontal();
			}

			// --------------------------------------------------------------
			EditorGUILayout.EndScrollView();


			// --------------------------------------------------------------



			if( needSave ){
				StaticSimpleSelectionMain.Save();
				SimpleSelectionHistoryWindow.Refresh();
			}
		}

		// --------------------------------------------------------------
		enum LangString{
			LangCount,
			AutoRemoveHistory,
			WithoutHierarcy,
			ScrollOnlyHitory,
			UpdateWhenSelected,
			ShowFavoriteIcon,
			LangageType,
			Favorite,
		}
		static string GetString( LangString type ){

			switch( StaticSimpleSelectionMain.HistoryData.Langage ){
				case LangageType.English:
					switch(type){
					case LangString.LangCount:						return "History Count";
					case LangString.AutoRemoveHistory:				return "Auto remove same file history";
					case LangString.WithoutHierarcy:				return "Without Hierarchy object";
					case LangString.ScrollOnlyHitory:				return "Scroll only history";
					case LangString.UpdateWhenSelected:				return "Update when selected history";
					case LangString.ShowFavoriteIcon:				return "Show favorite icon";
					case LangString.LangageType:					return "Langage";
					case LangString.Favorite:						return "Favorite";
					}
					break;
				case LangageType.Japanese:
					switch(type){
					case LangString.LangCount:						return "履歴数";
					case LangString.AutoRemoveHistory:				return "同じ履歴は自動で削除する";
					case LangString.WithoutHierarcy:				return "Hierarchyの選択は除外する";
					case LangString.ScrollOnlyHitory:				return "履歴のみスクロールする";
					case LangString.UpdateWhenSelected:				return "選択した時も履歴に追加する";
					case LangString.ShowFavoriteIcon:				return "お気に入りアイコンを表示する";
					case LangString.LangageType:					return "Langage";
					case LangString.Favorite:						return "お気に入り";
					}
					break;
			}
			return null;
		}
	}
}