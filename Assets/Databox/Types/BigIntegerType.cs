﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Numerics;
using UnityEngine;
using Databox;

[System.Serializable]
[DataboxTypeAttribute(Name = "BigInteger")]
public class BigIntegerType : DataboxType {
	
	[SerializeField]
	private BigInteger _bigInteger;
	[SerializeField]
	public BigInteger InitValue;
	public BigInteger Value
	{
		get => _bigInteger;
		set
		{
			if (value == _bigInteger){return;}
			
			_bigInteger = value;			
			if (OnValueChanged != null){OnValueChanged(this);}
		}
	}
	
	public BigIntegerType(){}
	
	public BigIntegerType(BigInteger _s)
	{
		_bigInteger = _s;
	}
	
	public override void DrawEditor()
	{
		var str = GUILayout.TextField(Value.ToString("#,#"));
		Value = BigInteger.Parse(str, NumberStyles.Number);
	}
	
	public override void DrawInitValueEditor()
	{
		GUI.color = Color.yellow;
		GUILayout.Label("Init Value:");
		GUI.color = Color.white;
		
		var str = GUILayout.TextField(InitValue.ToString( "#,#"));
		InitValue = BigInteger.Parse(str, NumberStyles.Number);
	} 
	
	// Reset value back to initial value
	public override void Reset()
	{
		Value = InitValue;
	}
	
	// Important for the cloud sync comparison
	public override string Equal(DataboxType changedValue)
	{
		var value = changedValue as BigIntegerType;
		if (!Value.Equals(value.Value))
		{
			// return original value and changed value
			return Value + " : " + value.Value;
		}
		else
		{
			return "";
		}
	}
	
	public override void Convert(string value)
	{
		Value = _bigInteger = BigInteger.Parse(value, NumberStyles.Number);
		InitValue = Value;
	}
}
