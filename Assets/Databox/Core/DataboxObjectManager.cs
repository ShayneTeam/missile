﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Serialization;

namespace Databox
{
    // Databox Object Manager.
    // Manage multiple databox object in one place
    //
    [System.Serializable]
    [CreateAssetMenu(menuName = "Databox/New Databox Object Manager")]
    public class DataboxObjectManager : ScriptableObject
    {
        int loadedCount = 0;

        public float LoadingPercent
        {
            get
            {
                if (databoxContainers.Count == 0)
                    return 100f;
                
                return (float)loadedCount / databoxContainers.Count;
            }
        }


        [System.Serializable]
        public class DataboxContainerSchema
        {
            public string id;
            public DataboxObject dataObject;
            public bool editorOpen; // setting for runtime editor

            public DataboxContainerSchema(string id, DataboxObject dataObject)
            {
                this.id = id;
                this.dataObject = dataObject;
            }
        }


        [FormerlySerializedAs("dataObjects")] public List<DataboxContainerSchema> databoxContainers = new List<DataboxContainerSchema>();
        private Dictionary<string, DataboxContainerSchema> _containersCached = new Dictionary<string, DataboxContainerSchema>();

        public DataboxObject GetDataboxObject(string id)
        {
            if (_containersCached.TryGetValue(id, out var data))
            {
                return data.dataObject;
            }

            foreach (var container in databoxContainers)
            {
                if (container.id == id)
                {
                    _containersCached.Add(id, container);
                    return container.dataObject;
                }
            }

            Debug.LogError($"Databox: No Databox Object with id: {id} has been found");
            return null;
        }

        public void Load(string id)
        {
            var databoxObject = GetDataboxObject(id);
            if (databoxObject == null || databoxObject.IsLoaded || !databoxObject.CanLoad())
                return;
            
            databoxObject.LoadDatabase();
        }

        public void LoadAll(Action callbackEach, Action callbackAll)
        {
            loadedCount = 0;

            void LocalOnLoaded(string databoxId)
            {
                loadedCount++;

#if UNITY_EDITOR
                Debug.Log($"{nameof(DataboxObjectManager.LoadAll)} : {databoxId}");
#endif
            
                callbackEach?.Invoke();

                if (loadedCount == databoxContainers.Count)
                {
                    callbackAll?.Invoke();
                }
            }
            
            foreach (var data in databoxContainers)
            {
                // 로드할 수 없는거라면, 로드스킵 
                if (!data.dataObject.CanLoad() || data.dataObject.IsLoaded)
                {
                    LocalOnLoaded(data.id);
                }
                else
                {
                    void LocalOnDatabaseLoaded()
                    {
                        LocalOnLoaded(data.id);
                        // 이벤트 해지
                        data.dataObject.OnDatabaseLoaded -= LocalOnDatabaseLoaded;
                    }
                    // 이벤트 등록
                    data.dataObject.OnDatabaseLoaded += LocalOnDatabaseLoaded;
                    // 로드
                    data.dataObject.LoadDatabase();
                }
            }
        }
    }
}