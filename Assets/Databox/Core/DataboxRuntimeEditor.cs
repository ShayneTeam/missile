﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Databox.Ed;

namespace Databox
{
	public class DataboxRuntimeEditor : MonoBehaviour {
		
		
		public DataboxObjectManager manager;
		
		public bool disableInspector;
		public bool disableDeleteOption;
		public bool disableConfigurationOption;
		public bool disableCloudOption;
		
		public GUISkin editorSkin;
		
		int selectedDB = 0;
		

		void OnGUI()
		{
			if (manager == null)
			{
				GUILayout.Label("No Databox manager assigned");
				return;
			}
			
			using (new GUILayout.HorizontalScope())
			{
				
				for (int d = 0; d < manager.databoxContainers.Count; d ++)
				{
					if (editorSkin != null)
					{
						GUI.skin = editorSkin;
					}
				
				
					if (GUILayout.Button(manager.databoxContainers[d].id))
					{
						selectedDB = d;
						manager.databoxContainers[d].editorOpen = !manager.databoxContainers[d].editorOpen;
					}
				}
			}
			
			for (int d = 0; d < manager.databoxContainers.Count; d ++)
			{	
				if (manager.databoxContainers[d].dataObject != null && selectedDB == d)
				{
					DataboxEditor.DrawEditorRuntime(
						manager.databoxContainers[d].dataObject, 
						editorSkin, 
						disableInspector,
						disableDeleteOption,
						disableConfigurationOption,
						disableCloudOption,
						manager.databoxContainers[d].editorOpen);
				}
			}
		}
	}
}