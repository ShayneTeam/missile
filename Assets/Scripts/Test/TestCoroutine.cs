﻿using System;
using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace Test
{
	public class TestCoroutine : MonoBehaviour
	{
		private int _enableCount;
		private CoroutineHandle _handle;


		private void OnEnable()
		{
			_enableCount++;
	    
			var coroutine = StartCoroutine(TestFunction());
			/*
			if (coroutine != null) StopCoroutine(coroutine);
			coroutine = StartCoroutine(TestFunction());
			
			if (coroutine != null) StopCoroutine(coroutine);
			coroutine = StartCoroutine(TestFunction());*/

			_handle = Timing.RunCoroutine(_TestCoroutine().CancelWith(gameObject));
		}

		private void Update()
		{
			if (_handle.IsRunning)
            	return;
		}

		private IEnumerator TestFunction()
		{
			var enableCount = _enableCount;

			//while (enabled)
			while (true)
			{
				ULogger.Log($"Coroutine Index : {enableCount}");

				yield return new WaitForSeconds(1f);
			}
		}

		private IEnumerator<float> _TestCoroutine()
		{
			yield return Timing.WaitUntilDone(_TestChain().CancelWith(gameObject));
		}
		
		private IEnumerator<float> _TestChain()
		{
			while (true)
				yield return Timing.WaitForOneFrame;
		}
	}
}
