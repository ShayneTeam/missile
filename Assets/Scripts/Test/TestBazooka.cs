﻿using InGame;
using Lean.Pool;
using UnityEngine;
using Utils;

namespace Test
{
    public class TestBazooka : MonoBehaviour
    {
        [SerializeField] private GameObject missilePrefab;
        [SerializeField] private Transform muzzle;
        private Transform _transform;


        public void Launch(Vector2 endPos)
        {
            if (_transform == null)
                _transform = transform;
            
            var position = (Vector2)_transform.position;
            
            // 야매 플립 
            var isFlipped = _transform.lossyScale.x < 0;
            
            var angle = Bazooka.GetLookAtAngle(position, endPos, isFlipped);

            // 타겟 향하도록 바주카 회전 
            var rotationLookAtTarget = Quaternion.Euler(0,0,angle);
            _transform.rotation = rotationLookAtTarget;

            // 미사일 스폰 
            {
                var newMissile = LeanPool.Spawn(missilePrefab, muzzle.position, rotationLookAtTarget, _transform.parent);
                var missile = newMissile.GetComponent<Missile>();
                missile.IsFlip = isFlipped;

                // 
                var direction = MathHelpers.DegreeToVector2(angle);
                missile.ShotWithDirection(direction);
            }
        }
    }
}
