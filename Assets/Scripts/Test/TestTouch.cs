﻿using UnityEngine;
using UnityEngine.EventSystems;

namespace Test
{
    public class TestTouch : MonoBehaviour, IPointerClickHandler
    {
        public void OnPointerClick(PointerEventData eventData)
        {
            if (Camera.main == null)
                return;

            // 터치되서 넘어온 스크린 좌표를 월드 좌표로 변환 
            var touchedWorldPoint = Camera.main.ScreenToWorldPoint(eventData.position);

            var testBazooka = FindObjectOfType<TestBazooka>();
            if (testBazooka != null)
            {
                // 쏘는 방향에 따라 플립
                var scale = testBazooka.transform.localScale;
                if (touchedWorldPoint.x < testBazooka.transform.position.x)
                {
                    scale.x = -1f;
                }
                else
                {
                    scale.x = 1f;
                }
                testBazooka.transform.localScale = scale;
                
                //
                testBazooka.Launch(touchedWorldPoint);
            }
        }
    }
}