﻿using Bolt;
using UnityEngine;

namespace Test
{
    public class TestCustomEvent : MonoBehaviour
    {
        [SerializeField] private GameObject target;

        public void OnClick()
        {
            const string eventName = "SetBg";
            //target.SetActive(false);
            /*
            target.SetActive(true);
            CustomEvent.Trigger(target, eventName, true);
            
            CustomEvent.Trigger(target, eventName, false);
            CustomEvent.Trigger(target, eventName, false);
            target.SetActive(false);
            */
            target.SetActive(false);
            target.SetActive(true);
            CustomEvent.Trigger(target, eventName, false);
            CustomEvent.Trigger(target, eventName, true);
            CustomEvent.Trigger(target, eventName, false);
            CustomEvent.Trigger(target, eventName, true);
            //target.SetActive(true);
        }
    }
}