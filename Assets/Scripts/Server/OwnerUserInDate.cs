﻿using UnityEngine;

namespace Server
{
    public class OwnerUserInDate : MonoBehaviour
    {
        public virtual string InDate => ServerMyInfo.InDate;
    }
}