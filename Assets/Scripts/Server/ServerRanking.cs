﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using BackEnd;
using CodeStage.AntiCheat.ObscuredTypes;
using Global;
using Global.Extensions;
using InGame.Controller;
using InGame.Data;
using LitJson;
using MEC;
using UnityEngine;
using Utils;

// ReSharper disable ClassNeverInstantiated.Global



namespace Server
{
    // LitJson 변환용 클래스 
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "UnassignedField.Global")]
    public class RankUserData
    {
        public string gamerInDate;
        public string nickname;
        public int rank;
        public int score;   // 최고 스테이지 
        public int index;
        
        // 최대 피해량
        // 반드시 ObscuredDouble로 걸 것
        // LitJson.ToObject 오류 
        // 서버에 소숫점 없이 저장되면, ListJson에서 Int64로 인식
        // 그 데이터를 Double로 파싱하려고 하면 Int64 -> Double 파싱 오류 뜸 
        // 이 문제를 해결하기 위해 JsonMapperRegister로 등록한 함수가 호출되게 함
        // ObscuredDouble으로 Import 돼야할 때, 강제로 변수 타입을 double로 '강제 캐스팅'해서 넘김
        // Data -> Int64 -> Double이 아닌, 
        // Data -> double -> ObscuredDouble이 되게 함 
        public ObscuredDouble damage;  
        
        public static RankUserData ParseRankData(JsonData row)
        {
            return new RankUserData
            {
                gamerInDate = row[nameof(gamerInDate)].ToString(),
                nickname = row[nameof(nickname)].ToString(),
                rank = int.Parse(row[nameof(rank)].ToString()),
                score = int.Parse(row[nameof(score)].ToString()),
                index = int.Parse(row[nameof(index)].ToString()),
                damage = double.Parse(row[nameof(damage)].ToString(), CultureInfo.InvariantCulture),
            };
        }
    }

    public class ServerRanking : MonoBehaviourSingleton<ServerRanking>
    {
        // 스테이지 랭킹 UUID
        public const string RankingUUID = "ffdd94b0-af0d-11ec-ab1d-ddff9191cabf";

        private string _myRankInDate;

        private CoroutineHandle _autoGettingMyRank;
        private CoroutineHandle _autoUpdatingMyRank;

        private RankUserData _myRankData;
        private int _rankedBestStage;
        private double _rankedDamage;

        private const float AutoGettingMyRankPeriod = 70f;
        // 새로운 랭킹 테이블에 빠르게 등록시키기 위해 특별히 주기를 매우 낮춤
        // 랭킹에 등록돼있지 않으면, 약탈 매칭 에러가 나므로, 빠르게 등록시켜야 함 
        private const float AutoUpdatingMyRankPeriod = 5f; 


        private void OnDestroy()
        {
            StopAllAuto();
        }

        public void StopAllAuto()
        {
            _myRankData = null;
            _rankedBestStage = 0;
            
            Timing.KillCoroutines(_autoGettingMyRank);
            Timing.KillCoroutines(_autoUpdatingMyRank);
        }

        public void StartAutoGettingMyRank()
        {
            // 자기 자신의 랭킹만 조회 (동기 호출)
            var result = Backend.URank.User.GetMyRank(RankingUUID);
            OnGotMyRank(result);
            
            // 랭크 자동 업데이트 설정 
            if (!_autoGettingMyRank.IsRunning)
            {
                // 일부러 주기를 UpdateRanking 주기와 다르게 준것에 유의
                // 랭킹의 업데이트와 가져오기 요청을 동시에 하면, 가져오기 요청에서 업데이트되지 않은 랭크가 넘어옴 (100%는 아니지만, 그런 빈도가 잦음) 
                // 일부러 업데이트보다 느린 주기를 가지게 해서, 가져오기 요청에선 항상 갱신된 랭크 값이 넘어오게 함
                _autoGettingMyRank = Timing.CallPeriodically(float.PositiveInfinity, AutoGettingMyRankPeriod, () =>
                {
                    // 비동기 호출
                    Backend.URank.User.GetMyRank(RankingUUID, OnGotMyRank);
                }, Segment.RealtimeUpdate);
            }
        }

        private void OnGotMyRank(BackendReturnObject result)
        {
            if (!result.IsSuccess()) return;

            _myRankData = RankUserData.ParseRankData(result.FlattenRows()[0]);

            // 갱신 (실제 랭크를 가져오는 데 성공했으므로 갱신해도 됨. 만약 언랭크면 GetMyRank 자체가 실패 함)
            _rankedBestStage = _myRankData.score;
            _rankedDamage = _myRankData.damage;

            ULogger.Log($@"내 랭킹 " +
                        $"- 유저 가입일자 : {_myRankData.gamerInDate}" +
                        $"- 유저 점수 : {_myRankData.score.ToLookUpString()}" +
                        $"- 유저 Index : {_myRankData.index.ToLookUpString()}" +
                        $"- 유저 랭킹 : {_myRankData.rank.ToLookUpString()}"
            );
        }

        public RankUserData GetMyRank()
        {
            return _myRankData;
        }

        public void StartAutoUpdatingMyRank()
        {
            // 에디터 환경에선, 테스트를 위해 스테이지 랭킹 올리지 않음 
            if (ExtensionMethods.IsUnityEditor()) return;

            // 일정 주기로 랭킹 갱신
            Timing.KillCoroutines(_autoUpdatingMyRank);

            _autoUpdatingMyRank = Timing.CallPeriodically(float.PositiveInfinity, AutoUpdatingMyRankPeriod, () =>
            {
                // 뒤끝규칙 ( 한국시간 4~5시는 점검시간이라 업데이트가 안됨. 하지만 호출하면 인증하느라 DB 읽기비용을 사용하기 때문에 코드에서 막음)
                if (ServerTime.Instance.NowUnscaled.ToUniversalTime().Hour == 19) return;
                    
                // 닉네임에 운영자 키워드가 들어간 경우, 랭킹 업데이트 하지 않음.
                if (ServerMyInfo.NickName.Contains(FirebaseRemoteConfigController.gmNickNameKeyword)) return;

                // 업데이트 조건
                // 현재 최고 스테이지가 랭크된 스테이지보다 클 때
                // 현재 최고 데미지가 랭크된 데미지보다 클 때
                if (UserData.My.Stage.bestStage <= _rankedBestStage && InGameControlHub.My.MissileController.GetMaxDamage() <= _rankedDamage) return;
                    
                // 업데이트
                UpdateMyRank();
                
            }, Segment.RealtimeUpdate);
        }

        public static List<RankUserData> GetRankers(int limit)
        {
            var stageBestRanking = Backend.URank.User.GetRankList(RankingUUID, limit);
            if (!stageBestRanking.IsSuccess())
                return null;

            var rankers = new List<RankUserData>();

            var flattenRows = stageBestRanking.FlattenRows();
            for (var i = 0; i < flattenRows.Count; i++)
            {
                var rankUser = RankUserData.ParseRankData(flattenRows[i]);
                rankers.Add(rankUser);
            }

            return rankers;
        }

        public static IEnumerator<float> _RequestRankersTables(string gamerInDate)
        {
            var loadTableList = new List<string>
            {
                ServerTable.Costume,
                ServerTable.Bazooka,
            };
            var rankUserData = UserData.Other[gamerInDate];
            yield return Timing.WaitUntilDone(rankUserData._RequestServerData(loadTableList));
        }
        
        public static IEnumerator<float> _GetRankers(int limit, int offset, Action<List<RankUserData>> callback)
        {
            var isDone = false;
            
            // 뒤끝 비동기 호출은 다른 스레드에서 호출 됨
            // 만약 콜백이 메인 스레드 호출이 강제되는 루틴 일 경우, 예외 발생 
            // 그래서 .Poll 타이밍을 Update로 정해둔 SendQueue로 호출 함 
            SendQueue.Enqueue(Backend.URank.User.GetRankList, RankingUUID, limit, offset, (result) =>
            {
                isDone = true;
                
                if (!result.IsSuccess())
                {
                    callback(null);
                    return;
                }

                var rankers = new List<RankUserData>();

                var flattenRows = result.FlattenRows();
                for (var i = 0; i < flattenRows.Count; i++)
                {
                    var rankUser = RankUserData.ParseRankData(flattenRows[i]);
                    rankers.Add(rankUser);
                }

                callback(rankers);
            });

            while (!isDone) yield return Timing.WaitForOneFrame;
        }

        public void UpdateMyRank()
        {
            // 최초 진입 시엔 총 두 번의 싸이클이 돌아야 갱신 됨
            // 1 싸이클 : Update, Insert로 인데이트 가져오기  
            // 2 싸이클 : UpdateUserScore 정상 처리
            //
            // 1 싸이클에서 인데이트 가져 온 다음, 바로 2싸이클 호출 처리해줄까? 하다 
            // 랭킹 업데이트도 딜레이줘가며 서버 비용 낮추고 있는데, 바로 호출하면 비용 더 나가니 패스 

            var bestStage = UserData.My.Stage.bestStage;
            var maxDamage = InGameControlHub.My.MissileController.GetMaxDamage();
            
            var param = new Param
            {
                // 최고 스테이지 
                { ParamKey.bestStage, bestStage },
                // 최대 피해량
                { ParamKey.damage, maxDamage }
            };

            // 내 랭킹 인데이트 있다면
            if (!string.IsNullOrEmpty(_myRankInDate))
            {
                // UpdateUserScore 함수는 Backend.GameData.Update 함수에 랭킹 갱신 기능이 추가된 함수 입니다.
                // UpdateUserScore 함수를 이용하지 않고 갱신된 데이터는 랭킹 점수, 추가 항목 관계없이 모두 랭킹에 반영되지 않습니다.
                Backend.URank.User.UpdateUserScore(RankingUUID, ServerTable.Ranking, _myRankInDate, param, callback =>
                {
                    var updatingUserScore = callback.CommonErrorCheck();
                    if (updatingUserScore.IsSuccess())
                    {
                        ULogger.Log($@"최고랭킹 업데이트 성공");
                        
                        // UpdateUserScore가 돼야만 랭크에 반영됨. 그러므로, 이때만 갱신
                        // 일반 Update, Insert에서는 갱신되면 안됨. 그때는 랭크에 반영되지 않기 때문
                        _rankedBestStage = bestStage;
                        _rankedDamage = maxDamage;
                        
                        // 원래 여기서 GetMyRank를 호출해서 갱신 데이터를 들고 있게 해주는 게 맞지만,
                        // GetMyRank는 별도 주기로 계속 호출하니 따로 호출하지 않도록 함 
                    }
                    else
                    {
                        ULogger.Log($@"최고랭킹 업데이트 실패 / {callback.GetMessage()}");
                    }
                    // 이후 처리
                });
            }
            // 내 랭킹 인데이트 없다면 (게임 실행 후, 처음 갱신)
            else
            {
                // 아래 로직을 맨 처음 Register 할 때, 딱 한 번 호출하려다, 그냥 여기다가 그냥 둔다 
                // 아래 로직도 비동기 호출이라 실패할 가능성이 있다 
                // 한 번 실패하면, 그 다음부터 모든 랭크 업데이트가 안 될 가능성이 있으므로, 이 로직을 여기다 둬서 실패하더라도 재호출되게 함 
                Backend.GameData.Update(ServerTable.Ranking, new Where(), param, updatingRank =>
                {
                    if (updatingRank.IsSuccess())
                    {
                        // 랭킹 테이블에 등록한, 내 인데이트 가져 옴 
                        Backend.GameData.GetMyData(ServerTable.Ranking, new Where(), gettingMyRankInDate =>
                        {
                            if (!gettingMyRankInDate.IsSuccess()) return;

                            // 갱신 
                            _myRankInDate = gettingMyRankInDate.GetInDate();
                        });

                        // 업데이트 성공했으므로, 아래 인서트 구문 못 타게 함 
                        return;
                    }
                    
                    /*
                        존재하지 않는 tableName인 경우
                        statusCode : 404
                        message : table not found, table을(를) 찾을 수 없습니다

                        where 조건으로 update할 테이블을 검색했으나 테이블이 존재하지 않은 경우
                        statusCode : 404
                        message : gameInfo not found, gameInfo을(를) 찾을 수 없습니다

                        (스키마) 스키마를 정의하지 않은 컬럼을 where 조건으로 검색하려 한 경우
                        statusCode : 404
                        message : gameInfo not found, gameInfo을(를) 찾을 수 없습니다
                     */
                    if (!updatingRank.IsSuccess() && updatingRank.GetStatusCode() == "404")
                    {
                        // 랭킹을 갱신하기 위해서는 먼저 랭킹을 갱신하려는 유저가 해당 테이블을 1회 Insert 하여 row를 생성해야 합니다.
                        Backend.GameData.Insert(ServerTable.Ranking, param, insertingMyRankInDate =>
                        {
                            if (!insertingMyRankInDate.IsSuccess()) return;

                            // 갱신
                            _myRankInDate = insertingMyRankInDate.GetInDate();
                        });
                    }
                });
            }
        }
    }
}