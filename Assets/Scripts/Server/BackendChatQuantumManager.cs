﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using BackEnd.Tcp;
using InGame.Data;
using LitJson;
using QFSW.QC;
using Server;

public class BackendChatQuantumManager : Singleton<BackendChatQuantumManager>
{
    [Command("test")]
    public void Test()
    {
        ULogger.Log("테스트 콜!");
    }
    
    [Command("tuto")]
    public void TutorialStart()
    {
        TutorialPopup.Instance.TutorialConditionCheck(TutorialPopup.GuideConditionType.DEFEAT, 1);
    }

    private void Start()
    {
        // 첫 번째 방법
        Backend.Chat.OnRecentChatLogs = (logs) =>
        {
            ULogger.Log(logs.ErrInfo.ToString());
            ULogger.Log("들어간 채널 종류 : " + logs.channelType.ToString());

            foreach (var chat in logs.LogInfos)
            {
                ULogger.Log("닉네임 : " + chat.NickName);
                ULogger.Log("메시지 : " + chat.Message);
            }
        };

        Backend.Chat.OnJoinChannel = (JoinChannelEventArgs args) =>
        {
            //입장에 성공한 경우
            if (args.ErrInfo == ErrorInfo.Success)
            {
                // 내가 접속한 경우 
                if (!args.Session.IsRemote)
                {
                    ULogger.Log("채널에 접속했습니다");
                }
                //다른 유저가 접속한 경우
                else
                {
                    ULogger.Log(args.Session.NickName + "님이 접속했습니다");
                }
            }
            else
            {
                //에러가 발생했을 경우
                ULogger.Log("입장 도중 에러가 발생했습니다 : " + args.ErrInfo.Reason);
            }
        };

        Backend.Chat.OnChat = (ChatEventArgs args) =>
        {
            ULogger.Log($"OnChat {args.ErrInfo}");

            if (args.ErrInfo == ErrorInfo.Success)
            {
                // 자신의 메시지일 경우
                if (!args.From.IsRemote)
                {
                    ULogger.Log("나 : " + args.Message);
                }
                // 다른 유저의 메시지일 경우
                else
                {
                    ULogger.Log($"{args.From.NickName}님 : {args.Message}");
                }
            }
            else if (args.ErrInfo.Category == ErrorCode.BannedChat)
            {
                // 도배방지 메세지 
                if (args.ErrInfo.Detail == ErrorCode.BannedChat)
                {
                    ULogger.Log("메시지를 너무 많이 입력하였습니다. 일정 시간 후에 다시 시도해 주세요");
                }
            }
        };
    }

    [Command("#푸쉬등록")]
    public void PutDeviceToken()
    {
        BackEndManager.PutDeviceToken();
    }
    
    [Command("#푸쉬해제")]
    public void DeleteDeviceToken()
    {
        BackEndManager.DeleteDeviceToken();
    }
    
    [Command("#배틀패스")]
    public void BattlePass(string idx)
    {
        UserData.My.BattlePass.PurchaseApplyBattlePass(idx);
        // UserData.My.BattlePass.ApplyBattlePass(1);
    }
    
    [Command("#채팅상태")]
    public void GetChatStatus()
    {
        BackendReturnObject chatStatusBRO = Backend.Chat.GetChatStatus();
        if (chatStatusBRO.IsSuccess())
        {
            ULogger.Log($@"[서버상태] 채팅이 초기화되어있습니다.");
            string yn = chatStatusBRO.GetReturnValuetoJSON()["chatServerStatus"]["chatServer"].ToString();
            if (yn == "y")
            {
                ULogger.Log($@"채팅에 접속할 수 있는 상태입니다.");
            }
            else
            {
                ULogger.Log($@"서버에서 채팅을 끈 상태입니다.");
            }
        }
        else
        {
            ULogger.Log($@"[서버상태] 채팅이 활성화 되어 있지 않습니다.");
        }
    }

    [Command("#약탈")]
    public void PlunderVictory(string gamerIndate)
    {
        //BackendManager.instance.Plunder(gamerIndate);
    }
    
    [Command("#약탈히스토리")]
    public void PlunderAttackedHistory()
    {
        //BackendManager.ApplyPlunderLog();
    }

    [Command("#상대정보조회")]
    public void GetOtherUserInfo(string gamer_id)
    {
        //BackendManager.instance.GetOtherUserInfo(gamer_id);
    }

    [Command("#최고랭킹업데이트")]
    public void UpdateBestRanking()
    {
        ServerRanking.Instance.UpdateMyRank();
    }


    [Command("#우편리스트")]
    public void GetMailList()
    {
        ServerPost.Instance.GetPostList(null);
    }

    [Command("#출석체크")]
    public void Attendance()
    {
        //BackEndManager.Attendance();
    }

    [Command("#내랭킹")]
    public void SearchMyStageRank()
    {
        //ServerRanking.GetMyRanking();
    }

    [Command("#베스트랭킹")]
    public void GetBestRanking()
    {
        //BackendManager.instance.GetBestRanking();
    }

    [Command("#갭랭킹")]
    public void SearchGameGapRank()
    {
        //BackendManager.PlunderDelegator.MatchMakingPlunder();
    }

    [Command("#우편수령")]
    public void receivedMail(string postIndate)
    {
        //ServerPost.Instance.ReceivePost(postIndate);
    }

    [Command("#인데이트")]
    public void SearchInDate(string nickname)
    {
        // 닉네임 검색을 통한 다른 유저의 inDate 가져오기
        var bro = Backend.Social.GetUserInfoByNickName(nickname);
        if (!bro.IsSuccess())
            return;
        
        // nickname으로 게임 유저 inDate가져오는 함수
        var inDate = bro.FlattenRows()[0]["inDate"].ToString();
        ULogger.Log($"닉네임 {nickname}의 inDate : {inDate}");
    }

    private List<ChannelNodeObject> _channelNodeObjects = new List<ChannelNodeObject>();

    [Command("#채팅방리스트")]
    public void GetGroupChannelList()
    {
        // clear.
        _channelNodeObjects.Clear();

        var bro = Backend.Chat.GetGroupChannelList("General");
        if (bro.IsSuccess())
        {
            JsonData rows = bro.GetReturnValuetoJSON()["rows"];
            ChannelNodeObject channelNode;
            for (int i = 0; i < rows.Count; i++)
            {
                JsonData data = rows[i];
                channelNode = new ChannelNodeObject(ChannelType.Public, "General", data["inDate"].ToString(),
                    (int) data["joinedUserCount"], (int) data["maxUserCount"],
                    data["serverAddress"].ToString(), data["serverPort"].ToString(), data["alias"].ToString());

                _channelNodeObjects.Add(channelNode);
                ULogger.Log("이름 : " + data["alias"].ToString() + "\n inDate : " + data["inDate"].ToString());
            }
        }
        else
        {
            ULogger.Log($@"[서버상태] 채팅방 리스트를 가져오지 못했습니다.");
        }
    }

    void Awake()
    {
        // 씬이 바뀌어도 계속 SendQueue를 사용하기 위해서
        // 아래 DontDestroyOnLoad 함수를 통해 SendQueue 객체를 파괴되지 않는 오브젝트로 만듭니다.
        // 자세한 설명은 아래 유니티 개발자문서를 참고해주세요.
        // https://docs.unity3d.com/kr/current/ScriptReference/Object.DontDestroyOnLoad.html
        DontDestroyOnLoad(this);

        // 만약 SendQueue가 초기화 되지 않았다면 초기화 수행
        if (SendQueue.IsInitialize == false)
        {
            // SendQueue는 시작과 동시에 초기화가 수행됩니다.
            // 디버그 로그 활성화, 예외 이벤트 핸들러 등록
            SendQueue.StartSendQueue(true, ExceptionEvent);
        }
    }


    // SendQueue 내부에서 예외가 발생했을 경우  
    // 아래 이벤트 핸들러를 통해 예외 이벤트가 전달됩니다.
    void ExceptionEvent(Exception e)
    {
        ULogger.Log(e.ToString());
    }

    [Command("#닉네임설정")]
    void SetNickName(string nickName)
    {
        var changeNickName = Backend.BMember.CreateNickname(nickName);
        if (changeNickName.IsSuccess())
        {
            ULogger.Log($@"닉네임을 {nickName} 으로 설정하였습니다.");
        }
        else
        {
            ULogger.Log($"닉네임을 설정 오류 발생.");
        }
    }

    [Command("#채팅")]
    void ChatToChannel(string msg)
    {
        Backend.Chat.ChatToChannel(ChannelType.Public, msg);
    }

    [Command("#채팅연결상태")]
    void isChatConnect()
    {
        bool isConnect = Backend.Chat.IsChatConnect(ChannelType.Public);
        ULogger.Log(isConnect ? "채팅 연결이 되어 있습니다." : "채팅 연결이 되어있지 않습니다.");
    }

    [Command("#채팅참여")]
    void JoinChannel()
    {
        // 가입가능
        var random = _channelNodeObjects.Where(t => t.joinedUserCount < t.maxUserCount).ToList();
        if (random.Count == 0)
        {
            ULogger.Log($@"가입 가능한 채팅방이 없습니다.");
            return;
        }

        var channelNode = random.OrderBy(t => Guid.NewGuid()).First();
        ULogger.Log($"입장 : {channelNode.type} / {channelNode.host} / {channelNode.port.ToString()} / {channelNode.groupName} / {channelNode.inDate}");
        Backend.Chat.JoinChannel(channelNode.type, channelNode.host, channelNode.port, channelNode.groupName,
            channelNode.inDate, out var info);

        switch (channelNode.type)
        {
            case ChannelType.Public:
                break;
            case ChannelType.Guild:
                break;
        }
    }

    [Command("#채팅나가기")]
    public void LeaveChannel()
    {
        Backend.Chat.LeaveChannel(ChannelType.Public);
    }
}