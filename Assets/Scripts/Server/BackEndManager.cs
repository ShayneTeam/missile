﻿using System;
using System.Collections.Generic;
using BackEnd;
using Global.Extensions;
using InGame.Controller;
using InGame.Controller.Event;
using InGame.Data;
using LitJson;
using MEC;
using Newtonsoft.Json;
using TapjoyUnity;
using UI.Popup;
using UnityEngine;
using UnityEngine.Purchasing;

namespace Server
{
    public class BackEndManager : MonoBehaviourSingleton<BackEndManager>
    {
        [NonSerialized]
        public readonly Dictionary<string, string> BackEndTableInDateDic = new Dictionary<string, string>();

        [NonSerialized] private Version gameVersion = Version.Parse("0.0.0");

        // 뒤끝 라이브러리가 EnterPlayMode 사용하고 싶다면, 이 클래스를 계속 유지시켜서 다음 플레이 때 Backend.Initialize()가 호출되지 않게 해야 함
        // 뒤끝 내부서 이벤트 핸들러르 +=로 추가시키므로, 도메인 내리지 않는 이상 핸들러를 초기화시킬 수 없다 
        // 내가 할 수 있는거라곤, 핸들러가 두 번 추가되지 않게 하는 것 뿐임 
        public bool IsInitialized { get; private set; }

        private Dictionary<string, Dictionary<string, JsonData>> gamerEquipDic =
            new Dictionary<string, Dictionary<string, JsonData>>();


        private string _tempNoticeContents;

        void Start()
        {
            gameVersion = Version.Parse(Application.version);
        }

        protected void Update()
        {
            if (SendQueue.IsInitialize && Backend.IsInitialized)
            {
                Backend.Chat.Poll();
            }
        }

        public BackendReturnObject Initialize()
        {
            var bro = Backend.Initialize();
            IsInitialized = bro.IsSuccess();
            return bro;
        }

        public void ConnectNotification()
        {
            //실시간 알림 서버에 연결합니다.
            Backend.Notification.Connect();

            Backend.Notification.OnReceivedMessage = () => { ULogger.Log("새 쪽지가 도착했습니다!"); };

            Backend.Notification.OnReceivedUserPost = () => { ULogger.Log("새 유저 우편이 도착했습니다!"); };
        }

        public bool ServerStatusCheck()
        {
            var status = Backend.Utils.GetServerStatus();
            if (!status.IsSuccess())
                return false;

            var mBackendServerStatus = (int)status.GetReturnValuetoJSON()["serverStatus"];
            switch (mBackendServerStatus)
            {
                case 0: // 온라인
                    ULogger.Log("뒤끝 상태 : 온라인");
                    return true;
                case 1: // 오프라인
                    // todo : alert
                    ULogger.Log("뒤끝 상태 : 오프라인");
                    break;
                case 2: // 점검
                    ULogger.Log("뒤끝 상태 : 점검");
                    break;
                // todo : alert
                default:
                    break;
            }

            return false;
        }

        public bool ServerVersionCheck()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.OSXEditor:
                case RuntimePlatform.WindowsEditor:
                    break;
                case RuntimePlatform.IPhonePlayer:
                case RuntimePlatform.Android:

                    // 업데이트 버전 체크
                    var mBackendVersionCheck = Backend.Utils.GetLatestVersion().GetReturnValuetoJSON();
                    var mBackEndLastVersion = mBackendVersionCheck["version"].StringValue();
                    var mBackendUpdateType = mBackendVersionCheck["type"].IntValue();

                    var serverVersion = Version.Parse(mBackEndLastVersion);
                    var mGetClientVersion = new Version(gameVersion.Major, gameVersion.Minor, gameVersion.Build, 0);

                    if (serverVersion > mGetClientVersion)
                    {
                        if (mBackendUpdateType == 1) // 선택
                        {
                            ULogger.Log($"선택 업데이트 있어요! {mBackEndLastVersion}");
                        }
                        else // 2 = 필수
                        {
                            ULogger.Log($"필수 업데이트 있어요! {mBackEndLastVersion}");
                            // Application.Quit();
                            return false;
                        }
                    }

                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            return true;
        }

        public static void ChatBlock(string nickName)
        {
            if (ServerMyInfo.NickName == nickName) return;

            Backend.Chat.BlockUser(nickName, blockCallback =>
            {
                // 성공
                // if (blockCallback)
                // {
                //     callbackAction?.Invoke();
                // }
            });
            
            MessagePopup.Show("info_307"); 
        }
        
        public static void ChatUnBlock(string nickName)
        {
            Backend.Chat.UnblockUser(nickName);
            
            MessagePopup.Show("info_364");
        }

        public static IEnumerator<float> Attendance()
        {
            /*
         * 반드시 BackendTime.ServerSync 이후에 호출돼야 함
         * 그래야만 서버 시간 기준으로 계산됨 
         */

            var resultGet = Backend.GameData.GetMyData(ServerTable.Attendance, new Where()).CommonErrorCheck();
            if (!resultGet.IsSuccess()) yield break;

            // 신규가입! (처음 온 유저이거나, 타임스탬프키 없는 유저인 경우)
            if (resultGet.Rows().Count == 0 || !resultGet.Rows()[0].ContainsKey(ParamKey.resetTimestamp))
            {
                // Insert
                var resultInsert = Backend.GameData.Insert(ServerTable.Attendance, AttendanceParam).CommonErrorCheck();
                if (!resultInsert.IsSuccess()) yield break;

                // 일일 처리 
                yield return Timing.WaitUntilDone(DailyProcess());
            }
            // 기존 유저
            else
            {
                // 현재 접속된 시간이 저장된 데일리 초기화 시간보다 이후라면, 보상 지급 
                var currentTime = ServerTime.Instance.NowUnscaled;
                var dailyResetTimestamp = resultGet.Rows()[0][ParamKey.resetTimestamp].DLongValue();

                if (ServerTime.Instance.NowUnscaled.ToTimestamp() > dailyResetTimestamp)
                {
                    var whereParam = new Where();
                    whereParam.Equal(ParamKey.owner_inDate, Backend.UserInDate);

                    // 업데이트하고
                    var resultUpdate = Backend.GameData.Update(ServerTable.Attendance, whereParam, AttendanceParam)
                        .CommonErrorCheck();
                    if (!resultUpdate.IsSuccess())
                        yield break;

                    // 일일 처리 
                    yield return Timing.WaitUntilDone(DailyProcess());
                }
                // 이미 오늘자의 출석 처리를 함.
                else
                {
                    var timeStr = currentTime.ToString("yyyy-MM-dd");
                    ULogger.Log($@"{Backend.UserInDate} 님은 {timeStr} 날짜의 출석 처리를 이미 하셨습니다.");

                    var timespan = TimeSpan.FromSeconds(dailyResetTimestamp - currentTime.ToTimestamp());
                    ULogger.Log($"데일리 초기화까지 남은 시간 {timespan.ToString()}");
                }
            }
        }

        // 함수 가독성 향상을 위해, 함수 상단에 위치한 이 부분을 따로 뻄
        private static Param AttendanceParam
        {
            get
            {
                var endTime = DailyTimeController.GetEndTimeOf(ServerTime.Instance.NowUnscaled);
                return new Param { { ParamKey.resetTimestamp, endTime.ToTimestamp() } };
            }
        }

        public static IEnumerator<float> DailyProcess()
        {
            // 요 처리는 반드시 UserData.ServerSync가 이루어진 후 호출돼야 함 

            // 약탈 깃발 충전
            ServerPlunder.RechargePlunderFlag();

            // 약탈 복수 횟수 충전 
            ServerPlunder.RechargeRevengeTicket();

            // 데일리 미션 초기화 
            MissionController.ResetDailyMission();

            // 약탈 매칭 새로운 녀석들로 갱신 
            yield return Timing.WaitUntilDone(InGameControlHub.My.PlunderController._RequestNewMatchingOnce());

            // 이벤트 7일 출석 초기화
            EventController.NAttendance.Reset();
            
            // 데일리 기프트 초기화 
            DailyGiftController.ResetToCollectAgain();
            DailyChipController.ResetToCollectAgain();
            
            // 레이드 티켓 구매 횟수 초기화
            RaidController.RaidTicket.ResetBuyCount();
            
            // 배틀쉽 티켓 충전
            BattleshipController.RechargeTicket();

            // 갱신된 테이블들 바로 저장시킴
            // 유저가 바로 게임 종료 시, 출석 테이블은 갱신돼있는데, 인게임 테이블은 저장 안돼있을 수 있음 (일정 주기별 간격 저장이므로)
            UserData.My.SaveToServer();
        }

        public static void PrintGuestId()
        {
            var id = Backend.BMember.GetGuestID();
            ULogger.Log("로컬 기기에 저장된 게스트 아이디 :" + id);
        }

        public static bool ValidateReceipt(Product productInfo)
        {
            BackendReturnObject validation = null;

            if (ExtensionMethods.IsAndroid())
            {
                validation = Backend.Receipt.IsValidateGooglePurchase(productInfo.receipt, "receiptDescription");
            }
            else
            {
                validation = Backend.Receipt.IsValidateApplePurchase(productInfo.receipt, "receiptDescription");
            }

            // 영수증 검증에 성공한 경우
            if (validation.IsSuccess())
            {
                WebLog.Instance.Log("iap_purchase_validate_success", JsonConvert.SerializeObject(productInfo), new Dictionary<string, object> 
                {
                    {"iap_statusCode",validation.GetStatusCode()},
                    {"iap_message",validation.GetMessage()},
                });
                Tapjoy.TrackPurchase(productInfo.definition.id, productInfo.metadata.isoCurrencyCode, decimal.ToDouble(productInfo.metadata.localizedPrice));
            }
            // 영수증 검증에 실패한 경우 
            else
            {
                // Or ... an unknown product has been purchased by this user. Fill in additional products here....
                ULogger.Log($"{validation.GetStatusCode()} : {validation.GetMessage()}");
                
                WebLog.Instance.Log("iap_purchase_validate_fail", JsonConvert.SerializeObject(productInfo), new Dictionary<string, object>
                {
                    {"iap_statusCode",validation.GetStatusCode()},
                    {"iap_message",validation.GetMessage()},
                });
            }

            // 성공/실패 여부 반환
            return validation.IsSuccess();
        }

        public void GetTempNotice()
        {
            Backend.Notice.GetTempNotice((args) => { _tempNoticeContents = args; });
        }

        public bool ContainsContentsTempNotice(string contents)
        {
            return _tempNoticeContents.Contains(contents);
        }

        public static void PutDeviceToken()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    Backend.Android.PutDeviceToken(Backend.Android.GetDeviceToken(), (callback) =>
                    {
                        var result = callback.CommonErrorCheck();
                        if (result.IsSuccess())
                        {
                            ULogger.Log("안드로이드 푸쉬 등록에 성공했습니다.");
                        }
                    });
                    break;
                case RuntimePlatform.IPhonePlayer:
                    Backend.iOS.PutDeviceToken(isDevelopment.iosDev, (callback) =>
                    {
                        var result = callback.CommonErrorCheck();
                        if (result.IsSuccess())
                        {
                            ULogger.Log("iOS 푸쉬 등록에 성공했습니다.");
                        }
                    });
                    break;
                default:
                    ULogger.Log($"미지원 플랫폼 : {Application.platform}");
                    break;
            }
        }

        public static void DeleteDeviceToken()
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    Backend.Android.DeleteDeviceToken(callback =>
                    {
                        var result = callback.CommonErrorCheck();
                        if (result.IsSuccess())
                        {
                            ULogger.Log("안드로이드 푸쉬 해제에 성공했습니다.");
                        }
                    });
                    break;
                case RuntimePlatform.IPhonePlayer:
                    Backend.iOS.DeleteDeviceToken(callback =>
                    {
                        var result = callback.CommonErrorCheck();
                        if (result.IsSuccess())
                        {
                            ULogger.Log("iOS 푸쉬 해제에 성공했습니다.");
                        }
                    });
                    break;
                default:
                    ULogger.Log($"미지원 플랫폼 : {Application.platform}");
                    break;
            }
        }

        public static IEnumerator<float> _GetUserTableData(string curTable, string inDate, Action<string> callback)
        {
            var param = new Where();
            param.Equal(ParamKey.owner_inDate, inDate);

            var isDone = false;
            
            SendQueue.Enqueue(Backend.GameData.Get, curTable, param, bro =>
            {
                // 이게 과연 최선일까. 더 나은 방법이 있을까. 고민 
                // 콜백이 떨어질 때, 자동으로 알 수 있는 방법 없을까 
                // 꼭 이렇게 변수를 써야 하나 
                isDone = true;
                
                if (bro.IsSuccess())
                {
                    if (bro.Rows().Count > 0)
                    {
                        var flatFirstRow = bro.FlattenRows()[0];

                        if (flatFirstRow.ContainsKey("data"))
                        {
                            ULogger.Log($"{curTable} data 있음");

                            callback?.Invoke(flatFirstRow["data"].StringValue());
                        }
                        else
                            ULogger.Log($"{curTable} data 없음");
                    }
                    else 
                        ULogger.Log($"{curTable} ROW 없음 (에러)");
                }
                else
                    ULogger.Log($"{curTable} 테이블 로딩 에러");
            });

            while (!isDone) yield return Timing.WaitForOneFrame;
        }

        public static IEnumerator<float> SaveToServerTableData(string curTable, string ownerInDate, string data)
        {
            var isDone = false;
            
            // 1단계. 테이블 row inDate 가져오기 
            var where = new Where();
            where.Equal(ParamKey.owner_inDate, ownerInDate);
            
            SendQueue.Enqueue(Backend.GameData.Get, curTable, where, resultGet =>
            {
                isDone = true;
                
                if (resultGet.IsSuccess() && resultGet.Rows().Count > 0)
                {
                    var flatFirstRow = resultGet.FlattenRows()[0];

                    if (flatFirstRow.ContainsKey("data"))
                    {
                        // 2단계. Update 요청 위해 다시 false로 설정
                        isDone = false;
                        
                        var rowInDate = flatFirstRow["inDate"].StringValue();
                        
                        // 테이블 업데이트 하기 
                        var param = new Param
                        {
                            { "data", data }
                        };
                        SendQueue.Enqueue(Backend.GameData.UpdateV2, curTable, rowInDate, ownerInDate, param, resultUpdate =>
                        {
                            isDone = true;
                            
                            // 로그 
                            if (resultUpdate.IsSuccess())
                                ULogger.Log($"{curTable} 테이블 서버 동기화 성공");
                            else
                                ULogger.Log($"{curTable} 테이블 서버 동기화 실패", Color.red);
                        });
                    }
                }
            });
            
            while (!isDone) yield return Timing.WaitForOneFrame;
        }
    }

    public class ServerNoticeSchema
    {
        public string country;
        public string startTime;
        public string endTime;
        public string title;
        public string context;
        public string btn1Name;
        public string btn1Url;
        public string btn2Name;
        public string btn2Url;
    }

    public enum LoginType
    {
        None,
        Guest,
        Facebook,
        Google,
        Apple,
        Account_SignUp,
        Account,
    }
}