﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BackEnd;
using BestHTTP.SocketIO3.Events;
using Global.Extensions;
using Gpm.Common.ThirdParty.LitJson;
using MEC;
using QFSW.QC;
using UnityEngine;
using System;
using System.Collections.Generic;
using System.Text;
using BestHTTP.Examples.Helpers;
using BestHTTP.Examples.SocketIO3;
using BestHTTP.SocketIO;
using BestHTTP.SocketIO3.Events;
using Databox.OdinSerializer.Utilities;
using Global;
using InGame.Controller;
using InGame.Data;
using QFSW.QC;
using Random = UnityEngine.Random;
using Socket = BestHTTP.SocketIO3.Socket;
using SocketIOEventTypes = BestHTTP.SocketIO3.SocketIOEventTypes;
using SocketManager = BestHTTP.SocketIO3.SocketManager;
using SocketOptions = BestHTTP.SocketIO3.SocketOptions;

// ReSharper disable ClassNeverInstantiated.Local
// ReSharper disable MemberCanBePrivate.Global

// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable InconsistentNaming
// ReSharper disable UnassignedField.Global

namespace Server
{
    public partial class ServerRaid : MonoBehaviourSingletonPersistent<ServerRaid>
    {
        [SerializeField] private bool localHost;
        
        /*
         * 스키마
         */
        public class UserInfo
        {
            public string inDate;
            public string nickName;
            public int rank;
            public string bazooka;
            public string missile;
            public string costume;
            // 스킬 지속시간 & 쿨타임과 관련된 데이타
            public string relic;
            public string dRelic;
            public string stone;
            // 어빌리티 합산용
            public string soldier;
            public string princess;
        }
        
        private static SocketManager _raidServer;

        /*
         * 내 정보 
         */
        private static UserInfo MyInfo
        {
            get
            {
                _myInfo ??= new UserInfo();
                
                _myInfo.inDate = ServerMyInfo.InDate;
                _myInfo.nickName = ServerMyInfo.NickName;
                _myInfo.rank = ServerRanking.Instance.GetMyRank()?.rank ?? -1;
                _myInfo.bazooka = UserData.My.Bazooka.ToString();
                _myInfo.missile = UserData.My.Missiles.ToString();
                _myInfo.costume = UserData.My.Costume.ToString();
                _myInfo.relic = UserData.My.NormalRelic.ToString();
                _myInfo.dRelic = UserData.My.DiabloRelic.ToString();
                _myInfo.stone = UserData.My.InfinityStone.ToString();
                _myInfo.soldier = UserData.My.Soldiers.ToString();
                _myInfo.princess = UserData.My.Princess.ToString();

                return _myInfo;
            }
        }
        private static UserInfo _myInfo;

        /*
         * 이벤트 
         */

        public static event VoidDelegate OnDisconnected;
        
        /*
         * 함수
         */
        [Command("#소켓연결")]
        public static void Connect()
        {
            // 소켓 주소
            var address = ExtensionMethods.IsUnityEditor() && Instance.localHost ? "http://localhost:7243" : FirebaseRemoteConfigController.raidUrl;

            // 초기화
            _raidServer = new SocketManager(new Uri(address), new SocketOptions
            {
            });
            
            // 소켓 접속 성공시
            _raidServer.Socket.On<ConnectResponse>(SocketIOEventTypes.Connect, response =>
            {
                ULogger.Log($"소켓 연결 {nameof(SocketIOEventTypes.Connect)}");
            });

            _raidServer.Socket.On(SocketIOEventTypes.Disconnect, () =>
            {
                ULogger.Log($"소켓 끊김 {nameof(SocketIOEventTypes.Disconnect)}");
                
                // 이벤트 
                OnDisconnected?.Invoke();
            });

            _raidServer.Socket.On(SocketIOEventTypes.Error, () =>
            {
                ULogger.Log($"소켓 에러 {nameof(SocketIOEventTypes.Error)}");
            });
            
            // 방 이벤트 등록 
            RegisterRoomEvents();
        }

        private static IEnumerator<float> _UpdateMyInfo(Action<bool> callback)
        {
            // 연결 체크 
            if (!IsConnected())
            {
                callback?.Invoke(false);
                yield break;
            }
            
            // 실패 가능성 있으므로, 3번까지 시도 
            for (var count = 0; count < 3; count++)
            {
                // 결과 이벤트 
                bool isDone = false, isSuccess = false;
                _raidServer.Socket.Once<bool>(RaidEvent.SERVER_UPDATE_USER_INFO, (success) =>
                {
                    isDone = true; isSuccess = success;
                    ULogger.Log($"{nameof(RaidEvent.SERVER_UPDATE_USER_INFO)} : {success.ToString()}");
                });
                
                // 서버에 내 정보를 업데이트 요청함.
                var myInfo = MyInfo;
                _raidServer.Socket.Emit(RaidEvent.CLIENT_UPDATE_USER_INFO, myInfo);
                
                // 결과 올 때까지 대기 
                while (!isDone) yield return Timing.WaitForOneFrame;
                
                // 실패 시, 다시 시도 
                if (!isSuccess)
                {
                    // 바로 시도하지 않고, 딜레이 후 시도 
                    yield return Timing.WaitForSeconds(1f);
                    continue;
                }
                
                // 성공 시, 종료
                callback?.Invoke(true);
                yield break;
            }

            // 몇 번 시도 후에도 여기까지 왔다면 실패한 것 
            callback?.Invoke(false);
        }

        public static bool IsConnected()
        {
            if (_raidServer == null || !_raidServer.Socket.IsOpen)
            {
                ULogger.Log("소켓에 접속되어 있지 않습니다. 작업이 불가합니다.");
                return false;
            }

            return true;
        }
    }
    
    // ReSharper disable InconsistentNaming
    public static partial class RaidEvent
    {
        public const string CLIENT_UPDATE_USER_INFO = "CLIENT_UPDATE_USER_INFO";
        
        public const string SERVER_UPDATE_USER_INFO = "SERVER_UPDATE_USER_INFO";
    }
}