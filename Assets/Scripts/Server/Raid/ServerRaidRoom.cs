﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using BestHTTP.Extensions;
using InGame.Data;
using MEC;
using Utils;
using Random = UnityEngine.Random;
// ReSharper disable ClassNeverInstantiated.Local
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable InconsistentNaming
// ReSharper disable UnassignedField.Global

namespace Server
{
    public delegate void RoomStartDelegate(bool victory, int difficulty, float hpDownRatioPerSec);
    
    // 왠만한 처리는 직접 ServerRaid를 참조해서 처리 
    // 원래는 RaidRoomController 라는 애가 있었는데, 하는 것이라곤 ServerRaid 위임 처리밖에 없어서 삭제함 
    // 코드에서 직접 ServerRaid 사용하는게 훨씬 코드가 깔끔 
    public partial class ServerRaid : MonoBehaviourSingletonPersistent<ServerRaid>
    {
        /*
         * 방 정보 
         */
        public class RoomList
        {
            public List<RoomInfo> roomList;
        }

        public class RoomInfo
        {
            public string roomNum;
            public string owner;
            public int difficulty;
            // 서버 송수신을 Dictionary로 받는 처리를 몰라서 List로 설정 (리서치해봐야 함)
            public List<RoomUserInfo> users;
        }

        public class RoomUserInfo
        {
            public string socketId;
            public UserInfo data;
        }

        private static RoomInfo _roomInfo { get; set; }

        /*
         * 방 시작 정보
         */
        public class RoomStartInfo
        {
            public string roomNum;
            public string nextRoomNum;
            public int difficulty;
            public bool victory;
            public float hpDownRatioPerSec;
        }
        
        private static RoomStartInfo _roomStartInfo { get; set; }

        /*
         * 이벤트 
         */
        // 이벤트에 UserInfo 전체를 넘겨줄 필요없음 
        // 심플하게 인데이트만 전달하면 충분 
        // 그리고 소켓ID는 여기서만 쓰이는 구현이므로, 외부에서 의존하지 않게 주의 필요
        public static event StringStringDelegate OnJoinedUser; 
        public static event StringStringDelegate OnLeftUser;   
        public static event StringStringDelegate OnChangedOwner;
        public static event VoidDelegate OnChat;
        public static event IntDelegate OnChangedDifficulty;
        public static event RoomStartDelegate OnStartRoom;
        
        /*
         * 채팅
         */
        public static FixedSizedQueue<ChatMessageSchema> ChatLogs { get; } = new FixedSizedQueue<ChatMessageSchema>(50);

        public static bool IsRoomOwner
        {
            get
            {
                if (_roomInfo == null) return false;

                return _roomInfo.owner == _raidServer.Socket.Id;
            }
        }


        private static void RegisterRoomEvents()
        {
            // 새로운 유저 접속 
            _raidServer.Socket.On<string, string, UserInfo>(RaidEvent.SERVER_ROOM_NEW_USER_JOINED, (roomNum, userSocketId, userInfo) =>
            {
                ULogger.Log($"{nameof(RaidEvent.SERVER_ROOM_NEW_USER_JOINED)} {userSocketId}");

                // 방이 있을 때만 처리 
                if (_roomInfo == null) return;
                
                // 현재 방 번호와 일치하는지 체크 
                if (_roomInfo.roomNum != roomNum) return;
                
                // 유저 추가 
                _roomInfo.users.Add(new RoomUserInfo{socketId = userSocketId, data = userInfo});
                    
                // UserData 반영 
                ConvertUserInfoToUserData(userInfo);
                    
                // 이벤트 
                OnJoinedUser?.Invoke(userInfo.inDate, userInfo.nickName);
            });

            // 유저 퇴장
            _raidServer.Socket.On<string, string>(RaidEvent.SERVER_ROOM_USER_LEFT, (roomNum, userSocketId) =>
            {
                ULogger.Log($"{nameof(RaidEvent.SERVER_ROOM_USER_LEFT)} {userSocketId}");

                // 방이 있을 때만 처리 
                if (_roomInfo == null) return;
                
                // 현재 방 번호와 일치하는지 체크 
                if (_roomInfo.roomNum != roomNum) return;
                
                // 유저 찾기
                var user = _roomInfo.users.SingleOrDefault(user => user.socketId == userSocketId);
                if (user == null) return;
                
                // 유저 삭제 
                _roomInfo.users.Remove(user);
                        
                // 이벤트 
                OnLeftUser?.Invoke(user.data.inDate, user.data.nickName);
            });
            
            // 방장이 변경된 경우
            _raidServer.Socket.On<string, string>(RaidEvent.SERVER_ROOM_OWNER_CHANGED, (roomNum, userSocketId) =>
            {
                ULogger.Log($"{nameof(RaidEvent.SERVER_ROOM_OWNER_CHANGED)} {userSocketId}");

                // 방이 있을 때만 처리 
                if (_roomInfo == null) return;
                
                // 현재 방 번호와 일치하는지 체크 
                if (_roomInfo.roomNum != roomNum) return;
                
                // 방장 교체
                _roomInfo.owner = userSocketId;
                    
                // 유저 찾기 
                var user = _roomInfo.users.SingleOrDefault(user => user.socketId == userSocketId);
                if (user == null) return;
                
                // 이벤트  
                OnChangedOwner?.Invoke(user.data.inDate, user.data.nickName);
            });
            
            // 채팅 메시지 온 경우 
            _raidServer.Socket.On<string, string, string>(RaidEvent.SERVER_CHAT, (roomNum, userSocketId, message) =>
            {
                ULogger.Log($"{nameof(RaidEvent.SERVER_CHAT)} {userSocketId} : {message}");

                // 방이 있을 때만 처리 
                if (_roomInfo == null) return;
                
                // 현재 방 번호와 일치하는지 체크 
                if (_roomInfo.roomNum != roomNum) return;
                
                // 채팅 보낸 유저의 정보 찾기 
                var user = _roomInfo.users.SingleOrDefault(user => user.socketId == userSocketId);
                if (user == null) return;
                
                // 차단 유저라면 스킵 
                if (Backend.Chat.IsUserBlocked(user.data.nickName)) return;
                
                // 채팅 큐에 넣기 
                var newChat = new ChatMessageSchema
                {
                    message = message,
                    nickname = user.data.nickName,
                    gamerInDate = user.data.inDate,
                    gamerId = "",   // 레이드 채팅은 로그 안 남기므로, 게이머ID 처리 하지 않음 
                    rank = user.data.rank
                };
                
                ChatLogs.Enqueue(newChat);
                
                // 이벤트
                OnChat?.Invoke();
            });
            
            // 방 난이도 변경 이벤트 (방장빼고 들어오는 이벤트)
            _raidServer.Socket.On<string, int>(RaidEvent.SERVER_CHANGE_DIFFICULTY, (roomNum, difficulty ) =>
            {
                ULogger.Log($"{nameof(RaidEvent.SERVER_CHANGE_DIFFICULTY)} {roomNum} : {difficulty.ToString()}");

                // 방이 있을 때만 처리 
                if (_roomInfo == null) return;
                
                // 현재 방 번호와 일치하는지 체크 
                if (_roomInfo.roomNum != roomNum) return;

                // 반영 
                _roomInfo.difficulty = difficulty;

                // 이벤트
                OnChangedDifficulty?.Invoke(difficulty);
            });
            
            // 방 시작 이벤트  
            _raidServer.Socket.On<RoomStartInfo>(RaidEvent.SERVER_START_ROOM_ALL, (info) =>
            {
                ULogger.Log($"{nameof(RaidEvent.SERVER_START_ROOM_ALL)} - " +
                            $"{nameof(info.roomNum)} : {info.roomNum.ToString()} / " +
                            $"{nameof(info.nextRoomNum)} : {info.nextRoomNum.ToString()} / " +
                            $"{nameof(info.difficulty)} : {info.difficulty.ToString()} / " +
                            $"{nameof(info.hpDownRatioPerSec)} : {info.hpDownRatioPerSec.ToString("0.0")} / " +
                            $"{nameof(info.victory)} : {info.victory.ToString()}");
                
                // 방이 있을 때만 처리 
                if (_roomInfo == null) return;
                
                // 시작된 방과 내 방이 동일하지 않다면, 종료
                if (_roomInfo.roomNum != info.roomNum) return;

                // 정보 세팅
                _roomStartInfo = info;
                
                // 통지  
                OnStartRoom?.Invoke(info.victory, info.difficulty, info.hpDownRatioPerSec);
            });
        }
        
        public static IEnumerator<float> _CreateRoom(Action<bool, string> callback)
        {
            // 연결 체크 
            if (!IsConnected())
            {
                callback?.Invoke(false, "disconnected");
                // 가끔 재연결 안될 때가 있어서 수동 호출
                Connect();
                yield break;
            }
            
            // 내 정보부터 서버에 업데이트 
            var isSuccessUpdate = false;
            yield return Timing.WaitUntilDone(_UpdateMyInfo((success) => { isSuccessUpdate = success;}));
            // 정보 업데이트 실패 시
            if (!isSuccessUpdate)
            {
                // 종료
                callback?.Invoke(false, "updateInfo");
                yield break;
            }
            
            // 실패 가능성 있으므로, 3번까지 시도 
            for (var count = 0; count < 3; count++)
            {
                // 방 생성 결과 이벤트 
                bool isDone = false, isSuccess = false;
                _raidServer.Socket.Once<bool, RoomInfo>(RaidEvent.SERVER_CREATE_ROOM, (success, roomInfo) =>
                {
                    isDone = true; isSuccess = success;

                    // 방 정보 갱신 
                    if (success) _roomInfo = roomInfo;
                    
                    ULogger.Log($"{nameof(RaidEvent.SERVER_CREATE_ROOM)} : {success.ToString()}");
                });
                
                // 방 생성 요청
                InternalCreateRoom();
                
                // 결과 올 때까지 대기 
                while (!isDone) yield return Timing.WaitForOneFrame;
                
                // 실패 시, 다시 시도 
                if (!isSuccess)
                {
                    // 바로 시도하지 않고, 딜레이 후 시도 
                    yield return Timing.WaitForSeconds(1f);
                    continue;
                }

                // 성공 시, 종료
                callback?.Invoke(true, "success");
                // 이전 방 정보 날리기 
                ClearPrevRoomInfo();
                yield break;
            }

            // 몇 번 시도 후에도 여기까지 왔다면 실패한 것 
            callback?.Invoke(false, "failed");
        }

        public static IEnumerator<float> _JoinRoom(string roomNum, Action<bool, string> callback, bool ignoreLocking = false)
        {
            // 연결 체크 
            if (!IsConnected())
            {
                callback?.Invoke(false, "disconnected");
                // 가끔 재연결 안될 때가 있어서 수동 호출
                Connect();
                yield break;
            }
            
            // 내 정보부터 서버에 업데이트 
            var isSuccessUpdate = false;
            yield return Timing.WaitUntilDone(_UpdateMyInfo((success) => { isSuccessUpdate = success;}));
            // 정보 업데이트 실패 시
            if (!isSuccessUpdate)
            {
                // 종료
                callback?.Invoke(false, "updateInfo");
                yield break;
            }
            
            // 실패 가능성 있으므로, 3번까지 시도 
            var msg = "";
            for (var count = 0; count < 3; count++)
            {
                // 이벤트 
                bool isDone = false, isSuccess = false;
                _raidServer.Socket.Once<bool, RoomInfo, string>(RaidEvent.SERVER_JOIN_ROOM, (success, roomInfo, reason) =>
                {
                    isDone = true; 
                    isSuccess = success;
                    msg = reason;
                    
                    // 방 정보 갱신 
                    if (success)
                    {
                        _roomInfo = roomInfo;
                        
                        // 방 유저들의 UserData 세팅 
                        foreach (var roomUser in roomInfo.users)
                        {
                            ConvertUserInfoToUserData(roomUser.data);
                        }
                    }
                    
                    ULogger.Log($"{nameof(RaidEvent.SERVER_JOIN_ROOM)} : {success.ToString()}");
                });
                
                // 요청 
                _raidServer.Socket.Emit(RaidEvent.CLIENT_JOIN_ROOM, new
                {
                    raidRoomNum = roomNum,
                    ignoreLocking
                });
                
                // 결과 올 때까지 대기 
                while (!isDone) yield return Timing.WaitForOneFrame;
                
                // 실패 시, 다시 시도 
                if (!isSuccess)
                {
                    // 바로 시도하지 않고, 딜레이 후 시도 
                    yield return Timing.WaitForSeconds(1f);
                    continue;
                }
                
                // 성공 시, 종료
                callback?.Invoke(true, msg);
                // 이전 방 정보 날리기
                ClearPrevRoomInfo();
                yield break;
            }

            // 몇 번 시도 후에도 여기까지 왔다면 실패한 것 
            callback?.Invoke(false, msg);
        }

        public static IEnumerator<float> _LeaveRoom(Action<bool> callback)
        {
            // 연결 체크 
            if (!IsConnected())
            {
                // 연결이 해지 됐다는 건, 가입된 방도 없다는 것 
                callback?.Invoke(true);
                yield break;
            }
            
            // 이벤트 
            bool isDone = false, isSuccess = false;
            _raidServer.Socket.Once<bool>(RaidEvent.SERVER_LEAVE_ROOM, (success) =>
            {
                isDone = true; isSuccess = success;
                ULogger.Log($"{nameof(RaidEvent.SERVER_LEAVE_ROOM)} : {success.ToString()}");
            });
            
            // 요청 (이건 여러 번 시도하지 않음 )
            _raidServer.Socket.Emit(RaidEvent.CLIENT_LEAVE_ROOM, new
            {
            });
                
            // 결과 올 때까지 대기 
            while (!isDone) yield return Timing.WaitForOneFrame;
            
            // 가지고 있던 방 정보 전부 날리기 
            _roomInfo = null;
            
            // 이전 방 정보 날리기 
            ClearPrevRoomInfo();

            // 종료 
            callback?.Invoke(isSuccess);
        }
        
        public static IEnumerator<float> _StartRoom(bool victory, float hpDownRatioPerSec, Action<bool> callback)
        {
            // 방 정보 유무 체크 
            if (_roomInfo == null)
            {
                callback?.Invoke(false);
                yield break;                
            }
            
            // 방장만 가능 
            if (_roomInfo.owner != _raidServer.Socket.Id)
            {
                callback?.Invoke(false);
                yield break;   
            }
            
            // 연결 체크 
            if (!IsConnected())
            {
                callback?.Invoke(false);
                yield break;
            }
            
            // 연계할 새로운 방 정보 가져오기 플로우 
            string nextRoomNum = null;
            
            for (var count = 0; count < 3; count++)
            {
                // 플레이 후 연계할 새로운 방 생성  
                bool isDone = false, isSuccess = false;
                _raidServer.Socket.Once<bool, RoomInfo>(RaidEvent.SERVER_CREATE_ROOM, (success, roomInfo) =>
                {
                    isDone = true; isSuccess = success;

                    // 방 정보 캐싱
                    if (success) nextRoomNum = roomInfo.roomNum;
                    
                    ULogger.Log($"{nameof(RaidEvent.SERVER_CREATE_ROOM)} : {success.ToString()}");
                });
                
                // 방 생성 요청
                InternalCreateRoom();

                // 결과 올 때까지 대기 
                while (!isDone) yield return Timing.WaitForOneFrame;
                
                // 실패 시, 다시 시도 
                if (!isSuccess)
                {
                    // 바로 시도하지 않고, 딜레이 후 시도 
                    yield return Timing.WaitForSeconds(1f);
                    continue;
                }

                // 성공 시, 루프 탈출
                break;
            }
            
            // 방 생성 실패 시, 실패 반환
            if (string.IsNullOrEmpty(nextRoomNum))
                callback?.Invoke(false);
            
            // 생성한 방 잠금 처리 
            LockRoom(nextRoomNum, true);

            // 방 시작 플로우 
            for (var count = 0; count < 3; count++)
            {
                // 플레이 후 연계할 새로운 방 생성  
                bool isDone = false, isSuccess = false;
                _raidServer.Socket.Once<bool>(RaidEvent.SERVER_START_ROOM, (success) =>
                {
                    isDone = true; isSuccess = success;

                    ULogger.Log($"{nameof(RaidEvent.SERVER_START_ROOM)} : {success.ToString()}");
                });
                
                // 방 시작 요청 
                var startRoomInfo = new RoomStartInfo
                {
                    roomNum = _roomInfo.roomNum,
                    difficulty = UserData.My.Raid.DifficultyCursor,
                    nextRoomNum = nextRoomNum,
                    victory = victory,
                    hpDownRatioPerSec = hpDownRatioPerSec,
                };
                _raidServer.Socket.Emit(RaidEvent.CLIENT_START_ROOM, startRoomInfo);

                // 결과 올 때까지 대기 
                while (!isDone) yield return Timing.WaitForOneFrame;
                
                // 실패 시, 다시 시도 
                if (!isSuccess)
                {
                    // 바로 시도하지 않고, 딜레이 후 시도 
                    yield return Timing.WaitForSeconds(1f);
                    continue;
                }

                // 성공 시, 루프 탈출
                callback?.Invoke(true);
                yield break;
            }

            // 몇 번 시도 후에도 여기까지 왔다면 실패한 것 
            callback?.Invoke(false);
        }

        public static IEnumerator<float> _MoveToNextRoomForOwner(Action<bool> callback)
        {
            // 연결 체크 
            if (!IsConnected())
            {
                callback?.Invoke(false);
                yield break;
            }
            
            // 다음 방 정보 가지고 있어야 함 
            if (_roomStartInfo == null)
            {
                callback?.Invoke(false);
                yield break;
            }
            
            // 방장만 가능 
            if (_roomInfo.owner != _raidServer.Socket.Id)
            {
                callback?.Invoke(false);
                yield break;   
            } 
            
            // 방 잠금 플기 
            LockRoom(_roomStartInfo.nextRoomNum, false);
            
            // 실패 가능성 있으므로, 3번까지 시도 
            for (var count = 0; count < 3; count++)
            {
                // 이벤트 
                bool isDone = false, isSuccess = false;
                _raidServer.Socket.Once<bool, RoomInfo>(RaidEvent.SERVER_REQUEST_ROOM_INFO, (success, roomInfo) =>
                {
                    isDone = true; 
                    isSuccess = success;
                    
                    // 방 정보 갱신 
                    if (success)
                    {
                        _roomInfo = roomInfo;
                        
                        // 방 유저들의 UserData 세팅 
                        foreach (var roomUser in roomInfo.users)
                        {
                            ConvertUserInfoToUserData(roomUser.data);
                        }
                    }
                    
                    ULogger.Log($"{nameof(RaidEvent.SERVER_REQUEST_ROOM_INFO)} : {success.ToString()}");
                });
                
                // 요청 
                _raidServer.Socket.Emit(RaidEvent.CLIENT_REQUEST_ROOM_INFO, new
                {
                    raidRoomNum = _roomStartInfo.nextRoomNum,
                });
                
                // 결과 올 때까지 대기 
                while (!isDone) yield return Timing.WaitForOneFrame;
                
                // 실패 시, 다시 시도 
                if (!isSuccess)
                {
                    // 바로 시도하지 않고, 딜레이 후 시도 
                    yield return Timing.WaitForSeconds(1f);
                    continue;
                }
                
                // 성공 시, 종료
                callback?.Invoke(true);
                // 이전 방 정보 날리기 (채팅 내역은 날리지 않음)
                _roomStartInfo = null;
                yield break;
            }

            // 몇 번 시도 후에도 여기까지 왔다면 실패한 것 
            callback?.Invoke(false);
        }

        public static IEnumerator<float> _MoveToNextRoomForAttendee(Action<bool, string> callback)
        {
            // 다음 방 정보 가지고 있어야 함 
            if (_roomStartInfo == null)
            {
                callback?.Invoke(false, "hasNotNextRoomInfo");
                yield break;
            }
            
            // 일반 참여원만 가능 
            if (_roomInfo.owner == _raidServer.Socket.Id)
            {
                // 기존 방장이 모든 참여 방에서 나가서, 방장이 변경된 경우 발생될 수 있음 
                // 방장이 계속 재참여를 유지한다면, 기존 방들에도 Leave되지 않으므로 방장이 변경되지 않는다 
                callback?.Invoke(false, "notAttendee");
                yield break;   
            } 
            
            // 일반 방 입장처럼 연계 
            yield return Timing.WaitUntilDone(_JoinRoom(_roomStartInfo.nextRoomNum, callback, true));
        }
        
        private static void InternalCreateRoom()
        {
            // 방 번호 랜덤 부여
            var roomNumStr = Random.Range(0, 99999).ToLookUpFormatString("00000");

            // 방 생성 요청 
            _raidServer.Socket.Emit(RaidEvent.CLIENT_CREATE_ROOM, new
            {
                raidRoomNum = roomNumStr,
                difficulty = UserData.My.Raid.DifficultyCursor
            });
        }

        public static void ConvertUserInfoToUserData(UserInfo userInfo)
        {
            // 매우 중요!! 내 데이터는 파싱하지 않는다 
            // 레이드로 받은 보상이 초기화 됨 (스톤)
            if (userInfo.inDate == ServerMyInfo.InDate) return;

            // 서버 레이드의 UserData 구현을 외부에 공개시키지 않기 위해 이 클래스 안에서 처리한다
            var userData = UserData.Other[userInfo.inDate];
            userData.Missiles.FromString(userInfo.missile);
            userData.Bazooka.FromString(userInfo.bazooka);
            userData.Costume.FromString(userInfo.costume);
            userData.NormalRelic.FromString(userInfo.relic);
            userData.DiabloRelic.FromString(userInfo.dRelic);
            userData.InfinityStone.FromString(userInfo.stone);
            userData.Soldiers.FromString(userInfo.soldier);
            userData.Princess.FromString(userInfo.princess);
        }

        public static bool TryGetRoomInfo(out string roomNum, out string ownerInDate, out int difficulty, out IEnumerable<UserInfo> users)
        {
            // 외부에 소켓ID 구현을 보여주지 않기 위함 
            // 필요한 정보를 가공해서 내보낸다 
            roomNum = _roomInfo?.roomNum;
            ownerInDate = _roomInfo?.users.SingleOrDefault(user => user.socketId == _roomInfo.owner)?.data.inDate;
            users = _roomInfo?.users.Select(roomUser => roomUser.data);
            difficulty = _roomInfo?.difficulty ?? 0;
            
            return _roomInfo != null;
        }

        public static void SendChat(string message)
        {
            // 채팅은 실패해도 괜찮으므로, 실패했다고 재시도 처리 하지 않음 
            
            // 연결 체크 
            if (!IsConnected())
            {
                // 연결이 해지 됐다는 건, 가입된 방도 없다는 것
                return;
            }
            
            // 방 정보 없으면 보내지 않음 
            if (_roomInfo == null) return;
            
            // 요청 (이건 여러 번 시도하지 않음 )
            _raidServer.Socket.Volatile().Emit(RaidEvent.CLIENT_CHAT, new
            {
                message,
                _roomInfo.roomNum
            });
        }
        
        public static void ChangeDifficulty(int difficulty)
        {
            // 실패해도 괜찮으므로, 실패했다고 재시도 처리 하지 않음 
            
            // 연결 체크 
            if (!IsConnected()) return;
            
            // 방 정보 없으면 보내지 않음 
            if (_roomInfo == null) return;
            
            // 방장만 가능 
            if (_roomInfo.owner != _raidServer.Socket.Id) return;
            
            // 요청 (이건 여러 번 시도하지 않음 )
            _raidServer.Socket.Volatile().Emit(RaidEvent.CLIENT_CHANGE_DIFFICULTY, new
            {
                _roomInfo.roomNum,
                difficulty
            });
        }
        
        public static void LockRoom(string raidRoomNum, bool isLocked)
        {
            // 실패해도 괜찮으므로, 실패했다고 재시도 처리 하지 않음 
            
            // 연결 체크 
            if (!IsConnected()) return;
            
            // 방 정보 없으면 보내지 않음 
            if (_roomInfo == null) return;
            
            // 방장만 가능 
            if (_roomInfo.owner != _raidServer.Socket.Id) return;
            
            // 요청 (이건 여러 번 시도하지 않음 )
            _raidServer.Socket.Emit(RaidEvent.CLIENT_LOCK_ROOM, new
            {
                raidRoomNum,
                isLocked
            });
        }

        private static void ClearPrevRoomInfo()
        {
            ChatLogs.Clear();
            _roomStartInfo = null;
        }
    }
    
    // ReSharper disable InconsistentNaming
    public static partial class RaidEvent
    {
        public const string CLIENT_CREATE_ROOM = "CLIENT_CREATE_ROOM";
        public const string CLIENT_JOIN_ROOM = "CLIENT_JOIN_ROOM";
        public const string CLIENT_LEAVE_ROOM = "CLIENT_LEAVE_ROOM";
        public const string CLIENT_START_ROOM = "CLIENT_START_ROOM";
        public const string CLIENT_CHAT = "CLIENT_CHAT";
        public const string CLIENT_REQUEST_ROOM_INFO = "CLIENT_REQUEST_ROOM_INFO";
        public const string CLIENT_CHANGE_DIFFICULTY = "CLIENT_CHANGE_DIFFICULTY";
        public const string CLIENT_LOCK_ROOM = "CLIENT_LOCK_ROOM";
        
        public const string SERVER_CREATE_ROOM = "SERVER_CREATE_ROOM";
        public const string SERVER_LEAVE_ROOM = "SERVER_LEAVE_ROOM";
        public const string SERVER_JOIN_ROOM = "SERVER_JOIN_ROOM";
        public const string SERVER_ROOM_OWNER_CHANGED = "SERVER_ROOM_OWNER_CHANGED";
        public const string SERVER_ROOM_NEW_USER_JOINED = "SERVER_ROOM_NEW_USER_JOINED";
        public const string SERVER_ROOM_USER_LEFT = "SERVER_ROOM_USER_LEFT";
        public const string SERVER_START_ROOM = "SERVER_START_ROOM";
        public const string SERVER_START_ROOM_ALL = "SERVER_START_ROOM_ALL";
        public const string SERVER_CHAT = "SERVER_CHAT";
        public const string SERVER_REQUEST_ROOM_INFO = "SERVER_REQUEST_ROOM_INFO";
        public const string SERVER_CHANGE_DIFFICULTY = "SERVER_CHANGE_DIFFICULTY";
    }
}