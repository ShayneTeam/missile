﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using InGame.Data;
using MEC;
using QFSW.QC;
using Random = UnityEngine.Random;
// ReSharper disable ClassNeverInstantiated.Local
// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable InconsistentNaming
// ReSharper disable UnassignedField.Global

namespace Server
{
    public partial class ServerRaid : MonoBehaviourSingletonPersistent<ServerRaid>
    {
        [Command("#정보갱신")]
        private static void TestUpdateMyInfo()
        {
            Timing.RunCoroutine(_UpdateMyInfo(null));
        }

        [Command("#방리스트")]
        private static void TestGetRoomList()
        {
            if (!IsConnected()) return;

            // 서버에 방 리스트를 주기를 요청함.
            _raidServer.Socket.Emit(RaidEvent.CLIENT_REQUEST_ROOM_LIST);
            
            // 룸 정보를 서버로부터 받았을 경우.
            _raidServer.Socket.Once<RoomList>(RaidEvent.SERVER_REQUEST_ROOM_LIST, data =>
            {
                if (data.roomList.Count == 0)
                {
                    ULogger.Log($"{nameof(RaidEvent.SERVER_REQUEST_ROOM_LIST)} 존재하는 레이드 방 없음");
                    return;
                }
                
                var sb = new StringBuilder(100);

                foreach (var info in data.roomList)
                {
                    sb.Clear();
                    info.users.ForEach(user => sb.Append($"{user.socketId} | "));
                    ULogger.Log($"방 : {info.roomNum} - 유저들 : {sb}");
                }
            });
        }

        [Command("#방생성")]
        private static void TestCreateRoom()
        {
            Timing.RunCoroutine(_CreateRoom(null));
        }
        
        [Command("#방나가기")]
        private static void TestLeaveRoom()
        {
            Timing.RunCoroutine(_LeaveRoom(null));
        }

        [Command("#방접속")]
        private static void TestJoinRoom(string roomNum)
        {
            Timing.RunCoroutine(_JoinRoom(roomNum, null));
        }
    }
    
    // ReSharper disable InconsistentNaming
    public static partial class RaidEvent
    {
        public const string CLIENT_REQUEST_ROOM_LIST = "CLIENT_REQUEST_ROOM_LIST";
        public const string SERVER_REQUEST_ROOM_LIST = "SERVER_REQUEST_ROOM_LIST";
    }
}