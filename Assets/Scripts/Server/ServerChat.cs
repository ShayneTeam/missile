﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.IO;
using System.Linq;
using BackEnd;
using BackEnd.Tcp;
using InGame.Data;
using LitJson;
using MEC;
using Newtonsoft.Json;
using UnityEngine;
using Utils;
using JsonWriter = LitJson.JsonWriter;

namespace Server
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "UnassignedField.Global")]
    [SuppressMessage("ReSharper", "FieldCanBeMadeReadOnly.Global")]
    [SuppressMessage("ReSharper", "MemberCanBePrivate.Global")]
    public class ChannelNodeObject
    {
        public ChannelType type;
        public string groupName;
        public string inDate;
        public string participants;
        public int joinedUserCount;
        public int maxUserCount;
        public string host;
        public ushort port;
        public string alias;

        public ChannelNodeObject(ChannelType type, string groupName, string inDate, int joinedUser, int maxUser,
            string host, string port, string alias)
        {
            this.type = type;
            this.groupName = groupName;
            this.inDate = inDate;
            this.joinedUserCount = joinedUser;
            this.maxUserCount = maxUser;
            this.participants = joinedUser + "/" + maxUser;
            this.host = host;
            this.port = ushort.Parse(port);
            this.alias = alias;
        }
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public class ChatMessageSchema
    {
        public string gamerInDate;
        public string gamerId;
        public string nickname;
        public int rank;
        public string message;
        public bool isNotice;
    }
    
    // 데이터와 컨트롤 둘다 하는 싱글턴이라 매니저라는 이름을 달아주는게 맞지만, '매니저'이름을 극혐하므로 최대한 일반화되게 지음 
    // ChatController 라는 네이밍도 이상함. 컨트롤만 하지 않기 때문 
    public class ServerChat : MonoBehaviourSingleton<ServerChat>
    {
        private string _channelGroupName = "General";


        private readonly FixedSizedQueue<ChatMessageSchema> _chatQueue = new FixedSizedQueue<ChatMessageSchema>(50);
        public FixedSizedQueue<ChatMessageSchema> ChatLogs => _chatQueue;
        
        private CoroutineHandle _autoReconnect;
        private static CoroutineHandle _activate;
        private static CoroutineHandle _deactivate;

        public event VoidDelegate OnChat;

        private void Start()
        {
            // 임포터는 등록할 필요없음 
            // ChatMessageSchema 안에 프로퍼티들이 전부 기본타입이기 때문에 등록 안 해줘도 됨 
            JsonMapper.RegisterExporter<ChatMessageSchema>(ExportChatMessageSchema);
        }

        [RuntimeInitializeOnLoadMethod]
        private static void EnterPlayMode()
        {
            UnregisterChatEvents();
        }

        private static void ExportChatMessageSchema(ChatMessageSchema obj, JsonWriter writer)
        {
            // JsonMapper.ToJson 호출 시, string을 \u314B 이런 식으로 변환함 
            // 그래서 뒤끝 비속어 필터링이 작동이 안됨. 로그에서도 채팅 내역과 닉네임 확인이 불가  
            // 커스텀 익스포터를 적용시켜, \u314B 이런 식으로 변환되지 않게 함 
            writer.WriteObjectStart();

            WriteStringNoUnicode(writer.TextWriter, "gamerInDate");
            writer.TextWriter.Write(':');
            WriteStringNoUnicode(writer.TextWriter, obj.gamerInDate);
            writer.TextWriter.Write(',');
            
            WriteStringNoUnicode(writer.TextWriter, "gamerId");
            writer.TextWriter.Write(':');
            WriteStringNoUnicode(writer.TextWriter, obj.gamerId);
            writer.TextWriter.Write(',');

            WriteStringNoUnicode(writer.TextWriter, "nickname");
            writer.TextWriter.Write(':');
            WriteStringNoUnicode(writer.TextWriter, obj.nickname);
            writer.TextWriter.Write(',');

            WriteStringNoUnicode(writer.TextWriter, "rank");
            writer.TextWriter.Write(':');
            writer.TextWriter.Write(obj.rank.ToLookUpString()); // 숫자는 ""로 감싸면 임포트 안됨
            writer.TextWriter.Write(',');

            WriteStringNoUnicode(writer.TextWriter, "message");
            writer.TextWriter.Write(':');
            WriteStringNoUnicode(writer.TextWriter, obj.message);

            writer.WriteObjectEnd();
        }

        private static void WriteStringNoUnicode(TextWriter writer, string str)
        {
            writer.Write('"');

            int n = str.Length;
            for (int i = 0; i < n; i++)
            {
                switch (str[i])
                {
                    case '\n':
                        writer.Write("\\n");
                        continue;

                    case '\r':
                        writer.Write("\\r");
                        continue;

                    case '\t':
                        writer.Write("\\t");
                        continue;

                    case '"':
                    case '\\':
                        writer.Write('\\');
                        writer.Write(str[i]);
                        continue;

                    case '\f':
                        writer.Write("\\f");
                        continue;

                    case '\b':
                        writer.Write("\\b");
                        continue;
                }

                writer.Write(str[i]);
            }

            writer.Write('"');
        }

        public void ReconnectChannel()
        {
            if (Backend.Chat.IsChatConnect(ChannelType.Public))
                Backend.Chat.LeaveChannel(ChannelType.Public);

            /* 재연결 부분은 JoinChannel 에서
                Backend.Chat.OnLeaveChannel = OnLeaveChannel;
                이벤트가 호출됨에 따라, 자동으로 Join 하도록 공통처리 되어 있습니다.
                공식 문서상에 '인터넷이 끊겼을때' 에도 이 이벤트가 호출되기도 한다고 적혀있어서, 이렇게 처리 합니다.
            */
        }

        public void ActivateBehind()
        {
            Timing.KillCoroutines(_activate);
            _activate = Timing.RunCoroutine(ServerChat.Instance._Activate());
        }
        
        public void DeactivateBehind()
        {
            Timing.KillCoroutines(_deactivate);
            _deactivate = Timing.RunCoroutine(ServerChat.Instance._Deactivate());
        }

        public IEnumerator<float> _Activate()
        {
            /*
             * 2021.11.20 ServerChat Desc
             * 1. 채팅 이벤트 등록과 동시에 JoinChat 호출시 간헐적으로 이벤트 무시됨. (그래서 세팅 후 3초뒤에 초기화하도록 변경)
             * 2. SetFilterUse() 호출시 0.5초 프레임 멈춤. (꽤 큰편) 그래서 1회만 시도하도록 함. (원래는 JoinChat 안에서 계속 호출)
             * 3. _autoChatConnectCheck => 약탈로그 등에서 시간이 오래걸리면 (10초 이상), 타이틀에서 채팅 접속 한번 더 시도하는것을 확인함. (중복 채팅 접속 시도) 시도하면서 프리징 발생. 수정 
             */
            
            // 채팅 이벤트 등록 
            RegisterChatEvents();

            // 필터링 사용
            Backend.Chat.SetFilterUse(true);
            Backend.Chat.SetFilterReplacementChar('*');
            
            // 채팅서버 찐접속 여부는 OnJoinChannel 이벤트로 검증되므로, 지역함수로 등록해둠
            var isDone = false;
            void JoinedChannel(JoinChannelEventArgs args) { isDone = true; }
            Backend.Chat.OnJoinChannel += JoinedChannel;

            // 이벤트 등록 후 어느정도 시간을 가지고, JoinChannel 호출하도록 함.
            Timing.CallDelayed(3f, JoinChannel);

            // 시도 후에 JoinChannel 채팅 채널
            _autoReconnect = Timing.CallPeriodically(float.PositiveInfinity, 30f, () =>
            {
                // 채팅에 접속되어있는 상태가 아니라면 자동연결
                if (!Backend.Chat.IsChatConnect(ChannelType.Public))
                {
                    ULogger.Log("채팅 접속함");
                    JoinChannel();
                }
            }, Segment.RealtimeUpdate);

            // 접속 완료될 때까지 대기 
            while (!isDone) yield return Timing.WaitForOneFrame;
            
            // 등록해둔 지역 함수 이벤트 해제 
            Backend.Chat.OnJoinChannel -= JoinedChannel;
        }

        public IEnumerator<float> _Deactivate()
        {
            // 이미 채팅 소켓 서버에 접속조차 안 돼있다면, 재낌  
            if (!Backend.Chat.IsChatConnect(ChannelType.Public)) yield break;

            // 채팅 이벤트 해제
            UnregisterChatEvents();
            
            // 자동 재접속 해제 
            Timing.KillCoroutines(_autoReconnect);
            
            // 채팅서버 찐접속 해제 여부는 OnJoinChannel 이벤트로 검증되므로, 지역함수로 등록해둠
            var isDone = false;
            void LeftChannel(LeaveChannelEventArgs args) { isDone = true; }
            Backend.Chat.OnLeaveChannel += LeftChannel;
            
            // 채널 나가기
            Backend.Chat.LeaveChannel(ChannelType.Public);

            // 채널 나가기 완료될 때까지 대기 
            while (!isDone) yield return Timing.WaitForOneFrame;
            
            // 등록해둔 지역 함수 이벤트 해제 
            Backend.Chat.OnLeaveChannel -= LeftChannel;
        }

        private static void RegisterChatEvents()
        {
            if (Backend.Chat == null) return;
            
            Backend.Chat.OnChat += Instance.OnReceived;
            Backend.Chat.OnRecentChatLogs += Instance.OnRecentChatLogs;
            Backend.Chat.OnNotification += Instance.OnReceivedNotification;
            // 채팅 퇴장_ 자신 혹은 다른 유저가 통신 환경 등이 좋지 않아 연결이 끊어진 후 재접속에 실패한 경우 호출됩니다.
            Backend.Chat.OnLeaveChannel += Instance.OnLeaveChannel;
        }

        private static void UnregisterChatEvents()
        {
            if (Backend.Chat == null) return;
            
            Backend.Chat.OnChat -= Instance.OnReceived;
            Backend.Chat.OnRecentChatLogs -= Instance.OnRecentChatLogs;
            Backend.Chat.OnNotification -= Instance.OnReceivedNotification;
            
            Backend.Chat.OnLeaveChannel -= Instance.OnLeaveChannel;
        }

        private void JoinChannel()
        {
            // 이미 접속돼있는 상태라면 종료
            if (Backend.Chat.IsChatConnect(ChannelType.Public))
                return;

            // 혹시나 재접일 수 있으니, 로그 전부 비워두기 
            while (_chatQueue.TryDequeue(out _)) { }

            // 요청
            var channelsAll = new List<ChannelNodeObject>();

            var groupLang = UserData.My.UserInfo.Language;
            
            // 이 국가의 유저들은 채팅 그룹을 묶음. (뒤끝 채팅그룹 최대 10개 제한) (포르투갈, 이탈리아, 프랑스, 독일)
            if (groupLang == "pt" || groupLang == "it" || groupLang == "fr" || groupLang == "de")
                groupLang = "eu";
            
            _channelGroupName = $@"General_{groupLang.ToUpper()}";
            var bro = Backend.Chat.GetGroupChannelList(_channelGroupName);
            if (!bro.IsSuccess())
            {
                ULogger.Log($@"[서버상태] 채팅방 리스트를 가져오지 못했습니다.");
                return;
            }

            // 정리 
            var rows = bro.GetReturnValuetoJSON()["rows"];
            for (var i = 0; i < rows.Count; i++)
            {
                var data = rows[i];

                var channelNode = new ChannelNodeObject(ChannelType.Public, _channelGroupName, data["inDate"].ToString(),
                    (int)data["joinedUserCount"], (int)data["maxUserCount"],
                    data["serverAddress"].ToString(), data["serverPort"].ToString(), data["alias"].ToString());

                channelsAll.Add(channelNode);

                ULogger.Log("이름 : " + data["alias"].ToString() + "\n inDate : " + data["inDate"].ToString());
            }

            // 들어갈 채널 있는지 체크 
            var channelsCanJoin = channelsAll.Where(t => t.joinedUserCount < t.maxUserCount).ToList();
            if (channelsCanJoin.Count == 0)
            {
                ULogger.Log($@"들어갈 채팅방이 없습니다.");
                return;
            }

            // 채널 접속 
            var channel = channelsCanJoin.OrderByDescending(t => t.joinedUserCount).First();

            ULogger.Log(
                $"입장 : {channel.type} / {channel.host} / {channel.port.ToString()} / {channel.groupName} / {channel.inDate}");

            var joinChannel = Backend.Chat.JoinChannel(channel.type, channel.host, channel.port, channel.groupName,
                channel.inDate, out _);

            if (!joinChannel) JoinChannel();
        }

        private void OnRecentChatLogs(RecentChatLogsEventArgs args)
        {
            ULogger.Log(args.ErrInfo.ToString());
            ULogger.Log("들어간 채널 종류 : " + args.channelType);

            // 역순으로 출력(오래된 내역이 로그의 위로 가도록)
            for (var i = args.LogInfos.Count - 1; i >= 0; i--)
            {
                var log = args.LogInfos[i];

                ULogger.Log("메시지 : " + log.Message);

                Enqueue(log.Message);
            }

            // 통지 
            OnChat?.Invoke();
        }

        private void OnLeaveChannel(LeaveChannelEventArgs args)
        {
            ULogger.Log($"OnLeaveChannel {args.ErrInfo}");
            
            // 퇴장에 성공한 경우
            if (args.ErrInfo == ErrorInfo.Success)
            {
                // 내가 퇴장한 경우 
                if (!args.Session.IsRemote)
                {
                }
                // 다른 유저가 퇴장한 경우
                else
                {
                    ULogger.Log(args.Session.NickName + "님이 퇴장했습니다");
                }
            }
        }

        private void OnReceivedNotification(NotificationEventArgs args)
        {
            // 공지 메시지 처리 
            var splitMessage = args.Message.Split('|');
            if (splitMessage.Length != 2) return;

            var noticeCountry = splitMessage[0];
            if (noticeCountry != ServerMyInfo.CountryCode) return;

            var noticeMessage = splitMessage[1];

            ULogger.Log($"OnChat {args.Subject}");

            var chatSchemaMsg = new ChatMessageSchema
            {
                gamerInDate = null,
                nickname = args.Subject,
                rank = 0,
                message = noticeMessage,
                isNotice = true
            };

            // 챗 로그 큐
            var chatMsgJson = JsonConvert.SerializeObject(chatSchemaMsg);
            Enqueue(chatMsgJson);

            // 통지 
            OnChat?.Invoke();
        }

        private void OnReceived(ChatEventArgs args)
        {
            ULogger.Log($"OnChat {args.ErrInfo}");

            if (args.ErrInfo != ErrorInfo.Success)
                return;

            // 챗 로그 큐
            Enqueue(args.Message);

            // 통지 
            OnChat?.Invoke();
        }

        private void Enqueue(string message)
        {
            var newChat = JsonMapper.ToObject<ChatMessageSchema>(message);
            _chatQueue.Enqueue(newChat);
        }

        public void Send(string message, int rank)
        {
            if (!Backend.Chat.IsChatConnect(ChannelType.Public))
            {
                JoinChannel();
                return;
            }

            var newChat = new ChatMessageSchema
            {
                message = message,
                nickname = ServerMyInfo.NickName,
                gamerInDate = ServerMyInfo.InDate,
                gamerId = ServerMyInfo.GamerId,
                rank = rank
            };

            var json = JsonMapper.ToJson(newChat);
            Backend.Chat.ChatToChannel(ChannelType.Public, json);
        }
    }
}