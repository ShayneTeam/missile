﻿using System;
using System.Collections;
using System.Collections.Generic;
using BackEnd;
using InGame.Data;
using InGame.Global;
using MEC;
using UnityEngine;

namespace Server
{
    public class ServerLogger : MonoBehaviourSingleton<ServerLogger>
    {
	    private static readonly Queue<Param> _queue = new Queue<Param>();
        private static CoroutineHandle _inserting;
        
        public static void Log(string type, string logEvent, double value, double amount)
        {
            var param = GetBaseParam(type, logEvent);
            param.Add(ParamKey.c1Value, Math.Floor(value).ToString("0"));
            param.Add(ParamKey.d1Amount, Math.Floor(amount).ToString("0"));
            
            // 웹로그, amount (total) 은 로그를 보낼때 자동으로 추가되기 때문에 여기서는 포함하지 않음.
            WebLog.Instance.AllLog($"goods_{type}_changed_log", new Dictionary<string, object>{{"type",type},{"reason",logEvent},{"change_count",value}});

            InternalLog(param);
        }
        
        public static void Log(string type, string logEvent, string idx)
        {
            var param = GetBaseParam(type, logEvent);
            param.Add(ParamKey.d2Idx, idx);
            
            InternalLog(param);
        }
        
        public static void Log(string type, string logEvent, bool flag)
        {
            var param = GetBaseParam(type, logEvent);
            param.Add(ParamKey.d3Flag, flag.ToString());
            
            InternalLog(param);
        }
        
        private static Param GetBaseParam(string type, string logEvent)
        {
            var param = new Param
            {
                { ParamKey.a1Event, logEvent },
                { ParamKey.b1Type, type },
            };
            return param;
        }

        private static void InternalLog(Param param)
        {
	        _queue.Enqueue(param);

	        // 
	        if (!_inserting.IsRunning) 
		        _inserting = Timing.RunCoroutine(_Insert(), Segment.RealtimeUpdate);
        }

        private static IEnumerator<float> _Insert()
        {
			var isInserting = false;
			
			// Transaction cancelled, please refer cancellation reasons for specific reasons [TransactionConflict] 
			// 한 프레임에 로그 요청을 여러 번 보낼 경우, AWS DynamoDB에서 위와 같은 트랜잭션 충돌 에러 발생 
			// 그래서 하나씩 보내도록 함 
			while (true)
			{
				if (!isInserting && _queue.Count > 0)
				{
					isInserting = true;

					var param = _queue.Peek();

					// 요청 (로그니깐 일부러 비동기 호출)
					Backend.GameData.Insert(ServerTable.Log, param, (result) =>
					{
						isInserting = false;
						
						if (!result.IsSuccess())
						{
							ULogger.Log($"서버 로그 실패 : {param.GetJson()} - {result.GetMessage()}");
							
							// 실패 시, 재요청
							return;
						}

						// 성공했을 때만 빼기 
						_queue.Dequeue();

						ULogger.Log($"서버 로그 성공 : {param.GetJson()}");
					});
				}
                
                yield return Timing.WaitForOneFrame;
			}
        }
        
        

        /*
         * 편의 함수 
         */
        public static void LogItem(string itemType, string logEvent, int value)
        {
            Log(itemType, logEvent, value, UserData.My.Items[itemType]);
        }
    }

    public static class LogEvent
    {
        public const string Fairy = "Fairy";
        public const string Coupon = "Coupon";
        public const string Post = "Post";
        public const string BoxGacha = "Gacha";
        public const string WorldMap = "WorldMap";
        public const string WormHole = "WormHole";
        public const string Mission = "Mission";
        public const string Tutorial = "Tutorial";
        public const string GuideMission = "GuideMission";
        public const string Purchase = "Purchase";
        public const string ShopGacha = "ShopGacha";
        public const string BattlePass = "BattlePass";
        public const string HunterShop = "HunterShop";
        public const string Attendance = "Attendance";
        public const string Costume = "Costume";
        public const string NormalRelicGacha = "NormalRelicGacha";
        public const string PlunderRematching = "PlunderRematching";
        public const string Plunder = "Plunder";
        public const string LevelUpGasFactory = "LevelUpGasFactory";
        public const string LevelUpStoneFactory = "LevelUpStoneFactory";
        public const string Nickname = "Nickname";
        public const string BazookaEnhance = "BazookaEnhance";
        public const string MissileEnhance = "MissileEnhance";
        public const string SoldierPromote = "SoldierPromote";
        public const string DailyFree = "DailyFree";
        public const string Guide = "Guide";
        public const string DailyGift = "DailyGift";
        public const string ThirtyDaysGift = "ThirtyDaysGift";
        public const string ExchangeKeys = "ExchangeKeys";
        public const string ExchangeCount = "ExchangeCount";
        public const string DailyChip = "DailyChip";
        public const string BuyRaidTicket = "BuyRaidTicket";
        public const string Raid = "Raid";
        public const string GradeUpTreasure = "GradeUpTreasure";
        public const string SellMissile = "SellMissile";
        public const string EarnMissile = "EarnMissile";
        public const string EarnEnergy = "EarnEnergy";
        public const string Battleship = "Battleship";
    }
}