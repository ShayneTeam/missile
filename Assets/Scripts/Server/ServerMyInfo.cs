﻿using System;
using BackEnd;
using BackEnd.GlobalSupport;
using Global;
using Global.Extensions;
using InGame;
using UnityEngine;

namespace Server
{
    public class ServerMyInfo : MonoBehaviourSingletonPersistent<ServerMyInfo>
    {
        [Header("활성화 시, 커스텀 로그인 진행")] public bool customLogin;
        [SerializeField] private string id = ""; // 뒤끝 아이디
        [SerializeField] private string password = ""; // 비밀번호

        public string ID
        {
            get => id;
            set => id = value;
        }

        public string Password
        {
            get => password;
            set => password = value;
        }

        public static string NickName => Backend.UserNickName;

        public static string InDate => Backend.UserInDate;

        public bool IsBlockedUser { get; private set; }

        public static string GamerId { get; private set; }
        public static string CountryCode { get; private set; }


        public void GetMyInfo()
        {
            var bro = Backend.BMember.GetUserInfo();
            if (!bro.IsSuccess())
                return;

            /*
             {
                "row": {
                    "gamerId":"123456a0-7890-11ab-22cd-33f4567fa89" // 유저의 gamer_id
                    "countryCode": "KR", // 국가 코드를 설정하지 않은 경우 null
                    "nickname": "테스트유저1", // 닉네임을 설정하지 않은 경우 null
                    "inDate": "2020-06-23T05:54:29.743Z", // 유저의 inDate
                    "emailForFindPassword": "backend@afidev.com", // 커스텀 계정 id, pw 찾기 용 이메일. 등록하지 않으면 null
                    "subscriptionType": "customSignUp", // 커스텀, 페더레이션 타입
                    "federationId": null // 구글, 애플, 페이스북 페더레이션 ID. 커스텀 계정은 null
                }
            }
             */

            var row = bro.GetReturnValuetoJSON()["row"];

            var federationId = row["federationId"];
            if (federationId == null)
            {
                // 토큰 업데이트
#if UNITY_ANDROID
                if (PlayGamesController.Authenticated)
                {
                    var token = PlayGamesController.GetToken();
                    Backend.BMember.UpdateFederationEmail(token, FederationType.Google, (callback) => { });
                }
#endif
            }
            else
            {
                ULogger.Log($@"페러데이션 ID가 존재합니다. ID : {federationId.StringValue()}");
            }

            // gamerId
            GamerId = row["gamerId"].StringValue();

            // countryCode
            var countryCode = row["countryCode"];

            if (countryCode == null)
            {
                var applicationCountryCode = Localization.GetSystemDefaultLanguage();
                
                // 2021-12-20
                // 영어가 뒤끝에는 US 로 등록해야 하는데, 밸런스 엑셀에는 en으로 되어있음.... 이미 라이브 중이라 작업이 좀 커질 수 있어서, 예외처리..
                if (applicationCountryCode == "en")
                    applicationCountryCode = "us";

                // 참고! GetSystemDefaultLanguage() 는 소문자를 리턴 (UserInfo에 이렇게 들어가야 하기 때문)
                // 하지만, 뒤끝에 등록할때는 ToUpper() 을 해서 대문자로 등록해야 입력 가능함.
                var country = CountryCodeDic.GetCountryName(applicationCountryCode.ToUpper());
                Backend.BMember.UpdateCountryCode(country);
                CountryCode = applicationCountryCode;
            }
            else
            {
                CountryCode = countryCode.ToString();
            }
            
            // 로그용 
            ULogger.Log($"나의 정보 - {row.ToJson()}");
        }

        public void Init(BackendReturnObject loginResult)
        {
            switch (loginResult.GetStatusCode())
            {
                case "403": // 차단당한 계정
                    ULogger.Log($@"차단당한 계정");
                    IsBlockedUser = true;
                    break;
            }
        }
    }
}