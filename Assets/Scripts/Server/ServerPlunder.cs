﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Globalization;
using System.Linq;
using BackEnd;
using Global;
using Global.Extensions;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using LitJson;
using MEC;
using UnityEngine;

namespace Server
{
    public class ServerPlunder : MonoBehaviourSingleton<ServerPlunder>
    {
        // 모델, 컨트롤러를 PlunderDelegator 하나에서 처리
        // 특정 스키마 형태로 취급하는게 아닌, ParamKey 형태로 취급하다보니 일단 여기서 처리시킴 
        private JsonData _logs;

        // 약탈 로그는 월드맵과 약탈에서 필요
        // 해당 UI열 때마다 요청하는 건, 비용 낭비
        // 그렇다고 게임 시작할 때만 호출하기엔 좀 그렇고, (약탈로그는 유저들에 의해 계속 변경되서 갱신해줘야 하기 때문)
        // 필요할 때 예약을 걸어두고, 비동기로 로딩하도록 함 
        private bool _isDirtyToGetLogs;
        private CoroutineHandle _getLogsFlow;
    
    
        public static void RechargePlunderFlag()
        {
            // 약탈 티켓 줌
            var currentFlagCount = UserData.My.Items[RewardType.REWARD_PLUNDER_FLAG];
            var dailyFreeFlagCount = int.Parse(DataboxController.GetConstraintsData(Constraints.PLUNDER_DAILY_TICKET_COUNT));
            if (currentFlagCount >= dailyFreeFlagCount)
                return;

            // 무료 충전 갯수보다 작다면, 강제로 그 갯수만큼 충전시켜줌
            UserData.My.Items[RewardType.REWARD_PLUNDER_FLAG] = dailyFreeFlagCount;

            ULogger.Log($@"{Backend.UserInDate} 님 보상 수령 성공! / 깃발 충전");
            
            // 로그 
            ServerLogger.LogItem(RewardType.REWARD_PLUNDER_FLAG, LogEvent.DailyFree, dailyFreeFlagCount);
        }
    
        public static void RechargeRevengeTicket()
        {
            // 약탈 티켓 줌
            var currentTicketCount = UserData.My.Items[RewardType.REWARD_REVENGE_TICKET];
            var dailyFreeCount = int.Parse(DataboxController.GetConstraintsData(Constraints.PLUNDER_DAILY_REVENGE_COUNT));
            if (currentTicketCount >= dailyFreeCount)
                return;

            // 무료 충전 갯수보다 작다면, 강제로 그 갯수만큼 충전시켜줌
            UserData.My.Items[RewardType.REWARD_REVENGE_TICKET] = dailyFreeCount;

            ULogger.Log($@"{Backend.UserInDate} 님 보상 수령 성공! / 복수 횟수 충전");
        }
        
        public static IEnumerator<float> _MatchMakingPlunder(Action<List<RankUserData>> callback)
        {
            var isDone = false;
            
            // 요청
            const int gap = 50; // 내 랭크 기준. 위 아래 50명 씩 검색   

            SendQueue.Enqueue(Backend.URank.User.GetMyRank, ServerRanking.RankingUUID, gap, result =>
            {
                isDone = true;
                
                result.CommonErrorCheck(() => ULogger.Log("GetGameRanking 호출 실패."));
                
                if (!result.IsSuccess())
                    return;

                // 정리 
                var rankUsers = new List<RankUserData>();
                var flattenRows = result.FlattenRows();
                for (var i = 0; i < flattenRows.Count; i++)
                {
                    var rankUser = RankUserData.ParseRankData(flattenRows[i]);
            
                    // 리스트에 내가 들어가면 안됨 
                    if (rankUser.gamerInDate != ServerMyInfo.InDate)
                        rankUsers.Add(rankUser);
                }

                ULogger.Log(rankUsers.Select(t => t.gamerInDate).MkString(" / "));

                // 내 랭킹
                //var myRankData = rankUsers.First(x => x.gamerInDate == Backend.UserInDate);

                // 랜덤으로 3명 뽑기 
                callback?.Invoke(rankUsers.OrderBy(x => Guid.NewGuid()).Take(3).ToList());
            });

            while (!isDone) yield return Timing.WaitForOneFrame;
        }

        public static IEnumerator<float> _WriteLog(string gamerInDate, double plunderedGas, double revengeGainGas, Action<bool> callback)
        {
            var isDone = false;
            
            var param = new Param
            {
                { ParamKey.attacker, Backend.UserInDate },
                { ParamKey.attackerNickName, Backend.UserNickName },
                { ParamKey.target, gamerInDate },
                { ParamKey.plunderedGas, plunderedGas },
                { ParamKey.revengeGainGas, revengeGainGas },
                { ParamKey.isAppliedGas, false },
                { ParamKey.hasAvenged, false },
                { ParamKey.timestamp, ServerTime.Instance.NowSecUnscaled },
            };

            Backend.GameData.Insert(ServerTable.Plunder_Log, param, (x) =>
            {
                isDone = true;
                
                var result = x.CommonErrorCheck();
                if (result.IsSuccess())
                {
                    ULogger.Log("공격 타겟을 제대로 설정 했습니다.");
                }
                callback?.Invoke(result.IsSuccess());
            });
            
            while (!isDone) yield return Timing.WaitForOneFrame;
        }

        public void GetLogsOnBehind()
        {
            _isDirtyToGetLogs = true;

            if (!_getLogsFlow.IsRunning)
                _getLogsFlow = Timing.RunCoroutine(_GetLogsFlow().CancelWith(Instance), Segment.RealtimeUpdate);
        }

        private IEnumerator<float> _GetLogsFlow()
        {
            while (Instance.enabled)
            {
                // 더티 플래그 켜졌을 떄만
                if (_isDirtyToGetLogs)
                {
                    // 데이터 쿼리 
                    InternalGetLogsAsync(() => { _isDirtyToGetLogs = false; });
                }
            
                // 체크 주기 
                yield return Timing.WaitForSeconds(60f);
            }
        }

        private void InternalGetLogsAsync(Action callback)
        {
            var where = new Where();
            where.Equal(ParamKey.target, Backend.UserInDate); // 내가 공격당한 기록

            // 최근 기록된 10개만 가져옴 
            // 이 갯수보다 더 많은 약탈을 당했다 해도, 복수노트에는 10개만 보여줄꺼기에 그 이상 가져올 필요없음 
            // 아무리 많이 약탈 당해도, 유저의 가스량 방어 때문에 엄청 차감되지도 않아서 데이터를 많이 가져올 필요없음  
            const int limit = 10;   
            Backend.GameData.Get(ServerTable.Plunder_Log, where, limit, result =>
            {
                callback?.Invoke();
            
                if (!result.IsSuccess())
                    return;

                // 약탈당한 기록 없다면, 종료 
                if (result.Rows().Count == 0)
                {
                    ULogger.Log($@"공격당한 기록이 없습니다.");
                    return;
                }

                // 저장 
                _logs = result.FlattenRows();
            });
        }

        public bool HasNewLog()
        {
            // 로그 요청이 아직 안됐거나, 로그가 없는 경우 false
            if (_logs == null || _logs.Count == 0)
                return false;

            // 로그 중 단 한 개라도 처리 되지 않은게 있다면 true
            foreach (JsonData log in _logs)
            {
                var isApplied = log[ParamKey.isAppliedGas].BoolValue();
                if (!isApplied)
                    return true;
            }

            // 모든 로그가 처리 돼있다면 false
            return false;
        }
    
        public double GetSumPlunderedGasByNewLogs()
        {
            // 새롭게 추가된 로그들의 약탈 가스량 총합 
            if (_logs == null || _logs.Count == 0)
                return 0;

            double sum = 0;
            foreach (JsonData log in _logs)
            {
                // 반영 처리 되지 않은 로그의 가스량만 총합 계산 
                var isApplied = log[ParamKey.isAppliedGas].BoolValue();
                if (!isApplied)
                {
                    sum += double.Parse(log[ParamKey.plunderedGas].ToString(), CultureInfo.InvariantCulture);
                }
            }

            return sum;
        }

        public IEnumerator<float> _MarkAppliedNewLogs()
        {
            if (_logs == null || _logs.Count == 0)
                yield break;
        
            // 다시 가스가 깎이지 않도록 Update.
            var isDoneList = new Dictionary<string, bool>();
            foreach (JsonData log in _logs)
            {
                // 이미 처리된 로그는 스킵 
                if (log[ParamKey.isAppliedGas].BoolValue())
                    continue;;
                
                // 수정은 한 개의 row에 대해서만 가능합니다.
                // where 쿼리로 여러 개의 row가 검색된 경우 제일 최근에 삽입된 row가 수정됩니다.
                var param = new Param
                {
                    { ParamKey.isAppliedGas, true },
                };
                // 로그의 인데이트로 업데이트 쳐야 됨
                var inDateLog = log[ParamKey.inDate].ToString();
                isDoneList.Add(inDateLog, false);

                // 비동기 요청
                Backend.GameData.Update(ServerTable.Plunder_Log, inDateLog, param, resultUpdate =>
                {
                    isDoneList[inDateLog] = true;
                    
                    if (!resultUpdate.IsSuccess()) return;
                    
                    // 수동으로 갱신시켜줌 (증요)
                    log[ParamKey.isAppliedGas] = true;
                });
            }

            // 하나라도 안 왔다면, 대기 
            while (isDoneList.Any(isDone => !isDone.Value)) yield return Timing.WaitForOneFrame;
        }

        public IEnumerator<float> _MarkAvengedLog(string logInDate, Action<bool> callback)
        {
            if (_logs == null || _logs.Count == 0)
            {
                callback(true);
                yield break;
            }

            var isDone = false;
            
            var param = new Param
            {
                { ParamKey.hasAvenged, true },
            };
            // 로그의 인데이트로 업데이트 쳐야 됨 
            Backend.GameData.Update(ServerTable.Plunder_Log, logInDate, param, resultUpdate =>
            {
                isDone = true;
                
                callback?.Invoke(resultUpdate.IsSuccess());

                if (!resultUpdate.IsSuccess()) 
                    return;
                
                // 수동으로 갱신시켜줌 (증요)
                foreach (JsonData log in _logs)
                {
                    // 해당 인데이트를 가진 로그 찾기 
                    if (log[ParamKey.inDate].ToString() != logInDate) 
                        continue;
                
                    // 해당 인데이트 수동 변경 
                    log[ParamKey.hasAvenged] = true;
                    break;
                }
            });

            while (!isDone) yield return Timing.WaitForOneFrame;
        }

        public List<PlunderLogSchema> GetAllLogs()
        {
            var allLogs = new List<PlunderLogSchema>();
            
            if (_logs == null || _logs.Count == 0)
                return allLogs;
        
            foreach (JsonData log in _logs)
            {
                var newLog = new PlunderLogSchema
                {
                    AttackerInDate = log[ParamKey.attacker].ToString(),
                    AttackerNickName = log[ParamKey.attackerNickName].ToString(),
                    RevengeGainGas = double.Parse(log[ParamKey.revengeGainGas].ToString(), CultureInfo.InvariantCulture),
                    LogInDate = log[ParamKey.inDate].ToString(),
                    HasAvenged = bool.Parse(log[ParamKey.hasAvenged].ToString()),
                    Timestamp = long.Parse(log[ParamKey.timestamp].ToString()),
                };
                allLogs.Add(newLog);
            }

            return allLogs;
        }

        public bool HasLogToAvenge()
        {
            if (_logs == null || _logs.Count == 0)
                return false;

            // 하나라도 복수 안 헀던 로그가 있다면 true 반환
            return _logs.Cast<JsonData>().Any(log => !log[ParamKey.hasAvenged].BoolValue());
        }
    }


// Plunder_Log 데이터는 Param Key 형태로 사용되어, 인게임에게 서버 구현을 공개하지 않기 위해 스키마를 만듬 
// 딱히 이거를 위해 데이타 계층 구조를 만들긴 귀찮아서, 일단 여기에 둠
// 원래는 다른 곳에 두는게 맞음 
    public class PlunderLogSchema
    {
        public string AttackerInDate;
        public string AttackerNickName;
        public double RevengeGainGas;
        public bool HasAvenged;
        public string LogInDate;
        public long Timestamp; 
    }
}