﻿using System;
using System.Collections.Generic;
using BackEnd;
using InGame.Controller;
using MEC;
using UnityEngine;

namespace Server
{
    public class ServerTime : MonoBehaviourSingleton<ServerTime>
    {
        private DateTime _serverTime;
        private DateTime _serverTimeUnscaled;

        private void Awake()
        {
	        // 서버 실패 시, 클라타임이라도 사용 (최악 고려)
	        _serverTime = DateTime.Now;
	        _serverTimeUnscaled = DateTime.Now;
        }

        public bool GetServerTimeSync()
        {
	        var result = Backend.Utils.GetServerTime ();
	        var time = result.GetReturnValuetoJSON()["utcTime"].ToString();
	        /*
	         * 시간 처리 일관성을 위해 모든 시간을 UTC+0 기준으로 처리
	         * ex) 한국 시간 기준 새벽 5시에 구매했다면, UTC+0 상으론 전날 오후 8시에 구매한 것   
	         */
	        var serverTime = DateTime.Parse(time);
	        
	        // 30일간 선물 테스트 
	        /*serverTime = serverTime.AddDays(1);

	        var remainTimeBeforeToReset = DailyTimeController.GetRemainTimeBeforeToReset(serverTime);
	        serverTime = serverTime.AddSeconds(remainTimeBeforeToReset - 30);*/
			
	        _serverTime = serverTime;
	        _serverTimeUnscaled = serverTime;

	        return result.IsSuccess();
        }

        public DateTime Now => _serverTime;
        public long NowSec => _serverTime.Ticks / System.TimeSpan.TicksPerSecond;

        public DateTime NowUnscaled => _serverTimeUnscaled;
        public long NowSecUnscaled => _serverTimeUnscaled.Ticks / System.TimeSpan.TicksPerSecond;
        
        
        
	    private void Update()
	    {
		    /*
		     * 서버시간 계속 갱신
		     */
		    
		    // 게임배속에 영향 있는 버전
		    _serverTime = _serverTime.AddSeconds(Time.deltaTime);
		    
		    // 게임배속에 영향 없는 버전 
		    _serverTimeUnscaled = _serverTimeUnscaled.AddSeconds(Time.unscaledDeltaTime);
	    }
    }
}