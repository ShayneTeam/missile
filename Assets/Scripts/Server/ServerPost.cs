﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using BackEnd;
using Global.Extensions;
using InGame.Data;
using LitJson;
using Debug = UnityEngine.Debug;

namespace Server
{
    public class ServerPost : MonoBehaviourSingleton<ServerPost>
    {
        private readonly List<ServerPostSchema> _postListFromAdmin = new List<ServerPostSchema>();
        public List<ServerPostSchema> PostList => _postListFromAdmin;

        public static event ServerPostDelegate OnReceivedPost;
        public static event ServerPostListDelegate OnReceivedPostAll;

        public void GetPostList(Action<bool> callback)
        {
            // 밖에 로딩 팝업의 콜백 출력 연계를 위해 SendQueue로 호출
            SendQueue.Enqueue(Backend.Social.Post.GetPostListV2,result =>
            {
                if (!result.IsSuccess())
                {
                    callback?.Invoke(false);
                    return;
                }
            
                var flattenJson = result.GetFlattenJSON();
                var fromAdmin = flattenJson["fromAdmin"];

                _postListFromAdmin.Clear();

                if (fromAdmin.Count == 0)
                    ULogger.Log("더이상 받을 우편이 없어요..");

                for (var i = 0; i < fromAdmin.Count; i++)
                {
                    var postJson = fromAdmin[i];
                    var post = JsonMapper.ToObject<ServerPostSchema>(postJson.ToJson());

                    if (string.IsNullOrWhiteSpace(post.receivedDate))
                    {
                        ULogger.Log($@"받지 않은 신규 메일이 있습니다. 이 메일을 받으려면 #우편수령 {post.inDate} 를 입력해주세요.
                        - 우편제목 : {post.title}
                        - 아이템 ID : {post.item.itemId}
                        - 아이템 수량 : {post.itemCount.ToString()}");
                    }
                
                    _postListFromAdmin.Add(post);
                }
                
                callback?.Invoke(true);
            });
        }

        public bool HasAnyPostToReceive()
        {
            return _postListFromAdmin.Any(post => !post.isReceived);
        }

        public int GetPostCountToReceive()
        {
            return _postListFromAdmin.Count(post => !post.isReceived);
        }
        
        public void Receive(string postInDate, Action<bool> callback)
        {
            var foundPost = _postListFromAdmin.FirstOrDefault(t => t.inDate == postInDate);
            if (foundPost == null)
            {
                ULogger.LogError($"inDate가 {postInDate} 인 우편을 유저는 가지고 있지 않습니다.");
                callback?.Invoke(false);
                return;
            }

            if (foundPost.isReceived)
            {
                ULogger.LogError($"inDate가 {postInDate} 인 우편을 유저는 이미 받았습니다. 받은시간 : {foundPost.receivedDate}");
                callback?.Invoke(false);
                return;
            }

            // 밖에 로딩 팝업의 콜백 출력 연계를 위해 SendQueue로 호출
            SendQueue.Enqueue(Backend.Social.Post.ReceiveAdminPostItemV2, postInDate, receive =>
            {
                if (receive.IsSuccess())
                {
                    ULogger.Log($"mail을 성공적으로 받았습니다. {receive.GetMessage()}");
                    
                    // 우편을 받았다는 체크를 위해, 이 변수를 null이 아닌 값으로 세팅  
                    foundPost.receivedDate = string.Empty;
                    
                    OnReceivedPost?.Invoke(foundPost);
                }
                else
                {
                    ULogger.Log($"mail을 받지 못했습니다. {receive.GetMessage()}");
                }
                callback?.Invoke(receive.IsSuccess());
            });
        }

        public void ReceiveAll(Action<bool> callback)
        {
            if (!HasAnyPostToReceive())
            {
                callback?.Invoke(false);
                return;
            }
            
            // 밖에 로딩 팝업의 콜백 출력 연계를 위해 SendQueue로 호출
            SendQueue.Enqueue(Backend.Social.Post.ReceiveAdminPostAllV2, receiveAll =>
            {
                if (!receiveAll.IsSuccess())
                {
                    callback?.Invoke(false);
                    return;
                }

                // 획득한 모든 우편 아이템 (로컬 상에 들고 있지 않은 새로운 우편의 아이템까지 수령 될 수 있음)
                var receivedPosts = new List<ServerPostSchema>();
                
                var flattenJson = receiveAll.GetFlattenJSON();
                var items = flattenJson["items"];

                if (items.Count == 0)
                {
                    ULogger.Log("받은 아이템이 없어요..");
                    callback?.Invoke(true);
                    return;
                }

                for (var i = 0; i < items.Count; i++)
                {
                    // ReceiveAdminPostAllV2()의 호출 결과엔 우편 정보가 넘어오지 않음
                    // item, itemCount만 들어옴
                    // title, content같은 정보는 들어오지 않음 
                    var postJson = items[i];
                    var post = JsonMapper.ToObject<ServerPostSchema>(postJson.ToJson());

                    ULogger.Log($@"획득한 아이템.                        
                            - 아이템 ID : {post.item.itemId}
                            - 아이템 ID : {post.item.itemId}
                            - 아이템 수량 : {post.itemCount.ToString()}");
                    
                    receivedPosts.Add(post);
                }
                
                // 모든 우편 리스트 받았다고 체크  
                foreach (var post in _postListFromAdmin)
                {
                    post.receivedDate = string.Empty;
                }
                
                // 이벤트 
                OnReceivedPostAll?.Invoke(receivedPosts);
                
                callback?.Invoke(true);
            });
        }
    }
    
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "UnassignedField.Global")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class ServerPostSchema
    {
        public string title; // 제목
        public string content; // 내용
        public string expirationDate; // 만료 일시
        public string inDate; // 우편의 inDate
        public string sentDate; // 보낸 일시 (필요없을듯)
        public string receivedDate; // 받은 일시 (이게 존재한다면, 이미 받은 우편임.)
        public long itemCount; // 수량 (반드시 정수형이어야 함. 실수 타입이면 안 됨. 또한 큰 수를 받을 수 있는 64비트여야 함)
        public ServerItem item;

        public bool isReceived => receivedDate != null;
    }

    [SuppressMessage("ReSharper", "InconsistentNaming")]
    [SuppressMessage("ReSharper", "UnassignedField.Global")]
    [SuppressMessage("ReSharper", "ClassNeverInstantiated.Global")]
    public class ServerItem
    {
        public string itemId; // custom.
        public string itemType; // custom.
        public string itemIdx; // custom.
    }
    
    public delegate void ServerPostDelegate(ServerPostSchema post);
    public delegate void ServerPostListDelegate(List<ServerPostSchema> posts);
}