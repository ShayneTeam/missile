﻿using System.Collections.Generic;
using BestHTTP;
using LitJson;
using UnityEngine;

namespace Server
{
    public class ServerCommonRsp
    {
        public int code { get; set; }
        public string message { get; set; }
        public string originalUrl { get; set; }
        public string utcTimeString { get; set; }
        public long utcTime { get; set; }

        public JsonData data { get; set; }
    }

    public static class ServerExtension
    {
        private static readonly List<string> _log = new List<string>();

        public static ServerCommonRsp ToServerRsp(this HTTPResponse rsp)
        {
            _log.Clear();
            _log.Add($"Response Data - {rsp?.DataAsText}");

            if (rsp?.StatusCode != 200 || string.IsNullOrWhiteSpace(rsp?.DataAsText))
            {
                _log.Insert(0, "REQUEST ERROR!");
                _log.ForEach(x => ULogger.Log(x, Color.red));

                return new ServerCommonRsp
                    { code = rsp?.StatusCode ?? 500, message = rsp?.Message ?? "Server Connect Error" };
            }

            // 서버의 데이터를 JsonConvert
            var response = JsonMapper.ToObject<ServerCommonRsp>(rsp.DataAsText);

            _log.Insert(0, "REQUEST Success!");
            _log.ForEach(x => ULogger.Log(x, Color.cyan));

            // 진짜 중요한 오류임. 무조건 타이틀로 튕굴 것.
            if (response.code == -999)
            {
            }

            return response;
        }
    }
}