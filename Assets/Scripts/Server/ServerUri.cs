﻿using System;
using Global.Extensions;
using UnityEngine;


namespace Server
{
    public class ServerUri : MonoBehaviourSingletonPersistent<ServerUri>
    {
        [SerializeField] private bool localHost;

        private static string _serverUri;
        public static string ServerUrl
        {
            get => ExtensionMethods.IsUnityEditor() && Instance.localHost ? "http://localhost:4002" : _serverUri;
            set => _serverUri = value;
        } 

        // ReSharper disable InconsistentNaming
        public static Uri getServerStatus => new Uri($"{ServerUrl}/api/v1/server/getServerStatus");
        public static Uri getLatestVersion => new Uri($"{ServerUrl}/api/v1/server/getLatestVersion");
        public static Uri getServerTime => new Uri($"{ServerUrl}/api/v1/server/getServerTime");
        
        public static Uri customLogin => new Uri($"{ServerUrl}/api/v1/bmember/customLogin");
        public static Uri authorizeFederation => new Uri($"{ServerUrl}/api/v1/bmember/authorizeFederation");
        public static Uri refreshAccountToken => new Uri($"{ServerUrl}/api/v1/bmember/refreshAccountToken");
        public static Uri loginAccountByToken => new Uri($"{ServerUrl}/api/v1/bmember/loginAccountByToken");
        public static Uri updateCountryCode => new Uri($"{ServerUrl}/api/v1/bmember/updateCountryCode");
        public static Uri updateNickname => new Uri($"{ServerUrl}/api/v1/bmember/updateNickname");
        
        public static Uri getMyData => new Uri($"{ServerUrl}/api/v1/gamedata/getMyData");
        public static Uri getMyAllData => new Uri($"{ServerUrl}/api/v1/gamedata/getMyAllData");
        public static Uri get => new Uri($"{ServerUrl}/api/v1/gamedata/get");
        public static Uri update => new Uri($"{ServerUrl}/api/v1/gamedata/update");
        public static Uri insert => new Uri($"{ServerUrl}/api/v1/gamedata/insert");
    }
}