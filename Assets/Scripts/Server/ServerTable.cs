﻿// ReSharper disable InconsistentNaming
namespace Server
{
    // 뒤끝 상에 등록된 서버 테이블 
    public static class ServerTable
    {
        public const string Costume = "Costume";
        public const string Stage = "Stage";
        public const string Bazooka = "Bazooka";
        public const string Resources = "Resources";
        public const string Mission = "Mission";
        public const string WorldMap = "WorldMap";
        public const string UserInfo = "UserInfo";
        public const string Boxes = "Boxes";
        public const string Missiles = "Missiles";
        public const string Soldiers = "Soldiers";
        public const string Skills = "Skills";
        public const string Buffs = "Buffs";
        public const string Items = "Items";
        public const string NormalRelic = "NormalRelic";
        public const string DiabloRelic = "DiabloRelic";
        public const string WormHole = "WormHole";
        public const string Plunder = "Plunder";
        public const string Plunder_Log = "Plunder_Log";
        public const string Attendance = "Attendance";
        public const string Purchase = "Purchase";
        public const string BattlePass = "BattlePass";
        public const string Ranking = "RankingNew";
        public const string Log = "Log";
        public const string Guide = "Guide";
        public const string InfinityStone = "InfinityStone";
        public const string DailyGift = "DailyGift";
        public const string DailyChip = "DailyChip";
        public const string ThirtyDaysGift = "ThirtyDaysGift";
        public const string Raid = "Raid";
        public const string Tutorial = "Tutorial";
        public const string Princess = "Princess";
        public const string Battleship = "Battleship";
        public const string Event = "Event";
    }

    public static class ParamKey
    {
        public const string attacker = "attacker";
        public const string attackerNickName = "attackerNickName";
        public const string target = "target";
        public const string plunderedGas = "plunderedGas";
        public const string revengeGainGas = "revengeGainGas";
        public const string isAppliedGas = "isAppliedGas";
        public const string hasAvenged = "hasAvenged";
        
        public const string owner_inDate = "owner_inDate";
        public const string resetTimestamp = "resetTimestamp";
        
        public const string inDate = "inDate";
        public const string timestamp = "timestamp";
        
        public const string bestStage = "bestStage";
        public const string damage = "damage";

        // 뒤끝 테이블 컬럼 정렬을 이용하기 위해 알파벳과 숫자를 접미사로 둠
        public const string a1Event = "a1Event";
        public const string b1Type = "b1Type";
        public const string c1Value = "c1Value";
        public const string d1Amount = "d1Amount";
        public const string d2Idx = "d2Idx";
        public const string d3Flag = "d3Flag";
    }
}