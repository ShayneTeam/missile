using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Linq;
using System.Net;
using System.Text;
using BackEnd;
using Firebase;
using Firebase.Analytics;
using Firebase.Crashlytics;
using Global;
using InGame.Data;
using InGame.Global;
using MEC;
using Mono.CSharp;
using Newtonsoft.Json;
using Server;
using UnityEngine;
using Parameter = Firebase.Analytics.Parameter;

public class WebLog : MonoBehaviourSingleton<WebLog>
{
    public Queue<(string type, Dictionary<string, object> args)> _queue =
        new Queue<(string type, Dictionary<string, object> args)>();

    private HashSet<(string tupleKey, object tupleValue)> baseParam =
        new HashSet<(string tupleKey, object tupleValue)>();

    private List<Parameter> _firebaseParam;

    public void StartWebLog()
    {
        Timing.RunCoroutine(SendLog(), Segment.RealtimeUpdate);
    }

    private IEnumerator<float> SendLog()
    {
        while (true)
        {
            while (_queue.Count > 0)
            {
                var firstQueue = _queue.Peek();

                firstQueue.args["timeStamp"] = ServerTime.Instance.NowUnscaled;
            
                // 로그는 안가도 됨. 하지만 이 구문때문에 클라이언트에 지장을 주어서는 안됨.
                try
                {
                    var request = WebRequest.Create($"{FirebaseRemoteConfigController.webLogUrl}/missile_{firstQueue.type}/_doc");
                    request.Method = "POST";
                    request.Credentials = new NetworkCredential("put_only", "c29nb25sYWJzcHV0b25seQ==");
                    request.PreAuthenticate = true;

                    var postData = JsonConvert.SerializeObject(firstQueue.args);
                    var byteArray = Encoding.UTF8.GetBytes(postData);

                    // 커스텀 헤더값 같이 보냄
                    request.ContentType = "application/json;charset=UTF-8";
                    request.ContentLength = byteArray.Length;
                    var dataStream = request.GetRequestStream();

                    // Post Data 같이 전송
                    dataStream.Write(byteArray, 0, byteArray.Length);
                    dataStream.Close();

                    var response = (HttpWebResponse)request.GetResponse();
                    if (response.StatusCode == HttpStatusCode.Created)
                    {
                        _queue.Dequeue();
                    }
                    else
                    {
                        ULogger.LogError($@"웹로그 오류발생!! : {response.StatusCode}");
                    }
                }
                catch (Exception e)
                {
                    // Crashlytics.Log(e.Message);
                }
                yield return Timing.WaitForSeconds(1f);
            }
            yield return Timing.WaitForSeconds(1f);
        }
    }

    /*
     * 유저 기본 파라미터 (파이어베이스, OpenSearch 공통)
     */
    private HashSet<(string tupleKey, object tupleValue)> UserBaseParam()
    {
        baseParam.Clear();
        
        // 절대 수정하지 마세요! 이름이 변경되면 기존에 축적된 로그 다 못쓰게 됩니다!
        baseParam.Add(("userInDate", Backend.UserInDate));
        baseParam.Add(("userNickName", Backend.UserNickName));
        baseParam.Add(("gamerId", ServerMyInfo.GamerId));
        baseParam.Add(("countryCode", ServerMyInfo.CountryCode));
        baseParam.Add(("timeStamp", ServerTime.Instance.NowUnscaled));
        baseParam.Add(("currentGameSpeed", Time.timeScale));
        baseParam.Add(("adRemove", UserData.My.UserInfo.GetFlag(UserFlags.CanAdRemove)));
        baseParam.Add(("currentStage", UserData.My.Stage.currentStage));
        baseParam.Add(("bestStage", UserData.My.Stage.bestStage));
        baseParam.Add(("userRank", ServerRanking.Instance.GetMyRank()?.rank ?? -1));
        baseParam.Add(("userMineral", UserData.My.Resources.mineral));
        baseParam.Add(("userUranium", UserData.My.Resources.uranium));
        baseParam.Add(("userDiabloKey", UserData.My.Resources.diablokey));
        baseParam.Add(("userKillPoint", UserData.My.Resources.killpoint));
        baseParam.Add(("userMileage", UserData.My.Resources.mileage));
        baseParam.Add(("userChipset", UserData.My.Items[RewardType.REWARD_CHIPSET]));
        baseParam.Add(("userWormHoleTicket", UserData.My.Items[RewardType.REWARD_WH_TICKET]));
        baseParam.Add(("userStone", UserData.My.Resources.stone));
        baseParam.Add(("deviceModel", SystemInfo.deviceModel));
        baseParam.Add(("deviceOS", SystemInfo.operatingSystem));
        baseParam.Add(("userAppVersion", Application.version));
        return baseParam;
    }

    /// <summary>
    /// 3th-party 로그 작성
    /// </summary>
    public void ThirdPartyLog(string type, Dictionary<string, object> args = null)
    {
        // 우선 유저 프로퍼티 무조건 갱신시킴.
        var firebaseBaseParam = UserBaseParam();
        foreach (var keyValuePair in firebaseBaseParam)
        {
            var key = keyValuePair.tupleKey;
            var value = keyValuePair.tupleValue;
            
            // Test Framework용 처리 
            if (key == null || value == null) continue;

            var stringValue = value.ToString();

            FirebaseAnalytics.SetUserProperty(key, stringValue);
        }

        // 추가로 넘어온 파라미터가 있으면, 파라미터는 그대로 로그를 남길 때 사용된다.
        _firebaseParam ??= new List<Parameter>();
        _firebaseParam.Clear();

        if (args != null)
        {
            foreach (var keyValuePair in args)
            {
                switch (keyValuePair.Value)
                {
                    case long longValue:
                        _firebaseParam.Add(new Parameter(keyValuePair.Key, longValue));
                        break;
                    case double doubleValue:
                        _firebaseParam.Add(new Parameter(keyValuePair.Key, doubleValue));
                        break;
                    case string stringValue:
                        _firebaseParam.Add(new Parameter(keyValuePair.Key, stringValue));
                        break;
                }
            }
        }

        FirebaseAnalytics.LogEvent(type, _firebaseParam.ToArray());
    }

    /// <summary>
    /// 3th-party OpenSearch (AWS) 로그를 모두 남깁니다.
    /// </summary>
    /// <param name="type"></param>
    /// <param name="args"></param>
    public void AllLog(string type, Dictionary<string, object> args)
    {
        // 3th-party 로그 남김
        ThirdPartyLog(type, args);

        // 웹 로그 남김
        OpenSearchLog(type, args);
    }

    /// <summary>
    /// JSON 값을 받아서, 파이어베이스와 OpenSearch (AWS) 로그를 모두 남깁니다.
    /// </summary>
    /// <param name="type">로그 타입</param>
    /// <param name="json">추가로 넘길 파라미터의 Json</param>
    public void Log(string type, string json, Dictionary<string, object> args)
    {
        try
        {
            var deserializeJsonObject = JsonConvert.DeserializeObject<Dictionary<string, object>>(json);

            for (var i = 0; i < args?.Count; i++)
            {
                var currentDictionary = args.ElementAt(i);
                deserializeJsonObject[currentDictionary.Key] = currentDictionary.Value;
            }
            
            AllLog(type, deserializeJsonObject);
        }
        catch (Exception e)
        {
            // ignore
        }
    }

    /// <summary>
    /// OpenSearch (AWS) 로그를 남깁니다.
    /// </summary>
    /// <param name="type">로그 타입</param>
    /// <param name="args">추가로 넘길 파라미터</param>
    private void OpenSearchLog(string type, Dictionary<string, object> args)
    {
        // 웹로그 남긴다고 되어있어야만 남김.
        if (!FirebaseRemoteConfigController.isOnWeblog ||
            string.IsNullOrWhiteSpace(FirebaseRemoteConfigController.webLogUrl)) return;

        if (args == null) args = new Dictionary<string, object>();

        // openSearch 규칙.
        type = type.ToLower();

        var userBaseParam = UserBaseParam();
        foreach (var userParam in userBaseParam)
        {
            args[userParam.tupleKey] = userParam.tupleValue;
        }
        
        _queue.Enqueue((type, args));
    }
}