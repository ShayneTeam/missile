﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using Firebase.Crashlytics;
using Global;
using Global.Extensions;
#if UNITY_ANDROID
using Google.Play.AppUpdate;
using Google.Play.Common;
#endif
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using InGame.Global;
using IronSource.Scripts;
using LunarConsolePlugin;
using MiscUtil.Collections.Extensions;
using SA.Android.GMS.Auth;
using SA.Android.GMS.Drive;
using SA.CrossPlatform.App;
using SA.CrossPlatform.GameServices;
using SA.CrossPlatform.UI;
using TapjoyUnity;
using UnityEngine;
using UnityEngine.Networking;
using UnityEngine.UDP;

public class LunarConsoleManager : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        LunarConsole.RegisterAction("게스트 제거", () =>
        {
            Backend.BMember.DeleteGuestInfo();
            ULogger.Log("게스트로그인 제거 완료");
        });
        
        LunarConsole.RegisterAction("UDP 결제 테스트", () =>
        {
            // 테이블에 작성된 모든 상품 ID 정리 
            var allProduct = new List<string> { "dev.purchase_test.1" };    // 테스트용 productId
            var allIdxes = DataboxController.GetAllIdxes(Table.Shop, Sheet.iap);

            // 플랫폼에 따라 상품 패키지 초기화.
            var columnKey = ExtensionMethods.IsIOS() ? Column.ios : Column.google;
            foreach (var idx in allIdxes)
            {
                var google = DataboxController.GetDataString(Table.Shop, Sheet.iap, idx, columnKey);
                allProduct.Add(google);
            }

            StoreService.Purchase(allProduct.OrderBy(t => Guid.NewGuid()).First(), "payload", new PurchaseListener());
        });
        
        LunarConsole.RegisterAction("구글 1", () =>
        {
            var builder = new AN_GoogleSignInOptions.Builder(AN_GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
            builder.RequestId();
            builder.RequestEmail();
            builder.RequestProfile();
            builder.RequestScope(AN_Drive.SCOPE_APPFOLDER);
            var gso = builder.Build();
            Debug.Log("Let's try Silent SignIn first");
            var client = AN_GoogleSignIn.GetClient(gso);
            client.SilentSignIn(result => {
                if (result.IsSucceeded) {
                    Debug.Log("SilentSignIn Succeeded");
                } else {
                    // Player will need to sign-in explicitly using via UI
                    Debug.Log($"SilentSignIn Failed with code: {result.Error.Code}");
                    Debug.Log("Starting the default Sign in flow");
                    //Starting the interactive sign-in
                    client.SignIn(signInResult => {
                        Debug.Log($"Sign In StatusCode: {signInResult.StatusCode}");
                        if (signInResult.IsSucceeded) {
                            Debug.Log("SignIn Succeeded");
                        } else {
                            Debug.LogError($"SignIn failed: {signInResult.Error.FullMessage}");
                        }
                    });
                }
            });
        });
        
        LunarConsole.RegisterAction("구글 2", () =>
        {
            var builder = new AN_GoogleSignInOptions.Builder(AN_GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
            builder.RequestId();
            builder.RequestEmail();
            builder.RequestScope(AN_Drive.SCOPE_APPFOLDER);
            var gso = builder.Build();
            Debug.Log("Let's try Silent SignIn first");
            var client = AN_GoogleSignIn.GetClient(gso);
            client.SilentSignIn(result => {
                if (result.IsSucceeded) {
                    Debug.Log("SilentSignIn Succeeded");
                } else {
                    // Player will need to sign-in explicitly using via UI
                    Debug.Log($"SilentSignIn Failed with code: {result.Error.Code}");
                    Debug.Log("Starting the default Sign in flow");
                    //Starting the interactive sign-in
                    client.SignIn(signInResult => {
                        Debug.Log($"Sign In StatusCode: {signInResult.StatusCode}");
                        if (signInResult.IsSucceeded) {
                            Debug.Log("SignIn Succeeded");
                        } else {
                            Debug.LogError($"SignIn failed: {signInResult.Error.FullMessage}");
                        }
                    });
                }
            });
        });
        
        LunarConsole.RegisterAction("구글 3", () =>
        {
            var builder = new AN_GoogleSignInOptions.Builder(AN_GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
            builder.RequestId();
            builder.RequestEmail();
            builder.RequestProfile();
            var gso = builder.Build();
            Debug.Log("Let's try Silent SignIn first");
            var client = AN_GoogleSignIn.GetClient(gso);
            client.SilentSignIn(result => {
                if (result.IsSucceeded) {
                    Debug.Log($"SilentSignIn Succeeded / Token : {result.Account.GetIdToken()}");
                } else {
                    // Player will need to sign-in explicitly using via UI
                    Debug.Log($"SilentSignIn Failed with code: {result.Error.Code}");
                    Debug.Log("Starting the default Sign in flow");
                    //Starting the interactive sign-in
                    client.SignIn(signInResult => {
                        Debug.Log($"Sign In StatusCode: {signInResult.StatusCode}");
                        if (signInResult.IsSucceeded) {
                            Debug.Log("SignIn Succeeded");
                        } else {
                            Debug.LogError($"SignIn failed: {signInResult.Error.FullMessage}");
                        }
                    });
                }
            });
        });
        
        LunarConsole.RegisterAction("구글 4", () =>
        {
            var builder = new AN_GoogleSignInOptions.Builder(AN_GoogleSignInOptions.DEFAULT_GAMES_SIGN_IN);
            builder.RequestId();
            builder.RequestEmail();
            var gso = builder.Build();
            Debug.Log("Let's try Silent SignIn first");
            var client = AN_GoogleSignIn.GetClient(gso);
            client.SilentSignIn(result => {
                if (result.IsSucceeded) {
                    Debug.Log($"SilentSignIn Succeeded / Token : {result.Account.GetIdToken()}");
                } else {
                    // Player will need to sign-in explicitly using via UI
                    Debug.Log($"SilentSignIn Failed with code: {result.Error.Code}");
                    Debug.Log("Starting the default Sign in flow");
                    //Starting the interactive sign-in
                    client.SignIn(signInResult => {
                        Debug.Log($"Sign In StatusCode: {signInResult.StatusCode}");
                        if (signInResult.IsSucceeded) {
                            Debug.Log("SignIn Succeeded");
                        } else {
                            Debug.LogError($"SignIn failed: {signInResult.Error.FullMessage}");
                        }
                    });
                }
            });
        });

        LunarConsole.RegisterAction("스크린 락", () =>
        {
            UM_Preloader.LockScreen("잠시만 기다려주세요..");
        });
        
        LunarConsole.RegisterAction("스크린 락 해제", () =>
        {
            UM_Preloader.UnlockScreen();
        });
        
        LunarConsole.RegisterAction("국가 확인", () =>
        {
            var currentLocale = UM_Locale.GetCurrentLocale();
            Debug.Log("currentLocale.CountryCode: " + currentLocale.CountryCode);
            Debug.Log("currentLocale.CurrencyCode: " + currentLocale.CurrencyCode);
            Debug.Log("currentLocale.LanguageCode: " + currentLocale.LanguageCode);
            Debug.Log("currentLocale.CurrencySymbol: " + currentLocale.CurrencySymbol);
        });

        LunarConsole.RegisterAction("네이티브 팝업 열기", () =>
        {
            string title = "Congrats";
            string message = "Your account has been verified";
            var builder = new UM_NativeDialogBuilder(title, message);
            builder.SetPositiveButton("Okay", () => {
                Debug.Log("Okay button pressed");
            });
            builder.SetNegativeButton("No", () =>
            {
                Debug.Log("Yes button pressed");
            });
            builder.SetNeutralButton("Later", () => {
                Debug.Log("Later button pressed");
            });

            var dialog = builder.Build();
            dialog.Show();
        });

        LunarConsole.RegisterAction("리더보드 열기", () =>
        {
            var client = UM_GameService.LeaderboardsClient;
            client.ShowUI((result) =>
            {
                if (result.IsSucceeded)
                {
                    ULogger.Log("User closed Leaderboards native view");
                }
                else
                {
                    ULogger.LogError($"Failed to start Leaderboards native view: {result.Error.FullMessage}");
                }
            });
        });

        LunarConsole.RegisterAction("업데이트 체크", () => { StartCoroutine(CheckForUpdate()); });

        LunarConsole.RegisterAction("리뷰요청", () =>
        {
#if UNITY_ANDROID
            PlayGamesController.Instance.RequestReview();
#endif
        });
        /*
        LunarConsole.RegisterAction("오류발생 (Firebase Crashlytics Test)",
            () => { Crashlytics.LogException(new ApplicationException("고의적으로 오류를 발생시켰습니다.")); });
        
        LunarConsole.RegisterAction("오류발생 (Firebase Crashlytics Test)",
            () =>
            {
                this.AsCoroutine().YieldWaitForSecondsRealtime(2f).Action(() =>
                {
                    Crashlytics.LogException(new ApplicationException("고의적으로 2초 후 오류를 발생시켰습니다."));
                }).Start(this);
            });*/

        LunarConsole.RegisterAction("IronSource 광고아이디 확인", () =>
        {
            ULogger.Log("START IronSource 광고 ID =================");
            ULogger.Log(IronSourceController.Agent.getAdvertiserId());
            ULogger.Log(@"END IronSource 광고 ID =================");
        });

        LunarConsole.RegisterAction("IronSource 미디에이션 광고 소스 확인", () =>
        {
            // ironsource 미디에이션 연동 테스트, 넣으면 미디에이션 초기화 성공했는지 알려줌. Debug.Log로 찍히는게 아니고, ADB Logcat 연결해야만 나옴.
            IronSourceController.Agent.validateIntegration();
        });

        LunarConsole.RegisterAction("Google Hash", () => { ULogger.Log(Backend.Utils.GetGoogleHash()); });

#if UNITY_ANDROID
        LunarConsole.RegisterAction("구글 로그아웃", () => { ((PlayGamesPlatform)Social.Active).SignOut(); });

        LunarConsole.RegisterAction("구글 토큰체크",
            () => { ULogger.Log($"구글 토큰 : {((PlayGamesLocalUser)Social.localUser).GetIdToken()}"); });

        LunarConsole.RegisterAction("구글 초기화", () =>
        {
            // GPGS 플러그인 설정
            PlayGamesClientConfiguration config = new PlayGamesClientConfiguration
                    .Builder()
                .RequestIdToken()
                .RequestServerAuthCode(false)
                // .RequestEmail() // 이메일 권한을 얻고 싶지 않다면 해당 줄(RequestEmail)을 지워주세요.
                .Build();
            //커스텀 된 정보로 GPGS 초기화
            PlayGamesPlatform.InitializeInstance(config);
            PlayGamesPlatform.DebugLogEnabled = true; // 디버그 로그를 보고 싶지 않다면 false로 바꿔주세요.
            //GPGS 시작.
            PlayGamesPlatform.Activate();
        });

        LunarConsole.RegisterAction("구글 로그인", () =>
        {
            Social.localUser.Authenticate((success) =>
            {
                var resultCode = PlayGamesPlatform.Instance.GetServerAuthCode();
                ULogger.Log("구글 로그인 결과 (코드) : " + resultCode);
                if (success)
                {
                    // if we signed in successfully, load data from cloud
                    ULogger.Log("구글 Login successful!");
                }
                else
                {
                    // no need to show error message (error messages are shown automatically by plugin)
                    ULogger.LogWarning("구글 로그인 실패 /  to sign in with Google Play Games.");
                }
            });
        });

        LunarConsole.RegisterAction("인앱 결제 테스트_TEST PRODUCT", () =>
        {
            UnityPurchaser.Purchase("dev.purchase_test.1", (b, s) =>
            {
                ULogger.Log(b.ToString());
                ULogger.Log(s);
            });
        });

        LunarConsole.RegisterAction("뒤끝 문의 창 열기", () =>
        {
            bool isQuestionViewCanOpen = false;
#if UNITY_ANDROID
            isQuestionViewCanOpen = BackEnd.Support.Android.Question.IsSdkVersionPossible();
#elif UNITY_IOS
  isQuestionViewCanOpen = BackEnd.Support.iOS.Question.IsSdkVersionPossible();
#endif

            if (isQuestionViewCanOpen)
            {
                // 1대1문의창 생성 로직
            }
            else
            {
                //MailMessage 기능등을 이용하여 이메일로 문의가 가도록 구현
            }

            BackendReturnObject bro = Backend.Question.GetQuestionAuthorize();
            string questionAuthorize = bro.GetReturnValuetoJSON()["authorize"].ToString();

            bool isQuestionViewOpen = false;

// margin(빈 여백)이 10인 1대1 문의 창을 생성합니다.
#if UNITY_ANDROID
            isQuestionViewOpen =
                BackEnd.Support.Android.Question.OpenQuestionView(questionAuthorize, Backend.UserInDate, 0, 0, 0, 0);
#elif UNITY_IOS
  isQuestionViewOpen = BackEnd.Support.iOS.Question.OpenQuestionView(questionAuthorize, myIndate, 10, 10, 10, 10);
#endif

            if (isQuestionViewOpen)
            {
                ULogger.Log("1대1 문의 창이 생성되었습니다");
            }
        });

        LunarConsole.RegisterAction("네이티브 문의 창 열기", () =>
        {
            string mailto = "game.services.1024@gmail.com";
            string subject =
                EscapeURL($@"미사일 RPG 2 문의하기");
            var bodys = String.Empty;
            bodys += "\n" + "DeviceName : " + SystemInfo.deviceModel;
            bodys += "\n" + "UserName : " + Backend.UserNickName;
            bodys += "\n" + "UserOs : " + SystemInfo.operatingSystem;
            bodys += "\n" + "Comment : ";
            string body = EscapeURL(bodys);
            Application.OpenURL("mailto:" + mailto + "?subject=" + subject + "&body=" + body);
        });

        LunarConsole.RegisterAction("뒤끝 푸쉬 등록", () =>
        {
            switch (Application.platform)
            {
                case RuntimePlatform.Android:
                    Backend.Android.PutDeviceToken(Backend.Android.GetDeviceToken(), (callback) =>
                    {
                        var result = callback.CommonErrorCheck();
                        if (result.IsSuccess())
                        {
                            ULogger.Log("안드로이드 푸쉬 등록에 성공했습니다.");
                        }
                        else
                        {
                            ULogger.Log(result.GetMessage());
                        }
                    });
                    break;
                case RuntimePlatform.IPhonePlayer:

                    var isDevelopmentType = ExtensionMethods.IsDev() ? isDevelopment.iosDev : isDevelopment.iosProd;
                
                    Backend.iOS.PutDeviceToken(isDevelopmentType, (callback) =>
                    {
                        var result = callback.CommonErrorCheck();
                        if (result.IsSuccess())
                        {
                            ULogger.Log("iOS 푸쉬 등록에 성공했습니다.");
                        }
                        else
                        {
                            ULogger.Log(result.GetMessage());
                        }
                    });
                    break;
                default:
                    ULogger.Log($"미지원 플랫폼 : {Application.platform}");
                    break;
            }
        });
#endif
    }

    private string EscapeURL(string url)
    {
        return UnityWebRequest.EscapeURL(url).Replace("+", "%20");
    }


    IEnumerator CheckForUpdate()
    {
#if UNITY_ANDROID
        ULogger.Log("인앱 업데이트 체크 시작..");
        AppUpdateManager appUpdateManager = new AppUpdateManager();

        PlayAsyncOperation<AppUpdateInfo, AppUpdateErrorCode> appUpdateInfoOperation =
            appUpdateManager.GetAppUpdateInfo();

        // Wait until the asynchronous operation completes.
        yield return appUpdateInfoOperation;

        ULogger.Log("인앱 업데이트 체크 응답 옴..");

        if (appUpdateInfoOperation.IsSuccessful)
        {
            ULogger.Log("인앱 업데이트 체크 성공함.");

            var appUpdateInfoResult = appUpdateInfoOperation.GetResult();

            ULogger.Log(
                $@"업데이트 존재! : {appUpdateInfoResult.AvailableVersionCode} / 업데이트 나온 날짜 : {appUpdateInfoResult.ClientVersionStalenessDays}");

            // Creates an AppUpdateOptions defining an immediate in-app
// update flow and its parameters.
            var appUpdateOptions = AppUpdateOptions.ImmediateAppUpdateOptions();

            ULogger.Log(
                $@"업데이트 옵션 가져옴! : {appUpdateInfoResult.UpdatePriority} / 업데이트 나온 날짜 : {appUpdateInfoResult.ClientVersionStalenessDays}");


            var startUpdateRequest = appUpdateManager.StartUpdate(
                // The result returned by PlayAsyncOperation.GetResult().
                appUpdateInfoResult,
                // The AppUpdateOptions created defining the requested in-app update
                // and its parameters.
                appUpdateOptions);
            yield return startUpdateRequest;

            ULogger.Log("업데이트 다 함");

            // Check AppUpdateInfo's UpdateAvailability, UpdatePriority,
            // IsUpdateTypeAllowed(), etc. and decide whether to ask the user
            // to start an in-app update.
        }
        else
        {
            // Log appUpdateInfoOperation.Error.
            ULogger.Log($"인앱 업데이트 실패 - {appUpdateInfoOperation.Error.ToString()}");
        }
#endif
        yield break;
    }
}