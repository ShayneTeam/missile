﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoActive : MonoBehaviour
{
    [SerializeField]
    bool active = true;


    private void Awake()
    {
        gameObject.SetActive(active);
    }

    
}
