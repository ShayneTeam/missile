﻿using System.Collections.Generic;
using Lean.Pool;
using MEC;
using UnityEngine;

namespace Utils
{
    public class AutoDespawn : MonoBehaviour
    {
        [SerializeField] private float delaySec;
        
        private void OnEnable()
        {
            /*
             * 어드레서블 때문인가?
             * LeanPool.Spawn 하자마자 OnEnable에서 LeanPool.Despawn(delay)를 호출하니
             * 등록된게 없다고 바로 Destroy 호출함
             * 그래서 한 프레임 기다린 후, 동작시키니 잘됨 
             */
            // CallDelayed는 this 캡쳐링에 추가 GC 할당 발생됨. 그리고 안에서 RunCoroutine + CancelWith가 실행 됨
            // 직접 호출해야 this 캡쳐링이라도 줄일 수 있음  
            Timing.RunCoroutine(_Despawn().CancelWith(gameObject));
        }

        private IEnumerator<float> _Despawn()
        {
            yield return Timing.WaitForSeconds(delaySec);
            
            LeanPool.Despawn(gameObject);
        }
    }
}