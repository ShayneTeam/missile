﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using BackEnd;
using InGame;
using LitJson;
using UI.Popup;
using UnityEngine;

namespace Global.Extensions
{
    public static class ExtensionMethods
    {
        public static string MkString(this IEnumerable<string> iQueryable, string seperator) =>
            string.Join(seperator, iQueryable);

        public static string
            MkString(this IEnumerable<string> iQueryable, string start, string seperator, string end) =>
            start + string.Join(seperator, iQueryable) + end;

        public static BackendReturnObject CommonErrorCheck(this BackendReturnObject bro, Action errorAction = null)
        {
            var errorCode = bro.GetStatusCode();
            switch (errorCode)
            {
                case "401":
                    ULogger.Log("게임이 점검 상태입니다.");
                    break;
                case "403": // 여기서 체크하지 마세요- 403 사유가 벤처리 이외에도 엄청 많습니다..
                    // OkPopup.Alert(Localization.GetText("system_010"), Localization.GetText("system_011"), () => { Application.Quit(); });
                    // Time.timeScale = 0;
                    break;
                default:
                    break;
            }

            if (!bro.IsSuccess())
            {
                ULogger.Log($@"잘못된 요청입니다. / {bro.GetMessage()} / {bro.GetErrorCode()} / {bro.GetStatusCode()}");
                errorAction?.Invoke();
            }

            return bro;
        }

        public static string DStringValue(this JsonData data)
        {
            var value = data["S"];
            if (!value.IsString)
                throw new InvalidDataException(
                    $"포맷 오류. {value.ToString()} 는 String 이 아닙니다. / 타입 : {value.GetJsonType()}");

            if (string.IsNullOrWhiteSpace(value.ToString()))
                return string.Empty;

            return value.ToString();
        }

        public static int DIntValue(this JsonData data)
        {
            var value = data["N"];

            if (int.TryParse(value.ToString(), out var result))
            {
                return result;
            }

            throw new InvalidDataException($"포맷 오류. {data.ToString()} 는 Int 가 아닙니다. / 타입 : {data.GetJsonType()}");
        }

        public static long DLongValue(this JsonData data)
        {
            var value = data["N"];

            if (long.TryParse(value.ToString(), out var result))
            {
                return result;
            }

            throw new InvalidDataException($"포맷 오류. {data.ToString()} 는 long 이 아닙니다. / 타입 : {data.GetJsonType()}");
        }

        public static JsonData BackEndJsonData(this JsonData data)
        {
            var ignoreKeyList = new List<string>
            {
                "inDate",
                "owner_inDate",
                "updatedAt",
                "client_date",
            };

            for (var j = data.Keys.Count - 1; j >= 0; j--)
            {
                var curKey = data.Keys.ElementAt(j);
                if (ignoreKeyList.Contains(curKey))
                    data.Remove(curKey);
            }

            return data;
        }

        public static Dictionary<string, JsonData> BackEndJsonToDict(this JsonData data)
        {
            var ignoreKeyList = new List<string>
            {
                "inDate",
                "owner_inDate",
                "updatedAt",
            };

            var keyValuePair = new Dictionary<string, JsonData>();

            var keyList = data.Keys;
            for (var i = 0; i < data.Count; i++)
            {
                var keyName = keyList.ElementAt(i);
                if (ignoreKeyList.Contains(keyName)) continue;
                keyValuePair.Add(keyName, data[i]);
            }

            return keyValuePair;
        }


        public static string StringValue(this JsonData data)
        {
            if (!data.IsString)
                throw new InvalidDataException(
                    $"포맷 오류. {data.ToString()} 는 String 이 아닙니다. / 타입 : {data.GetJsonType()}");

            if (string.IsNullOrWhiteSpace(data.ToString()))
                throw new InvalidDataException($"포맷 오류. {data.ToString()} 는 Nullable String 이 아닙니다.");

            return data.ToString();
        }

        public static string NullableStringValue(this JsonData data)
        {
            if (!data.IsString)
                throw new InvalidDataException(
                    $"포맷 오류. {data.ToString()} 는 String 이 아닙니다. / 타입 : {data.GetJsonType()}");

            return data.ToString();
        }

        public static DateTime DateTimeValue(this JsonData data)
        {
            if (DateTime.TryParse(data.StringValue(), out var result))
            {
                return result;
            }

            throw new InvalidDataException($"포맷 오류. {data.ToString()} 는 DateTime 이 아닙니다. / 타입 : {data.GetJsonType()}");
        }

        public static bool BoolValue(this JsonData data)
        {
            if (!data.IsBoolean)
                throw new InvalidDataException($"포맷 오류. {data.ToString()} 는 Bool 이 아닙니다. / 타입 : {data.GetJsonType()}");

            return (bool)data;
        }

        public static int PercentValue(this JsonData data)
        {
            if (!data.ContainsKey("%"))
                throw new InvalidDataException(
                    $"포맷 오류. {data.ToString()} 은 % 가 붙어이지 않습니다. / 타입 : {data.GetJsonType()}");

            return Convert.ToInt32(data.ToString().Replace("%", string.Empty));
        }

        public static int IntValue(this JsonData data)
        {
            if (!data.IsInt)
                throw new InvalidDataException($"포맷 오류. {data.ToString()} 는 Int 가 아닙니다. / 타입 : {data.GetJsonType()}");

            return (int)data;
        }

        public static long LongValue(this JsonData data)
        {
            if (!data.IsLong)
                throw new InvalidDataException($"포맷 오류. {data.ToString()} 는 Long 이 아닙니다. / 타입 : {data.GetJsonType()}");

            return (long)data;
        }

        public static double DoubleValue(this JsonData data)
        {
            if (!data.IsDouble)
                throw new InvalidDataException(
                    $"포맷 오류. {data.ToString()} 는 Double 이 아닙니다. / 타입 : {data.GetJsonType()}");

            return (double)data;
        }

        public static bool IsUnityEditor()
        {
            var editor = false;

#if UNITY_EDITOR
            editor = true;
#endif
            return editor;
        }

        public static bool IsAndroid()
        {
            var isAndroid = false;

#if UNITY_ANDROID
            isAndroid = true;
#endif
            return isAndroid;
        }

        public static bool IsIOS()
        {
            var isIOS = false;

#if UNITY_IOS
            isIOS = true;
#endif
            return isIOS;
        }
        
        public static bool IsDev()
        {
            var isDev = false;

#if DEV
            isDev = true;
#endif
            return isDev;
        }

        public static bool IsDevelopmentBuild()
        {
            var isDevelopmentBuild = false;

#if DEVELOPMENT_BUILD
            isDevelopmentBuild = true;
#endif

            if (isDevelopmentBuild)
            {
                ULogger.Log($@"[주의] 개발빌드를 체크하는 구문이 호출되었습니다.");
            }
            
            return isDevelopmentBuild;
        }
    }
}