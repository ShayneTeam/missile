﻿using UnityEditor;
using UnityEngine;
using UnityEditor.SceneManagement;
 
[InitializeOnLoad]
public class AutoStartScene
{
    // Static constructor that gets called when unity fires up.
    static AutoStartScene()
    {
        StartSceneWindow.SetDefaultIfEmpty();
    }
}