﻿using UnityEditor;
using UnityEditor.SceneManagement;
using UnityEngine;

public class StartSceneWindow : EditorWindow
{
    public const string  DEFAULT_START_SCENE_PATH = "Assets/Scenes/Title.unity";
    
    private void OnGUI()
    {
        // Use the Object Picker to select the start SceneAsset
        EditorSceneManager.playModeStartScene = (SceneAsset)EditorGUILayout.ObjectField(new GUIContent("Start Scene"), EditorSceneManager.playModeStartScene, typeof(SceneAsset), false);

        // Or set the start Scene from code
        if (GUILayout.Button("Set start Scene: " + DEFAULT_START_SCENE_PATH))
            SetPlayModeStartScene(DEFAULT_START_SCENE_PATH);
    }

    public static void SetPlayModeStartScene(string scenePath)
    {
        var myWantedStartScene = AssetDatabase.LoadAssetAtPath<SceneAsset>(scenePath);
        if (myWantedStartScene != null)
            EditorSceneManager.playModeStartScene = myWantedStartScene;
        else
            ULogger.Log("Could not find Scene " + scenePath);
    }

    public static void SetDefaultIfEmpty()
    {
        // 세팅되지 않았을 때만 디폴트로 세팅함 
        if (EditorSceneManager.playModeStartScene == null)
        {
            SetPlayModeStartScene(DEFAULT_START_SCENE_PATH);
        }
    }

    [MenuItem("Start Scene/Setting %e")]
    static void Open()
    {
        GetWindow<StartSceneWindow>();
    }
}