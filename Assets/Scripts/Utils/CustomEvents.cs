﻿using UnityEngine;
using System;
using UnityEngine.Events;



[Serializable] public class GameObjectEvent : UnityEvent<GameObject> { }

[Serializable] public class TransformEvent : UnityEvent<Transform> { }

[Serializable] public class RayCastHitEvent : UnityEvent<RaycastHit> { }

[Serializable] public class Vector3Event : UnityEvent<Vector3> { }

[Serializable] public class Vector2Event : UnityEvent<Vector2> { }

[Serializable] public class ColorEvent : UnityEvent<Color> { }

[Serializable] public class IntEvent : UnityEvent<int> { }

[Serializable] public class FloatEvent : UnityEvent<float> { }

[Serializable] public class BoolEvent : UnityEvent<bool> { }

[Serializable] public class BoolBoolEvent : UnityEvent<bool, bool> { }

[Serializable] public class StringEvent : UnityEvent<string> { }

[Serializable] public class StringBoolEvent : UnityEvent<string, bool> { }

[Serializable] public class ColliderEvent : UnityEvent<Collider> { }

[Serializable] public class Collider2DEvent : UnityEvent<Collider2D> { }

[Serializable] public class CollisionEvent : UnityEvent<Collision> { }

[Serializable] public class Collision2DEvent : UnityEvent<Collision2D> { }

[Serializable] public class ComponentEvent : UnityEvent<Component> { }



public delegate void GameObjectDelegate(GameObject arg);
public delegate void IntDelegate(int arg);
public delegate void StringDelegate(string arg);
public delegate void FloatDelegate(float arg);
public delegate void BoolDelegate(bool arg);
public delegate void DoubleDelegate(double arg);
public delegate void VoidDelegate();

public delegate void StringStringDelegate(string arg, string arg2);
public delegate void StringIntDelegate(string arg, int arg2);