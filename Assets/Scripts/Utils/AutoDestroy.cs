﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AutoDestroy : MonoBehaviour
{
    [SerializeField]
    float time;

    public float Time { get { return time; } set { time = value; } }

    void Start()
    {
        Destroy(gameObject, time);
    }
}
