﻿using UnityEngine;

namespace Utils.Layer
{
    /// <summary>
    /// Attribute to select a single layer.
    /// </summary>
    public class LayerAttribute : PropertyAttribute
    { 
        // NOTHING - just oxygen.
    }
}