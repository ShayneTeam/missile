using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class PhysicalDragThreshold : MonoBehaviour
{
    private const float inchToCm = 2.54f;

    [SerializeField] private EventSystem eventSystem = null;

    [SerializeField] private float dragThresholdCM = 0.5f;

    void Awake()
    {
        SetDragThreshold();
    }

    private void SetDragThreshold()
    {
        if (eventSystem != null)
        {
            eventSystem.pixelDragThreshold = (int) (dragThresholdCM * Screen.dpi / inchToCm);
            ULogger.Log($@"pixelDragThreshold Value : {eventSystem.pixelDragThreshold.ToString()}");
        }
    }
}