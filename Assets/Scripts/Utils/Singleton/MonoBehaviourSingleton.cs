﻿using System;
using UnityEngine;
using System.Collections;

/*
 * 처음 생성된 Instance만 사용 (이후 생성된 녀석들은 방치됨)
 * Instance는 Destroy 될 수 있음
 * Destroy되면 다시 세팅함 
 */
public class MonoBehaviourSingleton<T> : MonoBehaviour
    where T : Component
{
    private static T _instance;
    public static T Instance {
        get {
            if (_instance == null) 
            {
                var objs = FindObjectsOfType (typeof(T)) as T[];
                if (objs.Length > 0)
                    _instance = objs[0];
                if (objs.Length > 1) 
                {
                    ULogger.LogError ("There is more than one " + typeof(T).Name + " in the scene.");
                }
                if (_instance == null) {
                    var obj = new GameObject
                    {
                        name = typeof(T).ToString(),
                        hideFlags = HideFlags.DontSave
                    };
                    _instance = obj.AddComponent<T> ();
                }
            }
            return _instance;
        }
    }
}

/*
 * 처음 생성된 녀석만 계속 사용
 * (이후 생성된 녀석들은 강제 Destroy)
 */
public class MonoBehaviourSingletonPersistent<T> : MonoBehaviour
    where T : Component
{
    public static T Instance { get; private set; }
	
    public virtual void Awake ()
    {
        if (Instance == null) {
            Instance = this as T;
            DontDestroyOnLoad(gameObject);
        } else {
            Destroy (gameObject);
        }
    }
}