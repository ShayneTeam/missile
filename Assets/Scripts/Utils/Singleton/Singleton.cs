﻿using System.Collections;
using System.Collections.Generic;
using Databox.OdinSerializer;
using UnityEngine;

public class Singleton<T> : SerializedMonoBehaviour where T : Component
{
    private static T m_Instance;

    public static T Instance
    {
        get
        {
            if (m_Instance == null)
            {
                m_Instance = FindObjectOfType(typeof(T)) as T;
            }

            return m_Instance;
        }
    }

    private void Awake()
    {
        if (m_Instance == null)
        {
            m_Instance = (object) this as T;
        }
        else
        {
            if (!(m_Instance != this))
                return;
            Destroy(gameObject);
        }
    }
}