﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEditor;
using UnityEngine;


namespace Utils
{
    public class MonoBehaviourMultiton<T> : MonoBehaviour
        where T : Component
    {
        private static Dictionary<string, T> _instances = new Dictionary<string, T>();
        public static IEnumerable<T> Instances => _instances.Values;
    
        public static T GetInstance(string idx)
        {
            // 있으면 있는거 반환
            if (_instances.TryGetValue(idx, out var instance)) 
                return instance;
            
            // 없으면 새로 만들어서 반환 
            var  newInstance = CreateInstance(idx);
            _instances[idx] = newInstance;

            return newInstance;
        }

        private static T CreateInstance(string idx)
        {
            var newGameObj = new GameObject
            {
                // 주의. 아래 옵션 지정 시, FindObjectOfType로 검색되지 않음 
                hideFlags = HideFlags.DontSave,
                // 인덱스를 이름으로 지정시켜서 코드에서 찾을 수 있게 만듬 
                name = idx
            };
            
            return newGameObj.AddComponent<T>();
        }
    }
}