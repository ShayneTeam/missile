﻿using System;
using UnityEngine;

namespace Utils
{
    public class AutoPersist : MonoBehaviour
    {
        private void Awake()
        {
            DontDestroyOnLoad(this);
        }
    }
}