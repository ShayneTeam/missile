﻿using System.Collections.Generic;
using InGame.Enemy;
using UnityEngine;

namespace Utils
{
    // CachingDictionary는 null을 넣어도 값을 넣었다고 간주함 (TryGetValue 성공)
    // 일반 Dictionary는 null을 넣으면 값을 넣지 않았다고 간주함 (TryGetValue 실패)        
    // 주의. Key가 계속 추가되면서 콜렉션이 무한정 커질 수 있음 
    // 일반 Dictionary 캐싱 방식은 적어도 실제로 존재하는 것들만 콜렉션에 넣으므로, 무한정 커질 가능성이 적음 (가능성이 있긴 함)
    // 반드시 오브젝트 풀링되는 녀석들로만 구성해서 캐싱할 것 
    // 매번 새롭게 로딩되는 씬 데이터들은 절대 캐싱하면 안 됨. 삭제되지 않게 됨 
    public class CachingDictionary<TKey, TValue>
    {
        private class WrapValue
        {
            public TValue Value;
        }
    
        private static readonly Dictionary<TKey, WrapValue> _dic = new Dictionary<TKey, WrapValue>();

        public TValue this[TKey key]
        {
            get
            {
                var success = _dic.TryGetValue(key, out var value);
                return success ? value.Value : default(TValue);
            }
            set
            {
                // 없으면 새로 넣기 
                if (!_dic.ContainsKey(key))
                {
                    var newWrap = new WrapValue {Value = value};

                    _dic[key] = newWrap;
                }
	
                _dic[key].Value = value;
            }
        }

        public bool TryGetValue(TKey key, out TValue value)
        {
            value = default(TValue);

            if (!_dic.TryGetValue(key, out var wrapValue)) 
                return false;
            
            // 캐싱돼있으면 반환
            value = wrapValue.Value;
            return true;
        }
    }
}