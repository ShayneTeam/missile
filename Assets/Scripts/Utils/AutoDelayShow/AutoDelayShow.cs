using System.Collections.Generic;
using Bolt;
using MEC;
using UnityEngine;

namespace Utils
{
    public class AutoDelayShow : AutoDelayCommon
    {
        public override void Trigger()
        {
            StartDelayShow(true);
        }
    }
}