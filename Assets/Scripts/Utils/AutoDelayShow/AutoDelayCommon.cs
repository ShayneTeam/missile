﻿using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace Utils
{
    public abstract class AutoDelayCommon : MonoBehaviour
    {
        [SerializeField] private GameObject target;
        [SerializeField] private float delay;

        private CoroutineHandle _delayShow;
			
        protected void StartDelayShow(bool show)
        {
            // 이전 처리 날려버리기 
            Timing.KillCoroutines(_delayShow);

            _delayShow = Timing.RunCoroutine(_DelayShow(show).CancelWith(gameObject), Segment.RealtimeUpdate);
        }

        private IEnumerator<float> _DelayShow(bool show)
        {
            yield return Timing.WaitForSeconds(delay);

            target.SetActive(show);
        }

        public abstract void Trigger();
    }
}