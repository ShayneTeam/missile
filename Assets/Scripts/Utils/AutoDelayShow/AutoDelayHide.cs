using System.Collections.Generic;
using Bolt;
using MEC;
using UnityEngine;

namespace Utils
{
    public class AutoDelayHide : AutoDelayCommon
    {
        public override void Trigger()
        {
            StartDelayShow(false);
        }
    }
}