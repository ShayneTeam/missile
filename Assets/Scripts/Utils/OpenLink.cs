﻿using JetBrains.Annotations;
using UnityEngine;

namespace Utils
{
    public class OpenLink : MonoBehaviour
    {
        [SerializeField] private string link;
        
        [PublicAPI]
        public void Open()
        {
            Application.OpenURL(link); 
        }
    }
}