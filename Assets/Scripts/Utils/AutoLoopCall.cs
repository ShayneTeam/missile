using System;
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace Utils
{
	// 일정 간격으로, 요청이 들어와있을 때만 함수 호출
    public class AutoLoopCall
    {
        private CoroutineHandle _handle;
						
        public void Start(float interval, Action call, GameObject owner)
        {
	        Timing.KillCoroutines(_handle);
	        _handle = Timing.RunCoroutine(_AutoCallFlow(interval, call).CancelWith(owner), Segment.RealtimeUpdate);
        }
							
        private IEnumerator<float> _AutoCallFlow(float interval, Action call)
        {
	        while (true)
	        {
			    call();
		        
		        yield return Timing.WaitForSeconds(interval);
	        }
        }
    }
}