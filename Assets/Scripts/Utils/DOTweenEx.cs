﻿using UnityEngine.UI;
using System;
using DG.Tweening;
using TMPro;
using UnityEngine;

// ReSharper disable InconsistentNaming

namespace Utils
{
    public static class DOTweenEx
    {
        public static Tweener DOTextInt(this TextMeshProUGUI text, int initialValue, int finalValue, float duration, Func<int, string> convertor)

        {
            return DOTween.To(
                () => initialValue,
                it => text.text = convertor(it),
                finalValue,
                duration
            );
        }
        
        public static Tweener DoMatValue(this Material mat, string matName, float initialValue, float finalValue, float duration)
        {
            mat.SetFloat(matName, initialValue);
            
            return DOTween.To(
                () => initialValue, 
                it => mat.SetFloat(matName, it), 
                finalValue,
                duration);
        }

        public static Tweener DOTextInt(this TextMeshProUGUI text, int initialValue, int finalValue, float duration)

        {
            return DOTextInt(text, initialValue, finalValue, duration, it => it.ToString());
        }


        public static Tweener DOTextFloat(this TextMeshProUGUI text, float initialValue, float finalValue, float duration, Func<float, string> convertor)

        {
            return DOTween.To(
                () => initialValue,
                it => text.text = convertor(it),
                finalValue,
                duration
            );
        }


        public static Tweener DOTextFloat(this TextMeshProUGUI text, float initialValue, float finalValue, float duration)

        {
            return DOTextFloat(text, initialValue, finalValue, duration, it => it.ToString());
        }


        public static Tweener DOTextLong(this TextMeshProUGUI text, long initialValue, long finalValue, float duration, Func<long, string> convertor)

        {
            return DOTween.To(
                () => initialValue,
                it => text.text = convertor(it),
                finalValue,
                duration
            );
        }


        public static Tweener DOTextLong(this TextMeshProUGUI text, long initialValue, long finalValue, float duration)

        {
            return DOTextLong(text, initialValue, finalValue, duration, it => it.ToString());
        }


        public static Tweener DOTextDouble(this TextMeshProUGUI text, double initialValue, double finalValue, float duration, Func<double, string> convertor)

        {
            return DOTween.To(
                () => initialValue,
                it => text.text = convertor(it),
                finalValue,
                duration
            );
        }


        public static Tweener DOTextDouble(this TextMeshProUGUI text, double initialValue, double finalValue, float duration)

        {
            return DOTextDouble(text, initialValue, finalValue, duration, it => it.ToString());
        }
    }
}