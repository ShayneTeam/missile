﻿using System;
using System.Collections.Generic;
using System.Threading;
using UnityEngine;

namespace Utils.Dispatcher
{
    public class Dispatcher : MonoBehaviourSingletonPersistent<Dispatcher>
    {
        private int m_lock;
        private bool m_run;
        private Queue<Action> m_wait;

        public void BeginInvoke(Action action)
        {
            while (true)
            {
                if (0 == Interlocked.Exchange(ref m_lock, 1))
                {
                    m_wait.Enqueue(action);
                    m_run = true;
                    Interlocked.Exchange(ref m_lock, 0);
                    break;
                }
            }
        }

        public override void Awake()
        {
            base.Awake();
            
            m_wait = new Queue<Action>();
        }

        private void Update()
        {
            if (m_run)
            {
                Queue<Action> execute = null;
                if (0 == Interlocked.Exchange(ref m_lock, 1))
                {
                    execute = new Queue<Action>(m_wait.Count);
                    while (m_wait.Count != 0)
                    {
                        Action action = m_wait.Dequeue();
                        execute.Enqueue(action);
                    }
                    m_run = false;
                    Interlocked.Exchange(ref m_lock, 0);
                }

                if (execute != null)
                {
                    while (execute.Count != 0)
                    {
                        Action action = execute.Dequeue();
                        action();
                    }
                }
            }

        }
    }

}
