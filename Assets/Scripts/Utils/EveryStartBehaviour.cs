﻿using System;
using MEC;
using UnityEngine;

namespace Utils
{
    public abstract class EveryStartBehaviour : MonoBehaviour
    {
        [NonSerialized] private bool _started;
        private CoroutineHandle _call;


        protected virtual void OnEnable()
        {
            if (_started)
            {
                //this.OnStartOrEnable();
                // 일부러 다음 프레임에 호출시켜서, 일반 Start()와 동일한 타이밍에 호출되는 것처럼 만듬 
                _call = Timing.CallDelayed(0f, this.OnEveryStart);
            }
        }

        protected virtual void OnDisable()
        {
            // 같은 프레임에 OnDisable 호출되면, 다음 프레임콜 취소시킴
            Timing.KillCoroutines(_call);
        }

        protected virtual void Start()
        {
            _started = true;
            
            this.OnEveryStart();
        }

        /// <summary>
        /// On start or on enable if and only if start already occurred.
        /// This adjusts the order of 'OnEnable' so that it can be used in conjunction with 'OnDisable' to wire up handlers cleanly. 
        /// OnEnable occurs BEFORE Start sometimes, and other components aren't ready yet. This remedies that.
        /// </summary>
        protected abstract void OnEveryStart();
    }
}