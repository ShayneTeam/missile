using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.UI
{
    public class IconNameCount : MonoBehaviour
    {
        [SerializeField] public Image icon;
        [SerializeField] public TextMeshProUGUI nameText;
        [SerializeField] public TextMeshProUGUI countText;

        public void Init(Sprite spr, string nameStr, string countStr)
        {
            // 아이콘
            icon.gameObject.SetActive(spr != null);
            if (spr != null) icon.SetSpriteWithOriginSIze(spr);
            
            // 이름
            nameText.text = nameStr;
            
            // 카운트
            countText.text = countStr;
        }
    }
}