using UnityEngine;
using UnityEngine.UI;

namespace Utils
{
    [RequireComponent(typeof(Image))]
    public class ImageMaterialSwapper : MonoBehaviour
    {
        private Image image;
        
        [SerializeField] private Material on;
        [SerializeField] private Material off;

        public void Init(bool isOn)
        {
            if (image == null)
                image = GetComponent<Image>();

            image.material = isOn ? on : off;
        }
    }
}