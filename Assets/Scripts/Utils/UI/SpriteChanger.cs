using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

namespace Utils
{
    public class SpriteChanger : MonoBehaviour
    {
        [SerializeField] private List<Sprite> sprites;

        private Image _image;
        private SpriteRenderer _renderer;

        public void Init(int index)
        {
            if (_image == null)
                _image = GetComponent<Image>();
            
            if (_renderer == null)
                _renderer = GetComponent<SpriteRenderer>();

            if (_image != null)
            {
                _image.sprite = sprites[index];
                return;
            }
            
            if (_renderer != null)
            {
                _renderer.sprite = sprites[index];
                return;
            }
        }
    }
}