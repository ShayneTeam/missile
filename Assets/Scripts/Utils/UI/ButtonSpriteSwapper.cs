using UnityEngine;
using UnityEngine.UI;

namespace Utils
{
    [RequireComponent(typeof(Button))]
    public class ButtonSpriteSwapper : MonoBehaviour
    {
        private Button button;
        
        [SerializeField] private Sprite on;
        [SerializeField] private Sprite off;

        public void Init(bool isOn)
        {
            if (button == null)
                button = GetComponent<Button>();

            button.image.sprite = isOn ? on : off;
        }
    }
}