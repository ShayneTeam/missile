﻿using System;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace Utils
{
    [RequireComponent(typeof(EnhancedScroller))]
    public class CommonScroller : MonoBehaviour
    {
        [SerializeField] public EnhancedScrollerCellView cellPrefab;
        [SerializeField] public float cellSize;

        public EnhancedScroller Scroller { get; set; }

        private void Awake()
        {
            Scroller = GetComponent<EnhancedScroller>();
        }
    }
}