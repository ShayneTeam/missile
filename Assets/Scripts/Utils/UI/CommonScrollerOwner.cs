﻿using System;
using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace Utils
{
    public class CommonScrollerOwner : MonoBehaviour
    {
        [SerializeField] public EnhancedScrollerCellView cellPrefab;
        [SerializeField] public float cellSize;
        [SerializeField] public EnhancedScroller scroller;
    }
}