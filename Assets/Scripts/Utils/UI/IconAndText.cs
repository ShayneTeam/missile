﻿using Ludiq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Utils.UI
{
    public class IconAndText : MonoBehaviour
    {
        [SerializeField] public Image icon;
        [SerializeField] public TextMeshProUGUI text;

        public void Init(Sprite spr, string str)
        {
            // 아이콘
            icon.gameObject.SetActive(spr != null);
            
            if (spr != null)
                icon.SetSpriteWithOriginSIze(spr);
            
            // 텍스트
            text.text = str;
        }
    }
}