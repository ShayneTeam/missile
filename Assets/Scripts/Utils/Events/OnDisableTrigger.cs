using System.Collections.Generic;
using AsCoroutine;
using MEC;
using UnityEngine;
using UnityEngine.Events;

namespace Utils.Events
{
    public class OnDisableTrigger : MonoBehaviour
    {
        [SerializeField] private bool triggerNextFrame;
        
        [SerializeField] private UnityEvent action;

        private CoroutineHandle _invoking;
        
        private void OnDisable()
        {
            // 이벤트로 트윈 플레이를 걸면, OnEnable 타이밍엔 instant로 처리 됨 
            // OnEnable에서 트윈을 제대로 시작시키려면, 다음 프레임에 플레이 시켜야 함 
            if (triggerNextFrame)
            {
                Timing.KillCoroutines(_invoking);
                _invoking = Timing.RunCoroutine(_Invoke().CancelWith(gameObject));
            }
            else
            {
                action?.Invoke();
            }
        }
        
        private IEnumerator<float> _Invoke()
        {
            yield return Timing.WaitForOneFrame;
            
            action?.Invoke();
        }
    }
}