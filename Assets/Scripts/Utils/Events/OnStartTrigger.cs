using UnityEngine;
using UnityEngine.Events;

namespace Utils.Events
{
    public class OnStartTrigger : MonoBehaviour
    {
        [SerializeField] private UnityEvent onStart;

        private void OnStart()
        {
            onStart?.Invoke();
        }
    }
}