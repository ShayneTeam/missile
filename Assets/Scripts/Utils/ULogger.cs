using System.Diagnostics;
using UnityEngine;
using Debug = UnityEngine.Debug;

public static class ULogger
{
    [Conditional("UNITY_EDITOR"), Conditional("DEVELOPMENT_BUILD")]
    public static void Log(string logString, Color? color = null)
    {
        color ??= Color.white;

        Debug.Log("<color=#"+ColorUtility.ToHtmlStringRGB(color.Value)+"> "+logString+" </color>");
    }
    
    [Conditional("UNITY_EDITOR"), Conditional("DEVELOPMENT_BUILD")]
    public static void LogWarning(string logString)
    {
        Debug.LogWarning(logString);
    }

    [Conditional("UNITY_EDITOR"), Conditional("DEVELOPMENT_BUILD")]
    public static void LogError(string logString)
    {
        Debug.LogError(logString);
    }
}