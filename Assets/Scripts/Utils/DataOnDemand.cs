using System;
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace Utils
{
	
	// 일정 간격으로, 요청이 들어와있을 때만 함수 호출
    public class DataOnDemand<T>
    {
	    public delegate T DataOnDemandDelegate();

	    private DataOnDemandDelegate _getter;
	    
        private bool _isReserved;
        private CoroutineHandle _handle;
        private T _data;

        public T Data
        {
	        get
	        {
		        // 최초의 데이터가 null 일 때만 Getter 즉각 호출 
		        if (_data == null && _getter != null)
			        _data = _getter();
		        // 데이터가 있는 상태서 들어온 데이터 요청이라면, 예약 걸어둠
		        else
			        _isReserved = true;
			        
		        return _data;
	        }
        }

        public void Init(float checkInterval, DataOnDemandDelegate getter, GameObject owner)
        {
	        _getter = getter;
	        
	        // 체크 시작 
	        Timing.KillCoroutines(_handle);
	        _handle = Timing.RunCoroutine(_AutoCallFlow(checkInterval).CancelWith(owner), Segment.RealtimeUpdate);
        }
							
        private IEnumerator<float> _AutoCallFlow(float checkInterval)
        {
	        while (true)
	        {
		        // 예약돼있을 때만
		        if (_isReserved)
		        {
			        // getter 요청해서 데이타 받아옴 
			        _data = _getter();
			        
			        _isReserved = false;
		        }
		        
		        // 간격 대기 
		        yield return Timing.WaitForSeconds(checkInterval);
	        }
        }
    }
}