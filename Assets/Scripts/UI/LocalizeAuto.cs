﻿using System;
using InGame;
using InGame.Data;
using TMPro;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class LocalizeAuto : MonoBehaviour
    {
        [SerializeField] private string key;
        
        private TextMeshProUGUI _text;

        
        private void Start()
        {
            /*
             * UserData와 Databox 둘 다 로드된 이후에 정상 작동됨 
             */
            _text = GetComponent<TextMeshProUGUI>();
            
            SetText();

            // 이벤트 등록
            UserData.My.UserInfo.OnChangedLanguage += OnChangedLanguage;
        }

        private void OnDestroy()
        {
            // 이벤트 해제
            UserData.My.UserInfo.OnChangedLanguage -= OnChangedLanguage;
        }

        private void OnChangedLanguage(string lang)
        {
            SetText();
        }

        private void SetText()
        {
            _text.text = Localization.GetText(key, _text.text);
        }
    }
}