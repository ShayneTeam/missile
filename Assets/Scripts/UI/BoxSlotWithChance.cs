﻿using Global;
using InGame;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BoxSlotWithChance : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TextMeshProUGUI text;
	
        private string _dataIdx;
	
	
        public void Init(string idx)
        {
	        _dataIdx = idx;

	        // 이미지 
	        var icon = DataboxController.GetDataString(Table.Box, Sheet.Box, idx, "icon");
	        image.sprite = AtlasController.GetSprite(icon);

	        // 
	        RefreshText("1");
        }
		
        public void RefreshText(string planet)
        {
	        // 텍스트 
	        var rarityName = DataboxController.GetDataString(Table.Box, Sheet.Box, _dataIdx, "rarity", "normal");
	        var chance = DataboxController.GetDataFloat(Table.Stage, Sheet.RewardInfo, planet, rarityName);

	        var chanceStr = chance.ToString("0.00");
	        text.text = $"{chanceStr}%";
        }

    }
}