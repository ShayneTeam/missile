﻿using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using JetBrains.Annotations;
using MEC;
using UnityEngine;
using UnityEngine.Events;

namespace UI
{
    [RequireComponent(typeof(UIButton))]
    public class AutoClick : MonoBehaviour
    {
        [SerializeField] private float autoClickDelay;
        [SerializeField] private float autoClickInterval;

        [SerializeField] private UnityEvent onClick; 
        
        private CoroutineHandle _autoClick;


        private void Start()
        {
            var button = GetComponent<UIButton>();
            
            button.OnPointerDown.Enabled = true;
            button.OnPointerDown.OnTrigger.Action -= StartAutoClick;
            button.OnPointerDown.OnTrigger.Action += StartAutoClick;
            
            button.OnPointerUp.Enabled = true;
            button.OnPointerUp.OnTrigger.Action -= StopAutoClick;
            button.OnPointerUp.OnTrigger.Action += StopAutoClick;
            
            button.OnPointerExit.Enabled = true;
            button.OnPointerExit.OnTrigger.Action -= StopAutoClick;
            button.OnPointerExit.OnTrigger.Action += StopAutoClick;

        }
        
        private void StartAutoClick(GameObject _)
        {
            StopAutoClick(null);
            
            _autoClick = Timing.RunCoroutine(_AutoClickFlow().CancelWith(gameObject));
        }

        private void StopAutoClick(GameObject _)
        {
            if (_autoClick.IsRunning)
                Timing.KillCoroutines(_autoClick);
        }

        private IEnumerator<float> _AutoClickFlow()
        {
            // 들어오자마자 한번 처리 
            onClick?.Invoke();
				
            // 딜레이 
            yield return Timing.WaitForSeconds(autoClickDelay);
				
            // 
            while (true)
            {
                onClick?.Invoke();

                // 일정 주기로 반복 
                yield return Timing.WaitForSeconds(autoClickInterval);
            }
        }
    }
}