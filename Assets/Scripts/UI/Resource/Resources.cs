﻿using System;
using System.Globalization;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using UnityEngine;

namespace UI
{
    public class Resources : MonoBehaviour
    {
        [SerializeField] public ResourceText mineral;
        [SerializeField] public ResourceText gas;
        [SerializeField] public ResourceText stone;
        [SerializeField] public ResourceText uranium;

        private string _inDateRegistered;

        private void Start()
        {
            // 로그아웃 처리용
            Stage.OnStart += OnStartStage;
            LogoutController.OnLogout += OnLogout;
        }

        private void OnDestroy()
        {
            Stage.OnStart -= OnStartStage;
            LogoutController.OnLogout -= OnLogout;
        }

        private void OnEnable()
        {
            Refresh();
            
            Register();
        }

        private void OnDisable()
        {
            Unregister();
        }

        private void OnStartStage()
        {
            Refresh();
                        
            Register();
        }

        private void OnLogout()
        {
            Unregister();
        }

        private void Register()
        {
            // 이벤트 중복 등록시키지 않기 위함 
            if (_inDateRegistered == UserData.My.InDate) return;
            
            UserData.My.Resources.OnChangedMineral += OnChangedResource;
            UserData.My.Resources.OnChangedGas += OnChangedResource;
            UserData.My.Resources.OnChangedStone += OnChangedResource;
            UserData.My.Resources.OnChangedUranium += OnChangedResource;
            
            _inDateRegistered = UserData.My.InDate;
        }
        
        private void Unregister()
        {
            UserData.My.Resources.OnChangedMineral -= OnChangedResource;
            UserData.My.Resources.OnChangedGas -= OnChangedResource;
            UserData.My.Resources.OnChangedStone -= OnChangedResource;
            UserData.My.Resources.OnChangedUranium -= OnChangedResource;

            // 등록된 이벤트가 해제됐다면, null로 바꿔주기 
            if (_inDateRegistered == UserData.My.InDate) _inDateRegistered = null;
        }

        private void OnChangedResource(double value)
        {
            Refresh();
        }

        private void Refresh()
        {
            mineral.Init(UserData.My.Resources.mineral.ToUnitString());
            gas.Init(UserData.My.Resources.gas.ToUnitString());
            stone.Init(UserData.My.Resources.stone.ToUnitString());
            uranium.Init(Math.Floor(UserData.My.Resources.uranium).ToString("0"));
        }

        public void Pause()
        {
            Unregister();
        }

        public void Resume()
        {
            Refresh();
            
            Register();
        }
    }
}