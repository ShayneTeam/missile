﻿using System;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using UnityEngine;

namespace UI
{
    public class ResourcesWithEffect : MonoBehaviour
    {
        [SerializeField] public ResourceTextWithEffect mineral;
        [SerializeField] public ResourceTextWithEffect gas;
        [SerializeField] public ResourceTextWithEffect stone;
        [SerializeField] public ResourceTextWithEffect uranium;
    
        private void Start()
        {
            // 여기 들어올 때 UserData가 싱크돼있는 상태여야 함
            Init();

            // 중요. ResourceController에 본인을 등록
            // 이미 동작되는게 등록돼있다면, 덮어씌우지 않음 (로그아웃) 
            if (ResourceController.Instance.Resources == null)
                ResourceController.Instance.Resources = this;
            
            // 이벤트 
            Stage.OnStart += Init;
        }

        private void OnDestroy()
        {
            // 이벤트
            Stage.OnStart -= Init;
        }

        private void Init()
        {
            mineral.InitWithOutEffect(UserData.My.Resources.mineral.ToUnitString());
            gas.InitWithOutEffect(UserData.My.Resources.gas.ToUnitString());
            stone.InitWithOutEffect(UserData.My.Resources.stone.ToUnitString());
            uranium.InitWithOutEffect(Math.Floor(UserData.My.Resources.uranium).ToString("0"));
        }
    }
}