﻿using System;
using System.Collections;
using DG.Tweening;
using Lean.Pool;
using TMPro;
using UnityEngine;

[RequireComponent(typeof(TextMeshProUGUI))]
public class ResourceTextEffect : MonoBehaviour
{
    private TextMeshProUGUI _text;

    private Tween _scaling;
    private Tween _fading;
    private Tween _moving;

    private void Awake()
    {
        _text = GetComponent<TextMeshProUGUI>();
    }

    public void Init(string resourceText)
    {
        _text.text = resourceText;

        // 펀치 스케일링 없이 재사용되니 너무 밋밋한데 
        if (_scaling == null)
        {
            const float scalingDuration = 0.1f;
            var currentFontSize = _text.fontSize;
            _scaling = _text.DOFontSize(currentFontSize + 2, scalingDuration / 2f)
                .SetAutoKill(false)
                .OnComplete(() => { _text.DOFontSize(currentFontSize, scalingDuration / 2f); });
        }
        else
        {
            _scaling.Restart();
        }

        // 페이드 
        if (_fading == null)
        {
            const float fadeOutDuration = 1f;
            _fading = _text.DOFade(0f, fadeOutDuration)
                .SetDelay(1f)
                .SetAutoKill(false);
        }
        else
        {
            _fading.Restart();
        }

        // 위로 움직임 
        if (_moving == null)
        {
            const float movingDuration = 1f;
            _moving = _text.transform.DOLocalMoveY(10f, movingDuration)
                .SetDelay(1f)
                .SetAutoKill(false);
        }
        else
        {
            _moving.Restart();
        }
    }
}