﻿using System;
using UnityEngine;

namespace UI
{
    public class ResourceTextWithEffect : MonoBehaviour
    {
        [SerializeField] private ResourceText resourceText;
        [SerializeField] private ResourceTextEffect resourceTextEffect;
        
        private void Awake()
        {
            resourceText = GetComponent<ResourceText>();
        }

        public void Init(string initStr, string effectStr)
        {
            // 자원 
            resourceText.Init(initStr);
		
            // 이펙트
            resourceTextEffect.Init(effectStr);
        }

        public void InitWithOutEffect(string initStr)
        {
            // 자원 
            resourceText.Init(initStr);
        }
    }
}