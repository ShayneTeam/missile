﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class ResourceText : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI text;
    [SerializeField] private Image icon;

    public Image Icon => icon;
    public TextMeshProUGUI Text => text;
    
    private Tweener _countTweener;

    public void Init(string initStr)
    {
        // UI 갱신 
        text.text = initStr;
    }

    // 이 함수는 리소스 텍스트와는 별개의 컴포넌트로 뺀 후, 리소스텍스트를 RequireComponent 하는 식이 돼야 함  
    public void StartCountAnimation(double startValue, double endValue, float duration)
    {
        _countTweener = text.DOTextDouble(startValue, endValue, duration, d => d.ToUnitString()).SetAutoKill(true);
    }
}
