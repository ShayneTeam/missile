﻿using UnityEngine;

namespace UI
{
    public class ParentSwitcher : MonoBehaviour
    {
        private Transform _prevParent;

        public void Switch(Transform newParent)
        {
            if (newParent == _prevParent)
                return;
            
            _prevParent = transform.parent;
            
            transform.SetParent(newParent);
        }

        public void Rollback()
        {
            transform.SetParent(_prevParent);
        }
    }
}