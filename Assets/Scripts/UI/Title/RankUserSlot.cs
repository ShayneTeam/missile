﻿using System;
using InGame;
using TMPro;
using UnityEngine;

namespace UI.Title
{
    public class RankUserSlot : MonoBehaviour
    {
        [SerializeField] private CharacterSlot characterSlot;
        [SerializeField] private TextMeshProUGUI rankText;
        [SerializeField] private TextMeshProUGUI nicknameText;

        public void Awake()
        {
            /*
             * 시작 시엔 안 보이게 설정
             * 유저 세팅되면 그 때 보여줌 
             */
            Show(false);
        }

        public void Init(int rank, string nickname, string headIdx, string skinIdx, string bodyIdx, string bazookaIdx)
        {
            // 캐릭터
            characterSlot.WearCostume(headIdx, skinIdx, bodyIdx, bazookaIdx);
            
            // 순위
            rankText.text = Localization.GetFormatText("info_251", rank.ToLookUpString());
            
            // 닉네임 
            nicknameText.text = nickname;
            
            // 세팅다 된 후 보여주기 
            Show(true);
        }

        private void Show(bool active)
        {
            characterSlot.gameObject.SetActive(active);
            rankText.gameObject.SetActive(active);
            nicknameText.gameObject.SetActive(active);
        }
    }
}