﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Doozy.Engine.UI;
using InGame;
using InGame.Data;
using Lean.Pool;
using MEC;
using TMPro;
using UnityEngine;

namespace UI
{
	public class NormalTextEffect : MonoBehaviour
	{
		[SerializeField] private TextMeshProUGUI text;

		private float _originFontSize;
		public RectTransform rectTransform;
		
		private Tweener _punch;
		private Tweener _fade;

		public static void Show(string effectText, Color color, Vector3 position, float fontScale = 1f)
		{
			// 텍스트 이펙트 온오프 설정 
			if (!UserData.My.UserInfo.GetFlag(UserFlags.IsOnTextEffect, true)) return; 
			
			// 스폰
			var textEffect = LeanPool.Spawn(InGamePrefabs.Instance.normalText, InGamePrefabs.Instance.damageTextParent);
			textEffect.Init(effectText, color, fontScale);
			textEffect.rectTransform.FromWorldPosition(position, Camera.main, UICanvas.GetUICanvas("InGame").Canvas);
		}

		private void Awake()
		{
			rectTransform = GetComponent<RectTransform>();
			_originFontSize = text.fontSize;
		}

		private void Init(string effectText, Color color, float fontScale = 1f)
		{
			text.text = effectText;
			text.color = color;
			text.fontSize = _originFontSize * fontScale;

			//
			Timing.RunCoroutine(_Animation().CancelWith(gameObject));
		}
			
		private IEnumerator<float> _Animation()
		{
			// 스케일 펀치 
			const float punchDuration = 0.5f;
			if (_punch == null)
			{
				_punch = transform.DOPunchScale(Vector3.one * 1.5f, punchDuration, 0)
					.SetRelative(true)
					.SetAutoKill(false);
			}
			else
			{
				_punch.Restart();
			}
			yield return Timing.WaitForSeconds(punchDuration);

			// 사라지기 
			const float fadeOutDuration = 1f;
			if (_fade == null)
			{
				_fade = text.DOFade(0f, fadeOutDuration)
					.SetAutoKill(false);
			}
			else
			{
				_fade.Restart();
			}

			yield return Timing.WaitForSeconds(fadeOutDuration);

			// 반납 
			LeanPool.Despawn(gameObject);
		}

	}
}
