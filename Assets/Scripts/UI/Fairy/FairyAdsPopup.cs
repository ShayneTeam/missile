﻿using System;
using Doozy.Engine.UI;
using Firebase.Analytics;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Fairy;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
	[RequireComponent(typeof(UIPopup))]
    public class FairyAdsPopup : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI title;

        [SerializeField] private FairyItemSlot slot1;
        [SerializeField] private FairyItemSlot slot2;

        [SerializeField] private GameObject earnBtn;
        
        
        private Action _earnCallback;
        private Action _earnByAdsCallback;

        private Fairy _fairy;
        private UIPopup _popup;


        private void Awake()
        {
	        _popup = GetComponent<UIPopup>();
        }

        public static void Show(Fairy fairy, Action earnCallback, Action earnByAdsCallback)
        {
	        var popup = GetPopup();
	        popup.Show();

	        var fairyAdsPopup = popup.GetComponent<FairyAdsPopup>();
	        fairyAdsPopup._fairy = fairy;
	        fairyAdsPopup._earnCallback = earnCallback;
	        fairyAdsPopup._earnByAdsCallback = earnByAdsCallback;
	        
	        fairyAdsPopup.Init();
	        
	        WebLog.Instance.ThirdPartyLog("fairy_ads_open");
        }

        public static void HideIfIsMe(Fairy fairy)
        {
	        var popup = GetPopup();
	        
	        // 안 보여지고 있다면, 종료
	        if (!popup.IsVisible) return;
	        
	        // 팝업에서 보여지는 요정과 다르다면, 종료
	        var fairyAdsPopup = popup.GetComponent<FairyAdsPopup>();
	        if (fairyAdsPopup._fairy != fairy) return;

	        // 팝업 닫음 
	        popup.Hide();
        }

        private static UIPopup GetPopup()
        {
	        return UIPopup.GetPopup("Fairy Ads");
        }

        private void Init()
        {
	        FairyController.GetIconAndName(_fairy.Idx, out var iconSpr, out var nameStr);
	        
	        // 아이콘  
	        icon.SetSpriteWithOriginSIze(iconSpr);

	        // 이름
	        title.text = nameStr;
	        
	        // 리워드 슬롯 1 - 아이템 
	        {
		        FairyController.GetItemInfo(_fairy.RewardItemIdx, _fairy.Stage, out _, out var count, out var iconPath, out var nameKey);

		        slot1.Init(iconPath, nameKey, count.ToUnitString());
	        }
	        
	        // 리워드 슬롯 2 - 상자
	        {
		        var hasBoxReward = _fairy.HasBoxReward;
		        slot2.gameObject.SetActive(hasBoxReward);
		        if (hasBoxReward)
		        {
			        FairyController.GetBoxInfo(_fairy.RewardBoxIdx, out _, out var count, out var iconPath, out var nameKey);

			        slot2.Init(iconPath, nameKey, count.ToLookUpString());
		        }
	        }
	        
	        // 광고 제거 상풍 있을 시, 1배 획득 버튼 안 보이게 함  
	        earnBtn.SetActive(!UserData.My.UserInfo.GetFlag(UserFlags.CanAdRemove));
        }

        [PublicAPI]
        public void Earn()
        {
	        _earnCallback?.Invoke();
	        
	        _popup.Hide();
        }
		
        [PublicAPI]
        public void EarnByAds()
        {
	        AdsMediator.Instance.ShowVideoAds((success) =>
	        {
		        if (success)
		        {
			        _earnByAdsCallback?.Invoke();
			        
			        // 팝업 닫음 
			        _popup.Hide();
		        }
		        else
		        {
			        // 광고 시청 실패 메시지
			        MessagePopup.Show("shop_07");
		        }
	        });
        }
    }
}