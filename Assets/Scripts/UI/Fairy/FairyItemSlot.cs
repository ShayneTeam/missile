﻿using System;
using InGame;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class FairyItemSlot : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI amountText;

        [SerializeField] private bool fitSize; 
        private float _standardHeightSize;

        public void Init(string iconPath, string nameKey, string amountStr)
        {
            // 아이콘
            if (icon != null)
            {
                if (fitSize)
                {
                    if (_standardHeightSize == 0f) // Awake 타이밍보다 빨리 들어오므로, 레이지 초기화
                        _standardHeightSize = icon.sprite.rect.size.y; // 초기 사이즈 기준을 Height로 맞춤 

                    var sprite = AtlasController.GetSprite(iconPath);
                    icon.SetSpriteWithOriginSIze(sprite);

                    var revisionScale = _standardHeightSize / sprite.rect.size.y; // 어떤 스프라이트가 들어와도, 기준 사이즈에 맞추는 스케일 값 계산  
                    icon.rectTransform.localScale = Vector3.one * revisionScale;
                }
                else
                {
                    var sprite = AtlasController.GetSprite(iconPath);
                    icon.SetSpriteWithOriginSIze(sprite);
                }
            }
            
            // 이름
            if (nameText != null)
                nameText.text = Localization.GetText(nameKey);
            
            // 수량
            if (amountText != null)
                amountText.text = amountStr;
        }
    }
}