﻿using System;
using Doozy.Engine.UI;
using UnityEngine;

namespace UI.Popup
{
    public class FairyCatchEffect : MonoBehaviour
    {
        private RectTransform _rectTransform;

        private void Awake()
        {
            _rectTransform = GetComponent<RectTransform>();
        }

        public void Init(Vector3 position)
        {
            _rectTransform.FromWorldPosition(position, Camera.main, UICanvas.GetUICanvas("InGame").Canvas);
        }
    }
}