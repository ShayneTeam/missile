using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class IngameUpdatePopup : MonoBehaviour
{
    public TextMeshProUGUI percentText;
    public Image percentImage;

    public void ProgressUpdate(float percent, ulong downloadedBytes)
    {
        var percentStr = (percent * 100).ToString("F");
        percentText.text = $@"Update.. {percentStr}% ({GetFriendlySize(downloadedBytes)})";
        percentImage.fillAmount = percent; // 0~1
    }
    
    string GetFriendlySize(ulong byteSize)
    {
        string[] sizes = { "B", "KB", "MB", "GB", "TB" };
        int order = 0;
        ulong prevOrderRemainder = 0;
        while (byteSize >= 1024 && order < sizes.Length - 1)
        {
            order++;
            prevOrderRemainder = byteSize % 1024;
            byteSize = byteSize / 1024;
        }

        var byteSizeFloat = (double)byteSize + (double)prevOrderRemainder / 1024;

        var byteStr = byteSizeFloat.ToString("0.##");
        var result = $"{byteStr}{sizes[order]}";
        return result;
    }
}