﻿using System;
using DG.Tweening;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    [RequireComponent(typeof(RewardItemSlot))]
    public class RewardItemCellView : EnhancedScrollerCellView
    {
        [SerializeField] private string defaultSlotSpriteName;
        
        private RewardItemSlot _itemSlot;
        private DOTweenAnimation _tween;

        private void Awake()
        {
            _itemSlot = GetComponent<RewardItemSlot>();
            _tween = GetComponent<DOTweenAnimation>();
        }

        public void Init(RewardData reward)
        {
            // 타입에 따라 별도의 테이블을 분기해서 iconPath 전달 
            var iconPath = "";
            var slotPath = defaultSlotSpriteName;
            var scale = 1f;
            switch (reward.Type)
            {
                case RewardType.REWARD_MISSILE:
                {
                    iconPath = DataboxController.GetDataString(Table.Missile, Sheet.missile, reward.Idx, "component_id");
                    scale = 1.6f;
                } break;
                case RewardType.REWARD_DIABLO_RELIC:
                {
                    iconPath = DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, reward.Idx, Column.icon);
                } break;
                case RewardType.REWARD_GIRL:
                {
                    iconPath = DataboxController.GetDataString(Table.Princess, Sheet.girl, reward.Idx, Column.resource_id);
                    slotPath = DataboxController.GetDataString(Table.Princess, Sheet.girl, reward.Idx, Column.slot_id);
                } break;
                default:
                {
                    // 그 외에는 전부 리워드 테이블 참조 
                    iconPath = DataboxController.GetDataString(Table.Reward, Sheet.reward, reward.Type, "icon");
                } break;
            }
            
            _itemSlot.Init(iconPath, slotPath, "", reward.Count.ToString());
            
            // 원래 이런 건, 그래픽 단에서 일일히 사이즈를 맞춰야 하지만, 
            // 스케일 맞추겠답시고 모든 이미지 다 바꾸는 건 좀 아닌 것 같애서, 걍 코드로 조절한다 
            _itemSlot.SetIconScale(scale);
        }
        
        public void ShowAnim()
        {
            if (_tween != null) _tween.DORestart();
        }
    }
}