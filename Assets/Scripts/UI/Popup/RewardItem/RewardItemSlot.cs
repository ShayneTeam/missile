﻿using System;
using InGame;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class RewardItemSlot : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private Image slot;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI amountText;


        public void Init(string iconPath, string slotPath, string nameKey, string amountStr)
        {
            // 아이콘
            if (icon != null)
                icon.sprite = AtlasController.GetSprite(iconPath);
            
            // 슬롯 
            if (slot != null)
                slot.sprite = AtlasController.GetSprite(slotPath);
            
            // 이름
            if (nameText != null)
                nameText.text = Localization.GetText(nameKey);
            
            // 수량
            if (amountText != null)
                amountText.text = amountStr;
        }

        public void SetIconScale(float scale)
        {
            icon.rectTransform.localScale = Vector3.one * scale;
        }
    }
}