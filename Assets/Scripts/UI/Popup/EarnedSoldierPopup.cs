﻿using System.Collections.Generic;
using Global;
using InGame;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class EarnedSoldierPopup : MonoBehaviour
    {
        [SerializeField] private Image soldierImage;
        [SerializeField] private TextMeshProUGUI soldierName;

        public void Init(string earnedSoldierType)
        {
            // type과 grade가 일치하는 녀석 뽑아오기
            const int initGrade = 1;   // grade 1 버전의 name, image 사용
            Soldier.GetGradeIdx(earnedSoldierType, initGrade, out var foundSoldierGradeIdx);

            // 이미지
            var soldierIdx = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, foundSoldierGradeIdx, "soldier_1_idx");
            var resourceId = DataboxController.GetDataString(Table.Soldier, Sheet.Soldier, soldierIdx, "resource_1_id");
            var sprite = AtlasController.GetSprite(resourceId);
            soldierImage.SetSpriteWithOriginSIze(sprite);

            // 이름
            var soldierNameKey = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, foundSoldierGradeIdx, "name");
            soldierName.text = Localization.GetText(soldierNameKey);
        }
    }
}