﻿using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using UI.Popup.Menu.WorldMap;
using UnityEngine;
using Utils;

namespace UI.Popup
{
    public class RaidButton : MonoBehaviour
    {
        [SerializeField] private ImageMaterialSwapper imageMaterial;
        [SerializeField] private GameObject effect;
        
        private void Start()
        {
            //
            Refresh();
            
            //
            UserData.My.Stage.OnOwnPlanet += OnOwnPlanet;
            UserData.My.Items.OnChangedItem += Refresh;
        }

        private void OnDestroy()
        {
            //
            UserData.My.Stage.OnOwnPlanet -= OnOwnPlanet;
            UserData.My.Items.OnChangedItem -= Refresh;
        }

        private void OnOwnPlanet()
        {
            Refresh();
        }

        [PublicAPI]
        public void OnClick()
        {
            if (!RaidController.IsUnlocked)
            {
                // 메시지
                MessagePopup.Show("info_391");

                return;
            }
            
            RaidPopup.Show();
        }

        private void Refresh()
        {
            // 행성조건 달성 여부에 따라 회색 메테리얼 적용 
            var isUnlocked = RaidController.IsUnlocked;
            imageMaterial.Init(isUnlocked);
            
            // 티켓 보유하고 있다면 레드닷
            var hasTicket = RaidController.RaidTicket.HasTicket;
            // 무료 티켓있다면 레드닷
            var freeTicket = RaidController.RaidTicket.RequiredGemToBuy == 0;
            effect.SetActive(isUnlocked && (hasTicket || freeTicket));
        }
    }
}