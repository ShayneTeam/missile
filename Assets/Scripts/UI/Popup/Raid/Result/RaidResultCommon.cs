﻿using System;
using System.Collections.Generic;
using Doozy.Engine.UI;
using Global.Extensions;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;

namespace UI.Popup
{
    public class RaidResultCommon : MonoBehaviour
    {
        /*
         * 보유 정보  
         */
        [SerializeField] private TextMeshProUGUI ownedTicketText;
        [SerializeField] private TextMeshProUGUI ownedUraniumText;
        
        /*
         * 버튼 
         */
        [SerializeField] private GameObject exitBtn;
        [SerializeField] private GameObject rejoinBtn;
        
        /*
         * 카운트 다운
         */
        [SerializeField] private TextMeshProUGUI countDownTextExit;
        [SerializeField] private TextMeshProUGUI countDownTextRejoin;
        [Header("카운트 다운 (초)"), SerializeField] private int countDownTime;
        
        private CoroutineHandle _countDown;
        
        /*
         * 콜백
         */
        private Action _rejoin;
        private Action _exit;
        private bool _owner;


        public void Init(bool owner, Action exit, Action rejoin)
        {
            _owner = owner;
            // 오너 테스트 
            //if (ExtensionMethods.IsUnityEditor()) _owner = false;
            _exit = exit;
            _rejoin = rejoin;
            
            // 보유 티켓 
            ownedTicketText.text = Localization.GetFormatText("raid_024", RaidController.RaidTicket.Count.ToLookUpString());
            ownedTicketText.color = RaidController.RaidTicket.HasTicket ? Color.white : Color.red;
            
            // 보유 우라늄
            ownedUraniumText.text = RewardType.GetString(RewardType.REWARD_GEM, UserData.My.Resources.uranium);
            
            // 카운트다운 텍스트 초기화 
            ResetCountDownText();
        }

        private void OnEnable()
        {
            // 방장이면, 재참여 버튼만 활성화 
            if (_owner)
            {
                exitBtn.SetActive(false);
                rejoinBtn.SetActive(true);
                
                // 카운트 다운 시작 (CancelWith 땜시, OnEnable 이후에 해야 함) 
                _countDown = Timing.RunCoroutine(_CountDownRejoin().CancelWith(gameObject));
            }
            else
            {
                var hasTicket = RaidController.RaidTicket.HasTicket;
                
                // 방장 아닐 시, 재참여 버튼은 티켓있을 때만 보임
                exitBtn.SetActive(true);
                rejoinBtn.SetActive(hasTicket);
                
                // 카운트 다운 시작 (CancelWith 땜시, OnEnable 이후에 해야 함) 
                // 티켓 있으면, 자동 재참여로 유도
                // 티켓 없으면, 자동 나가기로 유도 
                _countDown = Timing.RunCoroutine(hasTicket ? _CountDownRejoin().CancelWith(gameObject) : _CountDownExit().CancelWith(gameObject));
            }
        }

        [PublicAPI]
        public void Exit()
        {
            // 콜백 호출
            _exit?.Invoke();
            
            // 눌리자마자 카운트다운 종료
            StopCountDown();
        }
        
        [PublicAPI]
        public void Rejoin()
        {
            // 콜백 호출
            _rejoin?.Invoke();
            
            // 눌리자마자 카운트다운 종료
            StopCountDown();
        }

        private IEnumerator<float> _CountDownExit()
        {
            // 카운트 다운
            var endTime = Time.time + countDownTime;
            while (Time.time < endTime)
            {
                var remainTime = endTime - Time.time;

                countDownTextExit.text = Localization.GetFormatText("raid_048", Mathf.CeilToInt(remainTime).ToLookUpString());
                
                yield return Timing.WaitForOneFrame;
            }

            // 이때까지 버튼 입력 없었으면 자동 나가기  
            Exit();
        }
        
        private IEnumerator<float> _CountDownRejoin()
        {
            // 카운트 다운
            var endTime = Time.time + countDownTime;
            while (Time.time < endTime)
            {
                var remainTime = endTime - Time.time;

                countDownTextRejoin.text = Localization.GetFormatText("raid_058", Mathf.CeilToInt(remainTime).ToLookUpString());
                
                yield return Timing.WaitForOneFrame;
            }

            // 이때까지 버튼 입력 없었으면 자동 재참여  
            Rejoin();
        }

        private void StopCountDown()
        {
            // 코루틴 종료 
            Timing.KillCoroutines(_countDown);
            
            // 카운트다운 안 보이기  
            ResetCountDownText();
        }

        private void ResetCountDownText()
        {
            countDownTextExit.text = "";
            countDownTextRejoin.text = "";
        }
    }
}