﻿using System;
using System.Linq;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils.UI;

namespace UI.Popup
{
    public class RaidVictoryPopup : UIPopupCommon
    {
        [SerializeField] RaidResultCommon common;
        [SerializeField] private IconNameCount[] rewards;
        [SerializeField] private PrincessTreasureSlot[] treasureSlots;
        [SerializeField] private GameObject rewardDoubleText;
        
        private float _standardHeightSize;
        
        
        public static UIPopup Show(bool owner, Action exit, Action rejoin, string rewardIdx, string rewardGirlIdx, int difficulty, bool canDouble)
        {
            // 보여주기
            var uiPopup = UIPopup.GetPopup("Raid Victory");
            uiPopup.Show();
        
            // 결과 공통
            var popup = uiPopup.GetComponent<RaidVictoryPopup>();
            popup.common.Init(owner, exit, rejoin);
            
            // 승리 보상 (일반)
            popup.SetRewards(rewardIdx, difficulty, canDouble);
            
            // 승리 보상 (공주)
            popup.SetRewardsTreasure(rewardGirlIdx, canDouble);

            // 두 배!
            popup.rewardDoubleText.SetActive(canDouble);
            
            return uiPopup;
        }

        private void SetRewards(string rewardIdx, int difficulty, bool canDouble)
        {
            if (_standardHeightSize == 0f) // Awake 타이밍보다 빨리 들어오므로, 레이지 초기화
                _standardHeightSize = rewards[0].icon.sprite.rect.size.y; // 초기 사이즈 기준을 Height로 맞춤 
            
            var icon = DataboxController.GetDataString(Table.Raid, Sheet.reward, rewardIdx, Column.icon);
            var count = RaidController.GetRewardCount(rewardIdx, difficulty);
            var nameKey = DataboxController.GetDataString(Table.Raid, Sheet.reward, rewardIdx, Column.name);
            var type = DataboxController.GetDataString(Table.Raid, Sheet.reward, rewardIdx, Column.reward_type);
            
            var sprite = AtlasController.GetSprite(icon);
            
            var rewardName = Localization.GetText(nameKey);
            var rewardCountStr = Localization.GetFormatText("raid_024", RewardType.GetString(type, count));
            
            // 먼저 다 꺼두기 
            foreach (var reward in rewards)
                reward.gameObject.SetActive(false);
            
            // 선택적으로 켜기 
            var maxCount = canDouble ? 2 : 1;
            for (var i = 0; i < maxCount; i++)
            {
                var reward = rewards[i];
                reward.gameObject.SetActive(true);
                
                // 아이콘
                reward.icon.SetSpriteWithOriginSIze(sprite);
                
                var revisionScale = _standardHeightSize / sprite.rect.size.y; // 어떤 스프라이트가 들어와도, 기준 사이즈에 맞추는 스케일 값 계산  
                reward.icon.rectTransform.localScale = Vector3.one * revisionScale;
                
                // 이름 
                reward.nameText.text = rewardName;
                
                // 카운트
                reward.countText.text = rewardCountStr;
            }
        }
        
        private void SetRewardsTreasure(string rewardGirlIdx, bool canDouble)
        {
            var rewardCount = DataboxController.GetDataInt(Table.Raid, Sheet.reward_girl, rewardGirlIdx, Column.reward_count);
            var treasureIdx = DataboxController.GetDataString(Table.Raid, Sheet.reward_girl, rewardGirlIdx, Column.reward_idx);
            
            // 먼저 다 꺼두기 
            foreach (var treasureSlot in treasureSlots)
                treasureSlot.gameObject.SetActive(false);
            
            // 선택적으로 켜기 
            var maxCount = canDouble ? 2 : 1;
            for (var i = 0; i < maxCount; i++)
            {
                var treasureSlot = treasureSlots[i];
                treasureSlot.gameObject.SetActive(true);
                
                // 세팅 
                treasureSlot.Init(treasureIdx, rewardCount);
            }
        }
    }
}