﻿using Global;
using InGame;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class PrincessTreasureSlot : MonoBehaviour
    {
        [SerializeField] private Image slot;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI countText;

        public void Init(string treasureIdx, int count)
        {
            // 아이콘 
            var iconName = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.resource_id);
            icon.sprite = AtlasController.GetSprite(iconName);
            
            // 슬롯 
            var slotPath = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.slot_id);
            slot.sprite = AtlasController.GetSprite(slotPath);
            
            // 이름 
            nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.name)); 
            
            // 카운트 
            countText.text = Localization.GetFormatText("info_136", count.ToLookUpString());
        }
    }
}