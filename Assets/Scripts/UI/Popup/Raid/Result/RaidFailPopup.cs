﻿using System;
using Doozy.Engine.UI;
using InGame.Controller;
using TMPro;
using UnityEngine;

namespace UI.Popup
{
    public class RaidFailPopup : UIPopupCommon
    {
        [SerializeField] private TextMeshProUGUI beforeText;
        [SerializeField] private TextMeshProUGUI afterText;
        
        [SerializeField] RaidResultCommon common;
        
        public static UIPopup Show(bool owner, Action exit, Action rejoin)
        {
            // 보여주기
            var uiPopup = UIPopup.GetPopup("Raid Fail");
            uiPopup.Show();
        
            // 결과 공통
            var popup = uiPopup.GetComponent<RaidFailPopup>();
            popup.common.Init(owner, exit, rejoin);
            
            // 이전 티켓 
            popup.beforeText.text = (RaidController.RaidTicket.Count - 1).ToLookUpString();
            
            // 현재 티켓 
            popup.afterText.text = RaidController.RaidTicket.Count.ToLookUpString();

            return uiPopup;
        }
    }
}