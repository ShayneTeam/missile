﻿using System;
using System.Collections.Generic;
using Doozy.Engine.UI;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global.Buff;
using JetBrains.Annotations;
using MEC;
using Server;
using TMPro;
using UnityEngine;

namespace UI.Popup
{
    public class RaidPopup : UIPopupCommon
    {
        /*
         * UI
         */
        [SerializeField] private TextMeshProUGUI difficultyText;
        
        [SerializeField] private TMP_InputField roomNumberInputField;
        
        [SerializeField] private TextMeshProUGUI createTicketText;
        [SerializeField] private TextMeshProUGUI joinTicketText;
        
        [SerializeField] private TextMeshProUGUI ownedTicketText;
        [SerializeField] private TextMeshProUGUI ticketUraniumText;
        
        
        /*
         * 코루틴
         */
        private CoroutineHandle _create;
        private CoroutineHandle _join;

        /*
         * 함수
         */
        public static void Show()
        {
            UIPopup.GetPopup("Raid").Show();
        }
        
        [PublicAPI]
        public void OnShow()
        {
            // BGM
            SoundController.Instance.PlayBGM("bgm_raid_robby");
            
            // 인게임 사운드 이펙트 음소거 설정
            SoundController.Instance.MuteBus("InGame");
            
            // 레이드 서버 접속 중이 아니라면, 접속 
            if (!ServerRaid.IsConnected()) ServerRaid.Connect();

            // 갱신
            Init();
            
            // 수동 GC
            GCController.CollectFull();
            
            // 로그
            WebLog.Instance.ThirdPartyLog("raid_show");
            
            // 서버레이드 연결 해지 이벤트 해지
            ServerRaid.OnDisconnected += OnDisconnected;
        }

        [PublicAPI]
        public void OnHide()
        {
            // BGM (씬 진입하지 않았을 때만 롤백)
            if (!RaidController.HasEnteredToScene)
                SoundController.Instance.RollbackBGM();

            // 인게임 사운드 이펙트 음소거 해제 
            SoundController.Instance.UnmuteBus("InGame");
            
            // 방 번호 텍스트 리셋 
            roomNumberInputField.text = "";
            
            // 수동 GC
            GCController.CollectFull();

            // 로그
            WebLog.Instance.ThirdPartyLog("raid_hide");
            
            // 서버레이드 연결 해지 이벤트 해지
            ServerRaid.OnDisconnected -= OnDisconnected;
        }

        private void OnDisconnected()
        {
            // 방 생성 혹은 입장 중일 때 서버 끊기면, 여기서 처리해줘야 함
            // 따로 Failed로 처리되지 않음 
            if (!_join.IsRunning && !_create.IsRunning) return;
            
            // 무한 뻉글이 일어나지 않도록 함
            LoadingIndicator.Hide();
                
            // 연결 끊김 메시지 
            MessagePopup.Show("raid_056");
        }

        private void Init()
        {
            // 갱신 
            Refresh();
        }

        private void Refresh()
        {
            // 난이도
            difficultyText.text = UserData.My.Raid.DifficultyCursor.ToLookUpString();
            
            // 티켓 
            var hasTicket = RaidController.RaidTicket.HasTicket;

            // 티켓 칼라
            var ticketColor = hasTicket ? Color.white : Color.red;
            createTicketText.color = ticketColor;
            joinTicketText.color = ticketColor;
            
            // 보유 티켓 
            ownedTicketText.text = Localization.GetFormatText("raid_024", RaidController.RaidTicket.Count.ToLookUpString());
            
            // 티켓 우라늄 비용 
            var requiredGem = RaidController.RaidTicket.RequiredGemToBuy;
            var uraniumText = requiredGem == 0 ? Localization.GetText("raid_026") : requiredGem.ToLookUpString();
            ticketUraniumText.text = uraniumText;
        }

        [PublicAPI]
        public void CreateRoom()
        {
            // 티켓 체크 
            if (!RaidController.RaidTicket.HasTicket)
            {
                // 티켓이 부족합니다
                MessagePopup.Show("raid_017");
                return;
            }
            
            // 방 생성 
            Timing.KillCoroutines(_create);
            _create = Timing.RunCoroutine(_CreateRoom().CancelWith(gameObject));
        }

        private IEnumerator<float> _CreateRoom()
        {
            // 로딩 보여주기 
            LoadingIndicator.Show();
            
            // 방 생성 (비동기)
            var isSuccess = false;
            var msg = "";
            yield return Timing.WaitUntilDone(ServerRaid._CreateRoom((success, message) => { 
                isSuccess = success;
                msg = message;
            }));

            if (isSuccess)
            {
                EnterRaidScene();
            }
            else
            {
                RaidController.ShowFailedMessage(msg);
            }
            
            // 로딩 가리기 
            LoadingIndicator.Hide();
        }

        private void EnterRaidScene()
        {
            // 씬 이동
            RaidController.EnterRaidScene();

            // 팝업 닫기 
            Popup.Hide();
        }

        [PublicAPI]
        public void BuyTicket()
        {
            // 티켓 구매
            var result = RaidController.RaidTicket.BuyTicket();
            switch (result)
            {
                case RaidController.FailedBuyReason.NotFailed:
                    // 갱신 
                    Refresh();
                    break;
                default:
                    RaidController.RaidTicket.ShowFailedBuyTicketMessage(result);
                    break;
            }
        }

        [PublicAPI]
        public void JoinRoom()
        {
            // 티켓 체크 
            if (!RaidController.RaidTicket.HasTicket)
            {
                // 티켓이 부족합니다
                MessagePopup.Show("raid_017");
                return;
            }
            
            // 방 입장 
            Timing.KillCoroutines(_join);
            _join = Timing.RunCoroutine(_JoinRoom().CancelWith(gameObject));
        }

        private IEnumerator<float> _JoinRoom()
        {
            // 로딩 보여주기 
            LoadingIndicator.Show();
            
            // 방 참여 (비동기)
            var roomNum = roomNumberInputField.text;
            var isSuccess = false;
            var msg = "";
            yield return Timing.WaitUntilDone(ServerRaid._JoinRoom(roomNum, (success, reason) =>
            {
                isSuccess = success;
                msg = reason;
            }));
            
            switch (msg)
            {                
                case "success":
                {
                    EnterRaidScene();
                }
                    break;
                default:
                    RaidController.ShowFailedMessage(msg);
                    break;
            }
            
            // 로딩 가리기 
            LoadingIndicator.Hide();
        }

        [PublicAPI]
        public void OnClickDifficultyUp()
        {
            // 다음 난이도 세팅
            RaidController.AddDifficultyCursor();
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");

            // UI 갱신
            Refresh();
        }

        [PublicAPI]
        public void OnClickDifficultyDown()
        {
            // 이전 난이도로 세팅
            RaidController.SubDifficultyCursor();
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");

            // UI 갱신
            Refresh();
        }
    }
}