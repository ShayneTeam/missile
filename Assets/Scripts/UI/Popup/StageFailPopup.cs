﻿using System;
using System.Collections.Generic;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup.Menu;
using UI.Popup.Menu.Shop;
using UnityEngine;

namespace UI.Popup
{
    [RequireComponent(typeof(UIPopup))]
    public class StageFailPopup : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI firstStageText;
        [SerializeField] private TextMeshProUGUI retryStageText;
        [SerializeField] private TextMeshProUGUI countDownText;
        [Header("카운트 다운 (초)"), SerializeField] private int countDownTime;

        private UIPopup _popup;
        private CoroutineHandle _countDown;
        private CoroutineHandle _retry;


        public static UIPopup Show()
        {
            var popup = UIPopup.GetPopup("Stage Fail");
            popup.Show();

            return popup;
        }
        

        private void Awake()
        {
            _popup = GetComponent<UIPopup>();
        }

        [PublicAPI]
        public void OnShow()
        {
            // 스테이지 텍스트 설정 
            firstStageText.text = Localization.GetFormatText("info_173", GetFirstStage().ToLookUpString());
            retryStageText.text = Localization.GetFormatText("info_173", GetFinalizedRetryStage().ToLookUpString());
            
            // 카운트 다운 시작 
            _countDown = Timing.RunCoroutine(_CountDown().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHide()
        {
            // 팝업 닫히면 카운트 다운 자동 종료
            StopCountDown();
        }

        private IEnumerator<float> _CountDown()
        {
            // 카운트 다운
            var endTime = Time.time + countDownTime;
            while (Time.time < endTime)
            {
                var remainTime = endTime - Time.time;

                countDownText.text = Localization.GetFormatText("info_326", Mathf.CeilToInt(remainTime).ToLookUpString());
                
                yield return Timing.WaitForOneFrame;
            }

            // 이 떄까지 버튼 입력 없었으면 자동 스테이지 재시작  
            yield return Timing.WaitUntilDone(_RetryStage());
        }

        private void StopCountDown()
        {
            Timing.KillCoroutines(_countDown);
        }

        [PublicAPI]
        public void OpenShop()
        {
            // 스테이지 재시도 처리 
            RetryStage();
            
            // 상점 오픈
            var menu = InGameMenu.Open(InGameMenuType.ShopMenu);
            menu.GetComponent<ShopMenuPopup>().ChangeTab(ShopTab.Bazooka);
        }

        [PublicAPI]
        public void MoveFirstStage()
        {
            // 행성의 첫 스테이지로 이동  
            var firstStage = GetFirstStage();
            if (!Stage.Instance.MoveStage(firstStage)) return;
            
            // 팝업 자동 닫기 
            _popup.Hide();
            
            // Hide() 되면, 그 다음 프레임에 OnHide() 바로 호출되면서 카운트 다운 바로 끝나야 함 
            // 허나 배속 모드 일 경우, 버튼 눌린 프레임에 카운트 다운 처리도 되면서, 중복 더블 이동 될 떄가 있음 
            // 그러지 않도록, 한 번 처리 되면, 카운트 다운 즉각 종료 함 
            StopCountDown();
        }

        [PublicAPI]
        public void RetryStage()
        {
            Timing.KillCoroutines(_retry);
            _retry = Timing.RunCoroutine(_RetryStage().CancelWith(gameObject));
        }

        private IEnumerator<float> _RetryStage()
        {
            // 이동 
            yield return Timing.WaitUntilDone(_MoveRetryStage());

            // 팝업 자동 닫기 
            _popup.Hide();

            // Hide() 되면, 그 다음 프레임에 OnHide() 바로 호출되면서 카운트 다운 바로 끝나야 함 
            // 허나 배속 모드 일 경우, 버튼 눌린 프레임에 카운트 다운 처리도 되면서, 중복 더블 이동 될 떄가 있음 
            // 그러지 않도록, 한 번 처리 되면, 카운트 다운 즉각 종료 함
            StopCountDown();
        }

        public static IEnumerator<float> _MoveRetryStage()
        {
            var finalizedRetryStage = GetFinalizedRetryStage();
            
            // MoveStage는 실패 가능성이 있으므로, 성공할 때까지 주기적으로 계속 호출
            while (!Stage.Instance.MoveStage(finalizedRetryStage)) yield return Timing.WaitForSeconds(1f);
        }

        private static int GetFirstStage()
        {
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out _, out var currentPlanetIdx);
            return InGameControlHub.My.StageController.GetFirstStage(currentPlanetIdx);
        }

        private static int GetFinalizedRetryStage()
        {
            // 지정 양만큼 빼주기 
            var rollbackAmount = int.Parse(DataboxController.GetConstraintsData(Constraints.BATTLE_DEFEAT_MOVE_STAGE));
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var currentStage, out var currentPlanetIdx);
            var retryStage = currentStage - rollbackAmount;

            // 행성 첫 스테이지 미만으로 떨어지지 않게 보정 
            StageControl.GetStageMinMaxData(currentPlanetIdx, out var stageMin, out _);
            return Math.Max(retryStage, stageMin);
        }
    }
}