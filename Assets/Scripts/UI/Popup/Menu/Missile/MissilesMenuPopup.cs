﻿using System;
using System.Linq;
using Global;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using UI.Popup.Menu.Button;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class MissilesMenuPopup : MonoBehaviour
    {
        [SerializeField] private MissileInfo info;
        [SerializeField] private MissilesScroll missiles;
        [SerializeField] private SkillsList skills;

        [SerializeField] private MissileEquipButton equipButton;
        [SerializeField] private MissileAncientSellButton sellButton;

        public string SelectedMissileIdx { get; private set; }
        private bool _isInit;

        [PublicAPI]
        public void OnShow()
        {
            RegisterEvents();
        }

        [PublicAPI]
        public void OnHide()
        {
            UnregisterEvents();
        }

        private void OnDestroy()
        {
            // EnterPlayMode
            UnregisterEvents();
        }

        private void RegisterEvents()
        {
            // 업그레이드, 레벨업 이벤트 등록 
            UserData.My.Missiles.OnEnhance += Refresh;
            UserData.My.Missiles.OnLevelUp += Refresh;
            UserData.My.Missiles.OnEquip += Refresh;
            MissileControl.OnSell += InternalRefresh;
            MissileControl.OnSynthesize += OnSynthesized;
        }

        private void UnregisterEvents()
        {
            // 업그레이드, 레벨업 이벤트 해제 
            UserData.My.Missiles.OnEnhance -= Refresh;
            UserData.My.Missiles.OnLevelUp -= Refresh;
            UserData.My.Missiles.OnEquip -= Refresh;
            MissileControl.OnSell -= InternalRefresh;
            MissileControl.OnSynthesize -= OnSynthesized;
        }

        [PublicAPI]
        public void Init()
        {
            // 미사일 정보
            var list = InGameControlHub.My.MissileController.AllIdxesEquippedWithoutAncient;
            // 그럴 일은 없겠지만, 혹여나 미사일 장착이 0개라면 종료
            if (!list.Any()) return;

            // 미사일 리스트 
            if (!_isInit)
            {
                var firstEquippedMissileIdx = list.First();
                
                missiles.Init(SelectMissile, IsSelectedMissile);

                // 스킬 리스트 
                skills.Init();
            
                // 장착 버튼 
                SelectMissile(firstEquippedMissileIdx);

                _isInit = true;
            }
            else
            {
                InternalRefresh();
            }
        }

        private void SelectMissile(string missileIdx)
        {
            SelectedMissileIdx = missileIdx;
            
            // 인포 갱신 
            info.Init(missileIdx);
            
            // 미사일 스크롤 갱신
            missiles.Refresh();
            
            // 장착 버튼 & 판매 버튼 갱신
            var rarity = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.rarity);
            var isAncient = rarity == MissileRarity.ancient.ToEnumString();
            equipButton.Init(missileIdx);
            equipButton.gameObject.SetActive(!isAncient);
            sellButton.gameObject.SetActive(isAncient);
        }

        private bool IsSelectedMissile(string missileIdx)
        {
            return SelectedMissileIdx == missileIdx;
        }

        private void Refresh(string _)
        {
            InternalRefresh();
        }

        private void OnSynthesized(int count)
        {
            InternalRefresh();
        }

        private void InternalRefresh()
        {
            info.Init(SelectedMissileIdx);

            missiles.Refresh();

            skills.Init();
        }

        [PublicAPI]
        public void OnClickBatchSynthesis()
        {
            YesOrNoPopup.Show("info_225", "info_226", "info_227", "info_082",
                () =>
                {
                    // 일괄 합성 
                    var results = InGameControlHub.My.MissileController.BatchSynthesize();
                    if (results.Count != 0)
                    {
                        // 일괄 합성 결과 팝업 
                        ShowRewardsPopup.Show(results, "info_225");
                    }
                    else
                    {
                        // 실패 메시지
                        MessagePopup.Show("info_302");
                    }
                }, 
                () => {} 
            );
        }
    }
}