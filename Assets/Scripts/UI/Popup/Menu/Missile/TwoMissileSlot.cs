﻿using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace UI.Popup.Menu
{
    /*
     * 미사일 슬롯 2개를 들고 있는 역할만 한다 
     */
    public class TwoMissileSlot : EnhancedScrollerCellView
    {
        [SerializeField] public MissileSlot slot1;
        [SerializeField] public MissileSlot slot2;
        
        // Init은 스크롤에서 직접 호출시켜 처리 
        // Refresh만 따로 처리 
        public override void RefreshCellView()
        {
            slot1.Refresh();
            slot2.Refresh();
        }
    }
}