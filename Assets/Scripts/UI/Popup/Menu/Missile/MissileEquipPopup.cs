﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using Firebase.Analytics;
using InGame.Controller;
using InGame.Data;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class MissileEquipPopup : UIPopupCommon
    {
        [SerializeField] private MissileSlot selectedMissileSlot;

        [SerializeField] private List<MissileSlot> equippedMissileSlots;
        [SerializeField] private Transform ancientMissileSlotsParent;
        private MissileSlot[] _ancientMissileSlots;

        private string _selectedMissileIdx;

        public static MissileEquipPopup Show(string selectedMissileIdx)
        {
            var popup = UIPopup.GetPopup("Missile Equip");

            var equipPopup = popup.GetComponent<MissileEquipPopup>();
            equipPopup.Init(selectedMissileIdx);
            
            popup.Show();

            return equipPopup;
        }

        private void Init(string selectedMissileIdx)
        {
            _selectedMissileIdx = selectedMissileIdx;
            
            // 선택 미사일 
            selectedMissileSlot.Init(selectedMissileIdx/*, false*/);
            
            // 장착된 미사일 리스트 
            var allIdxesEquipped = InGameControlHub.My.MissileController.AllIdxesEquippedWithoutAncient;

            for (var i = 0; i < equippedMissileSlots.Count; i++)
            {
                var slot = equippedMissileSlots[i];
                
                // 장착되지 않은 여분의 미사일 슬롯은 보여주지 않음 
                var activeSlot = i < allIdxesEquipped.Count();
                
                slot.gameObject.SetActive(activeSlot);
                
                // 안 보여질 슬롯이라면 세팅하지 않음 
                if (!activeSlot)
                    continue;;
                
                // 슬롯 초기화
                var missileIdx = allIdxesEquipped.ElementAt(i);
                slot.Init(missileIdx);
                slot.Select = OnClickedSlot;
                slot.IsSelected = null;
            }
            
            // 고대 미사일 리스트  
            _ancientMissileSlots ??= ancientMissileSlotsParent.GetComponentsInChildren<MissileSlot>();
                
            var allAncient = InGameControlHub.My.MissileController.AllIdxesAncient;
            for (var i = 0; i < _ancientMissileSlots.Length; i++)
            {
                var slot = _ancientMissileSlots[i];
                var ancientIdx = allAncient.ElementAt(i);
                slot.Init(ancientIdx);
            }
        }

        private void OnClickedSlot(string clickedMissileIdx)
        {
            // 미사일 장착
            InGameControlHub.My.MissileController.Equip(_selectedMissileIdx, clickedMissileIdx);

            WebLog.Instance.AllLog("missile_equipment_change", new Dictionary<string, object>{{"disrobe_missile_Idx",clickedMissileIdx},{"equip_missile_idx",_selectedMissileIdx}});

            // 팝업 닫기 
            Popup.Hide();
        }
    }
}