﻿using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class MissileAncientSellPopup : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI uraniumText;
        
        private string _missileIdx;

        public static MissileAncientSellPopup Show(string missileIdx)
        {
            // 생성
            var popup = UIPopup.GetPopup("Ancient Missile Sell");
            popup.Show();

            // 초기화
            var sellPopup = popup.GetComponent<MissileAncientSellPopup>();
            sellPopup._missileIdx = missileIdx;
            
            // 우라늄 세팅 
            sellPopup.uraniumText.text = $"x {MissileControl.GetSellUranium(missileIdx).ToLookUpString()}";

            // 반환
            return sellPopup;
        }

        [PublicAPI]
        public void Sell()
        {
            if (InGameControlHub.My.MissileController.SellAncient(_missileIdx))
            {
                MessagePopup.Show("info_382", MissileControl.GetSellUranium(_missileIdx).ToLookUpString());
            }
        }
    }
}