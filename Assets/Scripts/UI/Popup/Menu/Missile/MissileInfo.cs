﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bolt;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UI.Popup.Menu.Button;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu
{
    public class MissileInfo : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private Image image;
        [SerializeField] private TextMeshProUGUI rarityText;

        [SerializeField] private Image[] grades;
        [SerializeField] private TextMeshProUGUI descText;
        
        [SerializeField] private AbilityText[] equippedEffects;
        [SerializeField] private AbilityText[] ownedEffects;

        [SerializeField] private MissileEnhanceButton enhanceButton;
        [SerializeField] private MissileUpgradeButton upgradeButton;
        [SerializeField] private GameObject maxLevelText;

        [SerializeField] private MissileSynthesisButton synthesisButton;

        [Serializable]
        public class GradeSprite
        {
            public string type;
            public Sprite sprite;
        }
        [SerializeField] private List<GradeSprite> gradeSprites;

        private void Start()
        {
            UserData.My.Missiles.OnEnhance += ShowEnhanceEffect;
            UserData.My.Missiles.OnLevelUp += ShowUpgradeEffect;
        }

        private void OnDestroy()
        {
            UserData.My.Missiles.OnEnhance -= ShowEnhanceEffect;
            UserData.My.Missiles.OnLevelUp -= ShowUpgradeEffect;
        }

        private void ShowEnhanceEffect(string _)
        {
            var canvas = UICanvas.GetUICanvas("UI Effect");
            EffectSpawner.Instance.Spawn("upgrade UI effect", image.transform.position, canvas.transform);
        }
        
        private void ShowUpgradeEffect(string _)
        {
            var canvas = UICanvas.GetUICanvas("UI Effect");
            EffectSpawner.Instance.Spawn("enhance UI effect", image.transform.position, canvas.transform);
        }
        
        public void Init(string missileIdx)
        {
            UserData.My.Missiles.GetState(missileIdx, out var count, out var grade, out var level, out _);
            
            // 타이틀
            var nameKey = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.name);
            titleText.text = Localization.GetText(nameKey);
            
            // 레벨
            levelText.text = level.ToLookUpString();
            
            // 아이콘 
            var imagePath = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.resource_id);
            image.sprite = AtlasController.GetSprite(imagePath);
            
            // 등급 
            if (grade > grades.Length) // 10보다 크면 
            {
                for (var i = 0; i < grades.Length; i++)
                {
                    if (i < grade - grades.Length)  
                        grades[i].sprite = gradeSprites.FirstOrDefault(x => x.type == "blue")?.sprite;
                    else
                        grades[i].sprite = gradeSprites.FirstOrDefault(x => x.type == "yellow")?.sprite;
                }
            }
            else // 10보다 작으면
            {
                for (var i = 0; i < grades.Length; i++)
                {
                    if (i < grade)
                        grades[i].sprite = gradeSprites.FirstOrDefault(x => x.type == "yellow")?.sprite;
                    else
                        grades[i].sprite = gradeSprites.FirstOrDefault(x => x.type == "empty")?.sprite;
                }
            }
            
            // 등급 텍스트 
            rarityText.text = Localization.GetText(DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.rarity_name));
            
            // 설명 
            var descKey = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.desc);
            descText.text = Localization.GetText(descKey);
            
            // 착용 효과
            for (var i = 1; i <= equippedEffects.Length; i++)
            {
                var index = i.ToLookUpString();
                SetAbilityText(equippedEffects[i - 1], missileIdx, $"a_aidx_{index}", $"a_aidx_{index}_gain", level);
            }
            
            // 보유 효과 
            for (var i = 1; i <= ownedEffects.Length; i++)
            {
                var index = i.ToLookUpString();
                SetAbilityText(ownedEffects[i - 1], missileIdx, $"b_aidx_{index}", $"b_aidx_{index}_gain", level);
            }
            
            // 업그레이드 & 강화개조 버튼 
            var isMaxGrade = InGameControlHub.My.MissileController.IsMaxGrade(missileIdx);
            var isMaxLevel = InGameControlHub.My.MissileController.IsMaxLevel(missileIdx);
            
            maxLevelText.SetActive(isMaxGrade && isMaxLevel);
            
            upgradeButton.gameObject.SetActive(!isMaxLevel);
            enhanceButton.gameObject.SetActive(!isMaxGrade && isMaxLevel);
            
            upgradeButton.Init(missileIdx);
            enhanceButton.Init(missileIdx);
            
            // 합성 버튼 (마지막 미사일 인덱스만 아니라면 보여줌)
            var isLastMissile = MissileControl.IsLastMissileIdx(missileIdx);
            synthesisButton.gameObject.SetActive(!isLastMissile);
            synthesisButton.Init(missileIdx);
        }

        private static void SetAbilityText(AbilityText abilityText, string missileIdx, string abilityIdxColumn, string weightColumnName, int level)
        {
            MissileControl.GetAbilityState(missileIdx, abilityIdxColumn, weightColumnName, level, out var abilityIdx, out var finalizedValue);
                
            // 텍스트 적용
            abilityText.Init(abilityIdx, finalizedValue);
        }
    }
}