﻿using System;
using Global;
using InGame;
using InGame.Skill;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class SkillSlotBombing : SkillSlot
    {
        protected override void SetDesc()
        {
            // 데미지 보정율   
            var damageModifiedPer = SkillBombingFinder.Other[InDate].Skill.GetDamageModifiedPer();
            
            // 지속시간 
            var durationTimeSec = SkillBombingFinder.Other[InDate].Skill.GetDurationTimeSec();
            
            // 쿨타임 
            var coolTimeSec = SkillBombingFinder.Other[InDate].Skill.GetBaseCoolTimeSec();
            
            // 설명 
            descText.text = string.Format(GetDescFormat(), damageModifiedPer.ToString(), durationTimeSec.ToString(), coolTimeSec.ToString());
        }
    }
}