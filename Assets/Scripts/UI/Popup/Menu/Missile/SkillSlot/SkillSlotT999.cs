﻿using System;
using Global;
using InGame;
using InGame.Data;
using InGame.Skill;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class SkillSlotT999 : SkillSlot
    {
        protected override void SetDesc()
        {
            // 설명 
            descText.text = GetDescText(GetDescFormat());
        }

        public static string GetDescText(string format)
        {
            // 데미지 보정율   
            var damageModifiedPer = SkillT999Finder.Other[UserData.My.InDate].Skill.GetDamageModifiedPer();
            
            // 지속시간 
            var durationTimeSec = SkillT999Finder.Other[UserData.My.InDate].Skill.GetDurationTimeSec();
            
            // 쿨타임 
            var coolTimeSec = SkillT999Finder.Other[UserData.My.InDate].Skill.GetFinalizedCoolTimeSec();
            
            // 설명 
            return string.Format(format, damageModifiedPer.ToString(), durationTimeSec.ToString(), coolTimeSec.ToString());
        }

        protected override string GetLevelText()
        {
            return LevelText();
        }

        public static string LevelText()
        {
            return Localization.GetFormatText("info_145", SkillT999Finder.Other[UserData.My.InDate].Skill.GetSkillLevel().ToLookUpString());
        }
    }
}