﻿using System;
using Global;
using InGame;
using InGame.Skill;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class SkillSlotBulkUp : SkillSlot
    {
        protected override void SetDesc()
        {
            // 크리티컬 확률 보너스 
            var criticalChanceBonus = SkillBulkUp.GetCriticalChanceBonus();
            
            // 크리티컬 데미지 보너스 
            var criticalDamageBonus = SkillBulkUpFinder.Other[InDate].Skill.GetCriticalDamageBonus();
            
            // 지속시간 
            var durationTimeSec = SkillBulkUpFinder.Other[InDate].Skill.GetDurationTimeSec();
            
            // 쿨타임 
            var coolTimeSec = SkillBulkUpFinder.Other[InDate].Skill.GetBaseCoolTimeSec();
            
            // 설명 
            descText.text = string.Format(GetDescFormat(), criticalChanceBonus.ToString(), criticalDamageBonus.ToString(), durationTimeSec.ToString(), coolTimeSec.ToString());
        }
    }
}