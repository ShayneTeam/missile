﻿using System;
using Global;
using InGame;
using InGame.Skill;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class SkillSlotDoubleBazooka : SkillSlot
    {
        protected override void SetDesc()
        {
            // 지속시간 
            var durationTimeSec = SkillDoubleBazookaFinder.Other[InDate].Skill.GetDurationTimeSec();
            var skillDurationTimeBonusSec = SkillDoubleBazookaFinder.Other[InDate].Skill.GetDurationTimeBonusSec();
            var finalizedDurationTimeSec = durationTimeSec + skillDurationTimeBonusSec;
            
            // 쿨타임 
            var coolTimeSec = SkillDoubleBazookaFinder.Other[InDate].Skill.GetBaseCoolTimeSec();
            
            // 설명 
            descText.text = string.Format(GetDescFormat(), finalizedDurationTimeSec.ToString(), coolTimeSec.ToString());
        }
    }
}