﻿using System;
using Global;
using InGame;
using InGame.Skill;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class SkillSlotBigBeautifulMissile : SkillSlot
    {
        protected override void SetDesc()
        {
            // 스킬 레벨 데미지 보정 퍼센트  
            var damageModifiedPer = SkillBigBeautifulMissileFinder.Other[InDate].Skill.GetDamageModifiedPer();
            
            // 쿨타임 
            var coolTimeSec = SkillBigBeautifulMissileFinder.Other[InDate].Skill.GetBaseCoolTimeSec();
            
            // 설명 
            descText.text = string.Format(GetDescFormat(), damageModifiedPer.ToString(), coolTimeSec.ToString());
        }
    }
}