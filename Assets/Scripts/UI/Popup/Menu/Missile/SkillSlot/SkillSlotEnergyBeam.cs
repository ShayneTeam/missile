﻿using System;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using InGame.Skill;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class SkillSlotEnergyBeam : SkillSlot
    {
        private void Start()
        {
            Init();
        }

        public void Refresh()
        {
            SetLevelText();
            
            SetDesc();
        }

        protected override void SetDesc()
        {
            // 설명 
            descText.text = GetDescText(GetDescFormat());
        }

        public static string GetDescText(string format)
        {
            // 데미지
            var damagePer = SkillEnergyBeamFinder.My.Skill.DamagePer;
            
            // 쿨타임 
            var coolTime = SkillEnergyBeamFinder.My.Skill.GetBaseCoolTimeSec();
            
            // 설명 
            return string.Format(format, damagePer.ToString(), coolTime.ToString());
        }

        protected override string GetLevelText()
        {
            return LevelText();
        }

        public static string LevelText()
        { 
            return Localization.GetFormatText("info_145", InGameControlHub.My.PrincessController.Level.AverageOfAllCollected.ToLookUpString());
        }
    }
}