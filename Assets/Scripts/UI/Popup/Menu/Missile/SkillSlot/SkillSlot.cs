﻿using System;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using InGame.Skill;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu
{
    public abstract class SkillSlot : MonoBehaviour
    {
        /*
         * 스킬은 인덱스를 볼트로 넣어 처리 중
         * 스킬슬롯도 일관성 맞추기 위해 에디터에서 인덱스 넣는다 
         */
        [SerializeField] protected string skillIdx;



        [SerializeField] protected Image icon;
        [SerializeField] protected TextMeshProUGUI levelText;
        [SerializeField] protected TextMeshProUGUI nameText;
        [SerializeField] protected TextMeshProUGUI descText;
        
        [SerializeField] protected GameObject lockParent;
        [SerializeField] protected TextMeshProUGUI lockText;
        [SerializeField] protected Image unlockNeedIcon;

        protected string InDate => UserData.My.InDate;
        
        public void Init()
        {
            // 아이콘 
            var iconPath = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, Column.icon);
            icon.sprite = AtlasController.GetSprite(iconPath);
            
            // 레벨
            SetLevelText();
            
            // 이름 
            var nameKey = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, Column.name);
            nameText.text = Localization.GetText(nameKey);
            
            // 설명
            SetDesc();
            
            // 잠금 
            var isLock = !InGameControlHub.Find(transform).SkillController.IsUnlocked(skillIdx);
            lockParent.SetActive(isLock);
            
            // 잠금 설명
            var lockTextKey = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, "lock_tip");
            lockText.text = Localization.GetText(lockTextKey);
            
            // 잠금 해제 필요 아이콘
            var unlockNeedIconPath = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, "lock_tip_icon");
            unlockNeedIcon.sprite = AtlasController.GetSprite(unlockNeedIconPath);
        }

        protected abstract void SetDesc();

        protected string GetDescFormat()
        {
            var descFormatKey = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, Column.desc);
            return Localization.GetText(descFormatKey);
        }

        protected void SetLevelText()
        {
            levelText.text = GetLevelText();
        }

        protected virtual string GetLevelText()
        {
            return Localization.GetFormatText("info_145", InGameControlHub.My.SkillController.GetSkillLevel(skillIdx).ToLookUpString());
        }
    }
}