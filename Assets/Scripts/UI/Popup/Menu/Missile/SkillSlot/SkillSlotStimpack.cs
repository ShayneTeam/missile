﻿using System;
using Global;
using InGame;
using InGame.Skill;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class SkillSlotStimpack : SkillSlot
    {
        protected override void SetDesc()
        {
            // 발사 보너스 퍼센티지   
            var shootCountBonusPer = SkillStimpackFinder.Other[InDate].Skill.GetShootCountBonusPer();
            
            // 지속시간 
            var durationTimeSec = SkillStimpackFinder.Other[InDate].Skill.GetDurationTimeSec();
            
            // 쿨타임 
            var coolTimeSec = SkillStimpackFinder.Other[InDate].Skill.GetBaseCoolTimeSec();
            
            // 설명 
            descText.text = string.Format(GetDescFormat(), shootCountBonusPer.ToString(), durationTimeSec.ToString(), coolTimeSec.ToString());
        }
    }
}