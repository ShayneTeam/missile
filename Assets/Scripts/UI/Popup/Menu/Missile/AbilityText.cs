﻿using Global;
using InGame;
using InGame.Global;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class AbilityText : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI descText;

        public TextMeshProUGUI NameText => nameText;
        public TextMeshProUGUI DescText => descText;

        public void Init(string abilityIdx, float value)
        {
            // 이름
            var nameKey = DataboxController.GetDataString(Table.Ability, Sheet.Ability, abilityIdx, Column.name);
            nameText.text = Localization.GetText(nameKey);
            
            // 설명 
            var descKey = DataboxController.GetDataString(Table.Ability, Sheet.Ability, abilityIdx, Column.desc);
            descText.text = Localization.GetFormatText(descKey, value.ToUnitOrDecimalString());
        }
    }
}