﻿using System;
using System.Diagnostics.CodeAnalysis;
using Bolt;
using DG.Tweening;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu
{
    public delegate void SelectMissileDelegate(string missileIdx);
    public delegate bool IsSelectedMissileDelegate(string missileIdx);

    [SuppressMessage("ReSharper", "UnusedMember.Local")]
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum MissileRarity
    {
        normal,
        magic,
        rare,
        hero,
        legend,
        myth,
        ancient,
    }
    
    public static class MissileRarityExtensions
    {
        public static string ToEnumString(this MissileRarity type)
        {
            return type switch
            {
                MissileRarity.normal => nameof(MissileRarity.normal),
                MissileRarity.magic => nameof(MissileRarity.magic),
                MissileRarity.rare => nameof(MissileRarity.rare),
                MissileRarity.hero => nameof(MissileRarity.hero),
                MissileRarity.legend => nameof(MissileRarity.legend),
                MissileRarity.myth => nameof(MissileRarity.myth),
                MissileRarity.ancient => nameof(MissileRarity.ancient),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }

    public class MissileSlot : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI countText;
        [SerializeField] private Image image;
        [SerializeField] private Image skillIcon;
        [SerializeField] private Image slot;

        [SerializeField] private GameObject raritySlot;
        [SerializeField] private GameObject equipIcon;
        [SerializeField] private GameObject highlight;
        [SerializeField] private GameObject locked;

        [SerializeField] private SlicedFilledImage filledImage;
        
        [NonSerialized] private string _missileIdx;

        public SelectMissileDelegate Select;
        public IsSelectedMissileDelegate IsSelected;

        /*
         * 이런건 볼트로 처리하는게 가장 이상적이나, 볼트와 스크롤러가 궁합이 안 좋아서, 야매로 이렇게 레퍼 잡고 사용 
         */
        [SerializeField] private Sprite[] raritySlotSprites;

        public void Init(string missileIdx)
        {
            _missileIdx = missileIdx;
            
            // 슬롯 
            var rarity = (MissileRarity)Enum.Parse(typeof(MissileRarity), DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.rarity));
            slot.sprite = raritySlotSprites[(int) rarity];

            // 미사일 이미지 
            var imagePath = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.resource_id);
            image.sprite = AtlasController.GetSprite(imagePath);

            // 스킬 아이콘 
            var skillIconPath = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, "skill_icon");
            var skillIconSprite = AtlasController.GetSprite(skillIconPath);
            skillIcon.gameObject.SetActive(skillIconSprite != null);            
            skillIcon.sprite = skillIconSprite;
            
            // 갱신 
            Refresh();
        }

        public void Refresh()
        {
            var isEarned = UserData.My.Missiles.GetState(_missileIdx, out var count, out _, out var level, out var isEquipped);
            
            // 레벨 
            levelText.text = level.ToLookUpString();
            
            // 장착 아이콘 (손)
            equipIcon.SetActive(isEquipped);
            
            // 보유량 바
            var clampedCount = Mathf.Clamp(count, 0, 999);  //999개 초과시 버림
            var needMissileCount = MissileControl.GetNeedMissileCountForSynthesis();
            var ratio = clampedCount / (float)needMissileCount;
            filledImage.fillAmount = ratio;
            
            // 보유 갯수 
            countText.text = Localization.GetFormatText("info_134", clampedCount.ToLookUpString(), needMissileCount.ToLookUpString());

            // 미획득 잠김 
            locked.SetActive(!isEarned);

            // 강조
            highlight.SetActive(IsSelected?.Invoke(_missileIdx) ?? false);
        }

        public void OnClickedSlot()
        {
            // 사운드
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");
            
            // 호출
            Select?.Invoke(_missileIdx);
        }
    }
}