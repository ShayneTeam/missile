﻿using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Global;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class MissilesScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller scroller;
        [SerializeField] private EnhancedScrollerCellView normalCellPrefab;
        [SerializeField] private EnhancedScrollerCellView ancientCellPrefab;
        [SerializeField] private float cellSize;

        private SelectMissileDelegate _select;
        private IsSelectedMissileDelegate _isSelected;

        private ICollection<string> _allIdxesMissiles;

        
        public void Init(SelectMissileDelegate select, IsSelectedMissileDelegate isSelect)
        {
            _select = select;
            _isSelected = isSelect;
            
            _allIdxesMissiles = DataboxController.GetAllIdxes(Table.Missile, Sheet.missile);

            scroller.Delegate = this;
            scroller.ReloadData();
        }
        
        public int GetNumberOfCells(EnhancedScroller _)
        {
            // 한 줄 당 두 개씩 보여지니 / 2 연산
            return _allIdxesMissiles.Count / 2;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            var slot1Index = dataIndex * 2;
            var slot2Index = dataIndex * 2 + 1;
            
            var slot1MissileIdx = _allIdxesMissiles.ElementAt(slot1Index);
            var slot2MissileIdx = _allIdxesMissiles.ElementAt(slot2Index);
            
            // 첫 번째 셀이 고대 등급인지 체크
            var slot1Rarity = DataboxController.GetDataString(Table.Missile, Sheet.missile, slot1MissileIdx, Column.rarity);
            var isAncientSlot = slot1Rarity == MissileRarity.ancient.ToEnumString();
            var cellPrefab = isAncientSlot ? ancientCellPrefab : normalCellPrefab;
            
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellPrefab) as TwoMissileSlot;
            if (cellView == null) return null;

            cellView.slot1.Init(slot1MissileIdx);
            cellView.slot2.Init(slot2MissileIdx);

            // 함수 위임
            cellView.slot1.Select = _select;
            cellView.slot2.Select = _select;
            cellView.slot1.IsSelected = _isSelected;
            cellView.slot2.IsSelected = _isSelected;

            return cellView;
        }

        public void Refresh()
        {
            // 스크롤 갱신
            scroller.RefreshActiveCellViews();
        }
    }
}