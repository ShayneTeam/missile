﻿using System.Collections;
using System.Collections.Generic;
using Bolt.CustomUnits;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using Lean.Pool;
using MEC;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Button
{
    public class MissileSynthesisButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI countText;
        
        [SerializeField] private float autoClickDelay;
        [SerializeField] private float autoClickInterval;

        [SerializeField] private SynthesisEffect effectPrefab;
        [SerializeField] private Transform effectPivot;
        
        private string _missileIdx;
        
        private CoroutineHandle _autoClick;
        
        
        public void Init(string missileIdx)
        {
            _missileIdx = missileIdx;
            
            UserData.My.Missiles.GetState(missileIdx, out var ownedCount, out _, out _, out _);
            
            // 합성 갯수 현황 텍스트  
            var needCount = MissileControl.GetNeedMissileCountForSynthesis();
            var canSynthesize = ownedCount >= needCount;

            var formatKey = canSynthesize ? "info_241" : "info_240";
            countText.text = Localization.GetFormatText(formatKey, ownedCount.ToLookUpString(), needCount.ToLookUpString());
        }

        [PublicAPI]
        public void StartAutoClick()
        {
            StopAutoClick();
            
            _autoClick = Timing.RunCoroutine(_AutoClick().CancelWith(gameObject));
        }

        [PublicAPI]
        public void StopAutoClick()
        {
            if (_autoClick.IsRunning)
                Timing.KillCoroutines(_autoClick);
        }

        private IEnumerator<float> _AutoClick()
        {
            // 들어오자마자 한번 처리 
            Synthesize();
				
            // 딜레이 
            yield return Timing.WaitForSeconds(autoClickDelay);
				
            // 
            while (InGameControlHub.My.MissileController.CanSynthesize(_missileIdx) && enabled)
            {
                Synthesize();

                // 일정 주기로 반복 
                yield return Timing.WaitForSeconds(autoClickInterval);
            }
        }
        
        private void Synthesize()
        {
            if (!InGameControlHub.My.MissileController.Synthesize(_missileIdx, out var resultMissileIdx))
            {
                // 실패 메시지
                MessagePopup.Show("info_302");
                
                return;
            }
            
            // 합성 미사일 이펙트 
            var effect = LeanPool.Spawn(effectPrefab, effectPivot);
            effect.Init(resultMissileIdx);
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_enchant_01");
        }
    }
}
