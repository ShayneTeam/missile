﻿using Bolt;
using InGame;
using InGame.Data;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Button
{
    public class MissileEquipButton : MonoBehaviour
    {
        [SerializeField] private MissilesMenuPopup popup;
        [SerializeField] private TextMeshProUGUI label;

        [SerializeField] private GameObject bg;
        
        public void Init(string missileIdx)
        {
            var isEarned = UserData.My.Missiles.IsEarned(missileIdx);
            
            // 미획득 시 배경 처리는 볼트로 넘김
            CustomEvent.Trigger(bg, "SetBg", isEarned);

            // 라벨 
            label.text = Localization.GetText(isEarned ? "info_137" : "info_139");
        }

        [PublicAPI]
        public void OnClickEquip()
        {
            var isEarned = UserData.My.Missiles.GetState(popup.SelectedMissileIdx, out _, out _, out _, out var isEquipped);
            
            // 획득된 미사일 아닐 시, 무반응
            if (!isEarned) return;
            
            // 이미 장착중이라면 메시지 출력 
            if (isEquipped)
            {
                MessagePopup.Show("info_138");
                return;
            }
                
            // 그외에 미사일 장착팝업 보여주기 
            MissileEquipPopup.Show(popup.SelectedMissileIdx);
        }
    }
} 