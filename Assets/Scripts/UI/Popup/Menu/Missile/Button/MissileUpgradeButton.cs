﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bolt;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Button
{
    public class MissileUpgradeButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI gasText;
        
        [SerializeField] private float autoClickDelay;
        [SerializeField] private float autoClickInterval;
        
        private string _missileIdx;
        
        private CoroutineHandle _autoClick;

        private void Start()
        {
            UserData.My.Resources.OnChangedGas += OnChangedGas;
        }

        private void OnDestroy()
        {
            UserData.My.Resources.OnChangedGas -= OnChangedGas;
        }

        private void OnChangedGas(double value)
        {
            RefreshSlot();
        }

        private void RefreshSlot()
        {
            if (!string.IsNullOrEmpty(_missileIdx))
                CustomEvent.Trigger(gameObject, "SetSlotBg", InGameControlHub.My.MissileController.CanUpgrade(_missileIdx));
        }


        public void Init(string missileIdx)
        {
            _missileIdx = missileIdx;
            
            // 슬롯 배경 (볼트로 넘김)
            RefreshSlot();
            
            // 가스 텍스트 
            gasText.text = InGameControlHub.My.MissileController.GetRequiredGasForUpgrade(_missileIdx).ToUnitString();
        }

        [PublicAPI]
        public void StartAutoClick()
        {
            StopAutoClick();
            
            _autoClick = Timing.RunCoroutine(_AutoClick().CancelWith(gameObject));
        }

        [PublicAPI]
        public void StopAutoClick()
        {
            if (_autoClick.IsRunning)
                Timing.KillCoroutines(_autoClick);
        }

        private IEnumerator<float> _AutoClick()
        {
            // 들어오자마자 한번 처리 
            Upgrade();
				
            // 딜레이 
            yield return Timing.WaitForSeconds(autoClickDelay);
				
            // 
            while (InGameControlHub.My.MissileController.CanUpgrade(_missileIdx) && enabled)
            {
                Upgrade();

                // 일정 주기로 반복 
                yield return Timing.WaitForSeconds(autoClickInterval);
            }
        }
        
        private void Upgrade()
        {
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_upgrade_01");
            
            // 업그레이드 
            InGameControlHub.My.MissileController.Upgrade(_missileIdx);
        }
    }
}
