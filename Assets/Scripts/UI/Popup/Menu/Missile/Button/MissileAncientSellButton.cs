﻿using Global;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using UnityEngine;

namespace UI.Popup.Menu.Button
{
    public class MissileAncientSellButton : MonoBehaviour
    {
        [SerializeField] private MissilesMenuPopup popup;

        [PublicAPI]
        public void OnClickSell()
        {
            // 고대 등급만 판매 가능 
            var rarity = DataboxController.GetDataString(Table.Missile, Sheet.missile, popup.SelectedMissileIdx, Column.rarity);
            var isAncient = rarity == MissileRarity.ancient.ToEnumString();
            if (!isAncient) return;
            
            // 획득된 미사일 아닐 시, 무반응
            var isEarned = UserData.My.Missiles.GetState(popup.SelectedMissileIdx, out var count, out _, out _, out _);
            if (!isEarned || count == 0) 
            {
                // 메시지 
                MessagePopup.Show("info_412");
                return;
            }
            
            // 판매 팝업 띄우기 
            MissileAncientSellPopup.Show(popup.SelectedMissileIdx);
        }
    }
}