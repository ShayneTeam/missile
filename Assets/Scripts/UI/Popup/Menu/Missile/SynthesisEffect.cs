﻿using DG.Tweening;
using Global;
using InGame;
using InGame.Global;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu
{
    public class SynthesisEffect : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TextMeshProUGUI text;

        [SerializeField] private float fadingDuration;
        [SerializeField] private float moveYOffset;

        private Tween _imageTween;
        private Tween _textTween;
        private Tween _moveTween;
        
        public void Init(string missileIdx)
        {
            var imagePath = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.resource_id);
            image.sprite = AtlasController.GetSprite(imagePath);
            
            /*
             * 린풀 재활용으로 인한 트윈 재활용
             */
            if (_imageTween == null)
            {
                _imageTween = image.DOFade(0f, fadingDuration).SetAutoKill(false);
            }
            else
            {
                _imageTween.Restart();
            }
            
            if (_textTween == null)
            {
                _textTween = text.DOFade(0f, fadingDuration).SetAutoKill(false);
            }
            else
            {
                _textTween.Restart();
            }
            
            if (_moveTween == null)
            {
                _moveTween = transform.DOLocalMoveY(moveYOffset, fadingDuration).SetAutoKill(false);
            }
            else
            {
                _moveTween.Restart();
            }
            
            // 자동 반납 
            LeanPool.Despawn(gameObject, fadingDuration);
        }
    }
}