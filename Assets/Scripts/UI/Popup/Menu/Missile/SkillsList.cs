﻿using EnhancedUI.EnhancedScroller;
using UnityEngine;

namespace UI.Popup.Menu
{
    public class SkillsList : MonoBehaviour
    {
        [SerializeField] private Transform slotsParent;
        
        private SkillSlot[] _slots;


        public void Init()
        {
            _slots ??= slotsParent.GetComponentsInChildren<SkillSlot>();

            foreach (var slot in _slots)
            {
                slot.Init();   
            }
        }
    }
}