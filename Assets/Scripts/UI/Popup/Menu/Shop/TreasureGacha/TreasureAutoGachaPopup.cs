﻿using System;
using System.Collections.Generic;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using InGame;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Shop.Gacha
{
    [RequireComponent(typeof(CommonScrollerOwner))]
    public class TreasureAutoGachaPopup : UIPopupCommon, IEnhancedScrollerDelegate
    {
        [SerializeField] private int numberOfCellsPerRow = 10;

        [SerializeField] private Image imageForScrolling;
        
        [Header("자동 가챠"), SerializeField] private UIToggle auto;
        [SerializeField] private float autoDelay;

        /*
         * 멤버 필드 
         */
        private string _idx;
        private string _sheet;
        private List<TreasureGachaData> _results;
        private CoroutineHandle _autoGacha;
        private CommonScrollerOwner _common;
        private bool _hideLineWhenGetNew;

        protected override void Awake()
        {
            base.Awake();

            _common = GetComponent<CommonScrollerOwner>();
        }


        public static void Show(List<TreasureGachaData> results, string idx, string sheet)
        {
            var popup = UIPopup.GetPopup("Treasure Auto Gacha");

            var autoGacha = popup.GetComponent<TreasureAutoGachaPopup>();
            autoGacha._results = results;
            autoGacha._sheet = sheet;
            autoGacha._idx = idx;

            popup.Show();
        }

        [PublicAPI]
        public void OnShowStarted()
        {
            // 요 타이밍에 하는 거 중요
            _hideLineWhenGetNew = true;
            
            // 스크롤 초기화 
            _common.scroller.Delegate = this;
            _common.scroller.ReloadData();
            
            // 토글 초기화 
            auto.IsOn = false;
        }

        [PublicAPI]
        public void OnShowFinished()
        {
            // 연출 시작 (스크롤 초기화 이후에 빈 화면에서 시작해야 함)
            _autoGacha = Timing.RunCoroutine(_AutoGacha().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHideStarted()
        {
            // UI가 Hide 될 시, 안에 있는 것들만 꺼지고, 루트 게임오브젝트는 꺼지지 않아서 코루틴이 종료되질 않음 
            // 그래서 이벤트 받고 수동 종료시킨다 
            Timing.KillCoroutines(_autoGacha);
        }

        [PublicAPI]
        public void OnToggleIsOn()
        {
            // 연출 끝났는데 누른 경우, 강제로 끔
            if (_autoGacha.IsRunning) return;
            auto.IsOn = false;

            // 실패 이유는 무조건 우라늄 부족이라고 가정함 
            ShopController.ShowFailedMessage(ShopController.FailedReason.NotEnoughGem);
        }

        /*
         * Direction
         */
        #region Direction

        private IEnumerator<float> _Direction()
        {
            // 괜히 한 프레임 쉬어주기 
            yield return Timing.WaitForOneFrame;
            
            // 스크롤 못하게 막기 
            imageForScrolling.enabled = false;
            
            // 진행 
            for (var i = 0; i < _results.Count; i++)
            {
                var rowIndex = i / numberOfCellsPerRow;
                
                // 현재 연출되는 로우가 맨 아래에 보이게 스크롤 맞춤  
                _common.scroller.JumpToDataIndex(rowIndex, 1f, 1f);
                
                // 위의 점프가 반영되기 위해 한 프레임 대기
                // (안 기다리면, 미세한 딜레이로 인해 GetCellViewAtDataIndex()가 null이 반환됨
                // 점프가 반영되서 스크롤이 밑으로 내려가야만 GetCellView가 제대로 호출됨) 
                yield return Timing.WaitForOneFrame;
                
                // 
                var row = _common.scroller.GetCellViewAtDataIndex(rowIndex) as TreasureGachaRow;
                if (row != null)
                {
                    var cellIndex = i % numberOfCellsPerRow;
                    row.ShowCell(cellIndex);
                    
                    yield return Timing.WaitUntilDone(row.ShowCellEffect(cellIndex));
                }

                // 간격 대기 
                yield return Timing.WaitForOneFrame;
            }
            
            // 스크롤 풀기
            imageForScrolling.enabled = true;

            // 스크롤해도 라인 자유롭게 볼 수 있게 함 
            _hideLineWhenGetNew = false;
        }

        private IEnumerator<float> _AutoGacha()
        {
            // 파이어베이스 미사일 연속뽑기 트래킹용도
            var firebaseEventTrackingCount = new List<int> { 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
            var autoGachaCount = 1;

            // 일단 첫 연출 실행 
            yield return Timing.WaitUntilDone(_Direction());

            // 예외 처리
            if (string.IsNullOrEmpty(_idx) || string.IsNullOrEmpty(_sheet)) yield break;

            // 자동 가챠 무한 반복 (구매 실패 할 때까지)
            while (true)
            {
                // 딜레이 
                yield return Timing.WaitForSeconds(autoDelay);

                // 오토 꺼졌다면, 켜질 때까지 대기 
                while (!auto.IsOn) yield return Timing.WaitForOneFrame;

                // 재구매
                var isDone = false;
                var isSuccess = false;
                ShopController.Purchase(_idx, _sheet, (success, reason) =>
                {
                    isDone = true;
                    isSuccess = success;

                    // 실패 시, 
                    if (!success)
                    {
                        // 실패 메시지 
                        ShopController.ShowFailedMessage(reason);
                        // 바로 종료
                        return;
                    }

                    // 귀찮지만 여기서도 똑같이 로그 남겨두자 
                    WebLog.Instance.AllLog("shop_reward", new Dictionary<string, object> { { "idx", _idx } });

                    // 가차 
                    ShopController.Treasure.Gacha(_idx, _sheet, out var results);

                    // 가챠 횟수 증가
                    autoGachaCount += 1;

                    // 파이어베이스 이벤트_연속횟수
                    if (firebaseEventTrackingCount.Contains(autoGachaCount))
                    {
                        WebLog.Instance.ThirdPartyLog($"treasure_auto_gacha_count_{autoGachaCount.ToLookUpString()}");
                    }

                    // 가챠 결과 
                    _results = results;
                });

                // 구매 될 때까지 대기 
                while (!isDone) yield return Timing.WaitForOneFrame;

                // 구매 실패 시, 종료
                if (!isSuccess)
                {
                    // 구매 실패해서 오토가 풀렸다면, 토글도 품 
                    auto.IsOn = false;
                    yield break;
                }

                // 연출 초기화 
                _hideLineWhenGetNew = true;
                _common.scroller.ReloadData();

                // 구매 성공이라면, 넘어온 가챠 결과로 연출 또 시도 
                yield return Timing.WaitUntilDone(_Direction());
            }
        }

        #endregion

        /*
         * Scroller
         */
        #region Scroller

        public int GetNumberOfCells(EnhancedScroller _)
        {
            var rowCount = _results.Count / numberOfCellsPerRow;
            rowCount += _results.Count % numberOfCellsPerRow == 0 ? 0 : 1; // 나머지가 있다면 한 줄 더 추가 
            return rowCount;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return _common.cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            var row = _common.scroller.GetCellView(_common.cellPrefab) as TreasureGachaRow;
            if (row == null) return null;

            row.Init(ref _results, dataIndex * numberOfCellsPerRow);

            // 아직 연출 전이라면, 강제 하이딩 처리 
            if (_hideLineWhenGetNew)
            {
                row.HideAllCells();
            }

            return row;
        }

        #endregion
    }
}