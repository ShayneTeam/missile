using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using InGame.Controller;
using InGame.Global;
using Ludiq;
using MEC;
using UnityEngine;

namespace UI.Popup.Menu.Shop.Gacha
{
    public class TreasureGachaRow : EnhancedScrollerCellView
    {
        [SerializeField] private Transform cellsParent; 
        private TreasureGachaCell[] _cells;

        private void Awake()
        {
            _cells = cellsParent.GetComponentsInChildren<TreasureGachaCell>();
        }

        public void Init(ref List<TreasureGachaData> results, int startingIndex)
        {
            // 자식으로 각 셀을 가지고 있어야 한다 
            for (var i = 0; i < _cells.Length; i++)
            {
                // 셀 세팅 (포함 안되는 셀에게는 null 보냄)
                _cells[i].Init(startingIndex + i < results.Count ? results[startingIndex + i] : null);
            }
        }

        public void HideAllCells()
        {
            for (var i = 0; i < _cells.Length; i++)
            {
                _cells[i].Hide();
            }
        }

        public void ShowCell(int index)
        {
            _cells[index].Show();
        }
        
        public IEnumerator<float> ShowCellEffect(int index)
        {
            yield return Timing.WaitUntilDone(_cells[index]._WaitRarityEffectTime().CancelWith(gameObject));
        }
    }
}