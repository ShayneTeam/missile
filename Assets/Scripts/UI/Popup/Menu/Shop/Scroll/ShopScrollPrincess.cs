﻿using InGame.Data;
using UnityEngine;

namespace UI.Popup.Menu.Shop.Scroll
{
    public class ShopScrollPrincess : ShopScroll
    {
        protected override void OnEnable()
        {
            base.OnEnable();

            UserData.My.Princess.OnWear += OnWear;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            UserData.My.Princess.OnWear -= OnWear;
        }

        private void OnWear(string costumeIdx)
        {
            Refresh();
        }
    }
}