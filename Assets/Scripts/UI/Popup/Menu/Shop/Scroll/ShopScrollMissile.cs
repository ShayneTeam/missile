﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Data;
using InGame.Global;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Shop.Scroll
{
    public class ShopScrollMissile : ShopScroll
    {
        [SerializeField] public EnhancedScrollerCellView adCellPrefab;
        [SerializeField] public EnhancedScrollerCellView uraniumCellPrefab;
        [SerializeField] public EnhancedScrollerCellView chipsetCellPrefab;
        [SerializeField] public EnhancedScrollerCellView stoneCellPrefab;
        [SerializeField] public EnhancedScrollerCellView treasureCellPrefab;
        
        
        protected override void OnEnable()
        {
            base.OnEnable();
            
            UserData.My.Purchase.OnChangedGachaExp += Refresh;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            UserData.My.Purchase.OnChangedGachaExp -= Refresh;
        }
        
        public override EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            var idx = AllIdxes.ElementAt(dataIndex);
            
            // 타입에 따라 프리팹 분기 
            var type = DataboxController.GetDataString(Table.Shop, sheet, idx, Column.Type);
            var cellPrefab = adCellPrefab;
            switch (type)
            {
                case "chipset_ad":
                    cellPrefab = chipsetCellPrefab;
                    break;
                case "gacha_ad":
                    cellPrefab = adCellPrefab;
                    break;
                case "gacha_gem":
                    cellPrefab = uraniumCellPrefab;
                    break;
                case "gacha_stone":
                    cellPrefab = stoneCellPrefab;
                    break;
                case "gacha_girl":
                    cellPrefab = treasureCellPrefab;
                    break;
            }
            
            // 셀 초기화 
            var cellView = Common.Scroller.GetCellView(cellPrefab) as ShopCell;
            if (cellView == null)
                return null;

            cellView.Init(idx, sheet);
            
            return cellView;
        }
    }
}