﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Data;
using InGame.Global;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Shop.Scroll
{
    [RequireComponent(typeof(CommonScroller))]
    public abstract class ShopScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] protected string sheet;

        protected CommonScroller Common;

        protected ICollection<string> AllIdxes;

        private void Awake()
        {
            Common = GetComponent<CommonScroller>();
        }

        protected virtual void OnEnable()
        {
            UserData.My.Purchase.OnAddedPurchasedCount += Refresh;
        }

        protected virtual void OnDisable()
        {
            UserData.My.Purchase.OnAddedPurchasedCount -= Refresh;
        }

        public void Init()
        {
            AllIdxes = DataboxController.GetAllIdxes(Table.Shop, sheet);

            // 스크롤 초기화
            Common.Scroller.Delegate = this;
            Common.Scroller.ReloadData();
        }

        public void Refresh()
        {
            AllIdxes = DataboxController.GetAllIdxes(Table.Shop, sheet);

            Common.Scroller.RefreshActiveCellViews();
        }

        public void ChangeScrollPositionFactor(float value)
        {
            Common.Scroller.ReloadData(value);
        }

        public void MoveScrollPosition(int cellIndex)
        {
            Common.Scroller.JumpToDataIndex(cellIndex);
        }

        public int GetNumberOfCells(EnhancedScroller _)
        {
            return AllIdxes.Count;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return Common.cellSize;
        }

        public virtual EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀 초기화 
            var cellView = Common.Scroller.GetCellView(Common.cellPrefab) as ShopCell;
            if (cellView == null)
                return null;

            cellView.Init(AllIdxes.ElementAt(dataIndex), sheet);
            
            return cellView;
        }
    }
}