﻿using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Data;
using InGame.Global;
using UnityEngine;

namespace UI.Popup.Menu.Shop.Scroll
{
    public class ShopScrollAncient : ShopScroll
    {
        [SerializeField] public EnhancedScrollerCellView energyCellPrefab;
        [SerializeField] public EnhancedScrollerCellView chipCellPrefab;
        
        
        protected override void OnEnable()
        {
            base.OnEnable();
            
            UserData.My.Missiles.OnChangedEnergy += Refresh;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            UserData.My.Missiles.OnChangedEnergy -= Refresh;
        }
        
        public override EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            var idx = AllIdxes.ElementAt(dataIndex);
            
            // 타입에 따라 프리팹 분기 
            var type = DataboxController.GetDataString(Table.Shop, sheet, idx, Column.Type);
            var cellPrefab = energyCellPrefab;
            switch (type)
            {
                case "gacha_energy":
                    cellPrefab = energyCellPrefab;
                    break;
                case "gacha_chipset":
                    cellPrefab = chipCellPrefab;
                    break;
            }
            
            // 셀 초기화 
            var cellView = Common.Scroller.GetCellView(cellPrefab) as ShopCell;
            if (cellView == null) return null;

            cellView.Init(idx, sheet);
            
            return cellView;
        }
    }
}