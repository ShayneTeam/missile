﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using UI.Popup.Menu.Shop.Gacha;
using UnityEngine;

namespace UI.Popup.Menu.Shop.InfinityStone
{
    [RequireComponent(typeof(UIPopup))]
    public class ShopInfStoneGachaPopup : MonoBehaviour
    {
        [SerializeField] private float delayStone;
        [SerializeField] private float delayEffect;

        [SerializeField] private List<ShopInfStoneGachaCell> cells;
        
        [Header("자동 가챠"), SerializeField] private UIToggle auto;
        [SerializeField] private float autoDelay;
        
        private Dictionary<string, int> _result;

        private CoroutineHandle _direction;
        
        private string _idx;
        private string _sheet;

        public static void Show(Dictionary<string, int> result, string idx, string sheet)
        {
            var popup = UIPopup.GetPopup("Shop Stone Gacha");
            popup.Show();
            
            var autoGacha = popup.GetComponent<ShopInfStoneGachaPopup>();
            autoGacha._result = result;
            autoGacha._sheet = sheet;
            autoGacha._idx = idx;
        }

        [PublicAPI]
        public void OnShowStarted()
        {
            // 초기화 및 전부 안 보이게 하기 
            InitDirection();

            // 토글 초기화 
            auto.IsOn = false;
        }

        [PublicAPI]
        public void OnShowFinished()
        {
            _direction = Timing.RunCoroutine(_AutoGacha().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHideStarted()
        {
            Timing.KillCoroutines(_direction);
        }

        [PublicAPI]
        public void OnToggleIsOn()
        {
            // 연출 끝났는데 누른 경우, 강제로 끔 
            if (_direction.IsRunning) return;
            auto.IsOn = false;

            // 실패 이유는 무조건 우라늄 부족이라고 가정함 
            ShopController.ShowFailedMessage(ShopController.FailedReason.NotEnoughGem);
        }

        private void InitDirection()
        {
            var allIdxes = DataboxController.GetAllIdxes(Table.Stone, Sheet.stone);
            for (var i = 0; i < cells.Count; i++)
            {
                // 예외 처리 
                if (i >= allIdxes.Count)
                    break;

                var stoneIdx = allIdxes.ElementAt(i);
                _result.TryGetValue(stoneIdx, out var count);

                cells[i].Init(stoneIdx, count);
                cells[i].Hide();
            }
        }

        private IEnumerator<float> _AutoGacha()
        {
            // 파이어베이스 스톤 연속뽑기 트래킹용도
            var firebaseEventTrackingCount = new List<int> { 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
            var autoGachaCount = 1;
            
            // 일단 첫 연출 실행 
            yield return Timing.WaitUntilDone(_DirectionFlow());
					
            // 예외 처리
            if (string.IsNullOrEmpty(_idx) || string.IsNullOrEmpty(_sheet)) yield break;
					
            // 자동 가챠 무한 반복 (구매 실패 할 때까지)
            while (true)
            {
                // 딜레이 
                yield return Timing.WaitForSeconds(autoDelay);
                
                // 오토 꺼졌다면, 켜질 때까지 대기 
                while (!auto.IsOn) yield return Timing.WaitForOneFrame;
                
                // 재구매
                var isDone = false;
                var isSuccess = false; 
                ShopController.Purchase(_idx, _sheet, (success, reason) =>
                {
                    isDone = true;
                    isSuccess  = success;
					
                    // 실패 시, 
                    if (!success)
                    {
                        // 실패 메시지 
                        ShopController.ShowFailedMessage(reason);
                        // 바로 종료
                        return;
                    }
							    
                    // 귀찮지만 여기서도 똑같이 로그 남겨두자 
                    WebLog.Instance.AllLog("shop_reward", new Dictionary<string, object>{{"idx",_idx}});
							
                    // 가차 
                    InfStoneControl.StoneControl.Gacha(ShopController.GetGachaCount(_idx, _sheet), out var result);
                    
                    // 가챠 횟수 증가
                    autoGachaCount += 1;

                    // 파이어베이스 이벤트_연속횟수
                    if (firebaseEventTrackingCount.Contains(autoGachaCount))
                    {
                        WebLog.Instance.ThirdPartyLog($"stone_auto_gacha_count_{autoGachaCount}");
                    }

                    // 가챠 결과 
                    _result = result;    
                });
						
                // 구매 될 때까지 대기 
                while (!isDone) yield return Timing.WaitForOneFrame;
						
                // 구매 실패 시, 종료
                if (!isSuccess)
                {
                    // 구매 실패해서 오토가 풀렸다면, 토글도 품 
                    auto.IsOn = false;
                    yield break;
                }
                
                // 연출 초기화 
                InitDirection();

                // 구매 성공이라면, 넘어온 가챠 결과로 연출 또 시도 
                yield return Timing.WaitUntilDone(_DirectionFlow());
            }
        }
        
        private IEnumerator<float> _DirectionFlow()
        {
            // 괜히 한 프레임 쉬어주기 
            yield return Timing.WaitForOneFrame;
            
            // 랜덤으로 하나씩 등장 
            var shuffledCells = cells.OrderBy(x => Guid.NewGuid()).ToList();
            for (var i = 0; i < shuffledCells.Count; i++)
            {
                // 아이콘만 보여주기 
                shuffledCells[i].ShowIcon();
                
                // 간격 대기 
                yield return Timing.WaitForSeconds(delayStone);
            }
            
            // 한번 만 더 딜레이
            yield return Timing.WaitForSeconds(delayEffect);
            
            // 뽑은 것들만  
            for (var i = 0; i < shuffledCells.Count; i++)
            {
                shuffledCells[i].ShowEffectIfCan();
            }
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_gacha_02");
        }
    }
}