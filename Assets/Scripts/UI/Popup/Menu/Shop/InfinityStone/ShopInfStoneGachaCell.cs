using System;
using System.Collections.Generic;
using System.Linq;
using Coffee.UIExtensions;
using DG.Tweening;
using Global;
using Global.Extensions;
using InGame;
using InGame.Controller;
using InGame.Global;
using MEC;
using TMPro;
using UI.Popup.Menu.Stone;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using Utils;
using Random = System.Random;

namespace UI.Popup.Menu.Shop.Gacha
{
    public class ShopInfStoneGachaCell : MonoBehaviour
    {
        [SerializeField] private GameObject container;
        
        [SerializeField] private Image effect;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI countText;
        [SerializeField] private TextMeshProUGUI nameText;
        
        private DOTweenAnimation _tween;
        private int _count;
        private string _stoneIdx;


        public void Init(string stoneIdx, int count)
        {
            InternalInit(stoneIdx, count);

            // 이름 
            nameText.text = GetNameText(stoneIdx);

            // 카운트 
            countText.text = GetCountText(count);
        }

        public void InitMergedNameAndCount(string stoneIdx, int count)
        {
            InternalInit(stoneIdx, count);
            
            // 이름과 카운트를 병합해서 카운트 텍스트에 넣기 
            countText.text = $"{GetNameText(stoneIdx).Wrap(nameText.color)} {GetCountText(count)}";
        }

        private static string GetCountText(int count)
        {
            return Localization.GetFormatText("info_136", count.ToLookUpString());
        }

        private static string GetNameText(string stoneIdx)
        {
            return Localization.GetText(DataboxController.GetDataString(Table.Stone, Sheet.stone, stoneIdx, Column.name));
        }

        private void InternalInit(string stoneIdx, int count)
        {
            _stoneIdx = stoneIdx;
            _count = count;

            // 아이콘 
            icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Stone, Sheet.stone, stoneIdx, Column.icon));
        }

        public void ShowIcon()
        {
            container.SetActive(true);
            
            // 트윈
            if (_tween == null)
                _tween = container.GetComponent<DOTweenAnimation>();
            _tween.DORestart();
        }

        public void ShowEffectIfCan()
        {
            container.SetActive(_count > 0);
            
            // 배경 이펙트 
            effect.gameObject.SetActive(_count > 0);
           
            // 카운트 
            countText.gameObject.SetActive(_count > 0);
            
            // 강화 이펙트 
            if (_count > 0)
                InfStoneEffector.ShowEffect(_stoneIdx, icon.transform.position);
        }

        public void Hide()
        {
            container.SetActive(false);
            effect.gameObject.SetActive(false);
            countText.gameObject.SetActive(false);
        }
    }
}