﻿using System;
using System.Collections.Generic;
using Doozy.Engine.UI;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Shop.Gacha
{
    [RequireComponent(typeof(MissileGachaPopup))]
    public class MissileAutoGachaPopup : MonoBehaviour
    {
        [Header("자동 가챠"), SerializeField] private UIToggle auto;
        [SerializeField] private float autoDelay;

        private string _idx;
        private string _sheet;


        private MissileGachaPopup _popup;
        private CoroutineHandle _autoGacha;


        /*
         * 가챠_미사일로 뽑았을때 보너스 텍스트 표시용
         */
        [SerializeField] private TextMeshProUGUI displayMissileBonusText;
        
        /*
         * 고대 에너지 
         */
        [SerializeField] private SlicedFilledImage gaugeFill;
        [SerializeField] private TextMeshProUGUI gaugeText;
        
        
        private List<int> _firebaseEventTrackingCount;


        private void Awake()
        {
            _popup = GetComponent<MissileGachaPopup>();
        }

        public static void Show(List<ShopController.MissileGachaData> results, string idx, string sheet)
        {
            var popup = UIPopup.GetPopup("Shop Auto Gacha Missile");

            var autoGacha = popup.GetComponent<MissileAutoGachaPopup>();
            autoGacha._popup.Results = results;

            popup.Show();

            // 세팅
            autoGacha._sheet = sheet;
            autoGacha._idx = idx;
        }

        [PublicAPI]
        public void OnShowStarted()
        {
            // 토글 초기화 
            auto.IsOn = false;
        }

        [PublicAPI]
        public void OnShowFinished()
        {
            // 연출 시작 (스크롤 초기화 이후에 빈 화면에서 시작해야 함)
            // 주의! ShopGachaResultsPopup의 OnShowFinished()는 호출되지 못하게 할 것
            _autoGacha = Timing.RunCoroutine(_AutoGacha().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHideStarted()
        {
            Timing.KillCoroutines(_autoGacha);
        }

        [PublicAPI]
        public void OnToggleIsOn()
        {
            // 연출 끝났는데 누른 경우, 강제로 끔
            if (_autoGacha.IsRunning) return;
            auto.IsOn = false;

            // 실패 이유는 무조건 우라늄 부족이라고 가정함 
            ShopController.ShowFailedMessage(ShopController.FailedReason.NotEnoughGem);
        }

        private void RefreshMissileBonusText()
        {
            // 가챠 보너스 텍스트 표시
            displayMissileBonusText.text = Localization.GetFormatText("info_404",
                (0.01 * UserData.My.UserInfo.MissileBonusChance).ToString("0.00"));
        }

        private void RefreshAncientGauge()
        {
            // 게이지
            const string shopIdxAncientEnergy = "999001";
            var priceValue = ShopController.GetPriceValue(shopIdxAncientEnergy, Sheet.ancient);
            var curr = UserData.My.Missiles.AncientEnergy;
            gaugeFill.fillAmount = (float)curr / (float)priceValue;
            
            // 게이지 텍스트 
            gaugeText.text = Localization.GetFormatText("shop_02", curr.ToLookUpString(), priceValue.ToLookUpString());
        }

        private IEnumerator<float> _AutoGacha()
        {
            // 파이어베이스 미사일 연속뽑기 트래킹용도
            _firebaseEventTrackingCount ??= new List<int> { 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
            var autoGachaCount = 1;

            // 가챠 보너스 텍스트 갱신
            RefreshMissileBonusText();

            // 고대 에너지 갱신
            RefreshAncientGauge();

            // 일단 첫 연출 실행 
            yield return Timing.WaitUntilDone(_popup._DirectionFlow());

            // 예외 처리
            if (string.IsNullOrEmpty(_idx) || string.IsNullOrEmpty(_sheet)) yield break;

            // 자동 가챠 무한 반복 (구매 실패 할 때까지)
            while (true)
            {
                // 딜레이 
                yield return Timing.WaitForSeconds(autoDelay);

                // 오토 꺼졌다면, 켜질 때까지 대기 
                while (!auto.IsOn) yield return Timing.WaitForOneFrame;

                // 재구매
                var isDone = false;
                var isSuccess = false;
                ShopController.Purchase(_idx, _sheet, (success, reason) =>
                {
                    isDone = true;
                    isSuccess = success;

                    // 실패 시, 
                    if (!success)
                    {
                        // 실패 메시지 
                        ShopController.ShowFailedMessage(reason);
                        // 바로 종료
                        return;
                    }

                    // 귀찮지만 여기서도 똑같이 로그 남겨두자 
                    WebLog.Instance.AllLog("shop_reward", new Dictionary<string, object> { { "idx", _idx } });

                    // 가차 
                    ShopController.Missile.GachaWithLevelBonus(_idx, _sheet, out var results);

                    // 가챠 횟수 증가
                    autoGachaCount += 1;

                    // 파이어베이스 이벤트_연속횟수
                    if (_firebaseEventTrackingCount.Contains(autoGachaCount))
                    {
                        WebLog.Instance.ThirdPartyLog($"missile_auto_gacha_count_{autoGachaCount.ToLookUpString()}");
                    }

                    // 가챠 보너스 텍스트 표시
                    RefreshMissileBonusText();

                    // 고대 에너지 갱신
                    RefreshAncientGauge();

                    // 가챠 결과 
                    _popup.Results = results;
                });

                // 구매 될 때까지 대기 
                while (!isDone) yield return Timing.WaitForOneFrame;

                // 구매 실패 시, 종료
                if (!isSuccess)
                {
                    // 구매 실패해서 오토가 풀렸다면, 토글도 품 
                    auto.IsOn = false;
                    yield break;
                }

                // 연출 초기화 
                _popup.ResetDirection();

                // 구매 성공이라면, 넘어온 가챠 결과로 연출 또 시도 
                yield return Timing.WaitUntilDone(_popup._DirectionFlow());
            }
        }
    }
}