using System;
using System.Collections.Generic;
using System.Linq;
using Coffee.UIExtensions;
using DG.Tweening;
using Global;
using Global.Extensions;
using InGame.Controller;
using InGame.Global;
using MEC;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.UI;
using Random = System.Random;

namespace UI.Popup.Menu.Shop.Gacha
{
    public class MissileGachaCell : MonoBehaviour
    {
        [SerializeField] private GameObject container;
        
        [SerializeField] private Image effect;
        [SerializeField] private Image icon;

        [Header("쩐설 이펙트"), SerializeField] private GameObject effectLegend;
        [SerializeField] private float legendDelay;
        [Header("신화 이펙트"), SerializeField] private GameObject effectMyth;
        [SerializeField] private float mythDelay;

        private DOTweenAnimation _tween;
        private ShopController.MissileGachaData _data;


        public void Init(ShopController.MissileGachaData data)
        {
            _data = data;
            
            // 데이타 없으면 아예 컨테이터 꺼버림 
            container.SetActive(data != null);

            if (data == null) 
                return;

            // 이펙트
            var effectStr = DataboxController.GetDataString(Table.Shop, Sheet.reward_gacha, data.Idx, Column.effect);
            effect.gameObject.SetActive(effectStr != "none");
            
            if (effect.gameObject.activeSelf)
            {
                effect.sprite = AtlasController.GetSprite(effectStr);
            }
            
            // 아이콘 
            var iconStr = DataboxController.GetDataString(Table.Shop, Sheet.reward_gacha, data.Idx, Column.icon);
            icon.sprite = AtlasController.GetSprite(iconStr);

            // 컨테이너 밑에 들고 있는 이펙트 활성  
            // 아래 Show()가 호출 될 때, 컨테이너가 켜질 때, 자동으로 레어리티 이펙트도 켜지게 됨 
            effectMyth.SetActive(false);
            effectLegend.SetActive(false);
            
            switch (_data.Rarity)
            {
                case nameof(MissileRarity.legend):
                    effectLegend.SetActive(true);
                    break;
                case nameof(MissileRarity.myth):
                    effectMyth.SetActive(true);
                    break;
            }
        }

        public void Show()
        {
            container.SetActive(true);
            
            // 트윈
            if (_tween == null)
                _tween = container.GetComponent<DOTweenAnimation>();
            _tween.DORestart();
        }

        public void Hide()
        {
            container.SetActive(false);
        }

        public IEnumerator<float> _WaitRarityEffectTime()
        {
            switch (_data.Rarity)
            {
                case nameof(MissileRarity.legend):
                    // 사운드 
                    SoundController.Instance.PlayEffect("sfx_ui_gacha_02");
                    // 대기
                    yield return Timing.WaitForSeconds(legendDelay);
                    break;
                case nameof(MissileRarity.myth):
                    // 사운드 
                    SoundController.Instance.PlayEffect("sfx_ui_gacha_02");
                    // 대기
                    yield return Timing.WaitForSeconds(mythDelay);
                    break;
                default:
                    // 사운드 
                    SoundController.Instance.PlayEffect("sfx_ui_gacha_01");
                    yield return Timing.WaitForOneFrame;
                    break;
            }
        }
    }
}