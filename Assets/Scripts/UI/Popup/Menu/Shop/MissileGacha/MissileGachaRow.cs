using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using InGame.Controller;
using InGame.Global;
using MEC;
using UnityEngine;

namespace UI.Popup.Menu.Shop.Gacha
{
    public class MissileGachaRow : EnhancedScrollerCellView
    {
        [SerializeField] private List<MissileGachaCell> cells;
        
        public void Init(ref List<ShopController.MissileGachaData> results, int startingIndex)
        {
            // 자식으로 각 셀을 가지고 있어야 한다 
            for (var i = 0; i < cells.Count; i++)
            {
                // 셀 세팅 (포함 안되는 셀에게는 null 보냄)
                cells[i].Init(startingIndex + i < results.Count ? results[startingIndex + i] : null);
            }
        }

        public void HideAllCells()
        {
            for (var i = 0; i < cells.Count; i++)
            {
                cells[i].Hide();
            }
        }

        public void ShowCell(int index)
        {
            cells[index].Show();
        }
        
        public IEnumerator<float> ShowCellEffect(int index)
        {
            yield return Timing.WaitUntilDone(cells[index]._WaitRarityEffectTime().CancelWith(gameObject));
        }
    }
}