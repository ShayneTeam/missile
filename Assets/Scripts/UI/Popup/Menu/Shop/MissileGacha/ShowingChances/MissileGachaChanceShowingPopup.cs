using System;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Global;
using JetBrains.Annotations;
using Ludiq;
using UnityEngine;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(UIPopup))]
    public class MissileGachaChanceShowingPopup : MonoBehaviour
    {
        [SerializeField] private List<MissileGachaChanceShowingCell> cells;

        public static void Show()
        {
            var popup = UIPopup.GetPopup("Shop Gacha Chances");
            popup.Show();
        }

        [PublicAPI]
        public void Init()
        {
            var allIdxes = DataboxController.GetAllIdxes(Table.Shop, Sheet.gacha_info);
            for (var i = 0; i < allIdxes.Count; i++)
            {
                if (i >= cells.Count)
                    break;
                
                var idx = allIdxes.ElementAt(i);
                var cell = cells[i];
                cell.Init(idx, Table.Shop, Sheet.gacha_info);
            }
        }
    }
}