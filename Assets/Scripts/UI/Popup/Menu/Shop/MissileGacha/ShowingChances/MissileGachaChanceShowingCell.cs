using Global;
using InGame;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Shop
{
    public class MissileGachaChanceShowingCell : MonoBehaviour
    {
        [SerializeField] public Image icon;
        [SerializeField] public TextMeshProUGUI descText;
        [SerializeField] public TextMeshProUGUI chanceText;

        public void Init(string idx, string table, string sheet)
        {
            // icon
            icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(table, sheet, idx, Column.icon));

            // name
            descText.text = Localization.GetText(DataboxController.GetDataString(table, sheet, idx, Column.name));

            // desc
            chanceText.text = Localization.GetText(DataboxController.GetDataString(table, sheet, idx, Column.desc));
        }
    }
}