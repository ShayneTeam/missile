using System.Collections.Generic;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using InGame.Controller;
using JetBrains.Annotations;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Shop.Gacha
{
    [RequireComponent(typeof(UIPopup))]
    public class MissileGachaPopup : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScrollerCellView cellPrefab;
        [SerializeField] private float cellSize;
        [SerializeField] public EnhancedScroller scroller;
        
        [SerializeField] private int numberOfCellsPerRow = 10;

        [SerializeField] private Image imageForScrolling;

        /*
         * 가챠 결과
         */
        private List<ShopController.MissileGachaData> _results;

        public List<ShopController.MissileGachaData> Results
        {
            get => _results;
            set => _results = value;
        }

        /*
         * 연출
         */
        // 코루틴과 독립적으로 들어가야 함
        private bool _hideLineWhenGetNew;
        
        private CoroutineHandle _direction;


        public static MissileGachaPopup Show(List<ShopController.MissileGachaData> results)
        {
            var popup = UIPopup.GetPopup("Shop Gacha Results");
            
            var gachaPopup = popup.GetComponent<MissileGachaPopup>();
            gachaPopup.Results = results;
            
            popup.Show();

            return gachaPopup;
        }

        [PublicAPI]
        public void OnShowStarted()
        {
            // 요 타이밍에 하는 거 중요
            _hideLineWhenGetNew = true;
            
            // 스크롤 초기화 
            scroller.Delegate = this;
            scroller.ReloadData();
        }

        [PublicAPI]
        public void OnShowFinished()
        {
            // 연출 시작 (스크롤 초기화 이후에 빈 화면에서 시작해야 함)
            _direction = Timing.RunCoroutine(_DirectionFlow().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHideStarted()
        {
            // UI가 Hide 될 시, 안에 있는 것들만 꺼지고, 루트 게임오브젝트는 꺼지지 않아서 코루틴이 종료되질 않음 
            // 그래서 이벤트 받고 수동 종료시킨다 
            Timing.KillCoroutines(_direction);
        }

        public IEnumerator<float> _DirectionFlow()
        {
            // 괜히 한 프레임 쉬어주기 
            yield return Timing.WaitForOneFrame;
            
            // 스크롤 못하게 막기 
            imageForScrolling.enabled = false;
            
            // 진행 
            for (var i = 0; i < Results.Count; i++)
            {
                var rowIndex = i / numberOfCellsPerRow;
                
                // 현재 연출되는 로우가 맨 아래에 보이게 스크롤 맞춤  
                scroller.JumpToDataIndex(rowIndex, 1f, 1f);
                
                // 위의 점프가 반영되기 위해 한 프레임 대기
                // (안 기다리면, 미세한 딜레이로 인해 GetCellViewAtDataIndex()가 null이 반환됨
                // 점프가 반영되서 스크롤이 밑으로 내려가야만 GetCellView가 제대로 호출됨) 
                yield return Timing.WaitForOneFrame;
                
                // 
                var row = scroller.GetCellViewAtDataIndex(rowIndex) as MissileGachaRow;
                if (row != null)
                {
                    var cellIndex = i % numberOfCellsPerRow;
                    row.ShowCell(cellIndex);
                    
                    yield return Timing.WaitUntilDone(row.ShowCellEffect(cellIndex));
                }

                // 간격 대기 
                yield return Timing.WaitForOneFrame;
            }
            
            // 스크롤 풀기
            imageForScrolling.enabled = true;

            // 스크롤해도 라인 자유롭게 볼 수 있게 함 
            _hideLineWhenGetNew = false;
        }

        public void ResetDirection()
        {
            Timing.KillCoroutines(_direction);
            
            _hideLineWhenGetNew = true;
            scroller.ReloadData();
        }

        #region Scroller

        public int GetNumberOfCells(EnhancedScroller _)
        {
            var rowCount = Results.Count / numberOfCellsPerRow;
            rowCount += Results.Count % numberOfCellsPerRow == 0 ? 0 : 1; // 나머지가 있다면 한 줄 더 추가 
            return rowCount;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            var row = scroller.GetCellView(cellPrefab) as MissileGachaRow;
            if (row == null)
                return null;

            row.Init(ref _results, dataIndex * numberOfCellsPerRow);

            // 아직 연출 전이라면, 강제 하이딩 처리 
            if (_hideLineWhenGetNew)
            {
                row.HideAllCells();
            }

            return row;
        }

        #endregion
    }
}