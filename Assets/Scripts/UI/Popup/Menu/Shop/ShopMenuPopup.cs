﻿using System;
using System.Collections.Generic;
using System.Globalization;
using Firebase.Analytics;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using MEC;
using UI.Popup.Menu.Shop.Scroll;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Shop
{
    public enum ShopTab
    {
        Missile,
        Ancient,
        Goods,
        Bazooka,
        Gem,
        Mileage,
        Stone,
        Season,
    }
    
    public class ShopMenuPopup : MonoBehaviour, IPopup
    {
        [SerializeField] private Transform tabsParent;
        private Toggle[] _tabs;
        [SerializeField] private Transform scrollsParent;
        private ShopScroll[] _scrolls;

        [SerializeField] private ResourceText mileageText;

        public bool IsInit { get; private set; }
        
        private CoroutineHandle _changeTab;
        private ShopTab _currentTab;

        private void Awake()
        {
            _tabs = tabsParent.GetComponentsInChildren<Toggle>();
            _scrolls = scrollsParent.GetComponentsInChildren<ShopScroll>();
        }

        private void Start()
        {
            // 이벤트 등록
            UserData.My.Resources.OnChangedMileage += RefreshMileageText;
        }

        private void OnDestroy()
        {
            // 이벤트 해제 
            UserData.My.Resources.OnChangedMileage -= RefreshMileageText;
        }

        public void Init()
        {
            WebLog.Instance.ThirdPartyLog("shop_menu_popup_open");

            // 팝업 열릴 때마다 구매 횟수 초기화 로직 태우기
            ShopController.StartProcessToReset();

            if (!IsInit)
            {
                // 초기화 위해 다 켜기 
                foreach (var scroll in _scrolls) scroll.gameObject.SetActive(true);

                // 초기화 
                foreach (var scroll in _scrolls) scroll.Init();
                
                IsInit = true;
                
                // 기본 탭 설정 (탭 예약 아니라면)
                if (!_changeTab.IsRunning)
                    ChangeTab(ShopTab.Missile);
            }
            else
            {
                foreach (var scroll in _scrolls) scroll.Refresh();
            }
            
            // 마일리지 
            RefreshMileageText(UserData.My.Resources.mileage);
        }

        public void ChangeTab(ShopTab tab)
        {
            _currentTab = tab;
            
            Timing.KillCoroutines(_changeTab);
            _changeTab = Timing.RunCoroutine(_ChangeTabFlow(tab).CancelWith(gameObject));
        }

        private IEnumerator<float> _ChangeTabFlow(ShopTab tab)
        {
            // Init 될 때까지 대기 
            while (!IsInit) yield return Timing.WaitForOneFrame;
            
            // 변경 
            _tabs[(int)tab].isOn = true;
            
            // 탭 변경 토글 이벤트가 제대로 안 들어와서, 수동으로 호출
            InternalSwitchScroll((int)tab);
        }

        private void RefreshMileageText(double mileage)
        {
            mileageText.Text.text = mileage.ToString("0");
        }

        private void InternalSwitchScroll(int index)
        {
            if (!IsInit) return;
            
            // 스크롤 다 끄기 
            foreach (var scroll in _scrolls) scroll.gameObject.SetActive(false);
            
            // 선택된 스크롤만 켜기 
            var targetScroll = _scrolls[index];
            
            targetScroll.gameObject.SetActive(true);
            targetScroll.Refresh();
        }

        [PublicAPI]
        public void SwitchScrollWithToggle(Toggle toggle)
        {
            if (!IsInit) return;
            
            // 스크롤 다 끄기 
            foreach (var scroll in _scrolls) scroll.gameObject.SetActive(false);

            // 넘어온 토글 기반으로 선택시킬 스크롤 찾기 (하이어라키상 토글과 스크롤의 순서를 1:1 매칭시킬 것)
            var toggleIndex = 0;
            for (var i = 0; i < _tabs.Length; i++)
            {
                if (_tabs[i] == toggle)
                {
                    toggleIndex = i;
                    break;
                }
            }
            
            // 선택된 스크롤만 켜기 
            var targetScroll = _scrolls[toggleIndex];
            
            targetScroll.gameObject.SetActive(true);
            targetScroll.Refresh();
        }
        
        [PublicAPI]
        public void SwitchScrollWith(int index, float scrollPositionFactor)
        {
            if (!IsInit) return;
            
            InternalSwitchScroll(index);
            
            // 선택된 스크롤만 켜기 
            var targetScroll = _scrolls[index];
            targetScroll.ChangeScrollPositionFactor(1f);
        }

        public void MoveScrollPosition(int wantCellIndex)
        {
            var scroll = _scrolls[(int)_currentTab];
            scroll.MoveScrollPosition(wantCellIndex);
        }
    }
}