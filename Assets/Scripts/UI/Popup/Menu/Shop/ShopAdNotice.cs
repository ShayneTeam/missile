using System;
using System.Collections.Generic;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using MEC;
using UnityEngine;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(NoticeIcon))]
    public class ShopAdNotice : MonoBehaviour
    {
        private NoticeIcon _noticeIcon;
        private CoroutineHandle _checking;
        
        private void Awake()
        {
            _noticeIcon = GetComponent<NoticeIcon>();
            
            // 이벤트
            UserData.My.Purchase.OnAddedPurchasedCount += Refresh;
        }

        private void OnDestroy()
        {
            // 이벤트
            UserData.My.Purchase.OnAddedPurchasedCount -= Refresh;
        }

        private void Start()
        {
            Refresh();
        }

        private void Refresh()
        {
            var canPurchase = CanPurchase();
            _noticeIcon.notice.SetActive(canPurchase);

            // 구매 불가능 시, 일정 주기마다 체크 
            if (!canPurchase)
            {
                Timing.KillCoroutines(_checking);
                _checking = Timing.RunCoroutine(_CheckEndCoolTime().CancelWith(gameObject), Segment.RealtimeUpdate);
            }
        }

        private static bool CanPurchase()
        {
            return ShopController.HasAnyCanPurchasePriceType(Sheet.missile, ShopController.PriceType.ad);
        }

        // 상점 가챠 광고 상품이 단 하나 밖에 없어서, 그냥 여기서 체크 함 
        // 원래 이런 건 다른 곳에서 해주고, 여기선 이벤트 받는 게 맞음 
        IEnumerator<float> _CheckEndCoolTime()
        {
            // 구매가 가능할 때까지 일정 주기로 계속 체크 
            while (!CanPurchase())
            {
                yield return Timing.WaitForSeconds(1f);
            }

            // 구매 가능하다면, 갱신
            Refresh();
        }
    }
}