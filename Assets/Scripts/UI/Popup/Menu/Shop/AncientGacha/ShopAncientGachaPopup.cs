﻿using System;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Shop.Gacha
{
    public class ShopAncientGachaPopup : MonoBehaviour
    {
        [SerializeField] private float delay;
        [SerializeField] private Image icon;
        [SerializeField] private DOTweenAnimation anim;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI descText;
        
        private List<ShopController.MissileGachaData> _result;

        private CoroutineHandle _direction;
        

        public static void Show(List<ShopController.MissileGachaData> result, string idx, string sheet)
        {
            var popup = UIPopup.GetPopup("Shop Ancient Gacha");
            popup.Show();
            
            var gachaPopup = popup.GetComponent<ShopAncientGachaPopup>();
            gachaPopup._result = result;
        }

        [PublicAPI]
        public void OnShowStarted()
        {
            // 초기화 및 전부 안 보이게 하기 
            InitDirection();
        }

        [PublicAPI]
        public void OnShowFinished()
        {
            _direction = Timing.RunCoroutine(_DirectionFlow().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHideStarted()
        {
            Timing.KillCoroutines(_direction);

            // 다음번 재사용을 위해 애니메이션 되돌려놓음
            anim.DORewind();
        }

        private void InitDirection()
        {
            var missile = _result.FirstOrDefault();
            if (missile == null) return;
            
            // 아이콘
            icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Missile, Sheet.missile, missile.MissileIdx, Column.resource_id));
            
            // 이름
            nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Missile, Sheet.missile, missile.MissileIdx, Column.name));
            
            // 설명 
            descText.text = Localization.GetText(DataboxController.GetDataString(Table.Missile, Sheet.missile, missile.MissileIdx, Column.desc));
            
            // 일단 꺼두기 
            ShowTexts(false);
        }
        
        private IEnumerator<float> _DirectionFlow()
        {
            // 딜레이 
            yield return Timing.WaitForSeconds(delay);
            
            // 애니메이션
            anim.DORestart();
            
            // 이름 설명 켜기 
            ShowTexts(true);
        }

        private void ShowTexts(bool show)
        {
            nameText.gameObject.SetActive(show);
            descText.gameObject.SetActive(show);
        }
    }
}