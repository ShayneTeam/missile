﻿using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Firebase.Analytics;
using Global;
using InGame.Controller;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellNormal))]
    public class ShopCellMileage : ShopCell
    {
        private ShopCellBase _base;
        private ShopCellNormal _normal;

        private void Awake()
        {
            _base = GetComponent<ShopCellBase>();
            _normal = GetComponent<ShopCellNormal>();
        }

        public override void Init(string idx, string sheet)
        {
            base.Init(idx, sheet);
            
            RefreshCellView();
        }

        public override void OnClick()
        {
            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
                if (!success)
                {
                    // 구매 실패 메시지
                    ShopController.ShowFailedMessage(reason);
                    return;
                }
                
                WebLog.Instance.AllLog("consume_mileage", new Dictionary<string, object> {{"idx",Idx}});

                // 구매 성공 메시지 
                MessagePopup.Show("shop_24");
            });
        }

        public override void RefreshCellView()
        {
            // 노멀 초기화
            _base.Init(Idx, Sheet);
            _normal.Init(Idx, Sheet);
        }
    }
}