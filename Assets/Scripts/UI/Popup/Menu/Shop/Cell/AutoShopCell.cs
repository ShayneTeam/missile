﻿using System;
using UnityEngine;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCell))]
    public class AutoShopCell : MonoBehaviour
    {
        [SerializeField] public string idx;
        [SerializeField] public string sheet;
        
        private ShopCell _cell;

        private void Awake()
        {
            _cell = GetComponent<ShopCell>();
        }

        public void Start()
        {
            _cell.Init(idx, sheet);
        }
    }
}