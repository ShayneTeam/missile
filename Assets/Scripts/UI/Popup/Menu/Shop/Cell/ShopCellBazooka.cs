﻿using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Firebase.Analytics;
using Global;
using InGame.Controller;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellNormal))]
    public class ShopCellBazooka : ShopCell
    {
        private ShopCellBase _base;
        private ShopCellNormal _normal;

        private void Awake()
        {
            _base = GetComponent<ShopCellBase>();
            _normal = GetComponent<ShopCellNormal>();
        }

        public override void Init(string idx, string sheet)
        {
            base.Init(idx, sheet);
            
            RefreshCellView();
        }

        public override void OnClick()
        {
            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
                if (!success)
                {
                    // 구매 실패 메시지 
                    ShopController.ShowFailedMessage(reason);
                    return;
                }
                
                WebLog.Instance.AllLog("purchase_bazooka", new Dictionary<string, object>{{"idx",Idx}});

                // 신규 바주카 획득 팝업 
                var bazookaIdx = ShopController.Bazooka.GetRewardBazookaIdx(Idx, Sheet);
                NewBazookaPopup.Show(bazookaIdx);
            });
        }

        public override void RefreshCellView()
        {
            // 노멀 초기화
            _base.Init(Idx, Sheet);
            _normal.Init(Idx, Sheet);
        }
    }
}