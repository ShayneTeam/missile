﻿using System.Collections.Generic;
using System.Linq;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;
using Utils.UI;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellBase))]
    public class ShopCellNormal : MonoBehaviour
    {
        [SerializeField] public List<IconAndText> descList;

        [SerializeField] public TextMeshProUGUI tipText;

        private ShopCellBase _base;

        
        public void Init(string idx, string sheet)
        {
            if (_base == null)
                _base = GetComponent<ShopCellBase>();
            
            // name
            _base.title.text = Localization.GetText(DataboxController.GetDataString(Table.Shop, sheet, idx, Column.name));

            // icon
            _base.icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Shop, sheet, idx, Column.icon));
            
            // reward
            var rewards = ShopController.GetRewards(idx, sheet);
            var i = 0;
            foreach (var rewardIdx in rewards)
            {
                // icon
                var icon = AtlasController.GetSprite(DataboxController.GetDataString(Table.Shop, Sheet.reward, rewardIdx, Column.icon));
                
                // desc
                var descFormat = DataboxController.GetDataString(Table.Shop, Sheet.reward, rewardIdx, Column.desc);
                var rewardCount = DataboxController.GetDataInt(Table.Shop, Sheet.reward, rewardIdx, Column.reward_count);
                var desc = Localization.GetFormatText(descFormat, rewardCount.ToLookUpString());
                
                // 세팅 
                descList[i].Init(icon, desc); 
                i++;
            }
            
            // desc_sub
            tipText.text = Localization.GetText(DataboxController.GetDataString(Table.Shop, sheet, idx, Column.desc_sub));
        }
    }
}