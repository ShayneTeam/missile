using System;
using EnhancedUI.EnhancedScroller;

namespace UI.Popup.Menu.Shop
{
    public abstract class ShopCell : EnhancedScrollerCellView
    {
        [NonSerialized] protected string Idx;
        [NonSerialized] protected string Sheet;
        
        public virtual void Init(string idx, string sheet)
        {
            Idx = idx;
            Sheet = sheet;
        }
        
        public abstract void OnClick();
    }
}