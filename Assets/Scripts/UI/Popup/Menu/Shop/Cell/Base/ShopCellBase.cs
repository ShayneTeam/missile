﻿using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using InGame;
using InGame.Controller;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Utils.UI;

namespace UI.Popup.Menu.Shop
{
    public class ShopCellBase : MonoBehaviour
    {
        [SerializeField] public TextMeshProUGUI title;
        [SerializeField] public Image icon;

        [SerializeField] public UnityEngine.UI.Button button;
        [SerializeField] public TextMeshProUGUI buttonBackText;

        [SerializeField] public TextMeshProUGUI buttonText;
        [SerializeField] public IconAndText buttonIconAndText;

        private CoroutineHandle _coolTime;
        private ButtonSpriteSwapper _buttonSpriteSwapper;

        private void Awake()
        {
            _buttonSpriteSwapper = button.GetComponent<ButtonSpriteSwapper>();
        }

        public void Init(string idx, string sheet)
        {
            ShopController.GetPriceInfo(idx, sheet, out var priceType, out var priceIcon, out var priceValue);

            // 먼저 꺼두기 
            buttonText.gameObject.SetActive(false);
            buttonIconAndText.gameObject.SetActive(false);

            switch (priceType)
            {
                case ShopController.PriceType.payment:
                {
                    buttonText.gameObject.SetActive(true);

                    // 가격 표시 
                    var priceText = ShopController.GetLocalizedPriceText(idx, sheet);
                    if (!string.IsNullOrEmpty(priceText))
                        buttonText.text = priceText;
                }
                    break;
                case ShopController.PriceType.gem:
                case ShopController.PriceType.mileage:
                case ShopController.PriceType.energy:
                case ShopController.PriceType.chipset:
                {
                    buttonIconAndText.gameObject.SetActive(true);

                    // 아이콘 & 필요량 
                    var spr = AtlasController.GetSprite(priceIcon);
                    var text = priceValue.ToString();
                    buttonIconAndText.Init(spr, text);
                }
                    break;
                case ShopController.PriceType.ad:
                {
                    buttonIconAndText.gameObject.SetActive(true);

                    // 아이콘 & 광고 시청
                    var spr = AtlasController.GetSprite(priceIcon);
                    var text = Localization.GetText("shop_03");
                    buttonIconAndText.Init(spr, text);
                }
                    break;
            }

            // 버튼 & 버튼 백 텍스트 온 오프 
            ShopController.CanPurchase(idx, sheet, out var reason);

            

            button.gameObject.SetActive(true);
            buttonBackText.gameObject.SetActive(false);
            
            switch (reason)
            {
                case ShopController.FailedReason.Free:
                {
                    // 무료 상품은 이미 구매돼있다고 처리 
                    button.gameObject.SetActive(false);
                }
                    break;
                case ShopController.FailedReason.NotFailed:
                {
                    _buttonSpriteSwapper.Init(true);
                    buttonIconAndText.text.color = Color.white;
                }
                    break;
                case ShopController.FailedReason.LimitedCount:
                {
                    // 구매 횟수 부족 시, 버튼 가리기 
                    button.gameObject.SetActive(false);
                    buttonBackText.gameObject.SetActive(true);
                }
                    break;
                case ShopController.FailedReason.NotEnoughMileage:
                case ShopController.FailedReason.NotEnoughGem:
                case ShopController.FailedReason.NotEnoughEnergy:
                case ShopController.FailedReason.NotEnoughChipset:
                {
                    _buttonSpriteSwapper.Init(false);
                    buttonIconAndText.text.color = Color.red;
                }
                    break;
                case ShopController.FailedReason.CoolTime:
                {
                    _buttonSpriteSwapper.Init(false);
                    
                    // 버튼 텍스트 쿨타임 보여주기 
                    buttonIconAndText.gameObject.SetActive(false);
                    buttonText.gameObject.SetActive(true);
                    
                    // 쿨타임 텍스트
                    Timing.KillCoroutines(_coolTime);
                    _coolTime = Timing.RunCoroutine(_CoolTimeFlow(idx, sheet).CancelWith(gameObject), Segment.RealtimeUpdate);
                }
                    break;
            }
        }
        
        private IEnumerator<float> _CoolTimeFlow(string idx, string sheet)
        {
            var remainCoolTime = ShopController.GetRemainCoolTime(idx, sheet);

            while (remainCoolTime > 0)
            {
                var min = remainCoolTime / 60;
                var sec = remainCoolTime % 60;

                var minStr = min.ToLookUpFormatString("00");
                var secStr = sec.ToLookUpFormatString("00");
                buttonText.text = $"{minStr}:{secStr}";
                
                yield return Timing.WaitForSeconds(1);
		        
                remainCoolTime -= 1;
            }
	        
            Init(idx, sheet);
        }
    }
}