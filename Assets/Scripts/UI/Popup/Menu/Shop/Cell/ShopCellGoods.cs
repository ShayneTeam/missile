﻿using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Firebase.Analytics;
using Global;
using Global.Extensions;
using InGame;
using InGame.Controller;
using InGame.Global;
using UnityEngine;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellNormal))]
    public class ShopCellGoods : ShopCell
    {
        protected ShopCellBase Base { get; private set; }
        protected ShopCellNormal Normal { get; private set; }

        private void Awake()
        {
            Base = GetComponent<ShopCellBase>();
            Normal = GetComponent<ShopCellNormal>();
        }

        public override void Init(string idx, string sheet)
        {
            base.Init(idx, sheet);
            
            RefreshCellView();
        }

        public override void OnClick()
        {
            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
                if (!success)
                {
                    // 구매 실패 메시지
                    ShopController.ShowFailedMessage(reason);
                    return;
                }

                WebLog.Instance.AllLog("purchase_limited", new Dictionary<string, object>{{"idx",Idx}});

                // 구매 성공 메시지 
                MessagePopup.Show("shop_24");
            });
        }

        public override void RefreshCellView()
        {
            // 노멀 초기화
            Base.Init(Idx, Sheet);
            Normal.Init(Idx, Sheet);
            
            // desc_sub
            var format = DataboxController.GetDataString(Table.Shop, Sheet, Idx, Column.desc_sub);
            var remainCount = ShopController.GetRemainCountToPurchase(Idx, Sheet);
            var limitedCount = ShopController.GetLimitedCount(Idx, Sheet);
            Normal.tipText.text = Localization.GetFormatText(format, remainCount.ToLookUpString(), limitedCount.ToLookUpString());
        }
    }
}