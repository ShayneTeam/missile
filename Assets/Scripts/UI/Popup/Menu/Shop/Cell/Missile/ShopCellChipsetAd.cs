﻿using System.Collections.Generic;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using UI.Popup.Menu.Shop.Gacha;
using UnityEngine;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellMissileBase))]
    public class ShopCellChipsetAd : ShopCell
    {
        private ShopCellMissileBase _missileBase;
        
        private void Awake()
        {
            _missileBase = GetComponent<ShopCellMissileBase>();
        }

        public override void Init(string idx, string sheet)
        {
            base.Init(idx, sheet);
            
            
            _missileBase.Init(idx, sheet);
            
            Refresh();
        }

        public override void OnClick()
        {
            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
        	    if (!success)
        	    {
        		    // 실패 메시지 
        		    ShopController.ShowFailedMessage(reason);
        		    return;
        	    }

        	    WebLog.Instance.AllLog("shop_ad_reward", new Dictionary<string, object>{{"idx",Idx}});

        	    // 광고로 획득 가능한 모든 횟수를 전부 소모한 경우
        	    if (ShopController.GetRemainCountToPurchase(Idx, Sheet) == 0)
        	    {
        		    WebLog.Instance.AllLog("shop_ad_reward_finished", new Dictionary<string, object>{{"idx",Idx}});
        	    }

                // 칩셋 획득 (이런 획득은 Purchase 안에서 이미 처리가 되고 들어오는게 맞음)
                ShopController.Chipset.Earn(Idx, Sheet);
                
                // 메시지 
                var successMessage = ShopController.Chipset.GetEarnSuccessMessage(Idx, Sheet);
                MessagePopup.ShowString(successMessage);
            });
        }

        public override void RefreshCellView()
        {
            Refresh();
        }

        private void Refresh()
        {
            _missileBase.Refresh();
            
            // desc_3
            var desc3Format = DataboxController.GetDataString(Table.Shop, Sheet, Idx, Column.desc_3);
            var rewardValue = ShopController.GetRewardValue(Idx, Sheet);
            _missileBase.descList[2].text = Localization.GetFormatText(desc3Format, rewardValue.ToLookUpString());
            
            // desc_sub
            var descSubFormat = DataboxController.GetDataString(Table.Shop, Sheet, Idx, Column.desc_sub);
            var remainCount = ShopController.GetRemainCountToPurchase(Idx, Sheet);
            var limitedCount = ShopController.GetLimitedCount(Idx, Sheet);
            _missileBase.descList[3].text = Localization.GetFormatText(descSubFormat, remainCount.ToLookUpString(), limitedCount.ToLookUpString());
        }
    }
}