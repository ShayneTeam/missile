using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Firebase.Analytics;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using MEC;
using UI.Popup.Menu.Shop.Gacha;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Shop
{
	[RequireComponent(typeof(ShopCellMissileBase))]
    public class ShopCellMissileAd : ShopCell
    {
	    private ShopCellMissileBase _missileBase;

	    private void Awake()
	    {
		    _missileBase = GetComponent<ShopCellMissileBase>();
	    }

	    public override void Init(string idx, string sheet)
	    {
		    base.Init(idx, sheet);
		    
		    
		    _missileBase.Init(idx, sheet);
		    
		    Refresh();
	    }

	    public override void OnClick()
	    {
		    ShopController.Purchase(Idx, Sheet, (success, reason) =>
		    {
			    if (!success)
			    {
				    // 실패 메시지 
				    ShopController.ShowFailedMessage(reason);
				    return;
			    }

			    WebLog.Instance.AllLog("shop_ad_reward", new Dictionary<string, object>{{"idx",Idx}});

			    // 광고로 획득 가능한 모든 횟수를 전부 소모한 경우
			    if (ShopController.GetRemainCountToPurchase(Idx, Sheet) == 0)
			    {
				    WebLog.Instance.AllLog("shop_ad_reward_finished", new Dictionary<string, object>{{"idx",Idx}});
			    }

			    // 가차 전개
			    ShopController.Missile.Gacha(Idx, Sheet, out var results);
			    
			    MissileGachaPopup.Show(results);
		    });
	    }

	    public override void RefreshCellView()
	    {
		    Refresh();
	    }

	    private void Refresh()
        {
	        _missileBase.Refresh();
	        
	        // desc_3
	        var desc3Format = DataboxController.GetDataString(Table.Shop, Sheet, Idx, "desc_3");
	        var rewardGachaCount = ShopController.GetGachaCount(Idx, Sheet);
	        _missileBase.descList[2].text = Localization.GetFormatText(desc3Format, rewardGachaCount.ToLookUpString());
	        
	        // desc_sub
	        var descSubFormat = DataboxController.GetDataString(Table.Shop, Sheet, Idx, "desc_sub");
	        var remainCount = ShopController.GetRemainCountToPurchase(Idx, Sheet);
	        var limitedCount = ShopController.GetLimitedCount(Idx, Sheet);
	        _missileBase.descList[3].text = Localization.GetFormatText(descSubFormat, remainCount.ToLookUpString(), limitedCount.ToLookUpString());
        }
    }
}