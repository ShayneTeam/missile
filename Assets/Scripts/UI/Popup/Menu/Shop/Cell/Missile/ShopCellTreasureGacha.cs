﻿using System.Collections.Generic;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using UI.Popup.Menu.Shop.Gacha;
using UI.Popup.Menu.Shop.InfinityStone;
using UnityEngine;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellMissileBase))]
    public class ShopCellTreasureGacha : ShopCell
    {
        private ShopCellMissileBase _base;
        
        private void Awake()
        {
            _base = GetComponent<ShopCellMissileBase>();
        }

        public override void Init(string idx, string sheet)
        {
            base.Init(idx, sheet);
            
            
            _base.Init(idx, sheet);
            
            Refresh();
        }

        public override void OnClick()
        {
            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
        	    if (!success)
        	    {
        		    // 실패 메시지 
        		    ShopController.ShowFailedMessage(reason);
        		    return;
        	    }

        	    WebLog.Instance.AllLog("shop_reward", new Dictionary<string, object>{{"idx",Idx}});
                
                // 뽑기 전개
                ShopController.Treasure.Gacha(Idx, Sheet, out var result);
                
                TreasureAutoGachaPopup.Show(result, Idx, Sheet);
            });
        }

        public override void RefreshCellView()
        {
            Refresh();
        }

        private void Refresh()
        {
            _base.Refresh();
            
            // desc_3
            var desc3Format = DataboxController.GetDataString(Table.Shop, Sheet, Idx, Column.desc_3);
            var rewardValue = ShopController.GetRewardValue(Idx, Sheet);
            _base.descList[2].text = Localization.GetFormatText(desc3Format, rewardValue.ToLookUpString());
            
            // desc_sub
            _base.descList[3].text = Localization.GetText(DataboxController.GetDataString(Table.Shop, Sheet, Idx, Column.desc_sub));
        }
    }
}