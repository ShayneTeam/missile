﻿using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;
using Utils.UI;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellBase))]
    public class ShopCellMissileBase : MonoBehaviour
    {
        [SerializeField] public Image iconSub; 
        [SerializeField] public List<TextMeshProUGUI> descList;


        [NonSerialized] public ShopCellBase Base;

        private string _sheet;
        private string _idx;

        private void Awake()
        {
	        Base = GetComponent<ShopCellBase>();
        }

        public void Init(string idx, string sheet)
        {
	        _sheet = sheet;
	        _idx = idx;

	        Refresh();
        }

        public void Refresh()
        {
	        Base.Init(_idx, _sheet);
	        
	        // name
	        Base.title.text = Localization.GetText(DataboxController.GetDataString(Table.Shop, _sheet, _idx, Column.name));

	        // icon
	        Base.icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Shop, _sheet, _idx, Column.icon));

	        // icon_gacha
	        iconSub.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Shop, _sheet, _idx, Column.icon_gacha));

	        // desc_1
	        descList[0].text = Localization.GetText(DataboxController.GetDataString(Table.Shop, _sheet, _idx, "desc_1"));

	        // desc_2
	        descList[1].text = Localization.GetText(DataboxController.GetDataString(Table.Shop, _sheet, _idx, "desc_2"));
        }
    }
}