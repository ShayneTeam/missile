﻿using System.Collections.Generic;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using UI.Popup.Menu.Shop.Gacha;
using UI.Popup.Menu.Shop.InfinityStone;
using UnityEngine;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellMissileBase))]
    public class ShopCellInfStoneGacha : ShopCell
    {
        private ShopCellMissileBase _missileBase;
        
        private void Awake()
        {
            _missileBase = GetComponent<ShopCellMissileBase>();
        }

        public override void Init(string idx, string sheet)
        {
            base.Init(idx, sheet);
            
            
            _missileBase.Init(idx, sheet);
            
            Refresh();
        }

        public override void OnClick()
        {
            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
        	    if (!success)
        	    {
        		    // 실패 메시지 
        		    ShopController.ShowFailedMessage(reason);
        		    return;
        	    }

        	    WebLog.Instance.AllLog("shop_reward", new Dictionary<string, object>{{"idx",Idx}});
                
                // 스톤 뽑기 전개
                InfStoneControl.StoneControl.Gacha(ShopController.GetGachaCount(Idx, Sheet), out var result);
                
                ShopInfStoneGachaPopup.Show(result, Idx, Sheet);
            });
        }

        public override void RefreshCellView()
        {
            Refresh();
        }

        private void Refresh()
        {
            _missileBase.Refresh();
            
            // desc_3
            var desc3Format = DataboxController.GetDataString(Table.Shop, Sheet, Idx, Column.desc_3);
            var rewardValue = ShopController.GetRewardValue(Idx, Sheet);
            _missileBase.descList[2].text = Localization.GetFormatText(desc3Format, rewardValue.ToLookUpString());
            
            // desc_sub
            _missileBase.descList[3].text = Localization.GetText(DataboxController.GetDataString(Table.Shop, Sheet, Idx, Column.desc_sub));
        }
    }
}