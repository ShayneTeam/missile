using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Firebase.Analytics;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UI.Popup.Menu.Shop.Gacha;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellMissileBase))]
    public class ShopCellMissileGacha : ShopCell
    {
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private SlicedFilledImage gaugeFill;
        [SerializeField] private TextMeshProUGUI gaugeText;

        private ShopCellMissileBase _missileBase;

        private void Awake()
        {
            _missileBase = GetComponent<ShopCellMissileBase>();
        }

        public override void Init(string idx, string sheet)
        {
            base.Init(idx, sheet);

            _missileBase.Init(idx, sheet);
            
            Refresh();
        }

        public override void OnClick()
        {
            // [튜토리얼] 무료뽑기 클릭 이벤트가 동작했으면, 구매 가격,조건등을 무시하고 한번 뽑게 함.
            if (Sheet == "missile" && TutorialPopup.IsPlaying &&
                !UserData.My.UserInfo.GetFlag(UserFlags.TutorialMissileComplete))
            {
                // 가차 전개
                ShopController.Missile.GachaWithLevelBonus(Idx, Sheet, out var results);
                MissileGachaPopup.Show(results);
                return;
            }

            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
                if (!success)
                {
                    // 실패 메시지 
                    ShopController.ShowFailedMessage(reason);
                    return;
                }
                
                WebLog.Instance.AllLog("shop_reward", new Dictionary<string, object>{{"idx",Idx}});

                // 가차 전개
                ShopController.Missile.GachaWithLevelBonus(Idx, Sheet, out var results);
                
                MissileAutoGachaPopup.Show(results, Idx, Sheet);
            });
        }

        public override void RefreshCellView()
        {
            Refresh();
        }

        [PublicAPI]
        public void ShowChances()
        {
            MissileGachaChanceShowingPopup.Show();
        }

        private void Refresh()
        {
            _missileBase.Refresh();
            
            // desc_3
            var desc2 = DataboxController.GetDataString(Table.Shop, Sheet, Idx, "desc_3");
            var rewardGachaCount = ShopController.GetGachaCount(Idx, Sheet);
            var gachaBonus = ShopController.Missile.GetGachaBonus();
            _missileBase.descList[2].text = Localization.GetFormatText(desc2, rewardGachaCount.ToLookUpString(), gachaBonus.ToLookUpString());
            
            // desc_sub
            // 현재 레벨 및 경험치 표시
            ShopController.MissileDelegator.GetGachaLevel(out var currentLevel, out var nextLevel);
            ShopController.MissileDelegator.GetGachaExp(out var prevLevelExp, out var currentExp, out var nextLevelExp);
	        
            // 레벨텍스트 
            levelText.text = Localization.GetFormatText("shop_01", currentLevel.ToLookUpString());
            // 게이지
            ExpController.GetExpProgress(prevLevelExp, currentExp, nextLevelExp, out var ratio, out var progress, out var max);
            
            gaugeFill.fillAmount = ratio;
            // 게이지 텍스트 
            gaugeText.text = currentExp >= nextLevelExp ? Localization.GetText("info_132") : Localization.GetFormatText("shop_02", progress.ToLookUpString(), max.ToLookUpString());
        }
    }
}