﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Firebase.Analytics;
using Global;
using Global.Extensions;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using UI.Popup.Menu.Bazooka;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Shop
{
    public class ShopCellPrincess : ShopCellGoods
    {
        [SerializeField] private UIButton equipButton;

        private SpriteChanger _buttonBg;
        
        public override void RefreshCellView()
        {
            base.RefreshCellView();
            
            // 상점용 아이콘이 아니므로, 원본 이미지 사이즈대로 세팅 함 (프리팹에서 스케일 미리 조정해 둠)
            Base.icon.SetSpriteWithOriginSIze(AtlasController.GetSprite(DataboxController.GetDataString(Table.Shop, Sheet, Idx, Column.icon)));
            
            // 구매 시, 장착 버튼 보여줌 
            var priceType = ShopController.GetPriceType(Idx, Sheet);
            var canEquip = ShopController.HasEverPurchased(Idx) || priceType == ShopController.PriceType.free;
            equipButton.gameObject.SetActive(canEquip);

            // 장착 버튼 세팅 
            if (!canEquip) return;
            
            RefreshEquipButton();
        }

        private void RefreshEquipButton()
        {
            var costumeIdx = GetCostumeIdx();
            if (string.IsNullOrEmpty(costumeIdx)) return;

            // 본격 체크
            var isWorn = InGameControlHub.My.PrincessController.IsWornCostume(costumeIdx);
            var hasCollected = UserData.My.Princess.HasCollectedCostume(costumeIdx);

            _buttonBg ??= equipButton.Button.image.GetComponent<SpriteChanger>();

            if (isWorn)
            {
                _buttonBg.Init((int)ButtonBg.Gray);
                equipButton.TextMeshProLabel.text = Localization.GetText("info_122");
            }
            else
            {
                if (hasCollected)
                {
                    _buttonBg.Init((int)ButtonBg.Yellow);
                    equipButton.TextMeshProLabel.text = Localization.GetText("info_123");
                }
                else
                {
                    _buttonBg.Init((int)ButtonBg.Blue);
                    equipButton.TextMeshProLabel.text = Localization.GetText("info_124");
                }
            }
        }

        public override void OnClick()
        {
            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
                if (!success)
                {
                    // 구매 실패 메시지
                    ShopController.ShowFailedMessage(reason);
                    return;
                }

                WebLog.Instance.AllLog("purchase_princess", new Dictionary<string, object>{{"idx",Idx}});

                // 구매 성공 메시지 
                MessagePopup.Show("shop_24");
            });
        }
        
        [PublicAPI]
        public void OnClickEquip()
        {
            var costumeIdx = GetCostumeIdx();
            if (string.IsNullOrEmpty(costumeIdx)) return;

            // 본격 체크
            var isWorn = InGameControlHub.My.PrincessController.IsWornCostume(costumeIdx);
            var isEarned = UserData.My.Princess.HasCollectedCostume(costumeIdx);
            if (isWorn || !isEarned) return;
            
            UserData.My.Princess.WearCostume(costumeIdx);
        }

        private string GetCostumeIdx()
        {
            // 이 상품 안에 공주 코스튬을 입었는지 체크 해야 함  
            // 구매 시, 바로 지급되는 우라늄 
            var rewardsData = ShopController.GetRewardsData(Idx, InGame.Global.Sheet.princess);

            // 예외 처리 
            if (rewardsData.Count == 0) return "";

            // 상품 중, 코스튬 데이터 찾기  
            var costume = rewardsData.FirstOrDefault(data => data.Type == RewardType.REWARD_GIRL_COSTUME);
            if (costume == null || string.IsNullOrEmpty(costume.Idx)) return "";

            return costume.Idx;
        }
    }
}