using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Firebase.Analytics;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UI.Popup.Menu.Shop.Gacha;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(ShopCellMissileBase))]
    public class ShopCellAncientEnergy : ShopCell
    {
        [SerializeField] private SlicedFilledImage gaugeFill;
        [SerializeField] private TextMeshProUGUI gaugeText;

        private ShopCellMissileBase _missileBase;

        private void Awake()
        {
            _missileBase = GetComponent<ShopCellMissileBase>();
        }

        public override void Init(string idx, string sheet)
        {
            base.Init(idx, sheet);

            _missileBase.Init(idx, sheet);
            
            Refresh();
        }

        public override void OnClick()
        {
            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
                if (!success)
                {
                    // 실패 메시지 
                    ShopController.ShowFailedMessage(reason);
                    return;
                }
                
                WebLog.Instance.AllLog("shop_reward", new Dictionary<string, object>{{"idx",Idx}});

                // 가차 전개
                ShopController.Missile.Gacha(Idx, Sheet, out var results);
                
                ShopAncientGachaPopup.Show(results, Idx, Sheet);
            });
        }

        public override void RefreshCellView()
        {
            Refresh();
        }

        private void Refresh()
        {
            _missileBase.Refresh();
            
            // desc_3
            _missileBase.descList[2].text = Localization.GetText(DataboxController.GetDataString(Table.Shop, Sheet, Idx, "desc_3"));
            
            // 게이지
            var priceValue = ShopController.GetPriceValue(Idx, Sheet);
            var curr = UserData.My.Missiles.AncientEnergy;
            gaugeFill.fillAmount = (float)curr / (float)priceValue;
            
            // 게이지 텍스트 
            gaugeText.text = Localization.GetFormatText("shop_02", curr.ToLookUpString(), priceValue.ToLookUpString());
        }
    }
}