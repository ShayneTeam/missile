﻿using System;
using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Firebase.Analytics;
using Global;
using Global.Extensions;
using InGame;
using InGame.Controller;
using InGame.Global;
using UnityEngine;

namespace UI.Popup.Menu.Shop
{
    public class ShopCellEvent : ShopCellGoods
    {
        public override void OnClick()
        {
            ShopController.Purchase(Idx, Sheet, (success, reason) =>
            {
                if (!success)
                {
                    // 구매 실패 메시지
                    ShopController.ShowFailedMessage(reason);
                    return;
                }

                WebLog.Instance.AllLog("purchase_event", new Dictionary<string, object>{{"idx",Idx}});

                // 구매 성공 메시지 
                MessagePopup.Show("shop_24");
            });
        }
    }
}