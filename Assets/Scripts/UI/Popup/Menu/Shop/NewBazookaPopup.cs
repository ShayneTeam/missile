﻿using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UI.Popup.Menu.Relic.Normal;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Shop
{
    [RequireComponent(typeof(UIPopup))]
    public class NewBazookaPopup : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI nameText;

        public static void Show(string bazookaIdx)
        {
            var popup = UIPopup.GetPopup("New Bazooka");

            var newBazookaPopup = popup.GetComponent<NewBazookaPopup>();
            newBazookaPopup.Init(bazookaIdx);
            
            popup.Show();
        }

        private void Init(string bazookaIdx)
        {
            // 아이콘 
            var sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, bazookaIdx, Column.resource_id));
            icon.SetSpriteWithOriginSIze(sprite);
            
            // 이름
            nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, bazookaIdx, Column.name));
        }
    }
}