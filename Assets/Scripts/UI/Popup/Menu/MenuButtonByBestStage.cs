using InGame.Controller;
using InGame.Data;
using UnityEngine;

namespace UI.Popup.Menu.Soldier
{
    [RequireComponent(typeof(NoticeIcon))]
    public class MenuButtonByBestStage : MonoBehaviour
    {
        [Header("해당 행성 시 부턴 노티스 아이콘 끔"),SerializeField] private int offPlanetIndex;
        
        private NoticeIcon _noticeIcon;
        
        private void Awake()
        {
            _noticeIcon = GetComponent<NoticeIcon>();
            
            // 이벤트
            UserData.My.Stage.OnChangedBestStage += OnChangedBestStage;
        }

        private void OnDestroy()
        {
            // 이벤트
            UserData.My.Stage.OnChangedBestStage -= OnChangedBestStage;
        }

        private void Start()
        {
            Refresh();
        }

        private void OnChangedBestStage(int bestStage)
        {
            Refresh();
        }

        private void Refresh()
        {
            var myBestPlanetIndex = StageControl.GetPlanetIndex(UserData.My.Stage.bestStage);
            var activeNotice = myBestPlanetIndex < 3;
            _noticeIcon.notice.SetActive(activeNotice);
        }
    }
}