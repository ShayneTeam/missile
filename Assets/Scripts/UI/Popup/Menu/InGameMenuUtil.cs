﻿using UI.Popup.Menu.Shop;

namespace UI.Popup.Menu
{
    public static class InGameMenuType
    {
        public const string ShopMenu = "Shop Menu";
    }

    public static class InGameMenuUtil
    {
        public static void OpenShop(ShopTab shopTab)
        {
            var menu = InGameMenu.Open(InGameMenuType.ShopMenu);
            menu.GetComponent<ShopMenuPopup>().ChangeTab(shopTab);
        }
    }
}