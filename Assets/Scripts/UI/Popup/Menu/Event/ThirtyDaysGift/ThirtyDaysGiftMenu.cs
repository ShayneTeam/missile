﻿using System;
using System.Collections.Generic;
using InGame;
using InGame.Controller;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup;
using UnityEngine;
using Utils;

public class ThirtyDaysGiftMenu : MonoBehaviour
{
    [Header("구매 시, 즉시 우라늄 보상"), SerializeField] private TextMeshProUGUI rewardPurchasedUraniumText;
    
    [Header("구매 시, 누적 우라늄 보상"), SerializeField] private TextMeshProUGUI rewardSumUraniumText;
    [Header("구매 시, 누적 강화칩 보상"), SerializeField] private TextMeshProUGUI rewardSumChipText;
    
    [Header("구매 시, 매일 우라늄 보상"), SerializeField] private TextMeshProUGUI rewardDailyUraniumText;
    [Header("구매 시, 매일 강화칩 보상"), SerializeField] private TextMeshProUGUI rewardDailyChipText;
    
    [Serializable]
    public class PurchaseSchema
    {
        public GameObject root;
        public TextMeshProUGUI priceText;
    }
    [SerializeField] private PurchaseSchema purchase;
    
    [Serializable]
    public class CollectSchema
    {
        public GameObject root;
        public ButtonSpriteSwapper buttonSpriteSwapper;
        public TextMeshProUGUI buttonLabel;
        public TextMeshProUGUI collectableCount;
        public TextMeshProUGUI remainDays;
    }

    [SerializeField] private CollectSchema collect;
    
    private CoroutineHandle _autoRefresh;

    // 이벤트 메뉴 메카니즘이 탭 바뀌면 메뉴 SetActive로 껐다 켰다 함 
    // 메뉴 켜지면 알아서 초기화 되게 OnEnable에서 처리 
    private void OnEnable()
    {
        Init();
        
        // 주기 호출 전, 즉각 갱신
        Refresh();

        // 이벤트 받는 것보다 이게 더 심플해서 이렇게 함
        _autoRefresh = Timing.CallPeriodically(float.MaxValue,1f, Refresh, Segment.RealtimeUpdate);
    }

    private void OnDisable()
    {
        Timing.KillCoroutines(_autoRefresh);
    }

    private void Init()
    {
        // 구입 시, 즉각 지급 우라늄 보상
        rewardPurchasedUraniumText.text = RewardType.GetString(RewardType.REWARD_GEM, ThirtyDaysGiftController.RewardUraniumIfPurchase);

        // 매일 누적 보상 
        ThirtyDaysGiftController.GetSumRewardAllDays(out var sumUranium, out var sumChip);

        rewardSumUraniumText.text = RewardType.GetString(RewardType.REWARD_GEM, sumUranium);
        rewardSumChipText.text = sumChip.ToLookUpString();

        // 매일 우라늄 보상 
        rewardDailyUraniumText.text = Localization.GetFormatText("info_136", RewardType.GetString(RewardType.REWARD_GEM, ThirtyDaysGiftController.DailyRewardUranium));

        // 매일 강화칩 보상 
        rewardDailyChipText.text = Localization.GetFormatText("info_136", ThirtyDaysGiftController.DailyRewardChip.ToLookUpString());
    }

    private void Refresh()
    {
        // 구매 & 획득 스위칭 
        var canCollect = ThirtyDaysGiftController.CanCollect;
        var isClosed = ThirtyDaysGiftController.IsClosed;
        
        // 종료일이 지나도, 획득 못한 건 획득할 수 있어야 함 
        var visibleCollect = canCollect || !isClosed;   
        purchase.root.SetActive(!visibleCollect);
        collect.root.SetActive(visibleCollect);

        // 획득 버튼 플로우
        if (visibleCollect) 
        {
            // 버튼 스프라이트 & 라벨링
            collect.buttonSpriteSwapper.Init(canCollect);
            if (canCollect)
            {
                // 획득 가능
                collect.buttonLabel.text = Localization.GetText("info_188");
            }
            else
            {
                // 오늘 초기화 타임까지 남은 시간 반환 
                collect.buttonLabel.text = ThirtyDaysGiftController.GetRemainTimeTextToCollectNext();
            }
            
            // 획득 가능 갯수 
            collect.collectableCount.text = Localization.GetFormatText("shop_35",ThirtyDaysGiftController.CollectableCount.ToLookUpString());

            // 남은 날짜 
            collect.remainDays.text = Localization.GetFormatText("shop_36",ThirtyDaysGiftController.RemainDays.ToLookUpString());
        }
        // 구매 버튼 플로우
        else 
        {
            // 가격 표시 
            purchase.priceText.text = ThirtyDaysGiftController.LocalizedPriceText;
        }
    }

    [PublicAPI]
    public void Purchase()
    {
        // 구매 
        ThirtyDaysGiftController.Purchase((success) =>
        {
            if (!success) return;
            
            // 갱신
            Refresh();
        });
    }

    [PublicAPI]
    public void Collect()
    {
        // 획득 처리 
        if (ThirtyDaysGiftController.Collect())
            // 획득 성공 시, 갱신 
            Refresh();
        else
            // 획득 실패 시, 기다리라는 메시지
            MessagePopup.Show("info_380");
    }
}
