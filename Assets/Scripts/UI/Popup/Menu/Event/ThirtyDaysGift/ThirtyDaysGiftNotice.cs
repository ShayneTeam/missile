using System;
using InGame.Controller;
using InGame.Data;
using MEC;
using UnityEngine;

namespace UI.Popup.Menu.Event.ThirtyDaysGift
{
    [RequireComponent(typeof(NoticeIcon))]
    public class ThirtyDaysGiftNotice : MonoBehaviour
    {
        private NoticeIcon _notice;
        private CoroutineHandle _autoRefresh;

        private void Awake()
        {
            _notice = GetComponent<NoticeIcon>();
        }

        private void OnEnable()
        {
            // 주기 호출 전, 즉각 갱신
            Refresh();
            
            // 이벤트 처리보다, 1초 마다 갱신하는 게 심플하고 정확해서 이렇게 함 
            _autoRefresh = Timing.CallPeriodically(float.MaxValue,1f, Refresh, Segment.RealtimeUpdate);
        }

        private void OnDisable()
        {
            Timing.KillCoroutines(_autoRefresh);
        }

        private void Refresh()
        {
            _notice.notice.SetActive(ThirtyDaysGiftController.CanCollect); 
        }
    }
}