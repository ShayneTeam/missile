﻿using JetBrains.Annotations;
using UI.Popup.Menu.Shop;
using UnityEngine;

namespace UI.Popup.Menu.Event.Slot
{
    public class MysteryChanceButton : MonoBehaviour
    {
        [PublicAPI]
        public void OnClick()
        {
            MysteryChancePopup.Show();
        }
    }
}