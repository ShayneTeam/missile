﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Bolt;
using Databox;
using DG.Tweening;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller.Event;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using Server;
using TMPro;
using UI.Popup.Menu.Shop;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Event.Slot
{
    public class AttendanceEventSlot : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI dayText;

        [SerializeField] private Image icon;

        [SerializeField] private GameObject effect;

        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI descText;

        [SerializeField] private UnityEngine.UI.Button button;

        private int slotDay;

        // 보상과 관련된 정보
        private string rewardType;
        private int rewardCount;

        private bool canReceiveReward = false;

        public void Setting(int day)
        {
            slotDay = day;
            canReceiveReward = false;

            var idx = $"33000{slotDay.ToLookUpString()}";

            // day
            var dayName = DataboxController.GetDataString(Table.Event, Sheet.attendance, idx, Column.name);
            dayText.text = Localization.GetText(dayName);

            // 리워드 타입
            rewardType = DataboxController.GetDataString(Table.Event, Sheet.attendance, idx, Column.reward_type);
            rewardCount = DataboxController.GetDataInt(Table.Event, Sheet.attendance, idx, Column.reward_count);

            // 이미지
            var iconPath = DataboxController.GetDataString(Table.Reward, Sheet.reward, rewardType, Column.icon);
            icon.SetSpriteWithOriginSIze(AtlasController.GetSprite(iconPath));

            // 아이템 이름 
            var itemName = DataboxController.GetDataString(Table.Reward, Sheet.reward, rewardType, Column.name);
            nameText.text = Localization.GetText(itemName);

            // 아이템 갯수
            descText.text = Localization.GetFormatText("shop_10", RewardType.GetString(rewardType, rewardCount));
            
            // 이미 받은 거 전부 비활성화
            if (UserData.My.Event.AttendanceCount >= day)
            {
                button.gameObject.SetActive(false);
            }

            if (UserData.My.Event.AttendanceCount + 1 == day &&
                (UserData.My.Event.NextAttendanceTimestamp == 0 && day == 1 || // 첫번째 슬롯이면서 수령한 적이 없거나
                day != 1 &&  ServerTime.Instance.NowUnscaled.ToTimestamp() > UserData.My.Event.NextAttendanceTimestamp)) // 다음에 받을 수 있는 시간-현재 서버시간의 날짜가 1일이상 차이가 날 경우. 
            {
                canReceiveReward = true;
            }
            
            button.GetComponent<ButtonSpriteSwapper>().Init(canReceiveReward);
        }

        [PublicAPI]
        public void OnClicked()
        {
            if (!canReceiveReward)
            {
                MessagePopup.Show("shop_21");
                return;
            }
            
            // 출석 보상 수령
            EventController.NAttendance.Claim(rewardType, rewardCount);
            
            MessagePopup.Show("shop_28");
            
            // 갱신
            EventMenuPopup.Instance.Refresh();
            AttendanceMenu.Instance.Refresh();
        }
    }
}
