using Bolt;
using DG.Tweening;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Controller.Event;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using Server;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Bazooka
{
    public class HuntershopRewardSlot : EnhancedScrollerCellView
    {
	    [SerializeField] private TextMeshProUGUI dayText;

	    [SerializeField] private Image icon;

	    [SerializeField] private TextMeshProUGUI nameText;
	    [SerializeField] private TextMeshProUGUI descText;
	    [SerializeField] private TextMeshProUGUI priceText;

	    [SerializeField] private ButtonSpriteSwapper button;
        
        private string rewardType;
        private string rewardIdx;
        private int rewardCount;
        private int price;
        
        private void Start()
        {
	        UserData.My.Resources.OnChangedKillpoint += OnChangedKillpoint;
        }

        private void OnDestroy()
        {
	        UserData.My.Resources.OnChangedKillpoint -= OnChangedKillpoint;
        }
        
        private void OnChangedKillpoint()
        {
	        Refresh();
        }

        public void Init(string rewardId)
        {
	        // 아이콘 
	        var iconPath = DataboxController.GetDataString(Table.Event, Sheet.EventShop, rewardId, Column.icon);
	        var sprite = AtlasController.GetSprite(iconPath);
	        icon.SetSpriteWithOriginSIze(sprite);
	        
	        // 애니메이션
	        var aniType = DataboxController.GetDataString(Table.Event, Sheet.EventShop, rewardId, Column.icon_ani);
	        if (aniType == "up_down")
	        {
		        icon.transform.DORewind();
		        icon.transform.DOLocalMoveY(20f, 0.6f).SetLoops(-1, LoopType.Yoyo);
	        }

	        // 상단 설명
	        var nameKey = DataboxController.GetDataString(Table.Event, Sheet.EventShop, rewardId, Column.name);
	        dayText.text = Localization.GetText(nameKey);
	        
	        // 아이템 이름 
	        var itemName = DataboxController.GetDataString(Table.Event, Sheet.EventShop, rewardId, Column.reward_desc);
	        nameText.text = Localization.GetText(itemName);
	        
	        // 리워드 타입
	        rewardType = DataboxController.GetDataString(Table.Event, Sheet.EventShop, rewardId, Column.reward_type);
	        rewardCount = DataboxController.GetDataInt(Table.Event, Sheet.EventShop, rewardId, Column.reward_count);
	        rewardIdx = DataboxController.GetDataString(Table.Event, Sheet.EventShop, rewardId, Column.reward_idx);
	        
	        // 아이템 갯수
	        descText.text = Localization.GetFormatText("shop_10", RewardType.GetString(rewardType, rewardCount));
	        
	        // 가격
	        price = DataboxController.GetDataInt(Table.Event, Sheet.EventShop, rewardId, Column.price_count);
	        priceText.text = RewardType.GetString(rewardType, price);
	        
	        // 
	        Refresh();
        }
        
        private void Refresh()
        {
	        button.Init(CheckIfCanPurchase());
        }

        private bool CheckIfCanPurchase()
        {
	        return InGameControlHub.My.Data.Resources.killpoint >= price;
        }

        [PublicAPI]
        public void Purchase()
        {
	        if (!CheckIfCanPurchase())
	        {
		        MessagePopup.Show("shop_16");
		        return;
	        }
	        
	        // 보상 수령
	        EventController.Instance.HunterShopReceive(rewardType, rewardCount, price, rewardIdx);
	        
	        // 구매 다시 가능한 상태인지 Refresh
	        Refresh();

	        // 재화 Refresh
	        HuntershopMenu.Instance.RefreshKillPoint();
        }

        public override void RefreshCellView()
        {
	        Refresh();
        }
    }
}