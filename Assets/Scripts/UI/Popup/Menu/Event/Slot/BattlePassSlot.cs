using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller.Event;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using Server;
using TMPro;
using UI.Popup;
using UI.Popup.Menu.Event;
using UnityEngine;
using UnityEngine.UI;
using Utils;

public class BattlePassSlot : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI spaceTitle;
    
    [SerializeField] private Image spaceIcon;
    [SerializeField] private TextMeshProUGUI uraniumText;        

    [SerializeField] private Button button;
    [SerializeField] private Image buttonLockIcon;
    [SerializeField] private TextMeshProUGUI buttonLabelText;

    /* 슬롯 정보들 */
    private string slotBattlePassStr;
    private string slotSheetName;
    private string slotIdx;
    
    public void Init(string battlePassType, string sheetName, string idx)
    {
        // slot info
        slotBattlePassStr = battlePassType;
        slotSheetName = sheetName;
        slotIdx = idx;
        
        // 행성 이름
        var spaceName = DataboxController.GetDataString(Table.Event, sheetName, idx, Column.name);
        spaceTitle.text = Localization.GetText(spaceName);
        
        // 행성 아이콘
        var icon = DataboxController.GetDataString(Table.Event, sheetName, idx, Column.icon);
        spaceIcon.sprite = AtlasController.GetSprite(icon);
        
        // 보상 수량
        var count =  DataboxController.GetDataInt(Table.Event, sheetName, idx, Column.reward_count);
        uraniumText.text = count.ToLookUpString();
            
        // 구매여부
        var purchase = UserData.My.BattlePass.PurchasedCheck(battlePassType);
        
        // 무료로 얻을 수 있는 상품인지
        var itemLock = DataboxController.GetDataString(Table.Event, sheetName, idx, "lock", "TRUE");

        // 구매 안되어있으면 자물쇠 활성화
        buttonLockIcon.gameObject.SetActive(!purchase && itemLock == "TRUE");

        var condition = DataboxController.GetDataInt(Table.Event, sheetName, idx, "m_value", 99999);
        
        // 이미 수령했는지
        var receivedCheck = UserData.My.BattlePass.ReceivedCheck(slotBattlePassStr, condition);
        button.gameObject.SetActive(!receivedCheck);
        
        // 획득할 수 있는 조건을 만족했다면
        var conditionClearCheck = EventController.Instance.HasPreEventBattlePassToReceive(slotBattlePassStr, sheetName, idx, condition);
        button.GetComponent<ButtonSpriteSwapper>().Init(conditionClearCheck);
        
        // 조건 만족 못했다면 조건에 필요한 스테이지를 표시해줌.
        if (!conditionClearCheck)
            buttonLabelText.text = $"{condition.ToLookUpString()}";
        else
        {
            buttonLabelText.text = Localization.GetText("shop_20");
        }
    }
    
    [PublicAPI]
    public void Receive()
    {
        var condition = DataboxController.GetDataInt(Table.Event, slotSheetName, slotIdx, "m_value", 99999);
        
        var preConditionClearCheck = EventController.Instance.HasPreEventBattlePassToReceive(slotBattlePassStr, slotSheetName, slotIdx, condition);
        var conditionClearCheck = EventController.Instance.HasEventBattlePassToReceive(slotBattlePassStr, slotSheetName, slotIdx, condition);

        // 조건은 만족했는데 구매가 안된 경우
        if (preConditionClearCheck && !conditionClearCheck)
        {
            MessagePopup.Show("shop_22");
            return;
        }
        
        // 조건을 만족하지 못한 경우
        if (!conditionClearCheck)
        {
            MessagePopup.Show("shop_23");
            return;
        }
        
        // 배틀패스 클리어처리 및 보상 지급
        EventController.Instance.BattlePassToReceive(slotBattlePassStr, slotSheetName, slotIdx, condition);

        // 갱신
        EventMenuPopup.Instance.Refresh();
        Init(slotBattlePassStr, slotSheetName, slotIdx);
    }
}
