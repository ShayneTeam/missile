using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Firebase.Analytics;
using Global;
using InGame;
using InGame.Controller;
using InGame.Controller.Event;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using Server;
using TMPro;
using UI.Popup;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

public static class BattlePassTypeExtensions
{
    public static string ToEnumString(this BattlePassMenu.BattlePassType type)
    {
        return type switch
        {
            BattlePassMenu.BattlePassType.None => nameof(BattlePassMenu.BattlePassType.None),
            BattlePassMenu.BattlePassType.SpaceA => nameof(BattlePassMenu.BattlePassType.SpaceA),
            BattlePassMenu.BattlePassType.SpaceB => nameof(BattlePassMenu.BattlePassType.SpaceB),
            BattlePassMenu.BattlePassType.MonsterHunterA => nameof(BattlePassMenu.BattlePassType.MonsterHunterA),
            BattlePassMenu.BattlePassType.MonsterHunterB => nameof(BattlePassMenu.BattlePassType.MonsterHunterB),
            _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
        };
    }
}

public class BattlePassMenu : MonoBehaviour
{
    [SerializeField] private BattlePassType _battlePassType = BattlePassType.None;

    [Header("즉시 구매 지급 보상")] [SerializeField]
    private TextMeshProUGUI purchaseRewardText;

    [Header("최대 달성 보상")] [SerializeField] private TextMeshProUGUI maxRewardText;

    [SerializeField] private TextMeshProUGUI spaceTitle;
    [SerializeField] private TextMeshProUGUI spaceDesc;
    [SerializeField] private TextMeshProUGUI bestStageDesc;

    [FormerlySerializedAs("_battlePassSpacePlanSlots")] [SerializeField]
    private List<BattlePassSlot> _battlePassSlots = new List<BattlePassSlot>();

    [SerializeField] private Button purchaseButton;
    [SerializeField] private TextMeshProUGUI purchaseText;

    private string _sheetName;
    private string _firstIdx;
    private string _shopIdx;
    
    public enum BattlePassType
    {
        None,
        SpaceA,
        SpaceB,
        MonsterHunterA,
        MonsterHunterB,
    }

    public void Init()
    {
        _sheetName = string.Empty;
        ICollection<string> idxList = null;

        // 탭 기본 정보들 갱신
        switch (_battlePassType)
        {
            case BattlePassType.None:
                break;
            case BattlePassType.SpaceA:
                spaceTitle.text = Localization.GetText("shop_evt_03");
                
                _sheetName = Sheet.BattlePass_1;
                idxList = DataboxController.GetAllIdxes(Table.Event, _sheetName);

                // 내용 (공통)
                spaceDesc.text = Localization.GetText("shop_evt_05");
                break;
            case BattlePassType.SpaceB:
                spaceTitle.text = Localization.GetText("shop_evt_04");

                _sheetName = Sheet.BattlePass_2;
                idxList = DataboxController.GetAllIdxes(Table.Event, _sheetName);
                
                // 내용 (공통)
                spaceDesc.text = Localization.GetText("shop_evt_05");
                break;
            case BattlePassType.MonsterHunterA:
                spaceTitle.text = Localization.GetText("shop_evt_06");
                
                _sheetName = Sheet.MonsterHunter_1;
                idxList = DataboxController.GetAllIdxes(Table.Event, _sheetName);
                
                // 내용 (공통)
                spaceDesc.text = Localization.GetText("shop_evt_07");
                break;
            default:
                throw new ArgumentOutOfRangeException();
        }

        if (idxList == null)
            return;

        _firstIdx = idxList.First();
        
        _shopIdx = DataboxController.GetDataString(Table.Event, _sheetName,
            _firstIdx, Column.shop_idx);

        // 즉시 구매 보상
        var purchaseRewardCount = DataboxController.GetDataInt(Table.Event, _sheetName,
            _firstIdx, Column.reward_count);
        
        purchaseRewardText.text = purchaseRewardCount.ToLookUpString();

        // 획득 가능한 전체 보상의 수량
        var totalReward = 0;
        for (var i = 0; i < idxList.Count; i++)
        {
            var idx = idxList.ElementAt(i);
            totalReward +=
                DataboxController.GetDataInt(Table.Event, _sheetName, idx, Column.reward_count);

            // 어차피 설정 필요해서, 아이디 있는곳에서 루프 돌림.
            _battlePassSlots[i].Init(_battlePassType.ToEnumString(), _sheetName, idx);
        }

        // 최대 보상
        maxRewardText.text = totalReward.ToLookUpString();

        // 현재 미션 횟수
        UserData.My.BattlePass.CommonConditionCheck(_sheetName, _firstIdx, 0, out var count);
        bestStageDesc.text = count.ToLookUpString();

        // 구매여부
        var purchase = UserData.My.BattlePass.PurchasedCheck(_battlePassType.ToEnumString());

        // 구매되어 있지 않으면 구매버튼 활성화
        purchaseButton.gameObject.SetActive(!purchase);
        
        // 가격
        var priceText = ShopController.GetLocalizedPriceText(_shopIdx, Sheet.BattlePass);
        if (!string.IsNullOrEmpty(priceText))
            purchaseText.text = priceText;
    }

    public void Purchase()
    {
        var purchase = UserData.My.BattlePass.PurchasedCheck(_battlePassType.ToEnumString());
        
        // 구매를 했는데 또 구매하면 오류.
        if (purchase) return;
        
        ShopController.Purchase(_shopIdx, Sheet.BattlePass, (success, reason) =>
        {
            if (!success)
            {
                // 구매 실패 메시지 
                ShopController.ShowFailedMessage(reason);
                return;
            }

            WebLog.Instance.ThirdPartyLog($"purchase_battle_pass_{_battlePassType.ToEnumString()}");
            
            // 배틀패스 적용 및 구매보상 지급
            EventController.Instance.PurchaseApplyBattlePass(_battlePassType.ToEnumString(), _sheetName, _firstIdx);
            
            // 구매 성공 메시지 
            MessagePopup.Show("shop_24");
            
            // 갱신
            Init();
        });
    }
}