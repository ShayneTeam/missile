using System;
using System.Collections;
using System.Collections.Generic;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using Server;
using TMPro;
using UI.Popup.Menu.Event.Slot;
using UnityEngine;
using UnityEngine.SocialPlatforms;
using UnityEngine.UI;

public class AttendanceMenu : MonoBehaviourSingleton<AttendanceMenu>
{
    // N 일간의 선물 부분
    [SerializeField] private TextMeshProUGUI attendanceMenuTitle;
    [SerializeField] private TextMeshProUGUI attendanceMenuDesc;

    // 출석 스템프 부분
    [SerializeField] private TextMeshProUGUI stampTitle;
    [SerializeField] private TextMeshProUGUI stampDesc;

    // 슬롯들
    [SerializeField] private List<AttendanceEventSlot> _attendanceEventSlots = new List<AttendanceEventSlot>();

    public void Init()
    {
        // N 일간의 선물 Title
        attendanceMenuTitle.text = Localization.GetText("shop_18");
        // N 일간의 선물 Desc
        attendanceMenuDesc.text = Localization.GetText("shop_19");
        
        // stampTitle
        stampTitle.text = Localization.GetText("shop_27");
        
        Refresh();
    }

    public void Refresh()
    {
        // 타임스템프 현재 카운트 Refresh 필요
        stampDesc.text = $"<color=yellow>{UserData.My.Event.AttendanceCount.ToLookUpString()}</color> / 7";

        // Day 7 상품 셋팅 및 초기화
        for (var i = 0; i < 7; i++)
        {
            _attendanceEventSlots[i].Setting(i + 1);
        }
    }
}