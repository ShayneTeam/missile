using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UI.Popup.Menu.Bazooka;
using UnityEngine;

public class HuntershopMenu : MonoBehaviourSingleton<HuntershopMenu>, IEnhancedScrollerDelegate
{
    // 헌터상점 제목,내용
    [SerializeField] private TextMeshProUGUI huntershopMenuTitle;
    [SerializeField] private TextMeshProUGUI huntershopMenuDesc;

    // 수량
    [SerializeField] private TextMeshProUGUI killpointText;
    
    [SerializeField] private EnhancedScroller scroller;
    [SerializeField] private EnhancedScrollerCellView cellPrefab;
    [SerializeField] private EnhancedScrollerCellView cellBoxPrefab;
    [SerializeField] private float cellSize;

    private ICollection<string> _allRewardIdxes;

    private void Start()
    {
        UserData.My.Resources.OnChangedKillpoint += RefreshKillPoint;
    }

    private void OnDestroy()
    {
        UserData.My.Resources.OnChangedKillpoint += RefreshKillPoint;
    }

    public void Init()
    {
        huntershopMenuTitle.text = Localization.GetText("shop_14");
        huntershopMenuDesc.text = Localization.GetText("shop_15");
        
        RefreshKillPoint();
        
        //
        _allRewardIdxes = DataboxController.GetAllIdxes(Table.Event, Sheet.EventShop);

        scroller.Delegate = this;
        scroller.ReloadData();
    }

    public void RefreshKillPoint()
    {
        killpointText.text = $"<color=yellow>{UserData.My.Resources.killpoint.ToLookUpString()}</color> / 999";
    }

    public int GetNumberOfCells(EnhancedScroller _)
    {
        return _allRewardIdxes.Count;
    }

    public float GetCellViewSize(EnhancedScroller _, int dataIndex)
    {
        return cellSize;
    }

    public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
    {
        var rewardIdx = _allRewardIdxes.ElementAt(dataIndex);
        var rewardType = DataboxController.GetDataString(Table.Event, Sheet.EventShop, rewardIdx, Column.reward_type);
        
        var prefab = rewardType == RewardType.REWARD_BOX ? cellBoxPrefab : cellPrefab;
        
        // 셀뷰 초기화 
        var cellView = scroller.GetCellView(prefab);
        if (cellView == null)
            return null;

        var slot = cellView.GetComponent<HuntershopRewardSlot>();
        slot.Init(rewardIdx);

        return cellView;
    }
}
