using System;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using InGame.Controller.Event;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Event
{
    public enum EventTab
    {
        HunterShop,
        BattlePass1,
        BattlePass2,
        BattlePass3,
        Attendance,
        ThirtyDaysGift,
    }
    
    public class EventMenuPopup : MonoBehaviourSingleton<EventMenuPopup>
    {
        [SerializeField] private List<EventMenuUIClass> _menus = new List<EventMenuUIClass>();

        [Serializable]
        private class EventMenuUIClass
        {
            public Toggle tab;
            public TextMeshProUGUI tabNameText;
            public GameObject tabNotice;
            public GameObject menu;
        }

        // 처음 초기화 시, 디폴트 탭으로 강제 처리하기 위해 -1로 둠 
        private int? lastOpenEventTab = -1;

        private UIPopup _popup;
        
        private bool _isInit;
        private CoroutineHandle _switchTab;

        public static UIPopup Show()
        {
            var popup = UIPopup.GetPopup("Event Menu");
            popup.Show();
            
            return popup;
        }

        public static UIPopup Show(EventTab tab)
        {
            var popup = Show();

            var eventPopup = popup.GetComponent<EventMenuPopup>();
            eventPopup.SwitchTab(tab);

            return popup;
        }

        [PublicAPI]
        public void Init()
        {
            if (_popup == null)
                _popup = GetComponent<UIPopup>();
            
            Refresh();

            // Show로 여는 게 아닌, Open Menu로 '처음' 열었을 때, 처리 
            if (!_isInit && !_switchTab.IsRunning)
                SwitchTab(EventTab.HunterShop);

            _isInit = true;
        }

        public void Refresh()
        {
            // 모두 끄고
            foreach (var x in _menus.Where(x => x.tabNotice != null))
            {
                x.tabNotice.gameObject.SetActive(false);
            }
            
            // 배틀패스 1 받을 수 있는 경우 버튼
            _menus[1].tabNotice.SetActive(EventController.Instance.HasBattlePassToReceive(BattlePassMenu.BattlePassType.SpaceA));
            
            // 배틀패스 2 받을 수 있는 경우 버튼
            _menus[2].tabNotice.SetActive(EventController.Instance.HasBattlePassToReceive(BattlePassMenu.BattlePassType.SpaceB));
            
            // 몬스터 헌터 1 받을 수 있는 경우 버튼
            _menus[3].tabNotice.SetActive(EventController.Instance.HasBattlePassToReceive(BattlePassMenu.BattlePassType.MonsterHunterA));

            // 출석보상 받을 수 있는 경우 버튼
            _menus[4].tabNotice.SetActive(EventController.NAttendance.CanClaim);
        }

        private void SwitchTab(EventTab tab)
        {
            Timing.KillCoroutines(_switchTab);
            _switchTab = Timing.RunCoroutine(_SwitchTabFlow(tab).CancelWith(gameObject));
        }
        
        private IEnumerator<float> _SwitchTabFlow(EventTab tab)
        {
            // Init 될 때까지 대기 
            while (!_isInit) yield return Timing.WaitForOneFrame;
            
            // 변경 
            SwitchTab((int)tab);
        }

        [PublicAPI]
        public void SwitchTab(int index)
        {
            // 현재 탭과 같다면 추가 작업 하지 않음.
            if (lastOpenEventTab == index) return;
            
            // 메뉴 다 끄기 
            foreach (var x in _menus) x.menu.SetActive(false);

            // 선택된 메뉴만 켜기 
            lastOpenEventTab = index;
            _menus[index].tab.isOn = true;

            var targetMenu = _menus[index].menu;
            targetMenu.gameObject.SetActive(true);

            switch (index)
            {
                case 0:
                    targetMenu.GetComponent<HuntershopMenu>().Init();
                    break;
                case 1:
                    targetMenu.GetComponent<BattlePassMenu>().Init();
                    break;
                case 2:
                    targetMenu.GetComponent<BattlePassMenu>().Init();
                    break;
                case 3:
                    targetMenu.GetComponent<BattlePassMenu>().Init();
                    break;
                case 4:
                    targetMenu.GetComponent<AttendanceMenu>().Init();
                    break;
            }
        }
    }
}