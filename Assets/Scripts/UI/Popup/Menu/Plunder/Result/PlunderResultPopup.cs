﻿using System.Collections;
using System.Collections.Generic;
using BackEnd;
using Doozy.Engine.UI;
using InGame.Data;
using InGame.Data.Scene;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup.Menu.Shop.Gacha;
using UnityEngine;

namespace UI.Popup.Plunder
{
    public class PlunderResultPopup : MonoBehaviour
    {
        [SerializeField] private PlunderUserResult resultMe;
        [SerializeField] private PlunderUserResult resultTargetUser;
        [Header("약탈한 양 퍼센티지"), SerializeField] private TextMeshProUGUI percentText;
        [Header("연출 딜레이"), SerializeField] private float delayDirection;
        [Header("연출 시간"), SerializeField] private float durationDirection;
        [Header("인피니티 스톤 보상"), SerializeField] private ShopInfStoneGachaCell infStone;
        
        
        private CoroutineHandle _direction;

        public static UIPopup Show()
        {
            var popup = UIPopup.GetPopup("Plunder Result");
            popup.Show();

            return popup;
        }
        
        [PublicAPI]
        public void OnShow()
        {
            _direction = Timing.RunCoroutine(_Direction().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHide()
        {
            Timing.KillCoroutines(_direction);
        }

        private IEnumerator<float> _Direction()
        {
            // 내 슬롯 세팅 
            resultMe.Init(Backend.UserInDate, Backend.UserNickName, 0, InGame.Plunder.Plunder.Instance.TotalDamageMe);
            
            // 상대 슬롯 세팅
            SceneData.Instance.Plunder.GetTargetUserData(out var userInDate, out var nickname, out var initGas);
            resultTargetUser.Init(userInDate, nickname, initGas, InGame.Plunder.Plunder.Instance.TotalDamageTargetUser);
            
            // 연출 전 퍼센트 가려주기 
            percentText.gameObject.SetActive(false);
            
            // 보상 인피니티 스톤 보여주기  
            infStone.InitMergedNameAndCount(InGame.Plunder.Plunder.Instance.RewardInfStoneIdx, SceneData.Instance.Plunder.SweepCount);
            infStone.Hide();
            infStone.ShowIcon();
            
            // 딜레이 
            yield return Timing.WaitForSeconds(delayDirection);
            
            // 리소스 텍스트 연출 
            var finalizedGasMe = InGame.Plunder.Plunder.Instance.FinalizedGasMe;
            resultMe.GasResourceText.StartCountAnimation(0, finalizedGasMe, durationDirection);
            
            var finalizedGasTargetUser = InGame.Plunder.Plunder.Instance.FinalizedGasTargetUser;
            resultTargetUser.GasResourceText.StartCountAnimation(initGas, finalizedGasTargetUser, durationDirection);
            
            // 리소스 텍스트 연출 기다림 
            yield return Timing.WaitForSeconds(durationDirection);
            
            // 초기 약탈 가능 가스량에서 몇 퍼센트 약탈 당한건지 보여줌 
            var percentStr = InGame.Plunder.Plunder.Instance.FinalizedPlunderedPercent.ToString("0.0");
            percentText.text = $"-{percentStr}%";
            percentText.gameObject.SetActive(true);
            
            // 보상 인피니티 스톤 이펙트 보여주기 
            infStone.ShowEffectIfCan();
        }
    }
}