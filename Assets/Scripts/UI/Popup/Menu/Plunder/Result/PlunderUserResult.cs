﻿using BackEnd;
using InGame.Data;
using TMPro;
using UnityEngine;

namespace UI.Popup.Plunder
{
    public class PlunderUserResult : MonoBehaviour
    {
        [SerializeField] private CharacterSlot characterSlot;
        [SerializeField] private ResourceText gasResourceText;
        [SerializeField] private TextMeshProUGUI totalDamageText;
        [SerializeField] private TextMeshProUGUI nicknameText;

        public ResourceText GasResourceText => gasResourceText;

        public void Init(string inDate, string nickname, double gas, double totalDamage)
        {
            var userData = UserData.Other[inDate];

            // 캐릭터 슬롯 
            {
                // 코스튬
                var head = userData.Costume.GetEquipped(UserData.CostumeType.head);
                var skin = userData.Costume.GetEquipped(UserData.CostumeType.skin);
                var body = userData.Costume.GetEquipped(UserData.CostumeType.body);

                // 바주카
                userData.Bazooka.GetState(out var bazooka, out _, out _);

                // 슬롯 세팅 
                characterSlot.WearCostume(head, skin, body, bazooka);
            }
            
            // 닉네임  
            nicknameText.text = nickname;
            
            // 가스 
            gasResourceText.Init(gas.ToUnitString());
            
            // 데미지 합계
            totalDamageText.text = totalDamage.ToUnitString();
        }
    }
}