﻿using Doozy.Engine.UI;
using InGame;
using InGame.Controller;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

namespace UI.Popup.Plunder
{
    [RequireComponent(typeof(UIPopup))]
    public class PlunderedResultPopup : MonoBehaviour
    {
        [SerializeField] private ResourceText collectedGasText;
        [SerializeField] private ResourceText plunderedGasText;
        [SerializeField] private ResourceText resultGasText;
        [SerializeField] private TextMeshProUGUI protectionText;
        
        

        public static PlunderedResultPopup Show()
        {
            var popup = UIPopup.GetPopup("Plundered Result");
            popup.Show();

            return popup.GetComponent<PlunderedResultPopup>();
        }

        public void Init(double collectedGas, double plunderedGas, double remainingGas)
        {
            // 현재까지의 채굴량
            collectedGasText.Init(collectedGas.ToUnitString());
            
            // 약탈당한 가스 총합 (아무리 약탈당해도 내 현재 채굴량 이상은 약탈되지 않도록 표시)
            plunderedGasText.Init($"-{plunderedGas.ToUnitString()}");
            
            // 최종 가스량 
            resultGasText.Init(remainingGas.ToUnitString());
            
            // 방어도
            protectionText.text = InGameControlHub.My.PlunderController.GetProtectText(); 
        }
    }
}