﻿using Doozy.Engine.UI;
using InGame;
using InGame.Controller;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

namespace UI.Popup.Plunder
{
    [RequireComponent(typeof(UIPopup))]
    public class PlunderReadyPopup : UIPopupCommon
    {
        [SerializeField] private PlunderUserSlot userSlot;
        
        [SerializeField] private TextMeshProUGUI sweepCountText;
        [SerializeField] private TextMeshProUGUI sweepButtonLabel;

        private string _inDate;

        public static void Show(string inDate)
        {
            var popup = UIPopup.GetPopup("Plunder Ready");
            
            popup.GetComponent<PlunderReadyPopup>().Init(inDate);
            
            popup.Show();
        }
        
        private void Init(string inDate)
        {
            _inDate = inDate;
            
            // 슬롯 
            var matchedUser = InGameControlHub.My.Data.Plunder.MatchedUsers[inDate];
            userSlot.Init(matchedUser.inDate, matchedUser.nickname, matchedUser.plunderableGas);
            
            // 약탈 소탕 
            var sweepCount = InGameControlHub.My.PlunderController.MaxCountToSweep;
            sweepCountText.text = $"-{sweepCount.ToLookUpString()}";

            sweepButtonLabel.text = Localization.GetFormatText("info_381", sweepCount.ToLookUpString());
        }
        
        [PublicAPI]
        public void Sweep()
        {
            // 팝업 닫기 
            Popup.Hide();
            
            // PlunderPopup에게 처리 위임 (연출 위함)
            // 콜백함수로 넘겨 처리하려다, 넘기는 콜백 처리가 너무 많아져서, 유니티의 Find 장점을 사용 
            var plunderPopup = FindObjectOfType<PlunderPopup>();
            if (plunderPopup != null)
                plunderPopup.StartPlunder(_inDate, InGameControlHub.My.PlunderController.MaxCountToSweep);
        }
        
        [PublicAPI]
        public void Plunder()
        {
            // 팝업 닫기 
            Popup.Hide();
            
            // PlunderPopup에게 처리 위임 (연출 위함)
            // 콜백함수로 넘겨 처리하려다, 넘기는 콜백 처리가 너무 많아져서, 유니티의 Find 장점을 사용 
            var plunderPopup = FindObjectOfType<PlunderPopup>();
            if (plunderPopup != null)
                plunderPopup.StartPlunder(_inDate, 1);
        }
    }
}