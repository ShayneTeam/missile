﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using Firebase.Analytics;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Data.Scene;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using Server;
using TMPro;
using UI.Popup.Menu;
using UI.Popup.Menu.Plunder.Revenge;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace UI.Popup.Plunder
{
    [RequireComponent(typeof(UIPopup))]
    public class PlunderPopup : MonoBehaviour
    {
        /*
         * 컨텐츠
         */
        [Serializable]
        public class ContentsSchema
        {
            public GameObject root;
            
            // 플레이어
            public CharacterSlot characterSlot;
            public Image planetIcon;
            public TextMeshProUGUI stageText;
            public TextMeshProUGUI flagText;
            public List<Image> missilesImage;

            // 유저
            public List<PlunderUserSlot> usersSlot;

            // 리매칭 비용
            public TextMeshProUGUI rematchingCostText;
            
            // 복수 노트 알림 
            public GameObject revengeNotice;
        }
        [SerializeField] private ContentsSchema contents;
        
        /*
         * 연출
         */
        [Serializable]
        public class DirectingSchema
        {
            public UIView view;
            
            // 플레이어
            public CharacterSlot characterSlot;
            public Image planetIcon;
            public TextMeshProUGUI stageText;
            
            // 타겟
            public PlunderUserSlot targetUserSlot;
        }
        [SerializeField] private DirectingSchema directing;
        
        private UIPopup _popup;
        private Vector3 _myCharacterOriginPos;
        private CoroutineHandle _init;
        private CoroutineHandle _rematching;

        private void Awake()
        {
            _popup = GetComponent<UIPopup>();
            _myCharacterOriginPos = directing.characterSlot.transform.position;
        }

        [PublicAPI]
        public void OnShow()
        {
            // BGM
            SoundController.Instance.PlayBGM("bgm_plunder");
            
            // 갱신
            _init = Timing.RunCoroutine(_Init().CancelWith(gameObject));

            WebLog.Instance.ThirdPartyLog("plunder_show");
        }

        [PublicAPI]
        public void OnHide()
        {
            // BGM (씬 진입하지 않았을 때만 롤백)
            if (!PlunderControl.IsEntered())
                SoundController.Instance.RollbackBGM();
            
            // 배경 리셋 
            InGameMenu.SwitchBg(null, Color.black);

            // 코루틴 날림
            Timing.KillCoroutines(_init);
            Timing.KillCoroutines(_rematching);
            
            WebLog.Instance.ThirdPartyLog("plunder_hide");
        }

        [PublicAPI]
        public void Rematching()
        {
            // 로딩 팝업으로 일차적인 버튼 터치를 막지만, 혹시나 또 들어울 수 있으니 조건문 넣음
            if (!_rematching.IsRunning)
                _rematching = Timing.RunCoroutine(_Rematching().CancelWith(gameObject));    
        }

        [PublicAPI]
        public void OpenRevenge()
        {
            RevengePopup.Show();
        }

        private IEnumerator<float> _Init()
        {
            // 컨텐츠 켬
            contents.root.SetActive(true);
            directing.view.Hide(true);
            
            // 게임 도중에 약탈이 해금되거나, 기타 이유로 약탈 매칭이 안돼있는 경우, 여기서 해줌 
            LoadingIndicator.Show();
            yield return Timing.WaitUntilDone(InGameControlHub.My.PlunderController.MatchIfHasNotMatched());
            LoadingIndicator.Hide();

            // 배경
            InGameMenu.SwitchBg(AtlasController.GetSprite("BG_plunder"), Color.white);

            /*
             * 플레이어 세팅 
             */
            // 캐릭터, 스테이지, 행성
            InitMySlot(contents.characterSlot, contents.planetIcon, contents.stageText);

            // 미사일 
            var allEquippedMissileIdxes = UserData.My.Missiles.GetAllIdxesEquipped();
            for (var i = 0; i < contents.missilesImage.Count; i++)
            {
                if (i >= allEquippedMissileIdxes.Count()) 
                    break;
                
                var imagePath = DataboxController.GetDataString(Table.Missile, Sheet.missile, allEquippedMissileIdxes.ElementAt(i), Column.resource_id);
                contents.missilesImage[i].sprite = AtlasController.GetSprite(imagePath);
            }

            // 깃발 현황 
            var ownedCount = UserData.My.Items[RewardType.REWARD_PLUNDER_FLAG];
            var maxCount = int.Parse(DataboxController.GetConstraintsData(Constraints.PLUNDER_DAILY_TICKET_COUNT));
            contents.flagText.text = Localization.GetFormatText("info_134", ownedCount.ToLookUpString(), maxCount.ToLookUpString());

            /*
             * 매칭 유저 세팅 
             */
            var matchedUsers = UserData.My.Plunder.MatchedUsers.ToList();
            for (var i = 0; i < contents.usersSlot.Count; i++)
            {
                if (i >= matchedUsers.Count)
                    break;

                var matchedUser = matchedUsers[i].Value;
                contents.usersSlot[i].Init(matchedUser.inDate, matchedUser.nickname, matchedUser.plunderableGas);
            }

            // 리매칭 비용 
            contents.rematchingCostText.text = PlunderControl.GetRematchingCost().ToLookUpString();
            contents.rematchingCostText.color = InGameControlHub.My.PlunderController.HasRematchingCost() ? Color.yellow :  Color.red;
            
            // 복수 가능 알림 
            var canAvenge = CanAvenge();
            contents.revengeNotice.SetActive(canAvenge);
        }

        private IEnumerator<float> _Rematching()
        {
            // 리매칭 비용 부족 시, 메시지
            if (!InGameControlHub.My.PlunderController.HasRematchingCost())
            {
                MessagePopup.Show("info_237");
                yield break;
            }
            
            // 깃발 부족 시, 메시지 
            if (!InGameControlHub.My.PlunderController.HasFlag())
            {
                MessagePopup.Show("info_177");
                yield break;
            }
            
            // 재매칭 
            LoadingIndicator.Show();
            
            yield return Timing.WaitUntilDone(InGameControlHub.My.PlunderController._Rematching());
            
            WebLog.Instance.ThirdPartyLog("plunder_rematching");

            // 갱신
            yield return Timing.WaitUntilDone(_Init());
            
            LoadingIndicator.Hide();
        }

        public static bool CanAvenge()
        {
            var hasRevengeTicket = InGameControlHub.My.PlunderController.HasRevengeTicket();
            var hasLogToAvenge = ServerPlunder.Instance.HasLogToAvenge();
            return hasRevengeTicket && hasLogToAvenge;
        }

        private static void InitMySlot(CharacterSlot characterSlot, Image planetIcon, TextMeshProUGUI stageText)
        {
            // 캐릭터
            characterSlot.WearMyCostume();

            // 스테이지
            var bestStage = UserData.My.Stage.bestStage;
            stageText.text = Localization.GetFormatText("info_173", bestStage.ToLookUpString());

            // 행성 
            StageControl.GetPlanetIdxByStage(bestStage, out var planetIdx);
            var planetIconPath = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.PlanetIcon, "planet_01");
            planetIcon.sprite = AtlasController.GetSprite(planetIconPath);
        }

        public void StartPlunder(string targetInDate, int sweepCount)
        {
            // 공통 처리
            DirectionCommonStart();
            
            // 타겟 유저 세팅 
            var info = UserData.My.Plunder.MatchedUsers[targetInDate];
            directing.targetUserSlot.Init(info.inDate, info.nickname, info.plunderableGas);
            
            // 씬에서 세팅할 수 있도록 타겟유저정보 세팅 
            SceneData.Instance.Plunder.SetTargetUserDataForPlunder(info.inDate, info.nickname, info.plunderableGas, sweepCount);
            
            // 연출 시작 
            Timing.RunCoroutine(_Direction().CancelWith(gameObject));
        }

        public void StartRevenge(string userInDate, string nickname, double gas, string logInDate)
        {
            // 공통 처리
            DirectionCommonStart();
            
            // 타겟 유저 세팅 
            directing.targetUserSlot.Init(userInDate, nickname, gas);
            
            // 씬에서 세팅할 수 있도록 타겟유저정보 세팅 
            SceneData.Instance.Plunder.SetTargetUserDataForRevenge(userInDate, nickname, gas, logInDate);
            
            // 연출 시작 
            Timing.RunCoroutine(_Direction().CancelWith(gameObject));
        }

        private void DirectionCommonStart()
        {
            // 컨텐츠 끔 
            contents.root.SetActive(false);
            
            // 내 슬롯 세팅 
            directing.characterSlot.transform.position = _myCharacterOriginPos;
            InitMySlot(directing.characterSlot, directing.planetIcon, directing.stageText);
        }

        private void BlockMonkeyTouch(bool block)
        {
            // 빽버튼 막기 
            _popup.HideOnBackButton = !block;
            
            // 다른 메뉴 트랜지션 막기  
            InGameMenu.HideBackButton = !block;
            InGameMenu.BlockTransitionMenu = block;
        }

        private IEnumerator<float> _Direction()
        {
            // 빽버튼 막아서 팝업 안 닫히게 함
            BlockMonkeyTouch(true);

            // 연츌 뷰 보여주기
            directing.view.Show();
            yield return Timing.WaitUntilDone(directing.view._WaitShow().CancelWith(gameObject));
            
            // 연츌 뷰 보여주고 나서부터 약탈씬 로딩 시작  
            PlunderControl.EnterPlunderScene();
            
            // 커튼 치기 (미리 커튼치고 들어가야 팝업 닫히면서 찰나의 틈으로 보이지 않음)
            // 커튼 치는 시간을 기다리진 않음 
            CurtainScreenEffect.Instance.Show();
            
            // 약탈씬 로딩 다 됐으면, 이동 시작 
            yield return Timing.WaitUntilDone(MovePlanetDirector._MoveCharacterSlot(directing.characterSlot, directing.targetUserSlot.transform.position).CancelWith(gameObject));
            
            // 한 프레임 기다림
            yield return Timing.WaitForOneFrame;

            // 약탈 팝업 닫기 
            _popup.Hide();

            // 약탈 팝업 꺼짐 이벤트 
            _popup.HideBehavior.OnFinished.Action += OnFinishedHide;
            void OnFinishedHide(GameObject go)
            {
                _popup.HideBehavior.OnFinished.Action -= OnFinishedHide;
                
                // 연출 공통 종료 처리 (팝업이 닫히고 나서 하는게 정석임)
                BlockMonkeyTouch(false);
            }

            // 약탈 시작 
            InGame.Plunder.Plunder.Instance.StartMode();
        }
    }
}