﻿using System;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Plunder
{
    public class PlunderUserSlot : MonoBehaviour
    {
        [SerializeField] private CharacterSlot characterSlot;
        [SerializeField] private Image planetIcon;
        [SerializeField] private TextMeshProUGUI nicknameText;
        [SerializeField] private TextMeshProUGUI stageText;
        [SerializeField] private TextMeshProUGUI gasText;

        private string _inDate;
        
        public void Init(string userInDate, string nickname, double gas)
        {
            _inDate = userInDate;
            
            var userData = UserData.Other[userInDate];
            
            // 코스튬
            var head = userData.Costume.GetEquipped(UserData.CostumeType.head);
            var skin = userData.Costume.GetEquipped(UserData.CostumeType.skin);
            var body = userData.Costume.GetEquipped(UserData.CostumeType.body);
            
            // 바주카
            userData.Bazooka.GetState(out var bazooka, out _, out _);

            // 슬롯 세팅 
            characterSlot.WearCostume(head, skin, body, bazooka);
            
            // 행성 아이콘 
            StageControl.GetPlanetIdxByStage(userData.Stage.currentStage, out var planetIdx);
            var planetIconPath = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.PlanetIcon, "planet_01");
            planetIcon.sprite = AtlasController.GetSprite(planetIconPath);
            
            // 스테이지  
            stageText.text = Localization.GetFormatText("info_173", userData.Stage.currentStage.ToLookUpString());
            
            // 닉네임 
            nicknameText.text = nickname;
            
            // 가스 
            gasText.text = ((double)gas).ToUnitString();
        }

        [PublicAPI]
        public void OnTouched()
        {
            // 깃발 부족 시, 메시지 
            if (!InGameControlHub.My.PlunderController.HasFlag())
            {
                MessagePopup.Show("info_177");
                
                return;
            }
            
            // 약탈 준비 팝업 
            PlunderReadyPopup.Show(_inDate);
        }
    }
}