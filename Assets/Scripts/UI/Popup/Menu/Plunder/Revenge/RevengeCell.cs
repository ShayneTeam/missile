﻿using BackEnd;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using Server;
using TMPro;
using UI.Popup.Plunder;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Plunder.Revenge
{
    public class RevengeCell : EnhancedScrollerCellView
    {
        [SerializeField] private CharacterSlot characterSlot;
        [SerializeField] private TextMeshProUGUI nicknameText;
        [SerializeField] private TextMeshProUGUI stageText;

        [SerializeField] private ResourceText gasResourceText;

        [SerializeField] private GameObject avengeButton;
        [SerializeField] private GameObject buttonBack;

        [SerializeField] private TextMeshProUGUI elapsedTimeText;

        
        private PlunderLogSchema _log;

        public void Init(PlunderLogSchema log)
        {
	        _log = log;
	        
	        // 갱신 
	        Refresh();
        }

        private void Refresh()
        {
	        var userData = UserData.Other[_log.AttackerInDate];

	        // 캐릭터 슬롯 
	        {
		        // 코스튬
		        var head = userData.Costume.GetEquipped(UserData.CostumeType.head);
		        var skin = userData.Costume.GetEquipped(UserData.CostumeType.skin);
		        var body = userData.Costume.GetEquipped(UserData.CostumeType.body);

		        // 바주카
		        userData.Bazooka.GetState(out var bazooka, out _, out _);

		        // 슬롯 세팅 
		        characterSlot.WearCostume(head, skin, body, bazooka);
	        }
	        
	        // 스테이지  
	        stageText.text = Localization.GetFormatText("info_173", userData.Stage.currentStage.ToLookUpString());
            
	        // 닉네임 
	        nicknameText.text = _log.AttackerNickName;
            
	        // 가스 
	        gasResourceText.Init(_log.RevengeGainGas.ToUnitString());
	        
	        // 복수 여부에 따라 버튼 숨김 처리 
	        avengeButton.SetActive(!_log.HasAvenged);
	        buttonBack.SetActive(_log.HasAvenged);
	        
	        // 흐른 시간 
	        var elapsedTime = ServerTime.Instance.NowSecUnscaled - _log.Timestamp;
	        var minutes = (int)(elapsedTime / 60);
	        var hours = (int)(minutes / 60);

	        elapsedTimeText.text = hours >= 1 ? Localization.GetFormatText("info_253", hours.ToLookUpString()) : Localization.GetFormatText("info_252", minutes.ToLookUpString());
        }

	    [PublicAPI]
	    public void Avenge()
	    {
		    // 깃발 부족 시, 메시지 
		    if (!InGameControlHub.My.PlunderController.HasRevengeTicket())
		    {
			    MessagePopup.Show("info_267");
                
			    return;
		    }
		    
		    // 팝업 닫기 
		    var revengePopup = FindObjectOfType<RevengePopup>();
		    if (revengePopup != null)
			    revengePopup.GetComponent<UIPopup>().Hide();
            
		    // PlunderPopup에게 처리 위임 (연출 위함)
		    // 콜백함수로 넘겨 처리하려다, 넘기는 콜백 처리가 너무 많아져서, 유니티의 Find 장점을 사용 
		    var plunderPopup = FindObjectOfType<PlunderPopup>();
		    if (plunderPopup != null)
			    plunderPopup.StartRevenge(_log.AttackerInDate, _log.AttackerNickName, _log.RevengeGainGas, _log.LogInDate);
	    }

	    public override void RefreshCellView()
	    {
		    Refresh();
	    }
    }
}