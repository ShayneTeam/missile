﻿using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using Server;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Plunder.Revenge
{
    [RequireComponent(typeof(UIPopup))]
    public class RevengePopup : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private TextMeshProUGUI ticketText;
        
        [Header("스크롤"), SerializeField] private EnhancedScroller scroller;
        [SerializeField] private EnhancedScrollerCellView cellPrefab;
        [SerializeField] private float cellSize;
        
        private List<PlunderLogSchema> _allLogs;
        private CoroutineHandle _requesting;


        public static RevengePopup Show()
        {
            var popup = UIPopup.GetPopup("Plunder Revenge");
            popup.Show();

            return popup.GetComponent<RevengePopup>();
        }

        [PublicAPI]
        public void Init()
        {
            // 남은 티켓 
            var ownedCount = UserData.My.Items[RewardType.REWARD_REVENGE_TICKET];
            var maxCount = int.Parse(DataboxController.GetConstraintsData(Constraints.PLUNDER_DAILY_REVENGE_COUNT));
            ticketText.text = Localization.GetFormatText("info_134", ownedCount.ToLookUpString(), maxCount.ToLookUpString());
            
            // 로그 한 번 요청 
            ServerPlunder.Instance.GetLogsOnBehind();
            
            // 모든 약탈 로그 가져옴 
            _allLogs = Server.ServerPlunder.Instance.GetAllLogs();
            
            // 로그를 남긴 공격자의 데이터 전부 요청
            _requesting = Timing.RunCoroutine(_RequestLogUsersData().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHide()
        {
            Timing.KillCoroutines(_requesting);
        }

        private IEnumerator<float> _RequestLogUsersData()
        {
            // 시간 좀 걸릴 수 있음. 허나 캐싱되서 다음부턴 상대적으로 느려지지 않음
            LoadingIndicator.Show();

            foreach (var log in _allLogs)
            {
                // 로그 하나하나씩 순차적으로 요청하고, 올 때까지 대기 함 
                // 동일 유저가 여러 개의 로그를 남길 시, 쓸데없이 같은 유저 데이터를 여러 번 요청할 수 있어서, 순차적으로 호출 함
                yield return Timing.WaitUntilDone(PlunderControl._RequestUserData(log.AttackerInDate));
            }
            
            LoadingIndicator.Hide();
            
            // 스크롤러 초기화
            scroller.Delegate = this;
            scroller.ReloadData();
        }

        public int GetNumberOfCells(EnhancedScroller _)
        {
            return _allLogs.Count;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cell = scroller.GetCellView(cellPrefab) as RevengeCell;
            if (cell == null)
                return null;

            cell.Init(_allLogs[dataIndex]);
            
            return cell;
        }
    }
}