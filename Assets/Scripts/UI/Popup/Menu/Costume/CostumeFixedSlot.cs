﻿using System;
using Databox.Dictionary;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Costume
{
    [RequireComponent(typeof(CommonScrollerOwner))]
    public class CostumeFixedSlot : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private Image head;
        [SerializeField] private Image skin;
        [SerializeField] private Image body;
        
        [SerializeField] private TextMeshProUGUI nameText;

        [SerializeField] private UserData.CostumeType type;
        
        private CommonScrollerOwner _scroll;
        private OrderedDictionary<string, float> _abilities;

        private void Awake()
        {
            _scroll = GetComponent<CommonScrollerOwner>();
        }

        public void Init()
        {
            Refresh();
        }
        
        private void Refresh()
        {
            var equippedIdx = InGameControlHub.My.CostumeController.GetEquipped(type);
            
            // 이름
            var nameStr = Localization.GetText(DataboxController.GetDataString(Table.Costume, Sheet.costume, equippedIdx, Column.name));
            if (type == UserData.CostumeType.skin)
            {
                var suffix = Localization.GetText("cos_desc_07");
                nameText.text = nameStr + suffix;
            }
            else
            {
                nameText.text = nameStr;
            }
	        
            // 아이콘 
            head.gameObject.SetActive(false);
            skin.gameObject.SetActive(false);
            body.gameObject.SetActive(false);
            switch (type)
            {
                case UserData.CostumeType.head:
                {
                    head.gameObject.SetActive(true);
                    head.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Costume, Sheet.costume, equippedIdx, Column.ui_resource));
                }
                    break;
                case UserData.CostumeType.skin:
                {
                    head.gameObject.SetActive(true);
                    skin.gameObject.SetActive(true);
                    body.gameObject.SetActive(true);
                    
                    var headEquippedIdx = InGameControlHub.My.CostumeController.GetEquipped(UserData.CostumeType.head);
                    var bodyEquippedIdx = InGameControlHub.My.CostumeController.GetEquipped(UserData.CostumeType.body);
                    
                    head.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Costume, Sheet.costume, headEquippedIdx, "resource"));
                    skin.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Costume, Sheet.costume, equippedIdx, "resource"));
                    body.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Costume, Sheet.costume, bodyEquippedIdx, "resource"));
                }
                    break;
                case UserData.CostumeType.body:
                {
                    body.gameObject.SetActive(true);
                    body.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Costume, Sheet.costume, equippedIdx, Column.ui_resource));
                }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // 어빌리티
            _abilities = InGameControlHub.My.CostumeController.GetAppliedAbilitiesMerged(type);
            _scroll.scroller.Delegate = this;
            _scroll.scroller.ReloadData();
        }

        #region Scroller

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return _abilities.Count;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return _scroll.cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            var cell = _scroll.scroller.GetCellView(_scroll.cellPrefab) as CostumeAbilityCell;
            if (cell == null) return null;

            var abType = _abilities.KeyOf(dataIndex);
            var abValue = _abilities[dataIndex];
            cell.Init(abType, abValue);
            
            return cell;
        }

        #endregion
    }
}