﻿using System;
using Global;
using InGame.Global;
using UI.Popup.Menu.Shop;
using UnityEngine;

namespace UI.Popup.Menu.Costume
{
    public class CostumeShopButton : CostumeCollectButton
    {
        private string _costumeIdx;


        public override void Init(string costumeIdx)
        {
            _costumeIdx = costumeIdx;
        }

        public override void OnClick()
        {
            // 이동할 상점 탭 
            var wantTab = (ShopTab)Enum.Parse(typeof(ShopTab),DataboxController.GetDataString(Table.Costume, Sheet.costume, _costumeIdx, Column.price_type_info), true);

            // 상점 오픈
            var menu = InGameMenu.Open("Shop Menu");
            menu.GetComponent<ShopMenuPopup>().ChangeTab(wantTab);
        }
    }
}