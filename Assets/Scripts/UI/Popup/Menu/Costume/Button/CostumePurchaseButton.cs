﻿using Global;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Costume
{
    public class CostumePurchaseButton : CostumeCollectButton
    {
        [SerializeField] private TextMeshProUGUI costText;
        
        [SerializeField] private ButtonSpriteSwapper buttonSprite;

        private string _costumeIdx;
        
        public override void Init(string costumeIdx)
        {
            _costumeIdx = costumeIdx;
            
            // 텍스트
            costText.text = CostumeControl.GetNeedCost(costumeIdx).ToLookUpString();

            // 텍스트 컬러
            var isEnoughCost = InGameControlHub.My.CostumeController.HasMoney(costumeIdx);
            costText.color =  isEnoughCost ? Color.white : Color.red;
            
            // 배경 
            buttonSprite.Init(isEnoughCost);
        }

        public override void OnClick()
        {
            // 사운드
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");
            
            // 우라늄 부족 시, 메시지 
            if (!InGameControlHub.My.CostumeController.HasMoney(_costumeIdx))
            {
                MessagePopup.Show("info_237");
                return;
            }
            
            // 구매 확인 팝업 
            var nameKey = DataboxController.GetDataString(Table.Costume, Sheet.costume, _costumeIdx, Column.name);
            YesOrNoPopup.Show(nameKey, "info_238", "info_239", "info_228",
                () =>
                {
                    InGameControlHub.My.CostumeController.Purchase(_costumeIdx);
                }, 
                () => {} 
            );
        }
    }
}
