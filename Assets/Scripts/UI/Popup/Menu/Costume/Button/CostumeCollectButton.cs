﻿using UnityEngine;

namespace UI.Popup.Menu.Costume
{
    public abstract class CostumeCollectButton : MonoBehaviour
    {
        public abstract void Init(string costumeIdx);

        public abstract void OnClick();
    }
}