﻿using InGame.Data;
using JetBrains.Annotations;
using UnityEngine;

namespace UI.Popup.Menu.Costume
{
    public class CostumeMenuPopup : MonoBehaviour
    {
        [SerializeField] private CostumeScroll head;
        [SerializeField] private CostumeScroll skin;
        [SerializeField] private CostumeScroll body;
        
        [SerializeField] private CostumeFixedSlot headFixed;
        [SerializeField] private CostumeFixedSlot skinFixed;
        [SerializeField] private CostumeFixedSlot bodyFixed;

        private bool _init;
        
        private void Start()
        {
            UserData.My.Costume.OnEarned += OnChangedData;
            UserData.My.Costume.OnEquipped += OnChangedData;
        }

        private void OnDestroy()
        {
            UserData.My.Costume.OnEarned -= OnChangedData;
            UserData.My.Costume.OnEquipped -= OnChangedData;
        }

        private void OnChangedData(string costumeIdx)
        {
            Refresh();
        }

        [PublicAPI]
        public void Init()
        {
            if (!_init)
            {
                head.Init();
                skin.Init();
                body.Init();
                
                _init = true;
            }
            else
            {
                head.Refresh();
                skin.Refresh();
                body.Refresh();
            }
            
            headFixed.Init();
            skinFixed.Init();
            bodyFixed.Init();
        }

        private void Refresh()
        {
            head.Refresh();
            skin.Refresh();
            body.Refresh();
            
            headFixed.Init();
            skinFixed.Init();
            bodyFixed.Init();
        }
    }
}