﻿using EnhancedUI.EnhancedScroller;
using InGame.Controller;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Costume
{
    public class CostumeAbilityCell : EnhancedScrollerCellView
    {
        [SerializeField] private TextMeshProUGUI abilityText;
        
        public void Init(string abType, float abValue)
        {
            var abIdx = AbilityControl.GetIdxByType(abType);
            var abNameText = AbilityControl.GetNameText(abIdx);
            var abDescText = AbilityControl.GetDescText(abIdx, abValue);
            abilityText.text = abNameText + abDescText;
        }
    }
}