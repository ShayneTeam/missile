﻿using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using UnityEngine;

namespace UI.Popup.Menu.Costume
{
    public class CostumeScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller scroller;
        [SerializeField] private float cellSize;
        
        [SerializeField] private EnhancedScrollerCellView cellGemPrefab;
        [SerializeField] private EnhancedScrollerCellView cellShopPrefab;

        [SerializeField] private UserData.CostumeType type;
        private List<string> _allIdxes;
        
        public void Init()
        {
            _allIdxes = DataboxController.GetAllIdxesByGroup(Table.Costume, Sheet.costume, "type", type.ToEnumString()).ToList();
            
            scroller.Delegate = this;
            scroller.ReloadData();
        }

        public void Refresh()
        {
            scroller.RefreshActiveCellViews();
        }
        
        public int GetNumberOfCells(EnhancedScroller _)
        {
            return _allIdxes.Count;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 구매 타입에 따라 셀 프리팹 분리
            var costumeIdx = _allIdxes[dataIndex];
            var priceType = DataboxController.GetDataString(Table.Costume, Sheet.costume, costumeIdx, Column.price_type);

            EnhancedScrollerCellView cellPrefab = null;
            switch (priceType)
            {
                case "gem":
                    cellPrefab = cellGemPrefab;
                    break;
                case "shop":
                    cellPrefab = cellShopPrefab;
                    break;
            }

            if (cellPrefab == null) return null;
            
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellPrefab) as CostumeCell;
            if (cellView == null) return null;

            cellView.Init(costumeIdx);
            
            return cellView;
        }
    }
}