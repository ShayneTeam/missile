﻿using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Costume
{
    public class CostumeCell : EnhancedScrollerCellView
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI abilityText;
        
        [SerializeField] private CostumeCollectButton collectButton;
        [SerializeField] private GameObject equipButton;
        
        
        
        private string _costumeIdx;

        public void Init(string costumeIdx)
        {
            _costumeIdx = costumeIdx;

            Refresh();
        }

        public override void RefreshCellView()
        {
            Refresh();
        }

        private void Refresh()
        {
            // 이름
            nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Costume, Sheet.costume, _costumeIdx, Column.name));
	        
            // 아이콘 
            icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Costume, Sheet.costume, _costumeIdx, Column.ui_resource));
            
            // 어빌리티 값
            var abIdx = DataboxController.GetDataString(Table.Costume, Sheet.costume, _costumeIdx, "c_aidx");
            
            AbilityControl.GetNameAndDesc(abIdx, out var abNameText, out var abDescText);
            abilityText.text = abNameText + abDescText;

            // 버튼 상태 
            {
                collectButton.gameObject.SetActive(false);
                equipButton.SetActive(false);
                
                // 구매했다면
                if (InGameControlHub.My.CostumeController.IsEarned(_costumeIdx))
                {
                    // 착용버튼
                    if (!InGameControlHub.My.CostumeController.IsEquipped(_costumeIdx))
                    {
                        equipButton.SetActive(true);
                    }
                    
                    // 착용 중
                }
                // 미구매
                else
                {
                    // 버튼 보여주기  
                    collectButton.gameObject.SetActive(true);
                    collectButton.Init(_costumeIdx);
                }
            }
        }

        public void Equip()
        {
            // 사운드
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");
            
            // 장착 
            InGameControlHub.My.CostumeController.Equip(_costumeIdx);
        }
        
    }
}