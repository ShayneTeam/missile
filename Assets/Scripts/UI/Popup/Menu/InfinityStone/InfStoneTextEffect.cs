﻿using DG.Tweening;
using Global;
using InGame;
using InGame.Global;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu
{
    public class InfStoneTextEffect : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI text;

        [SerializeField] private float fadingDuration;
        [SerializeField] private float moveYOffset;

        private Tween _textTween;
        private Tween _moveTween;
        
        public void Init(string str)
        {
            text.text = str;
            
            /*
             * 린풀 재활용으로 인한 트윈 재활용
             */
            if (_textTween == null)
            {
                _textTween = text.DOFade(0f, fadingDuration).SetAutoKill(false);
            }
            else
            {
                _textTween.Restart();
            }
            
            if (_moveTween == null)
            {
                _moveTween = transform.DOLocalMoveY(moveYOffset, fadingDuration).SetAutoKill(false);
            }
            else
            {
                _moveTween.Restart();
            }
            
            // 자동 반납 
            LeanPool.Despawn(gameObject, fadingDuration);
        }
    }
}