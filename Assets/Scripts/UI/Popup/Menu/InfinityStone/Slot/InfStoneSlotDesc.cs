﻿using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Stone
{
    public class InfStoneSlotDesc : MonoBehaviour
    {
        public virtual string GetAbility1ValueText(string stoneIdx)
        {
            var abIdx = InfStoneControl.StoneControl.GetAbility1Idx(stoneIdx);

            var abNameText = AbilityControl.GetNameText(abIdx);

            var abValue = InGameControlHub.My.InfStoneController.Stone.GetFinalizedAbility1Value(stoneIdx);
            var abDescText = AbilityControl.GetDescText(abIdx, abValue);

            return abNameText + abDescText;
        }
        
        public string GetAbility2ValueText(string stoneIdx)
        {
            var abIdx = InfStoneControl.StoneControl.GetAbility2Idx(stoneIdx);

            var abNameText = AbilityControl.GetNameText(abIdx);

            var abValue = InGameControlHub.My.InfStoneController.Stone.GetFinalizedAbility2Value(stoneIdx);
            var abDescText = AbilityControl.GetDescText(abIdx, abValue);

            return abNameText + abDescText;
        }
    }
}