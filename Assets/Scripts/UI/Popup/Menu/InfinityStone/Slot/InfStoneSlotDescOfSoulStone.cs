﻿using System;
using InGame;
using InGame.Controller;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Stone
{
    public class InfStoneSlotDescOfSoulStone : InfStoneSlotDesc
    {
        public override string GetAbility1ValueText(string stoneIdx)
        {
            var abIdx = InfStoneControl.StoneControl.GetAbility1Idx(stoneIdx);

            // 어빌리티 이름
            var abNameText = AbilityControl.GetNameText(abIdx);

            // 어빌리티 설명 
            var damageRate = InGameControlHub.My.BazookaController.GetSumDamageRate();
            var abSoulStonePer = InGameControlHub.My.InfStoneController.Stone.GetFinalizedAbility1Value(stoneIdx);
            var finalizedFactor = InGameControlHub.My.InfStoneController.Stone.SoulStoneDamageFactor;
            
            var abDescFormat = AbilityControl.GetDescFormat(abIdx);
            var abDescText = Localization.GetFormatText(abDescFormat, Math.Floor(finalizedFactor).ToString("0"), damageRate.ToString(), abSoulStonePer.ToUnitOrDecimalString());

            return abNameText + abDescText;
        }
    }
}