﻿using InGame;
using InGame.Controller;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Stone
{
    public class InfStoneSlotDescOfMindStone : InfStoneSlotDesc
    {
        public override string GetAbility1ValueText(string stoneIdx)
        {
            var abIdx = InfStoneControl.StoneControl.GetAbility1Idx(stoneIdx);

            // 어빌리티 이름
            var abNameText = AbilityControl.GetNameText(abIdx);

            // 어빌리티 설명 
            var sumArmyLevel = InGameControlHub.My.InfStoneController.Stone.GetSumArmyLevel();
            var abMindStonePer = InGameControlHub.My.InfStoneController.Stone.GetFinalizedAbility1Value(stoneIdx);
            var finalizedPer = InGameControlHub.My.InfStoneController.Stone.MindStoneDamagePer;
            
            var abDescFormat = AbilityControl.GetDescFormat(abIdx);
            var abDescText = Localization.GetFormatText(abDescFormat, sumArmyLevel.ToLookUpString(), abMindStonePer.ToUnitOrDecimalString(), finalizedPer.ToUnitOrDecimalString());

            return abNameText + abDescText;
        }
    }
}