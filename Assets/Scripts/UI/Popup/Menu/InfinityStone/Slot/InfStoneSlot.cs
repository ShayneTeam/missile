﻿using System;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Stone
{
	[RequireComponent(typeof(InfStoneSlotDesc))]
    public class InfStoneSlot : MonoBehaviour
    {
        [SerializeField] public string stoneIdx;
	    
        [SerializeField] private Image icon;
        [SerializeField] private InfStoneEffector effector;
        [SerializeField] private TextMeshProUGUI amountText;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI descText;
        [SerializeField] private TextMeshProUGUI value1Text;
        [SerializeField] private TextMeshProUGUI value2Text;


        [SerializeField] private TextMeshProUGUI chanceText;

        [SerializeField] private GameObject button;
        [SerializeField] private TextMeshProUGUI buttonText;
        
        private ButtonSpriteSwapper _buttonSpriteSwapper;
        
        // 하향식 컴포넌트 설계
        private InfStoneSlotDesc _slotDesc;


        private void Awake()
        {
	        _buttonSpriteSwapper = button.GetComponent<ButtonSpriteSwapper>();
	        _slotDesc = GetComponent<InfStoneSlotDesc>();

	        // 이펙터
	        effector.stoneIdx = stoneIdx;
        }

        private void OnEnable()
        {
	        UserData.My.InfinityStone.OnLevelUpHelmet += Refresh;
        }

        private void OnDisable()
        {
	        UserData.My.InfinityStone.OnLevelUpHelmet -= Refresh;
        }

        public void Init()
        {
	        // 아이콘 
	        icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Stone, Sheet.stone, stoneIdx, Column.icon));

	        // 이름
	        nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Stone, Sheet.stone, stoneIdx, Column.name));
	        
	        // 설명 
	        descText.text = Localization.GetText(DataboxController.GetDataString(Table.Stone, Sheet.stone, stoneIdx, Column.desc));

			// 갱신 
	        Refresh();
        }

        public void Refresh()
        {
	        UserData.My.InfinityStone.GetStoneState(stoneIdx, out var level, out var amount);

	        var hasEnoughAmountForLevelUp = InGameControlHub.My.InfStoneController.Stone.HasEnoughAmountForLevelUp(stoneIdx);
	        var maxLevel = InGameControlHub.My.InfStoneController.Stone.MaxLevel;

	        // 수량
	        amountText.text = Localization.GetFormatText("info_136", amount.ToLookUpString());
	        amountText.color = hasEnoughAmountForLevelUp ? Color.white : Color.red;
	        
	        // 레벨 
	        levelText.text = Localization.GetFormatText("stone_003", level.ToLookUpString(), maxLevel.ToLookUpString());
		    
	        // 어빌리티 
	        {
		        // 어빌리티 1
		        value1Text.text = _slotDesc.GetAbility1ValueText(stoneIdx);
		        
		        // 어빌리티 2 (있을 수도 없을 수도 있음)
		        var hasAb2 = InfStoneControl.StoneControl.HasAbility2(stoneIdx);
		        
		        // 어빌리티2 있다면, 일반 설명 끄고, 어빌리티 2 텍스트를 보여줌 
		        value2Text.gameObject.SetActive(hasAb2);
		        descText.gameObject.SetActive(!hasAb2);

		        if (hasAb2) value2Text.text = _slotDesc.GetAbility2ValueText(stoneIdx);
	        }

	        // 확률 
	        chanceText.text = Localization.GetFormatText("stone_004", InGameControlHub.My.InfStoneController.Stone.ChanceForLevelUp.ToLookUpString());
	        
	        // 버튼
	        var isMaxLevel = InGameControlHub.My.InfStoneController.Stone.IsMaxLevel(stoneIdx);

	        if (isMaxLevel)
	        {
		        button.SetActive(false);    
	        }
	        else
	        {
		        button.SetActive(true);
		        
		        // 버튼 스프라이트
                var hasEnoughAmount = InGameControlHub.My.InfStoneController.Stone.HasEnoughAmountForLevelUp(stoneIdx);
                _buttonSpriteSwapper.Init(hasEnoughAmount);
                
                // 버튼 텍스트 
                var key = hasEnoughAmount ? "stone_005" : "stone_010";
                buttonText.text = Localization.GetText(key);
	        }
        }

        

        [PublicAPI]
	    public void LevelUp()
	    {
		    // 스톤 갯수 부족 시
		    if (!InGameControlHub.My.InfStoneController.Stone.HasEnoughAmountForLevelUp(stoneIdx))
		    {
			    MessagePopup.Show("stone_007");
			    return;
		    }
		    
		    // 그외에 이유로 불가능 시, 그냥 종료
		    if (!InGameControlHub.My.InfStoneController.Stone.CanLevelUp(stoneIdx))
			    return;
		    
		    // 주사위 돌리기 
		    if (!InGameControlHub.My.InfStoneController.Stone.RollForLevelUp(stoneIdx))
		    {
			    // 실패 시, 이펙트 출력
			    var effect = LeanPool.Spawn(InGamePrefabs.Instance.infStoneTextEffectPrefab, icon.transform);
			    effect.Init(Localization.GetText("stone_009"));
			    
			    // 갱신 
			    Refresh();

			    // 사운드 
			    SoundController.Instance.PlayEffect("sfx_ui_bt_close");
			    
			    // 종료
			    return;
		    }
		    
		    // 사운드 
		    SoundController.Instance.PlayEffect("sfx_ui_bt_mission");
		    
		    // 업그레이드 
		    InGameControlHub.My.InfStoneController.Stone.LevelUp(stoneIdx);
		    
		    // 갱신 
		    Refresh();
	    }
    }
}