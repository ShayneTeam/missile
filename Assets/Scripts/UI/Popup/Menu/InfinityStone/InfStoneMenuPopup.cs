﻿using System.Collections.Generic;
using Doozy.Engine.UI;
using InGame;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Stone
{
    [RequireComponent(typeof(UIPopup))]
    public class InfStoneMenuPopup : MonoBehaviour
    {
        [SerializeField] private List<InfStoneSlot> slots;
        [SerializeField] private TextMeshProUGUI helmetLevelText;
        [SerializeField] private TextMeshProUGUI allDamageText;
        [SerializeField] private TextMeshProUGUI mineralText;
        [SerializeField] private TextMeshProUGUI ampleText;
        [SerializeField] private TextMeshProUGUI stonesLevelText;

        [SerializeField] private string arrowRichText;
        
        [SerializeField] private GameObject button;
        private ButtonSpriteSwapper _buttonSpriteSwapper;
        
        
        private void Awake()
        {
            _buttonSpriteSwapper = button.GetComponent<ButtonSpriteSwapper>();
        }
        
        
        [PublicAPI]
        public void OnShow()
        {
            // 배경 바꾸기 위해 메뉴의 배경을 바꿈  
            InGameMenu.SwitchBg(AtlasController.GetSprite("BG_Void1"), Color.white);
            
            // 이벤트 
            UserData.My.InfinityStone.OnLevelUpHelmet += OnLevelUpHelmet;
            UserData.My.InfinityStone.OnLevelUpStone += OnLevelUpStone;
            
            // 슬롯 초기화 
            foreach (var slot in slots) slot.Init();
            
            // 갱신 
            Refresh();
        }

        [PublicAPI]
        public void OnHide()
        {
            // 배경 되돌림
            InGameMenu.SwitchBg(null, Color.black);
            
            // 이벤트
            UserData.My.InfinityStone.OnLevelUpHelmet -= OnLevelUpHelmet;
            UserData.My.InfinityStone.OnLevelUpStone -= OnLevelUpStone;
        }

        private void Refresh()
        {
            // 슬롯 세팅 
            foreach (var slot in slots) slot.Refresh();

            // 다음 레벨 정보 
            var isMaxLevel = InGameControlHub.My.InfStoneController.Helmet.IsMaxLevel();
            InGameControlHub.My.InfStoneController.Helmet.TryGetNextLevelInfo(out var nextDamageFactor, out var nextMineralFactor, out var nextAmplePer);
            
            // 헬멧 레벨 
            helmetLevelText.text = Localization.GetFormatText("stone_name_007", InGameControlHub.My.InfStoneController.Helmet.Level.ToLookUpString());

            // 모든 데미지 
            {
                var current = Localization.GetFormatText("stone_lv_001", InGameControlHub.My.InfStoneController.Helmet.AllDamageFactor.ToString("N0"));
                if (isMaxLevel)
                {
                    allDamageText.text = $"{current}";
                }
                else
                {
                    var next = Localization.GetFormatText("stone_lv_004", nextDamageFactor.ToString("N0"));
                    allDamageText.text = $"{current} {arrowRichText} {next}";
                }
            }

            // 미네랄 
            {
                var current = Localization.GetFormatText("stone_lv_002", InGameControlHub.My.InfStoneController.Helmet.MineralFactor.ToString("N0"));
                if (isMaxLevel)
                {
                    mineralText.text = $"{current}";
                }
                else
                {
                    var next = Localization.GetFormatText("stone_lv_004", nextMineralFactor.ToString("N0"));
                    mineralText.text = $"{current} {arrowRichText} {next}";
                }
            }

            // 증폭 데미지 
            {
                var current = Localization.GetFormatText("stone_lv_003", InGameControlHub.My.InfStoneController.Helmet.AmpleDamagePer.ToString("N0"));
                if (isMaxLevel)
                {
                    ampleText.text = $"{current}";
                }
                else
                {
                    var next = Localization.GetFormatText("stone_lv_005", nextAmplePer.ToString("N0"));
                    ampleText.text = $"{current} {arrowRichText} {next}";    
                }
            }

            // 모든 스톤 레벨 합계
            stonesLevelText.text = Localization.GetFormatText("stone_desc_007",
                InGameControlHub.My.InfStoneController.Stone.GetAllStonesLevelSum().ToLookUpString(),
                InGameControlHub.My.InfStoneController.Helmet.LevelSumRequirementForLevelUp.ToLookUpString());

            // 강화 버튼 
            if (isMaxLevel)
            {
                button.SetActive(false);    
            }
            else
            {
                button.SetActive(true);
		        
                // 버튼 스프라이트
                var hasMetLevelRequirement = InGameControlHub.My.InfStoneController.Helmet.HasMetLevelRequirement();
                _buttonSpriteSwapper.Init(hasMetLevelRequirement);
            }
        }
        
        private void OnLevelUpHelmet()
        {
            Refresh();
        }
        
        private void OnLevelUpStone(string idx)
        {
            Refresh();
        }

        [PublicAPI]
        public void LevelUp()
        {
            // 스톤 레벨 미충족 시,
            if (!InGameControlHub.My.InfStoneController.Helmet.HasMetLevelRequirement())
            {
                // 메시지 출력
                MessagePopup.Show("stone_006");
                return;
            }
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_upgrade_01");
            
            // 레벨업 
            InGameControlHub.My.InfStoneController.Helmet.LevelUp();
        }
    }
}