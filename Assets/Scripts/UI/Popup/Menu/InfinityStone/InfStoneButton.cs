﻿using System;
using Doozy.Engine.UI;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Stone
{
    [RequireComponent(typeof(UIButton))]
    [RequireComponent(typeof(NoticeIcon))]
    public class InfStoneButton : MonoBehaviour
    {
        private NoticeIcon _notice;
        private ImageMaterialSwapper _imageMaterial;

        private void Awake()
        {
            _notice = GetComponent<NoticeIcon>();
            _imageMaterial = GetComponent<UIButton>().Button.image.GetComponent<ImageMaterialSwapper>();
        }

        private void Start()
        {
            UserData.My.InfinityStone.OnChangedStoneAmount += OnChangedStoneAmount;
            UserData.My.InfinityStone.OnLevelUpHelmet += OnLevelUpHelmet;
            UserData.My.InfinityStone.OnLevelUpStone += OnLevelUpStone;
            UserData.My.Stage.OnOwnPlanet += OnOwnPlanet;
            // 로그아웃 처리용
            Stage.OnStart += Refresh;
            
            // 갱신
            Refresh();
        }

        private void OnDestroy()
        {
            UserData.My.InfinityStone.OnChangedStoneAmount -= OnChangedStoneAmount;
            UserData.My.InfinityStone.OnLevelUpHelmet -= OnLevelUpHelmet;
            UserData.My.InfinityStone.OnLevelUpStone -= OnLevelUpStone;
            UserData.My.Stage.OnOwnPlanet -= OnOwnPlanet;
            Stage.OnStart -= Refresh;
        }
        
        [PublicAPI]
        public void OpenMenu()
        {
            // 컨텐츠 미해금 시 
            if (!InGameControlHub.My.InfStoneController.IsUnlocked())
            {
                // 메시지
                MessagePopup.Show("info_367");

                return;
            }
            
            InGameMenu.Open("Stone Menu");
            
            // 눌릴 떄 마다, 혹시 모르니 갱신 처리
            Refresh();
        }

        [PublicAPI]
        public void TransitionMenu()
        {
            // 메뉴에서 버튼 눌렸을 때, 조건 선체크 하기 위한 야매 처리 함수
            
            // 컨텐츠 미해금 시
            if (!InGameControlHub.My.InfStoneController.IsUnlocked())
            {
                // 메시지
                MessagePopup.Show("info_367");

                return;
            }
            
            InGameMenu.TransitionMenuExternal("Stone Menu");
            
            // 눌릴 떄 마다, 혹시 모르니 갱신 처리
            Refresh();
        }
        
        private void OnChangedStoneAmount()
        {
            Refresh();
        }
        
        private void OnLevelUpHelmet()
        {
            Refresh();
        }
        
        private void OnLevelUpStone(string idx)
        {
            Refresh();
        }
        
        private void OnOwnPlanet()
        {
            Refresh();
        }

        private void Refresh()
        {
            // 행성조건 달성 여부에 따라 회색 메테리얼 적용
            var isUnlocked = InGameControlHub.My.InfStoneController.IsUnlocked();
            _imageMaterial.Init(isUnlocked);
            
            // 레드닷
            var canLevelUpHelmet = InGameControlHub.My.InfStoneController.Helmet.CanLevelUp();
            var canLevelUpStone = InGameControlHub.My.InfStoneController.Stone.HasAnyStoneForLevelUp();
            _notice.notice.SetActive(isUnlocked && (canLevelUpHelmet || canLevelUpStone));
        }
    }
}