﻿using System.Collections.Generic;
using Lean.Pool;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Stone
{
    public class InfStoneEffect : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private float intervalSec = 0.2f;
        [SerializeField] private int spriteCount;
        [SerializeField] private string format;
        
        private CoroutineHandle _anim;


        public void StartEffect(int stoneIndex)
        {
            Timing.KillCoroutines(_anim);
            _anim = Timing.RunCoroutine(_Animation(stoneIndex).CancelWith(gameObject));
        }
        
        private IEnumerator<float> _Animation(int stoneIndex)
        {
            for (var i = 1; i <= spriteCount; i++)
            {
                var stoneStr = stoneIndex.ToLookUpFormatString("00");
                var indexStr = i.ToLookUpFormatString("00");
                image.sprite = AtlasController.GetSprite(string.Format(format, $"{stoneStr}", $"{indexStr}"));

                yield return Timing.WaitForSeconds(intervalSec);
            }
	    
            // 애니 다 끝나고, 풀링 회수 
            LeanPool.Despawn(this);
        }
    }
}