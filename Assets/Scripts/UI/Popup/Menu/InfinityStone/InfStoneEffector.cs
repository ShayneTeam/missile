﻿using Doozy.Engine.UI;
using InGame;
using InGame.Controller;
using InGame.Data;
using Lean.Pool;
using UnityEngine;

namespace UI.Popup.Menu.Stone
{
    public class InfStoneEffector : MonoBehaviour
    {
        [SerializeField] public string stoneIdx;
        
        private void OnEnable()
        {
            UserData.My.InfinityStone.OnLevelUpHelmet += OnLevelUpHelmet;
            UserData.My.InfinityStone.OnLevelUpStone += OnLevelUpStone;
        }

        private void OnDisable()
        {
            UserData.My.InfinityStone.OnLevelUpHelmet -= OnLevelUpHelmet;
            UserData.My.InfinityStone.OnLevelUpStone -= OnLevelUpStone;
        }
        
        private void OnLevelUpHelmet()
        {
            InternalShowEffect();
        }
        
        private void OnLevelUpStone(string idx)
        {
            if (idx != stoneIdx) return;
            
            InternalShowEffect();
        }

        private void InternalShowEffect()
        {
            // 이 이펙트는 다른 이펙트와 다르게 스폰시킨 후, 따로 조작이 들어가야 하므로 어드레서블 사용하지 않음 
            ShowEffect(stoneIdx, transform.position);
        }

        public static void ShowEffect(string stoneIdx, Vector3 position)
        {
            var canvas = UICanvas.GetUICanvas("UI Effect");
            var newEffect = LeanPool.Spawn(InGamePrefabs.Instance.infStoneEffectPrefab, position, Quaternion.identity, canvas.transform);

            var effectIndex = InfStoneControl.StoneControl.GetIndex(stoneIdx);
            newEffect.StartEffect(effectIndex);
        }
    }
}