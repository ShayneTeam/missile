﻿using System;
using Doozy.Engine.UI;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using UnityEngine;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaMenuPopup : MonoBehaviour
    {
        [SerializeField] private BazookaInfo info;
        [SerializeField] private BazookaPartsScroller upgrades;

        [SerializeField] private BazookaEnhanceButton enhance;

        private bool _isInit;
        
        [PublicAPI]
        public void OnShow()
        {
            if (!_isInit)
            {
                info.Init();
                upgrades.Init();

                enhance.gameObject.SetActive(!InGameControlHub.My.BazookaController.IsMaxGrade());
                enhance.Init();

                _isInit = true;
            }
            else
            {
                Refresh();
            }
            
            // 바주카 데이타 변경 이벤트 등록 
            UserData.My.Bazooka.OnLevelUp += OnLevelUp;
            UserData.My.Bazooka.OnLevelUpParts += OnLevelUpParts;
            UserData.My.Bazooka.OnEarned += Refresh;
            UserData.My.Bazooka.OnEnhance += Refresh;
            UserData.My.Bazooka.OnEquipped += Refresh;
        }

        [PublicAPI]
        public void OnHide()
        {
            UserData.My.Bazooka.OnLevelUp -= OnLevelUp;
            UserData.My.Bazooka.OnLevelUpParts -= OnLevelUpParts;
            UserData.My.Bazooka.OnEarned -= Refresh;
            UserData.My.Bazooka.OnEnhance -= Refresh;
            UserData.My.Bazooka.OnEquipped -= Refresh;
        }

        private void OnLevelUp(int count)
        {
            Refresh();
        }
        
        private void OnLevelUpParts(string idx, int count)
        {
            Refresh();
        }

        private void Refresh(string _)
        {
            Refresh();
        }

        private void Refresh()
        {
            info.Init();
            upgrades.Refresh();
            
            enhance.gameObject.SetActive(!InGameControlHub.My.BazookaController.IsMaxGrade());
            enhance.Init();
        }

        public void OpenBazookaChangePopup()
        {
            var popup = UIPopup.GetPopup("Change Bazooka");
            popup.Show();
        }
    }
}