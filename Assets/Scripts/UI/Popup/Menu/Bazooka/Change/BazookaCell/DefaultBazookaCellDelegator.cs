﻿using InGame.Data;
using UnityEngine;

namespace UI.Popup.Menu.Bazooka.Change.BazookaCell
{
    public class DefaultBazookaCellDelegator : BazookaCellDelegator
    {
        public override bool IsEarned(string idx)
        {
            return UserData.My.Bazooka.IsEarned(idx);
        }

        public override bool IsEquipped(string idx)
        {
            UserData.My.Bazooka.GetState(out var equippedIdx, out _, out _);

            return equippedIdx == idx;
        }

        public override void Equip(string idx)
        {
            UserData.My.Bazooka.Equip(idx);
        }
    }
}