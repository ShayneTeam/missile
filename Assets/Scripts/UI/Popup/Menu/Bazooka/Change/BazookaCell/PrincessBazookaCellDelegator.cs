﻿using InGame.Data;
using UnityEngine;

namespace UI.Popup.Menu.Bazooka.Change.BazookaCell
{
    public class PrincessBazookaCellDelegator : BazookaCellDelegator
    {
        public override bool IsEarned(string idx)
        {
            return UserData.My.Bazooka.IsEarned(idx);
        }

        public override bool IsEquipped(string idx)
        {
            return UserData.My.Princess.EquippedBazooka == idx;
        }

        public override void Equip(string idx)
        {
            UserData.My.Princess.EquipBazooka(idx);
        }
    }
}