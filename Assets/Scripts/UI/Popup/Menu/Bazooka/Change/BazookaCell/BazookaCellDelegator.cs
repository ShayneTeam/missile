﻿using UnityEngine;

namespace UI.Popup.Menu.Bazooka.Change.BazookaCell
{
    public abstract class BazookaCellDelegator : MonoBehaviour
    {
        public abstract bool IsEarned(string idx);
        public abstract bool IsEquipped(string idx);
        public abstract void Equip(string idx);
    }
}