using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Global;
using UI.Popup.Menu.Bazooka.Change.BazookaCell;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Bazooka
{
    [RequireComponent(typeof(CommonScroller))]
    [RequireComponent(typeof(BazookaCellDelegator))]
    public class BazookaScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        private CommonScroller _common;
        private ICollection<string> _allIdxes;
        private BazookaCellDelegator _delegator;

        public void Init()
        {
            // 데이타
            _allIdxes = DataboxController.GetAllIdxes(Table.Bazooka, Sheet.bazooka);
            
            // 델리게이터 (공주와 병행 처리하기 위함)
            _delegator = GetComponent<BazookaCellDelegator>();

            // 스크롤러
            _common = GetComponent<CommonScroller>();

            _common.Scroller.Delegate = this;
            _common.Scroller.ReloadData();
        }

        public void Refresh()
        {
            _common.Scroller.RefreshActiveCellViews();
        }
        
        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return _allIdxes.Count;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return _common.cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            var cellView = _common.Scroller.GetCellView(_common.cellPrefab) as BazookaSlotCell;
            if (cellView == null) return null;

            cellView.Init(_allIdxes.ElementAt(dataIndex), _delegator);
            
            return cellView;
        }
    }
}