using System;
using InGame.Data;
using JetBrains.Annotations;
using UnityEngine;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaChangePopup : MonoBehaviour
    {
        [SerializeField] private BazookaScroll scroll;

        private bool _isInit;
        
        [PublicAPI]
        public void OnShow()
        {
            if (!_isInit)
            {
                scroll.Init();
                
                _isInit = true;
            }
            else
            {
                Refresh();
            }
            
            // 이벤트 등록
            UserData.My.Bazooka.OnEquipped += OnEquippedBazooka;
            UserData.My.Princess.OnEquipped += OnEquippedBazooka;
        }

        [PublicAPI]
        public void OnHide()
        {
            // 이벤트 해지 
            UserData.My.Bazooka.OnEquipped -= OnEquippedBazooka;
            UserData.My.Princess.OnEquipped -= OnEquippedBazooka;
        }

        private void Refresh()
        {
            scroll.Refresh();
        }

        private void OnEquippedBazooka(string idx)
        {
            Refresh();
        }
    }
}