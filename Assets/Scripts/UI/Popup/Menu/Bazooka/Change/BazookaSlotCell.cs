﻿using System;
using Bolt;
using DG.Tweening;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UI.Popup.Menu.Bazooka.Change.BazookaCell;
using UI.Popup.Menu.Shop;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Bazooka
{
    public enum ButtonBg
    {
        Gray,
        Yellow,
        Blue
    }

    public class BazookaSlotCell : EnhancedScrollerCellView
    {
        private string _bazookaIdx;

        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private Image icon;

        [SerializeField] private TextMeshProUGUI descText;
        [SerializeField] private GameObject effect;

        [SerializeField] private TextMeshProUGUI label;
        [SerializeField] private SpriteChanger buttonBg;

        [SerializeField] private TextMeshProUGUI damageText;
        [SerializeField] private TextMeshProUGUI speedText;

        [SerializeField] private TextMeshProUGUI abilityHeading;
        [SerializeField] private TextMeshProUGUI abilityNameText;
        [SerializeField] private TextMeshProUGUI abilityValueText;


        private BazookaCellDelegator _delegator;

        public void Init(string bazookaIdx, BazookaCellDelegator delegator)
        {
            _bazookaIdx = bazookaIdx;
            _delegator = delegator;

            // 이름 
            var nameKey = DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, bazookaIdx, Column.name);
            nameText.text = Localization.GetText(nameKey);

            // 이미지
            var iconPath = DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, bazookaIdx, Column.resource_id);
            icon.SetSpriteWithOriginSIze(AtlasController.GetSprite(iconPath));

            // 설명
            var descKey = DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, bazookaIdx, Column.desc);
            descText.text = Localization.GetText(descKey);

            // 데미지
            var damageRate = DataboxController.GetDataInt(Table.Bazooka, Sheet.bazooka, bazookaIdx, Column.damage_rate);
            damageText.text = Localization.GetFormatText("info_246", damageRate.ToLookUpString());

            // 속도 
            var speedRate = DataboxController.GetDataFloat(Table.Bazooka, Sheet.bazooka, bazookaIdx, "speed_rate", 1);
            speedText.text = Localization.GetFormatText("info_246", speedRate.ToString());

            // 어빌리티 
            var hasAbility = BazookaControl.HasAbility(bazookaIdx);
            abilityHeading.gameObject.SetActive(hasAbility);
            if (hasAbility)
            {
                var abilityIdx = BazookaControl.GetAbilityIdx(bazookaIdx);

                // 이름
                var abilityNameKey = DataboxController.GetDataString(Table.Ability, Sheet.Ability, abilityIdx, Column.name);
                abilityNameText.text = Localization.GetText(abilityNameKey);

                // 설명 
                var abilityValue = AbilityControl.GetValue(abilityIdx);
                abilityValueText.text = AbilityControl.GetDescText(abilityIdx, abilityValue);
            }
            
            // 
            RefreshCellView();
        }

        public override void RefreshCellView()
        {
            var isEquipped = _delegator.IsEquipped(_bazookaIdx);
            var isEarned = _delegator.IsEarned(_bazookaIdx);

            var text = "";
            if (isEquipped)
            {
                buttonBg.Init((int)ButtonBg.Gray);
                text = Localization.GetText("info_122");
            }
            else
            {
                if (isEarned)
                {
                    buttonBg.Init((int)ButtonBg.Yellow);
                    text = Localization.GetText("info_123");
                }
                else
                {
                    buttonBg.Init((int)ButtonBg.Blue);
                    text = Localization.GetText("info_124");
                }
            }

            // 이펙트 
            effect.SetActive(isEquipped);

            // 버튼 텍스트 
            label.text = text;
        }

        [PublicAPI]
        public void OnClicked()
        {
            // 장착 중이거나, 미획득 바주카는 장착처리 못함 
            var isEquipped = _delegator.IsEquipped(_bazookaIdx);
            var isEarned = _delegator.IsEarned(_bazookaIdx);
            
            if (!isEquipped && isEarned)
            {
                _delegator.Equip(_bazookaIdx);
                return;
            }

            // 미획득 바주카는 바주카 상점 탭으로 이동
            if (!isEarned)
            {
                // 이 팝업 닫아줘야 함 (가장 이상적인 건 delegate 받아서 호출하는거지만, 매우 귀찮으므로 일단 이렇게 함)
                var changePopup = transform.GetComponentInParent<BazookaChangePopup>();
                if (changePopup != null)
                    changePopup.GetComponent<UIPopup>().Hide();

                // 상점 메뉴 열기 
                var menu = InGameMenu.Open("Shop Menu");
                menu.GetComponent<ShopMenuPopup>().ChangeTab(ShopTab.Bazooka);
            }
        }
    }
}