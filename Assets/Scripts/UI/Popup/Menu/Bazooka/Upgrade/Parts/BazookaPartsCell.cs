﻿using System.Collections.Generic;
using Bolt;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using UnityEngine;

namespace UI.Popup.Menu.Bazooka
{
    [RequireComponent(typeof(BazookaUpgradeSlotCommon))]
    public class BazookaPartsCell : EnhancedScrollerCellView
    {
        [SerializeField] private BazookaUpgradeSlotCommon slot;
        [SerializeField] private List<BazookaPartsButton> buttons;

        private string _partsIdx;
        
        
        private void Start()
        {
	        UserData.My.Bazooka.OnLevelUpParts += OnLevelUpParts;
        }

        private void OnDestroy()
        {
	        UserData.My.Bazooka.OnLevelUpParts -= OnLevelUpParts;
        }

        private void OnLevelUpParts(string partsIdx, int count)
        {
	        if (_partsIdx != partsIdx)
		        return;

	        ShowEffect();
        }

        private void ShowEffect()
        {
	        var canvas = UICanvas.GetUICanvas("UI Effect");
	        EffectSpawner.Instance.Spawn("upgrade UI effect (small)", slot.icon.transform.position, canvas.transform);
        }

        public void Init(string partsIdx)
        {
	        _partsIdx = partsIdx;
	        
	        // 아이콘 
	        var iconPath = DataboxController.GetDataString(Table.Bazooka, Sheet.Parts, partsIdx, Column.resource_id);
	        var sprite = AtlasController.GetSprite(iconPath);
	        slot.icon.SetSpriteWithOriginSIze(sprite);

	        // 이름
	        var nameKey = DataboxController.GetDataString(Table.Bazooka, Sheet.Parts, partsIdx, Column.name);
	        slot.nameText.text = Localization.GetText(nameKey);

	        // 설명 헤딩 
	        var abilityIdx = DataboxController.GetDataString(Table.Bazooka, Sheet.Parts, partsIdx, "pt_aidx");
	        var abilityNameKey = DataboxController.GetDataString(Table.Ability, Sheet.Ability, abilityIdx, Column.name);
	        slot.descNameText.text = Localization.GetText(abilityNameKey);
	        
	        //
	        foreach (var button in buttons) button.Init(partsIdx);
	        
	        // 갱신 
	        Refresh();
        }

        public override void RefreshCellView()
        {
	        Refresh();
        }

        private void Refresh()
        {
	        // 레벨 
	        var level = UserData.My.Bazooka.GetPartsLevel(_partsIdx);
	        slot.levelText.text = Localization.GetFormatText("info_145", level.ToLookUpString());
	        
	        // 설명 값 
	        var abilityIdx = DataboxController.GetDataString(Table.Bazooka, Sheet.Parts, _partsIdx, "pt_aidx");
	        var abilityValue = DataboxController.GetDataFloat(Table.Ability, Sheet.Ability, abilityIdx, Column.value);

	        var finalizedAbilityValue = InGameControlHub.My.BazookaController.GetFinalizedAbilityValue(_partsIdx);

	        var descFormatKey = DataboxController.GetDataString(Table.Ability, Sheet.Ability, abilityIdx, Column.desc);

	        slot.descValueText.text = Localization.GetFormatText(descFormatKey, finalizedAbilityValue.ToUnitOrDecimalString());

	        // 최대 레벨 체크 
	        var isMaxLevel = InGameControlHub.My.BazookaController.Parts.IsMaxLevel(_partsIdx);
	        var isUnlock = InGameControlHub.My.BazookaController.Parts.IsUnlock(_partsIdx);
	        
	        // 팁 텍스트 (최대 레벨 달성할 때만 안 보여줌)
	        slot.tipText.gameObject.SetActive(!isMaxLevel);
	        if (!isMaxLevel)
	        {
		        var nextAbilityWeight = BazookaControl.PartsControl.GetLevelWeight(_partsIdx, level + 1);
		        var nextFinalizedAbilityValue = abilityValue * nextAbilityWeight;

		        var diff = nextFinalizedAbilityValue - finalizedAbilityValue;

		        slot.tipText.text = Localization.GetFormatText("info_104", diff.ToUnitOrDecimalString());
	        }

	        // 최대 레벨이거나 미획득 상태일 시, 팁과 버튼 안 보여줌 
	        var visibleButton = !isMaxLevel && isUnlock;
	        
	        foreach (var button in buttons)
	        {
		        button.gameObject.SetActive(visibleButton);
		        if (visibleButton)
			        button.Refresh();
	        }

	        slot.locked.SetActive(!isUnlock);				// 잠김 아이콘 
	        slot.buttonLocked.SetActive(!visibleButton);	// 버튼 잠김 텍스트 
	        if (!visibleButton)
	        {
		        // 버튼 잠김 텍스트 
		        if (isMaxLevel)
		        {
			        slot.buttonLockText.text = Localization.GetText("info_106");
		        }
		        if (!isUnlock)
		        {
			        var needGrade = DataboxController.GetDataInt(Table.Bazooka, Sheet.Parts, _partsIdx, "get_bzk_grade", 1);
			        slot.buttonLockText.text = Localization.GetFormatText("info_108", needGrade.ToLookUpString());
		        }
	        }
        }
    }
}