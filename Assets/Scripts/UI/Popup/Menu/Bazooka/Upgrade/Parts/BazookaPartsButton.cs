using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaPartsButton : MonoBehaviour
    {
        [SerializeField] public GameObject button;
        [SerializeField] public TextMeshProUGUI buttonText;
        [SerializeField] public ButtonSpriteSwapper buttonSpriteSwapper;
        
        [SerializeField] private int upgradeCount = 1;
        
        private string _partsIdx;
        
        private void Start()
        {
	        UserData.My.Resources.OnChangedMineral += OnChangedMineral;
        }

        private void OnDestroy()
        {
	        UserData.My.Resources.OnChangedMineral -= OnChangedMineral;
        }
        
        private void OnChangedMineral(double value)
        {
	        RefreshBg();
        }

        public void Init(string partsIdx)
        {
	        _partsIdx = partsIdx;
	        
	        // 갱신
	        Refresh();
        }

        public void Refresh()
        {
	        // 최대 레벨 체크 
	        var isMaxLevel = InGameControlHub.My.BazookaController.Parts.IsMaxLevel(_partsIdx);
	        var isUnlock = InGameControlHub.My.BazookaController.Parts.IsUnlock(_partsIdx);
	        
	        // 최대 레벨이거나 미획득 상태일 시, 팁과 버튼 안 보여줌 
	        var visibleButton = !isMaxLevel && isUnlock;
	        
	        button.SetActive(visibleButton);

	        if (visibleButton)
	        {
		        // 버튼 슬롯 
		        RefreshBg();

		        // 버튼 텍스트 
		        var requiredMineral = InGameControlHub.My.BazookaController.Parts.GetRequiredMineralToUpgrade(_partsIdx, upgradeCount);
		        buttonText.text = requiredMineral.ToUnitString();
	        }
        }

        [PublicAPI] 
        public void Upgrade()
        {
	        if (!CanUpgrade())
		        return;
	        
	        // 사운드 
	        SoundController.Instance.PlayEffect("sfx_ui_bt_upgrade_01");

	        // 업그레이드
	        InGameControlHub.My.BazookaController.Parts.Upgrade(_partsIdx, upgradeCount);
        }

        private void RefreshBg()
        {
	        buttonSpriteSwapper.Init(CanUpgrade());
        }

        private bool CanUpgrade()
        {
	        return InGameControlHub.My.BazookaController.Parts.CanUpgrade(_partsIdx, upgradeCount);
        }
    }
}