﻿using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Global;
using UnityEngine;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaPartsScroller : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller scroller;
        [SerializeField] private EnhancedScrollerCellView cellPrefab;
        [SerializeField] private float cellSize;

        [SerializeField] private BazookaUpgradeSlot damageUpgradeSlot;

        private ICollection<string> _allPartsIdxes;
        
        public void Init()
        {
            //
            damageUpgradeSlot.Init();
            
            //
            _allPartsIdxes = DataboxController.GetAllIdxes(Table.Bazooka, Sheet.Parts);
            
            scroller.Delegate = this;
            scroller.ReloadData();
        }

        public void Refresh()
        {
            damageUpgradeSlot.Refresh();
            
            /*
             * ReloadData로 갱신 시, 셀의 위치가 하나씩 밀려나면서, 버튼의 오토클릭이 활성화되지 않음
             */
            scroller.RefreshActiveCellViews();
        }
        
        public int GetNumberOfCells(EnhancedScroller _)
        {
            return _allPartsIdxes.Count;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellPrefab) as BazookaPartsCell;
            if (cellView == null)
                return null;

            var partsIdx = _allPartsIdxes.ElementAt(dataIndex);

            cellView.Init(partsIdx);

            return cellView;
        }
    }
}