﻿using System.Collections;
using InGame.Controller;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaUpgradeSlotCommon : MonoBehaviour
    {
        [SerializeField] public Image icon;
        [SerializeField] public TextMeshProUGUI levelText;

        [SerializeField] public TextMeshProUGUI nameText;
        [SerializeField] public TextMeshProUGUI descNameText;
        [SerializeField] public TextMeshProUGUI descValueText;
        [SerializeField] public TextMeshProUGUI tipText;
        
        [SerializeField] public GameObject locked;
        [SerializeField] public GameObject buttonLocked;
        [SerializeField] public TextMeshProUGUI buttonLockText;
    }
}