﻿using System.Collections.Generic;
using Bolt;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using UnityEngine;

namespace UI.Popup.Menu.Bazooka
{
    [RequireComponent(typeof(BazookaUpgradeSlotCommon))]
    public class BazookaUpgradeSlot : MonoBehaviour
    {
        [SerializeField] private BazookaUpgradeSlotCommon slot;

        [SerializeField] private List<BazookaUpgradeButton> buttons;

        private void Start()
        {
	        UserData.My.Bazooka.OnLevelUp += OnLevelUp;
        }

        private void OnDestroy()
        {
	        UserData.My.Bazooka.OnLevelUp -= OnLevelUp;
        }

        private void OnLevelUp(int count)
        {
	        ShowEffect();
        }

        private void ShowEffect()
        {
	        var canvas = UICanvas.GetUICanvas("UI Effect");
	        EffectSpawner.Instance.Spawn("upgrade UI effect (small)", slot.icon.transform.position, canvas.transform);
        }

        public void Init()
        {
	        // 이름
	        slot.nameText.text = Localization.GetText("info_102");

	        // 설명 헤딩 
	        slot.descNameText.text = Localization.GetText("info_103");
	        
	        //
	        foreach (var button in buttons) button.Init();

	        // 갱신 
	        Refresh();
        }

        public void Refresh()
        {
	        UserData.My.Bazooka.GetState(out _, out _, out var level);
	        
	        // 레벨 
	        slot.levelText.text = Localization.GetFormatText("info_145", level.ToLookUpString());

	        // 설명 값 
	        var currentBaseDamage = InGameControlHub.My.BazookaController.GetBaseDamage();
	        slot.descValueText.text = currentBaseDamage.ToUnitString();

	        // 최대 레벨 체크 
	        var isMaxLevel = InGameControlHub.My.BazookaController.IsMaxLevel();

	        // 최대 레벨일 시, 팁과 버튼 안 보여줌 
	        slot.tipText.gameObject.SetActive(!isMaxLevel);
	        foreach (var button in buttons) button.gameObject.SetActive(!isMaxLevel);
	        slot.buttonLocked.SetActive(isMaxLevel);
	        
	        if (isMaxLevel)
	        {
		        // 버튼 잠김 텍스트 
		        slot.buttonLockText.text = Localization.GetText("info_106");
		        
		        return;
	        }
	        
	        // 최대 레벨 아닐 때만 아래 처리 진행 
	        var tipFormat = Localization.GetText("info_104");
	        
	        var nextBaseDamage = InGameControlHub.My.BazookaController.GetBaseDamageForNextLevel();
	        var diffDamage = nextBaseDamage - currentBaseDamage;
	        slot.tipText.text = string.Format(tipFormat, diffDamage.ToUnitString());
	        
	        // 버튼 갱신 
	        foreach (var button in buttons) button.Refresh();
        }
    }
}