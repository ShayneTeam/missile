using Doozy.Engine.UI;
using InGame;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaUpgradeButton : MonoBehaviour
    {
        [SerializeField] public GameObject button;
        [SerializeField] public TextMeshProUGUI buttonText;
        [SerializeField] public ButtonSpriteSwapper buttonSpriteSwapper;
        
        [SerializeField] private int upgradeCount = 1;

        private void Start()
        {
	        UserData.My.Resources.OnChangedMineral += OnChangedMineral;
        }

        private void OnDestroy()
        {
	        UserData.My.Resources.OnChangedMineral -= OnChangedMineral;
        }

        private void OnChangedMineral(double value)
        {
	        RefreshBg();
        }

        public void Init()
        {
	        // 최대 레벨 체크 
	        var isMaxLevel = InGameControlHub.My.BazookaController.IsMaxLevel();

	        // 최대 레벨일 시, 버튼 안 보여줌 
	        button.SetActive(!isMaxLevel);

	        // 최대 레벨 아닐 때만 아래 처리 진행
	        if (!isMaxLevel)
	        {
		        // 버튼 슬롯 
		        Refresh();
	        }
        }

        [PublicAPI]
        public void Upgrade()
        {
	        if (!CanUpgrade())
		        return;
		        
	        // 사운드 
	        SoundController.Instance.PlayEffect("sfx_ui_bt_upgrade_01");
	        
	        // 업그레이드 
	        InGameControlHub.My.BazookaController.Upgrade(upgradeCount);
        }

        public void Refresh()
        {
	        // BG
	        RefreshBg();
		        
	        // 버튼 텍스트 
	        var requiredMineral = InGameControlHub.My.BazookaController.GetRequiredMineralToUpgrade(upgradeCount);
	        buttonText.text = requiredMineral.ToUnitString();
        }

        private void RefreshBg()
        {
	        buttonSpriteSwapper.Init(CanUpgrade());
        }

        private bool CanUpgrade()
        {
	        return InGameControlHub.My.BazookaController.CanUpgrade(upgradeCount);
        }
    }
}