﻿using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaEnhanceButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI chipsetText;
        
        
        public void Init()
        {
            // 칩셋 텍스트 
            var ownedChipsetCount = UserData.My.Items[RewardType.REWARD_CHIPSET];
            var needChipsetCount = InGameControlHub.My.BazookaController.GetNeedChipsetCountForEnhance();

            var canUpgrade = ownedChipsetCount >= needChipsetCount;

            var formatKey = canUpgrade ? "info_241" : "info_240";
            chipsetText.text = Localization.GetFormatText(formatKey, ownedChipsetCount.ToLookUpString(), needChipsetCount.ToLookUpString());
        }

        public void OnClick()
        {
            if (!InGameControlHub.My.BazookaController.IsEnoughChipsetForEnhance())
            {
                MessagePopup.Show("info_116");
                return;
            }
            
            // 강화칩 사용 확인 팝업              
            YesOrNoPopup.NoticeChipsetUse(() =>
            {
                // 사운드 
                SoundController.Instance.PlayEffect("sfx_ui_bt_enchant_01");
            
                // 강화
                InGameControlHub.My.BazookaController.Enhance();
            });
        }
    }
}
