﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bolt;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaInfo : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private Image image;
        [SerializeField] private TextMeshProUGUI gradeText;
        
        [SerializeField] private List<BazookaPartsSlot> parts;
        
        [SerializeField] private List<BazookaGradeSlot> grades;
        
        [SerializeField] private List<TextMeshProUGUI> abilities;

        private void Start()
        {
            UserData.My.Bazooka.OnEnhance += ShowEffect;
        }

        private void OnDestroy()
        {
            UserData.My.Bazooka.OnEnhance -= ShowEffect;
        }

        private void ShowEffect()
        {
            var canvas = UICanvas.GetUICanvas("UI Effect");
            EffectSpawner.Instance.Spawn("enhance UI effect", image.transform.position, canvas.transform);
        }

        public void Init()
        {
            UserData.My.Bazooka.GetState(out var equippedBazookaIdx, out var grade, out var level);
            
            // 이름 
            var nameKey = DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, equippedBazookaIdx, Column.name);
            nameText.text = Localization.GetText(nameKey);
            
            // 이미지
            var iconPath = DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, equippedBazookaIdx, Column.resource_id);
            image.SetSpriteWithOriginSIze(AtlasController.GetSprite(iconPath));
            
            // 파츠  
            var allPartsIdxes = DataboxController.GetAllIdxes(Table.Bazooka, Sheet.Parts);
            for (var i = 0; i < allPartsIdxes.Count; i++)
            {
                parts[i].Init(allPartsIdxes.ElementAt(i));
            }
            
            // 현재 등급 텍스트 
            gradeText.text = Localization.GetFormatText("info_110", grade.ToLookUpString());

            // 현재 등급 별 아이콘 (이건 볼트로 세팅)
            if (grade > grades.Count) // 10보다 크면 
            {
                for (var i = 0; i < grades.Count; i++)
                {
                    if (i < grade - grades.Count)  
                        grades[i].Init("blue");
                    else
                        grades[i].Init("yellow");
                }
            }
            else // 10보다 작으면
            {
                for (var i = 0; i < grades.Count; i++)
                {
                    if (i < grade)
                        grades[i].Init("yellow");
                    else
                        grades[i].Init("empty");
                }
            }

            /*
             * 어빌리티
             */
            // 공격력
            {
                var finalizedDamage = InGameControlHub.My.BazookaController.GetFinalizedDamage();
                
                abilities[0].text = Localization.GetFormatText("info_242", finalizedDamage.ToUnitString());
            }
            
            // 발사속도
            {
                var shootIntervalSec = InGameControlHub.My.BazookaController.DefaultShootIntervalSec();

                abilities[1].text = Localization.GetFormatText("info_243", shootIntervalSec.ToString("0.000"));
            }
            
            // 치명타 확률
            {
                var baseCriticalChance = InGameControlHub.My.BazookaController.GetCriticalChanceAppliedAbility();
                
                abilities[2].text = Localization.GetFormatText("info_244", baseCriticalChance.ToDecimalString());
            }
            
            // 치명타 피해량 (%)
            {
                var baseCriticalDamagePer = InGameControlHub.My.BazookaController.GetCriticalDamagePerAppliedAbility();
                
                abilities[3].text = Localization.GetFormatText("info_245", baseCriticalDamagePer.ToDecimalString());
            }
        }
    }
}