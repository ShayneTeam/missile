﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaGradeSlot : MonoBehaviour
    {
        [SerializeField] private Image icon;

        [SerializeField] private Sprite empty;
        [SerializeField] private Sprite yellow;
        [SerializeField] private Sprite blue;

        public void Init(string iconType)
        {
            switch (iconType)
            {
                case "yellow":
                    icon.sprite = yellow;
                    break;
                case "blue":
                    icon.sprite = blue;
                    break;
                case "empty":
                    icon.sprite = empty;
                    break;
            }
        }
    }
}