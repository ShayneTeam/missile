﻿using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Bazooka
{
    public class BazookaPartsSlot : MonoBehaviour
    {
        [SerializeField] public Image icon;
        [SerializeField] public TextMeshProUGUI levelText;
        [SerializeField] public GameObject locked;

        public void Init(string partsIdx)
        {
            // 아이콘 
            var iconPath = DataboxController.GetDataString(Table.Bazooka, Sheet.Parts, partsIdx, Column.resource_id);
            var sprite = AtlasController.GetSprite(iconPath);
            icon.SetSpriteWithOriginSIze(sprite);
            
            // 레벨 
            var level = UserData.My.Bazooka.GetPartsLevel(partsIdx);
            levelText.text = Localization.GetFormatText("info_145", level.ToLookUpString());
            
            // 잠김 
            var isUnlock = InGameControlHub.My.BazookaController.Parts.IsUnlock(partsIdx);
            locked.SetActive(!isUnlock);
        }
    }
}