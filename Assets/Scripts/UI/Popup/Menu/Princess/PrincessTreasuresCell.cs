using Global;
using InGame;
using InGame.Controller;
using InGame.Controller.Princess;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Princess
{
    public delegate bool IsSelectedDelegate(string treasureIdx);
    
    public class PrincessTreasuresCell : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private Image slot;
        [SerializeField] private GameObject highlight;
        [SerializeField] private GameObject heartIcon;
        [SerializeField] private TextMeshProUGUI levelText;
        
        [SerializeField] private GameObject lockParent;

        [SerializeField] private GameObject gaugeParent;
        [SerializeField] private SlicedFilledImage gauge;
        [SerializeField] private TextMeshProUGUI countText;

        private string _idx;

        public static event StringDelegate OnClicked;
        public static IsSelectedDelegate IsSelected;

        [RuntimeInitializeOnLoadMethod]
        private static void EnterPlayMode()
        {
            OnClicked = null;
            IsSelected = null;
        }

        public void Init(string idx)
        {
            _idx = idx;

            // 아이콘 
            var iconName = DataboxController.GetDataString(Table.Princess, Sheet.girl, idx, Column.resource_id);
            icon.sprite = AtlasController.GetSprite(iconName);
            
            // 슬롯 
            var slotPath = DataboxController.GetDataString(Table.Princess, Sheet.girl, idx, Column.slot_id);
            slot.sprite = AtlasController.GetSprite(slotPath);
            
            // 갱신
            Refresh();
        }

        public void Refresh()
        {
            // 상태 가져오기 
            if (!UserData.My.Princess.TryGetStateTreasure(_idx, out var count, out var grade, out var level))
            {
                gaugeParent.SetActive(false);
                lockParent.SetActive(true);
                highlight.SetActive(false);
                heartIcon.SetActive(false);
                return;
            }
            
            gaugeParent.SetActive(true);
            lockParent.SetActive(false);
            
            // 보유량 바
            var clampedCount = Mathf.Clamp(count, 0, 999);  //999개 초과시 버림
            var requiredCount = PrincessControl.TreasureSynthesis.RequiredCount;
            var ratio = clampedCount / (float)requiredCount;
            gauge.fillAmount = ratio;
            
            // 보유 갯수 
            countText.text = Localization.GetFormatText("info_134", clampedCount.ToLookUpString(), requiredCount.ToLookUpString());
            
            // 레벨 
            levelText.text = Localization.GetFormatText("info_145", level.ToLookUpString());
            
            // 선택 이펙트 
            var select = IsSelected?.Invoke(_idx) ?? false;
            highlight.SetActive(select);
            heartIcon.SetActive(select);
        }

        [PublicAPI]
        public void OnClick()
        {
            OnClicked?.Invoke(_idx);
        }
    }
}