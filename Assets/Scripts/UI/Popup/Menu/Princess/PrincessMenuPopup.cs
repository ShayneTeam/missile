﻿using System;
using System.Linq;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Controller.Princess;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup.Menu.Shop;
using UI.Popup.Menu.Soldier;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Princess
{
    public class PrincessMenuPopup : UIPopupCommon
    {
        [SerializeField] private PrincessInfo princessInfo;
        [SerializeField] private TreasureInfo treasureInfo;
        [SerializeField] private PrincessTreasuresView treasuresView;

        private bool _init;
        private string _selectedTreasureIdx;

        [PublicAPI]
        public void OnShow()
        {
            // 위임처리 지정 (세팅보다 먼저 해야 함)
            PrincessTreasuresCell.IsSelected += IsSelectedTreasure;
            
            // 세팅
            if (!_init)
            {
                Init();
                
                _init = true;
            }
            else
            {
                Refresh();
            }
        
            // 이벤트
            PrincessTreasuresCell.OnClicked += OnClickedTreasure;
            UserData.My.Princess.OnGradeUp += OnChangedTreasure;
            InGameControlHub.My.PrincessController.Level.OnLevelUp += OnLevelUpTreasure;
            PrincessControl.TreasureSynthesis.OnSynthesized += OnSynthesizedTreasure;
        }

        [PublicAPI]
        public void OnHide()
        {
            Unregister();
        }

        private void OnDestroy()
        {
            // EnterPlayMode용 
            Unregister();
        }

        private void Unregister()
        {
            // 위임처리 해제 
            PrincessTreasuresCell.IsSelected -= IsSelectedTreasure;

            // 이벤트
            PrincessTreasuresCell.OnClicked -= OnClickedTreasure;
            UserData.My.Princess.OnGradeUp -= OnChangedTreasure;
            InGameControlHub.My.PrincessController.Level.OnLevelUp -= OnLevelUpTreasure;
            PrincessControl.TreasureSynthesis.OnSynthesized -= OnSynthesizedTreasure;
        }

        private void Init()
        {
            // UI 처음 떴을 때, 최초로 선택시킬 보물 설정 (중요)
            _selectedTreasureIdx = DataboxController.GetAllIdxes(Table.Princess, Sheet.girl).FirstOrDefault();
            
            // 공주정보
            princessInfo.Init();
            
            // 보물정보
            treasureInfo.Init(_selectedTreasureIdx);
                
            // 보물뷰 
            treasuresView.Init();
        }

        private void Refresh()
        {
            // 공주정보
            princessInfo.Refresh();
            
            // 보물정보 
            treasureInfo.Refresh();
                
            // 보물뷰 
            treasuresView.Refresh();
        }

        /*
         * 이벤트 
         */
        #region Events

        private void OnChangedTreasure(string treasureIdx)
        {
            Refresh();
        }
        
        private void OnLevelUpTreasure()
        {
            Refresh();
        }
        
        private void OnSynthesizedTreasure(int synthesisCount)
        {
            Refresh();
        }

        #endregion

        /*
         * 보물 클릭
         */
        #region Clicked Trasure 

        private void OnClickedTreasure(string treasureIdx)
        {            
            _selectedTreasureIdx = treasureIdx;
            
            // 보물정보 갱신 
            treasureInfo.Init(treasureIdx);
            
            // 보물뷰 갱신
            treasuresView.Refresh();
        }

        private bool IsSelectedTreasure(string treasureIdx)
        {
            return _selectedTreasureIdx == treasureIdx;
        }

        #endregion

        /*
         * 바주카 교체 
         */
        [PublicAPI]
        public void ChangeBazooka()
        {
            UIPopup.GetPopup("Princess Change Bazooka").Show();
        }
    }
}