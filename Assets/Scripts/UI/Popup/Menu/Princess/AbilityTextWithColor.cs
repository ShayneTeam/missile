﻿using System;
using UnityEngine;

namespace UI.Popup.Menu.Princess
{
    [RequireComponent(typeof(AbilityText))]
    public class AbilityTextWithColor : MonoBehaviour
    {
        [SerializeField] private Color enableColor;
        [SerializeField] private Color disableColor;
        private AbilityText _abilityText;

        private void Awake()
        {
            _abilityText = GetComponent<AbilityText>();
        }

        public void Init(string abilityIdx, float value)
        {
            _abilityText.Init(abilityIdx, value);
        }

        public void SetEnable(bool enable)
        {
            _abilityText.NameText.color = enable ? enableColor : disableColor;
            _abilityText.DescText.color = enable ? enableColor : disableColor;
        }
    }
}