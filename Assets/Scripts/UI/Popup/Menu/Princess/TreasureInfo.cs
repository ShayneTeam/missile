﻿using System;
using System.Collections.Generic;
using System.Linq;
using Global;
using InGame;
using InGame.Controller;
using InGame.Controller.Princess;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UI.Popup.Menu.Soldier;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Princess
{
    public class TreasureInfo : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI rarityText;
        [SerializeField] private Image slot;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI levelText;
        
        [SerializeField] private GameObject lockParent; 
        
        /*
         * 등급
         */
        [Serializable]
        public class GradeSprite
        {
            public string type;
            public Sprite sprite;
        }
        [SerializeField] private List<GradeSprite> gradeSprites;
        
        [SerializeField] private Transform gradesParent;
        private Image[] _grades;
        
        /*
         * 어빌리티
         */
        [SerializeField] private AbilityText ability1;
        [SerializeField] private AbilityText ability2;
        
        /*
         * 레벨업, 등급업
         */
        [SerializeField] private GameObject upgradeParent;

        [Serializable]
        public class LevelUpFields
        {
            public GameObject root;
            public Image slot;
            public Image icon;
            public TextMeshProUGUI countText;
        }
        [SerializeField] private LevelUpFields levelUp;
        [SerializeField] private GameObject maxLevelText;

        [Serializable]
        public class GradeUpFields
        {
            public GameObject root;
            public TextMeshProUGUI countText;
        }
        [SerializeField] private GradeUpFields gradeUp;
         
        /*
         * 일반 필드 
         */
        private string _treasureIdx;

        private void Awake()
        {
            _grades = gradesParent.GetComponentsInChildren<Image>();
        }

        public void Init(string treasureIdx)
        {
            _treasureIdx = treasureIdx;

            // 아이콘 
            var iconName = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.resource_id);
            icon.sprite = AtlasController.GetSprite(iconName);
            levelUp.icon.sprite = AtlasController.GetSprite(iconName);
            
            // 슬롯 
            var slotName = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.slot_id);
            slot.sprite = AtlasController.GetSprite(slotName);
            levelUp.slot.sprite = AtlasController.GetSprite(slotName);

            // 이름 
            var nameKey = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.name);
            nameText.text = Localization.GetText(nameKey);
            
            // 희귀도
            rarityText.text = Localization.GetText(DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.rarity_name));
            
            // 갱신 
            Refresh();
        }

        public void Refresh()
        {
            var isCollected = UserData.My.Princess.TryGetStateTreasure(_treasureIdx, out var count, out var grade, out var level);
            
            lockParent.SetActive(!isCollected);
            upgradeParent.SetActive(isCollected);

            // 레벨 
            var maxLevel = InGameControlHub.My.PrincessController.Level.GetMax(_treasureIdx);
            levelText.text = Localization.GetFormatText("stone_003", level.ToLookUpString(), maxLevel.ToLookUpString());
            
            // 등급 
            if (grade > _grades.Length) // 5 보다 크면 
            {
                for (var i = 0; i < _grades.Length; i++)
                {
                    if (i < grade - _grades.Length)  
                        _grades[i].sprite = gradeSprites.FirstOrDefault(x => x.type == "blue")?.sprite;
                    else
                        _grades[i].sprite = gradeSprites.FirstOrDefault(x => x.type == "yellow")?.sprite;
                }
            }
            else // 5 보다 작으면
            {
                for (var i = 0; i < _grades.Length; i++)
                {
                    if (i < grade)
                        _grades[i].sprite = gradeSprites.FirstOrDefault(x => x.type == "yellow")?.sprite;
                    else
                        _grades[i].sprite = gradeSprites.FirstOrDefault(x => x.type == "empty")?.sprite;
                }
            }
            
            // 어빌리티 
            InGameControlHub.My.PrincessController.GetAbility1IdxAndValue(_treasureIdx, out var ab1Idx, out var ab1Value);
            ability1.Init(ab1Idx, ab1Value);
            InGameControlHub.My.PrincessController.GetAbility2IdxAndValue(_treasureIdx, out var ab2Idx, out var ab2Value);
            ability2.Init(ab2Idx, ab2Value);
            
            // 미획득 시, 아래 처리는 생략
            if (!isCollected) return;

            // 최대 등급 체크 
            var isMaxGrade = InGameControlHub.My.PrincessController.Grade.IsMax(_treasureIdx);
            var isMaxLevel = InGameControlHub.My.PrincessController.Level.IsMax(_treasureIdx);
            
            // 한 프레임에 SetActive(false) -> SetActive(true) 시키면, 버튼의 칼라틴트 애니메이션 씹힘
            // 켜져있었다면, 계속 켜진 상태를 유지시켜야, 칼라틴트 애니메이션이 작동함
            levelUp.root.SetActive(!isMaxLevel);
            gradeUp.root.SetActive(!isMaxGrade && isMaxLevel);
            maxLevelText.SetActive(isMaxGrade && isMaxLevel);

            // 최대 등급 시, 아래 처리 스킵 
            if (isMaxGrade && isMaxLevel) return;
            
            // 최대 레벨 체크
            if (isMaxLevel)
            {
                // 등급업 
                var requiredCount = PrincessControl.TreasureGrade.GetRequiredChipsetCount(_treasureIdx);
                gradeUp.countText.text = Localization.GetFormatText("info_136", requiredCount.ToLookUpString());
                gradeUp.countText.color = InGameControlHub.My.PrincessController.Grade.HasEnoughCount(_treasureIdx) ? Color.white : Color.red;
            }
            else
            {
                // 레벨업
                var requiredCount = PrincessControl.TreasureLevel.GetRequiredCount(_treasureIdx, 1);
                levelUp.countText.text = Localization.GetFormatText("info_136", requiredCount.ToLookUpString());
                levelUp.countText.color = count >= requiredCount ? Color.white : Color.red;
            }
        }

        [PublicAPI]
        public void LevelUp()
        {
            // 보유 갯수 체크
            if (!InGameControlHub.My.PrincessController.Level.HasEnoughCount(_treasureIdx, 1))
            {
                MessagePopup.Show("girl_007");
                return;
            }
            
            // 레벨업
            InGameControlHub.My.PrincessController.Level.Up(_treasureIdx, 1);
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_upgrade_01");
        }
        
        [PublicAPI]
        public void GradeUp()
        {
            // 강화칩 보유 체크 
            if (!InGameControlHub.My.PrincessController.Grade.HasEnoughCount(_treasureIdx))
            {
                MessagePopup.Show("info_116");
                return;
            }

            // 강화칩 사용 확인 팝업 
            YesOrNoPopup.NoticeChipsetUse(() =>
            {
                // 사운드 
                SoundController.Instance.PlayEffect("sfx_ui_bt_enchant_01");
                
                // 등급업
                InGameControlHub.My.PrincessController.Grade.Up(_treasureIdx);
            });
        }
    }
}