﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Controller;
using InGame.Global;
using JetBrains.Annotations;
using UI.Popup.Menu.Shop;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Princess
{
    public class PrincessTreasuresView : MonoBehaviour
    {
        private ICollection<string> _allIdxes;
        private PrincessTreasuresCell[] _cells;

        private void Awake()
        {
            _cells = GetComponentsInChildren<PrincessTreasuresCell>();
        }

        public void Init()
        {
            // 데이터 
            _allIdxes = DataboxController.GetAllIdxes(Table.Princess, Sheet.girl);
            
            // 초기화 
            for (var i = 0; i < _allIdxes.Count; i++)
            {
                var idx = _allIdxes.ElementAt(i);
                
                _cells[i].Init(idx);
            }
        }

        public void Refresh()
        {
            foreach (var cell in _cells)
            {
                cell.Refresh();
            }
        }
        
        [PublicAPI]
        public void SynthesizeBatch()
        {
            YesOrNoPopup.Show("info_225", "girl_001", "info_227", "info_082",
                () =>
                {
                    // 일괄 합성 
                    var results = InGameControlHub.My.PrincessController.Synthesis.SynthesizeBatch();
                    if (results.Count != 0)
                    {
                        // 일괄 합성 결과 팝업 
                        ShowRewardsPopup.Show(results, "info_225");
                    }
                    else
                    {
                        // 실패 메시지
                        MessagePopup.Show("girl_016");
                    }
                }, 
                () => {} 
            );
        }
        
        [PublicAPI]
        public void LevelUpBatch()
        {
            YesOrNoPopup.Show("info_408", "girl_018", "info_105", "info_082",
                () =>
                {
                    // 일괄 레벨업 
                    var success = InGameControlHub.My.PrincessController.Level.UpBatch();
                    MessagePopup.Show(success ? "girl_017" : "girl_019");
                }, 
                () => {} 
            );
        }
        
        [PublicAPI]
        public void MoveToShop()
        {
            var menu = InGameMenu.Open(InGameMenuType.ShopMenu);
            var shopMenuPopup = menu.GetComponent<ShopMenuPopup>();
            shopMenuPopup.ChangeTab(ShopTab.Missile);
            
            // 몇 번째 셀인지 알아오기 
            var missileTabAllIdxes = DataboxController.GetAllIdxes(Table.Shop, Sheet.missile);
            var gachaCellIdx = missileTabAllIdxes.FirstOrDefault(idx => DataboxController.GetDataString(Table.Shop, Sheet.missile, idx, Column.Type) == "gacha_girl");
            var gachaCellIndex = missileTabAllIdxes.ToList().IndexOf(gachaCellIdx);
            
            shopMenuPopup.MoveScrollPosition(gachaCellIndex);
        }
    }
}