﻿using System;
using System.Linq;
using Global;
using InGame;
using InGame.Controller;
using InGame.Controller.Princess;
using InGame.Data;
using InGame.Global;
using Ludiq;
using TMPro;
using UI.Popup.Menu.Soldier;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Princess
{
    public class PrincessInfo : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private Image body;
        [SerializeField] private Image bazooka;
        
        [SerializeField] private Transform abilitiesParent;
        private AbilityTextWithColor[] _abilitiesText;

        [SerializeField] private SkillSlotEnergyBeam skillSlot;
        

        private void Awake()
        {
            _abilitiesText = abilitiesParent.GetComponentsInChildren<AbilityTextWithColor>();
            
            // 이벤트
            UserData.My.Princess.OnEquipped += RefreshBazooka;
            UserData.My.Princess.OnWear += RefreshCostume;
        }

        private void OnDestroy()
        {
            // 이벤트
            UserData.My.Princess.OnEquipped -= RefreshBazooka;
            UserData.My.Princess.OnWear -= RefreshCostume;
        }

        public void Init()
        {
            Refresh();
        }

        public void Refresh()
        {
            // 코스튬 
            RefreshCostume(UserData.My.Princess.WornCostume);

            // 바주카 
            var equippedBazookaIdx = UserData.My.Princess.EquippedBazooka;
            RefreshBazooka(equippedBazookaIdx);

            // 스킬 
            skillSlot.Refresh();

            // 어빌리티 
            var allAbilitiesType = PrincessControl.GetAllAbilitiesType();
            var allAppliedAbilities = InGameControlHub.My.PrincessController.GetAllAppliedAbilities();
            for (var i = 0; i < _abilitiesText.Length; i++)
            {
                var abilityText = _abilitiesText[i];

                // 예외 처리 
                if (i >= allAbilitiesType.Count())
                {
                    abilityText.gameObject.SetActive(false);
                    continue;;
                }
                
                var abType = allAbilitiesType.ElementAt(i);
                
                // 보유 체크 
                var hasAb = allAppliedAbilities.TryGetValue(abType, out var value);
                
                var abIdx = AbilityControl.GetIdxByType(abType);
                abilityText.Init(abIdx, value);
                
                // 보유 여부에 따라 활성/비활성 
                abilityText.SetEnable(hasAb);
            }
        }

        private void RefreshBazooka(string bazookaIdx)
        {
            var iconPath = DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, bazookaIdx, Column.resource_id);
            bazooka.SetSpriteWithOriginSIze(AtlasController.GetSprite(iconPath));
        }
        
        private void RefreshCostume(string costumeIdx)
        {
            // 타이틀 
            title.text = Localization.GetText(DataboxController.GetDataString(Table.Costume, Sheet.princess, costumeIdx, Column.name));

            // 코스튬 
            body.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Costume, Sheet.princess, costumeIdx, Column.resource));
            
            var iconPath = DataboxController.GetDataString(Table.Costume, Sheet.princess, costumeIdx, Column.ui_resource);
            body.SetSpriteWithOriginSIze(AtlasController.GetSprite(iconPath));
        }
    }
}