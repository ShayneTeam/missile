﻿using System;
using Doozy.Engine.UI;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Stone
{
    [RequireComponent(typeof(UIButton))]
    [RequireComponent(typeof(NoticeIcon))]
    public class PrincessButton : MonoBehaviour
    {
        private NoticeIcon _notice;
        private ImageMaterialSwapper _imageMaterial;

        private void Awake()
        {
            _notice = GetComponent<NoticeIcon>();
            _imageMaterial = GetComponent<UIButton>().Button.image.GetComponent<ImageMaterialSwapper>();
        }

        private void Start()
        {
            RaidController.OnVictory += OnVictoryRaid;
            // 로그아웃 처리용
            Stage.OnStart += Refresh;
            
            // 갱신
            Refresh();
        }

        private void OnDestroy()
        {
            RaidController.OnVictory -= OnVictoryRaid;
            Stage.OnStart -= Refresh;
        }

        private void Refresh()
        {/*
            // 레드닷
            var canLevelUpHelmet = InGameControlHub.My.InfStoneController.Helmet.CanLevelUp();
            var canLevelUpStone = InGameControlHub.My.InfStoneController.Stone.HasAnyStoneForLevelUp();
            _notice.notice.SetActive(isUnlocked && (canLevelUpHelmet || canLevelUpStone));*/
            
            // 잠김 여부에 따라 회색 메테리얼 적용
            var isLocked = InGameControlHub.My.PrincessController.IsLocked;
            _imageMaterial.Init(!isLocked);
            
            // 레드닷
        }

        [PublicAPI]
        public void OpenMenu()
        {
            // 컨텐츠 미해금 시 
            if (InGameControlHub.My.PrincessController.IsLocked)
            {
                // 메시지
                MessagePopup.Show("girl_009");

                return;
            }
            
            InGameMenu.Open("Princess Menu");
            
            // 눌릴 때 마다, 혹시 모르니 갱신 처리
            Refresh();
        }

        [PublicAPI]
        public void TransitionMenu()
        {
            // 메뉴에서 버튼 눌렸을 때, 조건 선체크 하기 위한 야매 처리 함수
            
            // 컨텐츠 미해금 시
            if (InGameControlHub.My.PrincessController.IsLocked)
            {
                // 메시지
                MessagePopup.Show("girl_009");

                return;
            }
            
            InGameMenu.TransitionMenuExternal("Princess Menu");
            
            // 눌릴 때 마다, 혹시 모르니 갱신 처리
            Refresh();
        }

        private void OnVictoryRaid()
        {
            Refresh();
        }
    }
}