﻿using System.Collections.Generic;
using System.Linq;
using Bolt;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using JetBrains.Annotations;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu
{
    [RequireComponent(typeof(UIPopup))]
    public class InGameMenu : MonoBehaviour
    {
        [SerializeField] private List<GameObject> buttons;
        
        // 컨테이너를 팝업으로 구현 
        private static UIPopup _menu;
        private static UIPopup _menuContents;

        public static bool BlockTransitionMenu { get; set; }
        
        // 렌더링 최적화용
        //private Camera _mainCamera;

        // 요 코드는 위험성 있음 
        // set에서 메뉴가 있을 떄만 세팅함
        // 만약 메뉴가 있었을 때 세팅해둔 후, 메뉴를 닫고, 세팅하려하면 동작되지 않게 됨 
        // 나중에 이 부분을 다시 봐야 한다면, 구조를 바꾸자 
        public static bool HideBackButton
        {
            get => _menu != null && _menu.HideOnBackButton;
            set
            {
                if (_menu != null)
                    _menu.HideOnBackButton = value;
            }
        }

        public static UIPopup Open(string menuName)
        {
            // 인게임 사운드 이펙트 음소거 
            SoundController.Instance.MuteBus("InGame");
            
            // 메뉴 열기
            if (_menu == null)
            {
                _menu = UIPopup.GetPopup("Menu");
                _menu.Show();
            }

            // 메뉴 컨텐츠 열기 
            var menu = _menu.GetComponent<InGameMenu>();
            menu.TransitionMenu(menuName);

            return _menuContents;
        }

        public static bool IsShow()
        {
            return _menu != null;
        }

        public static void SwitchBg(Sprite sprite, Color color)
        {
            if (_menu == null)
                return;
            
            var bg = _menu.Overlay.RectTransform.GetComponent<Image>();
            bg.sprite = sprite;
            bg.color = color;
        }

        public static void TransitionMenuExternal(string menuName)
        {
            if (!IsShow())
                return;
            
            _menu.GetComponent<InGameMenu>().TransitionMenu(menuName);
        }
        
        [PublicAPI]
        public void TransitionMenu(string menuName)
        {
            // 외부서 트랜지션 막았으면, 트랜지션 못하게 함 
            if (BlockTransitionMenu)
                return;
            
            var prevMenuContents = _menuContents;
            if (prevMenuContents != null)
            {
                // 이전 메뉴와 동일한 메뉴 트랜지션일 경우, 무시 
                if (prevMenuContents.PopupName == menuName)
                    return;
                
                // 트랜지션 됐을 땐, 메뉴가 꺼지지 않도록 등록해둔 이벤트 해제
                prevMenuContents.HideBehavior.OnStart.Action -= OnHideMenuContents;
                prevMenuContents.Hide();
            }
            
            var newMenuContents = UIPopup.GetPopup(menuName);
            
            // 메뉴 컨텐츠에서 닫혔다면, 메뉴도 닫히도록 함 
            newMenuContents.HideBehavior.OnStart.Action += OnHideMenuContents;
            
            newMenuContents.Show();

            _menuContents = newMenuContents;
            
            /*
             * HACK 굉장히 야매지만 유용한 해킹 처리
             * Deactive 돼있는 볼트는 이벤트를 보내도 처리되지 않음
             * 여기선 아직 팝업이 떠있지 않은 상태라 버튼이 없는 상황
             * 1프레임 기다렸다가 다음 프레임에 확실하게 처리시킨다 
             */
            Timing.CallDelayed(0f, () => { ShowMenuSelectedImage(menuName); }, gameObject);
            
            // 수동 GC
            GCController.CollectFull();
        }

        [PublicAPI]
        public void OnShowFinished()
        {/*
            // Game 뷰에서 No Camera 메시지 뜨는 게 성가셔서 에디터 에선 사용 안 함 
            if (!ExtensionMethods.IsUnityEditor())
            {
                // 카메라
                _mainCamera = Camera.main;
                if (_mainCamera != null)
                    _mainCamera.enabled = false;
            }*/
            
            // 수동 GC
            GCController.CollectFull();
            
            // GC 예산 메뉴버전으로 설정
            GCController.Instance.SetBudget(FirebaseRemoteConfigController.gcBudgetMenu);
        }
        
        [PublicAPI]
        public void OnHideStarted()
        {/*
            // 카메라 
            if (_mainCamera != null)
                _mainCamera.enabled = true;*/
            
            // 수동 GC
            GCController.CollectFull();
            
            // GC 예산 롤백 
            GCController.Instance.RollbackBudget();
        }

        private void ShowMenuSelectedImage(string menuName)
        {
            // 모든 버튼의 선택 품 
            const string eventName = "Select";
            foreach (var button in buttons) CustomEvent.Trigger(button, eventName, false);

            // 넘어온 버튼만 선택 켬 
            var menuButton = buttons.FirstOrDefault(button => menuName.Contains(button.name));
            if (menuButton != null)
                CustomEvent.Trigger(menuButton, eventName, true);
        }

        private void OnHideMenuContents(GameObject go)
        {
            Close();
        }
        
        private void Close()
        {
            if (_menu != null)
            {
                _menu.Hide();
                _menu = null;
            }

            if (_menuContents != null)
            {
                _menuContents.HideBehavior.OnStart.Action -= OnHideMenuContents;
                _menuContents.Hide();
                _menuContents = null;
            }
            
            // 인게임 사운드 이펙트 음소거 풀기 
            SoundController.Instance.UnmuteBus("InGame");
        }
    }
}