﻿using System.Collections.Generic;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Soldier
{
    public class SoldierInfo : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private Image icon;
        
        [SerializeField] private List<SoldierGradeSlot> grades;
        [SerializeField] private TextMeshProUGUI gradeText;
        
        [SerializeField] private List<CommonAbilityText> abilitiesText;

        [SerializeField] private Image nextIcon;
        [SerializeField] private SoldierPromoteButton button;
        [SerializeField] private GameObject maxLevelText;
        
        private string _soldierType;
        
        
        
        private void Start()
        {
            UserData.My.Soldiers.OnPromoted += ShowEnhanceEffect;
        }

        private void OnDestroy()
        {
            UserData.My.Soldiers.OnPromoted -= ShowEnhanceEffect;
        }
        
        private void ShowEnhanceEffect(string _)
        {
            var canvas = UICanvas.GetUICanvas("UI Effect");
            EffectSpawner.Instance.Spawn("enhance UI effect", icon.transform.position, canvas.transform);
        }

        public void Init(string soldierType)
        {
            _soldierType = soldierType;
            
            Refresh();
        }

        public void Refresh()
        {
            // 해당 솔져의 데이터 상태 가져옴 
            UserData.My.Soldiers.GetState(_soldierType, out var grade, out var level);
            
            // 레벨 
            levelText.text = level.ToLookUpString();
            
            // 등급 슬롯 
            for (var i = 0; i < grades.Count; i++)
            {
                var enable = i < grade;
                grades[i].Init(enable);
            }
            
            // 등급 텍스트 
            gradeText.text = Localization.GetFormatText("info_144", grade.ToLookUpString());

            // grade 테이블 인덱스를 알아야 스폰 위치, 스폰할 솔져 인덱스 알아올 수 있음 
            InGame.Soldier.GetGradeIdx(_soldierType, grade, out var gradeIdx);

            // 이름
            var soldierNameKey = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, "name");
            nameText.text = Localization.GetText(soldierNameKey);

            // 아이콘 
            var resourceUI = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, "resource_ui");
            icon.SetSpriteWithOriginSIze(AtlasController.GetSprite(resourceUI));
            
            var count = DataboxController.GetDataInt(Table.Soldier, Sheet.Grade, gradeIdx, "soldier_count", 1);
            for (var index = 1; index <= count; index++)
            {
                // 용병 인덱스
                var soldierIdxValue = $"soldier_{index.ToLookUpString()}_idx";
                var soldierIdx = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, soldierIdxValue);
            }
            
            // 어빌리티 효과 
            var unlockedAbCount = InGameControlHub.My.SoldierController.GetUnlockedAbilitiesCount(_soldierType);

            const int abilityMax = 8;
            for (var index = 0; index < abilityMax; index++)
            {
                var abilityColumnName = $"aidx_{(index + 1).ToLookUpString()}";

                var unlockLevel = DataboxController.GetDataInt(Table.Soldier, Sheet.UnlockAbility, abilityColumnName, "unlock_level");

                var abilityIdx = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, abilityColumnName);

                // 레벨 
                var levelStr = Localization.GetFormatText("info_145", unlockLevel.ToLookUpString());

                // 이름
                var nameKey = DataboxController.GetDataString(Table.Ability, Sheet.Ability, abilityIdx, Column.name);
                var nameStr = Localization.GetText(nameKey);

                // 설명 
                var descKey = DataboxController.GetDataString(Table.Ability, Sheet.Ability, abilityIdx, Column.desc);
                var value = DataboxController.GetDataFloat(Table.Ability, Sheet.Ability, abilityIdx, Column.value);
                var descStr = Localization.GetFormatText(descKey, value.ToUnitOrDecimalString());

                var abText = abilitiesText[index];
                abText.Init(levelStr, nameStr, descStr);

                var enable = index < unlockedAbCount;
                abText.SetEnable(enable);
            }
            
            // 다음 등급 관련 
            var isMaxGrade = InGameControlHub.My.SoldierController.IsMaxGrade(_soldierType);
            var isEarned = UserData.My.Soldiers.IsEarned(_soldierType);
            
            nextIcon.gameObject.SetActive(!isMaxGrade);
            
            button.gameObject.SetActive(!isMaxGrade && isEarned);
            maxLevelText.SetActive(isEarned);
            
            if (!isMaxGrade)
            {
                // 다음 등급 이미지
                InGame.Soldier.GetGradeIdx(_soldierType, grade + 1, out var nextGradeIdx);
                var nextResourceUI = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, nextGradeIdx, "resource_ui");
                nextIcon.SetSpriteWithOriginSIze(AtlasController.GetSprite(nextResourceUI));

                // 승격 버튼 
                button.Init(_soldierType);
            }
        }
    }
}