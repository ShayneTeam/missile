﻿using System;
using System.Collections.Generic;
using Bolt;
using DG.Tweening;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Soldier
{
    public class SoldierUpgradeSlot : MonoBehaviour
    {
	    [SerializeField] public string soldierType;
	    
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI desc1Text;
        [SerializeField] private TextMeshProUGUI desc2Text;
        
        [SerializeField] private TextMeshProUGUI lockText;

        [SerializeField] private Image shipIcon;

        [SerializeField] private GameObject effect;

        [SerializeField] private List<SoldierUpgradeButton> buttons;

        private void Start()
        {
	        UserData.My.Soldiers.OnLevelUp += ShowEffect;
        }

        private void OnDestroy()
        {
	        UserData.My.Soldiers.OnLevelUp -= ShowEffect;
        }

        private void ShowEffect(string type)
        {
	        if (soldierType != type)
		        return;
	        
	        var canvas = UICanvas.GetUICanvas("UI Effect");
	        EffectSpawner.Instance.Spawn("upgrade UI effect (small)", icon.transform.position, canvas.transform);
        }

        public void Init()
        {
	        // 버튼
	        foreach (var button in buttons) button.Init(soldierType);

	        // 갱신
	        Refresh();
        }

        public void Refresh()
        {
	        UserData.My.Soldiers.GetState(soldierType, out var grade, out var level);
	        
	        InGame.Soldier.GetGradeIdx(soldierType, grade, out var gradeIdx);
	        
	        // 레벨 
	        levelText.text = Localization.GetFormatText("info_145", level.ToLookUpString());

	        // 이름
	        var nameKey = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, "name");
	        nameText.text = Localization.GetText(nameKey);
	        
	        // 아이콘 
	        var resourceId = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, "resource_ui");
	        icon.SetSpriteWithOriginSIze(AtlasController.GetSprite(resourceId));
	        
	        // 함선 타입 전용 추가 아이콘 
	        if (soldierType == "ship")	
		        shipIcon.SetSpriteWithOriginSIze(AtlasController.GetSprite(resourceId));

	        // 설명 
	        var damage = 0d;
	        var shootMin = 0f;
	        var shootMax = 0f;
	        
	        var count = DataboxController.GetDataInt(Table.Soldier, Sheet.Grade, gradeIdx, "soldier_count", 1);
	        for (var index = 1; index <= count; index++)
	        {
		        // 용병 인덱스
		        var soldierIdxValue = $"soldier_{index.ToLookUpString()}_idx";
		        var soldierIdx = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, soldierIdxValue);

		        // 데미지
		        damage += InGameControlHub.My.SoldierController.GetFinalizedDamage(soldierIdx);
		        
		        // 발사속도 (여러 개 중 한 마리껄로 걍 세팅)
		        var type = (SoldierType)Enum.Parse(typeof(SoldierType), soldierType);
		        InGameControlHub.My.SoldierController.GetFinalizedAtkDelay(soldierIdx, type, out var min, out var max);
		        shootMin = min;
		        shootMax = max;
	        }
	        desc1Text.text = Localization.GetFormatText("army_info_01", damage.ToUnitString());
	        desc2Text.text = Localization.GetFormatText("army_info_03", ((shootMin + shootMax) / 2f).ToDecimalString());
	        
	        // 최대 레벨 체크 
	        var isMaxLevel = InGameControlHub.My.SoldierController.IsMaxLevel(soldierType);
	        var isEarned = UserData.My.Soldiers.IsEarned(soldierType);

	        var visibleButton = !isMaxLevel && isEarned;
	        
			// 버튼
	        foreach (var button in buttons)
	        {
		        button.gameObject.SetActive(visibleButton);
		        
		        if (visibleButton)
			        button.Refresh();
	        }

	        // 최대 레벨일 시
	        if (isMaxLevel)
	        {
		        // 최대 레벨 텍스트 
		        lockText.text = Localization.GetText("info_106");
	        }
	        // 미획득 시
	        else if (!isEarned)
	        {
		        var earnSheet = DataboxController.GetAllIdxes(Table.Stage, Sheet.Earn);

		        foreach (var earnStage in earnSheet)
		        {
			        var earnSoldierType = DataboxController.GetDataString(Table.Stage, Sheet.Earn, earnStage, "soldier_type");
			        if (earnSoldierType == soldierType)
			        {
				        // 용병 획득 스테이지 알려줌 
				        lockText.text = Localization.GetFormatText("info_149", earnStage);				        
				        break;
			        }
		        }
	        }
        }

        public void Select(bool select)
        {
	        // 선택 효과 
	        effect.SetActive(select);
        }
    }
}