﻿using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Soldier
{
    public class SoldierGradeSlot : MonoBehaviour
    {
        [SerializeField] private Image image;
        
        [SerializeField] private Sprite enabledSprite;
        [SerializeField] private Sprite disabledSprite;

        public void Init(bool enable)
        {
            image.sprite = enable ? enabledSprite : disabledSprite;
        }
    }
}