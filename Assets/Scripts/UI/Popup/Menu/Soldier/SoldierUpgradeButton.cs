﻿using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Soldier
{
    public class SoldierUpgradeButton : MonoBehaviour
    {
        [SerializeField] private GameObject button;
        [SerializeField] private TextMeshProUGUI buttonText;
        [SerializeField] private int upgradeCount;
        
        private string _soldierType;
        
        private ButtonSpriteSwapper _buttonSpriteSwapper;
        
        
        private void Start()
        {
            UserData.My.Resources.OnChangedMineral += OnChangedMineral;
        }

        private void OnDestroy()
        {
            UserData.My.Resources.OnChangedMineral -= OnChangedMineral;
        }

        private void OnChangedMineral(double value)
        {
            RefreshButtonBg();
        }

        public void Init(string soldierType)
        {
            _soldierType = soldierType;

            Refresh();
        }

        public void Refresh()
        {
            // 최대 레벨 체크 
            var isMaxLevel = InGameControlHub.My.SoldierController.IsMaxLevel(_soldierType);
            var isEarned = UserData.My.Soldiers.IsEarned(_soldierType);

            var visibleButton = !isMaxLevel && isEarned;
            if (visibleButton)
            {
                // 버튼 슬롯 
                RefreshButtonBg();

                // 레벨업 필요 미네랄
                var requiredMineral = InGameControlHub.My.SoldierController.GetRequiredMineralForLevelUp(_soldierType, upgradeCount);
                buttonText.text = requiredMineral.ToUnitString();
            }
        }

        [PublicAPI]
        public void LevelUp()
        {
            if (!CanLevelUp())
                return;
		    
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_upgrade_02");
		    
            // 업그레이드 
            InGameControlHub.My.SoldierController.LevelUp(_soldierType, upgradeCount);
        }

        private void RefreshButtonBg()
        {
            if (_buttonSpriteSwapper == null)
                _buttonSpriteSwapper = button.GetComponent<ButtonSpriteSwapper>();
            
            _buttonSpriteSwapper.Init(CanLevelUp());
        }

        private bool CanLevelUp()
        {
            if (string.IsNullOrEmpty(_soldierType))
                return false;
            
            return InGameControlHub.My.SoldierController.CanLevelUp(_soldierType, upgradeCount);
        }
    }
}