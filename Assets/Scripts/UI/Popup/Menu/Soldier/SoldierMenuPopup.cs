﻿using System;
using System.Collections.Generic;
using InGame.Data;
using JetBrains.Annotations;
using UnityEngine;

namespace UI.Popup.Menu.Soldier
{
    public class SoldierMenuPopup : MonoBehaviour
    {
        [SerializeField] private SoldierInfo soldierInfo;
        [SerializeField] private List<SoldierUpgradeSlot> slots;
        [SerializeField] private SoldierAbilities abilities;

        private string _selectedType = "soldier";

        [PublicAPI]
        public void OnShow()
        {
            UserData.My.Soldiers.OnEarned += OnChangedData;
            UserData.My.Soldiers.OnLevelUp += OnLevelUp;
            UserData.My.Soldiers.OnPromoted += OnChangedData;
        }

        [PublicAPI]
        public void OnHide()
        {
            UserData.My.Soldiers.OnEarned -= OnChangedData;
            UserData.My.Soldiers.OnLevelUp -= OnLevelUp;
            UserData.My.Soldiers.OnPromoted -= OnChangedData;
        }

        private void OnChangedData(string soldierType)
        {
            Refresh();
        }
        
        private void OnLevelUp(string soldierType)
        {
            Refresh();
            
            // 선택 
            if (_selectedType != soldierType)
                OnClickedSlot(soldierType);
        }

        [PublicAPI]
        public void Init()
        {
            // 메인
            soldierInfo.Init(_selectedType);
            
            // 슬롯
            foreach (var slot in slots)
            {
                slot.Init();
                slot.Select(slot.soldierType == _selectedType);
            }

            // 어빌리티
            abilities.Init();
        }

        private void Refresh()
        {
            // 메인
            soldierInfo.Refresh();
            
            // 슬롯
            foreach (var slot in slots)
            {
                slot.Refresh();
            }

            // 어빌리티
            abilities.Refresh();
        }

        [PublicAPI]
        public void OnClickedSlot(string type)
        {
            _selectedType = type;
            
            // 메인
            soldierInfo.Init(type);
            
            // 슬롯
            foreach (var slot in slots)
            {
                slot.Select(slot.soldierType == _selectedType);
            }
        }
    }
}