﻿using System;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Soldier
{
    public class CommonAbilityText : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI descText;
        [SerializeField] private TextMeshProUGUI valueText;

        [SerializeField] private Color levelEnableColor;
        [SerializeField] private Color descEnableColor;
        [SerializeField] private Color valueEnableColor;
        
        [SerializeField] private Color disableColor;
        
        
        public void Init(string level, string desc, string value)
        {
            if (levelText != null)
                levelText.text = level;
            if (descText != null)
                descText.text = desc;
            if (valueText != null)
                valueText.text = value;
        }

        public void SetEnable(bool enable)
        {
            if (levelText != null)
                levelText.color = enable ? levelEnableColor : disableColor;
            if (descText != null)
                descText.color = enable ? descEnableColor : disableColor;
            if (valueText != null)
                valueText.color = enable ? valueEnableColor : disableColor;
        }
    }
}