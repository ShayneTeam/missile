﻿using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Soldier
{
    public class SoldierPromoteButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI chipsetText;
        
        private string _soldierType;
        
        
        public void Init(string type)
        {
            _soldierType = type;
            
            // 칩셋 텍스트 
            var ownedChipsetCount = UserData.My.Items[RewardType.REWARD_CHIPSET];
            var needChipsetCount = InGameControlHub.My.SoldierController.GetNeedChipsetCountForEnhance(type);

            var canUpgrade = ownedChipsetCount >= needChipsetCount;

            var formatKey = canUpgrade ? "info_241" : "info_240";
            chipsetText.text = Localization.GetFormatText(formatKey, ownedChipsetCount.ToLookUpString(), needChipsetCount.ToLookUpString());
        }

        [PublicAPI]
        public void OnClick()
        {
            if (!InGameControlHub.My.SoldierController.CanPromote(_soldierType))
            {
                MessagePopup.Show("info_116");
                return;
            }
            
            // 강화칩 사용 확인 팝업 
            YesOrNoPopup.NoticeChipsetUse(() =>
            {
                // 사운드 
                SoundController.Instance.PlayEffect("sfx_ui_bt_enchant_01");
            
                // 승급 
                InGameControlHub.My.SoldierController.Promote(_soldierType);
            });
        }
    }
}
