﻿using System.Collections.Generic;
using Databox.Dictionary;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using UI.Popup.Menu.Relic;
using UnityEngine;

namespace UI.Popup.Menu.Soldier
{
    public class SoldierAbilities : AbilitiesScroll
    {
        protected override OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
            return InGameControlHub.My.SoldierController.GetAllAppliedAbilities();
        }
    }
}