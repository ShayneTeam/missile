﻿using System;
using Databox.Dictionary;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using UnityEngine;

namespace UI.Popup.Menu.Soldier
{
    public delegate void GetTypeAndValueDelegate(int dataIndex, out string type, out float value);
    
    [RequireComponent(typeof(CommonAbilityText))]
    public class CommonAbilityCellView : EnhancedScrollerCellView
    {
        public GetTypeAndValueDelegate GetTypeAndValue = (int index, out string type,out float value) => { type = ""; value = 0f; };
        private CommonAbilityText _commonAbilityText;

        private void Awake()
        {
            _commonAbilityText = GetComponent<CommonAbilityText>();
        }

        public void Init(int index)
        {
            // ReloadData로 호출된 GetCellView 에서는 EnhancedScrollerCellView의 dataIndex가 세팅되기 전이라, 인자로 인덱스를 받아야 함 
            Refresh(index);
        }

        public override void RefreshCellView()
        {
            // RefreshCellView 에서는 EnhancedScrollerCellView의 dataIndex가 세팅된 이후의 호출 됨
            Refresh(dataIndex);
        }

        private void Refresh(int index)
        {
            GetTypeAndValue(index, out var type, out var value);
            
            var idx = AbilityControl.GetIdxByType(type);
            
            // 이름
            var nameKey = DataboxController.GetDataString(Table.Ability, Sheet.Ability, idx, Column.name);
            var nameStr = Localization.GetText(nameKey);

            // 설명 
            var descKey = DataboxController.GetDataString(Table.Ability, Sheet.Ability, idx, Column.desc);
            var descStr = Localization.GetFormatText(descKey, value.ToUnitOrDecimalString());
            
            _commonAbilityText.Init(null, nameStr, descStr);
            _commonAbilityText.SetEnable(true);
        }
    }
}