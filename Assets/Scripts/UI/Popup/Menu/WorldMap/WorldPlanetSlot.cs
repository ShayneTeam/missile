﻿using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.WorldMap
{
    public class WorldPlanetSlot : MonoBehaviour
    {
        [SerializeField] private Image planetIcon;
        [SerializeField] private Image factoryIcon;
        [SerializeField] private TextMeshProUGUI planetName;
        [SerializeField] private TextMeshProUGUI planetStage;
        [SerializeField] private GameObject lockParent;
        
        [SerializeField] private Image resource1Icon;
        [SerializeField] private TextMeshProUGUI resource1Value;
        [SerializeField] private Image resource2Icon;
        [SerializeField] private TextMeshProUGUI resource2Value;

        private string _planetIdx;

        public void Init(string worldPlanetIdx)
        {
            _planetIdx = worldPlanetIdx;
            
            // 행성 아이콘 
            planetIcon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Stage, Sheet.planet, _planetIdx, Column.PlanetIcon));

            // 팩토리 아이콘
            factoryIcon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.WorldMap, Sheet.planet, worldPlanetIdx, Column.FactoryIcon));

            // 행성 이름
            planetName.text = Localization.GetText(DataboxController.GetDataString(Table.Stage, Sheet.planet, _planetIdx, Column.name));

            // 리소스 1 
            {
                // 아이콘
                var res1Type = DataboxController.GetDataString(Table.WorldMap, Sheet.planet, worldPlanetIdx, Column.resource_1);
                resource1Icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Reward, Sheet.reward, res1Type, Column.icon));

                // 값
                resource1Value.text = DataboxController.GetDataInt(Table.WorldMap, Sheet.planet, worldPlanetIdx, Column.resource_1_value).ToLookUpString();
            }

            // 리소스 2 
            {
                // 아이콘
                var res2Type = DataboxController.GetDataString(Table.WorldMap, Sheet.planet, worldPlanetIdx, Column.resource_2);
                resource2Icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Reward, Sheet.reward, res2Type, Column.icon));

                // 값
                resource2Value.text = DataboxController.GetDataInt(Table.WorldMap, Sheet.planet, worldPlanetIdx, Column.resource_2_value).ToLookUpString();
            }
            
            Refresh();
        }

        public void Refresh()
        {
            // 행성 스테이지 
            StageControl.GetStageMinMaxData(_planetIdx, out _, out var stageMax);
            planetStage.text = stageMax.ToLookUpString();

            // 미획득 시, 잠금
            var isOwned = InGameControlHub.My.StageController.IsOwned(_planetIdx);
            lockParent.SetActive(!isOwned);

            resource1Icon.gameObject.SetActive(isOwned);
            resource1Value.gameObject.SetActive(isOwned);
            resource2Icon.gameObject.SetActive(isOwned);
            resource2Value.gameObject.SetActive(isOwned);
        }
    }
}