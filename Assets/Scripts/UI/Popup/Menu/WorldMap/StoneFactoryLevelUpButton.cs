﻿using InGame.Controller;
using TMPro;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.WorldMap
{
    public class StoneFactoryLevelUpButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI uraniumText;

        [SerializeField] private ButtonSpriteSwapper buttonSprite;
        
        public void Init()
        {
            // 비용 표기
            uraniumText.text = InGameControlHub.My.WorldMapController.StoneFactoryLevelUp.GetNeedCost().ToLookUpString();
            
            var isEnoughUraniumForGacha = InGameControlHub.My.WorldMapController.StoneFactoryLevelUp.IsEnoughCost();
            uraniumText.color = isEnoughUraniumForGacha ? Color.white : Color.red;

            // 버튼 배경
            buttonSprite.Init(isEnoughUraniumForGacha);
        }

        public void OnClick()
        {
            if (!InGameControlHub.My.WorldMapController.StoneFactoryLevelUp.IsEnoughCost())
            {
                MessagePopup.Show("info_101");
                return;
            }
            
            InGameControlHub.My.WorldMapController.StoneFactoryLevelUp.DoIt();
        }
    }
}
