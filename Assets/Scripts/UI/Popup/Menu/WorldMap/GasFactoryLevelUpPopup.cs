﻿using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.WorldMap
{
    [RequireComponent(typeof(UIPopup))]
    public class GasFactoryLevelUpPopup : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private TextMeshProUGUI level;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI desc;
        
        [SerializeField] private GasFactoryLevelUpButton button;

        public static GasFactoryLevelUpPopup Show()
        {
            var popup = UIPopup.GetPopup("Gas Factory LevelUp");

            var gasFactoryLevelUp = popup.GetComponent<GasFactoryLevelUpPopup>();
            gasFactoryLevelUp.Init();
            
            popup.Show();

            return gasFactoryLevelUp;
        }

        private void Init()
        {
            UserData.My.WorldMap.GetState(out var gasLevel, out _);
            
            // 타이틀
            title.text = Localization.GetText(DataboxController.GetDataString(Table.WorldMap, Sheet.factory_gas, gasLevel.ToLookUpString(), Column.name));
            
            // 아이콘
            icon.SetSpriteWithOriginSIze(AtlasController.GetSprite(DataboxController.GetDataString(Table.WorldMap, Sheet.factory_gas, gasLevel.ToLookUpString(), Column.icon)));
            
            // 갱신 
            Refresh();
            
            // 이벤트 등록
            UserData.My.WorldMap.OnLevelUpGasFactory += OnLevelUp;
        }

        private void OnDestroy()
        {
            // 이벤트 해제
            UserData.My.WorldMap.OnLevelUpGasFactory -= OnLevelUp;
        }

        private void OnLevelUp()
        {
            // 아이콘에 이펙트 출력 
            var canvas = UICanvas.GetUICanvas("UI Effect");
            EffectSpawner.Instance.Spawn("enhance UI effect", icon.transform.position, canvas.transform);
            
            // 갱신 
            Refresh();
        }

        private void Refresh()
        {
            UserData.My.WorldMap.GetState(out var gasLevel, out _);
            
            // 레벨 
            level.text = Localization.GetFormatText("info_145", gasLevel.ToLookUpString());
            
            // 설명 
            var format = DataboxController.GetDataString(Table.WorldMap, Sheet.factory_gas, gasLevel.ToLookUpString(), Column.bonus_id);
            var bonusPer = DataboxController.GetDataInt(Table.WorldMap, Sheet.factory_gas, gasLevel.ToLookUpString(), Column.bonus_per);
            desc.text = Localization.GetFormatText(format, bonusPer.ToLookUpString());
            
            // 버튼 갱신 
            button.Init();

            var isMaxLevel = InGameControlHub.My.WorldMapController.GasFactoryLevelUp.IsMax();
            button.gameObject.SetActive(!isMaxLevel);
        }
    }
}