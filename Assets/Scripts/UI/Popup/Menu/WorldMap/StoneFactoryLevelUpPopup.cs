﻿using BackEnd;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.WorldMap
{
    public class StoneFactoryLevelUpPopup : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI title;
        [SerializeField] private TextMeshProUGUI level;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI desc;
        
        [SerializeField] private StoneFactoryLevelUpButton button;

        public static StoneFactoryLevelUpPopup Show()
        {
            var popup = UIPopup.GetPopup("Stone Factory LevelUp");

            var stoneFactoryLevelUp = popup.GetComponent<StoneFactoryLevelUpPopup>();
            stoneFactoryLevelUp.Init();
            
            popup.Show();

            return stoneFactoryLevelUp;
        }

        private void Init()
        {
            UserData.My.WorldMap.GetState(out _, out var stoneLevel);
            
            // 타이틀
            title.text = Localization.GetText(DataboxController.GetDataString(Table.WorldMap, Sheet.factory_stone, stoneLevel.ToLookUpString(), Column.name));
            
            // 아이콘
            icon.SetSpriteWithOriginSIze(AtlasController.GetSprite(DataboxController.GetDataString(Table.WorldMap, Sheet.factory_stone, stoneLevel.ToLookUpString(), Column.icon)));
            
            // 갱신 
            Refresh();
            
            // 이벤트 등록
            UserData.My.WorldMap.OnLevelUpStoneFactory += OnLevelUp;
        }

        private void OnDestroy()
        {
            // 이벤트 해제
            UserData.My.WorldMap.OnLevelUpStoneFactory -= OnLevelUp;
        }

        private void OnLevelUp()
        {
            // 아이콘에 이펙트 출력 
            var canvas = UICanvas.GetUICanvas("UI Effect");
            EffectSpawner.Instance.Spawn("enhance UI effect", icon.transform.position, canvas.transform);
            
            // 갱신 
            Refresh();
        }

        private void Refresh()
        {
            UserData.My.WorldMap.GetState(out _, out var stoneLevel);
            
            // 레벨 
            level.text = Localization.GetFormatText("info_145", stoneLevel.ToLookUpString());
            
            // 설명 
            var format = DataboxController.GetDataString(Table.WorldMap, Sheet.factory_stone, stoneLevel.ToLookUpString(), Column.bonus_id);
            var bonusPer = DataboxController.GetDataInt(Table.WorldMap, Sheet.factory_stone, stoneLevel.ToLookUpString(), Column.bonus_per);
            desc.text = Localization.GetFormatText(format, bonusPer.ToLookUpString());
            
            // 버튼 갱신 
            button.Init();

            var isMaxLevel = InGameControlHub.My.WorldMapController.StoneFactoryLevelUp.IsMax();
            button.gameObject.SetActive(!isMaxLevel);
        }
    }
}