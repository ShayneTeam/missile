﻿using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using UI.Popup.Plunder;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.WorldMap
{
    public class WorldMapButton : MonoBehaviour
    {
        [SerializeField] private ImageMaterialSwapper imageMaterial;
        
        private void Start()
        {
            //
            Refresh();
            
            //
            UserData.My.Stage.OnOwnPlanet += OnOwnPlanet;
        }

        private void OnDestroy()
        {
            //
            UserData.My.Stage.OnOwnPlanet -= OnOwnPlanet;
        }

        private void OnOwnPlanet()
        {
            Refresh();
        }

        [PublicAPI]
        public void OnClick()
        {
            if (!InGameControlHub.My.WorldMapController.IsUnlocked())
            {
                // 메시지
                MessagePopup.Show("info_300");

                return;
            }

            WorldMapPopup.Show();
        }

        private void Refresh()
        {
            // 행성조건 달성 여부에 따라 회색 메테리얼 적용 
            var isUnlocked = InGameControlHub.My.WorldMapController.IsUnlocked();

            imageMaterial.Init(isUnlocked);
        }
    }
}