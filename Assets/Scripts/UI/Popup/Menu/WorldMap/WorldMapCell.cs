﻿using EnhancedUI.EnhancedScroller;
using InGame.Controller;
using UnityEngine;

namespace UI.Popup.Menu.WorldMap
{
    public class WorldMapCell : EnhancedScrollerCellView
    {
        [SerializeField] private WorldPlanetSlot slot1;
        [SerializeField] private WorldPlanetSlot slot2;
        
        [SerializeField] private GameObject lineToSecondPlanet;
        [SerializeField] private GameObject lineToNextCell;
        
        public void InitOnePlanetOnly(string planetIdx)
        {
            slot1.Init(planetIdx);
                
            slot1.gameObject.SetActive(true);
            slot2.gameObject.SetActive(false);

            lineToSecondPlanet.SetActive(false);
            lineToNextCell.SetActive(false);
        }

        public void InitTwoPlanets(string firstPlanetIdx, string secondPlanetIdx)
        {
            slot1.Init(firstPlanetIdx);
            slot2.Init(secondPlanetIdx);

            slot1.gameObject.SetActive(true);
            slot2.gameObject.SetActive(true);

            lineToSecondPlanet.SetActive(true);
            lineToNextCell.SetActive(true);
        }

        public void InitTwoPlanetsLast(string firstPlanetIdx, string secondPlanetIdx)
        {
            InitTwoPlanets(firstPlanetIdx, secondPlanetIdx);

            lineToNextCell.SetActive(false);
        }

        public override void RefreshCellView()
        {
            slot1.Refresh();
            slot2.Refresh();
        }

        public Vector3 GetPlanetPosition(int index)
        {
            return index == 0 ? slot1.transform.position : slot2.transform.position;
        }
    }
}