﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Effect;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup.Plunder;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.WorldMap
{
    [RequireComponent(typeof(CommonScrollerOwner))]
    [RequireComponent(typeof(UIPopup))]
    public class WorldMapPopup : MonoBehaviour, IEnhancedScrollerDelegate
    {
        private const int NumberOfPlanetPerCell = 2;
        [SerializeField] private CharacterSlot character;

        [SerializeField] private Resources resources;

        [SerializeField] private TextMeshProUGUI gasLevelText;
        [SerializeField] private TextMeshProUGUI stoneLevelText;
        
        [SerializeField] private ResourceText gasText;
        [SerializeField] private ResourceText stoneText;
        [SerializeField] private ResourceText uraniumText;

        [SerializeField] private TextMeshProUGUI gasTimeEarningText;
        [SerializeField] private TextMeshProUGUI stoneTimeEarningText;
        [SerializeField] private TextMeshProUGUI uraniumTimeEarningText;
        
        [Header("약탈된 가스량"), SerializeField] private TextMeshProUGUI plunderedGasText;

        [Header("이펙트 갯수"), SerializeField] private int effectCount = 10;
        
        [Header("이펙트 날아가는 시간")]
        [SerializeField] private float effectFlyingDuration;

        [Header("한 개 스폰될 때마다 걸리는 시간")]
        [SerializeField] private float effectSpawnIntervalSec = 0.1f;

        [SerializeField] private ScrollRect scrollerCharacter;

        private CoroutineHandle _waitAndResume;
        private CoroutineHandle _applyLog;
        private CommonScrollerOwner _scroll;
        private bool _isInit;
        private ICollection<string> _allPlanetIdxes;

        public static void Show()
        {
            UIPopup.GetPopup("WorldMap").Show();
        }

        private void Awake()
        {
            _scroll = GetComponent<CommonScrollerOwner>();
        }

        [PublicAPI]
        public void Init()
        {
            // BGM
            SoundController.Instance.PlayBGM("bgm_worldmap");

            // 스크롤러 초기화 
            if (!_isInit)
            {
                _allPlanetIdxes = DataboxController.GetAllIdxes(Table.WorldMap, Sheet.planet);

                _scroll.scroller.Delegate = this;
                _scroll.scroller.ReloadData();
                
                _isInit = true;
            }
            
            // 스크롤러 세팅 기다림  
            Timing.CallDelayed(0f, () =>
            {
                // 현재 위치한 행성을 가운데로 위치시킴 
                var currentPlanetIndex = InGameControlHub.My.StageController.GetCurrentPlanetIndex();
                var cellIndex = (currentPlanetIndex - 1) / NumberOfPlanetPerCell;
                
                // 현재 행성 셀을 스크롤러 가장 오른쪽에 붙이기 
                // 0.5, 0.5로 두면, 마지막 행성일 때 중앙에 위치할 수 없는 셀이라 강제 스크롤링 됨 
                // 이때 캐릭터 위치가 약간 어긋나게 배치됨 
                // 강제 스크롤링 되지 않게 하기 위해, 현재 행성 셀을 맨 오른쪽에 붙임
                // (근데 요러면 지구 행성일 때 반대로 스크롤 밀림 현상이 발생해야 하는데, 지구는 강제 스크롤링이 발생하지 않는다...뭐지)
                _scroll.scroller.JumpToDataIndex(cellIndex, 1.0f, 1.0f);

                // HACK. 스크롤러 위에 캐릭터를 위치시키기 위한 엄청난 해킹 
                // EnhancedScroller 그대로 카피하는 캐릭터 전용 스크롤러 세팅 
                scrollerCharacter.content.anchorMin = _scroll.scroller.Container.anchorMin;
                scrollerCharacter.content.anchorMax = _scroll.scroller.Container.anchorMax;
                scrollerCharacter.content.pivot = _scroll.scroller.Container.pivot;
                
                scrollerCharacter.content.offsetMax = _scroll.scroller.Container.offsetMax;
                scrollerCharacter.content.offsetMin = _scroll.scroller.Container.offsetMin;
                scrollerCharacter.content.localPosition = _scroll.scroller.Container.localPosition;
                scrollerCharacter.content.localRotation = _scroll.scroller.Container.localRotation;
                scrollerCharacter.content.localScale = _scroll.scroller.Container.localScale;

                // 한 번 더 딜레이 
                // 바로 하면, GetCellViewAtDataIndex()에서 null 넘어오네; 
                Timing.CallDelayed(0f, () =>
                {
                    // 캐릭터 코스튬 장착 
                    character.WearMyCostume();
                    
                    /*
                     * 행성 인덱스, 셀 인덱스 헷갈림 주의
                     * 
                     * 행성 인덱스 
                     * 1       3       5
                     *     2       4       6
                     *
                     * 셀 인덱스
                     *   0       1       2
                     */
                    
                    // 현재 위치해야 하는 셀 찾기 
                    var currCell = _scroll.scroller.GetCellViewAtDataIndex(cellIndex) as WorldMapCell;
                    if (currCell == null) return;
                    
                    // 행성 셀의 몇 번 째 행성 자리인가 
                    var slotIndex = (currentPlanetIndex - 1) % NumberOfPlanetPerCell;
                    
                    // 현재 행성이 지구 or 반복모드 일 시,  
                    if (currentPlanetIndex == 1 || UserData.My.Stage.IsRepeatMode)
                    {
                        // 해당 행성 슬롯 위치 고정
                        character.transform.position = currCell.GetPlanetPosition(slotIndex);
                    }
                    // 그외라면
                    else
                    {
                        // 현재 행성의 스테이지 클리어 양 계산  
                        InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var currentStage, out var currentPlanetIdx);
                        var firstStage = InGameControlHub.My.StageController.GetFirstStage(currentPlanetIdx);
                        var stageForPlanet = currentStage - firstStage;

                        StageControl.GetStageMinMaxData(currentPlanetIdx, out var stageMin, out var stageMax);
                        var stageAmountForPlanet = stageMax - stageMin;
                        var ratio = (float)stageForPlanet / (float)stageAmountForPlanet;
                        
                        // 첫 번째 행성 슬롯 자리라면,
                        if (slotIndex == 0)
                        {
                            // 이전 행성 셀의 두 번째 행성 ~ 현재 행성 셀의 첫 번째 행성
                            // 위 경로의 캐릭터 위치 
                            var prevCellIndex = cellIndex - 1;
                            var prevCell = _scroll.scroller.GetCellViewAtDataIndex(prevCellIndex) as WorldMapCell;
                            if (prevCell == null) return;
                            
                            var prevPos = prevCell.GetPlanetPosition(1);
                            var currPos = currCell.GetPlanetPosition(slotIndex);
                            var stagePos = Vector2.Lerp(prevPos, currPos, ratio);
                            character.transform.position = stagePos;
                        }
                        // 두 번째 행성 슬롯 자리라면,
                        else if (slotIndex == 1)
                        {
                            // 첫 번째 ~ 두 번째 슬롯 경로에 캐릭터 위치  
                            var prevPos = currCell.GetPlanetPosition(0);
                            var currPos = currCell.GetPlanetPosition(1);
                            var stagePos = Vector2.Lerp(prevPos, currPos, ratio);
                            character.transform.position = stagePos;
                        }
                    }
                });
            });
            
            // 이벤트 등록
            UserData.My.WorldMap.OnLevelUpGasFactory += OnLevelUp;
            UserData.My.WorldMap.OnLevelUpStoneFactory += OnLevelUp;
            
            // 새롭게 약탈 당한 기록이 있는가  
            _applyLog = Timing.RunCoroutine(ApplyPlunderLogs());

            // 공통 갱신 (약탈 반영 후에 해줘야 함)
            Refresh();
            
            // 수동 GC
            GCController.CollectFull();
        }

        private IEnumerator<float> ApplyPlunderLogs()
        {
            if (!InGameControlHub.My.PlunderController.HasNewLog()) yield break;

            // 월드맵 채굴량 계산과 약탈 계산 반영을 서로 독립적이게 유지하기 위한 발버둥 코드 
            // 새로운 약탈 로그가 생기면, 이전까지 당했던 약탈량을 차감하고 난 후의 값을 사용함 
            // 유저가 '채굴' 버튼을 누르기 전까진, 누적된 약탈량을 유지하기 위한 복잡한 코드임 
            var collectedGasUntilNow = InGameControlHub.My.WorldMapController.GetGasMiningSumUntilNow();
            var plunderedGasUntilNow = InGameControlHub.My.Data.Plunder.PlunderedGasSum;
            var remainingGasPrev = collectedGasUntilNow - plunderedGasUntilNow;

            // 안에서 PlunderedGasSum는 새롭게 갱신됨
            LoadingIndicator.Show();

            var plunderedGasThisTime = 0d;
            var remainingGasCurrent = 0d;
            yield return Timing.WaitUntilDone(InGameControlHub.My.PlunderController._ApplyLogs(remainingGasPrev, (plunderedGas, remainingGas) =>
            {
                plunderedGasThisTime = plunderedGas;
                remainingGasCurrent = remainingGas;
            }));

            LoadingIndicator.Hide();
            
            // 있다면 약탈 결과 팝업 보여주기 
            var popup = PlunderedResultPopup.Show();
            popup.Init(remainingGasPrev, plunderedGasThisTime, remainingGasCurrent);
            
            // 갱신
            Refresh();
        }

        [PublicAPI]
        public void OnHide()
        {
            // BGM
            SoundController.Instance.RollbackBGM();
            
            // 이벤트 해제 
            UserData.My.WorldMap.OnLevelUpGasFactory -= OnLevelUp;
            UserData.My.WorldMap.OnLevelUpStoneFactory -= OnLevelUp;
            
            // 코루틴 해제 
            Timing.KillCoroutines(_applyLog);
            
            // 수동 GC
            GCController.CollectFull();
        }

        private void OnLevelUp()
        {
            Refresh();
        }

        private void Refresh()
        {
            // 가스, 지옥석 팩토리 레벨 
            UserData.My.WorldMap.GetState(out var gasLevel, out var stoneLevel);
            gasLevelText.text = Localization.GetFormatText("info_145", gasLevel.ToLookUpString());
            stoneLevelText.text = Localization.GetFormatText("info_145", stoneLevel.ToLookUpString());
            
            // 시간 당 채굴량  
            var miningPerHour = InGameControlHub.My.WorldMapController.GetMiningAmountsPerHour();
            gasTimeEarningText.text = Localization.GetFormatText("info_094", miningPerHour["REWARD_GAS"].ToUnitString());
            stoneTimeEarningText.text = Localization.GetFormatText("info_094", miningPerHour["REWARD_STONE"].ToUnitString());
            uraniumTimeEarningText.text = Localization.GetFormatText("info_094", Math.Floor(miningPerHour["REWARD_GEM"]).ToString("0"));
            
            RefreshMiningUntilNowText();
        }
        
        private void RefreshMiningUntilNowText()
        {
            // 현재까지의 채굴량
            var miningAmountsUntilNow = InGameControlHub.My.WorldMapController.GetMiningSumUntilNow();
            
            // 가스 
            {
                // 약탈 반영 (위에 Init에서 갱신 반영된 PlunderedGasSum을 바로 사용함)
                var collectedGas = miningAmountsUntilNow["REWARD_GAS"];
                var plunderedGasSum = InGameControlHub.My.Data.Plunder.PlunderedGasSum;
                var finalizedGas = collectedGas - plunderedGasSum;
                
                gasText.Text.text = finalizedGas.ToUnitString();
                
                // 약탈당한 가스량 
                if (InGameControlHub.My.PlunderController.CanProtect())
                {
                    // 상품 구매 시, 방어도만 표시 
                    plunderedGasText.text = InGameControlHub.My.PlunderController.GetProtectText();
                    plunderedGasText.color = Color.green;
                }
                else
                {
                    // 약탈당한 가스량 표기 
                    plunderedGasText.text = Localization.GetFormatText("info_183", plunderedGasSum.ToUnitString());
                    plunderedGasText.color = Color.red;
                }
            }
            // 지옥석
            stoneText.Text.text = miningAmountsUntilNow["REWARD_STONE"].ToUnitString();
            // 우라늄
            uraniumText.Text.text = Math.Floor(miningAmountsUntilNow["REWARD_GEM"]).ToString("0");
        }

        [PublicAPI]
        public void OpenGasFactoryLevelUpPopup()
        {
            GasFactoryLevelUpPopup.Show();
        }
        
        [PublicAPI]
        public void OpenStoneFactoryLevelUpPopup()
        {
            StoneFactoryLevelUpPopup.Show();
        }

        [PublicAPI]
        public void Collect()
        {
            // 수집되는 자원양 기억
            var untilNow = InGameControlHub.My.WorldMapController.GetMiningSumUntilNow();
				
            // 획득될 자원이 전부 1 이상이 아닐 시, 취소 
            var cancel = true;
            foreach (var amount in untilNow)
            {
                if (amount.Value >= 1) // 한개라도 1 이상이면, 취소 안함 
                {
                    cancel = false;
                    break;
                }
            }
				
            if (cancel)
                return;
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_worldmap_get");

            // 채굴 전, 리소스 멈춤 
            resources.Pause();
				
            // 채굴 전, 현재 리소스를 기억해둠 
            var prevGas = UserData.My.Resources.gas;
            var prevStone = UserData.My.Resources.stone;
            var prevUranium = UserData.My.Resources.uranium;

            // 채굴
            InGameControlHub.My.WorldMapController.Mining();

            // 채굴 연출
            foreach (var amount in untilNow)
            {
                var type = amount.Key;
                var value = untilNow[type];
                
                // 데이터 상 1 이하의 숫자도 처리하지만, UI 상 1 미만은 0으로 보임
                // 그래서 1미만은 이펙트 연출 보여주지 않음 
                if (value < 1d)
                    continue;
                
                switch (type)
                {
                    case RewardType.REWARD_GAS:
                    {
                        Timing.RunCoroutine(_PlayEffect(type, gasText.Icon.transform.position, resources.gas.Icon.transform.position,
                            () => { gasText.Text.DOTextDouble(value, 0, effectFlyingDuration, d => d.ToUnitString()); }, 
                            () => { resources.gas.Text.DOTextDouble(prevGas, UserData.My.Resources.gas, effectFlyingDuration, d => d.ToUnitString()); })
                            .CancelWith(gameObject));
                    }
                        break;
                    case RewardType.REWARD_STONE:
                    {
                        Timing.RunCoroutine(_PlayEffect(type, stoneText.Icon.transform.position, resources.stone.Icon.transform.position,
                                () => { stoneText.Text.DOTextDouble(value, 0, effectFlyingDuration, d => d.ToUnitString()); }, 
                                () => { resources.stone.Text.DOTextDouble(prevStone, UserData.My.Resources.stone, effectFlyingDuration, d => d.ToUnitString()); })
                            .CancelWith(gameObject));
                    }
                        break;
                    case RewardType.REWARD_GEM:
                    {
                        uraniumText.Text.DOTextDouble(value, 0, effectFlyingDuration, d => d.ToUnitString());
                        
                        Timing.RunCoroutine(_PlayEffect(type, uraniumText.Icon.transform.position, resources.uranium.Icon.transform.position,
                                () => { uraniumText.Text.DOTextDouble(value, 0, effectFlyingDuration, d => Math.Floor(d).ToString("0")); }, 
                                () => { resources.uranium.Text.DOTextDouble(prevUranium, UserData.My.Resources.uranium, effectFlyingDuration, d => Math.Floor(d).ToString("0")); })
                            .CancelWith(gameObject));
                    }
                        break;
                }
            }

            // 기다린 후, 리소스 갱신 재개 (여유 있게 연출 시간보다 2배 더 기다린 후, 재개시킴)
            Timing.KillCoroutines(_waitAndResume);
            _waitAndResume = Timing.RunCoroutine(_WaitAndResumeResources(effectFlyingDuration * 2f).CancelWith(gameObject));
        }

        private IEnumerator<float> _PlayEffect(string type, Vector2 startPos, Vector2 endPos, Action startCallback, Action endCallback)
        {
            for (var i = 0; i < effectCount; i++)
            {
                // 맨 처음 이펙트만 콜백 달음. 전부 다 달면, 쓸데없이 글자 버벅거림 
                FlyingResourceEffect.Spawn(type, effectFlyingDuration, startPos, endPos, transform.parent, i == 0 ? startCallback : null, i == 0 ? endCallback : null);

                yield return Timing.WaitForSeconds(effectSpawnIntervalSec);
            }
        }

        private IEnumerator<float> _WaitAndResumeResources(float time)
        {
            yield return Timing.WaitForSeconds(time);
            
            // 리소스 이벤트 재개 
            resources.Resume();
                        
            // 약탈 당한 양 리셋 위해 갱신
            RefreshMiningUntilNowText();
        }

        /*
         * Scroller
         */
        #region Scroller

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            var cellCount = _allPlanetIdxes.Count / NumberOfPlanetPerCell;
            cellCount += _allPlanetIdxes.Count % NumberOfPlanetPerCell == 0 ? 0 : 1; // 나머지가 있다면 한 셀 더 추가 
            return cellCount;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return _scroll.cellSize;
        }
        
        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            var cell = _scroll.scroller.GetCellView(_scroll.cellPrefab) as WorldMapCell;
            if (cell == null) return null;

            var slot1Index = dataIndex * 2;
            var slot2Index = dataIndex * 2 + 1;
				
            // 마지막 슬롯 (행성 두개)
            if (slot2Index == _allPlanetIdxes.Count - 1)
            {
                var planet1Idx = _allPlanetIdxes.ElementAt(slot1Index);
                var planet2Idx = _allPlanetIdxes.ElementAt(slot2Index);
                cell.InitTwoPlanetsLast(planet1Idx, planet2Idx);
            }
            // 처음~중간 슬롯 
            else if (slot2Index < _allPlanetIdxes.Count)
            {
                var planet1Idx = _allPlanetIdxes.ElementAt(slot1Index);
                var planet2Idx = _allPlanetIdxes.ElementAt(slot2Index);
                cell.InitTwoPlanets(planet1Idx, planet2Idx);
            }
            // 마지막 슬롯 (행성 하나)
            else if (slot2Index >= _allPlanetIdxes.Count)
            {
                var planet1Idx = _allPlanetIdxes.ElementAt(slot1Index);
                cell.InitOnePlanetOnly(planet1Idx);
            }
					
            return cell;
        }

        #endregion
    }
}