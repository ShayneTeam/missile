﻿using System.Collections.Generic;
using Databox.Dictionary;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UI.Popup.Menu.Soldier;
using UnityEngine;

namespace UI.Popup.Menu.Relic
{
    public abstract class AbilitiesScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] protected EnhancedScroller scroller;
        [SerializeField] protected EnhancedScrollerCellView cellPrefab;
        [SerializeField] protected float cellSize;

        private OrderedDictionary<string, float> _allAppliedAbilities;

        public void Init()
        {
            _allAppliedAbilities = GetAllAppliedAbilities();

            // 스크롤러 초기화 
            scroller.Delegate = this;
            scroller.ReloadData(scroller.NormalizedScrollPosition);
        }

        public void Refresh()
        {
            // 적용된 모든 용병 어빌리티의 총합 가져옴 
            var prevAbilities = _allAppliedAbilities;
            var currAbilities = GetAllAppliedAbilities();
            _allAppliedAbilities = currAbilities;
            
            // 이전 어빌리티 목록에서 구성이 바뀔 때만 Reload (부하가 꽤 심하기 때문)
            if (prevAbilities.Count != currAbilities.Count)
                scroller.ReloadData(scroller.NormalizedScrollPosition);
            else
                scroller.RefreshActiveCellViews();
        }

        protected abstract OrderedDictionary<string, float> GetAllAppliedAbilities();
        
        public int GetNumberOfCells(EnhancedScroller _)
        {
            // 총 용병 적용 어빌리티 갯수
            return _allAppliedAbilities.Count;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellPrefab) as CommonAbilityCellView;
            if (cellView == null)
                return null;

            cellView.GetTypeAndValue = GetTypeAndValue;
            cellView.Init(dataIndex);
            
            return cellView;
        }

        private void GetTypeAndValue(int dataIndex, out string type, out float value)
        {
            type = _allAppliedAbilities.KeyOf(dataIndex);
            value = _allAppliedAbilities[dataIndex];
        }
    }
}