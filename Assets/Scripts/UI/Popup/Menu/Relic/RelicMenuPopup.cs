﻿using System;
using Firebase.Analytics;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using UI.Popup.Menu.Relic.Diablo;
using UI.Popup.Menu.Relic.Normal;
using UnityEngine;

namespace UI.Popup.Menu.Relic
{
    public class RelicMenuPopup : MonoBehaviour
    {
        [SerializeField] private RelicGallery galley;
        [SerializeField] private NormalRelicScroll relicScroll;
        
        [SerializeField] private Diablo.DiabloLab lab;
        [SerializeField] private DiabloRelicScroll diabloRelicScroll;

        [SerializeField] private NormalRelicAbilities normalRelicAbilities;
        [SerializeField] private DiabloRelicAbilities diabloRelicAbilities;

        [SerializeField] private GameObject galleryBottom;
        [SerializeField] private GameObject labBottom;

        /*
         * Init이 호출되기 전에 오브젝트는 꺼지면 안됨 
         * UIToggle의 OnEnable 타이밍에 스크롤러가 꺼지면, 스크롤러 Awake가 호출 안되면서 초기화 시 에러 발생함  
         */
        [HideInInspector] public bool _isInit;

        private void Awake()
        {
            // 초기화 위해 다 켜두기 
            /*
             * 팝업 ShowStart에서 Init 후에 ShowRelicGallery를 호출하여 디아블로는 꺼짐 
             */
            galley.gameObject.SetActive(true);
            lab.gameObject.SetActive(true);
            
            relicScroll.gameObject.SetActive(true);
            diabloRelicScroll.gameObject.SetActive(true);
            
            normalRelicAbilities.gameObject.SetActive(true);
            diabloRelicAbilities.gameObject.SetActive(true);
        }

        [PublicAPI]
        public void OnShow()
        {
            RegisterEvents();

            if (!_isInit)
            {
                Init();

                _isInit = true;
                
                ShowRelicGallery();
            }
            else
            {
                Refresh();
            }
        }

        [PublicAPI]
        public void OnHide()
        {
            UnregisterEvents();
        }

        private void OnDestroy()
        {
            // EnterPlayMode용 
            // UI 켜진 상태로 플레이 종료 시, OnHide는 호출 안되므로 이벤트 해제 안됨
            UnregisterEvents();
        }

        private void RegisterEvents()
        {
            UserData.My.NormalRelic.OnEarned += OnEarnedNormalRelic;
            UserData.My.NormalRelic.OnLevelUp += OnLevelUpNormalRelic;

            InGameControlHub.My.DiabloRelicController.OnGacha += OnGachaDiabloRelic;
            InGameControlHub.My.DiabloRelicController.OnExchange += OnExchangeDiabloRelic;
            UserData.My.DiabloRelic.OnLevelUp += OnLevelUpDiabloRelic;
        }

        private void UnregisterEvents()
        {
            UserData.My.NormalRelic.OnEarned -= OnEarnedNormalRelic;
            UserData.My.NormalRelic.OnLevelUp -= OnLevelUpNormalRelic;

            InGameControlHub.My.DiabloRelicController.OnGacha -= OnGachaDiabloRelic;
            InGameControlHub.My.DiabloRelicController.OnExchange -= OnExchangeDiabloRelic;
            UserData.My.DiabloRelic.OnLevelUp -= OnLevelUpDiabloRelic;
        }

        public void Init()
        {
            galley.Init();
            lab.Init();

            relicScroll.Init();
            diabloRelicScroll.Init();

            normalRelicAbilities.Init();
            diabloRelicAbilities.Init();
            
            WebLog.Instance.ThirdPartyLog("relic_menu_open");
        }

        private void Refresh()
        {
            galley.Refresh();
            lab.Refresh();
            
            relicScroll.Refresh();
            diabloRelicScroll.Refresh();;
            
            normalRelicAbilities.Refresh();
            diabloRelicAbilities.Refresh();
        
            WebLog.Instance.ThirdPartyLog("relic_menu_open");
        }
        
        private void OnEarnedNormalRelic(string relicIdx)
        {
            // 팝업 
            EarnedNewRelicPopup.Show(relicIdx);
            
            // 이때 스크롤을 다시 리로드해야 하므로 Init 호출 해줌 
            Init();
        }

        private void OnGachaDiabloRelic(int count)
        {
            Init();
        }
        
        private void OnExchangeDiabloRelic()
        {
            Init();
        }

        private void OnLevelUpDiabloRelic(string relicIdx)
        {
            Refresh();
        }
        
        private void OnLevelUpNormalRelic(string relicIdx, int count)
        {
            Refresh();
        }

        [PublicAPI]
        public void ShowRelicGallery()
        {
            if (!_isInit)
                return;
            
            SwitchRelicView(false);
        }

        [PublicAPI]
        public void ShowDiabloRelic()
        {
            if (!_isInit)
                return;

            SwitchRelicView(true);
        }

        private void SwitchRelicView(bool showDiablo)
        {
            galley.gameObject.SetActive(!showDiablo);
            lab.gameObject.SetActive(showDiablo);
            
            galleryBottom.SetActive(!showDiablo);
            labBottom.SetActive(showDiablo);
            
            relicScroll.gameObject.SetActive(!showDiablo);
            diabloRelicScroll.gameObject.SetActive(showDiablo);
            
            normalRelicAbilities.gameObject.SetActive(!showDiablo);
            diabloRelicAbilities.gameObject.SetActive(showDiablo);
            
            /*
             * 셀 업데이트를 위해 볼트 이벤트를 다음 프레임으로 넘기는 꼼수를 써도
             * 부모가 꺼져있으면 다음 프레임에도 이벤트 호출되지 않음
             * 그래서 이렇게 뷰가 스위치됐을 때, 갱신해줘야 한다  
             */
            Refresh();
        }
    }
}