﻿using Global;
using InGame;
using InGame.Data;
using InGame.Global;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Relic.Normal
{
    public class RelicSlot : MonoBehaviour
    {
        [SerializeField] private GameObject on;
        [SerializeField] private Image icon;

        public void Init(string relicIdx)
        {
            // 아이콘 
            if (!string.IsNullOrEmpty(relicIdx) && UserData.My.NormalRelic.IsEarned(relicIdx))
            {
                on.gameObject.SetActive(true);
                
                var iconPath = DataboxController.GetDataString(Table.Relic, Sheet.Relic, relicIdx, Column.icon);
                var iconSprite = AtlasController.GetSprite(iconPath);

                icon.sprite = iconSprite;
            }
            else
            {
                on.gameObject.SetActive(false);
            }
        }
    }
}