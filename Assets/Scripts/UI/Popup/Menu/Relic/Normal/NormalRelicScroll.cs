﻿using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using InGame.Data;
using UnityEngine;

namespace UI.Popup.Menu.Relic.Normal
{
    public class NormalRelicScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller scroller;
        [SerializeField] private EnhancedScrollerCellView cellPrefab;
        [SerializeField] private float cellSize;

        [SerializeField] private GameObject backText;
        
        private IEnumerable<string> _allEarnedRelics;
        
        public void Init()
        {
            _allEarnedRelics = UserData.My.NormalRelic.GetAllEarned();
            
            backText.SetActive(!_allEarnedRelics.Any());
            
            scroller.Delegate = this;
            scroller.ReloadData(scroller.NormalizedScrollPosition);
        }

        public void Refresh()
        {
            _allEarnedRelics = UserData.My.NormalRelic.GetAllEarned();
            
            backText.SetActive(!_allEarnedRelics.Any());
            
            scroller.RefreshActiveCellViews();
        }
        
        public int GetNumberOfCells(EnhancedScroller _)
        {
            return _allEarnedRelics.Count();
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellPrefab) as NormalRelicUpgradeSlot;
            if (cellView == null)
                return null;

            cellView.Init(_allEarnedRelics.ElementAt(dataIndex));
            
            return cellView;
        }
    }
}