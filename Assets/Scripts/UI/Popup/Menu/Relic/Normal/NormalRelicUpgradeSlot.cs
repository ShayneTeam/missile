﻿using System;
using System.Collections.Generic;
using Bolt;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Relic.Normal
{
    public class NormalRelicUpgradeSlot : EnhancedScrollerCellView
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI descText;
        [SerializeField] private TextMeshProUGUI abilityText;
        
        [SerializeField] private List<RelicUpgradeButton> buttons;
        
        private string _relicIdx;

        private void Start()
        {
	        UserData.My.NormalRelic.OnLevelUp += ShowEffect;
        }

        private void OnDestroy()
        {
	        UserData.My.NormalRelic.OnLevelUp -= ShowEffect;
        }

        private void ShowEffect(string relicIdx, int count)
        {
	        if (_relicIdx != relicIdx)
		        return;
	        
	        var canvas = UICanvas.GetUICanvas("UI Effect");
	        EffectSpawner.Instance.Spawn("enhance UI effect (small)", icon.transform.position, canvas.transform);
        }

        public void Init(string relicIdx)
        {
	        _relicIdx = relicIdx;
	        
	        // 버튼
	        foreach (var button in buttons) button.Init(relicIdx);

	        Refresh();
        }

        private void Refresh()
        {
	        UserData.My.NormalRelic.GetState(_relicIdx, out var level);
	        
	        // 레벨 
	        levelText.text = Localization.GetFormatText("info_145", level.ToLookUpString());

	        // 이름
	        nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Relic, Sheet.Relic, _relicIdx, Column.name));
	        
	        // 아이콘 
	        icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Relic, Sheet.Relic, _relicIdx, Column.icon));
	        
	        // 설명 
	        descText.text = Localization.GetText(DataboxController.GetDataString(Table.Relic, Sheet.Relic, _relicIdx, Column.desc));
	        
	        // 어빌리티 
	        var abilityIdx = DataboxController.GetDataString(Table.Relic, Sheet.Relic, _relicIdx, "r_aidx");
	        
	        var abilityNameText = AbilityControl.GetNameText(abilityIdx);
	        
	        var abilityValue = InGameControlHub.My.NormalRelicController.GetFinalizedAbilityValue(_relicIdx);
	        var abilityDescText = AbilityControl.GetDescText(abilityIdx, abilityValue);

	        abilityText.text = abilityNameText + abilityDescText;

	        // 최대 레벨 체크 
	        var isMaxLevel = InGameControlHub.My.NormalRelicController.IsMaxLevel(_relicIdx);
	         
	        // 버튼
	        var visibleButton = !isMaxLevel;
	        foreach (var button in buttons)
	        {
		        button.gameObject.SetActive(visibleButton);
		        
		        if (visibleButton) button.Refresh();
	        }
        }

	    public override void RefreshCellView()
	    {
		    Refresh();
	    }
    }
}