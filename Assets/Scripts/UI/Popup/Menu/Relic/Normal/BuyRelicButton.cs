﻿using InGame.Controller;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Relic.Normal
{
    public class BuyRelicButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI uraniumText;
        
        
        
        public void Init()
        {
            uraniumText.text = InGameControlHub.My.NormalRelicController.GetNeedUraniumForGacha().ToLookUpString();
            uraniumText.color = InGameControlHub.My.NormalRelicController.IsEnoughUraniumForGacha() ? Color.white : Color.red;
        }

        public void OnClick()
        {
            if (!InGameControlHub.My.NormalRelicController.IsEnoughUraniumForGacha())
            {
                MessagePopup.Show("info_162");
                return;
            }
            
            InGameControlHub.My.NormalRelicController.Gacha();
        }
    }
}
