﻿using System;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using Utils;

namespace UI.Popup.Menu.Relic.Normal
{
    [RequireComponent(typeof(ButtonSpriteSwapper))]
    public class RelicUpgradeButton : MonoBehaviour
    {
        [SerializeField] private GameObject button;
        [SerializeField] private TextMeshProUGUI buttonText;
        [SerializeField] private int upgradeCount;
        
        private string _relicIdx;
        
        private ButtonSpriteSwapper _buttonSpriteSwapper;


        private void Start()
        {
            UserData.My.Resources.OnChangedStone += OnChangedStone;
        }

        private void OnDestroy()
        {
            UserData.My.Resources.OnChangedStone -= OnChangedStone;
        }

        private void OnChangedStone(double value)
        {
            RefreshButtonBg();
        }

        public void Init(string relicIdx)
        {
            _relicIdx = relicIdx;

            Refresh();
        }

        public void Refresh()
        {
            // 최대 레벨 체크 
            var isMaxLevel = InGameControlHub.My.NormalRelicController.IsMaxLevel(_relicIdx);
            var isEarned = UserData.My.NormalRelic.IsEarned(_relicIdx);

            var visibleButton = !isMaxLevel && isEarned;
            if (visibleButton)
            {
                // 버튼 슬롯 
                RefreshButtonBg();

                // 버튼 텍스트 
                var requiredStone = (double)InGameControlHub.My.NormalRelicController.GetRequiredStoneForLevelUp(_relicIdx, upgradeCount);
                buttonText.text = requiredStone.ToUnitString();
            }
        }

        private void RefreshButtonBg()
        {
            var canLevelUp = InGameControlHub.My.NormalRelicController.CanLevelUp(_relicIdx, upgradeCount);
            _buttonSpriteSwapper ??= button.GetComponent<ButtonSpriteSwapper>();
            _buttonSpriteSwapper.Init(canLevelUp);
        }

        [PublicAPI]
        public void LevelUp()
        {
            if (!InGameControlHub.My.NormalRelicController.CanLevelUp(_relicIdx, upgradeCount)) return;
		    
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_upgrade_02");
		    
            // 업그레이드 
            InGameControlHub.My.NormalRelicController.LevelUp(_relicIdx, upgradeCount);
        }
    }
}