﻿using Databox.Dictionary;
using InGame.Controller;

namespace UI.Popup.Menu.Relic.Normal
{
    public class NormalRelicAbilities : AbilitiesScroll
    {
        protected override OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
            return InGameControlHub.My.NormalRelicController.GetAllAppliedAbilities();
        }
    }
}