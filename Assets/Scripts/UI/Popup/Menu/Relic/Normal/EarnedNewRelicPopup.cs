﻿using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Relic.Normal
{
    [RequireComponent(typeof(UIPopup))]
    public class EarnedNewRelicPopup : MonoBehaviour
    {
        public static void Show(string relicIdx)
        {
            var popup = UIPopup.GetPopup("Earned New Relic");

            var relicPopup = popup.GetComponent<EarnedNewRelicPopup>();
            relicPopup.Init(relicIdx);
            
            popup.Show();
        }

        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI abilityNameText;
        [SerializeField] private TextMeshProUGUI abilityDescText;

        private void Init(string relicIdx)
        {
            // 아이콘 
            icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Relic, Sheet.Relic, relicIdx, Column.icon));

            // 이름
            nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Relic, Sheet.Relic, relicIdx, Column.name));

            // 어빌리티  
            var abilityIdx = DataboxController.GetDataString(Table.Relic, Sheet.Relic, relicIdx, "r_aidx");
            
            abilityNameText.text = AbilityControl.GetNameText(abilityIdx);
	        
            var abilityValue = InGameControlHub.My.NormalRelicController.GetFinalizedAbilityValue(relicIdx);
            abilityDescText.text = AbilityControl.GetDescText(abilityIdx, abilityValue);
        }
    }
}