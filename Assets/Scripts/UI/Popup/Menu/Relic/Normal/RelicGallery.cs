﻿using System.Collections.Generic;
using System.Linq;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Relic.Normal
{
    public class RelicGallery : MonoBehaviour
    {
        [SerializeField] private Transform grid;
        [SerializeField] private TextMeshProUGUI stateText;
        [SerializeField] private BuyRelicButton button;
        
        private ICollection<string> _allRelicIdxes;

        private RelicSlot[] _slots;

        public void Init()
        {
            _allRelicIdxes = DataboxController.GetAllIdxes(Table.Relic, Sheet.Relic);

            _slots = grid.GetComponentsInChildren<RelicSlot>();

            Refresh();
        }

        public void Refresh()
        {
            // 유물 갤러리
            for (var i = 0; i < _allRelicIdxes.Count; i++)
            {
                var relicIdx = _allRelicIdxes.ElementAt(i);
                
                _slots[i].Init(relicIdx);
            }
            
            // 현황 텍스트 
            var earnedCount = UserData.My.NormalRelic.GetAllEarned().Count();
            stateText.text = Localization.GetFormatText("info_158", earnedCount.ToLookUpString(), _allRelicIdxes.Count.ToLookUpString());
            
            // 유물 구매 버튼
            if (InGameControlHub.My.NormalRelicController.IsAllEarned())
            {
                // 유물 모두 획득 시, 버튼 숨김 
                button.gameObject.SetActive(false);
            }    
            else
            {
                button.gameObject.SetActive(true);
                button.Init();
            }
        }
    }
}