﻿using InGame.Controller;
using InGame.Data;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Relic.Diablo
{
    public class DiabloLab : MonoBehaviour
    {
        [SerializeField] private Transform parentOfSlots;
        [SerializeField] private TextMeshProUGUI stateText;

        [SerializeField] private DiabloLabUnlockSkill skill;
        [SerializeField] private GachaDiabloRelicButton gachaButton;
        [SerializeField] private ExchangeDiabloRelicButton exchangeButton;
        
        private DiabloRelicSlot[] _slots;


        public void Init()
        {
            Refresh();
        }

        public void Refresh()
        {
            // 디아블로 유물 슬롯
            _slots ??= parentOfSlots.GetComponentsInChildren<DiabloRelicSlot>();
            
            for (var i = 0; i < _slots.Length; i++)
            {
                var child = _slots[i];
                child.Init();
            }
            
            // 현황 텍스트 
            stateText.text = $"x{UserData.My.Resources.diablokey.ToLookUpString()}";
            
            // 언락 스킬 
            skill.Init();
            
            // 버튼
            var canGacha = InGameControlHub.My.DiabloRelicController.Gacha.CanGacha;
            var allMax = InGameControlHub.My.DiabloRelicController.HasAllMaxCount;
            var visibleGacha = canGacha || !allMax;
            
            gachaButton.gameObject.SetActive(visibleGacha);
            exchangeButton.gameObject.SetActive(!visibleGacha);
            
            // 가챠가 불가능해도, 올맥스가 아니라면 가챠 버튼을 보여 줌
            if (visibleGacha)
                gachaButton.Init();
            else
                exchangeButton.Init();
        }
    }
}