﻿using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Relic.Diablo
{
    public class DiabloLabUnlockSkill : MonoBehaviour
    {
        [SerializeField] private string skillIdx;
        
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI descText;
        [SerializeField] private TextMeshProUGUI levelText;
        
        [SerializeField] private GameObject lockParent;

        public void Init()
        {
            // 이름
            nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, Column.name));
	        
            // 설명 
            descText.text = SkillSlotT999.GetDescText(Localization.GetText(DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, Column.desc)));
            
            // 레벨 
            levelText.text = SkillSlotT999.LevelText();
            
            // 모든 유물을 획득했는가 
            lockParent.SetActive(!InGameControlHub.My.DiabloRelicController.HasAllCollected());
        }
    }
}