﻿using Databox.Dictionary;
using InGame.Controller;

namespace UI.Popup.Menu.Relic.Diablo
{
    public class DiabloRelicAbilities : AbilitiesScroll
    {
        protected override OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
            return InGameControlHub.My.DiabloRelicController.GetAllAppliedAbilities();
        }
    }
}