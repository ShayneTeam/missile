﻿using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Relic.Diablo
{
    [RequireComponent(typeof(UIPopup))]
    public class EarnedNewDiabloRelicPopup : MonoBehaviour
    {
        public static void Show(string dRelicIdx)
        {
            var popup = UIPopup.GetPopup("Earned New Diablo Relic");

            var relicPopup = popup.GetComponent<EarnedNewDiabloRelicPopup>();
            relicPopup.Init(dRelicIdx);
            
            popup.Show();
        }

        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI abilityNameText1;
        [SerializeField] private TextMeshProUGUI abilityDescText1;
        
        [SerializeField] private TextMeshProUGUI abilityNameText2;
        [SerializeField] private TextMeshProUGUI abilityDescText2;

        private void Init(string dRelicIdx)
        {
            // 아이콘 
            icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, dRelicIdx, Column.icon));

            // 이름
            nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, dRelicIdx, Column.name));

            // 어빌리티 1
            {
                var ability1Idx = DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, dRelicIdx, "d_aidx_1");

                // 어빌리티 1 이름
                abilityNameText1.text = AbilityControl.GetNameText(ability1Idx);
                
                var abilityValue = InGameControlHub.My.DiabloRelicController.GetFinalizedAbilityValue(dRelicIdx, 1);
                abilityDescText1.text = AbilityControl.GetDescText(ability1Idx, abilityValue);
            }
            
            // 어빌리티 2
            {
                var ability2Idx = DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, dRelicIdx, "d_aidx_2");

                // 어빌리티 2 이름
                abilityNameText2.text = AbilityControl.GetNameText(ability2Idx);
                
                var abilityValue = InGameControlHub.My.DiabloRelicController.GetFinalizedAbilityValue(dRelicIdx, 2);
                abilityDescText2.text = AbilityControl.GetDescText(ability2Idx, abilityValue);
            }
        }
    }
}