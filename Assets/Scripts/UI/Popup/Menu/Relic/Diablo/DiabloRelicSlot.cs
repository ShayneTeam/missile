﻿using Global;
using InGame;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Menu.Relic.Diablo
{
    public class DiabloRelicSlot : MonoBehaviour
    {
        [SerializeField] private string diabloRelicIdx;
        
        [SerializeField] private GameObject on;
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI levelText;

        public void Init()
        {
            // 아이콘 
            if (!string.IsNullOrEmpty(diabloRelicIdx) && UserData.My.DiabloRelic.IsEarned(diabloRelicIdx))
            {
                on.gameObject.SetActive(true);

                // 아이콘 
                icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, diabloRelicIdx, Column.icon));
                
                // 레벨
                UserData.My.DiabloRelic.GetState(diabloRelicIdx, out var level, out _);
                levelText.text = Localization.GetFormatText("info_145", level.ToLookUpString());
            }
            else
            {
                on.gameObject.SetActive(false);
            }
        }
    }
}