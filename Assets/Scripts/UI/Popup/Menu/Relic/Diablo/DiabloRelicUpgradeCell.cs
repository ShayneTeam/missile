﻿using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Menu.Relic.Diablo
{
    public class DiabloRelicUpgradeCell : EnhancedScrollerCellView
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI levelText;
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI ability1Text;
        [SerializeField] private TextMeshProUGUI ability2Text;

        [SerializeField] private GameObject button;
        [SerializeField] private Image buttonBg;
        [SerializeField] private Image buttonIcon;
        [SerializeField] private TextMeshProUGUI buttonText;
        
        private string _dRelicIdx;
        
        private ButtonSpriteSwapper _buttonSpriteSwapper;

        private void Awake()
        {
	        _buttonSpriteSwapper = button.GetComponent<ButtonSpriteSwapper>();
        }

        private void Start()
        {
	        UserData.My.DiabloRelic.OnLevelUp += ShowEffect;
        }

        private void OnDestroy()
        {
	        UserData.My.DiabloRelic.OnLevelUp -= ShowEffect;
        }

        private void ShowEffect(string dRelicIdx)
        {
	        if (_dRelicIdx != dRelicIdx)
		        return;
	        
	        var canvas = UICanvas.GetUICanvas("UI Effect");
	        EffectSpawner.Instance.Spawn("enhance UI effect (small)", icon.transform.position, canvas.transform);
        }

        public void Init(string dRelicIdx)
        {
	        _dRelicIdx = dRelicIdx;

	        Refresh();
        }

        private void Refresh()
        {
	        UserData.My.DiabloRelic.GetState(_dRelicIdx, out var level, out var count);
	        
	        // 레벨 
	        levelText.text = Localization.GetFormatText("info_145", level.ToLookUpString());

	        // 이름
	        nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, _dRelicIdx, Column.name));
	        
	        // 아이콘 
	        var sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, _dRelicIdx, Column.icon));
	        icon.sprite = sprite;
	        buttonIcon.sprite = sprite;
	        
	        // 어빌리티 1
	        {
		        var ability1Idx = DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, _dRelicIdx, "d_aidx_1");

		        var abilityNameText = AbilityControl.GetNameText(ability1Idx);

		        var abilityValue = InGameControlHub.My.DiabloRelicController.GetFinalizedAbilityValue(_dRelicIdx, 1);
		        var abilityDescText = AbilityControl.GetDescText(ability1Idx, abilityValue);

		        ability1Text.text = abilityNameText + abilityDescText;
	        }
	        
	        // 어빌리티 2
	        {
		        var ability2Idx = DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, _dRelicIdx, "d_aidx_2");

		        var abilityNameText = AbilityControl.GetNameText(ability2Idx);

		        var abilityValue = InGameControlHub.My.DiabloRelicController.GetFinalizedAbilityValue(_dRelicIdx, 2);
		        var abilityDescText = AbilityControl.GetDescText(ability2Idx, abilityValue);

		        ability2Text.text = abilityNameText + abilityDescText;
	        }

	        // 최대 레벨 체크 
	        var isMaxLevel = InGameControlHub.My.DiabloRelicController.Level.IsMaxLevel(_dRelicIdx);
	         
	        button.SetActive(!isMaxLevel);

	        // 레벨업 가능 시
	        if (!isMaxLevel)
	        {
		        // 버튼 슬롯 
		        RefreshButtonBg();

		        // 버튼 비용 텍스트  
		        var requiredCount = InGameControlHub.My.DiabloRelicController.Level.CalcRequiredCountToLevelUp(_dRelicIdx, 1);
		        buttonText.text = Localization.GetFormatText("info_134", count.ToLookUpString(), requiredCount.ToLookUpString());
	        }
        }

	    [PublicAPI]
	    public void LeveUp()
	    {
		    if (!InGameControlHub.My.DiabloRelicController.Level.CanLevelUp(_dRelicIdx))
			    return;
		    
		    // 사운드 
		    SoundController.Instance.PlayEffect("sfx_ui_bt_upgrade_02");
		    
		    // 업그레이드
		    InGameControlHub.My.DiabloRelicController.Level.LevelUp(_dRelicIdx);
	    }

	    private void RefreshButtonBg()
	    {
		    var canUpgrade = InGameControlHub.My.DiabloRelicController.Level.HasEnoughCount(_dRelicIdx);
		    _buttonSpriteSwapper.Init(canUpgrade);
	    }

	    public override void RefreshCellView()
	    {
		    Refresh();
	    }
    }
}