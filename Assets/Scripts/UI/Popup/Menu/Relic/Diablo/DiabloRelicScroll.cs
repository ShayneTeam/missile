﻿using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using InGame.Data;
using UnityEngine;

namespace UI.Popup.Menu.Relic.Diablo
{
    public class DiabloRelicScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller scroller;
        [SerializeField] private EnhancedScrollerCellView cellPrefab;
        [SerializeField] private float cellSize;

        [SerializeField] private GameObject backText;
        
        private IEnumerable<string> _allEarnedDiabloRelics;
        
        public void Init()
        {
            _allEarnedDiabloRelics = UserData.My.DiabloRelic.GetAllEarned();
            
            backText.SetActive(!_allEarnedDiabloRelics.Any());
            
            scroller.Delegate = this;
            scroller.ReloadData(scroller.NormalizedScrollPosition);
        }

        public void Refresh()
        {
            _allEarnedDiabloRelics = UserData.My.DiabloRelic.GetAllEarned();
            
            backText.SetActive(!_allEarnedDiabloRelics.Any());
            
            scroller.RefreshActiveCellViews();
        }
        
        public int GetNumberOfCells(EnhancedScroller _)
        {
            return _allEarnedDiabloRelics.Count();
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellPrefab) as DiabloRelicUpgradeCell;
            if (cellView == null)
                return null;

            cellView.Init(_allEarnedDiabloRelics.ElementAt(dataIndex));
            
            return cellView;
        }
    }
}