﻿using InGame.Controller;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Relic.Diablo
{
    public class GachaDiabloRelicButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI costText;
        
        
        
        public void Init()
        {
            costText.text = DiabloRelicControl.DiabloRelicGacha.RequiredKeyToGacha.ToLookUpString();
            costText.color = InGameControlHub.My.DiabloRelicController.Gacha.HasEnoughKey ? Color.white : Color.red;
        }

        public void OnClick()
        {
            if (!InGameControlHub.My.DiabloRelicController.Gacha.HasEnoughKey)
            {
                // 열쇠 얻어라 
                MessagePopup.Show("info_170");
                return;
            }
            
            if (InGameControlHub.My.DiabloRelicController.HasAllMaxCount)
            {
                // 더 이상 획득 불가다
                MessagePopup.Show("info_171");
                return;
            }

            // 가챠 가능 횟수 1개 이상이라면, 일괄 가챠
            if (InGameControlHub.My.DiabloRelicController.Gacha.CalcGachaCount() > 1)
            {
                if (InGameControlHub.My.DiabloRelicController.Gacha.GachaBatch(out var results))
                    ShowRewardsPopup.Show(results, "info_157");
            }
            // 가챠 가능 갯수 1개라면, 가챠 하나 띄우기 
            else
            {
                if (InGameControlHub.My.DiabloRelicController.Gacha.GachaOnce(out var dRelicIdx))
                    EarnedNewDiabloRelicPopup.Show(dRelicIdx);
            }
        }
    }
}
