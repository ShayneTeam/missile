﻿using InGame.Controller;
using TMPro;
using UnityEngine;

namespace UI.Popup.Menu.Relic.Diablo
{
    public class ExchangeDiabloRelicButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI exchangeText;
        
        
        
        public void Init()
        {
            exchangeText.text = InGameControlHub.My.DiabloRelicController.Exchange.CalcAllExchangeAmount().ToLookUpString();
            exchangeText.color = InGameControlHub.My.DiabloRelicController.Exchange.HasEnoughKey ? Color.white : Color.red;
        }

        public void OnClick()
        {
            if (!InGameControlHub.My.DiabloRelicController.Exchange.HasEnoughKey)
            {
                // 열쇠 얻어라 
                MessagePopup.Show("info_170");
                return;
            }

            if (!InGameControlHub.My.DiabloRelicController.Exchange.CanExchange)
            {
                // 열쇠 얻어라 
                MessagePopup.Show("info_170");
                return;
            }

            // 환전 
            var exchangedCount = InGameControlHub.My.DiabloRelicController.Exchange.Exchange();
            MessagePopup.Show("info_382", exchangedCount.ToLookUpString());
        }
    }
}
