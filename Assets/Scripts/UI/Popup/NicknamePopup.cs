﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using BackEnd.Tcp;
using Doozy.Engine.UI;
using InGame.Controller;
using JetBrains.Annotations;
using MEC;
using Server;
using TMPro;
using UnityEngine;

namespace UI.Popup
{
    public class NicknamePopup : UIPopupCommon
    {
        [SerializeField] private TMP_InputField inputField;
        
        
        

        public static NicknamePopup Show()
        {
            var popup = UIPopup.GetPopup("Nickname");
            popup.Show();

            return popup.GetComponent<NicknamePopup>();
        }


        private void Start()
        {
            // 닉네임 필터링 구현을 위해 야매로 뒤끝 채팅 필터링 기능 사용
            // 야매 구현이라 여기서 중복 코드가 발생할 수 있음 (어차피 버릴 코드)
            // 만약 뒤끝챗 접속 플로우가 실패했다면, 뒤끝챗 필터링 사용 안 함 (플로우 중단하지 않음)
            NicknameController.ReadyFiltering();
            
            // 이벤트
            NicknameController.OnChangedNickname += OnChangedNickname;
        }

        private void OnDestroy()
        {
            NicknameController.EndFiltering();
            
            // 이벤트 
            NicknameController.OnChangedNickname -= OnChangedNickname;
        }

        [PublicAPI]
        public void Change()
        {
            NicknameController.Instance.StartValidation(inputField.text);
        }

        private void OnChangedNickname(string prevNickname, string currNickname)
        {
            Popup.Hide();
        }
    }
}