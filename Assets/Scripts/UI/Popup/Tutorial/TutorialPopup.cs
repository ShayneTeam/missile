using System;
using System.Collections.Generic;
using System.Linq;
using AsCoroutine;
using Coffee.UIExtensions;
using DG.Tweening;
using Doozy.Engine.UI;
using Doozy.Engine.UI.Input;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using MEC;
using Server;
using TMPro;
using UI.Popup;
using UI.Popup.Menu;
using UI.Popup.Menu.Relic;
using UI.Popup.Menu.Shop;
using UnityEngine;
using UnityEngine.UI;

public class TutorialPopup : Singleton<TutorialPopup>
{
    [SerializeField] private GameObject container;
    [SerializeField] private UnmaskRaycastFilter unmaskRaycastFilter;
    [SerializeField] private Unmask unmask;

    [SerializeField] private Image tutorialPointer;

    [SerializeField] private Image tutorialDialogImage;
    [SerializeField] private TextMeshProUGUI tutorialDialogText;

    [SerializeField] private Image textBoxImage;
    [SerializeField] private TextMeshProUGUI textBoxText;
    
    [SerializeField] private TextMeshProUGUI touchToScreen;

    [SerializeField] private Button touchBtnArea;


    private static bool _canPassTutorial;

    private static bool _isPlaying;
    public static bool IsPlaying
    {
        get => _isPlaying;
        private set
        {
            _isPlaying = value;
            
            // BackButton 제어 
            if (value) BackButton.Disable();
            else BackButton.Enable();
        }
    }

    private int currentTutorialIndex = -1;

    private (string id, string sheet, string tutorialGroupIdx) tutorialInfo;

    private List<(string Idx, int currentTutorialIdx)> tutorialSequenceIdx = new List<(string, int)>();


    public void Init()
    {
        InitStaticField();
        
        currentTutorialIndex = -1;
        tutorialSequenceIdx.Clear();
    }

    [RuntimeInitializeOnLoadMethod]
    private static void EnterPlayMode()
    {
        InitStaticField();
    }

    private static void InitStaticField()
    {
        _canPassTutorial = false;
        IsPlaying = false;
    }

    public void TutorialConditionCheck(string guideConditionType, int conditionValue)
    {
        if (!CanTutorialStart) return;

        // 기존 튜토리얼 정보 초기화
        tutorialSequenceIdx.Clear();

        // 튜토리얼의 정보 를 가져옴.
        tutorialInfo = GetTutorialInfo(guideConditionType, conditionValue);

        // 비어있으면 오류가 발생한 것.
        if (string.IsNullOrWhiteSpace(tutorialInfo.tutorialGroupIdx))
        {
            Close();
            return;
        }

        // 해당하는 시트에서 진행해야 할 시퀀스에 해당하는 ID List 를 전부 가져옴.
        var _allIdxes = new List<string>();
        _allIdxes.AddRange(DataboxController.GetAllIdxesByGroup(Table.Tutorial, tutorialInfo.sheet,
            Column.tutorial_group,
            tutorialInfo.tutorialGroupIdx));

        // 진행할 시퀀스의 갯수가 0 개면 진행 안함.
        if (_allIdxes.Count == 0)
        {
            Close();
            return;
        }

        foreach (var tutorialSeq in _allIdxes)
        {
            var tutorialId =
                DataboxController.GetDataInt(Table.Tutorial, tutorialInfo.sheet, tutorialSeq, "current_tutorial", 0);
            tutorialSequenceIdx.Add((tutorialSeq, tutorialId));
        }

        // 튜토리얼 진행
        StartTutorial();
    }

    private void StartTutorial()
    {
        IsPlaying = true;
        container.SetActive(true);

        // 시퀀스의 첫번째 튜토리얼을 진행한다.
        currentTutorialIndex = 0;
        Timing.RunCoroutine(StartSequence(tutorialSequenceIdx[currentTutorialIndex].Idx));
    }

    public void NextTutorial()
    {
        currentTutorialIndex += 1;
        Timing.RunCoroutine(StartSequence(tutorialSequenceIdx[currentTutorialIndex].Idx));
    }

    public void GotoNextCustomTutorial()
    {
        // var currentTutorialIdx = tutorialSequenceIdx[currentTutorialIndex];
        // var data = DataboxController.GetDataInt(Table.Tutorial, tutorialInfo.sheet, currentTutorialIdx, "next_tutorial",
        //     0);
    }

    IEnumerator<float> StartSequence(string sequenceId)
    {
        var searchSequenceId =
            DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId, Column.condition_type,
                string.Empty);

        var searchConditionValue =
            DataboxController.GetDataInt(Table.Tutorial, tutorialInfo.sheet, sequenceId, Column.condition_value, 0);

        // 조건이 있는데 만족했다면 다음 시퀀스 실행
        if (!string.IsNullOrWhiteSpace(searchSequenceId) && searchSequenceId != "NONE")
        {
            // 조건이 존재한다면, 조건 관련 부분을 체크한다.
            var condition = ConditionCheck(searchSequenceId, searchConditionValue);

            // 조건에 만족했다면
            if (condition)
            {
                // 조건이 해당하지 않을때의 시퀀스를 찾아서 실행함.
                var nextTutorialSequenceId = DataboxController.GetDataInt(Table.Tutorial, tutorialInfo.sheet,
                    sequenceId,
                    "condition_false_tutorial");

                var goTutorial = tutorialSequenceIdx.FindIndex(x => x.currentTutorialIdx == nextTutorialSequenceId);
                currentTutorialIndex = goTutorial;
                Timing.RunCoroutine(StartSequence(tutorialSequenceIdx[currentTutorialIndex].Idx));
            }
            else // 조건에 만족하지 않았다면
            {
                NextTutorial();
            }

            yield break;
        }

        // popup-path 가 존재할 경우, 오브젝트의 생성이 필요함. (UIPopup.Show)
        var popupPath = DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId,
            Column.popup_path, string.Empty);

        if (popupPath != "0")
        {
            // 팝업 보여주기 
            var popup = UIPopup.GetPopup(popupPath);

            // 팝업 열릴 때까지 무한 대기
            yield return Timing.WaitUntilDone(popup.WaitShow().CancelWith(gameObject));

            // 액션 타입 체크
            var actionType = DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId,
                Column.action_type, string.Empty);
            // 팝업을 생성하는게 이 튜토리얼의 목적이라면, 이미 완료한 것이기 때문에 다음 튜토리얼을 진행할 수 있도록 한다.
            if (actionType == "INSTANCE_POPUP")
            {
                NextTutorial();
                yield break;
            }
        }

        // object_path 가 존재할 경우, 이 오브젝트를 강조해야 함.
        var searchObjectPath = DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId,
            Column.object_path, string.Empty);

        unmask.gameObject.SetActive(false);
        tutorialPointer.gameObject.SetActive(false);
        textBoxImage.gameObject.SetActive(false);
        touchToScreen.gameObject.SetActive(false);

        if (searchObjectPath != "0" && !string.IsNullOrWhiteSpace(searchObjectPath))
        {
            var gameObj = GameObject.Find(searchObjectPath);
            if (gameObj == null)
            {
                ULogger.LogError($@"튜토리얼_ 경로오류! : {searchObjectPath} 경로는 존재하지 않습니다!! 테이블 확인해주세요!!");
                Close();
                yield break;
            }

            var rect = gameObj.GetComponent<RectTransform>();

            unmask.gameObject.SetActive(true);
            unmask.fitTarget = rect;

            unmaskRaycastFilter.enabled = false;
            
            // 포인터 설정
            PointerPositionTest(sequenceId, rect);
            
            // Text 박스의 포지션 셋팅
            TextBoxSetting(sequenceId, rect);

        }

        // Dialog 박스의 포지션 셋팅
        DialogSetting(sequenceId);

        // actionType 셋팅
        ActionSetting(sequenceId);
    }

    private void ActionSetting(string sequenceId)
    {
        var actionType = DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId,
            Column.action_type, string.Empty);

        touchBtnArea.gameObject.SetActive(false);

        switch (actionType)
        {
            case "REWARD":
                TutorialClear();
                break;
            case "CONDITION_CHECK":
                break;
            case "TOUCH_SCREEN":
                touchToScreen.gameObject.SetActive(true);
                touchBtnArea.gameObject.SetActive(true);
                touchBtnArea.onClick.RemoveAllListeners();
                touchBtnArea.onClick.AddListener(NextTutorial);
                break;
            // 아래항목들은 버튼이 클릭될 수 있어야 함.
            case "WATCH_ADS":
                unmaskRaycastFilter.enabled = true;
                break;
            case "TOUCH_BUTTON":
                unmaskRaycastFilter.enabled = true;

                // 호출 시점이 좀 모호하여 이렇게 처리. 
                // 터치_버튼의 경우, 사실상 UIPopup 이 열릴때까지 대기한다.
                NextTutorial();
                break;
            case "MOVE_TO_SHOP_MISSILE":
                break;
            case "MOVE_TO_SHOP_STONE":
                touchBtnArea.gameObject.SetActive(true);
                touchBtnArea.onClick.RemoveAllListeners();
                touchBtnArea.onClick.AddListener(() =>
                {
                    var menu = InGameMenu.Open("Shop Menu");

                    var pop = menu.GetComponent<ShopMenuPopup>();
                    pop.Init();
                    pop.ChangeTab(ShopTab.Missile);
                    pop.SwitchScrollWith((int)ShopTab.Missile, 1f);

                    this.AsCoroutine().YieldWaitUntil(() => pop.IsInit)
                        .YieldWaitForSeconds(1f)
                        .Action(()=>
                        {
                            NextTutorial();
                        }).Start(this);
                });
                break;
            case "FREE_MISSILE":
                break;
            case "FREE_STONE":
                break;
            case "DIABLO_RELIC":
                touchBtnArea.gameObject.SetActive(true);
                touchBtnArea.onClick.RemoveAllListeners();
                touchBtnArea.onClick.AddListener(() =>
                {
                    var menu = InGameMenu.Open("Relic Menu");

                    var pop = menu.GetComponent<RelicMenuPopup>();
                    pop.Init();

                    this.AsCoroutine().YieldWaitUntil(() => pop._isInit)
                        .Action(() =>
                        {
                            pop.ShowDiabloRelic();
                            NextTutorial();
                        }).Start(this);
                });
                break;
            case "INSTANCE_POPUP":
                break;
            default:
                throw new ArgumentException($"허가되지 않은 타입 {actionType}");
                break;
        }
    }

    void PointerPositionTest(string sequenceId, RectTransform targetPosition)
    {
        var searchPointerType = DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId,
            Column.pointer, string.Empty);

        tutorialPointer.gameObject.SetActive(true);

        var tutorialPosition = targetPosition.transform.position;

        switch (searchPointerType)
        {
            case "NONE":
                tutorialPointer.gameObject.SetActive(false);
                break;
            case "NORTH": // TOP
                tutorialPointer.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x,
                    tutorialPosition.y + targetPosition.rect.width / 2, tutorialPosition.z), Quaternion.Euler(0f,0f,0f));
                break;
            case "NORTH_EAST": // RIGHT_TOP
                tutorialPointer.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x + targetPosition.rect.width / 2,
                    tutorialPosition.y + targetPosition.rect.height / 2, tutorialPosition.z), Quaternion.Euler(0f,0f,-45f));
                break;
            case "EAST": // RIGHT
                tutorialPointer.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x + targetPosition.rect.width / 2,
                    tutorialPosition.y, tutorialPosition.z), Quaternion.Euler(0f,0f,-90f));
                break;
            case "SOUTH_EAST": // RIGHT_BOTTOM
                tutorialPointer.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x + targetPosition.rect.width / 2,
                    tutorialPosition.y - targetPosition.rect.height / 2, tutorialPosition.z), Quaternion.Euler(0f,0f,-135f));
                break;
            case "SOUTH": // BOTTOM
                tutorialPointer.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x,
                    tutorialPosition.y - targetPosition.rect.height / 2, tutorialPosition.z), Quaternion.Euler(0f,0f,180f));
                break;
            case "SOUTH_WEST": // LEFT_BOTTOM
                tutorialPointer.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x - targetPosition.rect.width / 2,
                    tutorialPosition.y - targetPosition.rect.height / 2, tutorialPosition.z), Quaternion.Euler(0f,0f,135f));
                break;
            case "WEST": // LEFT
                tutorialPointer.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x - targetPosition.rect.width / 2,
                    tutorialPosition.y, tutorialPosition.z), Quaternion.Euler(0f,0f,90f));
                break;
            case "NORTH_WEST": // LEFT_TOP
                tutorialPointer.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x - targetPosition.rect.width / 2,
                    tutorialPosition.y + targetPosition.rect.height / 2, tutorialPosition.z), Quaternion.Euler(0f,0f,45f));
                break;
        }
    }

    private void TextBoxSetting(string sequenceId, RectTransform targetPosition)
    {
        var searchTextPosition = DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId,
            Column.text_box_position, string.Empty);
        var searchBoxContent = DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId,
            Column.text_box_content, string.Empty);

        textBoxImage.gameObject.SetActive(true);
        textBoxText.text = Localization.GetText(searchBoxContent);
        textBoxImage.transform.SetParent(targetPosition, false);

        switch (searchTextPosition)
        {
            case "NONE":
                textBoxImage.gameObject.SetActive(false);
                break;
            case "BOTTOM_LEFT": // OK
                textBoxImage.rectTransform.anchorMin = new Vector2(1, 1);
                textBoxImage.rectTransform.anchorMax = new Vector2(1, 1);
                textBoxImage.rectTransform.pivot = new Vector2(1, 1);
                textBoxImage.rectTransform.anchoredPosition = new Vector3(0, targetPosition.rect.height*-1, 0);
                break;
            case "BOTTOM_RIGHT": // OK
                textBoxImage.rectTransform.anchorMin = new Vector2(0, 1);
                textBoxImage.rectTransform.anchorMax = new Vector2(0, 1);
                textBoxImage.rectTransform.pivot = new Vector2(0, 1);
                textBoxImage.rectTransform.anchoredPosition = new Vector3(0, targetPosition.rect.height*-1, 0);
                break;
            case "TOP_LEFT": // OK
                textBoxImage.rectTransform.anchorMin = new Vector2(1, 0);
                textBoxImage.rectTransform.anchorMax = new Vector2(1, 0);
                textBoxImage.rectTransform.pivot = new Vector2(1, 0);
                textBoxImage.rectTransform.anchoredPosition =
                    new Vector3(0, targetPosition.rect.height, 0);
                break;
            case "UPPER_RIGHT": // OK
                textBoxImage.rectTransform.anchorMin = new Vector2(0, 0);
                textBoxImage.rectTransform.anchorMax = new Vector2(0, 0);
                textBoxImage.rectTransform.pivot = new Vector2(0, 0);
                textBoxImage.rectTransform.anchoredPosition =
                    new Vector3(targetPosition.rect.width, 0, 0);
                break;
            case "UPPER_LEFT": // OK
                textBoxImage.rectTransform.anchorMin = new Vector2(1, 0);
                textBoxImage.rectTransform.anchorMax = new Vector2(1, 0);
                textBoxImage.rectTransform.pivot = new Vector2(1, 0);
                textBoxImage.rectTransform.anchoredPosition =
                    new Vector3(targetPosition.rect.width * -1, 0, 0);
                break;
            case "LOWER_LEFT": // OK
                textBoxImage.rectTransform.anchorMin = new Vector2(1, 1);
                textBoxImage.rectTransform.anchorMax = new Vector2(1, 1);
                textBoxImage.rectTransform.pivot = new Vector2(1, 1);
                textBoxImage.rectTransform.anchoredPosition =
                    new Vector3(targetPosition.rect.width * -1, 0, 0);
                break;
            case "TOP_RIGHT": // OK
                textBoxImage.rectTransform.anchorMin = new Vector2(0, 0);
                textBoxImage.rectTransform.anchorMax = new Vector2(0, 0);
                textBoxImage.rectTransform.pivot = new Vector2(0, 0);
                textBoxImage.rectTransform.anchoredPosition = new Vector3(0, targetPosition.rect.height, 0);
                break;
            case "LOWER_RIGHT": // OK
                textBoxImage.rectTransform.anchorMin = new Vector2(0, 1);
                textBoxImage.rectTransform.anchorMax = new Vector2(0, 1);
                textBoxImage.rectTransform.pivot = new Vector2(0, 1);
                textBoxImage.rectTransform.anchoredPosition = new Vector3(targetPosition.rect.height, 0, 0);
                break;
            default:
                ULogger.LogError($@"{searchTextPosition} 은 정의되지 않았습니다.");
                break;
        }
        
        textBoxImage.transform.SetParent(gameObject.transform, true);
        textBoxImage.transform.localScale = Vector3.one;
    }

    private void DialogSetting(string sequenceId)
    {
        var searchBoxPosition = DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId,
            Column.dialog_box_position, string.Empty);
        var searchBoxContent = DataboxController.GetDataString(Table.Tutorial, tutorialInfo.sheet, sequenceId,
            Column.dialog_box_content, string.Empty);

        tutorialDialogImage.gameObject.SetActive(true);

        switch (searchBoxPosition)
        {
            case "NONE":
                tutorialDialogImage.gameObject.SetActive(false);
                break;
            case "TOP":
                tutorialDialogText.text = Localization.GetText(searchBoxContent);
                break;
            case "BOTTOM":
                tutorialDialogText.text = Localization.GetText(searchBoxContent);
                break;
        }
    }

    private bool ConditionCheck(string conditionType, int conditionValue)
    {
        switch (conditionType)
        {
            case "NONE":
                return true;
            case "BUFF_SPEED_REMAIN_TIME":
                var userSpeedRemainTime = UserData.My.Buffs.GetRemainTime("BUFF_SPEED");
                var adRemoveCheck = UserData.My.UserInfo.GetFlag(UserFlags.CanAdRemove);

                // 조건보다 유저 광고수치가 더 높다면, 이미 봤다는 이야기.
                if (userSpeedRemainTime > conditionValue)
                    return true;

                // 광고를 제거했다면, 이미 봤다는 이야기.
                if (adRemoveCheck)
                    return true;

                return false;
            case "RELIC_COST":
                // 일반 유물 가격 확인
                var normalRelicPrice = InGameControlHub.My.NormalRelicController.GetNeedUraniumForGacha();
                return normalRelicPrice > 0;
                break;
            default:
                // return true;
                throw new ArgumentException("허가되지 않은 타입 ");
        }

        return false;
    }

    // ID = Row의 ID, Sheet = 해당하는 튜토리얼의 시트명, tutoraiLGroup
    private (string id, string sheet, string tutorialGroupIdx) GetTutorialInfo(string guideConditionType,
        int conditionValue)
    {
        var allIdxesSameGroup = DataboxController.GetAllIdxesByGroup(Table.Tutorial, Sheet.management,
            "guide_condition_type", guideConditionType);

        // 해당 조건과 일치하는 Row 데이터를 뽑아야 함.
        foreach (var idx in allIdxesSameGroup)
        {
            var sheetName = GetSheetType(idx);
            if (CheckTutorialCondition(conditionValue, sheetName, idx, out var tutorialGroupId))
                return (idx, sheetName, tutorialGroupId);
        }

        return (string.Empty, string.Empty, string.Empty);
    }

    private bool CheckTutorialCondition(int conditionValue, string idxSheet, string idx, out string tutorialGroupId)
    {
        switch (idxSheet)
        {
            case TutorialSheetType.TUTORIAL:
            {
                var idxConditionValue =
                    DataboxController.GetDataInt(Table.Tutorial, Sheet.management, idx, "guide_condition_value");
                // 순서대로 도는데, 조건을 이미 충족했는데도 깨지 않은 튜토리얼이 있다면, 해당 튜토리얼을 실행함.
                if (conditionValue >= idxConditionValue && UserData.My.Tutorial.GetFlag($"Tutorial_{idx}") == false)
                {
                    tutorialGroupId = idx;
                    return true;
                }
            }
                break;
            case TutorialSheetType.POPUP:
            {
                // todo : Popup 형태라면, 하루에 한번만 나와야 함.
                // var idxConditionValue =
                //     DataboxController.GetDataInt(Table.Tutorial, Sheet.management, idx, "guide_condition_value");
                // // 순서대로 도는데, 조건을 이미 충족했는데도 깨지 않은 튜토리얼이 있다면, 해당 튜토리얼을 실행함.
                // if (conditionValue >= idxConditionValue && UserData.My.Tutorial.GetFlag(idx) == false)
                // {
                //     tutorialIdx = idx;
                //     break;
                // }
            }
                break;
            default:
            {
                ULogger.Log("Error");
                Close();
            }
                break;
        }

        tutorialGroupId = string.Empty;
        return false;
    }

    private static string GetSheetType(string idx)
    {
        var idxSheet =
            DataboxController.GetDataString(Table.Tutorial, Sheet.management, idx, "sheet", string.Empty);

        return idxSheet;
    }

    public static bool CanTutorialStart
    {
        get
        {
            if (FirebaseRemoteConfigController.tutorialShow == false) return false;
            
            // 유저의 InDate 가 튜토리얼 개편 (업데이트) 이후여야 실행 가능.
            if (_canPassTutorial) return false;
            
            if (DateTime.Compare(Convert.ToDateTime(ServerMyInfo.InDate), new DateTime(2022, 1, 10, 0, 0, 0)) == -1)
            {
                _canPassTutorial = true;
                return false;
            }

            // 튜토리얼을 실행중이 아니라면 튜토리얼 실행 가능.
            if (IsPlaying) return false;

            // 어떠한 팝업이라도 열려있으면 튜토리얼 안함.
            if (UIPopup.AnyPopupVisible) return false;

            return true;
        }
    }

    void TutorialClear()
    {
        var tutorialKey = $"Tutorial_{tutorialInfo.tutorialGroupIdx}";

        // 혹 이미 깨져있다면 return.
        if (UserData.My.Tutorial.GetFlag(tutorialKey))
        {
            Close();
            return;
        }

        // 클리어 처리
        UserData.My.Tutorial.SetFlag(tutorialKey, true);

        // 보상 지급
        var rewardType = DataboxController.GetDataString(Table.Tutorial, Sheet.management, tutorialInfo.id,
            Column.reward_type, string.Empty);
        var rewardCount =
            DataboxController.GetDataInt(Table.Tutorial, Sheet.management, tutorialInfo.id, Column.reward_count, 0);

        var reward = new RewardData
        {
            Idx = "", // 일반 아이템은 타입이 곧 Idx
            Type = rewardType,
            Count = rewardCount
        };
        RewardTransactor.Transact(reward, $"{LogEvent.Tutorial}_{tutorialInfo.id}");

        // 보상지급 메세지
        MessagePopup.Show("info_318", rewardCount.ToLookUpString());

        // 유저 데이터 즉각 저장
        UserData.My.SaveToServer();
        
        // 로그
        WebLog.Instance.AllLog($"tutorial_clear", new Dictionary<string, object>{{"groupIndex",tutorialInfo.tutorialGroupIdx}});
        
        // 튜토리얼 클리어_ 지표 퍼널용으로 좀 쉽게 변형하여 한번 더 전송
        WebLog.Instance.ThirdPartyLog($"tutorial_clear_{tutorialInfo.tutorialGroupIdx}");

        Close();
    }

    void Close()
    {
        IsPlaying = false;
        container.SetActive(false);
    }

    #region 튜토리얼_미사일 뽑기

    // ReSharper disable Unity.PerformanceAnalysis
    void MissileTutorialStart(int index)
    {
        tutorialPointer.transform.DOKill();

        switch (index)
        {
            // 미사일뽑기 팝업 띄우기
            case 0:
                container.gameObject.SetActive(false);
                tutorialPointer.gameObject.SetActive(false);

                var popup = UIPopup.GetPopup("Missile Tutorial");
                popup.Show();

                popup.Data.Buttons[0].Button.onClick.AddListener(() =>
                {
                    var menu = InGameMenu.Open("Shop Menu");

                    var pop = menu.GetComponent<ShopMenuPopup>();
                    pop.Init();
                    pop.ChangeTab(ShopTab.Missile);

                    this.AsCoroutine().YieldWaitUntil(() => pop.IsInit)
                        .Action(() =>
                        {
                            popup.Hide();
                            MissileTutorialStart(1);
                        }).Start(this);
                });

                break;
            case 1:
                container.gameObject.SetActive(true);
                tutorialPointer.gameObject.SetActive(true);

                var gameObj =
                    GameObject.Find(
                        "Shop Menu Popup(Clone)/Container/Scrolls/Scroll (Missile)/Container/Shop Cell Missile (Gacha)(Clone)/Button");

                var rect = gameObj.GetComponent<RectTransform>();

                unmaskRaycastFilter.targetUnmask.gameObject.SetActive(true);
                unmaskRaycastFilter.targetUnmask.fitTarget = rect;

                var unmaskPos = unmaskRaycastFilter.targetUnmask.fitTarget.transform.position;
                tutorialPointer.transform.position = new Vector3(unmaskPos.x, unmaskPos.y + 50f, 0f);
                tutorialPointer.transform.rotation = Quaternion.Euler(0, 0, 0f);
                tutorialPointer.transform.DOLocalMove(new Vector3(0, 7, 0), 1f).SetRelative(true)
                    .SetLoops(-1, LoopType.Yoyo).SetAutoKill(true);

                var btn = gameObj.GetComponent<UIButton>();

                Action ac = () => { MissileTutorialStart(-1); };

                btn.OnClick.OnTrigger.Event.AddListener(() =>
                {
                    ac?.Invoke();
                    ac = null;
                });
                break;
            case -1: // complete

                LegacyCompleteTutorial(UserFlags.TutorialMissileComplete);

                break;
            default:
                break;
        }
    }

    #endregion

    #region 튜토리얼_ 유물뽑기

    void NormalRelicTutorialStart(int index)
    {
        tutorialPointer.transform.DOKill();

        switch (index)
        {
            case 0:
            {
                // 화면상에 존재하 유물 버튼을 누르도록 유도
                var obj = GameObject.Find("Quick Menu/Relic");
                var rect = obj.GetComponent<RectTransform>();

                unmaskRaycastFilter.targetUnmask.fitTarget = rect;

                var unmaskPos = unmaskRaycastFilter.targetUnmask.fitTarget.transform.position;
                tutorialPointer.transform.position = new Vector3(unmaskPos.x + 140, unmaskPos.y - 58f, 0f);
                tutorialPointer.transform.rotation = Quaternion.Euler(0, 0, 178f);
                tutorialPointer.transform.DOLocalMove(new Vector3(10, 7, 0), 1f).SetRelative(true)
                    .SetLoops(-1, LoopType.Yoyo).SetAutoKill(true);

                var btn = obj.GetComponent<UIButton>();

                Action ac = () =>
                {
                    if (!UserData.My.NormalRelic.GetAllEarned().Any()) // 어떤거라도 획득을 했는지 체크.
                    {
                        var menu = InGameMenu.Open("Relic Menu");

                        var pop = menu.GetComponent<RelicMenuPopup>();
                        pop.Init();

                        this.AsCoroutine().YieldWaitUntil(() => pop._isInit)
                            .Action(() =>
                            {
                                pop.ShowRelicGallery();
                                NormalRelicTutorialStart(1);
                            }).Start(this);
                    }
                    else
                    {
                        NormalRelicTutorialStart(-1);
                    }
                };

                btn.Button.onClick.AddListener(() =>
                {
                    ac?.Invoke();
                    ac = null;
                });
            }

                break;
            case 1:
            {
                var gameObj = GameObject.Find("Relic Menu Popup(Clone)/Container/Bottom (Gallery)/Button");
                var rect = gameObj.GetComponent<RectTransform>();
                unmaskRaycastFilter.targetUnmask.fitTarget = rect;

                var unmaskPos = unmaskRaycastFilter.targetUnmask.fitTarget.transform.position;
                tutorialPointer.transform.position = new Vector3(unmaskPos.x - 250, unmaskPos.y + 140, 0f);
                tutorialPointer.transform.rotation = Quaternion.Euler(0, 0, 0f);
                tutorialPointer.transform.DOLocalMove(new Vector3(10, 7, 0), 1f).SetRelative(true)
                    .SetLoops(-1, LoopType.Yoyo).SetAutoKill(true);

                var btn = gameObj.GetComponent<UIButton>();

                Action ac = () => { NormalRelicTutorialStart(-1); };

                btn.OnClick.OnTrigger.Event.AddListener(() =>
                {
                    ac?.Invoke();
                    ac = null;
                });
            }

                break;
            case -1: // complete

                LegacyCompleteTutorial(UserFlags.TutorialNormalRelicComplete);

                // reward
                ResourceController.Instance.AddUranium(100, $"{LogEvent.Tutorial}_NormalRelic");
                break;
            default:
                break;
        }
    }

    #endregion

    #region 튜토리얼_광고재생

    void AdsTutorialStart(int index)
    {
        tutorialPointer.transform.DOKill();

        switch (index)
        {
            case 0:
            {
                // 화면상에 존재하는 스피드 버프 버튼을 누르도록 유도
                var speedGameObject = GameObject.Find("Buffs And Option/Buff/Speed");
                var rect = speedGameObject.GetComponent<RectTransform>();

                unmaskRaycastFilter.targetUnmask.fitTarget = rect;

                var unmaskPos = unmaskRaycastFilter.targetUnmask.fitTarget.transform.position;
                tutorialPointer.transform.position = new Vector3(unmaskPos.x - 140, unmaskPos.y - 138f, 0f);
                tutorialPointer.transform.rotation = Quaternion.Euler(0, 0, 93.42f);
                tutorialPointer.transform.DOLocalMove(new Vector3(10, 7, 0), 1f).SetRelative(true)
                    .SetLoops(-1, LoopType.Yoyo).SetAutoKill(true);

                var btn = speedGameObject.GetComponent<UIButton>();

                Action ac = () =>
                {
                    if (!UserData.My.Buffs.IsEnoughRemainTime("BUFF_SPEED") && // 버프를 활성화하지 않아도 남은 시간이 있거나
                        !UserData.My.UserInfo.GetFlag(UserFlags.CanBeInfinityTimeAllBuff)) // 버프 무제한 상품을 구매한 상태이면
                    {
                        AdsTutorialStart(1);
                    }
                    else
                    {
                        AdsTutorialStart(-1);
                    }
                };

                btn.Button.onClick.AddListener(() =>
                {
                    ac?.Invoke();
                    ac = null;
                });
            }

                break;
            case 1:
            {
                var gameObj = GameObject.Find("Buff Ads Popup(Clone)/Container/Exp & Watch/Show Ads");
                var rect = gameObj.GetComponent<RectTransform>();
                unmaskRaycastFilter.targetUnmask.fitTarget = rect;

                var unmaskPos = unmaskRaycastFilter.targetUnmask.fitTarget.transform.position;
                tutorialPointer.transform.position = new Vector3(unmaskPos.x - 250, unmaskPos.y - 138f, 0f);
                tutorialPointer.transform.DOLocalMove(new Vector3(10, 7, 0), 1f).SetRelative(true)
                    .SetLoops(-1, LoopType.Yoyo).SetAutoKill(true);

                var btn = gameObj.GetComponent<UIButton>();

                Action ac = () => { AdsTutorialStart(-1); };

                btn.OnClick.OnTrigger.Event.AddListener(() =>
                {
                    ac?.Invoke();
                    ac = null;
                });
            }

                break;
            case -1: // complete

                LegacyCompleteTutorial(UserFlags.TutorialAdsComplete);

                // reward
                ResourceController.Instance.AddUranium(100, $"{LogEvent.Tutorial}_Ads");
                break;
            default:
                break;
        }
    }

    #endregion


    void LegacyCompleteTutorial(string flag)
    {
        // 방어코드
        if (UserData.My.UserInfo.GetFlag(flag) == true)
        {
            Close();
            return;
        }
        
        // flag Update
        UserData.My.UserInfo.SetFlag(flag, true);

        var rewardCount = 100;
        
        MessagePopup.Show("info_318", rewardCount.ToLookUpString());
        
        // reward
        ResourceController.Instance.AddUranium(rewardCount, $"{LogEvent.Tutorial}_Missile");

        // 서버 데이터 저장 구문 즉시 호출함.
        UserData.My.SaveToServer();
        
        Close();
    }


    public static class GuideConditionType
    {
        public const string NONE = "NONE";
        public const string DEFEAT = "DEFEAT";
        public const string STAGE = "STAGE";
    }

    public static class TutorialSheetType
    {
        public const string MANAGEMENT = "management";
        public const string TUTORIAL = "tutorial";
        public const string POPUP = "popup";
    }
}

public enum pointerType
{
    NONE,
    LEFT,
    RIGHT,
    TOP,
    BOTTOM,
    LEFT_TOP,
    LEFT_BOTTOM,
    RIGHT_TOP,
    RIGHT_BOTTOM
}

public static class TutorialExtensionClass
{
    public static void SetDynamicPointer(this Image tutorailBtn, Image targetPosition, pointerType pointerType,
        string a)
    {
        tutorailBtn.gameObject.SetActive(true);

        var tutorialPosition = targetPosition.transform.position;

        switch (pointerType)
        {
            case pointerType.NONE:
                tutorailBtn.gameObject.SetActive(false);
                break;
            case pointerType.LEFT:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                    tutorialPosition.x - targetPosition.preferredWidth / 2,
                    tutorialPosition.y, tutorialPosition.z), Quaternion.Euler(0f, 0f, 0f));
                break;
            case pointerType.RIGHT:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                    tutorialPosition.x + targetPosition.preferredWidth / 2,
                    tutorialPosition.y, tutorialPosition.z), Quaternion.Euler(0f, 0f, 0f));
                break;
            case pointerType.TOP:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x,
                        tutorialPosition.y + targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 0f));
                break;
            case pointerType.BOTTOM:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x,
                        tutorialPosition.y - targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 0f));
                break;
            case pointerType.LEFT_TOP:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                        tutorialPosition.x - targetPosition.preferredWidth / 2,
                        tutorialPosition.y + targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 0f));
                break;
            case pointerType.LEFT_BOTTOM:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                        tutorialPosition.x - targetPosition.preferredWidth / 2,
                        tutorialPosition.y - targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 0f));
                break;
            case pointerType.RIGHT_TOP:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                        tutorialPosition.x + targetPosition.preferredWidth / 2,
                        tutorialPosition.y + targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 0f));
                break;
            case pointerType.RIGHT_BOTTOM:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                        tutorialPosition.x + targetPosition.preferredWidth / 2,
                        tutorialPosition.y - targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 0f));
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(pointerType), pointerType, null);
        }
    }

    public static void SetTutorialPointer(this Image tutorailBtn, Image targetPosition, pointerType pointerType,
        string a)
    {
        tutorailBtn.gameObject.SetActive(true);

        var tutorialPosition = targetPosition.transform.position;

        switch (pointerType)
        {
            case pointerType.NONE:
                tutorailBtn.gameObject.SetActive(false);
                break;
            case pointerType.LEFT:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                    tutorialPosition.x - targetPosition.preferredWidth / 2,
                    tutorialPosition.y, tutorialPosition.z), Quaternion.Euler(0f, 0f, 90f));
                break;
            case pointerType.RIGHT:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                    tutorialPosition.x + targetPosition.preferredWidth / 2,
                    tutorialPosition.y, tutorialPosition.z), Quaternion.Euler(0f, 0f, -90f));
                break;
            case pointerType.TOP:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x,
                        tutorialPosition.y + targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 0f));
                break;
            case pointerType.BOTTOM:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(tutorialPosition.x,
                        tutorialPosition.y - targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 180f));
                break;
            case pointerType.LEFT_TOP:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                        tutorialPosition.x - targetPosition.preferredWidth / 2,
                        tutorialPosition.y + targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 45f));
                break;
            case pointerType.LEFT_BOTTOM:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                        tutorialPosition.x - targetPosition.preferredWidth / 2,
                        tutorialPosition.y - targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, 135f));
                break;
            case pointerType.RIGHT_TOP:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                        tutorialPosition.x + targetPosition.preferredWidth / 2,
                        tutorialPosition.y + targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, -45f));
                break;
            case pointerType.RIGHT_BOTTOM:
                tutorailBtn.transform.SetPositionAndRotation(new Vector3(
                        tutorialPosition.x + targetPosition.preferredWidth / 2,
                        tutorialPosition.y - targetPosition.preferredHeight / 2, tutorialPosition.z),
                    Quaternion.Euler(0f, 0f, -135f));
                break;
            default:
                throw new ArgumentOutOfRangeException(nameof(pointerType), pointerType, null);
        }
    }
}