﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using DG.Tweening;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    [RequireComponent(typeof(UIPopup))] 
    public class ShowRewardsPopup : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller scroller;
        
        [SerializeField] private EnhancedScrollerCellView cellViewPrefab;
        
        [SerializeField] private float cellViewSize;
        
        [SerializeField] private float rewardShowIntervalSec;



        private IEnumerable<RewardData> _boxRewards;
        private int _visibleRewardsCount;

        private bool _isDirecting;
        private CoroutineHandle _direction;

        public static void Show(IEnumerable<RewardData> boxRewards, string titleKey = "info_077")
        {
            var popup = UIPopup.GetPopup("Open Box Popup");
            popup.Data.SetLabelsTexts(
                Localization.GetText(titleKey)
                );
            
            var openBoxPopup = popup.GetComponent<ShowRewardsPopup>();
            openBoxPopup.Init(boxRewards);	// 상자 획득 연출까지 끝났다면 UI 갱신 

            popup.Show();
        }

        private void Init(IEnumerable<RewardData> boxRewards)
        {
            _boxRewards = boxRewards;
            
            // 스크롤러 초기화 
            scroller.Delegate = this;
            //scroller.ReloadData();    // 연출 상 _visibleRewardsCount가 0일 때 호출 시, 0을 나누면서 생기는 예외 발생 
        }

        [PublicAPI]
        public void StartDirecting()
        {
            // 팝업 Show 애니 다 끝나고 호출됨  
            _direction = Timing.RunCoroutine(_DirectingFlow().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHide()
        {
            // _visibleRewardsCount가 0일 때, scroller.ReloadData()가 호출되지 않게 해야 함
            Timing.KillCoroutines(_direction);
            _visibleRewardsCount = 0;
        }

        private IEnumerator<float> _DirectingFlow()
        {
            _isDirecting = true;

            for (var count = 0; count < _boxRewards.Count(); count++)
            {
                _visibleRewardsCount++;
                
                scroller.ReloadData(1f);
                
                // 사운드 
                SoundController.Instance.PlayEffect("sfx_ui_gacha_01");
                
                // 대기 
                yield return Timing.WaitForSeconds(rewardShowIntervalSec);
            }

            _isDirecting = false;
        }
        
        
        #region EnhancedScroller Handlers

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellViewPrefab) as RewardItemCellView;
            if (cellView == null)
                return null;
            
            cellView.Init(_boxRewards.ElementAt(dataIndex));

            // 연출 상 주기적으로 추가되는 셀뷰만 애니메이션 시킴  
            if (_isDirecting && dataIndex == _visibleRewardsCount - 1)
            {
                cellView.transform.localScale = Vector3.one;
                cellView.ShowAnim();
            }

            return cellView;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellViewSize;
        }

        public int GetNumberOfCells(EnhancedScroller _)
        {
            // 연출상 주기적으로 셀뷰 카운트가 늘어남   
            return _visibleRewardsCount;
        }

        #endregion
    }
}