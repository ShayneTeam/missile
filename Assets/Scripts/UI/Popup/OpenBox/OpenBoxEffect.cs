﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class OpenBoxEffect : MonoBehaviour
    {
        [SerializeField] private Image box;

        
        private Action _callback;

        public void Init(string boxIdx, Action callback)
        {
            // 콜백 저장
            _callback = callback;
            
            // 아이콘
            var icon = DataboxController.GetDataString(Table.Box, Sheet.Box, boxIdx, Column.icon);
            box.sprite = AtlasController.GetSprite(icon);
        }

        [PublicAPI]
        public void OnHide()
        {
            // 닫기 이벤트 들어오면 콜백 호출 
            _callback?.Invoke();
        }
    }
}