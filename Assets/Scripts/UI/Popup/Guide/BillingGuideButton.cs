﻿using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UI.Popup.Menu;
using UI.Popup.Menu.Shop;
using UnityEngine;

namespace UI.Popup.Guide
{
    public class BillingGuideButton : MonoBehaviour
    {
        [SerializeField] private string idx;
        [SerializeField] private TextMeshProUGUI rewardText;
        [SerializeField] private ShopTab wantTab;

        public void Start()
        {
            // 리워드 텍스트 초기화 
            var reward = GuideController.GetRewardValue(idx, Sheet.billing);
            var freeText = Localization.GetText("info_373");
            rewardText.text = $"{freeText} +{reward.ToLookUpString()}";
        }

        public void OnClick()
        {
            // 마킹 처리 
            GuideController.MarkGuide(idx, Sheet.billing);
					
            // 상점 탭 열기
            InGameMenuUtil.OpenShop(wantTab);
        }
    }
}