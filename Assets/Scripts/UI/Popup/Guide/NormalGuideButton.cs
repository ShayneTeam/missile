﻿using System;
using InGame;
using InGame.Controller;
using InGame.Global;
using TMPro;
using UnityEngine;

namespace UI.Popup.Guide
{
    public class NormalGuideButton : MonoBehaviour
    {
        [SerializeField] private string idx;
        [SerializeField] private TextMeshProUGUI rewardText;

        public void Start()
        {
            // 리워드 텍스트 초기화 
            var reward = GuideController.GetRewardValue(idx, Sheet.guide);
            rewardText.text = $"+{reward.ToLookUpString()}";
        }

        public void OnClick()
        {
            // 마킹 처리 
            GuideController.MarkGuide(idx, Sheet.guide);
        }
    }
}