using System.Collections.Generic;
using BackEnd;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using JetBrains.Annotations;
using LitJson;
using Server;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Option
{
    public class BlockUserPopup : MonoBehaviourSingleton<BlockUserPopup>, IEnhancedScrollerDelegate
    {
        private CommonScrollerOwner _common;
        private List<string> _allBlockUserList;
        private bool _isInit;

        public static void Show()
        {
            var popup = UIPopup.GetPopup("Block User Popup");
            popup.Show();
            popup.GetComponent<BlockUserPopup>().OnShow();
        }

        [PublicAPI]
        public void OnShow()
        {
            var blockUserList = Backend.Chat.GetBlockUserList();

            _allBlockUserList = new List<string>();
            foreach (JsonData o in blockUserList)
                _allBlockUserList.Add(o.ToString());

            _common = GetComponent<CommonScrollerOwner>();
            _common.scroller.Delegate = this;
            
            Refresh();
        }

        public void Refresh()
        {
            var blockUserList = Backend.Chat.GetBlockUserList();

            _allBlockUserList = new List<string>();
            foreach (JsonData o in blockUserList)
                _allBlockUserList.Add(o.ToString());

            _common.scroller.ReloadData();
        }

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return _allBlockUserList.Count;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return _common.cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            var row = scroller.GetCellView(_common.cellPrefab) as UnBlockCell;
            if (row == null)
                return null;

            var idx = _allBlockUserList[dataIndex];
            row.Init(idx);

            return row;
        }

        [PublicAPI]
        public void UnBlockUser(string userId)
        {
            BackEndManager.ChatUnBlock(userId);

            // 갱신
            Refresh();
        }

        [PublicAPI]
        public void OnHide()
        {
        }
    }
}