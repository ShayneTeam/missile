using System.Collections.Generic;
using DG.Tweening;
using EnhancedUI.EnhancedScroller;
using InGame;
using MEC;
using TMPro;
using UnityEngine;

namespace UI.Popup.Option.Ranking
{
    public class LoadingCell : EnhancedScrollerCellView
    {
        [SerializeField] private DOTweenAnimation tween;
        //[SerializeField] private TextMeshProUGUI loadingText;
        //[SerializeField] private float interval = 0.5f;

        //private CoroutineHandle _direction;

        //private int _dot;
        
        public void Init()
        {
            //Timing.KillCoroutines(_direction);
            //_direction = Timing.RunCoroutine(_DirectionFlow().CancelWith(gameObject));
            tween.DORestart();
        }
/*
        private IEnumerator<float> _DirectionFlow()
        {
            var initText = Localization.GetText("info_292");
            while (true)
            {
                var text = initText;
                for (; _dot < 3; _dot++)
                {
                    text += ".";
                    loadingText.text = text;
                    
                    yield return Timing.WaitForSeconds(interval);   
                }

                // 이거 없으면 무한 루프
                _dot = 0;
            }
        }*/
    }
}