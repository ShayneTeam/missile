using EnhancedUI.EnhancedScroller;
using TMPro;
using UnityEngine;

namespace UI.Popup.Option.Ranking
{
    public class RankingCell : EnhancedScrollerCellView
    {
        [SerializeField] private TextMeshProUGUI rankText;
        [SerializeField] private TextMeshProUGUI nicknameText;
        [SerializeField] private TextMeshProUGUI damageText;
        [SerializeField] private TextMeshProUGUI stageText;

        public void Init(int rank, string nickname, string damage, int stage)
        {
            rankText.text = rank.ToLookUpString();
            nicknameText.text = nickname;
            damageText.text = damage;
            stageText.text = stage.ToLookUpString();
        }
    }
}