using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using Doozy.Engine.UI;
using EnhancedScrollerDemos.Pagination;
using EnhancedUI.EnhancedScroller;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using Ludiq;
using MEC;
using Server;
using UnityEngine;
using Utils;

namespace UI.Popup.Option.Ranking
{
    [RequireComponent(typeof(CommonScrollerOwner))]
    [RequireComponent(typeof(UIPopup))]
    public class RankingPopup : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScrollerCellView loadingPrefab;
        [SerializeField] private List<TopRankerSlot> topRankersSlot;
        
        [SerializeField] private RankingCell myCell;
        private CommonScrollerOwner _common;

        private List<RankUserData> _scrollingRankers;

        [SerializeField] private int cellCountPerPage = 10;

        // 이 팝업이 재사용되기에 가능한 처리 
        // 재사용하지 않을 시, 이 처리는 무의미 함
        [Header("랭킹 데이터 리셋 주기"), SerializeField]
        private float refreshInterval;
        private float _refreshTime;

        private bool _isEndPage;
        private CoroutineHandle _requesting;
        private CoroutineHandle _loadingNewPage;


        public static void Show()
        {
            var popup = UIPopup.GetPopup("Ranking");
            popup.Show();
        }

        private void Awake()
        {
            _common = GetComponent<CommonScrollerOwner>();
        }

        [PublicAPI]
        public void OnShow()
        {
            // 내 랭킹 세팅 
            // 내 랭킹 데이터는 독립적으로 관리되므로, 항상 갱신된 값을 보여줄 수 있음 
            var myRankData = ServerRanking.Instance.GetMyRank();
            if (myRankData != null)
                myCell.Init(myRankData.rank, myRankData.nickname, ((double)myRankData.damage).ToUnitString(), myRankData.score);
            else
                myCell.Init(-1, Backend.UserNickName, InGameControlHub.My.MissileController.GetMaxDamage().ToUnitString(), UserData.My.Stage.bestStage);

            // 주기별 리셋 
            if (Time.unscaledTime > _refreshTime)
            {
                _refreshTime = Time.unscaledTime + refreshInterval;
                _scrollingRankers = null;
            }

            // 리셋된 상태일 때만 첫 페이지 랭커 데이터 요청 
            if (_scrollingRankers != null) return;

            // 랭커 요청
            _requesting = Timing.RunCoroutine(_RequestRankers().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnHide()
        {
            Timing.KillCoroutines(_requesting);
            Timing.KillCoroutines(_loadingNewPage);
        }

        private IEnumerator<float> _RequestRankers()
        {
            // 요청 전, 로딩 표시 
            LoadingIndicator.Show();

            // 넘어온 랭커 데이터 관리는 이 팝업에서 해야 함 
            // 캐싱 처리하겠답시고 다른 클래스에서 데이터를 관리하면, 이 팝업의 스크롤 페이징 처리가 꼬임 
            List<RankUserData> rankersOrigin = null;
            yield return Timing.WaitUntilDone(ServerRanking._GetRankers(cellCountPerPage, 0, resultRankers => rankersOrigin = resultRankers));
            
            if (rankersOrigin == null)
            {
                _isEndPage = true;
                LoadingIndicator.Hide();
                yield break;
            }
            
            // 동일 랭크일 경우, 최고 데미지 기준으로 정렬 
            var rankers = OrderByRankAndDamage(rankersOrigin);
        
            // 탑 랭커 세팅 
            for (var i = 0; i < topRankersSlot.Count; i++)
            {
                var slot = topRankersSlot[i];

                // 예외 사항 처리 
                if (rankers.Count <= i)
                {
                    slot.gameObject.SetActive(false);
                    continue;
                }
                else
                {
                    // 랭킹에서 탑 랭커가 없어졌다가, 다시 생기는 경우는 없을거임 
                    // 그래도 그냥 해놓는다 
                    slot.gameObject.SetActive(true);
                }

                // 랭커 데이터 요청 
                var rankerData = rankers[i];
                yield return Timing.WaitUntilDone(ServerRanking._RequestRankersTables(rankerData.gamerInDate));

                var rankUserData = UserData.Other[rankerData.gamerInDate];

                // 코스튬
                var head = rankUserData.Costume.GetEquipped(UserData.CostumeType.head);
                var skin = rankUserData.Costume.GetEquipped(UserData.CostumeType.skin);
                var body = rankUserData.Costume.GetEquipped(UserData.CostumeType.body);

                // 바주카
                rankUserData.Bazooka.GetState(out var equippedBazookaIdx, out _, out _);

                // 세팅 
                slot.Init(rankerData.rank, rankerData.nickname, head, skin, body, equippedBazookaIdx, ((double)rankerData.damage).ToUnitString(), rankerData.score);
            }
            
            // 탑 랭커 데이터까지 다 세팅된 후, 로딩 숨김
            LoadingIndicator.Hide();

            // 불러온 랭커 데이터가 탑 랭커 슬롯 갯수보단 많으면, 스크롤에 표시
            if (rankers.Count > topRankersSlot.Count)
                _scrollingRankers = rankers.GetRange(topRankersSlot.Count, rankers.Count - topRankersSlot.Count);
            else
                _scrollingRankers = new List<RankUserData>();

            // 요청한 랭커 수보다 적게 들어왔다면, 페이지 끝이라 가정 
            _isEndPage = _scrollingRankers.Count < cellCountPerPage - topRankersSlot.Count;

            // 스크롤 초기화 
            _common.scroller.Delegate = this;
            _common.scroller.scrollerScrolled = ScrollerScrolled;
            _common.scroller.ReloadData();
        }

        private static List<RankUserData> OrderByRankAndDamage(IEnumerable<RankUserData> rankersOrigin)
        {
            return rankersOrigin.OrderBy(x => x.rank).ThenByDescending(x => x.damage).ToList();
        }

        /*
         * Enhanced Scroller
         */
        #region Enhanced Scroller

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            // + 1은 로딩 셀 처리 위함
            return _isEndPage ? _scrollingRankers.Count : _scrollingRankers.Count + 1;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return _common.cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            if (dataIndex == _scrollingRankers.Count)
            {
                var loadingCell = scroller.GetCellView(loadingPrefab) as LoadingCell;
                if (loadingCell == null)
                    return null;

                loadingCell.Init();

                return loadingCell;
            }
            else
            {
                // 4 ~ 100위 랭커 스크롤 
                var rankingCell = scroller.GetCellView(_common.cellPrefab) as RankingCell;
                if (rankingCell == null)
                    return null;

                var data = _scrollingRankers[dataIndex];
                rankingCell.Init(data.rank, data.nickname, ((double)data.damage).ToUnitString(), data.score);

                return rankingCell;
            }
        }
        
        private void ScrollerScrolled(EnhancedScroller scroller, Vector2 val, float scrollPosition)
        {
            if (!(scroller.NormalizedScrollPosition >= 1f) || _loadingNewPage.IsRunning || _isEndPage) return;

            _loadingNewPage = Timing.RunCoroutine(_LoadNewPage(_scrollingRankers.Count + topRankersSlot.Count).CancelWith(gameObject), Segment.RealtimeUpdate);
        }

        #endregion
        
        private IEnumerator<float> _LoadNewPage(int pageStartIndex)
        {
            // 강제 딜레이 줘야만 무한 호출 안 됨
            yield return Timing.WaitForSeconds(1f);

            var previousLastIndex = _scrollingRankers.Count;
            
            yield return Timing.WaitUntilDone(ServerRanking._GetRankers(cellCountPerPage, pageStartIndex, (rankers) =>
            {
                if (rankers == null)
                {
                    _isEndPage = true;
                    return;
                }

                // 요청한 것보다 적게 가져왔으면 끝 페이지 
                _isEndPage = rankers.Count < cellCountPerPage;
                
                _scrollingRankers.AddRange(rankers);
                _scrollingRankers = OrderByRankAndDamage(_scrollingRankers);
                
                _common.scroller.ReloadData();

                _common.scroller.JumpToDataIndex(previousLastIndex, 1, 1);
            }));
        }
    }
}