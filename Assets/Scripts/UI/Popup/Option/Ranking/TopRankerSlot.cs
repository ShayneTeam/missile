using TMPro;
using UI.Title;
using UnityEngine;

namespace UI.Popup.Option.Ranking
{
    [RequireComponent(typeof(RankUserSlot))]
    public class TopRankerSlot : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI damageText;
        [SerializeField] private TextMeshProUGUI stageText;
        
        private RankUserSlot _rankUserSlot;

        public void Init()
        {
            
        }

        public void Init(int rank, string nickname, string headIdx, string skinIdx, string bodyIdx, string bazookaIdx, string damage, int stage)
        {
            if (_rankUserSlot == null)
                _rankUserSlot = GetComponent<RankUserSlot>();
            
            _rankUserSlot.Init(rank, nickname, headIdx, skinIdx, bodyIdx, bazookaIdx);

            // 추가 데이터
            damageText.text = damage;
            stageText.text = stage.ToLookUpString();
        }
    }
}