using System.Collections.Generic;
using Doozy.Engine.UI;
using InGame;
using JetBrains.Annotations;
using Server;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Option
{
    public class ChangeLanguagePopup : MonoBehaviour, IPopup
    {
        [SerializeField] private List<Toggle> languages;

        private string _initLang;

        public static void Show()
        {
            var popup = UIPopup.GetPopup("Change Language");
            popup.Show();
        }
        
        public void Init()
        {
            _initLang = Localization.Language;
            
            // 갱신
            Refresh();
        }

        private void Refresh()
        {
            // 현재 선택된 언어의 토글 선택 
            var lang = Localization.Language;

            foreach (var language in languages)
            {
                if (!language.name.Contains(lang)) 
                    continue;
                
                language.isOn = true;
                break;
            }
        }

        [PublicAPI]
        public void ChangeLanguage(string lang)
        {
            // 언어 변경
            Localization.Language = lang;
            
            // 채팅 채널 변경
            ServerChat.Instance.ReconnectChannel();
            
            // 갱신
            Refresh();
        }

        [PublicAPI]
        public void OnHide()
        {
            // 풀링된 팝업 전부 날려서, 다시 로컬라이징 되게 함 
            if (_initLang != Localization.Language)
            {
                UIPopupManager.DestroyAllHiddenPopups();
                UIPopupManager.RefreshAllVisiblePopups();
            }
        }
    }
}