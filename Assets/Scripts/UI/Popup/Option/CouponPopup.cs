using System;
using System.Collections.Generic;
using System.Globalization;
using BackEnd;
using Doozy.Engine.UI;
using Gpm.Common.ThirdParty.LitJson;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using Server;
using TMPro;
using UnityEngine;

namespace UI.Popup.Option
{
    [RequireComponent(typeof(UIPopup))]
    public class CouponPopup : MonoBehaviour
    {
        [SerializeField] private TMP_InputField inputField;

        public static void Show()
        {
            var popup = UIPopup.GetPopup("Coupon");
            popup.Show();
        }

        [PublicAPI]
        public void Use()
        {
            var couponCode = inputField.text;
            if (string.IsNullOrEmpty(couponCode))
            {
                MessagePopup.Show("info_209");
                return;
            }

            UseCoupon(couponCode);
        }

        private void UseCoupon(string couponCode)
        {
            LoadingIndicator.Show();
            SendQueue.Enqueue(Backend.Coupon.UseCoupon, couponCode, result =>
            {
                LoadingIndicator.Hide();
                switch (result.GetStatusCode())
                {
                    case "404": // 실패 
                        /*
                            중복이 허용되지 않는 시리얼 쿠폰을 중복 사용한 경우
                            statusCode : 404
                            message : 이미 사용되었거나, 틀린 번호입니다. not found, 이미 사용되었거나, 틀린 번호입니다.을(를) 찾을 수 없습니다
    
                            단일 쿠폰의 회수율이 100%인 경우
                            statusCode : 404
                            message : 전부 사용된 쿠폰입니다. not found, 전부 사용된 쿠폰입니다.을(를) 찾을 수 없습니다
    
                            기간 만료된 쿠폰의 경우
                            statusCode : 404
                            message : 이미 사용되었거나, 틀린 번호입니다. not found, 이미 사용되었거나, 틀린 번호입니다.을(를) 찾을 수 없습니다
    
                            한 명의 게이머가 단일 쿠폰을 두 번 이상 사용 시도한 경우
                            statusCode : 404
                            message : 이미 사용하신 쿠폰입니다. not found, 이미 사용하신 쿠폰입니다.을(를) 찾을 수 없습니다
                         */
                        // 제일 첫 문장만 메시지로 출력 
                        // 에러 코드가 전부 동일해서 분기 처리 불가 
                        if (UserData.My.UserInfo.Language == "kr")
                        {
                            var message = result.GetMessage();
                            var indexOfDot = message.IndexOf('.');
                            var subMessage = message.Substring(0, indexOfDot + 1);

                            MessagePopup.ShowString(subMessage);
                        }
                        // 한국어 아니라면
                        else
                        {
                            // 올바르지 않는 쿠폰이라고 표시 
                            MessagePopup.Show("info_210");
                        }

                        break;
                    case "200": // 성공

                        /*
                            아이템이 존재하지 않는 쿠폰을 사용한 경우
                            statusCode : 200
                            returnValue : {"uuid":613}
    
                            아이템이 존재하는 신규 쿠폰을 사용한 경우
                            statusCode : 200
                            returnValue : GetReturnValuetoJSON 신규 쿠폰 참조
    
                            아이템이 존재하는 구버전 쿠폰을 사용한 경우
                            statusCode : 200
                            returnValue : GetReturnValuetoJSON 구버전 쿠폰 참조
                        */


                        // 지급 
                        var data = result.GetReturnValuetoJSON();
                        var itemObjects = data["itemObject"];
                        if (itemObjects != null)
                        {
                            for (var i = 0; i < itemObjects.Count; i++)
                            {
                                var itemObject = itemObjects[i];

                                var item = itemObject["item"];
                                var count = itemObject["itemCount"];

                                // idx (차트에 컬럼으로 넣어줄 것)
                                var itemIdx = "";
                                if (item.ContainsKey("itemIdx"))
                                    itemIdx = item["itemIdx"].ToString();

                                // type (차트에 컬럼으로 넣어줄 것)
                                var itemType = "";
                                if (item.ContainsKey("itemType"))
                                    itemType = item["itemType"].ToString();

                                // 받은 걸 메일로 보내 주는게 정석...이자만, 그러면 또 한 번의 서버 쿼리를 날려야 한다 
                                // 그냥 즉각 지급 처리 한다 
                                var reward = new RewardData
                                {
                                    Idx = itemIdx,
                                    Type = itemType,
                                    Count = double.Parse(count.ToString(), CultureInfo.InvariantCulture)
                                };
                                RewardTransactor.Transact(reward, LogEvent.Coupon);

                                // 유저 데이터 즉각 저장
                                UserData.My.SaveToServer();

                                // 아이템 수령 팝업 
                                var rewards = new List<RewardData> { reward };
                                ReceivedRewardPopup.Show(rewards, "info_196", "info_212", "info_078");
                            }
                        }

                        // 인풋필드 지워주기 
                        inputField.text = "";
                        break;
                }
            });
        }
    }
}

/*
GetReturnValuetoJSON
// 신규 쿠폰
{
      // 쿠폰 uuid
      uuid: "400",
      // 신규 쿠폰은 2개 이상의 아이템을 쿠폰에 등록할 수 있기 때문에,
      // 리스트로 아이템이 리턴됩니다.
      itemObject : [
          {
              item: {
                   // 차트 파일 이름
                   chartFileName: "chartExample.xlsx",
                   // 아이템 정보
                   itemID: "i101",
                   itemName: "아이템1",
                   hpPower: "1",
                   num: "1"
              },
              itemCount: "1" // 지급되는 아이템 개수
          },
          ...
      ]
}
*/