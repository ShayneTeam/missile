﻿using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Global;
using UnityEngine;

namespace UI.Popup
{
    public class ProfileRow : EnhancedScrollerCellView
    {
        [SerializeField] private List<ProfileCell> cells;

        public void Init(string idx)
        {
            for (var i = 0; i < cells.Count; i++)
            {
                var cell = cells[i];
                var key = DataboxController.GetDataString(Table.Ability, Sheet.profile, idx, $"pf_line_{(i + 1).ToLookUpString()}");

                cell.Init(key);
            }
        }

        public override void RefreshCellView()
        {
            for (var i = 0; i < cells.Count; i++)
            {
                var cell = cells[i];
                cell.Refresh();
            }
        }
    }
}