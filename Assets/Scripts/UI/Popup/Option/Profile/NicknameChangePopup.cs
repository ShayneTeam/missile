﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using BackEnd.Tcp;
using Doozy.Engine.UI;
using InGame.Controller;
using JetBrains.Annotations;
using MEC;
using Server;
using TMPro;
using UnityEngine;
using Utils;

namespace UI.Popup
{
    public class NicknameChangePopup : UIPopupCommon
    {
        [SerializeField] private TMP_InputField inputField;

        [SerializeField] private ButtonSpriteSwapper button;
        [SerializeField] private TextMeshProUGUI costText;


        public static void Show()
        {
            var popup = UIPopup.GetPopup("Nickname Change");
            popup.Show();
        }

        [PublicAPI]
        public void OnShow()
        {
            // 버튼 세팅 
            var canChange = NicknameController.CanChangeNickname;
            button.Init(canChange);            
            
            // 비용
            costText.text = NicknameController.RequiredCostToChangeNickname.ToUnitString();
            costText.color = canChange ? Color.white : Color.red;
            
            // 닉네임 필터링 구현을 위해 야매로 뒤끝 채팅 필터링 기능 사용
            // 야매 구현이라 여기서 중복 코드가 발생할 수 있음 (어차피 버릴 코드)
            // 만약 뒤끝챗 접속 플로우가 실패했다면, 뒤끝챗 필터링 사용 안 함 (플로우 중단하지 않음)
            NicknameController.ReadyFiltering();
            
            // 이벤트
            NicknameController.OnChangedNickname += OnChangedNickname;
        }

        [PublicAPI]
        public void OnHide()
        {
            NicknameController.EndFiltering();
            
            // 이벤트 
            NicknameController.OnChangedNickname -= OnChangedNickname;
        }

        [PublicAPI]
        public void Change()
        {
            if (!NicknameController.CanChangeNickname)
            {
                MessagePopup.Show("info_219");
                return;
            }
            
            NicknameController.Instance.StartValidation(inputField.text);
        }

        private void OnChangedNickname(string prevNickname, string currNickname)
        {
            Popup.Hide();
            
            // 우라늄 차감 
            NicknameController.Instance.DoProcessAfterChangedNickname(prevNickname, currNickname);
            
            // 변경 성공 메시지 
            MessagePopup.Show("info_220");
        }
    }
}