﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Controller;
using InGame.Global;
using JetBrains.Annotations;
using UnityEngine;
using Utils;

namespace UI.Popup
{
    [RequireComponent(typeof(UIPopup))]
    [RequireComponent(typeof(CommonScrollerOwner))]
    public class ProfilePopup : MonoBehaviour, IEnhancedScrollerDelegate
    {
        private CommonScrollerOwner _common;
        private ICollection<string> _allIdxes;
        private bool _isInit;
        
        public static void Show()
        {
            var popup = UIPopup.GetPopup("Profile");
            popup.Show();
        }

        [PublicAPI]
        public void OnShow()
        {
            if (!_isInit)
            {
                _allIdxes = DataboxController.GetAllIdxes(Table.Ability, Sheet.profile);
                
                _common = GetComponent<CommonScrollerOwner>();

                _common.scroller.Delegate = this;
                _common.scroller.ReloadData();

                _isInit = true;
            }
            else
            {
                _common.scroller.RefreshActiveCellViews();
            }
            
            // 이벤트
            NicknameController.OnChangedNickname += OnChangedNickname;
        }

        [PublicAPI]
        public void OnHide()
        {
            // 이벤트
            NicknameController.OnChangedNickname -= OnChangedNickname;
        }

        [PublicAPI]
        public void OnClickedNickname()
        {
            NicknameChangePopup.Show();
        }

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return _allIdxes.Count;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return _common.cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            var row = scroller.GetCellView(_common.cellPrefab) as ProfileRow;
            if (row == null)
                return null;

            var idx = _allIdxes.ElementAt(dataIndex);
            row.Init(idx);

            return row;
        }

        private void OnChangedNickname(string prevNick, string currNickname)
        {
            _common.scroller.RefreshActiveCellViews();
        }
    }
}