﻿using BackEnd;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;

namespace UI.Popup
{
    public class ProfileCell : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI nameText;
        [SerializeField] private TextMeshProUGUI valueText;

        private string _key;
        public void Init(string key)
        {
            _key = key;
            
            Refresh();
        }

        public void Refresh()
        {
            switch (_key)
            {
                case PROFILE_BAZOOKA_DAMAGE:
                    nameText.text = Localization.GetText("info_111");
                    valueText.text = InGameControlHub.My.BazookaController.GetFinalizedDamage().ToUnitString();
                    break;
                case PROFILE_BAZOOKA_LEVEL:
                    UserData.My.Bazooka.GetState(out _, out _, out var level);
                    nameText.text = Localization.GetText("info_293");
                    valueText.text = level.ToLookUpString();
                    break;
                case PROFILE_BAZOOKA_GRADE:
                    UserData.My.Bazooka.GetState(out _, out var grade, out _);
                    nameText.text = Localization.GetText("info_294");
                    valueText.text = grade.ToLookUpString();
                    break;
                case PROFILE_NICKNAME:
                    nameText.text = Localization.GetText("info_203");
                    valueText.text = Backend.UserNickName;
                    break;
                case PROFILE_BAZOOKA_SPEED:
                    nameText.text = Localization.GetText("info_112");
                    valueText.text = Localization.GetFormatText("info_243", InGameControlHub.My.BazookaController.DefaultShootIntervalSec().ToString("0.000"));
                    break;
                case BAZOOKA_BASE_DAMAGE:
                    nameText.text = Localization.GetText("info_117");
                    valueText.text = Localization.GetFormatText("info_246", InGameControlHub.My.BazookaController.GetSumDamageRate().ToString());
                    break;
                case BAZOOKA_BASE_SPEED:
                    nameText.text = Localization.GetText("info_118");
                    valueText.text = Localization.GetFormatText("info_246", InGameControlHub.My.BazookaController.GetBestSpeedRate().ToString());
                    break;
                case Ability.ALL_DAMAGE_DOUBLE_POWER:
                {
                    var idx = AbilityControl.GetIdxByType(_key);
                    var value = InGameControlHub.My.AbilityController.GetAmpleDamagePer();
                    nameText.text = AbilityControl.GetNameText(idx);
                    valueText.text = AbilityControl.GetDescText(idx, value);
                }
                    break;
                default:
                {
                    var idx = AbilityControl.GetIdxByType(_key);
                    var value = InGameControlHub.My.AbilityController.GetUnifiedValue(_key);
                    nameText.text = AbilityControl.GetNameText(idx);
                    valueText.text = AbilityControl.GetDescText(idx, value);
                }
                    break;
            }
        }
        
        // ReSharper disable InconsistentNaming
        private const string PROFILE_BAZOOKA_DAMAGE = "PROFILE_BAZOOKA_DAMAGE";
        private const string PROFILE_BAZOOKA_LEVEL = "PROFILE_BAZOOKA_LEVEL";
        private const string PROFILE_BAZOOKA_GRADE = "PROFILE_BAZOOKA_GRADE";
        private const string PROFILE_NICKNAME = "PROFILE_NICKNAME";
        private const string PROFILE_BAZOOKA_SPEED = "PROFILE_BAZOOKA_SPEED";
        private const string BAZOOKA_BASE_DAMAGE = "BAZOOKA_BASE_DAMAGE";
        private const string BAZOOKA_BASE_SPEED = "BAZOOKA_BASE_SPEED";
    }
}