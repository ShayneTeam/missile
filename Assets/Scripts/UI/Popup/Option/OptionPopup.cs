﻿using System;
using BackEnd;
using Doozy.Engine.UI;
using Global;
using Global.Extensions;
using InGame;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using Server;
using TMPro;
using UI.Popup.Option.Ranking;
using UnityEngine;
using UnityEngine.Networking;
using Utils;

namespace UI.Popup.Option
{
    public class OptionPopup : UIPopupCommon, IPopup
    {
        [SerializeField] private SpriteChanger bgm;
        [SerializeField] private SpriteChanger sfx;
        [SerializeField] private SpriteChanger push;
        
        [SerializeField] private UIButton damageEffect;
        [SerializeField] private UIButton textEffect;

        [SerializeField] private TextMeshProUGUI accountId;
        
        [SerializeField] private UIButton cafeButton;


        public static void Show()
        {
            var popup = UIPopup.GetPopup("Option");
            popup.Show();
        }

        public void Init()
        {
            // BGM
            RefreshBGM();

            // SFX
            RefreshSFX();

            // Push
            RefreshPush();

            // 데미지 이펙트
            RefreshDamageEffect();
            
            // 텍스트 이펙트 
            RefreshTextEffect();
            
            // 카페
            cafeButton.gameObject.SetActive(true);

            // 계정 ID (회원번호)
            accountId.text = $"{Localization.GetFormatText("info_198", ServerMyInfo.GamerId)} ( {ServerMyInfo.NickName} )";
        }

        private void RefreshBGM()
        {
            bgm.Init(SoundController.Instance.IsOnBGM ? 0 : 1);
        }

        private void RefreshSFX()
        {
            sfx.Init(SoundController.Instance.IsOnSFX ? 0 : 1);
        }

        private void RefreshPush()
        {
            push.Init(UserData.My.UserInfo.GetFlag(UserFlags.IsOnPush, true) ? 0 : 1);
        }
        
        private void RefreshDamageEffect()
        {
            var isOn = UserData.My.UserInfo.GetFlag(UserFlags.IsOnDamageEffect, true);
            damageEffect.GetComponent<ButtonSpriteSwapper>().Init(isOn);
            damageEffect.TextMeshProLabel.text = isOn ? "ON" : "OFF";
        }
        
        private void RefreshTextEffect()
        {
            var isOn = UserData.My.UserInfo.GetFlag(UserFlags.IsOnTextEffect, true);
            textEffect.GetComponent<ButtonSpriteSwapper>().Init(isOn);
            textEffect.TextMeshProLabel.text = isOn ? "ON" : "OFF";
        }

        [PublicAPI]
        public void ShowProfile()
        {
            ProfilePopup.Show();
        }

        [PublicAPI]
        public void ShowRanking()
        {
            RankingPopup.Show();
        }

        [PublicAPI]
        public void ShowLanguage()
        {
            ChangeLanguagePopup.Show();
        }
        
        [PublicAPI]
        public void ShowBlockUser()
        {
            BlockUserPopup.Show();
        }
        
        [PublicAPI]
        public void ShowCafe()
        {
            if (ServerMyInfo.CountryCode == "KR")
            {
                Application.OpenURL("https://cafe.naver.com/missilerpg2");
            }
            else
            {
                Application.OpenURL("https://www.reddit.com/r/missileduderpg2");
            }
        }

        [PublicAPI]
        public void ShowCoupon()
        {
            CouponPopup.Show();
        }

        [PublicAPI]
        public void ToggleBgm()
        {
            SoundController.Instance.IsOnBGM = !SoundController.Instance.IsOnBGM;
            SoundController.Instance.Init();

            // 갱신
            RefreshBGM();
        }

        [PublicAPI]
        public void ToggleSfx()
        {
            SoundController.Instance.IsOnSFX = !SoundController.Instance.IsOnSFX;
            SoundController.Instance.Init();

            // 갱신
            RefreshSFX();
        }

        [PublicAPI]
        public void TogglePush()
        {
            // 토글 
            var toggle = !UserData.My.UserInfo.GetFlag(UserFlags.IsOnPush, true);
            UserData.My.UserInfo.SetFlag(UserFlags.IsOnPush, toggle);

            // 반영 
            if (toggle)
                BackEndManager.PutDeviceToken();
            else
                BackEndManager.DeleteDeviceToken();

            // 갱신
            RefreshPush();
        }
        
        [PublicAPI]
        public void ToggleDamageEffect()
        {
            // 데미지 이펙트 
            var toggle = !UserData.My.UserInfo.GetFlag(UserFlags.IsOnDamageEffect, true);
            UserData.My.UserInfo.SetFlag(UserFlags.IsOnDamageEffect, toggle);

            // 갱신
            RefreshDamageEffect();
        }
        
        [PublicAPI]
        public void ToggleTextEffect()
        {
            // 데미지 텍스트 이펙트 
            var toggle = !UserData.My.UserInfo.GetFlag(UserFlags.IsOnTextEffect, true);
            UserData.My.UserInfo.SetFlag(UserFlags.IsOnTextEffect, toggle);

            // 갱신
            RefreshTextEffect();
        }

        [PublicAPI]
        public void OnClickedGoogle()
        {
            // 구글 리더보드
#if UNITY_ANDROID
            PlayGamesController.ShowLeaderboard();
#endif
        }

        [PublicAPI]
        public void ShowPowerSaving()
        {
            if (!PowerSavingPopup.CanShow)
            {
                MessagePopup.Show("system_049");
                return;
            }
            PowerSavingPopup.Show();
        }

        [PublicAPI]
        public void ShowAsk()
        {
            var canOpenQuestionView = false;
            if (!ExtensionMethods.IsUnityEditor())
            {
#if UNITY_ANDROID
                canOpenQuestionView = BackEnd.Support.Android.Question.IsSdkVersionPossible();
#elif UNITY_IOS
                canOpenQuestionView = BackEnd.Support.iOS.Question.IsSdkVersionPossible();
#endif
            }
            
            // 언어설정이 한글이거나, 국가설정이 한국이라면, 뒤끝 문의 태움
            var isMyCountryKr = ServerMyInfo.CountryCode.Equals("kr", StringComparison.OrdinalIgnoreCase);
            var isMyLangKr = Localization.Language.Equals("kr", StringComparison.OrdinalIgnoreCase);

            // 둘 다 해당돼야 함 
            if (canOpenQuestionView && (isMyCountryKr || isMyLangKr))
            {
                var bro = Backend.Question.GetQuestionAuthorize();
                var questionAuthorize = bro.GetReturnValuetoJSON()["authorize"].ToString();

#if UNITY_ANDROID
                BackEnd.Support.Android.Question.OpenQuestionView(questionAuthorize, Backend.UserInDate, 10, 10, 10,
                    10);
#elif UNITY_IOS
                BackEnd.Support.iOS.Question.OpenQuestionView(questionAuthorize, Backend.UserInDate, 10, 10, 10,
                    10);
#endif
            }
            else
            {
                string EscapeURL(string url)
                {
                    return UnityWebRequest.EscapeURL(url).Replace("+", "%20");
                }

                var mailto = "sogonlabs@gmail.com";
                // 메일 제목 (default)
                var subject = string.Empty;
                var bodys = $@"DeviceName : {SystemInfo.deviceModel}
UserName : {Backend.UserNickName}
UserOS : {SystemInfo.operatingSystem}

================================


";
                var body = EscapeURL(bodys);
                Application.OpenURL("mailto:" + mailto + "?subject=" + subject + "&body=" + body);
            }
        }

        [PublicAPI]
        public void OnClickedLogout()
        {
            // 확인 팝업 
            YesOrNoPopup.Show("info_259", "system_043", "system_014", "system_042",
                () =>
                {
                    // 팝업 닫기 
                    Popup.Hide();
                    
                    // 로그아웃 
                    LogoutController.Instance.Logout();
                },
                () => { });
        }
    }
}