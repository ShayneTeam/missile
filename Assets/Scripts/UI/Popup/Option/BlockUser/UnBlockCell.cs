﻿using System.Collections.Generic;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Global;
using TMPro;
using UI.Popup.Option;
using UnityEngine;

namespace UI.Popup
{
    public class UnBlockCell : EnhancedScrollerCellView
    {
        private string _slotUserNickName;
        public TextMeshProUGUI nickNameText;
        
        public void Init(string userName)
        {
            _slotUserNickName = userName;
            
            nickNameText.text = _slotUserNickName;
        }

        public void UnBlock()
        {
            BlockUserPopup.Instance.UnBlockUser(_slotUserNickName);
        }
    }
}