using System;
using System.Collections.Generic;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;
using Stage = InGame.Global.Stage;

namespace UI.Popup.Option
{
    public class PowerSavingPopup : UIPopupCommon
    {
        [SerializeField] private TextMeshProUGUI stageText;
        [SerializeField] private int targetFrameRate;

        private int _prevFrameRate;
        private float _prevSaveToServerDelay;
        private Camera _mainCamera;

        private CoroutineHandle _gc;

        public static void Show()
        {
            var popup = UIPopup.GetPopup("Power Saving");
            popup.Show();
        }

        // 절전 모드는 스테이지 방치를 위해 존재 함 
        // 스테이지 모드가 아니라면 동작되면 안됨
        public static bool CanShow => Stage.Instance != null;

        [PublicAPI]
        public void OnShowStarted()
        {
            // 스테이지 텍스트 
            RefreshStageText();
            
            // 이벤트
            UserData.My.Stage.OnChangedCurrentStage += OnChangedCurrentStage;
        }

        [PublicAPI]
        public void OnShowFinished()
        {
            // 프레임
            // _prevFrameRate = Application.targetFrameRate;
            // Application.targetFrameRate = targetFrameRate;
            
            // 서버 데이터 저장 주기
            _prevSaveToServerDelay = UserData.SaveToServerDelay;
            UserData.SaveToServerDelay = FirebaseRemoteConfigController.saveToServerDelayNightMode;
            
            // 캔버스 
            var thisPopup = Popup;
            var allUICanvases = UICanvas.Database;
            foreach (var uiCanvas in allUICanvases)
            {
                // 절전 팝업이 떠있는 캔버스는 켜둠
                if (uiCanvas.CanvasName == thisPopup.CanvasName)
                    continue;
                
                // 나머지 캔버스는 다 끔 
                uiCanvas.Canvas.enabled = false;
            }
            
            // 카메라
            _mainCamera = Camera.main;
            if (_mainCamera != null)
                _mainCamera.enabled = false;
            
            // 주기적 GC 호출 
            Timing.KillCoroutines(_gc);
            _gc = Timing.RunCoroutine(_GC().CancelWith(gameObject), Segment.RealtimeUpdate);
            
            // GC 예산 메뉴버전으로 설정
            GCController.Instance.SetBudget(FirebaseRemoteConfigController.gcBudgetMenu);
        }

        [PublicAPI]
        public void OnHideStarted()
        {
            // 프레임 
            // Application.targetFrameRate = _prevFrameRate;
            
            // 딜레이가 변경되었기 때문에, 서버 저장 구문을 즉시 호출함.
            UserData.My.SaveToServer();
            
            // 서버 데이터 저장 주기
            UserData.SaveToServerDelay = _prevSaveToServerDelay;
            UserData.My.RefreshAutoSave();

            // 캔버스 
            var allUICanvases = UICanvas.Database;
            foreach (var uiCanvas in allUICanvases)
            {
                uiCanvas.Canvas.enabled = true;
            }
            
            // 카메라 
            if (_mainCamera != null)
                _mainCamera.enabled = true;
            
            // 이벤트
            UserData.My.Stage.OnChangedCurrentStage -= OnChangedCurrentStage;
            
            // GC 코루틴 제거 
            Timing.KillCoroutines(_gc);
            
            // GC 예산 롤백 
            GCController.Instance.RollbackBudget();
        }

        private IEnumerator<float> _GC()
        {
            while (true)
            {
                // 주기적으로 GC 전체 회수
                // 절전 모드 풀릴 때, 프리징 걸리는 경우가 잦아서, 절전 모드 시엔 화면 안 보이니 GC 회수 적극적으로 함
                GCController.CollectFull();
                
                // 주기
                const float interval = 30;
                yield return Timing.WaitForSeconds(interval);
            }
        }

        private void OnChangedCurrentStage(int stage)
        {
            RefreshStageText();
        }

        private void RefreshStageText()
        {
            stageText.text = Stage.Instance.GetStageTitleStr();
        }
    }
}