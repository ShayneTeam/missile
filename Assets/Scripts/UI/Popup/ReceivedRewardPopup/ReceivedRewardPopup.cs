﻿using System;
using System.Collections.Generic;
using Doozy.Engine.UI;
using InGame;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using UnityEngine;

namespace UI.Popup
{
    [RequireComponent(typeof(UIPopup))]
    public class ReceivedRewardPopup : MonoBehaviour
    {
        [SerializeField] private List<RewardCell> cells;    // 넉넉하게 에디터에 여러 개 만들어 두고, 링크 해 둠
        [SerializeField] private float intervalDelay;
        
        private List<RewardData> _rewards;

        private CoroutineHandle _direction;
        
        public static void Show(List<RewardData> rewards, string titleKey, string descKey, string okKey)
        {
            var title = Localization.GetText(titleKey);
            var desc = Localization.GetText(descKey); 
            var ok = Localization.GetText(okKey); 
            
            var popup = UIPopup.GetPopup("Received Reward");

            popup.Data.SetLabelsTexts(title, desc, ok);
            
            var receivedRewardPopup = popup.GetComponent<ReceivedRewardPopup>();
            receivedRewardPopup._rewards = rewards;

            popup.Show();
        }

        [PublicAPI]
        public void OnShow()
        {
            foreach (var cell in cells)
            {
                cell.gameObject.SetActive(false);
            }
            
            // 리워드 연출  
            Timing.KillCoroutines(_direction);
            _direction = Timing.RunCoroutine(_Direction().CancelWith(gameObject));
        }

        private IEnumerator<float> _Direction()
        {
            for (var i = 0; i < _rewards.Count; i++)
            {
                // 준비된 셀 보다 많으면 보여주지 않음 
                if (i > cells.Count - 1)
                    break;
                
                var cell = cells[i];
                var reward = _rewards[i];
                
                cell.gameObject.SetActive(true);
                cell.Init(reward);
                
                // 리워드 당 딜레이 
                yield return Timing.WaitForSeconds(intervalDelay);
            }
        }
    }
}