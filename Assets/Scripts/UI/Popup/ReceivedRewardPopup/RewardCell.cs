﻿using Global;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class RewardCell : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI countText;
        
        
        private RewardData _reward;

        public void Init(RewardData reward)
        {
            _reward = reward;
            
            // 아이콘
            var iconPath = DataboxController.GetDataString(Table.Reward, Sheet.reward, reward.Type, "icon");
            var sprite = AtlasController.GetSprite(iconPath);
            icon.SetSpriteWithOriginSIze(sprite);
            
            // 카운트
            if (countText != null)
                countText.text = RewardType.GetString(reward.Type, reward.Count);
        }
    }
}