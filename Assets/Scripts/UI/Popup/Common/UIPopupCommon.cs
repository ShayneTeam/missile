using Doozy.Engine.UI;
using UnityEngine;

namespace UI.Popup
{
    [RequireComponent(typeof(UIPopup))]
    public class UIPopupCommon : MonoBehaviour
    {
        protected UIPopup Popup;

        protected virtual void Awake()
        {
            Popup = GetComponent<UIPopup>();
        }
    }
}