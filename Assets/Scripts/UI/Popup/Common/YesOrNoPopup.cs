﻿using System;
using Doozy.Engine.UI;
using InGame;
using UnityEngine;
using UnityEngine.Events;

namespace UI.Popup
{
    public class YesOrNoPopup : MonoBehaviour
    {
        public static void Show(string titleKey, string descKey, string yesKey, string noKey, UnityAction yesCallback, UnityAction noCallback)
        {
            var title = Localization.GetText(titleKey);
            var desc = Localization.GetText(descKey); 
            var yes = Localization.GetText(yesKey); 
            var no = Localization.GetText(noKey); 
            
            ShowWithManualStr(title, desc, yes, no, yesCallback, noCallback);
        }

        public static void ShowWithManualStr(string title, string desc, string yes, string no, UnityAction yesCallback, UnityAction noCallback)
        {
            var popup = UIPopup.GetPopup("Yes or No");

            popup.Data.SetLabelsTexts(title, desc, yes, no);
            popup.Data.SetButtonsCallbacks(yesCallback, noCallback);

            popup.Show();
        }
        
        /*
         * 편의함수
         */
        public static void NoticeChipsetUse(UnityAction yesCallback)
        {
            Show("info_365", "info_366", "system_014", "system_042", yesCallback, null);
        }
    }
}