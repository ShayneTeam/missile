﻿using Doozy.Engine.UI;
using InGame;
using UnityEngine;

namespace UI.Popup
{
    public class MessagePopup : MonoBehaviour
    {
        public static void Show(string textKey)
        {
            var message = Localization.GetText(textKey); 
            
            ShowString(message);
        }
        
        public static void Show(string textFormatKey, params object[] arg)
        {
            var message = Localization.GetFormatText(textFormatKey, arg);

            ShowString(message);
        }
        
        public static void ShowString(string str)
        {
            var popup = UIPopup.GetPopup("Message");
            popup.Data.SetLabelsTexts(str);
            popup.Show();
        }
    }
}