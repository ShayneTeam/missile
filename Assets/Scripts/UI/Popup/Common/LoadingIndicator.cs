﻿using Doozy.Engine.UI;
using UnityEngine;

namespace UI.Popup
{
    public class LoadingIndicator : MonoBehaviour
    {
        public static void Show()
        {
            var popup = GetPopup();
            popup.Show();
        }
        
        public static void Hide()
        {
            var popup = GetPopup();
            popup.Hide();
        }
        
        public static bool IsShow()
        {
            var popup = GetPopup();
            return popup.IsVisible || popup.IsShowing;
        }

        private static UIPopup GetPopup()
        {
            return UIPopup.GetPopup("Loading");
        }
    }
}