﻿using System;
using Doozy.Engine.UI;
using InGame;
using UnityEngine;
using UnityEngine.Events;

namespace UI.Popup
{
    public class OkPopup : MonoBehaviour
    {
        public static void Show(string titleKey, string descKey, string okKey, UnityAction okCallback)
        {
            var title = Localization.GetText(titleKey);
            var desc = Localization.GetText(descKey); 
            var ok = Localization.GetText(okKey);

            InternalShow(title, desc, ok, okCallback);
        }
        
        public static void Alert(string title, string desc, UnityAction okCallback)
        {
            var ok = Localization.GetText("info_078"); 
            
            InternalShow(title, desc, ok, okCallback);
        }

        private static void InternalShow(string title, string desc, string ok, UnityAction okCallback)
        {
            var popup = UIPopup.GetPopup("Ok");
            
            popup.Data.SetLabelsTexts(title, desc, ok);
            popup.Data.SetButtonsCallbacks(okCallback);
            
            popup.Show();
        }
    }
}