﻿using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class BattleshipSweepPopup : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI difficultyText;
        
        [SerializeField] private TextMeshProUGUI uraniumText;
        
        [SerializeField] private Image infStoneIcon;
        [SerializeField] private TextMeshProUGUI infStoneAmountText;
        
        
        public static UIPopup Show(string battleshipIdx, int difficulty)
        {
            var popup = UIPopup.GetPopup("Battleship Sweep");
            popup.Show();

            // 초기화 
            {
                var self = popup.GetComponent<BattleshipSweepPopup>();

                // 난이도 
                self.difficultyText.text = Localization.GetFormatText("wormhole_15", difficulty.ToLookUpString());

                // 보상 
                BattleshipController.GetRewards(battleshipIdx, difficulty, out var rewardUranium, out _, out var rewardInfStoneAmount);

                // 우라늄
                self.uraniumText.text = RewardType.GetString(RewardType.REWARD_GEM, rewardUranium);
                
                // 인피니티스톤
                self.infStoneIcon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Battleship, Sheet.battleship, battleshipIdx, Column.reward_stone_icon));
                self.infStoneAmountText.text = rewardInfStoneAmount.ToLookUpString();
            }
            
            return popup;
        }
    }
}