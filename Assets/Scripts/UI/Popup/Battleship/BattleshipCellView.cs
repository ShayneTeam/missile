﻿using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Global;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class BattleshipCellView : EnhancedScrollerCellView
    {
        [SerializeField] private Image planetIcon;
        [SerializeField] private GameObject effect;
        
        private string _battleshipIdx;
	
        public void Init(string battleshipIdx)
        {
            _battleshipIdx = battleshipIdx;

            var visible = !string.IsNullOrEmpty(battleshipIdx);
		
            // 행성 아이콘
            var icon = DataboxController.GetDataString(Table.Battleship, Sheet.battleship, battleshipIdx, Column.PlanetIcon);
            planetIcon.sprite = AtlasController.GetSprite(icon);
            planetIcon.gameObject.SetActive(visible);
            
            // 갱신 
            RefreshCellView();
        }

        public override void RefreshCellView()
        {
            // 이펙트 끄기 
            SetActiveEffect(false);
        }

        public void SetActiveEffect(bool isActive)
        {
            effect.gameObject.SetActive(isActive);
        }
    }
}