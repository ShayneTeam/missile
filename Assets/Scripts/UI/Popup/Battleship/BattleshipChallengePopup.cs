﻿using System.Collections.Generic;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Battleship;
using InGame.Controller;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class BattleshipChallengePopup : UIPopupCommon
    {
        [SerializeField] private TextMeshProUGUI difficultyText;
        [SerializeField] private TextMeshProUGUI uraniumText;
        
        [SerializeField] private Image infStoneIcon;
        [SerializeField] private TextMeshProUGUI infStoneAmountText;
        
        [SerializeField] private TextMeshProUGUI ownedTicketText;
        
        [SerializeField] private TextMeshProUGUI retryCostText;
        [SerializeField] private TextMeshProUGUI nextCostText;
        
        /*
         * 코루틴
         */
        private CoroutineHandle _exitingBattleship;
        

        public static UIPopup Show()
        {
            var popup = UIPopup.GetPopup("Battleship Challenge");
            
            popup.GetComponent<BattleshipChallengePopup>().Init();
            
            popup.Show();
            
            return popup;
        }

        private void Init()
        {
            var battleshipIdx = Battleship.Instance.BattleshipIdx;
            var difficulty = Battleship.Instance.Difficulty;
            
            var hasTicket = BattleshipController.HasAnyTicket;
            
            // 난이도 
            difficultyText.text = Localization.GetFormatText("wormhole_15", difficulty.ToLookUpString());

            // 보상 
            BattleshipController.GetRewards(battleshipIdx, difficulty, out var rewardUranium, out _, out var rewardInfStoneAmount);

            // 우라늄
            uraniumText.text = RewardType.GetString(RewardType.REWARD_GEM, rewardUranium);
            
            // 보유 티켓
            ownedTicketText.text = BattleshipController.OwnedTicketText;
            
            // 인피니티스톤
            infStoneIcon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Battleship, Sheet.battleship, battleshipIdx, Column.reward_stone_icon));
            infStoneAmountText.text = rewardInfStoneAmount.ToLookUpString();
            
            // 버튼 밑 비용 텍스트 칼라 
            retryCostText.color = hasTicket ? Color.white : Color.red;
            nextCostText.color = hasTicket ? Color.white : Color.red;
        }

        private IEnumerator<float> _ExitBattleship()
        {
            /*
             * 씬 전환 시, 바로 UI 닫으면 UI 깨지는 현상 발생함
             * 그 현상 없애기 위해 만든 플로우
             */
            
            // 팝업 닫기 
            Popup.Hide();
            yield return Timing.WaitUntilDone(Popup.WaitHide());
            
            // 배틀쉽 나가기 
            BattleshipController.ExitBattleship();
        }

        [PublicAPI]
        public void Close()
        {
            // 배틀쉽 나가는 중이라면, 종료
            if (_exitingBattleship.IsRunning) return;
            
            _exitingBattleship = Timing.RunCoroutine(_ExitBattleship().CancelWith(gameObject));
        }

        [PublicAPI]
        public void Retry()
        {
            // 배틀쉽 나가는 중이라면, 종료
            if (_exitingBattleship.IsRunning) return;
            
            // 배틀쉽 가능한지 체크
            if (!BattleshipController.HasAnyTicket)
            {
                // 티켓 부족 메시지
                MessagePopup.Show("battleship_10");
                
                return;
            }

            // 재도전 
            Battleship.Instance.StartMode(Battleship.Instance.BattleshipIdx, Battleship.Instance.Difficulty);
            
            // UI 닫기 
            Popup.Hide();
        }

        [PublicAPI]
        public void Next()
        {
            // 배틀쉽 나가는 중이라면, 종료
            if (_exitingBattleship.IsRunning) return;
            
            var nextDifficulty = Battleship.Instance.Difficulty + 1;
            
            // 마지막 난이도였다면 재도전 루틴 처리
            if (!BattleshipController.IsValidDifficulty(nextDifficulty))
            {
                Retry();

                return;
            }
            
            // 배틀쉽 가능한지 체크
            if (!BattleshipController.HasAnyTicket)
            {
                // 티켓 부족 메시지
                MessagePopup.Show("battleship_10");
                
                return;
            }

            // 다음 난이도
            Battleship.Instance.StartMode(Battleship.Instance.BattleshipIdx, nextDifficulty);
            
            // UI 닫기 
            Popup.Hide();
        }
    }
}