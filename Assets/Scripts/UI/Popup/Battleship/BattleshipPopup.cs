﻿using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Battleship;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using Ludiq;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup
{
    [RequireComponent(typeof(CommonScrollerOwner))]
    public class BattleshipPopup : UIPopupCommon, IEnhancedScrollerDelegate
    {
        /*
         * UI 
         */
        [SerializeField] private TextMeshProUGUI uraniumText;
        
        [SerializeField] private Image infStoneIcon;
        [SerializeField] private TextMeshProUGUI infStoneAmountText;
        [SerializeField] private TextMeshProUGUI infStoneNameText;
        
        [SerializeField] private Image planetIcon;
        [SerializeField] private TextMeshProUGUI difficultyText;
        
        [SerializeField] private TextMeshProUGUI challengeCostText;
        
        [SerializeField] private TextMeshProUGUI sweepCostText;
        [SerializeField] private UIButton sweepButton;
        
        [SerializeField] private TextMeshProUGUI ownedTicketText;
        
        /*
         * 코루틴 
         */
        private CoroutineHandle _enteringScene;
        
        private ButtonSpriteSwapper _sweepButtonSpriteSwapper;
        private CommonScrollerOwner _scroll;
        
        private ICollection<string> _allBattleshipIdxes;
        
        private CoroutineHandle _cellScrolling;
        
        private bool _isInit;


        public static void Show()
        {
            UIPopup.GetPopup("Battleship").Show();
        }

        protected override void Awake()
        {
            base.Awake();
            
            _sweepButtonSpriteSwapper = sweepButton.GetComponent<ButtonSpriteSwapper>();
            _scroll = GetComponent<CommonScrollerOwner>();
        }

        [PublicAPI]
        public void OnShow()
        {
            // BGM
            SoundController.Instance.PlayBGM("bgm_wormhole_01");
            
            // 인게임 사운드 이펙트 음소거 설정
            SoundController.Instance.MuteBus("InGame");

            // 갱신
            Init();
            
            // 수동 GC
            GCController.CollectFull();
            
            WebLog.Instance.ThirdPartyLog("battleship_show");
        }

        private void Init()
        {
            if (!_isInit)
            {
                _allBattleshipIdxes = DataboxController.GetAllIdxes(Table.Battleship, Sheet.battleship);
                
                _scroll.scroller.Delegate = this;

                _scroll.scroller.scrollerScrolled += ScrollerScrolled;
                _scroll.scroller.scrollerScrollingChanged += ScrollerScrollingChanged;

                _scroll.scroller.ReloadData();

                _isInit = true;
            }
            
            // 첫 행성 이펙트 켜주기 
            Timing.KillCoroutines(_cellScrolling);
            _cellScrolling = Timing.RunCoroutine(_Init().CancelWith(gameObject));
            
            // UI 갱신 
            Refresh();
        }
        
        private IEnumerator<float> _Init()
        {
            // 한 프레임 대기 
            yield return Timing.WaitForOneFrame;
            
            // 현재 위치한 행성을 가운데로 이동 
            _scroll.scroller.JumpToDataIndex(UserData.My.Battleship.BattleshipCursor, 0.5f, 0.5f);

            // 한 프레임 대기 
            yield return Timing.WaitForOneFrame;
            
            // 이동한 가운데 셀 이펙트 켜기 
            _scroll.scroller.RefreshActiveCellViews();   // 다른 셀의 이펙트는 전부 끄기 
            SetEffect(_scroll.scroller.CenterDataIndex, true);
        }

        [PublicAPI]
        public void OnHide()
        {
            // BGM (씬 진입하지 않았을 때만 롤백)
            if (!BattleshipController.HasEnteredToScene)
                SoundController.Instance.RollbackBGM();

            // 인게임 사운드 이펙트 음소거 해제 
            SoundController.Instance.UnmuteBus("InGame");
            
            // 수동 GC
            GCController.CollectFull();

            WebLog.Instance.ThirdPartyLog("battleship_hide");
        }

        private void Refresh()
        {
            var battleshipIdx = BattleshipController.BattleshipIdxByCursor;
            var difficulty = UserData.My.Battleship.DifficultyCursor;

            var hasAnyTicket = BattleshipController.HasAnyTicket;
            
            // 난이도 
            difficultyText.text = difficulty.ToLookUpString();
            
            // 행성 아이콘 
            var icon = DataboxController.GetDataString(Table.Battleship, Sheet.battleship, battleshipIdx, Column.PlanetIcon);
            planetIcon.sprite = AtlasController.GetSprite(icon);
            
            // 보상 
            {
                BattleshipController.GetRewards(battleshipIdx, difficulty, out var rewardUranium, out _, out var rewardInfStoneAmount);
                
                // 우라늄
                uraniumText.text = RewardType.GetString(RewardType.REWARD_GEM, rewardUranium);
                
                // 인피니티스톤
                infStoneIcon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Battleship, Sheet.battleship, battleshipIdx, Column.reward_stone_icon));
                infStoneNameText.text = Localization.GetText(DataboxController.GetDataString(Table.Battleship, Sheet.battleship, battleshipIdx, Column.reward_stone_name));
                infStoneAmountText.text = rewardInfStoneAmount.ToLookUpString();
            }

            // 티켓
            ownedTicketText.text = BattleshipController.OwnedTicketText;
            
            // 도전 버튼
            challengeCostText.color = hasAnyTicket ? Color.white : Color.red;
            
            // 소탕 버튼
            sweepCostText.color = hasAnyTicket ? Color.white : Color.red;

            _sweepButtonSpriteSwapper.Init(BattleshipController.CanSweep(difficulty));
        }

        private IEnumerator<float> _EnterScene()
        {
            // 커튼치기 (기다리진 않음)
            CurtainScreenEffect.Instance.Show();
            
            // 동기
            BattleshipController.EnterBattleship();
            
            // 씬 변수들 세팅될 수 있도록 한 프레임 대기
            yield return Timing.WaitForOneFrame;
            
            // 씬 세팅 
            var battleshipIdx = BattleshipController.BattleshipIdxByCursor;
            var difficulty = UserData.My.Battleship.DifficultyCursor;
            Battleship.Instance.StartMode(battleshipIdx, difficulty);
            
            WebLog.Instance.AllLog("battleship_start", new Dictionary<string, object> {{"difficulty",difficulty}, {"battleshipIdx", battleshipIdx}});

            // 팝업 닫기 
            Popup.Hide();
        }

        [PublicAPI]
        public void OnClickBattleshipLeft()
        {
            // 이전 배틀쉽으로 세팅  
            var newBattleshipCursor = UserData.My.Battleship.BattleshipCursor - 1;
            if (!BattleshipController.ChangeBattleshipCursor(newBattleshipCursor)) return;
            
            ChangeBattleshipCursor(newBattleshipCursor);
        }

        [PublicAPI]
        public void OnClickBattleshipRight()
        {
            // 다음 배틀쉽으로 세팅  
            var newBattleshipCursor = UserData.My.Battleship.BattleshipCursor + 1;
            if (!BattleshipController.ChangeBattleshipCursor(newBattleshipCursor)) return;
            
            ChangeBattleshipCursor(newBattleshipCursor);
        }

        private void ChangeBattleshipCursor(int newBattleshipCursor)
        {
            // 스크롤 점프
            _scroll.scroller.JumpToDataIndex(newBattleshipCursor, 0.5f, 0.5f);
            
            // 모든 셀뷰의 이펙트 끄기  
            _scroll.scroller.RefreshActiveCellViews();

            // 가운데 셀뷰만 켜기 
            SetEffect(newBattleshipCursor, true);

            // UI 갱신
            Refresh();
        }

        [PublicAPI]
        public void OnClickDifficultyDown()
        {
            // 이전 난이도로 세팅
            var difficulty = UserData.My.Battleship.DifficultyCursor;
            if (!BattleshipController.ChangeDifficultyCursor(difficulty - 1)) return;
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");

            // UI 갱신
            Refresh();
        }

        [PublicAPI]
        public void OnClickDifficultyUp()
        {
            // 다음 난이도 세팅
            var difficulty = UserData.My.Battleship.DifficultyCursor;
            if (!BattleshipController.ChangeDifficultyCursor(difficulty + 1)) return;
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");

            // UI 갱신
            Refresh();
        }

        [PublicAPI]
        public void OnClickChallenge()
        {
            // 배틀쉽 씬 로딩 중이라면, 종료
            if (_enteringScene.IsRunning) return;
            
            // 배틀쉽 가능한지 체크
            if (!BattleshipController.HasAnyTicket)
            {
                // 티켓 부족 메시지
                MessagePopup.Show("battleship_10");
                
                return;
            }

            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");
            
            // 배틀쉽 로딩 
            _enteringScene = Timing.RunCoroutine(_EnterScene().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnClickSweep()
        {
            // 배틀쉽 씬 로딩 중이라면, 종료
            if (_enteringScene.IsRunning) return;
            
            // 배틀쉽 가능한지 체크
            if (!BattleshipController.HasAnyTicket)
            {
                // 티켓 부족 메시지
                MessagePopup.Show("battleship_10");
                
                return;
            }
            
            // 소탕 가능한 지, 체크
            var difficulty = UserData.My.Battleship.DifficultyCursor;
            if (!BattleshipController.CanSweep(difficulty))
            {
                // 소탕 불가능하다면, 메시지
                MessagePopup.Show("wormhole_20");
                
                return;
            }
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");
            
            // 소탕 처리 
            BattleshipController.Sweep(BattleshipController.BattleshipIdxByCursor, difficulty);
            
            // 소탕 팝업
            var popup = BattleshipSweepPopup.Show(BattleshipController.BattleshipIdxByCursor, difficulty);

            // 소탕 팝업 닫히면, 갱신 
            popup.HideBehavior.OnFinished.Action += go => { Refresh(); };
        }
        
        #region EnhancedScroller Handlers

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(_scroll.cellPrefab) as BattleshipCellView;
            if (cellView == null) return null;
            
            // 테이블상의 행성 인덱스 얻기 
            var battleshipIdx = _allBattleshipIdxes.ElementAt(dataIndex);

            cellView.Init(battleshipIdx);

            return cellView;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return _scroll.cellSize;
        }

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return _allBattleshipIdxes.Count;
        }


        private void ScrollerScrolled(EnhancedScroller scroller, Vector2 val, float scrollPosition)
        {
            Refresh();
        }

        private void ScrollerScrollingChanged(EnhancedScroller scroller, bool scrolling)
        {
            if (scrolling)
            {
                // 모든 셀뷰의 이펙트 끄기  
                _scroll.scroller.RefreshActiveCellViews();
            }
            else
            {
                // 스크롤이 멈출 때 커서 저장 
                UserData.My.Battleship.BattleshipCursor = scroller.CenterDataIndex;
                
                // 가운데 셀뷰만 켜기 
                SetEffect(scroller.CenterDataIndex, true);
            }
        }

        private void SetEffect(int dataIndex, bool active)
        {
            // 가운데 셀뷰만 이펙트 켜기 
            var cellView = _scroll.scroller.GetCellViewAtDataIndex(dataIndex) as BattleshipCellView;
            if (cellView == null) return;
            cellView.SetActiveEffect(active);
        }

        #endregion
    }
}