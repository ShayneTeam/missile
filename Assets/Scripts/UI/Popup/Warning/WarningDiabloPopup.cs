﻿using System;
using DG.Tweening;
using Doozy.Engine.UI;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class WarningDiabloPopup : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private Image bg;
        [SerializeField] private Image text;

        [SerializeField] private float duration;
        [SerializeField] private int loop;
        [SerializeField] private Ease ease;

        public static UIPopup Show()
        {
            var popup = UIPopup.GetPopup("Warning Diablo");
            popup.Show();

            return popup;
        }

        public void Init()
        {
            icon.DOFade(0f, duration).From().SetLoops(loop, LoopType.Yoyo).SetEase(ease);
            bg.DOFade(0f, duration).From().SetLoops(loop, LoopType.Yoyo).SetEase(ease);
            text.DOFade(0f, duration).From().SetLoops(loop, LoopType.Yoyo).SetEase(ease);
        }
    }
}