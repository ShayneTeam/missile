﻿using System;
using DG.Tweening;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class WarningBossPopup : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private Image bg;
        [SerializeField] private TextMeshProUGUI text;

        [SerializeField] private float duration;
        [SerializeField] private int loop;
        [SerializeField] private Ease ease;

        public void Init()
        {
            icon.DOFade(0f, duration).From().SetLoops(loop, LoopType.Yoyo).SetEase(ease);
            bg.DOFade(0f, duration).From().SetLoops(loop, LoopType.Yoyo).SetEase(ease);
            text.DOFade(0f, duration).From().SetLoops(loop, LoopType.Yoyo).SetEase(ease);
        }
    }
}