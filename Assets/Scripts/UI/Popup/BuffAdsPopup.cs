﻿using System;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Controller;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UI.Popup.Menu;
using UI.Popup.Menu.Shop;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
	[RequireComponent(typeof(UIPopup))]
    public class BuffAdsPopup : MonoBehaviour
    {
        [SerializeField] private Image icon;
        
        [SerializeField] private TextMeshProUGUI title;
        
        [SerializeField] private TextMeshProUGUI desc;
        
        [SerializeField] private SlicedFilledImage expGauge;
        [SerializeField] private TextMeshProUGUI expText;

        [Header("과금 유도"), SerializeField] private GameObject billingGuide;
        [SerializeField] private AutoShopCell billingGuideShopCell;

        private Action _callback;
        private UIPopup _popup;

        
        private void Awake()
        {
	        _popup = GetComponent<UIPopup>();
        }

        public static BuffAdsPopup Show()
        {
	        var popup = UIPopup.GetPopup("Buff Ads");
	        popup.Show();
	        
	        return popup.GetComponent<BuffAdsPopup>();
        }
        
        public void Init(string buffIdx, int buffTime, int currentExp, int prevLevelExp, int nextLevelExp, Action watchedAdsCallback)
        {
	        _callback = watchedAdsCallback;
	        
	        // 아이콘 
	        var spritePath = DataboxController.GetDataString(Table.Buff, Sheet.buff, buffIdx, "icon");
	        icon.sprite = AtlasController.GetSprite(spritePath);

	        // 타이틀
	        var titleLocalizationKey = DataboxController.GetDataString(Table.Buff, Sheet.buff, buffIdx, "name");
	        title.text = Localization.GetText(titleLocalizationKey);

	        // 내용 
	        var descLocalizationKey = DataboxController.GetDataString(Table.Buff, Sheet.buff, buffIdx, "desc");
	        desc.text = Localization.GetFormatText(descLocalizationKey, buffTime.ToLookUpString());

	        // 경험치 게이지 
	        ExpController.GetExpProgress(prevLevelExp, currentExp, nextLevelExp, out var ratio, out var progress, out var max);
		        
	        expText.text = currentExp >= nextLevelExp ? Localization.GetText("info_132") : $"{progress.ToLookUpString()} / {max.ToLookUpString()}";
	        
	        expGauge.fillAmount = ratio;
        }

        [PublicAPI]
        public void OnShow()
        {
	        // 과금 유도 상품 구매 시, 가림 
	        var canPurchase = ShopController.CanPurchase(billingGuideShopCell.idx, billingGuideShopCell.sheet, out _);
			billingGuide.SetActive(canPurchase);
        }
		
        public void ShowAds()
        {
	        AdsMediator.Instance.ShowVideoAds((success) =>
	        {
		        if (success)
		        {
			        _callback?.Invoke();
			        
			        // 팝업 닫음 
			        _popup.Hide();
		        }
		        else
		        {
			        // 광고 시청 실패 메시지
			        MessagePopup.Show("shop_07");
		        }
	        });
        }
        
        [PublicAPI]
        public void OpenShop()
        {
	        // 이 팝업 닫음 
	        _popup.Hide();
	        
	        // 상점 오픈
	        var menu = InGameMenu.Open(InGameMenuType.ShopMenu);
	        menu.GetComponent<ShopMenuPopup>().ChangeTab(ShopTab.Bazooka);
        }
    }
}