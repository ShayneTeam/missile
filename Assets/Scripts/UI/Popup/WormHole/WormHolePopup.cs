﻿using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using Firebase.Analytics;
using Global;
using InGame;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Data;
using InGame.Global;
using InGame.WormHole;
using JetBrains.Annotations;
using Lean.Pool;
using MEC;
using TMPro;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;
using Utils;

namespace UI.Popup
{
    public class WormHolePopup : UIPopupCommon
    {
        /*
         * UI 
         */
        [SerializeField] private Image planetIcon;
        [SerializeField] private TextMeshProUGUI difficultyText;
        [SerializeField] private TextMeshProUGUI stoneText;
        [SerializeField] private TextMeshProUGUI chipText;
        
        [SerializeField] private Image uraniumIcon;
        [SerializeField] private TextMeshProUGUI uraniumText;
        [SerializeField] private TextMeshProUGUI noticeText;
        
        [SerializeField] private TextMeshProUGUI challengeCostText;
        
        [SerializeField] private TextMeshProUGUI sweepCostText;
        [SerializeField] private UIButton sweepButton;
        
        [SerializeField] private TextMeshProUGUI ownedTicketText;
        [SerializeField] private TextMeshProUGUI ownedChipText;
        
        /*
         * 코루틴 
         */
        private CoroutineHandle _enteringWormHole;
        private ButtonSpriteSwapper _buttonSpriteSwapper;


        public static void Show()
        {
            UIPopup.GetPopup("WormHole").Show();
        }

        protected override void Awake()
        {
            base.Awake();
            
            _buttonSpriteSwapper = sweepButton.GetComponent<ButtonSpriteSwapper>();
        }

        [PublicAPI]
        public void OnShow()
        {
            // BGM
            SoundController.Instance.PlayBGM("bgm_wormhole_01");
            
            // 인게임 사운드 이펙트 음소거 설정
            SoundController.Instance.MuteBus("InGame");

            // 갱신
            Init();
            
            // 수동 GC
            GCController.CollectFull();
            
            WebLog.Instance.ThirdPartyLog("wormhole_show");
        }

        private void Init()
        {
            // UI 갱신 
            Refresh();
        }

        [PublicAPI]
        public void OnHide()
        {
            // BGM (씬 진입하지 않았을 때만 롤백)
            if (!WormHoleController.HasEnteredToScene)
                SoundController.Instance.RollbackBGM();

            // 인게임 사운드 이펙트 음소거 해제 
            SoundController.Instance.UnmuteBus("InGame");
            
            // 수동 GC
            GCController.CollectFull();

            WebLog.Instance.ThirdPartyLog("wormhole_hide");
        }

        private void Refresh()
        {
            var difficulty = UserData.My.WormHole.DifficultyCursor;
            var wormHoleIdx = WormHoleController.GetWormHoleIdxByDifficulty(difficulty);
            
            var isCleared = WormHoleController.IsCleared(difficulty);

            var isEnoughTicket = WormHoleController.HasAnyTicket();
            
            // 난이도 
            difficultyText.text = difficulty.ToLookUpString();
            
            // 행성 아이콘 
            var icon = DataboxController.GetDataString(Table.WormHole, Sheet.wormhole, wormHoleIdx, Column.PlanetIcon);
            planetIcon.sprite = AtlasController.GetSprite(icon);
            
            // 보상 
            {
                WormHoleController.GetRewards(difficulty, out var rewardStone, out var rewardUranium);

                // 지옥석 
                stoneText.text = rewardStone.ToUnitString();

                // 강화칩 
                WormHoleController.GetRewardChipRate(difficulty, out var baseChipRate, out var bonusChipRate);
                var baseStr = baseChipRate.ToString("0");
                var bonusStr = bonusChipRate.ToString("0.0");
                chipText.text = $"{baseStr}%(+{bonusStr}%)";
                
                // 우라늄
                uraniumIcon.color = isCleared ? Color.gray : Color.white;
                
                uraniumText.text = rewardUranium.ToUnitString();
                uraniumText.color = isCleared ? Color.gray : Color.white;

                // 안내 텍스트 
                noticeText.text = Localization.GetText(isCleared ? "wormhole_07" : "wormhole_06");
                noticeText.color = isCleared ? Color.gray : Color.yellow;
            }
            
            // 인벤토리
            {
                // 티켓
                ownedTicketText.text = $"x{UserData.My.Items[RewardType.REWARD_WH_TICKET].ToLookUpString()}";

                // 강화칩
                ownedChipText.text = $"x{UserData.My.Items[RewardType.REWARD_CHIPSET].ToLookUpString()}";
            }
            
            // 도전 버튼
            challengeCostText.color = isEnoughTicket ? Color.white : Color.red;
            
            // 소탕 버튼
            sweepCostText.color = isEnoughTicket ? Color.white : Color.red;
            
            var canSweep = WormHoleController.CanSweep(difficulty);
            _buttonSpriteSwapper.Init(canSweep);
        }

        private IEnumerator<float> _EnterWormHoleFlow()
        {
            // 커튼치기 (기다리진 않음)
            CurtainScreenEffect.Instance.Show();
            
            WormHoleController.EnterWormHole();
            
            /*
             * 웜홀 씬 변수들 세팅될 수 있도록 한 프레임 대기
             * WormHole.Instance가 세팅 안 되어라
             */
            yield return Timing.WaitForOneFrame;
            
            // 웜홀 세팅 
            var difficulty = UserData.My.WormHole.DifficultyCursor;
            WormHole.Instance.StartMode(difficulty);
            
            WebLog.Instance.AllLog("wormhole_start", new Dictionary<string, object> {{"stage",difficulty}});

            // 팝업 닫기 
            Popup.Hide();
        }

        [PublicAPI]
        public void OnClickDifficultyDown()
        {
            // 이전 난이도로 세팅
            var difficulty = UserData.My.WormHole.DifficultyCursor;
            if (!WormHoleController.ChangeDifficultyCursor(difficulty - 1))
                return;
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");

            // UI 갱신
            Refresh();
        }

        [PublicAPI]
        public void OnClickDifficultyUp()
        {
            // 다음 난이도 세팅
            var difficulty = UserData.My.WormHole.DifficultyCursor;
            if (!WormHoleController.ChangeDifficultyCursor(difficulty + 1))
                return;
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");

            // UI 갱신
            Refresh();
        }

        [PublicAPI]
        public void OnClickChallenge()
        {
            // 웜홀 씬 로딩 중이라면, 종료
            if (_enteringWormHole.IsRunning)
                return;
            
            // 웜홀 가능한지 체크
            if (!WormHoleController.HasAnyTicket())
            {
                // 티켓 부족 메시지
                MessagePopup.Show("wormhole_13");
                
                return;
            }

            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");
            
            // 웜홀 로딩 
            _enteringWormHole = Timing.RunCoroutine(_EnterWormHoleFlow().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnClickSweep()
        {
            // 웜홀 씬 로딩 중이라면, 종료
            if (_enteringWormHole.IsRunning)
                return;
            
            // 웜홀 가능한지 체크
            if (!WormHoleController.HasAnyTicket())
            {
                // 티켓 부족 메시지
                MessagePopup.Show("wormhole_13");
                
                return;
            }
            
            // 소탕 가능한 지, 체크
            var difficultyCursor = UserData.My.WormHole.DifficultyCursor;
            if (!WormHoleController.CanSweep(difficultyCursor))
            {
                // 소탕 불가능하다면, 메시지
                MessagePopup.Show("wormhole_20");
                
                return;
            }
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");
            
            // 소탕 처리 
            WormHoleController.Sweep(difficultyCursor, out var earnedChip);
            
            // 소탕 팝업
            var popup = WormHoleSweepPopup.Show(earnedChip);

            // 소탕 팝업 닫히면, 갱신 
            popup.HideBehavior.OnFinished.Action += go => { Refresh(); };
        }
    }
}