﻿using Doozy.Engine.UI;
using InGame;
using InGame.Controller;
using InGame.Data;
using TMPro;
using UnityEngine;

namespace UI.Popup
{
    public class WormHoleSweepPopup : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI difficultyText;
        
        [SerializeField] private TextMeshProUGUI stoneText;
        [SerializeField] private TextMeshProUGUI chipText;
        
        
        public static UIPopup Show(int earnedChip)
        {
            var popup = UIPopup.GetPopup("Sweep");
            
            popup.GetComponent<WormHoleSweepPopup>().Init(earnedChip);
            
            popup.Show();
            
            return popup;
        }

        private void Init(int earnedChip)
        {
            var difficulty = UserData.My.WormHole.DifficultyCursor;
            
            // 난이도 
            difficultyText.text = Localization.GetFormatText("wormhole_11", difficulty.ToLookUpString());
            
            // 보상 
            WormHoleController.GetRewards(difficulty, out var rewardStone, out _);
            
            // 지옥석 
            stoneText.text = rewardStone.ToUnitString();

            // 획득한 강화칩 
            chipText.text = earnedChip.ToLookUpString();
        }
    }
}