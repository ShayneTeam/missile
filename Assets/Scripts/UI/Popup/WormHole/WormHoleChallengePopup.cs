﻿using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using InGame.WormHole;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup.Menu;
using UnityEngine;

namespace UI.Popup
{
    public class WormHoleChallengePopup : UIPopupCommon
    {
        [SerializeField] private TextMeshProUGUI difficultyText;
        [SerializeField] private TextMeshProUGUI stoneText;
        [SerializeField] private TextMeshProUGUI chipText;
        [SerializeField] private TextMeshProUGUI uraniumText;
        
        [SerializeField] private GameObject uraniumInfo;
        
        [SerializeField] private TextMeshProUGUI ownedTicketText;
        [SerializeField] private TextMeshProUGUI ownedChipText;
        
        [SerializeField] private TextMeshProUGUI retryCostText;
        [SerializeField] private TextMeshProUGUI nextCostText;
        
        /*
         * 코루틴
         */
        private CoroutineHandle _exitingWormHole;
        

        public static UIPopup Show(bool isFirstClear)
        {
            var popup = UIPopup.GetPopup("Challenge");
            
            popup.GetComponent<WormHoleChallengePopup>().Init(isFirstClear);
            
            popup.Show();
            
            return popup;
        }

        private void Init(bool isFirstClear)
        {
            var difficulty = WormHole.Instance.Difficulty;
            
            var isEnoughTicket = WormHoleController.HasAnyTicket();
            
            // 난이도 
            difficultyText.text = Localization.GetFormatText("wormhole_11", difficulty.ToLookUpString());
            
            // 보상 
            WormHoleController.GetRewards(difficulty, out var rewardStone, out var rewardUranium);

            // 지옥석 
            stoneText.text = rewardStone.ToUnitString();

            // 획득한 강화칩 
            chipText.text = WormHole.Instance.EarnedChipset.ToLookUpString();
                
            // 우라늄
            if (isFirstClear)
            {
                uraniumInfo.SetActive(true);
                uraniumText.text = rewardUranium.ToUnitString();    
            }
            else
            {
                uraniumInfo.SetActive(false);
            }
            
            // 인벤토리
            {
                // 티켓
                ownedTicketText.text = $"x{UserData.My.Items[RewardType.REWARD_WH_TICKET].ToLookUpString()}";

                // 강화칩
                ownedChipText.text = $"x{UserData.My.Items[RewardType.REWARD_CHIPSET].ToLookUpString()}";
            }
            
            // 버튼 밑 비용 텍스트 칼라 
            retryCostText.color = isEnoughTicket ? Color.white : Color.red;
            nextCostText.color = isEnoughTicket ? Color.white : Color.red;
        }
        
        private IEnumerator<float> _ExitWormHoleFlow()
        {
            /*
             * 씬 전환 시, 바로 UI 닫으면 UI 깨지는 현상 발생함
             * 그 현상 없애기 위해 만든 플로우
             */
            
            // 팝업 닫기 
            Popup.Hide();
            yield return Timing.WaitUntilDone(Popup.WaitHide());
            
            // 웜홀 나가기 
            WormHoleController.ExitWormHole();
        }

        [PublicAPI]
        public void Close()
        {
            // 웜홀 나가는 중이라면, 종료
            if (_exitingWormHole.IsRunning)
                return;
            
            _exitingWormHole = Timing.RunCoroutine(_ExitWormHoleFlow().CancelWith(gameObject));
        }

        [PublicAPI]
        public void Retry()
        {
            // 웜홀 나가는 중이라면, 종료
            if (_exitingWormHole.IsRunning)
                return;
            
            // 웜홀 가능한지 체크
            if (!WormHoleController.HasAnyTicket())
            {
                // 티켓 부족 메시지
                MessagePopup.Show("wormhole_13");
                
                return;
            }

            // 재도전 
            WormHole.Instance.StartMode(WormHole.Instance.Difficulty);
            
            // UI 닫기 
            Popup.Hide();
        }

        [PublicAPI]
        public void Next()
        {
            // 웜홀 나가는 중이라면, 종료
            if (_exitingWormHole.IsRunning)
                return;
            
            var nextDifficulty = WormHole.Instance.Difficulty + 1;
            
            // 마지막 난이도였다면 재도전 루틴 처리
            if (!WormHoleController.IsValid(nextDifficulty))
            {
                Retry();

                return;
            }
            
            // 웜홀 가능한지 체크
            if (!WormHoleController.HasAnyTicket())
            {
                // 티켓 부족 메시지
                MessagePopup.Show("wormhole_13");
                
                return;
            }

            // 다음 난이도
            WormHole.Instance.StartMode(nextDifficulty);
            
            // UI 닫기 
            Popup.Hide();
        }
    }
}