﻿using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using UI.Popup.Menu.WorldMap;
using UnityEngine;
using Utils;

namespace UI.Popup
{
    public class WormHoleButton : MonoBehaviour
    {
        [SerializeField] private ImageMaterialSwapper imageMaterial;
        [SerializeField] private GameObject effect;
        
        private void Start()
        {
            //
            Refresh();
            
            //
            UserData.My.Stage.OnOwnPlanet += OnOwnPlanet;
            UserData.My.Items.OnChangedItem += Refresh;
        }

        private void OnDestroy()
        {
            //
            UserData.My.Stage.OnOwnPlanet -= OnOwnPlanet;
            UserData.My.Items.OnChangedItem -= Refresh;
        }

        private void OnOwnPlanet()
        {
            Refresh();
        }

        [PublicAPI]
        public void OnClick()
        {
            if (!WormHoleController.IsUnlocked())
            {
                // 메시지
                MessagePopup.Show("info_299");

                return;
            }
            
            WormHolePopup.Show();
        }

        private void Refresh()
        {
            // 행성조건 달성 여부에 따라 회색 메테리얼 적용 
            var isUnlocked = WormHoleController.IsUnlocked();
            imageMaterial.Init(isUnlocked);
            
            // 웜홀 티켓 
            effect.SetActive(isUnlocked && WormHoleController.HasAnyTicket());
        }
    }
}