﻿using Doozy.Engine.UI;
using JetBrains.Annotations;
using UnityEngine;

namespace UI.Popup
{
    [RequireComponent(typeof(UIPopup))]
    public class AgreementPopup : MonoBehaviour
    {
        public static AgreementPopup Show()
        {
            var popup = UIPopup.GetPopup("Agreement");
            popup.Show();

            return popup.GetComponent<AgreementPopup>();
        }

        private bool isAgreeService;
        private bool isAgreePrivacy;
        
        [PublicAPI]
        public void OnChangedAgreeService(bool isOn)
        {
            isAgreeService = isOn;
            
            if (isAgreeService && isAgreePrivacy)
                Finish();
        }
        
        [PublicAPI]
        public void OnChangedAgreePrivacy(bool isOn)
        {
            isAgreePrivacy = isOn;
            
            if (isAgreeService && isAgreePrivacy)
                Finish();
        }

        [PublicAPI]
        public void AllAgree()
        {
            Finish();
        }

        private void Finish()
        {
            GetComponent<UIPopup>().Hide();
        }
    }
}