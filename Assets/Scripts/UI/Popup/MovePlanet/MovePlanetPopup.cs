﻿using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UnityEngine;

namespace UI.Popup
{
    [RequireComponent(typeof(UIPopup))]
    public class MovePlanetPopup : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller planetsScroller;

        [SerializeField] private EnhancedScrollerCellView cellViewPrefab;

        [SerializeField] private TextMeshProUGUI stageRange;

        [SerializeField] private float cellViewSize;

        [SerializeField] private UIButton moveToSelectedButton;
        [SerializeField] private TextMeshProUGUI moveToSelectedButtonLabel;
        [SerializeField] private TextMeshProUGUI moveToSelectedButtonMessage;
        
        // 더미셀뷰 구현을 위해 더미데이터를 넣어둔 리스트
        private ICollection<string> _allPlanetIdxes;

        
        [SerializeField] private Transform boxUIParent;
        [SerializeField] private GameObject boxUIPrefab;
        private readonly List<BoxSlotWithChance> _boxChanceUIList = new List<BoxSlotWithChance>();
        private UIPopup _popup;
        private bool _isInit;
        private CoroutineHandle _cellScrolling;


        private void Awake()
        {
            _popup = GetComponent<UIPopup>();
        }

        [PublicAPI]
        public void Init()
        {
            if (!_isInit)
            {
                _allPlanetIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.planet);
                
                planetsScroller.Delegate = this;

                planetsScroller.scrollerScrolled += ScrollerScrolled;
                planetsScroller.scrollerScrollingChanged += ScrollerScrollingChanged;

                planetsScroller.ReloadData();
                
                // 박스 확률 리스트 생성 
                var allBoxIdxes = DataboxController.GetAllIdxes(Table.Box, Sheet.Box);
                foreach (var idx in allBoxIdxes)
                {
                    // 테이블 상 보여줄 애들만 보여줌 
                    if (!DataboxController.GetDataBool(Table.Box, Sheet.Box, idx, "show_ui"))
                        continue;

                    var newBoxChanceUI = GameObject.Instantiate(boxUIPrefab, boxUIParent);

                    var comp = newBoxChanceUI.GetComponent<BoxSlotWithChance>();
                    comp.Init(idx);

                    _boxChanceUIList.Add(comp);
                }

                _isInit = true;
            }
            
            // 첫 행성 이펙트 켜주기 
            Timing.KillCoroutines(_cellScrolling);
            _cellScrolling = Timing.RunCoroutine(_Init().CancelWith(gameObject));
        }

        private IEnumerator<float> _Init()
        {
            // 한 프레임 대기 
            yield return Timing.WaitForOneFrame;
            
            // 현재 위치한 행성을 가운데로 이동 
            planetsScroller.JumpToDataIndex(InGameControlHub.My.StageController.GetCurrentPlanetIndex()-1, 0.5f, 0.5f);

            // 한 프레임 대기 
            yield return Timing.WaitForOneFrame;
            
            // 이동한 가운데 셀 이펙트 켜기 
            planetsScroller.RefreshActiveCellViews();   // 다른 셀의 이펙트는 전부 끄기 
            SetEffect(planetsScroller.CenterDataIndex, true);
        }

        [PublicAPI]
        public void MoveToSelectedPlanet()
        {
            var selectedPlanetIdx = _allPlanetIdxes.ElementAt(planetsScroller.CenterDataIndex);

            // 이동 불가 시, 메시지 팝업 
            var isAvailable = InGameControlHub.My.StageController.IsAvailable(selectedPlanetIdx);
            if (!isAvailable)
            {
                MessagePopup.Show("info_085");
                return; 
            }
            
            // 점령 행성
            var isOwned = InGameControlHub.My.StageController.IsOwned(selectedPlanetIdx);
            if (isOwned)
            {
                // 행성의 첫 스테이지 이동 (어빌리티 반영) 
                if (!Stage.Instance.MoveStage(InGameControlHub.My.StageController.GetFinalizedFirstStage(selectedPlanetIdx))) return;
                
                // 반복 모드 켜기
                Stage.Instance.SetRepeat(true);
            }
            // 미점령 행성  
            else
            {
                // 행성의 첫 스테이지로 이동  
                if (!Stage.Instance.MoveStage(InGameControlHub.My.StageController.GetFirstStage(selectedPlanetIdx))) return;
                
                // 반복 모드 끄기
                Stage.Instance.SetRepeat(false);
            }
            
            // 팝업 닫기 
            _popup.Hide();
        }

        [PublicAPI]
        public void MoveToBestStage()
        {
            // 최고 스테이지가 획득한 행성이라고 반복 모드 키면 위험
            // 최악 상황 발생 시, 반복 모드를 끌 수 있는 방편이 없기 때문 (반복 모드를 서버에도 저장하기에 재접속해도 반복 됨)
            // 최고 스테이지가 게임의 마지막 스테이지이고, 마지막 행성을 점령했다면, 그때 반복모드를 돌린다
            // 까다로운 조건으로 예외처리를 해야만 함 
            StageControl.GetPlanetIdxByStage(UserData.My.Stage.bestStage, out var bestPlanetIdx);
            var bestPlanetIndex = StageControl.GetPlanetIndex(bestPlanetIdx);
            
            var isLastPlanet = StageControl.IsLastPlanet(bestPlanetIndex);
            var isOwned = InGameControlHub.My.StageController.IsOwned(bestPlanetIdx);
            if (isLastPlanet && isOwned)
            {
                // 행성의 첫 스테이지 이동 
                if (!Stage.Instance.MoveStage(InGameControlHub.My.StageController.GetFinalizedFirstStage(bestPlanetIdx))) return;
                
                // 반복 모드 켜기
                Stage.Instance.SetRepeat(true);
            }
            else
            {
                // 최고 스테이지 이동 
                if (!Stage.Instance.MoveStage(UserData.My.Stage.bestStage)) return;
                
                // 반복 모드 끄기
                Stage.Instance.SetRepeat(false);
            }

            // 팝업 닫기 
            _popup.Hide();
        }


        #region EnhancedScroller Handlers

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellViewPrefab) as MovePlanetPopupCellView;
            if (cellView == null)
                return null;
            
            // 테이블상의 행성 인덱스 얻기 
            var planetIdx = _allPlanetIdxes.ElementAt(dataIndex);

            cellView.Init(planetIdx);

            return cellView;
        }

        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return cellViewSize;
        }

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            // 행성 테이블 시트의 총 엔트리 개수 
            return _allPlanetIdxes.Count;
        }


        private void ScrollerScrolled(EnhancedScroller scroller, Vector2 val, float scrollPosition)
        {
            // 스테이지 범위 텍스트 
            var centerPlanetIdx = _allPlanetIdxes.ElementAt(scroller.CenterDataIndex);
            StageControl.GetStageMinMaxData(centerPlanetIdx, out var min, out var max);
            stageRange.text = Localization.GetFormatText("info_081", min.ToLookUpString(), max.ToLookUpString());
            
            // 이동 버튼 
            var isAvailable = InGameControlHub.My.StageController.IsAvailable(centerPlanetIdx);
            if (isAvailable)
            {
                // 이동 가능
                moveToSelectedButtonLabel.text = Localization.GetText("info_083", moveToSelectedButtonLabel.text); // 이동 하기 
                moveToSelectedButtonMessage.text = Localization.GetText("info_084", moveToSelectedButtonMessage.text); 
                moveToSelectedButton.Button.image.sprite = moveToSelectedButton.Button.spriteState.highlightedSprite;   // 이동 가능 스킨으로 변경 
            }
            else 
            {
                // 이동 불가 
                moveToSelectedButtonLabel.text = Localization.GetText("info_087", moveToSelectedButtonLabel.text); // 이동 불가
                moveToSelectedButtonMessage.text = Localization.GetText("info_086", moveToSelectedButtonMessage.text); 
                moveToSelectedButton.Button.image.sprite = moveToSelectedButton.Button.spriteState.disabledSprite;  // 이동 불가 스킨으로 변경
            }
            
            // 행성의 박스 확률 갱신 
            var planet = DataboxController.GetDataInt(Table.Stage, Sheet.planet, centerPlanetIdx, "planet", 1);
            foreach (var x in _boxChanceUIList) x.RefreshText(planet.ToLookUpString());
        }

        private void ScrollerScrollingChanged(EnhancedScroller scroller, bool scrolling)
        {
            if (scrolling)
            {
                // 모든 셀뷰의 이펙트 끄기  
                planetsScroller.RefreshActiveCellViews();
            }
            else
            {
                // 가운데 셀뷰만 켜기 
                SetEffect(scroller.CenterDataIndex, true);
            }
        }

        private void SetEffect(int dataIndex, bool active)
        {
            // 가운데 셀뷰만 이펙트 켜기 
            var cellView = planetsScroller.GetCellViewAtDataIndex(dataIndex) as MovePlanetPopupCellView;
            if (cellView != null)
                cellView.SetActiveEffect(active);
        }

        #endregion
    }
}