﻿using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class MovePlanetPopupCellView : EnhancedScrollerCellView
    {
        [SerializeField] private Image planetIcon;
        [SerializeField] private GameObject lockIcon;
        [SerializeField] private TextMeshProUGUI planetName;
        [SerializeField] private GameObject effect;
        [SerializeField] private GameObject ownedIcon;
        
        private string _planetIdx;
	
        public void Init(string planetIdx)
        {
            _planetIdx = planetIdx;

            var visible = !string.IsNullOrEmpty(planetIdx);
		
            // 행성 아이콘
            var icon = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.PlanetIcon);
            planetIcon.sprite = AtlasController.GetSprite(icon);
            planetIcon.gameObject.SetActive(visible);
            
            // 행성 이름 
            var planetNameKey = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.name);
            planetName.text = Localization.GetText(planetNameKey);
            planetName.gameObject.SetActive(visible);
            
            // 갱신 
            RefreshCellView();
        }

        public override void RefreshCellView()
        {
            // 행성 잠금 아이콘 
            var visible = !string.IsNullOrEmpty(_planetIdx);
            var isAvailable = InGameControlHub.My.StageController.IsAvailable(_planetIdx);
            lockIcon.SetActive(visible && !isAvailable);

            // 행성 점령 아이콘 
            var isOwned = InGameControlHub.My.StageController.IsOwned(_planetIdx);
            ownedIcon.SetActive(isOwned);
            
            // 이펙트 끄기 
            SetActiveEffect(false);
        }

        public void SetActiveEffect(bool isActive)
        {
            effect.gameObject.SetActive(isActive);
        }
    }
}