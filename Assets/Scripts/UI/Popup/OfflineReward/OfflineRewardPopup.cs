﻿using System;
using Databox.Dictionary;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;

namespace UI.Popup
{
    public class OfflineRewardPopup : UIPopupCommon, IEnhancedScrollerDelegate
    {
        [SerializeField] private TextMeshProUGUI mineralText;
        [SerializeField] private TextMeshProUGUI timeText;
        [SerializeField] private GameObject earnBtn;
        
        [SerializeField] private EnhancedScroller scroller;
        [SerializeField] private EnhancedScrollerCellView cellPrefab;
        [SerializeField] private float cellSize;
        
        private double _mineral;
        private OrderedDictionary<string, int> _rewardBoxes;
        private Action _earnCallback;
        private Action _earnAdsCallback;
        
        public static UIPopup Show(double mineral, OrderedDictionary<string, int> rewardBoxes, Action earnCallback, Action earnAdsCallback)
        {
            var popup = UIPopup.GetPopup("Offline Reward");
            popup.Show();

            var offlineRewardPopup = popup.GetComponent<OfflineRewardPopup>();
            offlineRewardPopup.Init(mineral, rewardBoxes, earnCallback, earnAdsCallback);

            return popup;
        }

        private void Init(double mineral, OrderedDictionary<string, int> rewardBoxes, Action earnCallback, Action earnAdsCallback)
        {
            _mineral = mineral;
            _rewardBoxes = rewardBoxes;
            _earnCallback = earnCallback;
            _earnAdsCallback = earnAdsCallback;
            
            // 광고 제거 상품 있을 시, 1배 획득 버튼 안 보이게 함 
            earnBtn.SetActive(!UserData.My.UserInfo.GetFlag(UserFlags.CanAdRemove));
        }

        [PublicAPI]
        public void OnShowStarted()
        {
            // 미네랄 
            mineralText.text = _mineral.ToUnitString();
            
            // 타임
            var offlineTime = OfflineRewardController.GetOfflineTimeMinutes();
            timeText.text = Localization.GetFormatText("system_024", offlineTime.ToLookUpString(), OfflineRewardController.GetMaxOfflineTime().ToLookUpString());

            // 스크롤러 초기화 
            scroller.Delegate = this;
            scroller.ReloadData(0.5f);  // 시작 시, 스크롤 위치가 중앙에 오도록 함 
        }

        [PublicAPI]
        public void Earn()
        {
            _earnCallback?.Invoke();
            
            Popup.Hide();
        }

        [PublicAPI]
        public void EarnAds()
        {
            AdsMediator.Instance.ShowVideoAds((success) =>
            {
                if (success)
                {
                    _earnAdsCallback?.Invoke();

                    Popup.Hide();
                }
                else
                {
                    // 광고보기 실패 시, 메시지 
                    MessagePopup.Show("shop_07");
                }
            });
        }

        #region Scroller

        public int GetNumberOfCells(EnhancedScroller _)
        {
            return _rewardBoxes.Count;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            var cellView = scroller.GetCellView(cellPrefab);
            if (cellView == null)
                return null;

            var cell = cellView.GetComponent<BoxCell>();
            var idx = _rewardBoxes.KeyOf(dataIndex);
            var count = _rewardBoxes[dataIndex];
            cell.Init(idx, count);

            return cellView;
        }

        #endregion
    }
}