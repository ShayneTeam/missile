﻿using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class BoxCell : EnhancedScrollerCellView
    {
        [SerializeField] private Image icon;
        [SerializeField] private TextMeshProUGUI countText;
        
        
        public void Init(string boxIdx, int count)
        {
            // 이미지
            icon.sprite = AtlasController.GetSprite(DataboxController.GetDataString(Table.Box, Sheet.Box, boxIdx, Column.icon));
		
            // 카운트
            countText.text = $"x{count.ToLookUpString()}";
        }
    }
}