﻿using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Mission
{
    public class CommonMissionCell : EnhancedScrollerCellView
    {
        [SerializeField] private TextMeshProUGUI nameText;

        [SerializeField] private SlicedFilledImage fill;
        [SerializeField] private TextMeshProUGUI fillText;

        [SerializeField] private GameObject button;
        
        [SerializeField] private Image rewardIcon;
        [SerializeField] private TextMeshProUGUI rewardText;

        private UserData.MissionData.CommonMissionType _type;
        private string _missionIdx;

        public void Init(UserData.MissionData.CommonMissionType type, string missionIdx)
        {
	        _type = type;
	        _missionIdx = missionIdx;

	        Refresh();
        }

        private void Refresh()
        {
	        UserData.My.Mission.GetState(_type, _missionIdx, out var currentValue, out _, out var isEarned);
	        var isRepeat = MissionController.IsRepeatCommon(_type, _missionIdx);
	        
	        // 이름
	        nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Mission, _type.ToEnumString(), _missionIdx, Column.name));

	        // 현황 텍스트 
	        var goalValue = DataboxController.GetDataInt(Table.Mission, _type.ToEnumString(), _missionIdx, Column.m_value);
	        fillText.text = Localization.GetFormatText("info_189", currentValue.ToLookUpString(), goalValue.ToLookUpString());
	        
	        // 현황 게이지 
	        fill.fillAmount = (float)currentValue / (float)goalValue;
	        
	        // 버튼 
	        if (isRepeat)
	        {
		        // 반복 미션은 버튼 무조건 보여줌 
		        button.SetActive(true);    
	        }
	        else
	        {
		        // 반복 미션 아닐 시, 획득했다면 안 보여줌 
		        button.SetActive(!isEarned);
	        }

	        button.GetComponent<ButtonSpriteSwapper>().Init(MissionController.IsFinishedCommon(_type, _missionIdx));
	        
	        // 리워드 
	        rewardText.text = MissionController.GetRewardGemCommon(_type, _missionIdx).ToLookUpString();
        }

	    [PublicAPI]
	    public void Earn()
	    {
		    if (MissionController.EarnCommonOnce(_type, _missionIdx))
		    {
			    // 메시지 
			    MessagePopup.Show("info_191", MissionController.GetRewardGemCommon(_type, _missionIdx).ToLookUpString());
		    }
	    }

	    public override void RefreshCellView()
	    {
		    Refresh();
	    }
    }
}