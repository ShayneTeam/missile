﻿using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using Utils;

namespace UI.Popup.Mission
{
    public class MissionPopup : MonoBehaviour
    {
        [SerializeField] private DailyMissionScroll daily;
        [SerializeField] private CommonMissionScroll enchant;
        [SerializeField] private CommonMissionScroll battle;
        
        [SerializeField] private ButtonSpriteSwapper collectAllButton;
        [SerializeField] private TextMeshProUGUI collectPendingCount;

        private bool _isInit;
        
        private void Awake()
        {
            UserData.My.Mission.OnChangedCommon += OnChangedCommon;
        }

        private void OnDestroy()
        {
            UserData.My.Mission.OnChangedCommon -= OnChangedCommon;
        }

        [PublicAPI]
        public void Init()
        {
            if (!_isInit)
            {
                daily.Init();
                enchant.Init();
                battle.Init();
                
                _isInit = true;
                
                // 디폴트 탭
                SwitchView("daily");
            }
            else
            {
                daily.Refresh();
                enchant.Refresh();
                battle.Refresh();
            }

            Refresh();
        }

        [PublicAPI]
        public void SwitchView(string view)
        {
            if (!_isInit)
                return;
            
            // 전환
            daily.gameObject.SetActive(view == "daily");
            battle.gameObject.SetActive(view == "battle");
            enchant.gameObject.SetActive(view == "enchant");
            
            // 갱신
            Init();
        }
        
        private void OnChangedCommon(UserData.MissionData.CommonMissionType missionType)
        {
            Refresh();
        }
        
        private void Refresh()
        {
            // 전체 획득 버튼
            collectAllButton.Init(MissionController.IsAnyFinishedMission());
            
            // 획득 가능한 미션 갯수 
            collectPendingCount.text = MissionController.GetFinishedMissionsCount().ToLookUpString();
        }
        
        [PublicAPI]
        public void CollectAll()
        {
            MissionController.CollectAllMissions();
            
            // 데일리 미션 올클리어 하는 미션 때문에 한 번 더 호출
            MissionController.CollectAllMissions();
            
            // 메시지
            MessagePopup.Show("info_362");
        }
    }
}