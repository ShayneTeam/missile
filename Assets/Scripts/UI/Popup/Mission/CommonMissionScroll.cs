﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Data;
using InGame.Global;
using UI.Popup.Menu.Relic.Diablo;
using UnityEngine;

namespace UI.Popup.Mission
{
    public class CommonMissionScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller scroller;
        [SerializeField] private EnhancedScrollerCellView cellPrefab;
        [SerializeField] private float cellSize;
        
        [SerializeField] private UserData.MissionData.CommonMissionType type;

        private ICollection<string> _allBattles;

        private void Awake()
        {
            UserData.My.Mission.OnChangedCommon += Refresh;
        }

        private void OnDestroy()
        {
            UserData.My.Mission.OnChangedCommon -= Refresh;
        }

        public void Init()
        {
            _allBattles = DataboxController.GetAllIdxes(Table.Mission, type.ToEnumString());
            
            scroller.Delegate = this;
            scroller.ReloadData();
        }

        public void Refresh()
        {
            scroller.RefreshActiveCellViews();
        }

        private void Refresh(UserData.MissionData.CommonMissionType missionType)
        {
            if (type == missionType)
                scroller.RefreshActiveCellViews();
        }
        
        public int GetNumberOfCells(EnhancedScroller _)
        {
            return _allBattles.Count;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellPrefab) as CommonMissionCell;
            if (cellView == null)
                return null;

            cellView.Init(type, _allBattles.ElementAt(dataIndex));
            
            return cellView;
        }
    }
}