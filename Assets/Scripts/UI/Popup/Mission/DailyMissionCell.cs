﻿using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using TMPro;
using UnityEngine;
using UnityEngine.UI;
using Utils;

namespace UI.Popup.Mission
{
    public class DailyMissionCell : EnhancedScrollerCellView
    {
        [SerializeField] private TextMeshProUGUI nameText;

        [SerializeField] private SlicedFilledImage fill;
        [SerializeField] private TextMeshProUGUI fillText;

        [SerializeField] private GameObject button;
        
        [SerializeField] private Image rewardIcon;
        [SerializeField] private TextMeshProUGUI rewardText;
        
        private string _dailyIdx;
        private ButtonSpriteSwapper _buttonSpriteSwapper;

        public void Init(string dailyIdx)
        {
	        _dailyIdx = dailyIdx;

	        Refresh();
        }

        private void Refresh()
        {
	        UserData.My.Mission.GetState(UserData.MissionData.CommonMissionType.daily, _dailyIdx, out var currentValue, out _, out var isEarned);
	        
	        // 이름
	        nameText.text = Localization.GetText(DataboxController.GetDataString(Table.Mission, Sheet.daily, _dailyIdx, Column.name));

	        // 현황 텍스트 
	        var goalValue = DataboxController.GetDataInt(Table.Mission, Sheet.daily, _dailyIdx, Column.m_value);

	        // 클리어 조건 충족여부 체크
	        var goldCheck = currentValue >= goalValue;
	        
	        // 클리어 했다면, 보일때 현재횟수/클리어 조건 횟수 가 아닌, 클리어 조건횟수까지만 표시되도록 함.
	        fillText.text = Localization.GetFormatText("info_189", goldCheck ? goalValue.ToLookUpString() : currentValue.ToLookUpString(), goalValue.ToLookUpString());
	        
	        // 현황 게이지 
	        fill.fillAmount = (float)currentValue / (float)goalValue;

	        // 버튼 갱신  
	        button.SetActive(!isEarned);

	        if (_buttonSpriteSwapper == null)
				_buttonSpriteSwapper = button.GetComponent<ButtonSpriteSwapper>();

	        _buttonSpriteSwapper.Init(MissionController.IsDailyFinished(_dailyIdx));
	        
	        // 리워드 
	        var reward = MissionController.GetDailyReward(_dailyIdx);
	        rewardIcon.SetSpriteWithOriginSIze(AtlasController.GetSprite(RewardTransactor.GetIcon(reward.Type)));
	        rewardText.text = RewardType.GetString(reward.Type, reward.Count);
        }

	    [PublicAPI]
	    public void Earn()
	    {
		    if (MissionController.EarnDaily(_dailyIdx))
		    {
			    var reward = MissionController.GetDailyReward(_dailyIdx);
			    var rewardName = RewardTransactor.GetName(reward.Type);
			    MessagePopup.Show("info_250", rewardName, reward.Count.ToString());
		    }
	    }

	    public override void RefreshCellView()
	    {
		    Refresh();
	    }
    }
}