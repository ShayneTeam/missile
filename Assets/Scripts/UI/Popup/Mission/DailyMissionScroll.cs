﻿using System;
using System.Collections.Generic;
using System.Linq;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Data;
using InGame.Global;
using UI.Popup.Menu.Relic.Diablo;
using UnityEngine;

namespace UI.Popup.Mission
{
    public class DailyMissionScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller scroller;
        [SerializeField] private EnhancedScrollerCellView cellPrefab;
        [SerializeField] private float cellSize;

        private ICollection<string> _allDailies;

        private void Awake()
        {
            UserData.My.Mission.OnChangedCommon += Refresh;
        }

        private void OnDestroy()
        {
            UserData.My.Mission.OnChangedCommon -= Refresh;
        }

        public void Init()
        {
            _allDailies = DataboxController.GetAllIdxes(Table.Mission, Sheet.daily);
            
            scroller.Delegate = this;
            scroller.ReloadData();
        }
        
        public void Refresh()
        {
            scroller.RefreshActiveCellViews();
        }

        private void Refresh(UserData.MissionData.CommonMissionType type)
        {
            if (type == UserData.MissionData.CommonMissionType.daily)
                scroller.RefreshActiveCellViews();
        }
        
        public int GetNumberOfCells(EnhancedScroller _)
        {
            return _allDailies.Count;
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            return cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellPrefab) as DailyMissionCell;
            if (cellView == null)
                return null;
            
            cellView.Init(_allDailies.ElementAt(dataIndex));
            
            return cellView;
        }
    }
}