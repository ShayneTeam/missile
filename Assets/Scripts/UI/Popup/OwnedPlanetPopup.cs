﻿using System.Collections.Generic;
using Global;
using InGame;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup
{
    public class OwnedPlanetPopup : MonoBehaviour
    {
        [SerializeField] private Image planetImage;
        [SerializeField] private TextMeshProUGUI planetName;

        public void Init(string planetIdx)
        {
            // 행성 아이콘
            var planetIcon = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.PlanetIcon, "planet_01");
            planetImage.sprite = AtlasController.GetSprite(planetIcon);

            // 행성 이름 
            var planetNameKey = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.name, "info_041");
            planetName.text = Localization.GetText(planetNameKey);
        }
    }
}