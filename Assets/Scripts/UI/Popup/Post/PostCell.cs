﻿using System;
using System.Globalization;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame.Global;
using JetBrains.Annotations;
using Server;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI.Popup.Post
{
    public class PostCell : EnhancedScrollerCellView
    {
        [SerializeField] private TextMeshProUGUI titleText;
        [SerializeField] private TextMeshProUGUI contentText;
        [SerializeField] private TextMeshProUGUI dateText;

        [SerializeField] private GameObject button;
        
        [SerializeField] private Image rewardIcon;
        [SerializeField] private TextMeshProUGUI rewardCountText;
        
        
        private ServerPostSchema _post;

        public void Init(ServerPostSchema post)
        {
            _post = post;
            
            RefreshCellView();
        }

        public override void RefreshCellView()
        {
            // 타이틀, 내용, 보내진 날짜
            titleText.text = _post.title;
            contentText.text = _post.content;
            dateText.text = DateTime.Parse(_post.sentDate).ToString("yyyy/MM/dd", CultureInfo.InvariantCulture);
            
            // 버튼
            button.SetActive(!_post.isReceived);

            // 아이콘
            var iconPath = DataboxController.GetDataString(Table.Reward, Sheet.reward, _post.item.itemType, Column.icon);
            rewardIcon.SetSpriteWithOriginSIze(AtlasController.GetSprite(iconPath));
            
            // 카운트 
            rewardCountText.text = RewardType.GetString(_post.item.itemType, _post.itemCount);
        }

        [PublicAPI]
        public void Receive()
        {
            LoadingIndicator.Show();
            ServerPost.Instance.Receive(_post.inDate, success => LoadingIndicator.Hide());
        }
    }
}