﻿using System;
using System.Collections.Generic;
using System.Linq;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using Server;
using TMPro;
using UI.Popup.Option.Ranking;
using UnityEngine;
using Utils;

namespace UI.Popup.Post
{
    [RequireComponent(typeof(CommonScrollerOwner))]
    [RequireComponent(typeof(UIPopup))]
    public class PostPopup : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private ButtonSpriteSwapper receiveAllButton;
        [SerializeField] private TextMeshProUGUI remainPostCount;

        [SerializeField] private GameObject noticeEmpty;
        
        private CommonScrollerOwner _common;
        
        
        
        public static void Show()
        {
            var popup = UIPopup.GetPopup("Post");
            popup.Show();
        }

        private void Awake()
        {
            _common = GetComponent<CommonScrollerOwner>();
        }

        [PublicAPI]
        public void OnShow()
        {
            // 이벤트
            ServerPost.OnReceivedPost += OnReceivedPost;
            ServerPost.OnReceivedPostAll += OnReceivedAll;
            
            // 우편은 실시간 알림 없음 (유저 우편은 있지만, 관리자 우편은 없음)
            // 그래서 매 우편 팝업 열릴 때마다 새로 가져오는 게 베스트
            LoadingIndicator.Show();
            
            ServerPost.Instance.GetPostList((success) =>
            {
                LoadingIndicator.Hide();    
                
                if (!success)
                    return;

                // 스크롤 초기화
                _common.scroller.Delegate = this;
                _common.scroller.ReloadData();
            
                // 받을 우편이 없습니다 
                noticeEmpty.SetActive(ServerPost.Instance.PostList.Count == 0);
            
                // 갱신
                Refresh();
            });
        }

        [PublicAPI]
        public void OnHide()
        {
            // 이벤트
            ServerPost.OnReceivedPost -= OnReceivedPost;
            ServerPost.OnReceivedPostAll -= OnReceivedAll;
        }

        [PublicAPI]
        public void ReceiveAll()
        {
            LoadingIndicator.Show();
            ServerPost.Instance.ReceiveAll(success => LoadingIndicator.Hide());
        }

        private void OnReceivedPost(ServerPostSchema post)
        {
            // 스크롤 갱신
            _common.scroller.RefreshActiveCellViews();
            
            // 획득 처리를 위한 리워드화
            var reward = new RewardData
            {
                Idx = post.item.itemIdx,
                Type = post.item.itemType,
                Count = post.itemCount
            };
                            
            var rewards = new List<RewardData> { reward };
            
            // 획득
            EarnRewards(rewards);
        }

        private void OnReceivedAll(List<ServerPostSchema> posts)
        {
            // 서버 우편함엔 있지만, 아직 로컬 상에서 들고 있지 않는 우편도 받아짐 
            // 서버 결과로 어떤 아이템을 수령했는지만 넘어오고, 
            // 어떤 우편이 수령됐는지는 넘어오지 않음 
            // 우편 정보가 넘어오지 않으므로, 그런 우편은 리스트로 보여줄 수 없다
            // 원래는 ReloadData() 위해 따로 이벤트화 시킨거였지만 
            // 추가되는 우편 정보가 없으므로, RefreshActiveCellViews()만 호출한다  
            _common.scroller.RefreshActiveCellViews();

            // 똑같은 아이템 보상이 여러 개 들어올 경우, 하나로 합쳐서 정리 
            var arrangedRewards = new List<RewardData>();
	        
            arrangedRewards.AddRange(posts.Where(p => p.item.itemId != null).GroupBy(p => p.item.itemId)
                .Select(group => new RewardData
                {
                    Idx = group.First().item.itemIdx,
                    Type = group.First().item.itemType,
                    Count = group.Sum(p => p.itemCount)
                }).ToList());
            
            // 획득
            EarnRewards(arrangedRewards);
        }

        private void EarnRewards(List<RewardData> rewards)
        {
            // 획득 처리 
            RewardTransactor.Transact(rewards, LogEvent.Post);
            
            // 유저 데이터 즉각 저장
            UserData.My.SaveToServer();

            // 아이템 수령 팝업 
            ReceivedRewardPopup.Show(rewards, "info_291", "info_289", "info_078");
            
            // 갱신
            Refresh();
        }

        private void Refresh()
        {
            // 전체 수령 버튼
            receiveAllButton.Init(ServerPost.Instance.HasAnyPostToReceive());
            
            // 남은 수령 가능한 우편 갯수 
            remainPostCount.text = ServerPost.Instance.GetPostCountToReceive().ToLookUpString();
        }

        #region Scrollers

        public int GetNumberOfCells(EnhancedScroller scroller)
        {
            return ServerPost.Instance.PostList.Count;
        }
        
        public float GetCellViewSize(EnhancedScroller scroller, int dataIndex)
        {
            return _common.cellSize;
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller scroller, int dataIndex, int cellIndex)
        {
            var cell = scroller.GetCellView(_common.cellPrefab) as PostCell;
            if (cell == null)
                return null;

            var post = ServerPost.Instance.PostList[dataIndex];
            cell.Init(post);

            return cell;
        }

        #endregion
    }
}