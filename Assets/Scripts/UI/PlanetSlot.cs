﻿using System.Collections;
using System.Collections.Generic;
using Global;
using InGame;
using InGame.Global;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

public class PlanetSlot : MonoBehaviour
{
    [SerializeField] private Image icon;
    [SerializeField] private TextMeshProUGUI text;
	
    public void Init(string planetIdx)
    {
	    // 행성 아이콘
	    var planetIcon = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.PlanetIcon, "planet_01");
	    icon.sprite = AtlasController.GetSprite(planetIcon);

	    // 행성 이름 
	    var planetNameKey = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.name, "info_041");
	    text.text = Localization.GetText(planetNameKey);
    }
    
}
