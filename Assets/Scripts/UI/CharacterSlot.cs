﻿using System.Collections;
using System.Collections.Generic;
using InGame;
using InGame.Controller;
using UnityEngine;
using UnityEngine.UI;

public class CharacterSlot : MonoBehaviour
{
    [SerializeField] private Image head;
    [SerializeField] private Image skin;
    [SerializeField] private Image body;
    [SerializeField] private Image set;
    
    [SerializeField] private Image bazooka;
    
    public void WearMyCostume()
    {
        // 헤드 스킨 바디 
        InGameControlHub.My.CostumeController.GetCostumeSprite(out var headSpr, out var skinSpr, out var bodySpr);
        
        // 바주카 
        var bazookaSpr = InGameControlHub.My.BazookaController.GetMySkinSprite();
        
        // 입히기
        Wear(headSpr, skinSpr, bodySpr, bazookaSpr);
    }

    public void WearCostume(string headIdx, string skinIdx, string bodyIdx, string bazookaIdx)
    {
        // 헤드 스킨 바디 
        CostumeControl.GetCostumeSprite(headIdx, skinIdx, bodyIdx, out var headSpr, out var skinSpr, out var bodySpr);
        
        // 바주카 
        var bazookaSpr = BazookaControl.GetSkinSprite(bazookaIdx);
        
        // 입히기
        Wear(headSpr, skinSpr, bodySpr, bazookaSpr);
    }

    private void Wear(Sprite headSpr, Sprite skinSpr, Sprite bodySpr, Sprite bazookaSpr)
    {
        // 코스튬 
        head.sprite = headSpr;
        skin.sprite = skinSpr;
        body.sprite = bodySpr;
        
        // 바주카 
        bazooka.SetSpriteWithOriginSIze(bazookaSpr);
    }
}
