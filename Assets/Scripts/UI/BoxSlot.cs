﻿using System.Collections.Generic;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Global;
using TMPro;
using UI.Popup;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BoxSlot : MonoBehaviour
    {
        [SerializeField] private Image image;
        [SerializeField] private TextMeshProUGUI text;
	
        private string _boxIdx;
	
	
        public void Init(string idx)
        {
	        _boxIdx = idx;

	        // 이미지 
	        var icon = DataboxController.GetDataString(Table.Box, Sheet.Box, idx, Column.icon);
	        image.sprite = AtlasController.GetSprite(icon);

	        // 
	        RefreshText();
        }
		
        private void RefreshText()
        {
	        // 텍스트 
	        var boxCount = BoxController.GetBoxCount(_boxIdx);
	        text.text = $"x{boxCount.ToLookUpString()}";
	        text.color = boxCount == 0 ? Color.red : Color.white;
        }
		
        public void Refresh()
        {
	        RefreshText();
        }

        public void OpenBox()
        {
	        // 박스 개수 0개면 진행 안함
	        if (BoxController.GetBoxCount(_boxIdx) <= 0) 
		        return;

	        ConsumeBox(_boxIdx);
	        
	        // UI 바로 갱신 
	        RefreshText();
        }

        public void ConsumeBox(string boxIdx)
        {
	        // 박스 개수 0개면 진행 안함
	        if (BoxController.GetBoxCount(boxIdx) <= 0) 
		        return;
	        
	        // 가챠 먼저 진행 
	        var boxRewards = BoxController.GachaBox(boxIdx);

	        // 사운드 
	        SoundController.Instance.PlayEffect("sfx_ui_box_open");
		        
	        // 이후 연출 진행 
	        OpenBoxEffect(boxIdx, boxRewards);
        }

        private void OpenBoxEffect(string boxIdx, IEnumerable<RewardData> boxRewards)
        {
	        // 오픈 박스 이펙트 
	        var effect = UIPopup.GetPopup("Open Box Effect");

	        var openBoxEffect = effect.GetComponent<OpenBoxEffect>();
	        openBoxEffect.Init(boxIdx, () => OpenBoxPopup(boxRewards));	// 이펙트 끝나면 팝업 띄우기 

	        effect.Show();
        }
        
        private void OpenBoxPopup(IEnumerable<RewardData> boxRewards)
        {
	        // 오픈 박스 팝업  
	        ShowRewardsPopup.Show(boxRewards);
        }
    }
}