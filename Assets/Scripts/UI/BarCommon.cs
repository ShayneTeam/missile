﻿using System;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class BarCommon : MonoBehaviour
    {
        [SerializeField] private Image fill;

        private void OnEnable()
        {
            // 다시 켜졌을 때, 최대치로 초기화 
            fill.fillAmount = 1;
        }

        public void SetFill(float amount)
        {
            fill.fillAmount = amount;
        }
    }
}