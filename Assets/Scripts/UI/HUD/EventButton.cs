using System;
using System.Collections.Generic;
using Doozy.Engine.UI;
using InGame.Controller;
using InGame.Controller.Event;
using InGame.Data;
using JetBrains.Annotations;
using Server;
using UI.Popup;
using UI.Popup.Menu;
using UnityEngine;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UIButton))]
    public class EventButton : MonoBehaviour
    {
        [SerializeField] private GameObject noticePoint;
        
        private readonly AutoLoopCall _autoUpdateEventRefreshCall = new AutoLoopCall();

        private void Start()
        {
            //
            Refresh();
            
            // 이벤트
            EventController.OnReceivedEvent += OnReceivedEvent;
            
            _autoUpdateEventRefreshCall.Start(300f, Refresh, gameObject);
        }

        private void OnDestroy()
        {
            // 이벤트
            EventController.OnReceivedEvent -= OnReceivedEvent;
        }

        private void Refresh()
        {
            // 수령 가능한 보상이 있는 이벤트 있다면 알림 표시 
            noticePoint.gameObject.SetActive(EventController.Instance.HasAnyEventToReceive());
        }
        
        private void OnReceivedEvent()
        {
            Refresh();
        }
    }
}