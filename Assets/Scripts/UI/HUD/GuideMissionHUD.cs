﻿using System;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using Ludiq;
using TMPro;
using UI.Popup;
using UI.Popup.Menu;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(NoticeIcon))]
    public class GuideMissionHUD : MonoBehaviour
    {
        [SerializeField] private GameObject effect;
        [SerializeField] private TextMeshProUGUI text;
        
        private NoticeIcon _notice;

        private void Awake()
        {
            _notice = GetComponent<NoticeIcon>();
        }

        private void Start()
        {
            Refresh();
                    
            // 이벤트 등록     
            UserData.My.Mission.OnChangedGuide += OnChangedGuide;
            UserData.My.UserInfo.OnChangedLanguage += OnChangedLanguage;
        }

        private void OnDestroy()
        {
            // 이벤트 해제
            UserData.My.Mission.OnChangedGuide -= OnChangedGuide;
            UserData.My.UserInfo.OnChangedLanguage -= OnChangedLanguage;
        }

        private void OnChangedGuide()
        {
            Refresh();
        }
        
        private void OnChangedLanguage(string lang)
        {
            Refresh();
        }

        private void Refresh()
        {
            // 가이드 미션 올클리어 시  
            if (MissionController.IsGuideAllClear)
            {
                // 꺼버림 
                gameObject.SetActive(false);
                
                return;
            }

            // 이펙트는 완료될 때만 켜줌 
            var isFinished = MissionController.IsCurrentGuideFinished;
            effect.SetActive(isFinished);
            
            _notice.notice.SetActive(isFinished);
            
            // 텍스트 
            text.text = MissionController.GetCurrentGuideText();
        }

        public void OnClick()
        {
            // 완료되지 않았다면 스킵 
            if (!MissionController.IsCurrentGuideFinished) return;
            
            // 획득 우라늄 양 
            var reward = MissionController.GetCurrentGuideReward();
            MessagePopup.Show("info_191", reward.ToLookUpString());
            
            // 획득 처리 
            MissionController.ClearCurrentGuide();
            
            // 사운드
            SoundController.Instance.PlayEffect("sfx_ui_bt_mission");
        }
    }
}