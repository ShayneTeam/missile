﻿using System;
using Doozy.Engine.UI;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using UI.Popup;
using UI.Popup.Menu;
using UI.Popup.Plunder;
using UnityEngine;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UIButton))]
    public class PlunderButton : MonoBehaviour
    {
        [SerializeField] private GameObject noticePoint;
        private ImageMaterialSwapper _imageMaterial;

        private void Awake()
        {
            _imageMaterial = GetComponent<UIButton>().Button.image.GetComponent<ImageMaterialSwapper>();
        }

        private void Start()
        {
            //
            Refresh();
            
            //
            UserData.My.Stage.OnOwnPlanet += OnOwnPlanet;
            UserData.My.Items.OnChangedItem += OnChangedItem;
        }

        private void OnDestroy()
        {
            //
            UserData.My.Stage.OnOwnPlanet -= OnOwnPlanet;
            UserData.My.Items.OnChangedItem -= OnChangedItem;
        }

        private void Refresh()
        {
            var isUnlocked = InGameControlHub.My.PlunderController.IsUnlocked();
            
            var canPlunder = InGameControlHub.My.PlunderController.HasFlag();
            var canAvenge = PlunderPopup.CanAvenge();
            
            // 행성조건 달성 여부에 따라 회색 메테리얼 적용 
            _imageMaterial.Init(isUnlocked);
            
            // 행성조건 달성 및 약탈권 있다면, 포인트 표시 
            noticePoint.gameObject.SetActive(isUnlocked && (canPlunder || canAvenge));
        }

        private void OnOwnPlanet()
        {
            Refresh();
        }

        private void OnChangedItem()
        {
            Refresh();
        }

        [PublicAPI]
        public void OpenMenu()
        {
            // 컨텐츠 미해금 시 
            if (!InGameControlHub.My.PlunderController.IsUnlocked())
            {
                // 메시지
                MessagePopup.Show("info_185");

                return;
            }
            
            InGameMenu.Open("Plunder Menu");
            
            // 눌릴 떄 마다, 혹시 모르니 갱신 처리
            Refresh();
        }

        [PublicAPI]
        public void TransitionMenu()
        {
            // 메뉴에서 약탈 버튼 눌렸을 때, 약탈 조건 선체크 하기 위한 야매 처리 함수
            
            // 컨텐츠 미해금 시
            if (!InGameControlHub.My.PlunderController.IsUnlocked())
            {
                // 메시지
                MessagePopup.Show("info_185");

                return;
            }
            
            InGameMenu.TransitionMenuExternal("Plunder Menu");
            
            // 눌릴 떄 마다, 혹시 모르니 갱신 처리
            Refresh();
        }
    }
}