﻿using System;
using System.Collections;
using System.Collections.Generic;
using Global;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup;
using UnityEngine;

namespace UI
{
    // 상태가 3개인 클래스
    // 이런 걸 볼트로 빼야 하는데, 클래스로 우겨 넣으니 이해하기 빡셈 
    public class DailyChipButton : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI progressText;
        [SerializeField] private GameObject effect;

        [Serializable]
        public class Reward
        {
            public GameObject root;
            public TextMeshProUGUI rewardText;
        }

        [SerializeField] private Reward reward;
        
        [SerializeField] private GameObject seeYouAgain;
        

        private CoroutineHandle _timer;
        
        private void Start()
        {
            // 원격에서 꺼놨으면 보여주지 않음 
            if (!FirebaseRemoteConfigController.isOnDailyChip)
            {
                gameObject.SetActive(false);
                return;
            }
            
            // 기본 초기화 
            reward.rewardText.text = DailyChipController.RewardAmount.ToLookUpString();
            
            // 갱신 
            Refresh();
            
            // 이벤트
            UserData.My.UserInfo.OnChangedLanguage += OnChangedLanguage;
        }

        private void OnDestroy()
        {
            // 이벤트
            UserData.My.UserInfo.OnChangedLanguage -= OnChangedLanguage;
        }
        
        private void OnChangedLanguage(string lang)
        {
            Refresh();
        }

        private void Refresh()
        {
            // 기본 초기화 
            effect.SetActive(DailyChipController.CanCollect);
            
            seeYouAgain.SetActive(DailyChipController.HasCollected);
            
            progressText.gameObject.SetActive(!DailyChipController.HasCollected);
            reward.root.SetActive(!DailyChipController.HasCollected);
            
            // 진행 표기 타이머 
            Timing.KillCoroutines(_timer);
            _timer = Timing.RunCoroutine(_Timer().CancelWith(gameObject), Segment.RealtimeUpdate);
        }

        private IEnumerator<float> _Timer()
        {
            // 시간 채워야 한다면
            while (DailyChipController.HasRemainTime)
            {
                // 남은 시간 표기
                var remainTime = DailyChipController.RemainTimeToCollect;
                var min = remainTime / 60;
                var sec = remainTime % 60;
                progressText.text = Localization.GetFormatText("info_374", min.ToLookUpString(), sec.ToLookUpString());

                // 1초 마다 갱신 
                yield return Timing.WaitForSeconds(1);
            }
            
            // 시간 다 채웠다면, 획득 가능 메시지     
            progressText.text = Localization.GetText("info_375");
            
            // 이펙트 켜기 
            effect.SetActive(DailyChipController.CanCollect);
        }

        [PublicAPI]
        public void OnClick()
        {
            // 오늘자 보상을 받았다면
            if (DailyChipController.HasCollected)
            {
                MessagePopup.Show("info_379");
                return;
            }
            
            // 시간 더 채워야 한다면
            if (DailyChipController.HasRemainTime)
            {
                MessagePopup.Show("info_378");
                return;
            }
            
            // 처리 
            DailyChipController.Collect();
            
            // 획득 메시지 
            MessagePopup.Show("info_377");
            
            // 갱신 (원래 이런 건 이벤트 받고 해줘야 하지만, 동기 코드니 바로 했음)
            Refresh();
        }
    }
}