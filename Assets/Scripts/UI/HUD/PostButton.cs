﻿using System;
using System.Collections.Generic;
using Doozy.Engine.UI;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using Server;
using UI.Popup;
using UI.Popup.Menu;
using UnityEngine;
using Utils;

namespace UI
{
    [RequireComponent(typeof(UIButton))]
    public class PostButton : MonoBehaviour
    {
        [SerializeField] private GameObject noticePoint;
        
        private void Start()
        {
            //
            Refresh();
            
            // 이벤트
            ServerPost.OnReceivedPost += OnReceivedPost;
            ServerPost.OnReceivedPostAll += OnReceivedAll;
        }

        private void OnDestroy()
        {
            // 이벤트
            ServerPost.OnReceivedPost -= OnReceivedPost;
            ServerPost.OnReceivedPostAll -= OnReceivedAll;
        }

        private void Refresh()
        {
            // 우편 있다면 알림 표시 
            noticePoint.gameObject.SetActive(ServerPost.Instance.HasAnyPostToReceive());
        }
        
        private void OnReceivedPost(ServerPostSchema post)
        {
            Refresh();
        }

        private void OnReceivedAll(List<ServerPostSchema> posts)
        {
            Refresh();
        }
    }
}