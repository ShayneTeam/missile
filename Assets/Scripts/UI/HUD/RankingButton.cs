using System.Collections;
using System.Collections.Generic;
using GoogleMobileAds.Api.Mediation;
using JetBrains.Annotations;
using UI.Popup.Option.Ranking;
using UnityEngine;

public class RankingButton : MonoBehaviour
{
    [PublicAPI]
    public void ShowRanking()
    {
        RankingPopup.Show();
    }
}
