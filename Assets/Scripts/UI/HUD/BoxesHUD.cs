﻿using System;
using System.Collections.Generic;
using Global;
using InGame.Data;
using InGame.Fairy;
using InGame.Global;
using UnityEngine;

namespace UI
{
    public class BoxesHUD : MonoBehaviour
    {
        [SerializeField] private Transform boxUIParent;
	
        [SerializeField] private GameObject boxUIPrefab;
	
        private readonly List<BoxSlot> _boxUIList = new List<BoxSlot>();
        
        private void Start()
        {
            /*
             * 이미 서버 데이터 싱크가 완료돼있다고 가정한다 
             * BoxController는 스테이지 전용 UI View라 가정
             * 스테이지로 넘어올 떄, 이미 데이터 싱크가 돼있어야 함 
             */
            InitUI();
            
            // 이벤트 등록
            UserData.My.Boxes.OnChangedBox += RefreshUI;
        }

        private void OnDestroy()
        {
            // 이벤트 해제 
            UserData.My.Boxes.OnChangedBox -= RefreshUI;
        }

        private void InitUI()
        {
            var allBoxIdxes = DataboxController.GetAllIdxes(Table.Box, Sheet.Box);
            foreach (var idx in allBoxIdxes)
            {
                // 테이블 상 보여줄 애들만 보여줌 
                if (!DataboxController.GetDataBool(Table.Box, Sheet.Box, idx, "show_ui"))
                    continue;
                
                var newBoxUI = GameObject.Instantiate(boxUIPrefab, boxUIParent);
			
                var boxUI = newBoxUI.GetComponent<BoxSlot>();
                boxUI.Init(idx);
			
                _boxUIList.Add(boxUI);  
            }
        }

        private void RefreshUI()
        {
            foreach (var x in _boxUIList) x.Refresh();
        }
    }
}