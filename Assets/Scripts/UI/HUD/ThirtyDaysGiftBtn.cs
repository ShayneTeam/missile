﻿using System;
using System.Collections;
using System.Collections.Generic;
using InGame;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup;
using UI.Popup.Menu.Event;
using UnityEngine;

namespace UI
{
    public class ThirtyDaysGiftBtn : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI hudText;
        [SerializeField] private GameObject effect;

        private CoroutineHandle _autoRefresh;
        
        private void Start()
        {
            // 주기 호출 전, 즉각 갱신
            Refresh();
            
            // 타이머 코루틴을 두는 것보다, 그냥 1초마다 Refresh 돌리는 게 심플한 구현이라 이렇게 함 
            _autoRefresh = Timing.CallPeriodically(float.MaxValue,1f, Refresh, Segment.RealtimeUpdate);
        }

        private void OnDestroy()
        {
            // 자동 갱신 종료 
            Timing.KillCoroutines(_autoRefresh);
        }

        private void Refresh()
        {
            var canPurchase = ThirtyDaysGiftController.CanPurchase;
            var canCollect = ThirtyDaysGiftController.CanCollect;
            
            // 이펙트
            effect.SetActive(canCollect);
            
            // 구매 가능
            if (canPurchase)
            {
                // 30일  
                hudText.text = Localization.GetText("shop_40");
            }
            // 획득 가능 
            else if (canCollect)
            {
                hudText.text = Localization.GetText("shop_41");
            }
            // 구매 & 획득 둘 다 못하는 상태 
            else
            {
                // 대기 시간 출력
                hudText.text = ThirtyDaysGiftController.GetRemainTimeTextToCollectNext();
            }
        }

        [PublicAPI]
        public void OnClick()
        {
            // 이벤트 - 30일의 선물 탭으로 이동 
            EventMenuPopup.Show(EventTab.ThirtyDaysGift);
        }
    }
}