﻿using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using UnityEngine;
using Utils;

namespace UI.Popup
{
    public class BattleshipButton : MonoBehaviour
    {
        [SerializeField] private ImageMaterialSwapper imageMaterial;
        [SerializeField] private GameObject notice;
        
        private void Start()
        {
            //
            Refresh();
            
            //
            UserData.My.Stage.OnOwnPlanet += OnOwnPlanet;
            UserData.My.Items.OnChangedItem += Refresh;
        }

        private void OnDestroy()
        {
            //
            UserData.My.Stage.OnOwnPlanet -= OnOwnPlanet;
            UserData.My.Items.OnChangedItem -= Refresh;
        }

        private void OnOwnPlanet()
        {
            Refresh();
        }

        [PublicAPI]
        public void OnClick()
        {
            if (!BattleshipController.IsUnlocked)
            {
                // 메시지
                MessagePopup.Show("info_420");

                return;
            }
            
            BattleshipPopup.Show();
        }

        private void Refresh()
        {
            // 행성조건 달성 여부에 따라 회색 메테리얼 적용 
            var isUnlocked = BattleshipController.IsUnlocked;
            imageMaterial.Init(isUnlocked);
            
            // 티켓 보유하고 있다면 레드닷
            var hasTicket = BattleshipController.HasAnyTicket;
            notice.SetActive(isUnlocked && hasTicket);
        }
    }
}