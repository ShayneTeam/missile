﻿using InGame.Fairy;
using UnityEngine;
using UnityEngine.EventSystems;
using Debug = System.Diagnostics.Debug;

namespace UI
{
    public class TouchZone : MonoBehaviour, IPointerDownHandler, IPointerClickHandler
    {
        [SerializeField] private Canvas canvas;

        [SerializeField] private LayerMask wantLayer;
        
        
        void Start()
        {
        
        }

    
    
        public void OnPointerDown(PointerEventData eventData)
        {
            //Army.Instance.ShootCharacter();
        
            //UIView.ShowView("Main", "Test Random Costume");
        }


        public void OnPointerClick(PointerEventData eventData)
        {
            if (Camera.main == null)
                return;

            // 터치되서 넘어온 스크린 좌표를 월드 좌표로 변환 
            var touchedWorldPoint = Camera.main.ScreenToWorldPoint(eventData.position);

            // 해당 월드 좌표에 위치한 타겟 찾아내기 
            var hit = Physics2D.Raycast(touchedWorldPoint, Vector2.zero, 0.1f, wantLayer);
            if (hit.collider == null)
                return;

            var touchable = hit.collider.gameObject.GetComponentInParent<ITouchable>();
            touchable?.OnTouched();
        }
    }
}
