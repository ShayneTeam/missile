﻿using System;
using Server;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(ChatView))]
    public class ChatViewRaid : MonoBehaviour
    {
        private ChatView _chatView;

        private void Awake()
        {
            _chatView = GetComponent<ChatView>();
        }

        private void Start()
        {
            _chatView.Init(ServerRaid.ChatLogs);
            
            ServerRaid.OnChat += OnChat;
        }

        private void OnDestroy()
        {
            ServerRaid.OnChat -= OnChat;
        }

        private void OnChat()
        {
            _chatView.Refresh();
        }

        public void SendChat()
        {
            var message = _chatView.InputField.text;
            if (string.IsNullOrEmpty(message) || string.IsNullOrWhiteSpace(message))
                return;
            
            // 보내기 
            ServerRaid.SendChat(message);
            
            // 인풋필드 날리기 
            _chatView.InputField.text = "";
        }
    }
}