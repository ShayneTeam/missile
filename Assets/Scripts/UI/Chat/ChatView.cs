﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using DG.Tweening;
using Global;
using InGame;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using Server;
using TMPro;
using UnityEngine;
using Utils;

namespace UI
{
    public class ChatView : MonoBehaviour
    {
        [SerializeField] private RectTransform resizeRoot;
        [SerializeField] private RectTransform resizeTarget;

        [SerializeField] private float duration = 0.5f;

        [Header("확장 전의 보여줄 오브젝트 모음"), SerializeField] private GameObject beforeExpand;
        [Header("확장 후의 보여줄 오브젝트 모음"), SerializeField] private GameObject afterExpand;

        [Header("채팅 필드"), SerializeField] private TMP_InputField inputField;
        public TMP_InputField InputField => inputField;
        
        [SerializeField] private ChatScroll chatScroll;

        [SerializeField] private TextMeshProUGUI recentChatText;

        private Vector2 _originOffsetMax;
        
        private IEnumerable<ChatMessageSchema> _chatLogs;
        

        private void Start()
        {
            _originOffsetMax = resizeRoot.offsetMax;

            RefreshRecentChatText();
        }

        public void Init(IEnumerable<ChatMessageSchema> logs)
        {
            _chatLogs = logs;
        }

        public void Refresh()
        {
            RefreshRecentChatText();

            chatScroll.Refresh();
        }

        private void RefreshRecentChatText()
        {
            if (_chatLogs == null || !_chatLogs.Any()) return;
            
            var recentChat = _chatLogs.Last();
            recentChatText.text = ConvertToChatText(recentChat);
        }

        [PublicAPI]
        public void OnClickExpand()
        {
            Timing.RunCoroutine(ExpandFlow().CancelWith(gameObject));
        }

        [PublicAPI]
        public void OnClickReduce()
        {
            Timing.RunCoroutine(ReduceFlow().CancelWith(gameObject));
        }

        private IEnumerator<float> ExpandFlow()
        {
            // 버튼 가리기
            beforeExpand.SetActive(false);

            //
            var wantedOffsetMax = resizeTarget.offsetMax;

            DOTween.To(() => resizeRoot.offsetMax, x => resizeRoot.offsetMax = x, wantedOffsetMax, duration);

            yield return Timing.WaitForSeconds(duration);

            // 연출 다 끝나고, 버튼 보여주기
            afterExpand.SetActive(true);
            
            // 1프레임 쉬고, 스크롤 갱신 (스크롤러가 최초의 꺼졌다가 커진 경우, Awake 이후에 ReloadData가 불러져야 함)
            yield return Timing.WaitForOneFrame;
            
            chatScroll.Init(_chatLogs);
        }

        private IEnumerator<float> ReduceFlow()
        {
            // 버튼 가리기
            afterExpand.SetActive(false);
            
            //
            var wantedOffsetMax = _originOffsetMax;

            DOTween.To(() => resizeRoot.offsetMax, x => resizeRoot.offsetMax = x, wantedOffsetMax, duration);

            yield return Timing.WaitForSeconds(duration);

            // 연출 다 끝나고, 버튼 보여주기
            beforeExpand.SetActive(true);

            // 최근 채팅 텍스트 표시 
            RefreshRecentChatText();
        }


        public static string ConvertToChatText(ChatMessageSchema chatLog)
        {
            var messageFormat = string.Empty;

            // 공지가 아니면서 닉네임이 운영자가 사용하는 닉네임 키워드와 일치한다면
            if (!chatLog.isNotice && chatLog.nickname.Contains(FirebaseRemoteConfigController.gmNickNameKeyword))
            {
                messageFormat = $@"{FirebaseRemoteConfigController.gmChatMessageColor.Replace("{0}",$"{chatLog.nickname} : {chatLog.message}")}";
            }
            else
            {
                messageFormat = $"{GetNicknameText(chatLog)} : {GetMessageText(chatLog)}";
            }
            
            return messageFormat;
        }

        private static string GetMessageText(ChatMessageSchema chatLog)
        {
            if (chatLog.isNotice)
            {
                return chatLog.message;
            }
            else
            {
                var appliedNoParse = $"<noparse>{chatLog.message}</noparse>";

                // 내 메시지는 따로 색깔 처리
                if (chatLog.gamerInDate == Backend.UserInDate)
                    return Localization.GetFormatText("chat_message_me", appliedNoParse);
                else
                    return appliedNoParse;
            }
        }

        private static string GetNicknameText(ChatMessageSchema chatLog)
        {
            if (chatLog.isNotice)
            {
                return Localization.GetFormatText("chat_notice", chatLog.nickname);
            }
            else
            {
                var allIdxes = DataboxController.GetAllIdxes(Table.Chat, Sheet.chat);
                foreach (var idx in allIdxes)
                {
                    var rankMin = DataboxController.GetDataInt(Table.Chat, Sheet.chat, idx, Column.rank_min);
                    var rankMax = DataboxController.GetDataInt(Table.Chat, Sheet.chat, idx, Column.rank_max);

                    if (rankMin <= chatLog.rank && chatLog.rank <= rankMax)
                    {
                        var format = DataboxController.GetDataString(Table.Chat, Sheet.chat, idx, Column.chat_id);
                        return Localization.GetFormatText(format, chatLog.rank.ToLookUpString(), chatLog.nickname);
                    }
                }
            }

            // 아무것도 없으면 그냥 닉네임 반환
            return chatLog.nickname;
        }
    }
}