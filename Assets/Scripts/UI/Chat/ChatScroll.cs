﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using Global.Extensions;
using InGame;
using InGame.Global;
using Ludiq;
using MEC;
using Server;
using TMPro;
using UI.Popup;
using UI.Popup.Menu.Relic.Normal;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{    
    public class ChatScroll : MonoBehaviour, IEnhancedScrollerDelegate
    {
        [SerializeField] private EnhancedScroller scroller;
        [SerializeField] private EnhancedScrollerCellView cellPrefab;
        [SerializeField] private float cellSize;

        [SerializeField] private TextMeshProUGUI simulationChatCellText;


        private bool _isInit;
        private IEnumerable<ChatMessageSchema> _chatLogs;
        private CoroutineHandle _refresh;

        private readonly Dictionary<ChatMessageSchema, int> _lineCounts = new Dictionary<ChatMessageSchema, int>();

        public void Init(IEnumerable<ChatMessageSchema> logs)
        {
            if (!_isInit)
            {
                // 스크롤 세팅
                _isInit = true;
            }
            
            _chatLogs = logs;
            
            Refresh(1f);
        }

        public void Refresh()
        {
            if (_chatLogs == null || !_chatLogs.Any()) return;
            
            // 방금 온 채팅이 내가 친거라면, 스크롤 강제 밑으로 내림 
            // 내가 아니라면, 기존 스크롤 위치 유지
            var isLastChatMyChat = _chatLogs.Last().gamerInDate == Backend.UserInDate;
            Refresh(isLastChatMyChat ? 1f : scroller.NormalizedScrollPosition);
        }

        private void Refresh(float scrollPos)
        {
            Timing.KillCoroutines(_refresh);
            _refresh = Timing.RunCoroutine(_Refresh(scrollPos).CancelWith(gameObject));
        }

        private IEnumerator<float> _Refresh(float scrollPos)
        {
            // 너무 자주 초기화시켜서 매번 오래 걸리게 하지 않음 
            // 그렇다고 아예 초기화를 안 하면 메모리에 계속 들고 있게 되니, 
            // 적당한 크기일 때 삭제시킴 
            if (_lineCounts.Count >= 1000)
                _lineCounts.Clear();
            
            // 라인카운트 계산 
            foreach (var chatMessage in _chatLogs)
            {
                // 이미 계산했다면 스킵 
                if (_lineCounts.ContainsKey(chatMessage)) continue;
                
                // 애플 플랫폼에서 GetTextInfo 호출 때마다 텍스트 메쉬 오지게 생성하고 내리는 듯 
                // 메모리 스파이크 치는 이슈가 있어서, 이 처리를 프레임 분산 시킴 
                var convertToChatText = ChatView.ConvertToChatText(chatMessage);
                _lineCounts[chatMessage] = simulationChatCellText.GetTextInfo(convertToChatText).lineCount;

                // 1프레임 대기 (iOS만. 안드로이드는 해당 현상 없음)
                if (ExtensionMethods.IsIOS()) yield return Timing.WaitForOneFrame;
            }

            // 라인 카운트 전부 다 계산 후에 스크롤러 세팅
            scroller.Delegate = this;
            scroller.ReloadData(scrollPos);
        }

        /*
         * Scroller
         */
        #region Scroller

        public int GetNumberOfCells(EnhancedScroller _)
        {
            return _chatLogs.Count();
        }

        public float GetCellViewSize(EnhancedScroller _, int dataIndex)
        {
            // HACK
            // TMP Pro에게 계산을 위임시키고, 총 라인 카운트를 알아냄 
            // 그리고 셀 사이즈만큼 곱해서, 라인 수가 늘어나는만큼 늘려줌 
            // 여기서 GetCellView를 호출하면, 셀 생성이 뒤죽박죽이 되서 여기선 GetCellView를 호출하면 안됨 
            return cellSize * _lineCounts[_chatLogs.ElementAt(dataIndex)];
        }

        public EnhancedScrollerCellView GetCellView(EnhancedScroller _, int dataIndex, int cellIndex)
        {
            // 셀뷰 초기화 
            var cellView = scroller.GetCellView(cellPrefab) as ChatCellView;
            if (cellView == null)
                return null;

            // 텍스트 세팅 
            var chatCellData = _chatLogs.ElementAt(dataIndex);

            cellView.Init(chatCellData);

            return cellView;
        }

        #endregion
    }
}