﻿using System;
using Server;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(ChatView))]
    public class ChatViewBackEnd : MonoBehaviour
    {
        private ChatView _chatView;

        private void Awake()
        {
            _chatView = GetComponent<ChatView>();
        }

        private void Start()
        {
            _chatView.Init(ServerChat.Instance.ChatLogs);
            
            ServerChat.Instance.OnChat += OnChat;
        }

        private void OnDestroy()
        {
            ServerChat.Instance.OnChat -= OnChat;
        }

        private void OnChat()
        {
            _chatView.Refresh();
        }

        public void SendChat()
        {
            var message = _chatView.InputField.text;
            if (string.IsNullOrEmpty(message) || string.IsNullOrWhiteSpace(message))
                return;
            
            // 보내기 
            Send(message);

            // 인풋필드 날리기 
            _chatView.InputField.text = "";
        }

        public void Send(string message)
        {
            var myRankData = ServerRanking.Instance.GetMyRank();
            var rank = myRankData?.rank ?? -1;

            ServerChat.Instance.Send(message, rank);
        }
    }
}