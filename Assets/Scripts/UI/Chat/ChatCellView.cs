﻿using System;
using BackEnd;
using Doozy.Engine.UI;
using EnhancedUI.EnhancedScroller;
using Global;
using InGame;
using Server;
using TMPro;
using UI.Popup;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(TextMeshProUGUI))]
    public class ChatCellView : EnhancedScrollerCellView
    {
        private TextMeshProUGUI _text;
        private ChatMessageSchema _data;

        private void Awake()
        {
            _text = GetComponent<TextMeshProUGUI>();
        }

        public void Init(ChatMessageSchema data)
        {
            _data = data;

            _text.text = ChatView.ConvertToChatText(data);
        }
        
        public void BlockUser()
        {
            // 공지 메시지거나, 내 채팅이거나, GM이면 제외
            if (_data.isNotice || _data.gamerInDate == Backend.UserInDate || _data.nickname.Contains(FirebaseRemoteConfigController.gmNickNameKeyword))
                return;
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");
            
            var isBlockUser = Backend.Chat.IsUserBlocked(_data.nickname);
            
            var title = isBlockUser ? Localization.GetText("info_311") : Localization.GetText("info_305");
            var desc = isBlockUser ? Localization.GetFormatText("info_363", _data.nickname) : Localization.GetFormatText("info_306", _data.nickname);
            var yes = Localization.GetText("system_014"); 
            var no = Localization.GetText("system_042"); 
            
            YesOrNoPopup.ShowWithManualStr(title, desc, yes, no, () =>
            {
                if (isBlockUser)
                    BackEndManager.ChatUnBlock(_data.nickname);
                else
                    BackEndManager.ChatBlock(_data.nickname);
            }, null);
        }
    }
}