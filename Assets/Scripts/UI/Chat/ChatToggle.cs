﻿using System;
using Databox.OdinSerializer.Utilities;
using Doozy.Engine.UI;
using JetBrains.Annotations;
using MEC;
using UnityEditor;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(UIToggle))]
    public class ChatToggle : MonoBehaviour
    {
        // UserData로 저장하지 않음 
        private static bool _isOn = true;
        
        // 내부에서만 쓰이는 변수라 _붙임 
        private static event VoidDelegate _onToggle;

        /*
         * 멤버필드 
         */
        private UIToggle _toggle;
        [SerializeField] private GameObject[] onList;
        [SerializeField] private GameObject[] offList;
        

        /*
         * 함수
         */
        [RuntimeInitializeOnLoadMethod]
        private static void EnterPlayMode()
        {
            _isOn = true;
            _onToggle = null;
        }
        
        private void OnEnable()
        {
            _onToggle += InternalOnToggle;
        }

        private void OnDisable()
        {
            _onToggle -= InternalOnToggle;
        }

        private void Start()
        {
            Timing.CallDelayed(0f, Init);
        }

        private void Init()
        {
            _toggle ??= GetComponent<UIToggle>();
            _toggle.IsOn = _isOn;
            
            // 라벨
            onList.ForEach(go => go.SetActive(_isOn));
            offList.ForEach(go => go.SetActive(!_isOn));
        }


        #region 이벤트 처리 
        
        // OnValueChange으로 하면 isOn 변경시마다 무한 루프에 빠짐. On/Off 이벤트로 각각 따로 처리

        [PublicAPI]
        public void OnToggleOn()
        {
            _isOn = true;
            _onToggle?.Invoke();
        }
        
        [PublicAPI]
        public void OnToggleOff()
        {
            _isOn = false;
            _onToggle?.Invoke();
        }

        private void InternalOnToggle()
        {
            Init();
        }

        #endregion
    }
}