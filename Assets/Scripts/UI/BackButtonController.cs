﻿using InGame.Data;
using UI.Popup;
using UnityEngine;

namespace UI
{
    public class BackButtonController : MonoBehaviourSingletonPersistent<BackButtonController>
    {
        public void OnClick()
        {
            // 백버튼 누르자마자 서버 저장
            UserData.My.SaveToServer();
            
            // 정말 종료할꺼냐 
            YesOrNoPopup.Show("system_017", "system_050", "system_017", "system_042",
                Application.Quit, null);
        }
    }
}