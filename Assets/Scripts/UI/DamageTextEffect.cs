﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Doozy.Engine.UI;
using Global;
using InGame;
using InGame.Data;
using Lean.Pool;
using MEC;
using Mono.CSharp;
using TMPro;
using UnityEngine;
using Constraints = InGame.Global.Constraints;

namespace UI
{
	public class DamageTextEffect : MonoBehaviour
	{
		[SerializeField] private TextMeshProUGUI text;


		private float _originFontSize;
		private RectTransform rectTransform;
		
		private Tweener _punch;
		private Tweener _fade;


		/*
		 * Staic
		 */
		public static void Spawn(string damageStr, DamageType damageType, Vector3 position, float fontScale = 1f)
		{
			// 텍스트 이펙트 온오프 설정 
			if (!UserData.My.UserInfo.GetFlag(UserFlags.IsOnTextEffect, true)) return; 
			
			// 스폰 
			var damageText = LeanPool.Spawn(InGamePrefabs.Instance.damageText, InGamePrefabs.Instance.damageTextParent);

			Color damageColor;
			switch (damageType)
			{
				case DamageType.Normal:
					damageColor = Color.white;
					fontScale *= 1f;
					break;
				case DamageType.Critical:
					damageColor = Color.yellow;
					fontScale *= float.Parse(DataboxController.GetConstraintsData(Constraints.FONT_SCALE_CRITICAL), CultureInfo.InvariantCulture);
					break;
				case DamageType.Ample:
					damageColor = Color.red;
					fontScale *= float.Parse(DataboxController.GetConstraintsData(Constraints.FONT_SCALE_AMPLE), CultureInfo.InvariantCulture);
					break;
				case DamageType.BigBang:
					damageColor = Color.magenta;
					fontScale *= float.Parse(DataboxController.GetConstraintsData(Constraints.FONT_SCALE_BIGBANG), CultureInfo.InvariantCulture);
					break;
				default:
					throw new ArgumentOutOfRangeException();
			}

			damageText.Init(damageStr, damageColor, fontScale);

			// 데미지 텍스트 위치  
			damageText.rectTransform.FromWorldPosition(position, Camera.main, UICanvas.GetUICanvas("InGame").Canvas);
		}
		
		
		/*
		 * Functions  
		 */
		private void Awake()
		{
			rectTransform = GetComponent<RectTransform>();
			_originFontSize = text.fontSize;
		}

		public void Init(string effectText, Color color, float fontScale = 1f)
		{
			text.text = effectText;
			text.color = color;
			text.fontSize = _originFontSize * fontScale;

			//
			Timing.RunCoroutine(_Animation().CancelWith(gameObject));
		}
			
		private IEnumerator<float> _Animation()
		{
			// 스케일 펀치 
			{
				const float punchDuration = 0.5f;
				
				if (_punch == null)
				{
					_punch = transform.DOPunchScale(Vector3.one * 1.5f, punchDuration, 0)
						.SetRelative(true)
						.SetAutoKill(false);
				}
				else
				{
					_punch.Restart();
				}

				yield return Timing.WaitUntilDone(_punch.WaitForCompletion(true));
			}

			// 상승하기 
			{
				const float moveUpDuration = 1f;

				// 얘는 Restart 쓰면 안됨. 처음 트윈 시작했던 위치 기반으로 트윈돼버림 
				var move = transform.DOMoveY(100f, moveUpDuration)
					.SetRelative(true)
					.SetAutoKill(true);

				// 사라지기 
				const float fadeOutDuration = 1f;
				if (_fade == null)
				{
					_fade = text.DOFade(0f, fadeOutDuration)
						.SetAutoKill(false);
				}
				else
				{
					_fade.Restart();
				}

				yield return Timing.WaitUntilDone(move.WaitForCompletion(true));

				yield return Timing.WaitUntilDone(_fade.WaitForCompletion(true));
			}

			// 반납 
			LeanPool.Despawn(gameObject);
		}
	}
}
