﻿using System;
using Doozy.Engine.UI;
using Lean.Pool;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    [RequireComponent(typeof(RectTransform))]
    public class HealthBar : MonoBehaviour, IPoolable
    {
        [SerializeField] private Image fill;
        [SerializeField] private TextMeshProUGUI text;

        private Transform _target;
        private UICanvas _canvas;
        private RectTransform _rect;

        private void Awake()
        {
            _canvas = UICanvas.GetUICanvas("InGame");
            _rect = GetComponent<RectTransform>();
        }

        public void SetHp(string hpText, float amount)
        {
            text.text = hpText;
            fill.fillAmount = amount;
        }

        public void FollowTarget(Transform target)
        {
            _target = target;

            // Update 에서도 하지만, 가끔씩 이전 위치서 점핑 되는 현상 생겨서 바로 업데이트 시킴 
            FollowTarget();
        }

        // 헬스바가 꺼지면, 팔로잉타겟 코루틴 걸어놨던게 풀림
        // 그래서 그냥 Update에서 타겟있으면 계속 따라가게 하는걸로 바꿈 
        private void Update()
        {
            FollowTarget();
        }

        private void FollowTarget()
        {
            // 카메라가 껐다 다시 켜져도 타게팅 제대로 잡힐 수 있도록 매번 카메라 가져옴
            var mainCamera = Camera.main;
            
            if (_target == null || mainCamera == null)
                return;
        
            _rect.FromWorldPosition(_target.position, mainCamera, _canvas.Canvas);
        }
    
        private void OnEnable()
        {
            // 체력바 켜지자마자 위치 갱신해주기 
            // 몬스터의 경우, 데미지 들어갈 때 보여주는데, 이미 FollowTarget은 돼있지만, Deactive 상태였어서 갱신이 즉각 되지 않음 
            FollowTarget();
        }

        public void OnSpawn()
        {
        
        }

        public void OnDespawn()
        {
        
        }
    }
}
