﻿using Doozy.Engine.UI;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using UI.Popup;
using UI.Popup.Menu;
using UI.Popup.Plunder;
using UnityEngine;
using Utils;

namespace UI
{
    [RequireComponent(typeof(NoticeIcon))]
    public class DiabloRelicNotice : MonoBehaviour
    {
        private NoticeIcon _notice;
        
        private void Awake()
        {
            _notice = GetComponent<NoticeIcon>();
            
            // 이벤트
            UserData.My.Resources.OnChangedDiabloKey += Refresh;
        }

        private void OnDestroy()
        {
            // 이벤트
            UserData.My.Resources.OnChangedDiabloKey -= Refresh;
        }

        private void Start()
        {
            Refresh();
        }

        private void Refresh()
        {
            var canNotice = InGameControlHub.My.DiabloRelicController.Gacha.HasEnoughKey || 
                            InGameControlHub.My.DiabloRelicController.Exchange.HasEnoughKey;
            
            _notice.notice.SetActive(canNotice);
        }
    }
}