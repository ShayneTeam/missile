// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class GooglePlayTangle
    {
        private static byte[] data = System.Convert.FromBase64String("phSXtKabkJ+8EN4QYZuXl5eTlpUX1dal8b9YTE3ymHaXCH2wARbwzgDvHuScAySCwTYev1CRUZil1ux2FJeZlqYUl5yUFJeXlgGonJG8rGU3vD+QO5xtTOfkmSzBnaYH4zF1f70xUFHNa883r2HhePexqlSAFkm3rmrffuPLZFc3sB41zEyGAGGFDEIuJLwuY0jOiz1E3qwGm6FiN6NHy5oL21SInPGXZy/cc0BqW20i+HhQNO/l+loV41N09gRyZ6Iwxxf63RV/HfZukPzxYusVxOEpn96r8+1AcVkhZX1hlTs6TCQ6nxIWrnHRHkpdYxY66v7GqlE3JSiZMx4CdjEqkuLcrSwti/bvQHqMwLA26TP8ewNdCA1nqzF1WG0PmZSVl5aX");
        private static int[] order = new int[] { 0,2,11,11,8,11,10,10,9,10,12,11,13,13,14 };
        private static int key = 150;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
