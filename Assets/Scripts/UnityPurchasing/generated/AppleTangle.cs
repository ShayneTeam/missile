// WARNING: Do not modify! Generated file.

namespace UnityEngine.Purchasing.Security {
    public class AppleTangle
    {
        private static byte[] data = System.Convert.FromBase64String("DI+OiIekCMYIee3qi4++D3y+pIhXuPFPCdtXKRc3vMx1Vlv/EPAv3KQIxgh5g4+Pi4uOvuy/hb6HiI3bioidjNvdv52+n4iN24qEnYTP/v7xzyYWd19E6BKq5Z9eLTVqlaRNkai+qoiN24qFnZPP/v7i667N6/z6htC+DI+fiI3bk66KDI+GvgyPir6RCw0LlRezybl8JxXOAKJaPx6cVoETs32lx6aURnBAOzeAV9CSWEWziI3bk4CKmIqapV7nyRr4h3B65QO4F8Kj9jljAhVSffkVfPhc+b7BT67v4Oqu7ev8+ufo5+3v+ufh4K7+JlLwrLtEq1tXgVjlWiyqrZ95LyL+4uuu3OHh+q7Nz76QmYO+uL66vAH9D+5IldWHoRw8dsrGfu62EJt7u7y/ur69uNSZg727vry+t7y/ur7n6Oft7/rn4eCuz/v65uH85/r3v+q7rZvFm9eTPRp5eBIQQd40T9beoM4oecnD8YbQvpGIjduTrYqWvpiLjo0Mj4GOvgyPhIwMj4+Oah8nh74MijW+DI0tLo2Mj4yMj4y+g4iHoq7t6/z65+jn7e/6667+4eLn7fcbEPSCKskF1VqYub1FSoHDQJrnX8vwkcLl3hjPB0r67IWeDc8JvQQPs6jprgS95HmDDEFQZS2hd93k1er65uH85/r3v5i+moiN24qNnYPP/qpsZV85/lGBy2+pRH/j9mNpO5mZ3Ovi5+/g7euu4eCu+ubn/a7t6/yRH1WQyd5li2PQ9wqjZbgs2cLbYk7tvfl5tImi2GVUga+AVDT9l8E7+ufo5+3v+uuu7Peu7+D3rv7v/Pri667H4O2gv6i+qoiN24qFnZPP/vS+DI/4voCIjduTgY+PcYqKjYyPDpqlXufJGviHcHrlA6DOKHnJw/E5lTMdzKqcpEmBkzjDEtDtRsUOmZi+moiN24qNnYPP/v7i667c4eH6+fmg7/7+4uug7eHjoe/+/uLr7e+9uNS+7L+FvoeIjduKiJ2M292/nTB6/RVgXOqBRffBulYssHf2ceVGiWLztw0F3a5dtko/MRTBhOVxpXIFlwdQd8Xie4klrL6MZpawdt6HXekBhjqueUUioq7h/jixj74COc1BiL6BiI3bk52Pj3GKi76Nj49xvpO+n4iN24qEnYTP/v7i667H4O2gv/eu7/39++Pr/a7v7e3r/vrv4O3rruHorvrm66765uvgru/+/uLn7e/HVvgRvZrrL/kaR6OMjY+Ojy0Mj+zi6679+u/g6u/86q766/zj/a7vO7QjeoGAjhyFP6+YoPpbsoNV7Jj+4uuuzev8+ufo5+3v+ufh4K7P+4OIh6QIxgh5g4+Pi4uOjQyPj47SR5f8e9OAW/HRFXyrjTTbAcPTg3/XKYuH8pnO2J+Q+l05Ba21yS1b4a7Nz74Mj6y+g4iHpAjGCHmDj4+P4Oqu7eHg6uf65+Hg/a7h6K77/es/vtZi1Iq8AuY9AZNQ6/1x6dDrMqG+D02IhqWIj4uLiYyMvg84lA89hqWIj4uLiYyPmJDm+vr+/bShofklLf8cyd3bTyGhzz12dW3+Q2gtwvzv7frn7euu/frv+uvj6+D6/aC+3iQEW1Rqcl6Hibk++/uv");
        private static int[] order = new int[] { 34,58,58,58,17,10,33,49,22,30,48,45,54,33,55,49,45,20,31,55,34,44,31,25,56,48,53,38,49,57,39,58,49,49,59,54,36,58,50,51,45,44,57,51,45,45,50,57,48,59,57,55,52,59,55,59,58,59,58,59,60 };
        private static int key = 142;

        public static readonly bool IsPopulated = true;

        public static byte[] Data() {
        	if (IsPopulated == false)
        		return null;
            return Obfuscator.DeObfuscate(data, order, key);
        }
    }
}
