﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Bolt;
using DG.Tweening;
using Doozy.Engine.UI;
using Firebase.Analytics;
using Global;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Data;
using InGame.Data.Scene;
using InGame.Effect;
using InGame.Global;
using Lean.Pool;
using MEC;
using Server;
using TMPro;
using UI.Popup.Plunder;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace InGame.Plunder
{
    public class Plunder : MonoBehaviour
    {
        /*
         * 씬 내에서만 싱글턴 
         */
        #region Scene Object

        public static Plunder Instance;


        private void Awake()
        {
            Instance = this;
        }

        #endregion
        
        
        
        /*
         * 시간 제한 
         */
        [Header("시간 제한"), SerializeField] private TextMeshProUGUI limitedTimeText;

        private CoroutineHandle _timer;

        public bool IsStarted => _timer.IsRunning;
        
        /*
         * 가스 
         */
        [Header("가스"), SerializeField] private GameObject gasPrefab;
        [SerializeField] private SpriteRenderer gasZone;
        [SerializeField] private int gasSpawnCount = 15;

        [SerializeField] private ResourceText gasTextMe;
        [SerializeField] private ResourceText gasTextTargetUser;

        [SerializeField] private float gasCountAnimDuration;
        
        [SerializeField] private float gasEffectFlyingDuration;

        private double _plunderableGas;
        
        private double _gasMe;
        public double FinalizedGasMe => _gasMe;
        
        private double _gasTargetUser;
        public double FinalizedGasTargetUser => _gasTargetUser;

        public float FinalizedPlunderedPercent
        {
            get
            {
                SceneData.Instance.Plunder.GetTargetUserData(out _, out _, out var initGas);
                
                return (float)(_gasMe / initGas * 100f);
            }
        }

        /*
         * 기계
         */
        [Header("기계 프리팹"), SerializeField] private PlunderMachine machinePrefab;
        [Header("기계 스폰 위치"), SerializeField] private Transform spawnPosOfMachine;
        [NonSerialized] public PlunderMachine Machine;
        
        /*
         * 이동 속도 
         */
        [Header("기계 이동 속도"), SerializeField] private Slider machineSpeedSlider;
        [SerializeField] private TextMeshProUGUI machineSpeedPercentText;
        
        /*
         * 데미지 합계 
         */
        [Header("데미지 합계"), SerializeField] private TextMeshProUGUI totalDamageTextMe;
        [SerializeField] private TextMeshProUGUI totalDamageTextTargetUser;
        
        private double _totalDamageMe;
        public double TotalDamageMe => _totalDamageMe;
        
        private double _totalDamageTargetUser;
        public double TotalDamageTargetUser => _totalDamageTargetUser;

        [Header("데미지 계산 주기(초)"), SerializeField]
        private float totalDamageCalcInterval = 1f;

        private float _cumulatedSpeedValue; // -100 ~ +100 범위

        private const float SpeedMinValue = -100f;
        private const float SpeedMaxValue = 100f;

        private CoroutineHandle _totalDamageCalcFlow;
        
        /*
         * 승리, 패배 
         */
        private CoroutineHandle _victoryFlow;
        private CoroutineHandle _defeatFlow;
        
        [Header("결과 팝업 전, 딜레이"), SerializeField] private float delayToShowResult = 2f;
        
        /*
         * 시간 
         */
        [NonSerialized] public long StartTimestamp;

        /*
         * 파이어베이스 로깅용 기본 파라미터들
         */
        private Dictionary<string, object> _enemyDefaultParameters = new Dictionary<string, object>();
        
        /*
         * 인피니티 스톤 
         */
        [NonSerialized] public string RewardInfStoneIdx;
        

        /*
         * 시작 
         */
        public void StartMode()
        {
            // BGM
            SoundController.Instance.PlayBGM("bgm_dg_battle_01");
            
            // 
            SceneData.Instance.Plunder.GetTargetUserData(out var userInDate, out var userNickName, out var initGas);
            
            var plunderUserControlHub = InGameControlHub.Other[userInDate];
            
            // 가스 설정 
            _plunderableGas = initGas;
            
            _gasMe = 0d;
            _gasTargetUser = _plunderableGas;
            
            gasTextMe.Init(_gasMe.ToUnitString());
            gasTextTargetUser.Init(_gasTargetUser.ToUnitString());
            
            // 데미지 합계 초기화 
            totalDamageTextMe.text = string.Empty;
            totalDamageTextTargetUser.text = string.Empty;
            
            // 시간
            limitedTimeText.text = DataboxController.GetConstraintsData(Constraints.PLUNDER_BATTLE_TIME); 
            
            // 배경
            StageControl.GetPlanetIdxByStage(plunderUserControlHub.Data.Stage.bestStage, out var planetIdx);
            var bg = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.bg_id, "BG_01");
            BackgroundController.Instance.SetBg(AtlasController.GetSprite(bg));

            // 시작 플로우
            Timing.RunCoroutine(_StartFlow().CancelWith(gameObject));

            _enemyDefaultParameters = new Dictionary<string, object>()
            {
                {"userIndate", userInDate},
                {"userNickName", userNickName},
                {"userInitGas", initGas},
            };

            WebLog.Instance.AllLog("plunder_start", _enemyDefaultParameters);
        }

        private void GetArmies(out ArmyPlunder myArmy, out ArmyPlunder enemyArmy)
        {
            SceneData.Instance.Plunder.GetTargetUserData(out var userInDate, out _, out _);

            myArmy = Army.My as ArmyPlunder;
            enemyArmy = Army.Other[userInDate] as ArmyPlunder;
        }
        
        private IEnumerator<float> _StartFlow()
        {
            GetArmies(out var myArmy, out var enemyArmy);
            if (myArmy == null || enemyArmy == null)
                yield break;
            
            // 커튼 치기
            // 이미 커튼이 쳐서 들어와야 백그라운드가 안 보임 
            var curtainShowAsync = CurtainScreenEffect.Instance.Show();
            while (!curtainShowAsync.IsDone)
                yield return Timing.WaitForOneFrame;

            // 심플하게 모든 것 싹 다 반납
            LeanPool.DespawnAll();

            // 아군, 적 전부 날리기 (린풀에서의 반납과 클래스 상에서의 캐싱리스트 반납과는 별개임)
            myArmy.DespawnAll();
            enemyArmy.DespawnAll();

            // 한 프레임 쉬기
            yield return Timing.WaitForOneFrame;
            
            // 가스통 스폰 
            var gasBounds = gasZone.bounds;
            for (var i = 0; i < gasSpawnCount; i++)
            {
                var spawnPos = Vector2.zero;
                spawnPos.x = Random.Range(gasBounds.min.x, gasBounds.max.x); 
                spawnPos.y = Random.Range(gasBounds.min.y, gasBounds.max.y);
                
                LeanPool.Spawn(gasPrefab, spawnPos, Quaternion.identity, transform);
            }

            // 내 부대 스폰 
            myArmy.SpawnAll();
            
            // 기계 만들기
            Machine = SpawnMachine(myArmy.Character);
            
            // 상대 부대 스폰 
            enemyArmy.SpawnAll();

            // 랜딩 준비 
            Machine.ReadyLanding();
            myArmy.ReadyLandingSoldiers();

            enemyArmy.ReadyLandingAll();

            // 커튼 딜레이
            yield return Timing.WaitForSeconds(CurtainScreenEffect.Instance.CurtainWaitingDelaySec);

            // 커튼 열기
            var curtainHideAsync = CurtainScreenEffect.Instance.Hide();
            while (!curtainHideAsync.IsDone)
                yield return Timing.WaitForOneFrame;
            
            var landingTime = myArmy.LandingTime;
             
            // 기계 랜딩
            Machine.Landing(landingTime);
            yield return Timing.WaitForSeconds(landingTime);
            
            // 첫 가스 폭파 자원 획득 
            
            // 상대 캐릭터 랜딩
            enemyArmy.LandingCharacter(landingTime);
            yield return Timing.WaitForSeconds(landingTime);
            
            // 아군 용병 + 상대 용병 동시 랜딩 
            myArmy.LandingSoldiers(landingTime);
            enemyArmy.LandingSoldiers(landingTime);
            yield return Timing.WaitForSeconds(landingTime);
            
            // 약간의 딜레이 
            yield return Timing.WaitForSeconds(1f);
            
            // 공격 시작 전 타임스탬프 기록 (첫 스킬 강제 쿨타임 적용 위함)
            StartTimestamp = ServerTime.Instance.NowSec;
            
            // 공격 시작 
            Machine.StartMove();
            
            myArmy.Attack();
            enemyArmy.Attack();
            
            // 데미지 정산 시작 
            _totalDamageCalcFlow = Timing.RunCoroutine(_DamageCalcFlow().CancelWith(gameObject));
            _timer = Timing.RunCoroutine(_TimerFlow().CancelWith(gameObject));
        }
        
        private PlunderMachine SpawnMachine(Character character)
        {
            // 기계 스폰 
            var machine = LeanPool.Spawn(machinePrefab, spawnPosOfMachine.position, Quaternion.identity, Army.My.transform);
            
            // 기계에 캐릭터 어태치
            character.transform.position = machine.characterPivot.position;
            character.transform.SetParent(machine.characterPivot, true);
            
            return machine;
        }

        private IEnumerator<float> _TimerFlow()
        {
            // 시간 표시 
            var remainTime = float.Parse(DataboxController.GetConstraintsData(Constraints.PLUNDER_BATTLE_TIME), CultureInfo.InvariantCulture);
            while (remainTime > 0f)
            {
                remainTime -= Time.deltaTime;
                
                // 타임 텍스트 
                limitedTimeText.text = $"{remainTime.ToString("0")}";
			
                yield return Timing.WaitForOneFrame;
            }
            
            // 타임오버 처리 
            Timing.RunCoroutine(_TimeOverFlow().CancelWith(gameObject));
        }
        
        /*
         * 승리
         */
        public void Victory()
        {
            if (_victoryFlow.IsRunning)
                return;
                
            _victoryFlow = Timing.RunCoroutine(_VictoryFlow().CancelWith(gameObject));
        }

        private IEnumerator<float> _VictoryFlow()
        {
            GetArmies(out var myArmy, out var enemyArmy);
            if (myArmy == null || enemyArmy == null)
                yield break;
            
            // 아군 사격 중지 
            myArmy.Stop();
            
            // 적 전부 사망
            enemyArmy.DieAll();
            
            WebLog.Instance.AllLog("plunder_victory", _enemyDefaultParameters);
            
            // 종료 공통 
            yield return Timing.WaitUntilDone(_EndCommonFlow().CancelWith(gameObject));
        }
        
        /*
         * 패배 
         */
        private void Defeat()
        {
            if (_defeatFlow.IsRunning)
                return;

            _defeatFlow = Timing.RunCoroutine(_DefeatFlow().CancelWith(gameObject));
        }
        
        private IEnumerator<float> _DefeatFlow()
        {
            GetArmies(out var myArmy, out var enemyArmy);
            if (myArmy == null || enemyArmy == null)
                yield break;
            
            // 상대 공격 중지 
            enemyArmy.Stop();
            
            // 기계 멈춤 
            Machine.StopMove();
            
            // 아군 캐릭터 터트리기 
            myArmy.Character.DieExplosion();
            
            // 아군 용병 죽음 처리 
            myArmy.DieAllSoldiers();

            WebLog.Instance.AllLog("plunder_defeat", _enemyDefaultParameters);

            // 종료 공통 
            yield return Timing.WaitUntilDone(_EndCommonFlow().CancelWith(gameObject));
        }

        private IEnumerator<float> _TimeOverFlow()
        {
            GetArmies(out var myArmy, out var enemyArmy);
            if (myArmy == null || enemyArmy == null)
                yield break;
            
            // 둘 다 공격 중지 
            enemyArmy.Stop();
            myArmy.Stop();
            
            // 기계 멈춤 
            Machine.StopMove();
            
            WebLog.Instance.AllLog("plunder_timeOver", _enemyDefaultParameters);

            // 종료 공통 
            yield return Timing.WaitUntilDone(_EndCommonFlow().CancelWith(gameObject));
        }
        
        private IEnumerator<float> _EndCommonFlow()
        {
            // 데미지 계산 멈춤 
            if (_totalDamageCalcFlow.IsRunning)
                Timing.KillCoroutines(_totalDamageCalcFlow);
            
            // 제한 시간 멈춤 
            if (_timer.IsRunning)
                Timing.KillCoroutines(_timer);
            
            // 딜레이 
            yield return Timing.WaitForSeconds(delayToShowResult);
            
            // 인피니티 스톤 하나 랜덤 뽑기
            RewardInfStoneIdx = InfStoneControl.StoneControl.RandomPickStone();
            
            // 약탈 결과 팝업 
            var popup = PlunderResultPopup.Show();
            yield return Timing.WaitUntilDone(popup.WaitHide().CancelWith(gameObject));  // 팝업 닫힐 때까지 대기 
            
            // 약탈 결과 반영 
            PlunderMode.Instance.End();
        }

        /*
         * 가스 획득 및 차감 
         */
        public void PlunderTargetUserGas(float gasPercentage, Vector3 effectStartWorldPos)
        {
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_plunder_gas_get");
            
            // 퍼센티지 계산 
            var gas = _plunderableGas * (gasPercentage / 100f);
            
            // UI 이펙트를 스폰시키려면 진짜 스크린상의 좌표로 스폰시켜야 함 
            var mainCamera = Camera.main;
            var startPos = GlobalFunction.WorldToScreen(effectStartWorldPos, mainCamera);
            var endPos = gasTextMe.Icon.transform.position;
            
            // 이펙트 
            FlyingResourceEffect.Spawn(RewardType.REWARD_GAS, gasEffectFlyingDuration, startPos, endPos, gasTextMe.transform.parent,
                () => { SubtractTargetUserGas(gas); }
                , () => { AddMyGas(gas); }
                );
        }
    
        private void SubtractTargetUserGas(double gas)
        {
            var prevGas = _gasTargetUser;
            var subtractedGas = _gasTargetUser - gas;
            gasTextTargetUser.StartCountAnimation(prevGas, subtractedGas, gasCountAnimDuration);

            _gasTargetUser = subtractedGas;
        }

        private void AddMyGas(double gas)
        {
            var prevGas = _gasMe;
            var addedGas = _gasMe + gas;
            gasTextMe.StartCountAnimation(prevGas, addedGas, gasCountAnimDuration);

            _gasMe = addedGas;
        }

        /*
         * 고정 자리서 여러 개의 가스 통이 간격 별로 날아가기 
         */
        public void FlyGasBarrels(int barrelCount, float gasPercentage, Vector3 startWorldPos, float intervalSec)
        {
            Timing.RunCoroutine(_FlyGasBarrelsFlow(barrelCount, gasPercentage, startWorldPos, intervalSec).CancelWith(gameObject));
        }

        private static IEnumerator<float> _FlyGasBarrelsFlow(int barrelCount, float gasPercentage, Vector3 startWorldPos, float intervalSec)
        {
            for (var i = 0; i < barrelCount; i++)
            {
                Plunder.Instance.PlunderTargetUserGas(gasPercentage, startWorldPos);

                yield return Timing.WaitForSeconds(intervalSec);
            }
        }
        
        /*
         * 데미지 
         */
        public void AddDamageTotalToTargetUser(double damage)
        {
            _totalDamageTargetUser += damage;
            totalDamageTextTargetUser.text = _totalDamageTargetUser.ToUnitString();
        }

        public void AddDamageTotalToMe(double damage)
        {
            _totalDamageMe += damage;
            totalDamageTextMe.text = _totalDamageMe.ToUnitString();
        }

        private IEnumerator<float> _DamageCalcFlow()
        {
            // 2초 기다림 
            yield return Timing.WaitForSeconds(2f);

            var allIdxes = DataboxController.GetAllIdxes(Table.Plunder, Sheet.speed);
            
            // 끝날 때까지 주기적 체크  
            while (true)
            {
                // 데미지 정산 
                var diffDamage = TotalDamageMe - _totalDamageTargetUser;

                // 내 데미지가 더 크다면, 상대 데미지보다 얼만큼 더 큰지, 
                // 상대 데미지가 더 크다면, 내 데미지보다 얼만큼 더 큰지를 계산 
                var standardDamage = diffDamage > 0d ? _totalDamageTargetUser : TotalDamageMe;

                var diffPercentage = standardDamage == 0 ? diffDamage : diffDamage / standardDamage; // 0으로 나눠지지 않게 처리   

                // 인덱스 위부터 아래로 순서대로 체크 
                // 만약 -50 퍼센트면 적용되는 기준 퍼센티지가 없어서 Last가 적용됨 
                // 만약 +50 퍼센트면 처음부터 걸려서 First가 적용됨 
                var chosenIdx = allIdxes.Last();
                foreach (var idx in allIdxes)
                {
                    var idxPercentage = float.Parse(idx, CultureInfo.InvariantCulture);

                    // 기준 퍼센티지보다 작다면, 다음 기준 퍼센티지 비교 
                    if (diffPercentage < idxPercentage)
                    {
                        continue;
                    }
                    // 기준 퍼센티지보다 크거나 같다면
                    else
                    {
                        // 사용 
                        chosenIdx = idx;
                        break;
                    }
                }

                var thisTurnSpeedIncreaseValue = (float)DataboxController.GetDataInt(Table.Plunder, Sheet.speed, chosenIdx, Column.speed_increase);

                var thisTurnRemainTime = totalDamageCalcInterval;
                while (thisTurnRemainTime > 0f)
                {
                    // 프레임 기다림 
                    yield return Timing.WaitForOneFrame;
                    
                    // 프레임 기준 속도증감값 
                    var valuePerFrame = thisTurnSpeedIncreaseValue * Time.deltaTime;
                    
                    // 시간 차감 
                    thisTurnRemainTime -= Time.deltaTime;
                    
                    // 스피드 값 증감 
                    var currentSpeedValue = _cumulatedSpeedValue + valuePerFrame;
                    
                    // -100 ~ +100 범위서만 조정됨 
                    currentSpeedValue = Mathf.Clamp(currentSpeedValue, SpeedMinValue, SpeedMaxValue);

                    // 다음 계산 턴에 사용할 누적값 갱신 
                    _cumulatedSpeedValue = currentSpeedValue;
                    
                    // UI 갱신
                    var percentStr = (100f + currentSpeedValue).ToString("0");
                    machineSpeedPercentText.text = $"{percentStr}%";
                    machineSpeedSlider.value = currentSpeedValue;
                        
                    // 변경된 속도 반영 
                    // -100이면 속도 0되서 멈춤 
                    // +100이면 속도 2되서 두 배 됨 
                    Machine.MoveSpeedRatio = 1f + (currentSpeedValue / 100f);
                        
                    // 속도 -100이면 패배 처리  
                    if (currentSpeedValue <= SpeedMinValue)
                    {
                        Defeat();
                        yield break;
                    }
                }
            }
        }
    }
}
