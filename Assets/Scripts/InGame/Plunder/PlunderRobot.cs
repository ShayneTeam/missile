﻿using System.Collections;
using Doozy.Engine.UI;
using Global;
using InGame.Effect;
using InGame.Global;
using Lean.Pool;
using UnityEngine;

namespace InGame.Plunder
{
    public class PlunderRobot : MonoBehaviour
    {
        [SerializeField] private int robotIndex = 1;
        
        [SerializeField] private int gasBarrelCount = 5;
        [SerializeField] private float gasBarrelIntervalSec = 0.1f;
        
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            var position = transform.position;

            // 이펙트 
            EffectSpawner.Instance.Spawn("boss_die", position);
            
            // 드럽통 5개 날아감  
            var gasPercentage = DataboxController.GetDataInt(Table.Plunder, Sheet.reward, $"gas_rwd_robot_{robotIndex.ToLookUpString()}", Column.gas_rwd_percent);
            
            // 날라가는 드럼 갯수 하나 당 얻게될 가스 퍼센티지 계산
            var gasPercentagePerBarrel = gasPercentage / (float)gasBarrelCount;
            
            // 가스 당, 해당하는 자원 획득 
            // 로봇이 꺼져도 코루틴 살아있도록, Plunder에게 코루틴 등록 
            Plunder.Instance.FlyGasBarrels(gasBarrelCount, gasPercentagePerBarrel, position, gasBarrelIntervalSec);
            
            // 애는 씬 오브젝트라, 그냥 하이드 시키기 
            gameObject.SetActive(false);
        }
    }
}