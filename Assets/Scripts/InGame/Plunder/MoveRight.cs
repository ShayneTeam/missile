﻿using System;
using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace InGame.Plunder
{
    public class MoveRight : MonoBehaviour
    {
        [SerializeField] private float moveSpeed;
        [NonSerialized] public float MoveSpeedRatio = 1f;

        private CoroutineHandle _move;
        private float _originMoveSpeedRatio;

        private void Awake()
        {
            _originMoveSpeedRatio = MoveSpeedRatio;
        }

        private void OnEnable()
        {
            _move = Timing.RunCoroutine(_Move().CancelWith(gameObject));
        }

        private void OnDisable()
        {
            Timing.KillCoroutines(_move);
        }

        private IEnumerator<float> _Move()
        {
            var cacheTransform = transform;
            while (true)
            {
                var moveSpeedPerFrame = moveSpeed * MoveSpeedRatio * Time.deltaTime;
            
                var newPos = cacheTransform.position;
                newPos.x += moveSpeedPerFrame;
                cacheTransform.position = newPos;

                yield return Timing.WaitForOneFrame;
            }
        }

        public void ResetMoveSpeedRatio()
        {
            MoveSpeedRatio = _originMoveSpeedRatio;
        }
    }
}
