﻿using System.Collections.Generic;
using InGame.Common;
using Lean.Pool;
using UI;
using UnityEngine;
using Utils;

namespace InGame.Plunder
{
    public class PlunderUserCharacter : MonoBehaviour
    {
        [SerializeField] private Shaking shaking;
        
        private static readonly CachingDictionary<Collider2D, TriggerEnemy> _cachingDic = new CachingDictionary<Collider2D, TriggerEnemy>();

        private void OnTriggerEnter2D(Collider2D other)
        {
            // 캐싱 찾기 
            if (!_cachingDic.TryGetValue(other, out var triggerEnemy))
            {
                triggerEnemy = other.GetComponent<TriggerEnemy>();
                _cachingDic[other] = triggerEnemy;
            }
            
            // 데미지 처리 할 수 없는게 충돌됬다면 캔슬  
            if (triggerEnemy == null)
                return;

            // 데미지 
            var damage = triggerEnemy.GetDamageDefault(out var damageType);

            Plunder.Instance.AddDamageTotalToMe(damage);

            // 데미지 텍스트 
            var position = transform.position;
            DamageTextEffect.Spawn(damage.ToUnitString(), damageType, position);
            
            // 흔들기
            shaking.Shake();

            // 그냥 반납시키면 밋밋하니 이펙트 보여주기 
            EffectSpawner.SpawnRandomDamageEffect(other.ClosestPoint(position));
            
            // 알아서 반납할지 말지 처리  
            triggerEnemy.ProcessAfterDamage();
        }
    }
}