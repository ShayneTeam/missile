﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using InGame.Common;
using Lean.Pool;
using UI;
using UnityEngine;

namespace InGame.Plunder
{
    [RequireComponent(typeof(BoxCollider2D))]
    public class PlunderMachine : MonoBehaviour, ILanding
    {
        [Header("캐릭터 어태치 위치")]
        [SerializeField] public Transform characterPivot;
        [Header("스프라이트 애니메이션")]
        [SerializeField] private SpriteEffect spriteAnimation;
        [Header("이동")]
        [SerializeField] private MoveRight moveRight;
        [Header("랜딩")]
        [SerializeField] private Landing landing;
        [Header("흔들기")]
        [SerializeField] private Shaking shaking;

        private BoxCollider2D _collider;
        
        // 얘는 캐싱딕셔너리 처리하지 않는다
        // 매번 새롭게 로딩되는 약탈 씬 오브젝트들과 충돌되기 때문. 콜렉션에 계속 누적되게 됨 
        private static readonly Dictionary<Collider2D, TriggerEnemy> _cachedDic = new Dictionary<Collider2D, TriggerEnemy>();

        public float MoveSpeedRatio
        {
            get => moveRight.MoveSpeedRatio;
            set => moveRight.MoveSpeedRatio = value;
        }

        private void Awake()
        {
            _collider = GetComponent<BoxCollider2D>();
            // 콜라이더 처음엔 무조건 끔 (랜딩 전에 로봇이랑 먼저 부딪히면 안됨)
            _collider.enabled = false;
        }

        private void OnTriggerEnter2D(Collider2D other)
        {
            // 캐싱 찾기 
            if (!_cachedDic.TryGetValue(other, out var triggerEnemy))
            {
                triggerEnemy = other.GetComponent<TriggerEnemy>();
                if (triggerEnemy != null) _cachedDic[other] = triggerEnemy;
            }
            
            // 데미지 처리 할 수 없는게 충돌됬다면 캔슬  
            if (triggerEnemy == null)
                return;
            
            var damage = triggerEnemy.GetDamageDefault(out var damageType);

            Plunder.Instance.AddDamageTotalToTargetUser(damage);

            // 데미지 텍스트 
            var position = transform.position;
            DamageTextEffect.Spawn(damage.ToUnitString(), damageType, position);
            
            // 흔들기 
            shaking.Shake();

            // 그냥 반납시키면 밋밋하니 이펙트 보여주기 
            EffectSpawner.SpawnRandomDamageEffect(other.ClosestPoint(position));
            
            // 알아서 반납할지 말지 처리  
            triggerEnemy.ProcessAfterDamage();
        }

        public void ReadyLanding()
        {
            landing.Ready();
        }

        public void Landing(float duration)
        {
            landing.StartLanding(duration, () =>
            {
                // 사운드 
                SoundController.Instance.PlayEffect("sfx_ui_plunder_robot_gen");
            });

            // 랜딩할 때, 첫 로봇 터트리는 연출 위해서 
            // 에디터에서 꺼놨다가 이때 다시 켬 
            _collider.enabled = true;
        }

        public void StartMove()
        {
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_plunder_robot_move");
            
            // 이동
            Move(true);
        }

        public void StopMove()
        {
            Move(false);
        }

        private void Move(bool on)
        {
            moveRight.enabled = on;
            spriteAnimation.enabled = on;
        }

        private void OnDisable()
        {
            // 린풀 반납되면 초기화 처리 
            Move(false);
            
            _collider.enabled = false;
            
            moveRight.ResetMoveSpeedRatio();
        }
    }
}