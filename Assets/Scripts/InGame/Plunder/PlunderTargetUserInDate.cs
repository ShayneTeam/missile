﻿using BackEnd;
using InGame.Data;
using InGame.Data.Scene;
using Server;
using UnityEngine;

namespace InGame.Plunder
{
    public class PlunderTargetUserInDate : OwnerUserInDate
    {
        public override string InDate
        {
            get
            {
                SceneData.Instance.Plunder.GetTargetUserData(out var userInDate, out _, out _);
                return userInDate;
            }
        }
    }
}