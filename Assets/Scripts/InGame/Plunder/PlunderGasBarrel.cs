﻿using Global;
using InGame.Global;
using Lean.Pool;
using UnityEngine;

namespace InGame.Plunder
{
    public class PlunderGasBarrel : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            // 트럼통 1개 날아감
            var gasPercentage = DataboxController.GetDataInt(Table.Plunder, Sheet.reward, "gas_rwd_drum", Column.gas_rwd_percent);
            
            Plunder.Instance.PlunderTargetUserGas(gasPercentage, transform.position);
            
            // 반납 
            LeanPool.Despawn(gameObject);
        }
    }
}