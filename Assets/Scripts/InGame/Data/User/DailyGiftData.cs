﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class DailyGiftData
        {
            // Constraints로 빼놨다가 Default 설정을 반드시 해놔야 해서 이쪽으로 옮김
            private const int RequiredTime = 7200; 
            
            private class DataSchema
            {
                public ObscuredBool isCollected = false;
                public ObscuredInt remainTime = RequiredTime;
            }
            private DataSchema _data = new DataSchema();
            
            // Properties
            public bool HasCollected => _data.isCollected;
            public int RemainTimeToCollect => _data.remainTime;

            // Events


            // Functions
            public void Reset()
            {
                // 오늘자 획득 처리하지 않았다고 마킹 
                _data.isCollected = false;
                
                // 시간 충전
                _data.remainTime = RequiredTime;
            }

            public void Collect()
            {
                // 획득 마킹 
                _data.isCollected = true;
            }

            public void ElapseTime(int time)
            {
                _data.remainTime -= time;
            }

            public bool CanCollect()
            {
                // 이미 획득 했다면, false
                if (_data.isCollected) return false;

                // 아직 남은 시간 채워야 한다면, false
                if (_data.remainTime > 0) return false;

                // 그외에는 true
                return true;
            }

            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<DataSchema>(from);
            }
        }
    }
}