﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using Server;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public static class UserFlags
    {
        public const string IsAgreeService = "IsAgreeService";
        public const string IsAgreePrivacy = "IsAgreePrivacy";
        public const string IsWatchedIntro = "IsWatchedIntro";
        public const string IsWatchedReviewPopup = "IsWatchedReviewPopup";

        public const string CanAdRemove = "CanAdRemove";
        public const string CanBeInfinityTimeAllBuff = "CanBeInfinityTimeAllBuff";
        public const string CanDefendPlunder = "CanDefendPlunder";
        public const string CanBeSpeedBuff3x = "CanBeSpeedBuff3x";
        public const string CanAutoCatchFairy = "CanAutoCatchFairy";
        
        public const string IsOnBGM = "IsOnBGM";
        public const string IsOnSFX = "IsOnSFX";
        public const string IsOnPush = "IsOnPush";
        public const string IsOnTextEffect = "IsOnTextEffect";
        public const string IsOnDamageEffect = "IsOnDamageEffect";
        
        public const string TutorialAdsComplete = "TutorialAdsComplete";
        public const string TutorialNormalRelicComplete = "TutorialNormalRelicComplete";
        public const string TutorialMissileComplete = "TutorialMissileComplete";
    }
    
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class UserInfoData
        {
            private class LitData
            {
                public ObscuredLong lastPlayedTimestamp = 0;

                public string language = "";

                public Dictionary<string, ObscuredBool> flags = new Dictionary<string, ObscuredBool>();
                
                public ObscuredInt missileBonusChance = 0;
            }
            private LitData _info = new LitData();
            
            // Events
            public event StringDelegate OnChangedLanguage;
            
            // Functions
            public string Language
            {
                get => _info.language;
                set
                {
                    _info.language = value;
                    
                    OnChangedLanguage?.Invoke(value);
                }
            }
            
            public int MissileBonusChance
            {
                get => _info.missileBonusChance;
                set => _info.missileBonusChance = value;
            }

            public long LastPlayedTimestamp => _info.lastPlayedTimestamp;
            
            public void StampLastPlayedTime(long timestamp)
            {
                // 기록되는 타임스탬프 값이 더 커야 기록한다 (해킹방지)
                if (timestamp > _info.lastPlayedTimestamp)
                    _info.lastPlayedTimestamp = timestamp;
            }

            public bool GetFlag(string key, bool defaultValue = false)
            {
                if (_info.flags.TryGetValue(key, out var value))
                    return value;

                return defaultValue;
            }

            public void SetFlag(string key, bool value)
            {
                _info.flags[key] = value;
            }
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_info);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _info = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}