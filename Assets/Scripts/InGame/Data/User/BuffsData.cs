﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class BuffsData 
        {
            public class BuffData
            {
                public ObscuredInt exp = 0;
                public ObscuredDouble remainTime = 0;
            }
            
            private class LitData
            {
                public Dictionary<string, BuffData> buffs = new Dictionary<string, BuffData>();
            }
            private LitData _data = new LitData();
            
            

            public bool IsEnoughRemainTime(string type) //
            {
                // 데이터 없다면, 시간 충전 안돼있는걸로 간주 
                if (!_data.buffs.TryGetValue(type, out var buff))
                    return false;

                return buff.remainTime > 0;
            }
		
            public float GetRemainTime(string type)
            {
                // 데이터 없다면, 지속시간 없다고 간주 
                if (!_data.buffs.TryGetValue(type, out var buff))
                    return 0;

                return (float)buff.remainTime;
            }

            public void SetRemainTime(string type, float remainTime)
            {
                // 데이터 없다면 넣어주기 
                if (!_data.buffs.TryGetValue(type, out var buff))
                {
                    buff = new BuffData();
                    _data.buffs[type] = buff;
                }

                // 0초 밑으로 떨어지지 않게 고정 
                buff.remainTime = Mathf.Clamp(remainTime, 0, remainTime);
            }

            public int GetExp(string type)
            {
                // 데이터 없다면, 경험치 없다고 간주 
                if (!_data.buffs.TryGetValue(type, out var buff))
                    return 0;

                return buff.exp;
            }

            public void AddExp(string type)
            {
                // 데이터 없다면 넣어주기 
                if (!_data.buffs.TryGetValue(type, out var buff))
                {
                    buff = new BuffData();
                    _data.buffs[type] = buff;
                }

                buff.exp += 1;
            }
                
                

            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}
