﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class PlunderData
        {
            /*
             * Schema
             */
            public class MatchedUserSchema
            {
                public string inDate = "";
                public string nickname = "";
                public ObscuredDouble plunderableGas = 0;
            }
            
            private class PlunderSchema
            {
                public Dictionary<string, MatchedUserSchema> matchedUsers = new Dictionary<string, MatchedUserSchema>();

                // 약탈을 하는 타이밍과 약탈 결과를 반영하는 타이밍이 다름 
                // 또한, 약탈 한 양과 실제 차감되는 약탈양은 다름 
                // 클라에서 어느 타이밍 하나 잡고, 차감돼야 할 약탈양을 계산 후 저장해둠
                public ObscuredDouble plunderedGasSum = 0d;
            }
            
            /*
             * Data
             */
            private PlunderSchema _data = new PlunderSchema();

            /*
             * Properties
             */
            public Dictionary<string, MatchedUserSchema> MatchedUsers
            {
                get => _data.matchedUsers;
                set => _data.matchedUsers = value;
            }
            
            public double PlunderedGasSum
            {
                get => _data.plunderedGasSum;
                set => _data.plunderedGasSum = value;
            }
            

            /*
             * Load & Save
             */
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;

                _data = JsonMapper.ToObject<PlunderSchema>(from);
            }
        }
    }
}