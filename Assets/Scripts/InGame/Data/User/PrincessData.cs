﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public enum TreasureType
    {
        hat,
        accessory,
        bag,
        belt,
        shoes,
    }
    
    public static class TreasureTypeExtensions
    {
        public static string ToEnumString(this TreasureType type)
        {
            return type switch
            {
                TreasureType.hat => nameof(TreasureType.hat),
                TreasureType.accessory => nameof(TreasureType.accessory),
                TreasureType.bag => nameof(TreasureType.bag),
                TreasureType.belt => nameof(TreasureType.belt),
                TreasureType.shoes => nameof(TreasureType.shoes),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
    
    public enum TreasureRarity
    {
        normal,
        magic,
        rare,
        hero,
        legend,
        myth
    }
    
    public static class TreasureRarityExtensions
    {
        public static string ToEnumString(this TreasureRarity type)
        {
            return type switch
            {
                TreasureRarity.normal => nameof(TreasureRarity.normal),
                TreasureRarity.magic => nameof(TreasureRarity.magic),
                TreasureRarity.rare => nameof(TreasureRarity.rare),
                TreasureRarity.hero => nameof(TreasureRarity.hero),
                TreasureRarity.legend => nameof(TreasureRarity.legend),
                TreasureRarity.myth => nameof(TreasureRarity.myth),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
    
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class PrincessData
        {
            private class TreasureSchema
            {
                public ObscuredInt count = 0;
                public ObscuredInt grade = 1;
                public ObscuredInt level = 1;
            }

            private class DataSchema
            {
                public ObscuredBool isLocked = true;
                public Dictionary<string, TreasureSchema> treasures = new Dictionary<string, TreasureSchema>();
                public string equippedBazooka = ""; // null로 넣을 시 NRE 뜸
                
                public string wornCostume = ""; // null로 넣을 시 NRE 뜸
                public List<string> ownedCostumes = new List<string>();
            }
            private DataSchema _data = new DataSchema();
            
            // Properties
            public bool IsLocked => _data.isLocked;
            public string EquippedBazooka => _data.equippedBazooka;
            public string WornCostume => _data.wornCostume;
            
            // Events
            public event StringDelegate OnEarned;
            public event StringDelegate OnLevelUp;
            public event StringDelegate OnGradeUp;
            public event VoidDelegate OnFromString;
            public event StringDelegate OnEquipped;
            public event StringDelegate OnWear;
            public event StringDelegate OnCollectedCostume;
            
            // Functions
            public void Unlock()
            {
                _data.isLocked = false;
            }

            public IEnumerable<string> AllTreasureIdxesCollected => _data.treasures.Keys;
            public IEnumerable<string> AllCostumeIdxesCollected => _data.ownedCostumes;

            public bool TryGetStateTreasure(string treasureIdx, out int count, out int grade, out int level)
            {
                count = 0;
                grade = 1;
                level = 1;

                // 미획득이라면 false 반환 
                if (!_data.treasures.TryGetValue(treasureIdx, out var treasure)) return false;
                
                count = treasure.count;
                grade = treasure.grade;
                level = treasure.level;

                return true;
            }

            public void EarnTreasure(string treasureIdx, int amount)
            {
                if (!_data.treasures.TryGetValue(treasureIdx, out var treasure))
                {
                    // 최초 획득 시, 데이타 생성 
                    treasure = new TreasureSchema();
                    _data.treasures[treasureIdx] = treasure;
                }

                treasure.count += amount;
                
                OnEarned?.Invoke(treasureIdx);
            }
            
            public void ConsumeTreasure(string treasureIdx, int amount)
            {
                // 획득 안 한 미사일이라면, 바로 종료
                if (!_data.treasures.TryGetValue(treasureIdx, out var treasure))
                    return;

                treasure.count -= amount;
            }

            public void LevelUpTreasure(string treasureIdx, int amount)
            {
                // 미획득 미사일이라면 아무것도 안함 
                if (!_data.treasures.TryGetValue(treasureIdx, out var treasure)) 
                    return;

                treasure.level += amount;
                
                OnLevelUp?.Invoke(treasureIdx);
            }
            
            public void GradeUpTreasure(string treasureIdx, int amount)
            {
                // 미획득 미사일이라면 아무것도 안함 
                if (!_data.treasures.TryGetValue(treasureIdx, out var treasure)) 
                    return;

                treasure.grade += amount;
                
                OnGradeUp?.Invoke(treasureIdx);
            }
            
            public void EquipBazooka(string bazookaIdx)
            {
                _data.equippedBazooka = bazookaIdx;

                OnEquipped?.Invoke(bazookaIdx);
            }
            
            public bool HasCollectedCostume(string costumeIdx)
            {
                return _data.ownedCostumes.Contains(costumeIdx);
            }

            public void CollectCostume(string costumeIdx)
            {
                // 이미 보유중이라면 스킵
                if (_data.ownedCostumes.Contains(costumeIdx)) return;
                
                _data.ownedCostumes.Add(costumeIdx);
                
                OnCollectedCostume?.Invoke(costumeIdx);
            }
            
            public void WearCostume(string costumeIdx)
            {
                if (!HasCollectedCostume(costumeIdx)) return;

                _data.wornCostume = costumeIdx;

                OnWear?.Invoke(costumeIdx);
            }
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<DataSchema>(from);
                
                // 이벤트 
                OnFromString?.Invoke();
            }
        }
    }
}