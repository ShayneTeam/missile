﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using BackEnd;
using BestHTTP;
using BestHTTP.Forms;
using Cysharp.Threading.Tasks;
using Global;
using Global.Extensions;
using InGame.Global;
using LitJson;
using Ludiq;
using MEC;
using Sentry;
using Server;
using UI.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;
using Utils;
using Utils.Dispatcher;

// 선로컬 후서버 데이터 저장을 위한 클래스 
// Databox와 Backend 연계 
namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public UserInfoData UserInfo { get; } = new UserInfoData();
        public TutorialData Tutorial { get; } = new TutorialData();
        public CostumeData Costume { get; } = new CostumeData();
        public StageData Stage { get; } = new StageData();
        public BazookaData Bazooka { get; } = new BazookaData();
        public MissilesData Missiles { get; } = new MissilesData();
        public ResourcesData Resources { get; } = new ResourcesData();
        public BoxesData Boxes { get; } = new BoxesData();
        public SoldiersData Soldiers { get; } = new SoldiersData();
        public SkillsData Skills { get; } = new SkillsData();
        public BuffsData Buffs { get; } = new BuffsData();
        public ItemsData Items { get; } = new ItemsData();
        public NormalRelicData NormalRelic { get; } = new NormalRelicData();
        public DiabloRelicData DiabloRelic { get; } = new DiabloRelicData();
        public WorldMapData WorldMap { get; } = new WorldMapData();
        public MissionData Mission { get; } = new MissionData();
        public WormHoleData WormHole { get; } = new WormHoleData();
        public PlunderData Plunder { get; } = new PlunderData();
        public PurchaseData Purchase { get; } = new PurchaseData();
        public BattlePassData BattlePass { get; } = new BattlePassData();
        public GuideData Guide { get; } = new GuideData();
        public InfinityStoneData InfinityStone { get; } = new InfinityStoneData();
        public DailyGiftData DailyGift { get; } = new DailyGiftData();
        public DailyChipData DailyChip { get; } = new DailyChipData();
        public ThirtyDaysGiftData ThirtyDaysGift { get; } = new ThirtyDaysGiftData();
        public RaidData Raid { get; } = new RaidData();
        public PrincessData Princess { get; } = new PrincessData();
        public BattleshipData Battleship { get; } = new BattleshipData();
        public EventData Event { get; } = new EventData();

        public static UserData My => GetInstance(ServerMyInfo.InDate);

        public class Indexer
        {
            public UserData this[string inDate] => GetInstance(inDate);
        }

        public static readonly Indexer Other = new Indexer();

        public static float SaveToServerDelay = 60f;
        public static float SaveToServerDelayForOurServer = 60f;

        /*
         * UserData에서 inDate를 idx로 사용 
         * Multiton에서 idx를 name으로 저장
         */
        public string InDate
        {
            get
            {
                _inDate ??= name;
                return _inDate;
            }
        }

        private string _inDate;

        public void Awake()
        {
            // Sync 호출보다 먼저 해줘야 ObscuredType에 임포트, 익스포트가 제대로 작동함 
            RegisterToJsonMapper();
        }

        #region Sync

        // private CoroutineHandle _autoLocalSave;
        private CoroutineHandle _autoServerSave;
        private CoroutineHandle _autoServerSaveForOurServer;

        public bool SyncServerDataAllForHost()
        {
            // Sync 호출보다 먼저 해줘야 ObscuredType에 임포트, 익스포트가 제대로 작동함 
            RegisterToJsonMapper();
            
            // 처음은 무조건 서버에서 유저 정보를 가져옵니다.
            var tableDataDic = AllTableData;

            for (var i = 0; i < tableDataDic.Count; i++)
            {
                var curDic = tableDataDic.ElementAt(i);
                var bm = Backend.GameData.GetMyData(curDic.tupleKey, new Where());

                if (!bm.IsSuccess())
                    return false;

                var rowCount = bm.Rows().Count;
                if (rowCount == 0) // 서버에 유저의 테이블 데이터가 없을 경우
                {
                    var param = new Param();
                    param.Add("data", curDic.tupleValue);

                    var newDate = Backend.GameData.Insert(curDic.tupleKey, param).CommonErrorCheck();
                    _lastServerSavedDic[curDic.tupleKey] = curDic.tupleValue;
                }
                else
                {
                    var serverString = bm.Rows()[0]["data"].DStringValue();
                    DataboxController.SetDataString(Table.Runtime, Sheet.user, InDate,
                        curDic.tupleKey, serverString);

                    _lastServerSavedDic[curDic.tupleKey] = serverString;
                }
            }

            // 서버데이터 로컬에 덮어씌운 다음, 로컬데이터 로드 (default 셋팅을 위함)
            LoadLocal();

            return true;
        }

        public void StartAutoSaveToServer()
        {
            // 뒤끝
            Timing.KillCoroutines(_autoServerSave);
            _autoServerSave = Timing.RunCoroutine(AutoSaveToServer().CancelWith(gameObject), Segment.RealtimeUpdate);
            
            // 우리 서버
            Timing.KillCoroutines(_autoServerSaveForOurServer);
            _autoServerSaveForOurServer = Timing.RunCoroutine(AutoSaveToOurServer().CancelWith(gameObject), Segment.RealtimeUpdate);
        }

        public void StopAutoSaveToServer()
        {
            Timing.KillCoroutines(_autoServerSave);
            Timing.KillCoroutines(_autoServerSaveForOurServer);
        }

        public void RefreshAutoSave()
        {
            // 오토 세이브 코루틴 재실행 (딜레이 초기화 위함)
            StartAutoSaveToServer();
        }

        private void OnApplicationPause(bool isPause)
        {
            // 자동 저장 코루틴 동작중일 때만 처리 
            if (!_autoServerSave.IsRunning) return;

            if (isPause == false)
            {
                // 어플리케이션이 재실행 되었을 때 자동저장 재개
                Timing.ResumeCoroutines(_autoServerSave);
                Timing.ResumeCoroutines(_autoServerSaveForOurServer);
            }
            else
            {
                // 어플리케이션이 정지되었을 때 자동저장 정지  
                Timing.PauseCoroutines(_autoServerSave);
                Timing.PauseCoroutines(_autoServerSaveForOurServer);
            }
        }

        public IEnumerator<float> _RequestServerData(List<string> wantTables)
        {
            // 만약 내 데이터를 요청 한다면, 로컬에 모든 테이블 데이터 저장 
            // wantTables 이외에 테이블은 LoadLocal 시, 가장 최근의 SaveToLocal 된 값으로 빽섭되기 때문
            // (최초 접속 타이밍 때만 하므로, 최초 접속 데이터로 빽섭되게 됨)  
            if (InDate == ServerMyInfo.InDate)
            {
                SaveToLocal();
            }

            // 요청 
            foreach (var curTable in wantTables)
            {
                // 비동기 요청 
                yield return Timing.WaitUntilDone(BackEndManager._GetUserTableData(curTable, InDate, tableData =>
                {
                    DataboxController.SetDataString(Table.Runtime, Sheet.user, InDate, curTable, tableData);
                }));
            }

            // 로드 
            LoadLocal();
        }

        #endregion


        #region Load & Save

        private IEnumerator<float> AutoSaveToLocal()
        {
            /*
             * 1. 한 프레임에 여러 데이터가 변경될 때, 쓸데없이 여러 번 저장될 수 있는 문제 방지
             * 2. 최초의 세이브는 최초의 로드 이후에 할 수 있도록 강제  
             */
            while (true)
            {
                /* 
                 * 리얼타임 특정 초마다 저장
                 * 정말 중요한 데이터는 수동으로 SaveToLocal 호출할 것 
                 */
                yield return Timing.WaitForSeconds(SaveToServerDelay);

                SaveToLocal();
            }
        }


        private IEnumerator<float> AutoSaveToServer()
        {
            /*
             * 1. 한 프레임에 여러 데이터가 변경될 때, 쓸데없이 여러 번 저장될 수 있는 문제 방지
             * 2. 최초의 세이브는 최초의 로드 이후에 할 수 있도록 강제  
             */
            while (true)
            {
                /* 
                 * 리얼타임 특정 초마다 저장
                 * 정말 중요한 데이터는 수동으로 SaveToLocal 호출할 것 
                 */
                yield return Timing.WaitForSeconds(SaveToServerDelay);

                SaveToServer();
            }
        }

        public void LoadLocal()
        {
            // 유저 정보 
            UserInfo.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.UserInfo, null));

            if (string.IsNullOrWhiteSpace(UserInfo.Language))
            {
                // 만약 국가코드가 없다면, 초기 국가코드를 등록해줘야 함.
                UserInfo.Language = Localization.GetSystemDefaultLanguage();
            }

            // 스테이지
            Stage.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Stage, null));

            // 자원  
            Resources.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Resources, null));

            // 상자 
            Boxes.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Boxes, "{}"));

            // 코스튬 
            Costume.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Costume, "{}"));

            const string defaultHead = "12001";
            if (!Costume.IsEarned(CostumeType.head, defaultHead))
            {
                Costume.Earn(CostumeType.head, defaultHead);
                Costume.Equip(CostumeType.head, defaultHead);
            }

            const string defaultSkin = "11001";
            if (!Costume.IsEarned(CostumeType.skin, defaultSkin))
            {
                Costume.Earn(CostumeType.skin, defaultSkin);
                Costume.Equip(CostumeType.skin, defaultSkin);
            }

            const string defaultBody = "13001";
            if (!Costume.IsEarned(CostumeType.body, defaultBody))
            {
                Costume.Earn(CostumeType.body, defaultBody);
                Costume.Equip(CostumeType.body, defaultBody);
            }

            // 바주카 
            const string defaultBazooka = "40001";
            {
                Bazooka.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user,
                    InDate,
                    ServerTable.Bazooka, "{}"));

                // 기본 바주카 넣어주기 
                if (!Bazooka.IsEarned(defaultBazooka))
                {
                    Bazooka.Earn(defaultBazooka);
                    Bazooka.Equip(defaultBazooka);
                }
            }

            // 미사일 
            {
                Missiles.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user,
                    InDate,
                    ServerTable.Missiles, "{}"));

                // 기본 미사일 넣어주기
                const string defaultMissile = "10001";
                if (!Missiles.IsEarned(defaultMissile))
                {
                    Missiles.Earn(defaultMissile, 1);
                    Missiles.Equip(defaultMissile, true);
                }
/*
                // TODO. 테스트. 그냥 다 넣기
                var allMissileIdxes = DataboxController.GetAllIdxes(Table.Missile, Sheet.Missile);
                allMissileIdxes.ForEach(idx =>
                {
                    Missiles.Earn(idx, 1);

                    // 획득하지 않고, 이렇게 접근하면 NRE 뜨게 함 
                    Missiles[idx].isEquipped = true;
                });*/
            }

            // 용병 
            Soldiers.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Soldiers, "{}"));

            // 스킬
            Skills.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Skills, "{}"));

            // 버프
            Buffs.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Buffs, "{}"));

            // 아이템
            Items.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Items, "{}"));
            {
                // 웜홀 티켓 한 번도 가져본 적이 없었다면, 지급 
                if (!Items.HasEverHad(RewardType.REWARD_WH_TICKET))
                    Items[RewardType.REWARD_WH_TICKET] = 10;
            }

            // 유물
            NormalRelic.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user,
                InDate,
                ServerTable.NormalRelic, "{}"));

            DiabloRelic.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user,
                InDate,
                ServerTable.DiabloRelic, "{}"));

            // 월드맵
            WorldMap.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.WorldMap, "{}"));

            // 미션 
            Mission.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Mission, "{}"));
            {
                // 가이드 없을 시, 첫 가이드 넣어주기 
                Mission.GetGuideState(out var currentGuideIdx, out _);
                if (currentGuideIdx == "")
                {
                    var allGuides = DataboxController.GetAllIdxes(Table.Mission, Sheet.guide);
                    if (allGuides.Count > 0)
                        Mission.ChangeGuide(allGuides.ElementAt(0));
                }
            }

            // 웜홀 
            WormHole.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.WormHole, "{}"));

            // 약탈
            Plunder.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Plunder, "{}"));

            // 구매
            Purchase.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Purchase, "{}"));

            // 배틀패스
            BattlePass.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.BattlePass, "{}"));

            // 가이드
            Guide.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Guide, "{}"));

            // 인피니티 스톤
            InfinityStone.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.InfinityStone, "{}"));

            // 데일리 기프트
            DailyGift.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.DailyGift, "{}"));
            
            // 30일 간의 선물
            ThirtyDaysGift.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.ThirtyDaysGift, "{}"));
            
            // 데일리 강화칩
            DailyChip.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.DailyChip, "{}"));
                
            // 레이드
            Raid.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Raid, "{}"));
            
            // 튜토리얼
            Tutorial.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Tutorial, "{}"));
            
            // 공주
            {
                Princess.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                    ServerTable.Princess, "{}"));

                // 기본 바주카 장착
                if (string.IsNullOrEmpty(Princess.EquippedBazooka))
                {
                    Princess.EquipBazooka(defaultBazooka);
                }
                
                // 기본 코스튬 
                if (string.IsNullOrEmpty(Princess.WornCostume))
                {
                    const string defaultCostume = "15001";
                    Princess.CollectCostume(defaultCostume);
                    Princess.WearCostume(defaultCostume);
                }
            }
            
            // 배틀쉽
            Battleship.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Battleship, "{}"));
            
            // 이벤트
            Event.FromString(DataboxController.GetDataString(Table.Runtime, Sheet.user, InDate,
                ServerTable.Event, "{}"));
        }

        private void SaveToLocal()
        {
            SaveToLocalByInDate(InDate);
        }

        public void CopyTo(string inDate)
        {
            SaveToLocalByInDate(inDate);
        }

        private void SaveToLocalByInDate(string inDate)
        {
            // 유저 정보 
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate,
                ServerTable.UserInfo, UserInfo.ToString());

            // 스테이지
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Stage,
                Stage.ToString());

            // 자원 
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate,
                ServerTable.Resources,
                Resources.ToString());

            // 상자
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Boxes,
                Boxes.ToString());

            // 헤드, 스킨, 바디
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Costume,
                Costume.ToString());

            // 바주카 
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Bazooka,
                Bazooka.ToString());

            // 미사일 
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate,
                ServerTable.Missiles,
                Missiles.ToString());

            // 용병 
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate,
                ServerTable.Soldiers,
                Soldiers.ToString());

            // 스킬
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Skills,
                Skills.ToString());

            // 버프
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Buffs,
                Buffs.ToString());

            // 아이템
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Items,
                Items.ToString());

            // 유물
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate,
                ServerTable.NormalRelic,
                NormalRelic.ToString());
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate,
                ServerTable.DiabloRelic,
                DiabloRelic.ToString());

            // 월드맵
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate,
                ServerTable.WorldMap,
                WorldMap.ToString());

            // 미션 
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Mission,
                Mission.ToString());

            // 웜홀 
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.WormHole,
                WormHole.ToString());

            // 약탈
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Plunder,
                Plunder.ToString());

            // 구매
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Purchase,
                Purchase.ToString());

            // 배틀패스
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.BattlePass,
                BattlePass.ToString());

            // 가이드
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Guide,
                Guide.ToString());

            // 인피니티 스톤
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.InfinityStone,
                InfinityStone.ToString());

            // 데일리 기프트
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.DailyGift,
                DailyGift.ToString());

            // 30일 간의 선물 
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.ThirtyDaysGift,
                ThirtyDaysGift.ToString());

            // 데일리 강화칩
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.DailyChip,
                DailyChip.ToString());

            // 레이드
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Raid,
                Raid.ToString());

            // 튜토리얼
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Tutorial,
                Tutorial.ToString());

            // 공주
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Princess,
                Princess.ToString());
            
            // 배틀쉽
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Battleship,
                Battleship.ToString());
            
            // 이벤트 
            DataboxController.SetDataString(Table.Runtime, Sheet.user, inDate, ServerTable.Event,
                Event.ToString());

            // 데이타박스 저장 
            DataboxController.Save(Table.Runtime);
        }

        public HashSet<(string tupleKey, string tupleValue)> AllTableData
        {
            get
            {
                //튜플로 List<int,string> 형식 선언
                _allTableData.Clear();
                _allTableData.Add((ServerTable.UserInfo, UserInfo.ToString()));
                _allTableData.Add((ServerTable.Stage, Stage.ToString()));
                _allTableData.Add((ServerTable.Resources, Resources.ToString()));
                _allTableData.Add((ServerTable.Boxes, Boxes.ToString()));
                _allTableData.Add((ServerTable.Costume, Costume.ToString()));
                _allTableData.Add((ServerTable.Bazooka, Bazooka.ToString()));
                _allTableData.Add((ServerTable.Missiles, Missiles.ToString()));
                _allTableData.Add((ServerTable.Soldiers, Soldiers.ToString()));
                _allTableData.Add((ServerTable.Skills, Skills.ToString()));
                _allTableData.Add((ServerTable.Buffs, Buffs.ToString()));
                _allTableData.Add((ServerTable.Items, Items.ToString()));
                _allTableData.Add((ServerTable.NormalRelic, NormalRelic.ToString()));
                _allTableData.Add((ServerTable.DiabloRelic, DiabloRelic.ToString()));
                _allTableData.Add((ServerTable.WorldMap, WorldMap.ToString()));
                _allTableData.Add((ServerTable.Mission, Mission.ToString()));
                _allTableData.Add((ServerTable.WormHole, WormHole.ToString()));
                _allTableData.Add((ServerTable.Plunder, Plunder.ToString()));
                _allTableData.Add((ServerTable.BattlePass, BattlePass.ToString()));
                _allTableData.Add((ServerTable.Purchase, Purchase.ToString()));
                _allTableData.Add((ServerTable.Guide, Guide.ToString()));
                _allTableData.Add((ServerTable.InfinityStone, InfinityStone.ToString()));
                _allTableData.Add((ServerTable.DailyGift, DailyGift.ToString()));
                _allTableData.Add((ServerTable.ThirtyDaysGift, ThirtyDaysGift.ToString()));
                _allTableData.Add((ServerTable.DailyChip, DailyChip.ToString()));
                _allTableData.Add((ServerTable.Raid, Raid.ToString()));
                _allTableData.Add((ServerTable.Tutorial, Tutorial.ToString()));
                _allTableData.Add((ServerTable.Princess, Princess.ToString()));
                _allTableData.Add((ServerTable.Battleship, Battleship.ToString()));
                _allTableData.Add((ServerTable.Event, Event.ToString()));
                return _allTableData;
            }
        }
        private readonly HashSet<(string, string)> _allTableData = new HashSet<(string, string)>();
        private readonly Dictionary<string, string> _lastServerSavedDic = new Dictionary<string, string>();
        private bool _isShowLoading;
        private bool _isMaintain;

        public void SaveToServer(HashSet<string> dicList = null)
        {
            var tableDataDic = AllTableData;

            if (dicList?.Count > 0)
            {
                var newArr = new HashSet<(string, string)>();
                foreach (var valueTuple in tableDataDic.Where(valueTuple => dicList.Contains(valueTuple.tupleKey)))
                {
                    newArr.Add(valueTuple);
                }
                tableDataDic = newArr;
            }

            for (var i = 0; i < tableDataDic.Count; i++)
            {
                var curDic = tableDataDic.ElementAt(i);

                if (_isMaintain) break;

                // 마지막으로 저장한 데이터와 동일하다면, 불필요한 저장을 하지 않도록 함.
                if (_lastServerSavedDic.TryGetValue(curDic.tupleKey, out var data))
                {
                    if (data.Trim() == curDic.tupleValue) continue;
                }
                
                // clear
                var param = new Param
                {
                    // all
                    { "data", curDic.tupleValue }
                };

                Backend.GameData.Update(curDic.tupleKey, new Where(), param, bro =>
                {
                    // 위에서 insert 했기 때문에, 로직상 무조건 성공해야 하긴 함.
                    var result = bro.CommonErrorCheck();

                    // 실패 시, 저장 다시 할 수 있게 캐싱 날림 
                    if (!bro.IsSuccess())
                        _lastServerSavedDic.Remove(curDic.tupleKey);

                    // 실패 시, 로딩 표시
                    if (!result.IsSuccess())
                    {
                        // UIPopup은 메인스레드에서 호출 해야 예외 안 뜸
                        // (Backend.GameData.Update를 폴링방식으로 바꾸기엔 서버 저장 일관성이 깨질 수 있으므로, 비동기 유지 했음)
                        Dispatcher.Instance.BeginInvoke(() =>
                        {
                            // 쓰레드 관련 문제로 이쪽에서 수정. 위에 if 안에 && 를 이용하여 And 로 넣으면, MainThread 문제 발생함.
                            if (LoadingIndicator.IsShow()) return;
                            
                            LoadingIndicator.Show();
                            _isShowLoading = true;

                            var statusCheck = result.GetStatusCode();
                            if (statusCheck == "401" || statusCheck == "403" && result.GetErrorCode() != "Forbidden") // 401 = 점검, 403 = 차단
                            {
                                _isMaintain = true;
                                var errTitle = Localization.GetText(statusCheck == "401" ? "system_006" : "system_010");
                                var errMsg = statusCheck == "401"
                                    ? Localization.GetText("system_007")
                                    : Localization.GetFormatText("system_011", result.GetErrorCode());

                                OkPopup.Alert(errTitle, errMsg, () => { SceneManager.LoadScene("Title"); });
                                Time.timeScale = 0;
                            }
                        });
                    }
                    // 성공 시, 로딩 숨김 (이 클래스에서 띄웠을 때만) 
                    else if (result.IsSuccess() && _isShowLoading)
                    {
                        Dispatcher.Instance.BeginInvoke(() =>
                        {
                            LoadingIndicator.Hide();
                            _isShowLoading = false;
                        });
                    }

                    if (ExtensionMethods.IsUnityEditor() && result.IsSuccess())
                        ULogger.Log($"{curDic.Item1} 테이블 서버 동기화 업데이트 완료");
                });

                // update
                _lastServerSavedDic[curDic.tupleKey] = curDic.tupleValue;
            }
        }

        public IEnumerator<float> SaveToServerWithoutLogin()
        {
            var tableDataDic = AllTableData;

            for (var i = 0; i < tableDataDic.Count; i++)
            {
                var (tupleKey, tupleValue) = tableDataDic.ElementAt(i);

                yield return Timing.WaitUntilDone(BackEndManager.SaveToServerTableData(tupleKey, InDate, tupleValue));
            }
        }

        #endregion

        #region Our Server 
        
        private IEnumerator<float> AutoSaveToOurServer()
        {
            while (true)
            {
                SaveToOurServer();
                
                yield return Timing.WaitForSeconds(SaveToServerDelayForOurServer);
            }
        }

        private void SaveToOurServer()
        {
            var tableDataDic = AllTableData;
            for (var i = 0; i < tableDataDic.Count; i++)
            {
                var (tableName, tableValue) = tableDataDic.ElementAt(i);

                if (_isMaintain) break;
                
                // 테이블 저장 요청
                SaveTableAsync(tableName, tableValue).Forget();
            }
        }

        private static async UniTaskVoid SaveTableAsync(string tableName, string data)
        {
            // 요청
            var req = new HTTPRequest(ServerUri.update, HTTPMethods.Post);
            
            var rawData = new JsonData
            {
                ["tableName"] = tableName,
                ["params"] = new JsonData
                {
                    ["data"] = data
                }
            };

            req.SetHeader("content-type", "application/json; charset=utf-8");
            req.RawData = Encoding.UTF8.GetBytes(rawData.ToJson());
            
            req.FormUsage = HTTPFormUsage.UrlEncoded;
            var rsp = await req.GetHTTPResponseAsync();

            if (rsp.IsSuccess)
            {
                ULogger.Log($"우리 서버 DB 전송 완료! {tableName}");
            }
            else
            {
                ULogger.Log($"우리 서버 DB 전송 실패 {tableName}");
            }
        }

        #endregion
    }
}