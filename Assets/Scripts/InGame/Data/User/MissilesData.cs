﻿using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using Global;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class MissilesData 
        {
            public class MissileData
            {
                public ObscuredInt count = 0;
                public ObscuredInt grade = 0;
                public ObscuredInt level = 1;
                public ObscuredBool isEquipped = false;
            }
            
            private class LitData
            {
                public Dictionary<string, MissileData> missiles = new Dictionary<string, MissileData>();
                public ObscuredInt ancientEnergy = 0;
            }
            private LitData _data = new LitData();
            
            
            
            public MissileData this[string idx] => _data.missiles[idx];
            public int AncientEnergy => _data.ancientEnergy;

            // 이벤트 
            public event StringDelegate OnEarned;
            public event StringDelegate OnLevelUp;
            public event StringDelegate OnEnhance;
            public event StringDelegate OnConsume;
            public event StringDelegate OnEquip;
            public event VoidDelegate OnChangedEnergy;
            public event VoidDelegate OnFromString;

            /*
             * 미사일데이터를 가지고 있다는 것만으로 이미 소유하고 있다고 가정
             * 미사일데이터는 한번 획득되면 영구적이라 isEarned는 사용성 없는 bool이 됨 
             */
            public bool IsEarned(string idx)
            {
                return _data.missiles.ContainsKey(idx);
            }
            
            public IEnumerable<string> GetAllIdxesEarned()
            {
                return _data.missiles.Keys;
            }

            public IEnumerable<string> GetAllIdxesEquipped()
            {
                return from missileKV in _data.missiles where missileKV.Value.isEquipped select missileKV.Key;
            }
            
            public IEnumerable<string> GetAllIdxesUnEquipped()
            {
                return from missileKV in _data.missiles where missileKV.Value.isEquipped == false select missileKV.Key;
            }

            public bool GetState(string idx, out int outCount, out int outGrade, out int outLevel, out bool outIsEquipped)
            {
                outCount = outGrade = outLevel = 0;
                outIsEquipped = false;

                // 미획득이라면 false 반환 
                if (!_data.missiles.TryGetValue(idx, out var missile)) 
                    return false;
                
                outCount = missile.count;
                outGrade = missile.grade;
                outLevel = missile.level;
                outIsEquipped = missile.isEquipped;

                return true;
            }

            public void Earn(string idx, int amount)
            {
                if (!_data.missiles.TryGetValue(idx, out var missile))
                {
                    // 최초 획득 시, 데이타 생성 
                    missile = new MissileData();
                    _data.missiles[idx] = missile;
                }

                missile.count += amount;
                
                OnEarned?.Invoke(idx);
            }
            
            public void Consume(string idx, int amount)
            {
                // 획득 안 한 미사일이라면, 바로 종료
                if (!_data.missiles.TryGetValue(idx, out var missile))
                    return;

                missile.count -= amount;
                
                OnConsume?.Invoke(idx);
            }

            public void Equip(string idx, bool equip)
            {
                // 획득 안 한 미사일이라면, 바로 종료
                if (!IsEarned(idx)) return;

                _data.missiles[idx].isEquipped = equip;
                
                OnEquip?.Invoke(idx);
            }

            public void LevelUp(string idx, int amount)
            {
                // 미획득 미사일이라면 아무것도 안함 
                if (!_data.missiles.TryGetValue(idx, out var missile)) 
                    return;

                missile.level += amount;
                
                OnLevelUp?.Invoke(idx);
            }
            
            public void Enhance(string idx, int amount)
            {
                // 미획득 미사일이라면 아무것도 안함 
                if (!_data.missiles.TryGetValue(idx, out var missile)) 
                    return;

                missile.grade += amount;
                
                OnEnhance?.Invoke(idx);
            }

            public void AddEnergy(int amount)
            {
                _data.ancientEnergy += amount;
                
                OnChangedEnergy?.Invoke();
            }
            
            public void SubEnergy(int amount)
            {
                _data.ancientEnergy -= amount;
                
                OnChangedEnergy?.Invoke();
            }
            
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
                
                // 이벤트 
                OnFromString?.Invoke();
            }
        }
    }
}
