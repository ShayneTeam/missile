﻿using System;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Global;
using InGame.Controller.Event;
using InGame.Global;
using LitJson;
using Server;
using Utils;

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class BattlePassData
        {
            private class BattlePassDataSchema
            {
                public List<BattlePassBaseSchema> _datas = new List<BattlePassBaseSchema>();
            }

            private class BattlePassBaseSchema
            {
                public string battlePassId;
                public ObscuredBool purchased;
                public List<ObscuredInt> rewardIdxs;
            }

            private BattlePassDataSchema _battlePassDatas = new BattlePassDataSchema();

            // 이벤트 
            public event VoidDelegate OnPurchasedBattlePass;

            // 배틀패스 적용
            public void PurchaseApplyBattlePass(string idx)
            {
                var findBattlePassCheck = _battlePassDatas._datas.Find(x => x.battlePassId == idx);

                // 무료면 purchasing 이 되지 않은 상태에서, 아이템을 구매할 수 있는 항목이 있기 때문에 이미 들어가 있을 수 있음.
                if (findBattlePassCheck == null)
                {
                    _battlePassDatas._datas.Add(new BattlePassBaseSchema
                    {
                        battlePassId = idx,
                        purchased = true,
                        rewardIdxs = new List<ObscuredInt>(),
                    });
                }
                else
                {
                    findBattlePassCheck.purchased = true;
                }
            }

            public bool PurchasedCheck(string battlePassId)
            {
                return _battlePassDatas._datas.Exists(x => x.battlePassId == battlePassId && x.purchased);
            }

            /// <summary>
            /// 배틀패스중 획득 가능한 상품이 있는지 확인.
            /// </summary>
            /// <param name="battlePassType">배틀패스 타입</param>
            /// <returns></returns>
            public bool PossibleAnyReceiveCheck(BattlePassMenu.BattlePassType battlePassType)
            {
                var slotSheetName = string.Empty;

                switch (battlePassType)
                {
                    case BattlePassMenu.BattlePassType.None:
                        break;
                    case BattlePassMenu.BattlePassType.SpaceA:
                        slotSheetName = Sheet.BattlePass_1;
                        break;
                    case BattlePassMenu.BattlePassType.SpaceB:
                        slotSheetName = Sheet.BattlePass_2;
                        break;
                    case BattlePassMenu.BattlePassType.MonsterHunterA:
                        slotSheetName = Sheet.MonsterHunter_1;
                        break;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(battlePassType), battlePassType, null);
                }
                
                var idx_list = DataboxController.GetAllIdxes(Table.Event, slotSheetName);

                foreach (var idx in idx_list)
                {
                    var condition = DataboxController.GetDataInt(Table.Event, slotSheetName, idx, "m_value", 99999);
                    var conditionClearCheck = EventController.Instance.HasEventBattlePassToReceive(battlePassType.ToEnumString(), slotSheetName, idx,
                            condition);
                    // 획득 가능하다면 true 리턴 (Loop 종료)
                    if (conditionClearCheck) return true;
                }

                return false;
            }

            // 클리어 체크 (공통)
            public bool CommonConditionCheck(string sheetName, string idx, int condition, out int currentCount)
            {
                var missionType = DataboxController.GetDataString(Table.Event, sheetName, idx, Column.m_type);
                
                // 미션에 존재하지 않는 커스텀
                switch (missionType)
                {
                    case "MISSION_STAGE_CLEAR":
                        currentCount = My.Stage.bestStage;
                        return My.Stage.bestStage > condition;
                        break;
                    default:
                        break;
                }

                // common
                var commonMissions = new List<string> { Sheet.daily, Sheet.battle, Sheet.enchant };
                foreach (var commonMission in commonMissions)
                {
                    var allMissions = DataboxController.GetAllIdxes(Table.Mission, commonMission);
                    foreach (var missionIdx in allMissions)
                    {
                        var dataMissionType = DataboxController.GetDataString(Table.Mission, commonMission, missionIdx, Column.m_type);
                        if (dataMissionType == missionType)
                        {
                            // HACK. CommonMissionType과 Sheet가 일치하다는 전제하에 쓰인 코드
                            var commonType = (MissionData.CommonMissionType)Enum.Parse(typeof(MissionData.CommonMissionType), commonMission);
                            My.Mission.GetState(commonType, missionIdx, out var _, out var count, out _);
                            currentCount = count;
                            return count > condition;
                        }
                    }
                }

                currentCount = 0;
                return false;
            }

            public bool PossibleReceiveCheck(string battlePassType, string sheetName, string idx, int condition,
                bool prePurchaseCheck = false)
            {
                if (!CommonConditionCheck(sheetName, idx, condition, out _)) return false;

                // prePurchase 면 체크 결과만 가지고 return.
                if (prePurchaseCheck) return true;

                var searchUserBattlePassData = _battlePassDatas._datas.Find(x => x.battlePassId == battlePassType);

                // 유료인가
                var itemLock = DataboxController.GetDataString(Table.Event, sheetName, idx, "lock", "TRUE");
                if (itemLock == "TRUE")
                {
                    if (searchUserBattlePassData != null && searchUserBattlePassData.purchased &&
                        !searchUserBattlePassData.rewardIdxs.Contains(condition))
                        return true;
                }
                else // 무료
                {
                    // 상품 관련 데이터가 없거나, 획득한 히스토리에 없으면 획득 가능.
                    return searchUserBattlePassData == null || !searchUserBattlePassData.rewardIdxs.Contains(condition);
                }

                return false;
            }

            public bool ReceivedCheck(string battlePassType, int condition)
            {
                var passData = _battlePassDatas._datas.Find(x => x.battlePassId == battlePassType);
                return passData != null && passData.rewardIdxs.Contains(condition);
            }

            public bool ReceiveReward(string battlePassId, string sheetName, string idx, int condition)
            {
                // 조건을 만족하지 못했다면 return.
                if (!CommonConditionCheck(sheetName, idx, condition, out _)) return false;
                
                // 이미 획득했다면 return false
                if (ReceivedCheck(battlePassId, condition)) return false;

                var passData = _battlePassDatas._datas.Find(x => x.battlePassId == battlePassId);
                if (passData == null)
                {
                    _battlePassDatas._datas.Add(new BattlePassBaseSchema
                    {
                        battlePassId = battlePassId,
                        purchased = false,
                        rewardIdxs = new List<ObscuredInt>
                        {
                            condition
                        }
                    });
                }
                else
                {
                    passData.rewardIdxs.Add(condition);
                    return true;
                }
                
                ULogger.Log($"보상 신규 획득 / BattlePass : {battlePassId} / condition : {condition.ToString()}");
                return true;
            }

            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_battlePassDatas);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;

                _battlePassDatas = JsonMapper.ToObject<BattlePassDataSchema>(from);
            }
        }
    }
}