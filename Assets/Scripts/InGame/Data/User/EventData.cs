﻿using CodeStage.AntiCheat.ObscuredTypes;
using LitJson;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class EventData
        {
            private class DataSchema
            {
                public ObscuredLong nextAttendanceTimestamp = 0;
                public ObscuredInt attendanceCount = 0;
            }
            private DataSchema _data = new DataSchema();
            
            // Properties
            public int AttendanceCount
            {
                get => _data.attendanceCount;
                set => _data.attendanceCount = value;
            }

            public long NextAttendanceTimestamp => _data.nextAttendanceTimestamp;

            // Functions
            public void AddAttendance(long nextTimestamp)
            {
                // 기록되는 타임스탬프 값이 더 커야 기록한다 (해킹방지)
                if (nextTimestamp > _data.nextAttendanceTimestamp)
                    _data.nextAttendanceTimestamp = nextTimestamp;
                
                // 카운트 증가 
                _data.attendanceCount++;
            }

            public void ResetAttendanceTime()
            {
                _data.nextAttendanceTimestamp = 0;
                _data.attendanceCount = 0;
            }
            
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<DataSchema>(from);
            }
        }
    }
}