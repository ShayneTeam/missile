﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using Server;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class TutorialData
        {
            private class LitData
            {
                public Dictionary<string, ObscuredBool> flags = new Dictionary<string, ObscuredBool>();
            }
            private LitData _info = new LitData();

            public bool GetFlag(string key, bool defaultValue = false)
            {
                if (_info.flags.TryGetValue(key, out var value))
                    return value;

                return defaultValue;
            }

            public void SetFlag(string key, bool value)
            {
                _info.flags[key] = value;
            }
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_info);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _info = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}