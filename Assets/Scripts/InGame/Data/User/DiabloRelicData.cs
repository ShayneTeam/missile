﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class DiabloRelicData 
        {
            public class RelicData
            {
                public ObscuredInt level = 1;
                public ObscuredInt count = 1;
            }
            
            private class LitData
            {
                public Dictionary<string, RelicData> dRelics = new Dictionary<string, RelicData>();
            }
            private LitData _data = new LitData();
            
            
            public RelicData this[string idx] => _data.dRelics[idx];

            public event StringDelegate OnLevelUp;
            public event StringDelegate OnEarned;
            public event StringDelegate OnConsume;
            public event VoidDelegate OnFromString;

            /*
             * 미사일데이터를 가지고 있다는 것만으로 이미 소유하고 있다고 가정
             * 미사일데이터는 한번 획득되면 영구적이라 isEarned는 사용성 없는 bool이 됨 
             */
            public bool IsEarned(string dRelicIdx)
            {
                return _data.dRelics.ContainsKey(dRelicIdx);
            }

            public void Earn(string dRelicIdx)
            {
                // 이미 추가돼있다면 덮어씌우지 않음 
                if (!_data.dRelics.TryGetValue(dRelicIdx, out var dRelic))
                {
                    _data.dRelics[dRelicIdx] = new RelicData();
                }
                else
                {
                    dRelic.count++;
                }
                
                OnEarned?.Invoke(dRelicIdx);
            }

            public IEnumerable<string> GetAllEarned()
            {
                return _data.dRelics.Keys;
            }
            
            public void LevelUp(string dRelicIdx)
            {
                if (!_data.dRelics.TryGetValue(dRelicIdx, out var relic))
                    return;

                relic.level++;
                
                OnLevelUp?.Invoke(dRelicIdx);
            }
            
            public void Consume(string dRelicIdx, int amount)
            {
                // 획득 안 했다면, 바로 종료
                if (!_data.dRelics.TryGetValue(dRelicIdx, out var relic))
                    return;

                relic.count -= amount;
                
                OnConsume?.Invoke(dRelicIdx);
            }

            public bool GetState(string dRelicIdx, out int outLevel, out int outCount)
            {
                outLevel = outCount = -1;
                
                if (!_data.dRelics.TryGetValue(dRelicIdx, out var relic))
                    return false;

                outLevel = relic.level;
                outCount = relic.count;

                return true;
            }

            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
                
                // 이벤트 
                OnFromString?.Invoke();
            }
        }
    }
}
