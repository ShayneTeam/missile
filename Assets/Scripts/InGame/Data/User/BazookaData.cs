﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using Firebase.Analytics;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable FieldCanBeMadeReadOnly.Local
// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class BazookaData
        {
            private class BazookaSchema
            {
                public ObscuredInt grade = 1;
                public ObscuredInt level = 1;
                public string equippedBazooka = ""; // empty로 넣어놔야 SetDataAndSaveToLocal에서 null이라 return 되지 않음
                public List<string> ownedBazookas = new List<string>();
                public Dictionary<string, ObscuredInt> parts = new Dictionary<string, ObscuredInt>();
            }

            private BazookaSchema _bazooka = new BazookaSchema();


            public event StringDelegate OnEarned;
            public event VoidDelegate OnEnhance;
            public event IntDelegate OnLevelUp;
            public event StringIntDelegate OnLevelUpParts;
            public event StringDelegate OnEquipped;
            public event VoidDelegate OnFromString;

            public void GetState(out string outEquippedBazookaIdx, out int outGrade, out int outLevel)
            {
                outEquippedBazookaIdx = _bazooka.equippedBazooka;
                outGrade = _bazooka.grade;
                outLevel = _bazooka.level;
            }

            public void Earn(string idx)
            {
                if (IsEarned(idx))
                    return;

                _bazooka.ownedBazookas.Add(idx);

                OnEarned?.Invoke(idx);
            }

            public bool IsEarned(string idx)
            {
                return _bazooka.ownedBazookas.Contains(idx);
            }

            public void Equip(string idx)
            {
                // 획득 안 한 미사일이라면, 바로 종료
                if (!IsEarned(idx))
                    return;

                _bazooka.equippedBazooka = idx;

                OnEquipped?.Invoke(idx);
            }

            public void Enhance(int amount)
            {
                _bazooka.grade += amount;

                OnEnhance?.Invoke();
            }

            public void LevelUp(int count)
            {
                _bazooka.level += count;

                OnLevelUp?.Invoke(count);
            }

            public int GetPartsLevel(string idx)
            {
                if (_bazooka.parts.ContainsKey(idx))
                    return _bazooka.parts[idx];

                return 1; // 파츠 기본 레벨 1
            }

            public void LevelUpParts(string idx, int count)
            {
                _bazooka.parts[idx] = GetPartsLevel(idx) + count;

                OnLevelUpParts?.Invoke(idx, count);
            }


            public override string ToString()
            {
                return JsonMapper.ToJson(_bazooka);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;

                _bazooka = JsonMapper.ToObject<BazookaSchema>(from);
                
                // 이벤트 
                OnFromString?.Invoke();
            }
        }
    }
}