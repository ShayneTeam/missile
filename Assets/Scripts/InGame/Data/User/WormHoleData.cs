﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class WormHoleData
        {
            private class LitData
            {
                public ObscuredInt bestDifficulty = 0;
                public ObscuredInt difficultyCursor = 1;
                
                public List<ObscuredInt> clearedDifficulties = new List<ObscuredInt>();
            }
            private LitData _data = new LitData();
            
            // Properties
            public int DifficultyCursor
            {
                get => _data.difficultyCursor;
                set => _data.difficultyCursor = value;
            }

            public int BestDifficulty => _data.bestDifficulty;

            // Events
            
            // Functions
            public bool IsCleared(int difficulty)
            {
                return _data.clearedDifficulties.Contains(difficulty);
            }

            public void Clear(int difficulty)
            {
                // 이미 클리어했다면, 종료
                if (IsCleared(difficulty))
                    return;
                
                _data.clearedDifficulties.Add(difficulty);
                
                // 최고 클리어 난이도 갱신  
                if (difficulty > _data.bestDifficulty)
                {
                    _data.bestDifficulty = difficulty;
                }
            }
            
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}