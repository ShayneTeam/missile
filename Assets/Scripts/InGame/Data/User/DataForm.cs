﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class DataForm
        {
            private class DataSchema
            {
                public ObscuredInt equippedCostume = 1;
                
                public List<ObscuredDouble> ownedCostume = new List<ObscuredDouble>();
            }
            private DataSchema _data = new DataSchema();
            
            // Properties
            
            // Events
            
            // Functions
            
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<DataSchema>(from);
            }
        }
    }
}