﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public static class CostumeTypeExtensions
    {
        public static string ToEnumString(this UserData.CostumeType type)
        {
            return type switch
            {
                UserData.CostumeType.head => nameof(UserData.CostumeType.head),
                UserData.CostumeType.skin => nameof(UserData.CostumeType.skin),
                UserData.CostumeType.body => nameof(UserData.CostumeType.body),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
    
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        // 일부러 string 비교 위해 소문자로 해둔 것
        [SuppressMessage("ReSharper", "InconsistentNaming")]
        public enum CostumeType
        {
            head,
            skin,
            body
        }
        
        public class CostumeData 
        {
            private class CostumeParts
            {
                public string equipped = "";
                
                public List<string> owned = new List<string>();
            }
            private class CostumeSchema
            {
                public CostumeParts head = new CostumeParts();
                public CostumeParts skin = new CostumeParts();
                public CostumeParts body = new CostumeParts();
            }
            private CostumeSchema _costume = new CostumeSchema();
            
            
            
            public event StringDelegate OnEarned;
            public event StringDelegate OnEquipped;
            public event VoidDelegate OnFromString;

            private CostumeParts GetCostume(CostumeType type)
            {
                switch (type)
                {
                    case CostumeType.head:
                        return _costume.head;
                    case CostumeType.skin:
                        return _costume.skin;
                    case CostumeType.body:
                        return _costume.body;
                    default:
                        throw new ArgumentOutOfRangeException(nameof(type), type, null);
                }
            }
            
            public void Earn(CostumeType type, string idx)
            {
                if (IsEarned(type, idx))
                    return;
                    
                GetCostume(type).owned.Add(idx);
                
                OnEarned?.Invoke(idx);
            }
            
            public bool IsEarned(CostumeType type, string idx)
            {
                return GetCostume(type).owned.Contains(idx);
            }
            
            public void Equip(CostumeType type, string idx)
            {
                if (!IsEarned(type, idx))
                    return;

                GetCostume(type).equipped = idx;
                
                OnEquipped?.Invoke(idx);
            }

            public string GetEquipped(CostumeType type)
            {
                return GetCostume(type).equipped;
            }

            public IEnumerable<string> GetAllEarned(CostumeType type)
            {
                return GetCostume(type).owned.Select(costume => (string) costume);
            }
            
            
            public override string ToString()
            {
                return JsonMapper.ToJson(_costume);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _costume = JsonMapper.ToObject<CostumeSchema>(from);
                
                // 이벤트 
                OnFromString?.Invoke();
            }
        }
    }
}