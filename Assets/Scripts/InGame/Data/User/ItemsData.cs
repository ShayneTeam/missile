﻿using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable FieldCanBeMadeReadOnly.Local

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        // 카테고라이징 돼있지 않은 것들
        public class ItemsData 
        {
            private class LitData
            {
                public Dictionary<string, ObscuredInt> items = new Dictionary<string, ObscuredInt>();
            }
            private LitData _data = new LitData();
            
            public int this[string idx]
            {
                get
                {
                    if (_data.items.ContainsKey(idx))
                        return _data.items[idx];

                    return 0;
                } 
                set
                {
                    // 0 밑으로 안 떨어지게 함
                    _data.items[idx] = value < 0 ? 0 : value;
                    
                    OnChangedItem?.Invoke();
                }
            }
            
            public event VoidDelegate OnChangedItem;
            
            // Functions
            public bool HasEverHad(string idx)
            {
                return _data.items.ContainsKey(idx);
            }
            
            // Save & Load
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}