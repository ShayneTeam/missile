﻿using System;
using System.Collections;
using System.Collections.Generic;
using BackEnd;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using Global;
using LitJson;
using Server;
using UnityEngine;
using Utils;

// ReSharper disable FieldCanBeMadeReadOnly.Local

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global

namespace InGame.Data
{
    public static class SkillModeExtensions
    {
        public static string ToEnumString(this UserData.SkillMode type)
        {
            return type switch
            {
                UserData.SkillMode.Stage => nameof(UserData.SkillMode.Stage),
                UserData.SkillMode.WormHole => nameof(UserData.SkillMode.WormHole),
                UserData.SkillMode.Plunder => nameof(UserData.SkillMode.Plunder),
                UserData.SkillMode.Raid => nameof(UserData.SkillMode.Raid),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
    
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public enum SkillMode
        {
            Stage,
            WormHole,
            Plunder,
            Raid,
        }
        
        public class SkillsData
        {
            private class LitData
            {
                public Dictionary<string, Dictionary<string, ObscuredLong>> timestamps = new Dictionary<string, Dictionary<string, ObscuredLong>>();
            }
            private LitData _skills = new LitData();
            
            private readonly Dictionary<string, Dictionary<string, ObscuredLong>> _timestampsScaled = new Dictionary<string, Dictionary<string, ObscuredLong>>();

            public void StampTime(SkillMode mode, string idx)
            {
                // 스탬프 찍기 
                
                // 런타임 배속 적용되지 않은 순정 타임스탬프
                // 게임 시작하고, 런타임 배속 타임스탬프가 없을 때만, 이 타임스탬프를 사용한다 
                // 그 때는 NowSec과 NowSecUnscaled의 차이가 없는 타이밍임 
                GetModeTimestamps(mode)[idx] = ServerTime.Instance.NowSecUnscaled;
                
                // 런타임 배속이 적용된 타임스탬프
                // 이건 런타임 안에서만 사용된다 
                // 따로 서버에 저장하지 않는다 
                GetModeTimestampsScaled(mode)[idx] = ServerTime.Instance.NowSec;
            }

            public long GetTimestamp(SkillMode mode, string idx)
            {
                // 런타임 타임스탬프가 있다면 얘를 가져온다 
                if (GetModeTimestampsScaled(mode).TryGetValue(idx, out var timestampScaled))
                {
                    return timestampScaled;
                }
                
                // 없다면 언스케일 타임스탬프 반환 
                if (GetModeTimestamps(mode).TryGetValue(idx, out var timestamp))
                {
                    return timestamp;
                }
                else
                {
                    return 0;
                }
            }

            public void ResetTimeStamp(SkillMode mode, string idx)
            {
                GetModeTimestamps(mode)[idx] = 0;
                GetModeTimestampsScaled(mode)[idx] = 0;
            }
            
            private Dictionary<string, ObscuredLong> GetModeTimestamps(SkillMode mode)
            {
                // 찾아서 있으면 그거 반환 
                if (_skills.timestamps.TryGetValue(mode.ToEnumString(), out var modeTimestamps))
                {
                    return modeTimestamps;
                }
                
                // 없다면 새로 만들기 
                var newModeTimestamps = new Dictionary<string, ObscuredLong>();
                _skills.timestamps[mode.ToEnumString()] = newModeTimestamps;

                return newModeTimestamps;
            }
            
            private Dictionary<string, ObscuredLong> GetModeTimestampsScaled(SkillMode mode)
            {
                // 찾아서 있으면 그거 반환 
                if (_timestampsScaled.TryGetValue(mode.ToEnumString(), out var modeTimestamps))
                {
                    return modeTimestamps;
                }
                
                // 없다면 새로 만들기 
                var newModeTimestamps = new Dictionary<string, ObscuredLong>();
                _timestampsScaled[mode.ToEnumString()] = newModeTimestamps;

                return newModeTimestamps;
            }

            public override string ToString()
            {
                return JsonMapper.ToJson(_skills);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _skills = JsonMapper.ToObject<LitData>(from);
                
                // EnterPlayMode용 초기화
                // UserData가 유지되서 이전 실행에서의 Scaled 타임스탬프가 계속 남아있게 됨 (미래로 앞서 가 있음)
                // UserData Sync될 때, Scaled 타임스탬프 다 날려서, Sync된 후에는 UnScaled 타임스탬프가 쓰이도록 함
                _timestampsScaled.Clear();
            }
        }
    }
}