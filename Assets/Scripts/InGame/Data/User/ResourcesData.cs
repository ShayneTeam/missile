﻿using CodeStage.AntiCheat.ObscuredTypes;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class ResourcesData
        {
            private class LitData
            {
                public ObscuredDouble mineral = 0;
                public ObscuredDouble gas = 0;
                public ObscuredDouble stone = 0;
                public ObscuredDouble uranium = 0;
                public ObscuredInt diablokey = 0;
                public ObscuredInt killpoint = 0;
                public ObscuredInt mileage = 0;
            }
            private LitData _resources = new LitData();

            public event DoubleDelegate OnChangedMineral;
            public event DoubleDelegate OnChangedGas;
            public event DoubleDelegate OnChangedStone;
            public event DoubleDelegate OnChangedUranium;
            public event DoubleDelegate OnChangedMileage;
            public event VoidDelegate OnChangedKillpoint;
            public event VoidDelegate OnEarnedKillpoint;
            
            public event VoidDelegate OnEarnedDiabloKey;
            public event VoidDelegate OnChangedDiabloKey;

            public double mineral
            {
                get => _resources.mineral;
                set
                {
                    _resources.mineral = value;
                    
                    OnChangedMineral?.Invoke(value);
                }
            }

            public double gas
            {
                get => _resources.gas;
                set
                {
                    _resources.gas = value;
                    
                    OnChangedGas?.Invoke(value);
                }
            }

            public double stone
            {
                get => _resources.stone;
                set
                {
                    _resources.stone = value;
                    
                    OnChangedStone?.Invoke(value);
                }
            }

            public double uranium
            {
                get => _resources.uranium;
                set
                {
                    _resources.uranium = value;
                    
                    OnChangedUranium?.Invoke(value);
                }
            }

            public int diablokey
            {
                get => _resources.diablokey;
                set
                {
                    if (value > _resources.diablokey)
                    {
                        OnEarnedDiabloKey?.Invoke();
                    }
                    
                    _resources.diablokey = value;
                    
                    OnChangedDiabloKey?.Invoke();
                }
            }
            
            public int killpoint
            {
                get => _resources.killpoint;
                set
                {
                    // 최대 값 이상 획득 불가
                    if (value > 999)
                        value = 999;
                    
                    if (value > _resources.killpoint)
                    {
                        OnEarnedKillpoint?.Invoke();
                    }
                    
                    _resources.killpoint = value;
                    
                    OnChangedKillpoint?.Invoke();
                }
            }

            public int mileage
            {
                get => _resources.mileage;
                set
                {
                    _resources.mileage = value;
                    
                    OnChangedMileage?.Invoke(value);
                }
            }


            public override string ToString()
            {
                return JsonMapper.ToJson(_resources);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _resources = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}