﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class SoldiersData 
        {
            public class SoldierSchema
            {
                public ObscuredInt grade = 1;
                public ObscuredInt level = 1;
            }
            
            private class SoldiersSchema
            {
                public Dictionary<string, SoldierSchema> soldiers = new Dictionary<string, SoldierSchema>();
            }
            private SoldiersSchema _data = new SoldiersSchema();
            
            
            
            public SoldierSchema this[string idx] => _data.soldiers[idx];

            public event StringDelegate OnPromoted;
            public event StringDelegate OnLevelUp;
            public event StringDelegate OnEarned;
            public event VoidDelegate OnFromString;

            /*
             * 미사일데이터를 가지고 있다는 것만으로 이미 소유하고 있다고 가정
             * 미사일데이터는 한번 획득되면 영구적이라 isEarned는 사용성 없는 bool이 됨 
             */
            public bool IsEarned(string type)
            {
                return _data.soldiers.ContainsKey(type);
            }

            public void Earn(string type)
            {
                // 이미 추가돼있다면 덮어씌우지 않음 
                if (IsEarned(type))
                    return;
                
                _data.soldiers[type] = new SoldierSchema();
                
                OnEarned?.Invoke(type);
            }

            public IEnumerable<string> GetAllEarnedTypes()
            {
                return _data.soldiers.Keys;
            }

            public void Promote(string type)
            {
                if (!_data.soldiers.TryGetValue(type, out var soldier))
                    return;

                soldier.grade++;
                
                OnPromoted?.Invoke(type);
            }
            
            public void LevelUp(string type, int count)
            {
                if (!_data.soldiers.TryGetValue(type, out var soldier))
                    return;

                soldier.level += count;
                
                OnLevelUp?.Invoke(type);
            }

            public void GetState(string type, out int outGrade, out int outLevel)
            {
                outGrade = outLevel = 1;
                
                if (!_data.soldiers.TryGetValue(type, out var soldier))
                    return;

                outGrade = soldier.grade;
                outLevel = soldier.level;
            }

            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<SoldiersSchema>(from);
                
                // 이벤트 
                OnFromString?.Invoke();
            }
        }
    }
}
