﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class ThirtyDaysGiftData
        {
            private class DataSchema
            {
                public ObscuredInt collectedCount = 0;
            }

            private DataSchema _data = new DataSchema();

            // Properties
            public int CollectedCount => _data.collectedCount;

            // Events
            public event VoidDelegate OnChangedCollectedCount;

            // Functions
            public void Collect()
            {
                _data.collectedCount++;
                
                OnChangedCollectedCount?.Invoke();
            }

            public void Reset()
            {
                _data.collectedCount = 0;
                
                OnChangedCollectedCount?.Invoke();
            }


            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;

                _data = JsonMapper.ToObject<DataSchema>(from);
            }
        }
    }
}