﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class NormalRelicData 
        {
            public class RelicData
            {
                public ObscuredInt level = 1;
            }
            
            private class LitData
            {
                public Dictionary<string, RelicData> relics = new Dictionary<string, RelicData>();
            }
            private LitData _data = new LitData();
            
            
            public RelicData this[string idx] => _data.relics[idx];

            public event StringIntDelegate OnLevelUp;
            public event StringDelegate OnEarned;
            public event VoidDelegate OnFromString;

            /*
             * 미사일데이터를 가지고 있다는 것만으로 이미 소유하고 있다고 가정
             * 미사일데이터는 한번 획득되면 영구적이라 isEarned는 사용성 없는 bool이 됨 
             */
            public bool IsEarned(string idx)
            {
                return _data.relics.ContainsKey(idx);
            }

            public void Earn(string idx)
            {
                // 이미 추가돼있다면 덮어씌우지 않음 
                if (IsEarned(idx))
                    return;
                
                _data.relics[idx] = new RelicData();
                
                OnEarned?.Invoke(idx);
            }

            public IEnumerable<string> GetAllEarned()
            {
                return _data.relics.Keys;
            }
            
            public void LevelUp(string idx, int count)
            {
                if (!_data.relics.TryGetValue(idx, out var relic))
                    return;

                relic.level += count;
                
                OnLevelUp?.Invoke(idx, count);
            }

            public bool GetState(string type, out int outLevel)
            {
                outLevel = 1;
                
                if (!_data.relics.TryGetValue(type, out var soldier))
                    return false;

                outLevel = soldier.level;

                return true;
            }

            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
                
                // 이벤트 
                OnFromString?.Invoke();
            }
        }
    }
}
