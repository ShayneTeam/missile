﻿using System.Collections;
using System.Collections.Generic;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable FieldCanBeMadeReadOnly.Local

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class BoxesData 
        {
            private class LitData
            {
                public Dictionary<string, ObscuredInt> boxes = new Dictionary<string, ObscuredInt>();
            }
            private LitData _data = new LitData();
            
            // 이벤트 
            public event VoidDelegate OnChangedBox;
            
            // 인덱서 
            public int this[string idx]
            {
                get
                {
                    if (_data.boxes.ContainsKey(idx))
                        return _data.boxes[idx];

                    return 0;
                } 
                set
                {
                    // 0 밑으로 안 떨어지게 함
                    _data.boxes[idx] = value < 0 ? 0 : value;
                    
                    // 통지
                    OnChangedBox?.Invoke();
                }
            }

            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }

            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}