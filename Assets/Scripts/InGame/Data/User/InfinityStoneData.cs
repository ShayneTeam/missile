﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class InfinityStoneData
        {
            public class StoneSchema
            {
                public ObscuredInt level = 1;
                public ObscuredInt amount = 0;
            }
            
            private class DataSchema
            {
                public ObscuredInt helmetLevel = 1;
                
                public Dictionary<string, StoneSchema> stones = new Dictionary<string, StoneSchema>();
            }
            private DataSchema _data = new DataSchema();
            
            // Events
            public event VoidDelegate OnLevelUpHelmet;
            public event StringDelegate OnLevelUpStone;
            public event VoidDelegate OnChangedStoneAmount;
            public event VoidDelegate OnFromString;

            // Helmet
            public int HelmetLevel => _data.helmetLevel;

            public void LevelUpHelmet()
            {
                _data.helmetLevel++;
                
                OnLevelUpHelmet?.Invoke();
            }
            
            // Stone
            public void GetStoneState(string idx, out int level, out int amount)
            {
                if (!_data.stones.TryGetValue(idx, out var stone))
                {
                    // 없으면 만들기 
                    stone = new StoneSchema();
                    _data.stones[idx] = stone;
                }

                level = stone.level;
                amount = stone.amount;
            }
            
            public void LevelUpStone(string idx)
            {
                if (!_data.stones.TryGetValue(idx, out var stone))
                    return;

                stone.level++;
                
                OnLevelUpStone?.Invoke(idx);
            }
            
            public void AddStone(string idx, int count)
            {
                if (!_data.stones.TryGetValue(idx, out var stone))
                    return;

                stone.amount += count;
                
                OnChangedStoneAmount?.Invoke();
            }
            
            public void SubtractStone(string idx, int count)
            {
                if (!_data.stones.TryGetValue(idx, out var stone))
                    return;

                stone.amount -= count;
                
                OnChangedStoneAmount?.Invoke();
            }

            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<DataSchema>(from);
                
                // 이벤트 
                OnFromString?.Invoke();
            }
        }
    }
}