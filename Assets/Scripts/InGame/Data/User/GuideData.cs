﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class GuideData
        {
            private class DataSchema
            {
                //public Dictionary<string, Dictionary<string, MissionSchema>> missions = new Dictionary<string, Dictionary<string, MissionSchema>>();
                public Dictionary<string, ObscuredBool> guides = new Dictionary<string, ObscuredBool>();
            }
            private DataSchema _data = new DataSchema();
            
            // Properties
            
            // Events
            
            // Functions
            public void ResetGuide(string idx)
            {
                _data.guides[idx] = false;
            }
            
            public void MarkGuide(string idx)
            {
                _data.guides[idx] = true;
            }
            
            public bool IsMarkedGuide(string idx)
            {
                if (_data.guides.TryGetValue(idx, out var value))
                {
                    return value;
                }

                return false;
            }
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<DataSchema>(from);
            }
        }
    }
}