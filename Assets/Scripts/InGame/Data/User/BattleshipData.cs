﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class BattleshipData
        {
            private class DataSchema
            {
                public ObscuredInt battleshipCursor = 0;
                
                public ObscuredInt bestDifficulty = 0;
                public ObscuredInt difficultyCursor = 1;

                public List<ObscuredInt> completedDifficulties = new List<ObscuredInt>();
            }
            private DataSchema _data = new DataSchema();
            
            // Properties
            public int BattleshipCursor
            {
                get => _data.battleshipCursor;
                set => _data.battleshipCursor = value;
            }
            
            public int DifficultyCursor
            {
                get => _data.difficultyCursor;
                set => _data.difficultyCursor = value;
            }

            public int BestDifficulty => _data.bestDifficulty;
            
            // Events
            
            // Functions
            public bool HasCompleted(int difficulty)
            {
                return _data.completedDifficulties.Contains(difficulty);
            }

            public void Complete(int difficulty)
            {
                // 이미 클리어했다면, 종료
                if (HasCompleted(difficulty)) return;
                
                _data.completedDifficulties.Add(difficulty);
                
                // 최고 클리어 난이도 갱신  
                if (difficulty > _data.bestDifficulty)
                {
                    _data.bestDifficulty = difficulty;
                }
            }
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<DataSchema>(from);
            }
        }
    }
}