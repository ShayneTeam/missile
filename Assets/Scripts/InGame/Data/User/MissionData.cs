﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;
// ReSharper disable FieldCanBeMadeReadOnly.Local

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global


namespace InGame.Data
{
    public static class CommonMissionTypeExtensions
    {
        public static string ToEnumString(this UserData.MissionData.CommonMissionType type)
        {
            return type switch
            {
                UserData.MissionData.CommonMissionType.daily => nameof(UserData.MissionData.CommonMissionType.daily),
                UserData.MissionData.CommonMissionType.battle => nameof(UserData.MissionData.CommonMissionType.battle),
                UserData.MissionData.CommonMissionType.enchant => nameof(UserData.MissionData.CommonMissionType.enchant),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
    
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class MissionData
        {
            public delegate void CommonMissionDelegate(CommonMissionType type);
            
            public enum CommonMissionType
            {
                // 가이드미션은 다른 데이터 구조로 구현됨. 그러므로 이 타입에 포함시키지 않음 
                daily,
                battle, 
                enchant
            }

            private class MissionSchema
            {
                public ObscuredInt Value = 0;
                public ObscuredInt AccumulatedValue = 0;
                public ObscuredBool IsEarned = false;
            }

            private class LitData
            {
                // guide
                public string currentGuideIdx = "";
                public ObscuredInt currentGuideValue = 0;   // 가이드 미션은 하나씩 진행되므로, 획득여부를 갖는 MissionSchema 쓰지 않음
                
                // common (daily, battle, enchant)
                public Dictionary<string, Dictionary<string, MissionSchema>> missions = new Dictionary<string, Dictionary<string, MissionSchema>>();
            }
            private LitData _data = new LitData();
            
            // Events
            public event VoidDelegate OnLoaded;
            public event VoidDelegate OnChangedGuide;
            public event CommonMissionDelegate OnChangedCommon;
            
            // guide
            public void GetGuideState(out string outIdx, out int outValue)
            {
                outIdx = _data.currentGuideIdx;
                outValue = _data.currentGuideValue;
            }

            public void ChangeGuide(string idx)
            {
                _data.currentGuideIdx = idx;
                _data.currentGuideValue = 0;
                
                OnChangedGuide?.Invoke();
            }

            public void AddGuideValue(int count = 1)
            {
                _data.currentGuideValue += count;
                
                OnChangedGuide?.Invoke();
            }

            // daily
            public void ResetDaily()
            {
                GetMissions(CommonMissionType.daily).Clear();
                
                OnChangedCommon?.Invoke(CommonMissionType.daily);
            }

            // common
            public void GetState(CommonMissionType type, string idx, out int outValue, out int accumulatedValue, out bool outIsEarned)
            {
                var missions = GetMissions(type);
                
                outValue = 0;
                outIsEarned = false;
                accumulatedValue = 0;
                
                if (missions.TryGetValue(idx, out var mission))
                {
                    outValue = mission.Value;
                    accumulatedValue = mission.AccumulatedValue;
                    outIsEarned = mission.IsEarned;
                }
            }
            
            public void AddValue(CommonMissionType type, string idx, int count = 1)
            {
                var missions = GetMissions(type);
                
                // 없으면 새로 만들기
                if (!missions.ContainsKey(idx))
                {
                    var newMission = new MissionSchema();
                    missions[idx] = newMission;
                }
                
                missions[idx].Value += count;
                missions[idx].AccumulatedValue += count;
                
                OnChangedCommon?.Invoke(type);
            }
            
            public void SubtractValue(CommonMissionType type, string idx, int count)
            {
                var missions = GetMissions(type);
                
                // 없으면 그냥 종료
                if (!missions.ContainsKey(idx))
                {
                    return;
                }
                
                missions[idx].Value -= count;
                
                OnChangedCommon?.Invoke(type);
            }

            public void Earn(CommonMissionType type, string idx)
            {
                var missions = GetMissions(type);
                
                // 없으면 새로 만들기
                if (!missions.ContainsKey(idx))
                {
                    var newMission = new MissionSchema();
                    missions[idx] = newMission;
                }

                missions[idx].IsEarned = true;
                
                OnChangedCommon?.Invoke(type);
            }
            
            private Dictionary<string, MissionSchema> GetMissions(CommonMissionType type)
            {
                // int 키로 저장된 게 없으면, string 키로 저장된 거 찾기  
                if (_data.missions.TryGetValue(type.ToEnumString(), out var missions))
                {
                    return missions;
                }
                
                // 없다면 새로 만들기 
                var newMissions = new Dictionary<string, MissionSchema>();
                _data.missions[type.ToEnumString()] = newMissions;

                return newMissions;
            }

            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
                
                OnLoaded?.Invoke();
            }
        }
    }
}