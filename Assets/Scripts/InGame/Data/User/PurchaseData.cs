﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using Server;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class PurchaseData
        {
            private class PurchaseSchema
            {
                public Dictionary<string, ObscuredLong> lastPurchasedTimestamps = new Dictionary<string, ObscuredLong>();
                public Dictionary<string, ObscuredLong> lastResetTimestamps = new Dictionary<string, ObscuredLong>();
                public Dictionary<string, ObscuredInt> purchasedCount = new Dictionary<string, ObscuredInt>();
                
                public ObscuredInt gachaAdExp = 0;
            }
            private PurchaseSchema _purchases = new PurchaseSchema();
            
            // 이벤트 
            public event VoidDelegate OnAddedPurchasedCount;
            public event VoidDelegate OnUpdatedLastPurchasedTime;
            public event VoidDelegate OnChangedGachaExp;
            
            
            // 가차 경험치 
            public int GachaExp
            {
                get => _purchases.gachaAdExp;
                set
                {
                    _purchases.gachaAdExp = value;
                    
                    OnChangedGachaExp?.Invoke();
                }
            }

            // 가장 최근 초기화 날짜의 타임스탬프 
            public void StampLastResetTime(string idx)
            {
                _purchases.lastResetTimestamps[idx] = ServerTime.Instance.NowSecUnscaled;
            }

            public long GetLastResetTimestamp(string idx)
            {
                return _purchases.lastResetTimestamps.TryGetValue(idx, out var timestamp) ? timestamp : 0;
            }
            
            // 가장 최근 구매일 타임스탬프 
            public void StampLastPurchasedTime(string idx)
            {
                _purchases.lastPurchasedTimestamps[idx] = ServerTime.Instance.NowSecUnscaled;
                
                // 이벤트 
                OnUpdatedLastPurchasedTime?.Invoke();
            }

            public long GetLastPurchasedTimestamp(string idx)
            {
                return _purchases.lastPurchasedTimestamps.TryGetValue(idx, out var timestamp) ? timestamp : 0;
            }

            // 구매 횟수
            public int GetPurchasedCount(string idx)
            {
                 return _purchases.purchasedCount.TryGetValue(idx, out var value) ? (int)value : 0;
            }

            public void AddPurchasedCount(string idx)
            {
                if (!_purchases.purchasedCount.ContainsKey(idx))
                    _purchases.purchasedCount[idx] = 0;
                        
                _purchases.purchasedCount[idx]++;
                
                // 이벤트 
                OnAddedPurchasedCount?.Invoke();
            }

            public void ResetPurchaseCount(string idx)
            {
                _purchases.purchasedCount[idx] = 0;
            }
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_purchases);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _purchases = JsonMapper.ToObject<PurchaseSchema>(from);
            }
        }
    }
}