﻿using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Firebase.Analytics;
using Global;
using Global.Extensions;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class StageData
        {
            private class LitData
            {
                // 현재 스테이지
                public ObscuredInt currentStage = 1;
            
                // 도달한 최고 스테이지
                public ObscuredInt bestStage = 1;

                // 보유 행성
                public List<string> ownedPlanets = new List<string>();
                
                // 반복모드 
                public ObscuredBool isRepeatMode = false;
            }
            
            private LitData _data = new LitData();

            public event IntDelegate OnChangedBestStage;
            public event IntDelegate OnChangedCurrentStage;
            public event VoidDelegate OnOwnPlanet;
            
            // 현재 스테이지
            public int currentStage
            {
                get => _data.currentStage;
                set
                {
                    // 도달 최고 스테이지 값보다 크다면, 도달 최고 스테이지값 갱신 
                    if (value > bestStage)
                        bestStage = value;
                    
                    _data.currentStage = value;
                    
                    OnChangedCurrentStage?.Invoke(value);
                }
            }

            public int bestStage
            {
                get => _data.bestStage;
                private set
                {
                    // 갱신
                    _data.bestStage = value;

                    // 이벤트
                    OnChangedBestStage?.Invoke(value);
                }
            }

            public bool IsRepeatMode
            {
                get => _data.isRepeatMode;
                set => _data.isRepeatMode = value;
            }

            public void Earn(string planetIdx)
            {
                if (IsOwned(planetIdx))
                    return;
                
                _data.ownedPlanets.Add(planetIdx);
                
                OnOwnPlanet?.Invoke();
            }
            
            public bool IsOwned(string planetIdx)
            {
                return _data.ownedPlanets.Contains(planetIdx);
            }
            
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}