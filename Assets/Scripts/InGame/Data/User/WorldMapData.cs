﻿using System.Collections.Generic;
using System.Linq;
using BackEnd;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using Global;
using LitJson;
using Server;
using UnityEngine;
using Utils;

// ReSharper disable MemberCanBePrivate.Global
// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class WorldMapData
        {
            private class LitData
            {
                public ObscuredInt factoryGasLevel = 1;

                public ObscuredInt factoryStoneLevel = 1;
                
                // 행성 타임스탬프
                public Dictionary<string, ObscuredLong> planetsTimestamp = new Dictionary<string, ObscuredLong>();
            }
            
            private LitData _data = new LitData();
            
            public event VoidDelegate OnLevelUpGasFactory;
            public event VoidDelegate OnLevelUpStoneFactory;


            public void GetState(out int outFactoryGasLevel, out int outFactoryStoneLevel)
            {
                outFactoryGasLevel = _data.factoryGasLevel;
                outFactoryStoneLevel = _data.factoryStoneLevel;
            }
            
            public void StampTime(string planetIdx)
            {
                // 스탬프 찍기 
                _data.planetsTimestamp[planetIdx] = ServerTime.Instance.NowSecUnscaled;
            }

            public Dictionary<string, long> GetAllPlanetTimestamp()
            {
                var newDic = new Dictionary<string, long>();
                _data.planetsTimestamp.ForEach(planet => newDic.Add(planet.Key, planet.Value));
                return newDic;
            }
            
            public void LevelUpGasFactory()
            {
                _data.factoryGasLevel++;
                
                OnLevelUpGasFactory?.Invoke();
            }
            
            public void LevelUpStoneFactory()
            {
                _data.factoryStoneLevel++;
                
                OnLevelUpStoneFactory?.Invoke();
            }
            
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}