﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data
{
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        public class RaidData
        {
            private class LitData
            {
                public ObscuredInt difficultyCursor = 1;
                public ObscuredInt ticketBuyCount = 0;
                
            }
            private LitData _data = new LitData();
            
            // Properties
            public int DifficultyCursor
            {
                get => _data.difficultyCursor;
                set => _data.difficultyCursor = value;
            }

            public int TicketBuyCount => _data.ticketBuyCount;

            // Events
            
            // Functions
            public void ResetTicketBuyCount()
            {
                _data.ticketBuyCount = 0;
            }

            public void AddTicketBuyCount()
            {
                _data.ticketBuyCount++;
            }
            
            // Load & Save
            public override string ToString()
            {
                return JsonMapper.ToJson(_data);
            }
            
            public void FromString(string from)
            {
                if (from == null)
                    return;
                
                _data = JsonMapper.ToObject<LitData>(from);
            }
        }
    }
}