﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using CodeStage.AntiCheat.ObscuredTypes;
using Databox.OdinSerializer.Utilities;
using LitJson;
using UnityEngine;
using Utils;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global


namespace InGame.Data
{
    /*
     * Custom Type이 LitJson의 Read & Write 시 사용할 함수 등록  
     */
    public partial class UserData : MonoBehaviourMultiton<UserData>
    {
        private static bool _hasRegistered;
        
        public static void RegisterToJsonMapper()
        {
            // 멀티톤으로 바꼈으므로 딱 한 번만 하게 함 
            if (_hasRegistered)
                return;

            _hasRegistered = true;
            
            // Exporter
            JsonMapper.RegisterExporter<ObscuredString>(ExportObscuredString);
            JsonMapper.RegisterExporter<ObscuredFloat>(ExportObscuredFloat);
            JsonMapper.RegisterExporter<ObscuredInt>(ExportObscuredInt);
            JsonMapper.RegisterExporter<ObscuredLong>(ExportObscuredLong);
            JsonMapper.RegisterExporter<ObscuredBool>(ExportObscuredBool);
            JsonMapper.RegisterExporter<ObscuredDouble>(ExportObscuredDouble);

            // Importer
            // 임포팅 중에 클래스 프로퍼티의 타입이 '오른쪽'인데, 
            // 작성된 json 타입이 '왼쪽'일 때,
            // 호출됨. 이 상황이 정확히 일치해야만 호출됨 
            JsonMapper.RegisterImporter<string, ObscuredString>(ImportObscuredString);
            JsonMapper.RegisterImporter<int, ObscuredInt>(ImportObscuredInt);
            JsonMapper.RegisterImporter<float, ObscuredFloat>(ImportObscuredFloat);
            JsonMapper.RegisterImporter<long, ObscuredLong>(ImportObscuredLong);
            JsonMapper.RegisterImporter<bool, ObscuredBool>(ImportObscuredBool);
            JsonMapper.RegisterImporter<double, ObscuredDouble>(ImportObscuredDouble);
        }

        // Exporter
        #region Exporter

        private static void ExportObscuredString(ObscuredString obj, JsonWriter writer)
        {
            writer.Write (obj);
        }
        
        private static void ExportObscuredInt(ObscuredInt obj, JsonWriter writer)
        {
            writer.Write (obj);
        }
        
        private static void ExportObscuredLong(ObscuredLong obj, JsonWriter writer)
        {
            writer.Write (obj);
        }
        
        private static void ExportObscuredFloat(ObscuredFloat obj, JsonWriter writer)
        {
            writer.Write (obj);
        }
        
        private static void ExportObscuredBool(ObscuredBool obj, JsonWriter writer)
        {
            writer.Write (obj);
        }
        
        private static void ExportObscuredDouble(ObscuredDouble obj, JsonWriter writer)
        {
            writer.Write (obj);
        }
        
        #endregion

        
        // Importer
        #region Importer

        private static ObscuredString ImportObscuredString(string input)
        {
            return input;
        }
        
        private static ObscuredInt ImportObscuredInt(int input)
        {
            return input;
        }
        
        private static ObscuredLong ImportObscuredLong(long input)
        {
            return input;
        }
        
        private static ObscuredFloat ImportObscuredFloat(float input)
        {
            return input;
        }
        
        private static ObscuredBool ImportObscuredBool(bool input)
        {
            return input;
        }

        private static ObscuredDouble ImportObscuredDouble(double input)
        {
            return input;
        }

        #endregion
    }
}