﻿using UnityEngine;

namespace InGame.Data.Scene
{
    // UserData는 유저마다 귀속돼있는 데이터 
    // 씬에서만 잠깐 사용되는 데이터를 UserData에 두는게 영 맘에 안들어서 싱글턴으로 뺌  
    public partial class SceneData : MonoBehaviourSingleton<SceneData>
    {
        public PlunderData Plunder = new PlunderData();
    }
}