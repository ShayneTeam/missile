﻿using CodeStage.AntiCheat.ObscuredTypes;
using UnityEngine;

// ReSharper disable InconsistentNaming
// ReSharper disable ClassNeverInstantiated.Global
// ReSharper disable FieldCanBeMadeReadOnly.Local

namespace InGame.Data.Scene
{
    public partial class SceneData : MonoBehaviourSingleton<SceneData>
    {
        public class PlunderData
        {
            // 서버에 저장하진 않고, 클라 캐싱용 데이터 
            // (이런 데이터는 UserData가 아니기에, SceneData라는 싱글톤으로 빼는게 나음)
            private class PlunderSceneSettingSchema
            {
                public string targetUserInDate = "";
                public string targetUserNickname = "";
                public ObscuredDouble targetUserInitGas = 0;
                
                public ObscuredBool isRevenge = false;
                public string revengeLogInDate = "";

                public ObscuredInt sweepCount = 1;
            }

            private PlunderSceneSettingSchema _dataForScene = new PlunderSceneSettingSchema();

            /*
             * Properties
             */
            public bool IsRevenge => _dataForScene.isRevenge;
            public string RevengeLogInDate => _dataForScene.revengeLogInDate;
            public int SweepCount => _dataForScene.sweepCount;

            /*
             * Functions
             */
            public void SetTargetUserDataForPlunder(string userInDate, string nickname, double initGas, int sweepCount)
            {
                _dataForScene.targetUserInDate = userInDate;
                _dataForScene.targetUserNickname = nickname;
                _dataForScene.targetUserInitGas = initGas;
                _dataForScene.sweepCount = sweepCount;
                
                _dataForScene.isRevenge = false;
                _dataForScene.revengeLogInDate = "";
            }
            
            public void SetTargetUserDataForRevenge(string userInDate, string nickname, double initGas, string logInDate)
            {
                _dataForScene.targetUserInDate = userInDate;
                _dataForScene.targetUserNickname = nickname;
                _dataForScene.targetUserInitGas = initGas;
                _dataForScene.sweepCount = 1;
                
                _dataForScene.isRevenge = true;
                _dataForScene.revengeLogInDate = logInDate;
            }

            public void GetTargetUserData(out string userInDate, out string nickname, out double initGas)
            {
                userInDate = _dataForScene.targetUserInDate;
                nickname = _dataForScene.targetUserNickname;
                // 획득 가능한 가스량 세팅을 소탕 카운트만큼 곱해서 적용  
                initGas = _dataForScene.targetUserInitGas * _dataForScene.sweepCount;
            }
        }
    }
}