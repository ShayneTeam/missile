﻿using BackEnd;
using Global.Extensions;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using UnityEditor;
using UnityEngine;

namespace InGame.Editor
{
    public class BackendToolWindow : EditorWindow
    {
        [MenuItem("Test/로컬 게스트의 서버 데이터 제거")]
        private static void RemoveServerData()
        {
            if (!Backend.IsInitialized)
            {
                var s = Backend.Initialize();
                if(s.IsSuccess()) ULogger.Log("뒤끝 초기화 성공.");
            }
            
            Backend.BMember.GuestLogin();

            var tableList = Backend.GameData.GetTableList();
            if (tableList.IsSuccess())
            {
                var tables = tableList.GetReturnValuetoJSON()["tables"];
                for (var i = 0; i < tables.Count; i++)
                {
                    var json = tables[i];
                    var tableName = json["tableName"].StringValue();

                    Backend.GameData.Delete(tableName, new Where());
                }
                ULogger.Log("전체 데이터 제거 완료.");
            }
        }
        
        [MenuItem("Test/로컬 게스트의 로컬 정보 제거")]
        private static void DeleteGuestInfo()
        {
            if (!Backend.IsInitialized)
            {
                var s = Backend.Initialize();
                if(s.IsSuccess()) ULogger.Log("뒤끝 초기화 성공.");
            }
            
            Backend.BMember.DeleteGuestInfo();
            
            ULogger.Log("로컬 게스트 정보 제거 완료.");
        }
    }
}