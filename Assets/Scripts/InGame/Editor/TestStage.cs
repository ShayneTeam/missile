﻿using InGame.Global;
using UnityEditor;

namespace InGame.Editor
{
    public class TestStage
    {
        [MenuItem("Test/Fail Stage")]
        private static void FailStage()
        {
            if (Stage.Instance == null)
                return;
            
            Stage.Instance.Fail();
        }
    }
}