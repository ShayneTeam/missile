﻿using BackEnd;
using Global.Extensions;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using UnityEditor;
using UnityEngine;

namespace InGame.Editor
{
    public class EnterPlayModeWindow : EditorWindow
    {
        [MenuItem("Enter Play Mode/Reload Domain")]
        private static void ReloadDomain()
        {
            EditorUtility.RequestScriptReload();
        }
    }
}