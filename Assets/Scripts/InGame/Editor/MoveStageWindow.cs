﻿using InGame.Controller;
using InGame.Data;
using InGame.Global;
using UnityEditor;
using UnityEngine;

namespace InGame.Editor
{
    public class MoveStageWindow : EditorWindow
    {
        private static string _targetStage;
        
        [MenuItem("Test/Move Stage %t")]
        private static void ShowWindow()
        {
            // 런타임 실행만 허용
            if (Stage.Instance == null)
                return;
            
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var currentStage, out _);
            _targetStage = currentStage.ToString();

            var window = GetWindow<MoveStageWindow>();
            window.titleContent = new GUIContent("Move Stage");
            window.Show();
        }

        private void OnGUI()
        {
            // 매프레임마다 호출되는 함수라 반드시 멤버변수에 기록시켜 갱신해야 함 
            _targetStage = EditorGUILayout.TextField ("Stage", _targetStage);

            if(GUILayout.Button("Move"))
                Stage.Instance.MoveStage(int.Parse(_targetStage));
        }
    }
}