﻿using System;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using Global;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using MEC;
using NUnit.Framework;
using UnityEditor;
using UnityEngine;

namespace InGame.Editor
{
    public class CopyUserDataWindow : EditorWindow
    {
        private static string _fromNickname = "Nickname";
        private static string _toNickname = "Nickname";
        
        private CoroutineHandle _copying;

        [MenuItem("The Backend/Copy User Data")]
        private static void ShowWindow()
        {
            var window = GetWindow<CopyUserDataWindow>();
            window.titleContent = new GUIContent("Copy User Data");
            window.Show();
        }

        private void OnGUI()
        {
            _fromNickname = EditorGUILayout.TextField("From", _fromNickname);
            _toNickname = EditorGUILayout.TextField("To", _toNickname);

            // 버튼 누를 시
            if (!GUILayout.Button("Copy")) return;
            
            // 뒤끝 초기화 
            if (!Backend.IsInitialized)
            {
                var init = Backend.Initialize();
                if (!init.IsSuccess())
                {
                    ULogger.Log("뒤끝 초기화 실패");
                    return;
                }
            }
            
            // 게스트 로그인 (GetUserInfoByNickName 호출 조건)
            var login = Backend.BMember.GuestLogin();
            if (!login.IsSuccess())
            {
                ULogger.Log("뒤끝 로그인 실패");
                return;
            }

            // 코루틴
            Timing.KillCoroutines(_copying);
            _copying = Timing.RunCoroutine(_Copy(_fromNickname, _toNickname), Segment.EditorUpdate);
        }

        private IEnumerator<float> _Copy(string fromNickname, string toNickname)
        {
            // From 인데이트 가져오기 
            var fromGet = TryGetUserInDate(fromNickname, out var fromInDate);
            if (!fromGet)
            {
                ULogger.Log("From 인데이트 가져오기 실패");
                yield break;
            }

            // To 인데이트 가져오기 
            var toGet = TryGetUserInDate(toNickname, out var toInDate);
            if (!toGet)
            {
                ULogger.Log("To 인데이트 가져오기 실패");
                yield break;
            }
            
            // 데이터박스 로딩 
            yield return Timing.WaitUntilDone(DataboxController.Instance.Load((perfect) => { }), Segment.EditorUpdate);
            
            // SendQueue 초기화 
            if (!SendQueue.IsInitialize) SendQueue.StartSendQueue(true, (exception) =>{ });

            // From 데이터 가져오기 
            UserData.RegisterToJsonMapper();
            var fromUserData = UserData.Other[fromInDate];
            var allTables = fromUserData.AllTableData.Select(tuple => tuple.tupleKey).ToList();
            yield return Timing.WaitUntilDone(fromUserData._RequestServerData(allTables));

            // From 데이터 To에 복사
            fromUserData.CopyTo(toInDate);
            
            // To 데이터 서버에 저장 
            var toUserData = UserData.Other[toInDate];
            toUserData.LoadLocal();
            yield return Timing.WaitUntilDone(toUserData.SaveToServerWithoutLogin());
            
            // 성공 
            ULogger.Log("유저 데이터 복사 성공", Color.green);
            
            // SendQueue 종료
            SendQueue.StopSendQueue();
        }

        private static bool TryGetUserInDate(string nickname, out string inDate)
        {
            // Nickname으로 게임 유저 inDate가져오는 함수
            var bro = Backend.Social.GetUserInfoByNickName(nickname);
            if (!bro.IsSuccess())
            {
                inDate = string.Empty;
                return false;
            }
            
            inDate = bro.GetReturnValuetoJSON()["row"]["inDate"].ToString();
            return true;
        }

        private void Update()
        {
            // 요청된 유저 데이터 가져오기 위해 폴링  
            if (SendQueue.IsInitialize) BackEnd.SendQueue.Poll();
        }
    }
}