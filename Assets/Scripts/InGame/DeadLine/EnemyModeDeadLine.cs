﻿using InGame.Controller.Mode;
using UnityEngine;

namespace InGame.DeadLine
{
    public class EnemyModeDeadLine : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            // Enemy가 선을 밟을 때만 처리
            if (!other.CompareTag("Enemy"))
                return;
        
            // 실패 
            (EnemyMode.Behaviour as IDefeat)?.Defeat();
        }
    }
}
