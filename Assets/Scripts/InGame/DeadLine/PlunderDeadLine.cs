﻿using System.Collections;
using System.Collections.Generic;
using Global;
using InGame.Controller.Mode;
using InGame.Global;
using MEC;
using UnityEngine;

namespace InGame.DeadLine
{
    public class PlunderDeadLine : MonoBehaviour
    {
        [SerializeField] private Transform gasBarrelsParent;
        
        [SerializeField] private float gasBarrelIntervalSec = 0.1f;
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            // 데드라인 뒤에 가스통 전부 획득 
            Timing.RunCoroutine(_FlyGasBarrelsFlow().CancelWith(gameObject));
            
            // 승리 플로우 시작 
            Plunder.Plunder.Instance.Victory();
        }

        private IEnumerator<float> _FlyGasBarrelsFlow()
        {
            // 마지막 가스 드럼통 획득 
            var gasPercentage = DataboxController.GetDataInt(Table.Plunder, Sheet.reward, "gas_rwd_deadline", Column.gas_rwd_percent);
            
            // 날라가는 가스통 하나 당 얻게될 가스 퍼센티지 계산
            var gasPercentagePerBarrel = gasPercentage / (float)gasBarrelsParent.childCount;
            
            for (var i = 0; i < gasBarrelsParent.childCount; i++)
            {
                var gasBarrel = gasBarrelsParent.GetChild(i);
                 
                // 얘네 위치서 마지막 데드라인 가스 나눠가지면서 획득 처리 
                Plunder.Plunder.Instance.PlunderTargetUserGas(gasPercentagePerBarrel, gasBarrel.position);
                 
                // 가스통 끄기 
                gasBarrel.gameObject.SetActive(false);
                
                // 딜레이 
                yield return Timing.WaitForSeconds(gasBarrelIntervalSec);
            }
        }
    }
}
