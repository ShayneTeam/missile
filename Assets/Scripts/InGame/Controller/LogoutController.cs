﻿using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global.Buff;
using Server;
using UnityEngine;

namespace InGame.Controller
{
    public class LogoutController : MonoBehaviourSingletonPersistent<LogoutController>
    {
        [Header("로그아웃 시, 접속할 계정 (에디터 전용)"), SerializeField] private string id;
        [SerializeField] private string pw;

        public static event VoidDelegate OnLogout;
        
        public void Logout()
        {
            // 플랫폼 별 계정 로그아웃
            if (ExtensionMethods.IsAndroid())
            {
                PlayerPrefs.DeleteKey("lastLogin");
#if UNITY_ANDROID
                PlayGamesController.SignOut();
#endif
            }
                    
            // 에디터 로그아웃 테스트 
            if (ExtensionMethods.IsUnityEditor())
            {
                ServerMyInfo.Instance.ID = id;
                ServerMyInfo.Instance.Password = pw;
            }
            
            // 버프 초기화 
            // 각 버프와 버프버튼은 독립적임 
            // 버프 먼저 리셋하고, 그 다음 버프버튼을 리셋해야 함 
            BuffSpeed.Instance.Reset();
            BuffDamage.Instance.Reset();
            BuffMineral.Instance.Reset();
            
            // 미션체크 종료 
            MissionController.Instance.StopMissionCheck();
            
            // 채팅 종료 
            ServerChat.Instance.DeactivateBehind();
            
            // 리뷰 종료 
            ReviewController.Instance.StopToCheck();
            
            // 리더보드 종료 
            LeaderBoardController.Instance.StopAutoReport();
            
            // 로그 종료 
            LogController.Instance.StopAutoLog();
            
            // 랭킹 종료 
            ServerRanking.Instance.StopAllAuto();
            
            // 튜토리얼 초기화 
            TutorialPopup.Instance.Init();
            
            // 동기화 중지 
            UserData.My.StopAutoSaveToServer();

            // 돌아가기 전에 세이브
            UserData.My.SaveToServer();
                    
            // 타이틀 씬으로 이동 
            SceneController.ReturnToTitleScene();
            
            // 이벤트 
            OnLogout?.Invoke();
        }
    }
}