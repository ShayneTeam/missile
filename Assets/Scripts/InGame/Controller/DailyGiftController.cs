﻿using System.Collections.Generic;
using Global;
using InGame.Data;
using InGame.Global;
using MEC;
using Server;
using UnityEngine;

namespace InGame.Controller
{
    public class DailyGiftController : MonoBehaviourSingleton<DailyGiftController>
    {
        public static int RewardAmount => int.Parse(DataboxController.GetConstraintsData(Constraints.EVENT_ONEHOUR_URANIUM_DAY));

        public static bool CanCollect => UserData.My.DailyGift.CanCollect();
        public static bool HasCollected => UserData.My.DailyGift.HasCollected;
        public static bool HasRemainTime => RemainTimeToCollect > 0;
        public static int RemainTimeToCollect => UserData.My.DailyGift.RemainTimeToCollect;

        private static CoroutineHandle _timer;


        public static void ResetToCollectAgain()
        {
            UserData.My.DailyGift.Reset();
        }

        public static void StartTimer()
        {
            Timing.KillCoroutines(_timer);
            _timer = Timing.RunCoroutine(_Timer(), Segment.RealtimeUpdate);
        }

        private static IEnumerator<float> _Timer()
        {
            // 남은 시간 0 될 때까지 루프
            while (HasRemainTime)
            {
                // 기다림
                const int interval = 1;
                yield return Timing.WaitForSeconds(interval);
                    
                // 남은 시간 차감
                UserData.My.DailyGift.ElapseTime(interval);
            }
        }

        public static void Collect()
        {
            if (!CanCollect) return;
            
            // 획득 처리 
            UserData.My.DailyGift.Collect();
            
            // 우라늄 지급 
            ResourceController.Instance.AddUranium(RewardAmount, LogEvent.DailyGift);
            
            // 저장 
            UserData.My.SaveToServer();
        }
    }
}