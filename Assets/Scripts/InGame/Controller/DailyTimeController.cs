﻿using System;
using BackEnd;
using Server;
using UnityEngine;

namespace InGame.Controller
{
    // 기획에서 정한 데일리 기준 시간을 처리
    // 해당 기준 시간이 됐을 때, 다음 날이 되도록 함 
    public class DailyTimeController : MonoBehaviourSingleton<DailyTimeController>
    {
        // UTC+0 20시. 일일 초기화 기준 시각 (한국시간 UTC+9 기준. 새벽 5시)
        private const int StandardHoursToReset = 20 * 3600;
        private const int TwentyFourHours = 3600 * 24;

        // 하루 초기화 기준 시각까지 얼마나 남았나
        public static int GetRemainTimeBeforeToReset(DateTime date)
        {
            // UTC+0 기준으로 맞추기
            // 주의!. 이미 밖에서 ToUniversalTime() 하고 넘어오면 다시 ToUniversalTime()이 적용됨 
            // 즉, -9시간 되서 넘어왔는데, 다시 -9시간이 됨
            var universalDateTime = date.ToUniversalTime();
            
            // 해당 날의 자정부터 흐른 시간  
            var elapsedTimeOfDay = (int)universalDateTime.TimeOfDay.TotalSeconds;

            // 초기화 기준 시간까지 남은 시간  
            int remainTimeBeforeToReset;
            var diffTime = StandardHoursToReset - elapsedTimeOfDay;
            
            // 현재 시간이 기준 시각보다 넘어섰다면, 다음 날의 기준 시각과의 시차 반환
            if (elapsedTimeOfDay >= StandardHoursToReset)
            {
                // 여기서 diffTime은 -임 
                remainTimeBeforeToReset = diffTime + TwentyFourHours;
            }
            // 현재 시간이 아직 기준 시간 전이라면, 그 기준 시각과의 시차 반환 
            else
            {
                remainTimeBeforeToReset = diffTime;
            }
            
            return remainTimeBeforeToReset;
            
            /*
             * 시간 계산은 위처럼 RemainTime 방식으로 하는게 가장 직관적
             * 기준 시간대를 UTC+0 으로 잡고, 하루 종료 시각을 20시로 설정한 것
             */
        }

        // 일간 체크용
        public static DateTime GetEndTimeOf(DateTime standardDate)
        {
            var remainTimeBeforeToReset = GetRemainTimeBeforeToReset(standardDate);
            return standardDate.AddSeconds(remainTimeBeforeToReset);
        }
        
        // 주간 체크용
        public static DateTime GetNextWeekDay(DateTime standardDate, DayOfWeek day)
        {   
            // 해당하는 요일이 될 때까지 루프해서 반환
            var result = standardDate;
            while( result.DayOfWeek != day )
                result = result.AddDays(1);
            
            return result;
        }
        
        // 월간 체크용
        public static DateTime GetNextMonthDay(DateTime standardDate, int day)
        {   
            // 해당하는 날이 될 때까지 루프해서 반환 
            var result = standardDate;
            while( result.Day != day )
                result = result.AddDays(1);
            
            return result;
        }
    }
}
