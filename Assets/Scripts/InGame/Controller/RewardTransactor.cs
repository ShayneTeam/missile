﻿using System;
using System.Collections.Generic;
using Global;
using InGame.Controller;
using InGame.Data;
using LitJson;
using Server;
using UnityEngine;

namespace InGame.Global
{
    public class RewardData
    {
        public string Type;
        public string Rarity;
        public string Idx;
        // 이걸 원래는 string으로 들고 있었는데, 실수 할 여지가 보여 숫자형 타입으로 바꿈 
        // 만약 string 타입이었는데, 그 안에 UnitString으로 들어가있거나, 이상한 문자가 들어가 있으면,
        // 숫자형 타입으로 변환할 수 없기 때문 
        // 애초에 Count라는 네이밍인데 string 타입인게 상식적이지 않음 
        public double Count;

        public RewardData()
        {
        }
        
        public RewardData(string type, string idx, double count)
        {
            Type = type;
            Idx = idx;
            Count = count;
        }
        
        public RewardData(string type, string rarity, string idx, double count)
        {
            Type = type;
            Rarity = rarity;
            Idx = idx;
            Count = count;
        }
    }

    // ReSharper disable InconsistentNaming
    public static class RewardType
    {
        public const string REWARD_MINERAL = "REWARD_MINERAL";
        public const string REWARD_GAS = "REWARD_GAS";
        public const string REWARD_STONE = "REWARD_STONE";
        public const string REWARD_GEM = "REWARD_GEM";
        public const string REWARD_KEY = "REWARD_KEY";
        public const string REWARD_PLUNDER_FLAG = "REWARD_PLUNDER_FLAG";
        public const string REWARD_REVENGE_TICKET = "REWARD_REVENGE_TICKET";
        public const string REWARD_PVP_TICKET = "REWARD_PVP_TICKET";
        public const string REWARD_EVENT_COIN = "REWARD_EVENT_COIN";
        public const string REWARD_MILEAGE = "REWARD_MILEAGE";
        public const string REWARD_CHIPSET = "REWARD_CHIPSET";
        public const string REWARD_WH_TICKET = "REWARD_WH_TICKET";
        public const string REWARD_RAID_TICKET = "REWARD_RAID_TICKET";
        public const string REWARD_BATTLESHIP_TICKET = "REWARD_BATTLESHIP_TICKET";
        public const string REWARD_MISSILE = "REWARD_MISSILE";
        public const string REWARD_GIRL = "REWARD_GIRL";
        public const string REWARD_GIRL_COSTUME = "REWARD_GIRL_COSTUME";
        public const string REWARD_BAZOOKA = "REWARD_BAZOOKA";
        public const string REWARD_DIABLO_RELIC = "REWARD_DIABLO_RELIC";
        public const string REWARD_INF_STONE = "REWARD_INF_STONE";
        public const string REWARD_COSTUME = "REWARD_COSTUME";
        public const string REWARD_BOX = "REWARD_BOX";
        public const string REWARD_AD_CANCEL = "REWARD_AD_CANCEL"; // 광고제거 
        public const string REWARD_BUFF_UNLIMITED = "REWARD_BUFF_UNLIMITED"; // 버프 무제한 
        public const string REWARD_PLUNDER_DEFENCE = "REWARD_PLUNDER_DEFENCE"; // 약탈 100% 방어   
        public const string REWARD_BUFF_SPEED_3X = "REWARD_BUFF_SPEED_3X"; // 버프 3배속 
        public const string REWARD_BUFF_FAIRY = "REWARD_BUFF_FAIRY"; // 버프 3배속 
        public const string REWARD_ENERGY = "REWARD_ENERGY"; // 고대 에너지 

        public static string GetString(string type, double count)
        {
            if (type == REWARD_GEM || type == REWARD_MILEAGE || type == REWARD_INF_STONE)
                return Math.Floor(count).ToString("0");
            else
                return count.ToUnitString();
        }
    }

    public class RewardTransactor : MonoBehaviourSingleton<RewardTransactor>
    {
        // 로그의 책임까지 지는게 조금 그렇지만, 리워드 처리를 한 방에 몰아서 하는게 트랜잭터 역할이라, 
        // 로그도 동시에 몰아 하는 것도 이 녀석이 해주는게 맞다 보임
        public static void Transact(RewardData reward, string logEvent)
        {
            switch (reward.Type)
            {
                case RewardType.REWARD_MINERAL:
                {
                    ResourceController.Instance.AddMineral(reward.Count);
                }
                    break;

                case RewardType.REWARD_GAS:
                {
                    ResourceController.Instance.AddGas(reward.Count);
                }
                    break;

                case RewardType.REWARD_STONE:
                {
                    ResourceController.Instance.AddStone(reward.Count);
                }
                    break;

                case RewardType.REWARD_GEM:
                {
                    ResourceController.Instance.AddUranium(reward.Count, logEvent);
                }
                    break;

                case RewardType.REWARD_MILEAGE:
                {
                    UserData.My.Resources.mileage += (int)reward.Count;
                    
                    // 로그 
                    ServerLogger.Log(reward.Type, logEvent, reward.Count, UserData.My.Resources.mileage);
                }
                    break;

                case RewardType.REWARD_MISSILE:
                {
                    InGameControlHub.My.MissileController.Earn(reward.Idx, (int)reward.Count);
                }
                    break;
                    
                case RewardType.REWARD_GIRL:
                {
                    InGameControlHub.My.PrincessController.EarnTreasure(reward.Idx, (int)reward.Count);
                }
                    break;
                case RewardType.REWARD_GIRL_COSTUME:
                {
                    UserData.My.Princess.CollectCostume(reward.Idx);
                }
                    break;

                case RewardType.REWARD_BOX:
                {
                    BoxController.AddBox(reward.Idx, (int)reward.Count);
                }
                    break;

                case RewardType.REWARD_BAZOOKA:
                {
                    InGameControlHub.My.BazookaController.Earn(reward.Idx);
                    
                    // 로그
                    ServerLogger.Log(reward.Type, logEvent, reward.Idx);
                }
                    break;

                case RewardType.REWARD_KEY:
                {
                    UserData.My.Resources.diablokey += (int)reward.Count;
                }
                    break;

                case RewardType.REWARD_AD_CANCEL:
                {
                    UserData.My.UserInfo.SetFlag(UserFlags.CanAdRemove, true);
                    
                    // 로그
                    ServerLogger.Log(reward.Type, logEvent, true);
                }
                    break;

                case RewardType.REWARD_BUFF_UNLIMITED:
                {
                    UserData.My.UserInfo.SetFlag(UserFlags.CanBeInfinityTimeAllBuff, true);
                    
                    // 로그
                    ServerLogger.Log(reward.Type, logEvent, true);
                }
                    break;

                case RewardType.REWARD_BUFF_SPEED_3X:
                {
                    UserData.My.UserInfo.SetFlag(UserFlags.CanBeSpeedBuff3x, true);
                    
                    // 로그
                    ServerLogger.Log(reward.Type, logEvent, true);
                }
                    break;

                case RewardType.REWARD_PLUNDER_DEFENCE:
                {
                    UserData.My.UserInfo.SetFlag(UserFlags.CanDefendPlunder, true);
                    
                    // 로그
                    ServerLogger.Log(reward.Type, logEvent, true);
                }
                    break;

                case RewardType.REWARD_WH_TICKET:
                case RewardType.REWARD_PLUNDER_FLAG:
                case RewardType.REWARD_CHIPSET:
                case RewardType.REWARD_RAID_TICKET:
                case RewardType.REWARD_BATTLESHIP_TICKET:
                {
                    AddItems(reward);
                    
                    // 로그
                    ServerLogger.LogItem(reward.Type, logEvent, (int)reward.Count);
                }
                    break;

                case RewardType.REWARD_INF_STONE:
                {
                    UserData.My.InfinityStone.AddStone(reward.Idx, (int)reward.Count);
                }
                    break;
                
                case RewardType.REWARD_COSTUME:
                {
                    // 코스튬 리워드 타입은 세트로 들어올 수 있어서 스플릿 해야 함
                    var splitIdxes = reward.Idx.Split(',');
                    foreach (var idx in splitIdxes)
                    {
                        InGameControlHub.My.CostumeController.Earn(idx);    
                    }                    
                }
                    break;

                case RewardType.REWARD_BUFF_FAIRY:
                {
                    UserData.My.UserInfo.SetFlag(UserFlags.CanAutoCatchFairy, true);
                    
                    // 로그
                    ServerLogger.Log(reward.Type, logEvent, true);
                }
                    break;

                case RewardType.REWARD_ENERGY:
                {
                    UserData.My.Missiles.AddEnergy((int)reward.Count);
                    
                    // 로그
                    ServerLogger.Log(reward.Type, logEvent, (int)reward.Count, UserData.My.Missiles.AncientEnergy);
                }
                    break;
                
                default:
                {
                    /*
                     * 별도로 카테고라이징 돼있지 않은 녀석들
                     * REWARD_PVP_TICKET
                     * REWARD_EVENT_COIN
                     * REWARD_KEY
                     */

                    AddItems(reward);
                }
                    break;
            }
        }

        private static void AddItems(RewardData reward)
        {
            UserData.My.Items[reward.Type] += (int)reward.Count;
        }

        public static void Transact(IEnumerable<RewardData> rewards, string logEvent)
        {
            foreach (var reward in rewards)
            {
                Transact(reward, logEvent);
            }
        }

        public static string GetIcon(string type)
        {
            return DataboxController.GetDataString(Table.Reward, Sheet.reward, type, Column.icon);
        }

        public static string GetName(string type)
        {
            return Localization.GetText(DataboxController.GetDataString(Table.Reward, Sheet.reward, type, Column.name));
        }
    }
}