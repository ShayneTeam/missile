﻿using System;
using System.Collections.Generic;
using System.Linq;
using Databox.OdinSerializer.Utilities;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using Server;
using UI.Popup.Menu;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace InGame.Controller
{
	public class TreasureGachaData
	{
		public string Idx;
		public string Type;
		public string Rarity;
		public string TreasureIdx;
		public int Count;
	}

	public partial class ShopController : MonoBehaviourSingleton<ShopController>
    {
	    public class TreasureDelegator 
        {
	        public static event VoidDelegate OnGacha;
	        
	        public static string GetGachaGroup(string idx, string sheet)
            {
	            return DataboxController.GetDataString(Table.Shop, sheet, idx, Column.reward_gacha_group);
            }

            public void Gacha(string idx, string sheet, out List<TreasureGachaData> results)
            {
	            // 가챠 
	            var group = GetGachaGroup(idx, sheet);
	            var count = GetGachaCount(idx, sheet);

	            InternalGacha(group, count, out results);
	            
	            // 유저 데이터 즉각 저장
	            UserData.My.SaveToServer();
            }
            
            private static void InternalGacha(string group, int count, out List<TreasureGachaData> results)
            {
	            results = new List<TreasureGachaData>();
	            
		        // 가챠 
		        for (var i = 0; i < count; i++)
		        {
			        var randomIdx = DataboxController.RandomPick(Table.Shop, Sheet.reward_gacha, "group", @group, Column.chance);

			        var treasureIdx = DataboxController.GetDataString(Table.Shop, Sheet.reward_gacha, randomIdx, "missile_idx");
			        
			        var rewardData = new TreasureGachaData
			        {
				        Idx = randomIdx, 
				        TreasureIdx = treasureIdx,
				        Rarity = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.rarity),
				        Type = DataboxController.GetDataString(Table.Shop, Sheet.reward_gacha, randomIdx, Column.reward_type),
				        Count = DataboxController.GetDataInt(Table.Shop, Sheet.reward_gacha, randomIdx, Column.reward_count)
			        };

			        results.Add(rewardData);
		        }
		        
		        // 획득 처리를 위해 정렬 최적화 (반환할 땐, 가챠 연출을 위해 정렬하지 않은 걸 보냄)  
				var resultsSorted = results.GroupBy(x => x.TreasureIdx)
					.Select(g => new RewardData
					{
						Idx = g.Key,
						Type = g.First().Type,
						Rarity = g.First().Rarity,
						Count = g.Sum(x => x.Count),
					}).OrderByDescending(x => x.Idx);

				// 획득 
				RewardTransactor.Transact(resultsSorted, LogEvent.ShopGacha);
	            
				// 신화 획득시 로그 기능
				resultsSorted.Where(t => t.Rarity == "myth").ForEach(x =>
				{
					WebLog.Instance.AllLog("myth_log", new Dictionary<string, object>
					{
						{"Type", x.Type},
						{"Rarity", x.Rarity},
						{"Idx", x.Idx},
						{"Count", x.Count},
					});
				});
				
				// 유저 데이터 즉각 저장
				UserData.My.SaveToServer();
				
				// 이벤트 
				OnGacha?.Invoke();
            }
        }

        public static readonly TreasureDelegator Treasure = new TreasureDelegator();
    }
}