﻿using System;
using System.Collections.Generic;
using System.Linq;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using Server;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace InGame.Controller
{
    public partial class ShopController : MonoBehaviourSingleton<ShopController>
    {
        public class BazookaDelegator 
        {
            public string GetRewardBazookaIdx(string idx, string sheet)
            {
                var rewards = GetRewardsDataWithType(idx, sheet, RewardType.REWARD_BAZOOKA);
                foreach (var reward in rewards)
                {
                    return reward.Idx;
                }

                return "";
            }
        }

        public static readonly BazookaDelegator Bazooka = new BazookaDelegator();
    }
}