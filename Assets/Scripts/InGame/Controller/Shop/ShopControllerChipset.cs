﻿using System;
using System.Collections.Generic;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using Server;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace InGame.Controller
{
    public partial class ShopController : MonoBehaviourSingleton<ShopController>
    {
        public class ChipsetDelegator 
        {
            public void Earn(string idx, string sheet)
            {
                // 획득 
                var reward = GetRewardValue(idx, sheet);
                
                UserData.My.Items[RewardType.REWARD_CHIPSET] += reward;
            
                // 로그 
                var logEvent = $"{LogEvent.Purchase}_{idx}";
                ServerLogger.LogItem(RewardType.REWARD_CHIPSET, logEvent, reward);
                
                // 바로 저장 
                UserData.My.SaveToServer();
            }

            public string GetEarnSuccessMessage(string idx, string sheet)
            {
                return Localization.GetFormatText("shop_chipset_earn",GetRewardValue(idx, sheet).ToString());
            }
        }

        public static readonly ChipsetDelegator Chipset = new ChipsetDelegator();
    }
}