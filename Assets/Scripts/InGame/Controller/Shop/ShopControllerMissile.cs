﻿using System;
using System.Collections.Generic;
using System.Linq;
using Databox.OdinSerializer.Utilities;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using Server;
using UI.Popup.Menu;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace InGame.Controller
{
    public partial class ShopController : MonoBehaviourSingleton<ShopController>
    {
	    private static List<CommonRoll> _rollList;

	    public class MissileGachaData
	    {
		    public string Idx;
		    public string Type;
		    public string Rarity;
		    public string MissileIdx;
		    public int Count;
	    }
	    
        public class MissileDelegator 
        {
	        public static event VoidDelegate OnGacha;

	        public int GetGachaBonus()
            {
                GetGachaLevel(out var currentLevel, out _);
                return DataboxController.GetDataInt(Table.Shop, Sheet.level, currentLevel.ToLookUpString(), Column.gacha_bonus);
            }

	        public static void GetGachaExp(out int prevLevelExp, out int currentExp, out int nextLevelExp)
            {
                currentExp = UserData.My.Purchase.GachaExp;
                ExpController.FindPrevAndNextExp(currentExp, Table.Shop, Sheet.level, Column.gacha_exp, out prevLevelExp, out nextLevelExp);
            }

	        public static void GetGachaLevel(out int currentLevel, out int nextLevel)
            {
	            var currentExp = UserData.My.Purchase.GachaExp;
	            ExpController.GetLevel(currentExp, Table.Shop, Sheet.level, Column.gacha_exp, out currentLevel, out nextLevel);
            }

            public void Gacha(string idx, string sheet, out List<MissileGachaData> results)
            {
	            // 가챠 
	            var group = GetGachaGroup(idx, sheet);
	            var count = GetGachaCount(idx, sheet);

	            InternalGacha(group, count, out results);
            }

            public void GachaWithLevelBonus(string idx, string sheet, out List<MissileGachaData> results)
            {
                // 가챠 
                var group = GetGachaGroup(idx, sheet);
                var count = GetGachaCount(idx, sheet) + GetGachaBonus();

                InternalGacha(group, count, out results);
                
                // 신화 나오지 않았다면 보너스 값을 늘림.
                if (!results.Exists(t => t.Rarity == "myth"))
	                UserData.My.UserInfo.MissileBonusChance += 1;
                else // 나왔으면 값 초기화
	                UserData.My.UserInfo.MissileBonusChance = 0;
                
                // 획득된 신화 갯수만큼 고대 에너지 획득
                var mythCount = results.Count(t => t.Rarity == MissileRarity.myth.ToEnumString());
				UserData.My.Missiles.AddEnergy(mythCount);
				
				// 로그 
				if (mythCount > 0) ServerLogger.Log(RewardType.REWARD_ENERGY, LogEvent.EarnEnergy, mythCount, UserData.My.Missiles.AncientEnergy);

                // 획득한 미사일 갯수 만큼 경험치 획득
                UserData.My.Purchase.GachaExp += results.Sum(result => result.Count);
                
                // 유저 데이터 즉각 저장
                UserData.My.SaveToServer();
            }

            private static void InternalGacha(string group, int count, out List<MissileGachaData> results)
            {
	            results = new List<MissileGachaData>();
	            
		        // 가챠 
		        for (var i = 0; i < count; i++)
		        {
			        var randomIdx = RandomPick(Table.Shop, Sheet.reward_gacha, "group", @group, Column.chance);

			        var missileIdx = DataboxController.GetDataString(Table.Shop, Sheet.reward_gacha, randomIdx, "missile_idx");
			        
			        var rewardData = new MissileGachaData
			        {
				        Idx = randomIdx, 
				        MissileIdx = missileIdx,
				        Rarity = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.rarity),
				        Type = DataboxController.GetDataString(Table.Shop, Sheet.reward_gacha, randomIdx, Column.reward_type),
				        Count = DataboxController.GetDataInt(Table.Shop, Sheet.reward_gacha, randomIdx, Column.reward_count)
			        };

			        results.Add(rewardData);
		        }
		        
		        // 획득 처리를 위해 정렬 최적화 (반환할 땐, 가챠 연출을 위해 정렬하지 않은 걸 보냄)  
				var resultsSorted = results.Where(t => t.MissileIdx != null).GroupBy(x => x.MissileIdx)
					.Select(g => new RewardData
					{
						Idx = g.Key,
						Type = g.First().Type,
						Rarity = g.First().Rarity,
						Count = g.Sum(x => x.Count),
					}).OrderByDescending(x => x.Idx);

				// 획득 
				RewardTransactor.Transact(resultsSorted, LogEvent.ShopGacha);
	            
				// 신화, 고대 획득시 로그 
				resultsSorted.Where(t => t.Rarity == nameof(MissileRarity.myth) || t.Rarity == nameof(MissileRarity.ancient)).ForEach(x =>
				{
					WebLog.Instance.AllLog($"{x.Rarity.ToString()}_log", new Dictionary<string, object>
					{
						{"Type", x.Type},
						{"Rarity", x.Rarity},
						{"Idx", x.Idx},
						{"Count", x.Count},
					});
					
					// 뒤끝 로그 
					UserData.My.Missiles.GetState(x.Idx, out var ownedCount, out _, out _, out _);
					ServerLogger.Log(RewardType.REWARD_MISSILE, $"{LogEvent.EarnMissile}_{x.Idx}", x.Count, ownedCount);
				});

				// 유저 데이터 즉각 저장
				UserData.My.SaveToServer();
				
				// 이벤트 
				OnGacha?.Invoke();
            }

            private static string RandomPick(string tableName, string sheetName, string groupColumn, string targetGroup, string rarityColumn)
            {
	            _rollList ??= new List<CommonRoll>();
	            _rollList.Clear();
            
	            var allIdxesSameGroup = DataboxController.GetAllIdxesByGroup(tableName, sheetName, groupColumn, targetGroup);
	            foreach (var idx in allIdxesSameGroup)
	            {
		            // 레어리티 넣기 
		            var rarity = DataboxController.GetDataInt(tableName, sheetName, idx, rarityColumn);
		            GlobalFunction.AddRoll(ref _rollList, idx, rarity);
                
		            // 보너스
		            var bonusChange = DataboxController.GetDataInt(tableName, sheetName, idx, Column.chance_bonus) * UserData.My.UserInfo.MissileBonusChance;
                
		            var totalChange = rarity + bonusChange;
                
		            GlobalFunction.AddRoll(ref _rollList, idx, totalChange);
	            }
            
	            // 확률적으로 하나 뽑기
	            return GlobalFunction.Roll(ref _rollList).Name;
            }
        }

        public static readonly MissileDelegator Missile = new MissileDelegator();
    }
}