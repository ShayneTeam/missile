﻿using System;
using System.Collections.Generic;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using Server;
using UI.Popup;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace InGame.Controller
{
    public partial class ShopController : MonoBehaviourSingleton<ShopController>
    {
        private enum CycleType
        {
            none,
            day,
            week,
            month,
            limited,
        }
        
        public enum PriceType
        {
            free,
            payment,
            gem,
            ad,
            mileage,
            energy,
            chipset,
        }

        public enum FailedReason
        {
            NotFailed,
            LimitedCount,
            NotEnoughMileage,
            NotEnoughGem,
            NotEnoughEnergy,
            NotEnoughChipset,
            CoolTime,
            FailedIAP,
            ExistingPurchasePending,
            FailedAd,
            Free,
        }

        public static void StartProcessToReset()
        {
            // 리셋 시간을 넘어섰다면 리셋
            InternalProcessToReset(Sheet.missile);
            InternalProcessToReset(Sheet.ancient);
            InternalProcessToReset(Sheet.goods);
            InternalProcessToReset(Sheet.gem);
            InternalProcessToReset(Sheet.stone);
            InternalProcessToReset(Sheet.stone2);
            InternalProcessToReset(Sheet.season);
            InternalProcessToReset(Sheet.@event);
            InternalProcessToReset(Sheet.princess);
        }

        private static void InternalProcessToReset(string sheet)
        {
            // 모든 인덱스 돌기 
            var allIdxes = DataboxController.GetAllIdxes(Table.Shop, sheet);
            foreach (var idx in allIdxes)
            {
                // 미구매 상품은 돌지 않음
                var lastPurchasedTimestamp = GetLastPurchasedTimestamp(idx);
                if (lastPurchasedTimestamp == 0) continue;
                
                // 타입 체크 
                var cycleType = GetCycleType(idx, sheet);
                switch (cycleType)
                {
                    // 순환 타입에만 적용
                    case CycleType.day:
                    case CycleType.week:
                    case CycleType.month:
                    {
                        // 현재 시간이 이전 구매 날짜의 순환 종료 시간을 넘어섰다면   
                        var endTime = GetCycleEndDateTime(idx, sheet);
                        var canReset = ServerTime.Instance.NowUnscaled.ToTimestamp() > endTime.ToTimestamp();
                        if (!canReset)
                        {
                            // 디버깅용 로그 
                            var name = Localization.GetText(DataboxController.GetDataString(Table.Shop, sheet, idx, Column.name));
                            var remainTime = ServerTime.Instance.NowUnscaled.ToTimestamp() - endTime.ToTimestamp();
                            var timespan = TimeSpan.FromSeconds(remainTime);
                            ULogger.Log($"{name}의 초기화까지 남은 시간 {timespan.ToString()}");
                            
                            continue;
                        }
                        
                        // 구매 횟수 초기화 
                        UserData.My.Purchase.ResetPurchaseCount(idx);
                            
                        // 리셋 타임스탬프 갱신
                        UserData.My.Purchase.StampLastResetTime(idx);
                    } break;
                }
            }
        }

        private static DateTime GetCycleEndDateTime(string idx, string sheet)
        {
            // 마지막 리셋일의 24시를 기준 
            var lastResetDateTime = GetLastResetDateTime(idx);
            var endTimeOfTheResetDay = DailyTimeController.GetEndTimeOf(lastResetDateTime);

            var cycleType = GetCycleType(idx, sheet);
            switch (cycleType)
            {
                case CycleType.day:
                {
                    // 일일 주기 체크 
                    return endTimeOfTheResetDay;
                }
                case CycleType.week:
                {
                    // 주간 주기 체크 
                    return DailyTimeController.GetNextWeekDay(endTimeOfTheResetDay, DayOfWeek.Monday);
                }
                case CycleType.month:
                {
                    // 한달 주기 체크 
                    return DailyTimeController.GetNextMonthDay(endTimeOfTheResetDay, 1);
                }
                default:
                {
                    // 그외에 타입은 현재 시간이 순환 종료 시간
                    return ServerTime.Instance.NowUnscaled;
                }
            }
        }

        private static CycleType GetCycleType(string idx, string sheet)
        {
            return (CycleType)Enum.Parse(typeof(CycleType), DataboxController.GetDataString(Table.Shop, sheet, idx, Column.cycle_type));
        }

        private static DateTime GetLastResetDateTime(string idx)
        {
            // 타임스탬프 가져옴 (리셋한 적이 없다면, 마지막 구매 날짜로 기준 잡음)
            var lastResetTimestamp = GetLastResetTimestamp(idx);
            if (lastResetTimestamp == 0) lastResetTimestamp = GetLastPurchasedTimestamp(idx);
            
            // 타임스탬프를 데이트타임으로 변환
            return GlobalFunction.ToDateTime(lastResetTimestamp);
        }

        private static bool HasRemainCountToPurchase(string idx, string sheet)
        {
            // 구매 타임스탬프가 없다면 구매 가능 (한 번도 구매하지 않은 상태) 
            var timestamp = GetLastPurchasedTimestamp(idx);
            if (timestamp == 0) return true;
            
            // 타임스탬프 시간에 의존하지 않음 
            // 구매 횟수에만 의존함 
            return GetRemainCountToPurchase(idx, sheet) > 0;
        }

        public static int GetRemainCountToPurchase(string idx, string sheet)
        {
            var limitedCount = GetLimitedCount(idx, sheet);
            var purchasedCount = UserData.My.Purchase.GetPurchasedCount(idx);

            return limitedCount - purchasedCount;
        }

        public static int GetLimitedCount(string idx, string sheet)
        {
            return DataboxController.GetDataInt(Table.Shop, sheet, idx, Column.limited_count);
        }

        private static long GetLastResetTimestamp(string idx)
        {
            return UserData.My.Purchase.GetLastResetTimestamp(idx);
        }
        
        public static long GetLastPurchasedTimestamp(string idx)
        {
            return UserData.My.Purchase.GetLastPurchasedTimestamp(idx);
        }

        public static bool HasEverPurchased(string idx)
        {
            return UserData.My.Purchase.GetLastPurchasedTimestamp(idx) != 0;
        }

        public static string GetProductId(string idx, string sheet)
        {
            var iapIdx = DataboxController.GetDataString(Table.Shop, sheet, idx, Column.iap_idx);
            
            if (ExtensionMethods.IsAndroid())
                return DataboxController.GetDataString(Table.Shop, Sheet.iap, iapIdx, Column.google);
            if (ExtensionMethods.IsIOS())
                return DataboxController.GetDataString(Table.Shop, Sheet.iap, iapIdx, Column.ios);

            return "";
        }
        
        public static string GetLocalizedPriceText(string idx, string sheet)
        {
            var productId = GetProductId(idx, sheet);
            return UnityPurchaser.Instance.GetProductLocalizedPriceText(productId);
        }

        public static IEnumerable<string> GetRewards(string idx, string sheet)
        {
            var group = DataboxController.GetDataString(Table.Shop, sheet, idx, Column.reward_group);
            return DataboxController.GetAllIdxesByGroup(Table.Shop, Sheet.reward, "group", @group);
        }

        public static List<RewardData> GetRewardsData(string idx, string sheet)
        {
            var rewards = GetRewards(idx, sheet);
            
            var rewardsData = new List<RewardData>();
            foreach (var rewardIdx in rewards)
            {
                // none 타입은 스킵 
                var type = DataboxController.GetDataString(Table.Shop, Sheet.reward, rewardIdx, Column.reward_type);
                if (type == "none")
                    continue;
                
                var newReward = new RewardData
                {
                    Type = type,
                    Idx = DataboxController.GetDataString(Table.Shop, Sheet.reward, rewardIdx, Column.reward_idx),
                    Count = DataboxController.GetDataInt(Table.Shop, Sheet.reward, rewardIdx, Column.reward_count),
                };
                rewardsData.Add(newReward);
            }

            return rewardsData;
        }
        
        private static List<RewardData> GetRewardsDataWithType(string idx, string sheet, string rewardType)
        {
            var rewards = GetRewards(idx, sheet);
            
            var rewardsData = new List<RewardData>();
            foreach (var rewardIdx in rewards)
            {
                // none 타입은 스킵 
                var type = DataboxController.GetDataString(Table.Shop, Sheet.reward, rewardIdx, Column.reward_type);
                
                // 검색하려는 타입과 일치하지 않으면 continue
                if (type != rewardType) continue;
                
                if (type == "none")
                    continue;

                var newReward = new RewardData
                {
                    Type = type,
                    Idx = DataboxController.GetDataString(Table.Shop, Sheet.reward, rewardIdx, Column.reward_idx),
                    Count = DataboxController.GetDataInt(Table.Shop, Sheet.reward, rewardIdx, Column.reward_count),
                };
                rewardsData.Add(newReward);
            }

            return rewardsData;
        }
        
        public static bool CanPurchase(string idx, string sheet, out FailedReason reason)
        {
            reason = FailedReason.NotFailed;
            
            // limited_count 체크 (이 체크부터 먼저 함. 횟수가 부족하면 아예 구매 버튼조차 안 나오게 하려면 요걸르 반환해야 함)
            var limitedCount = GetLimitedCount(idx, sheet);
            // 무제한 구매가 아니라면 
            if (limitedCount != 0)
            {
                // 구매 갯수 남아있는지 체크 
                if (!HasRemainCountToPurchase(idx, sheet))
                {
                    reason = FailedReason.LimitedCount;
                    return false;
                }
            }
            
            // price_type 체크 
            var priceType = GetPriceType(idx, sheet);
            var priceValue = GetPriceValue(idx, sheet);
            switch (priceType)
            {
                case PriceType.free:
                    // 무료 타입은 이미 구매돼있다고 처리
                    {
                        reason = FailedReason.Free;
                        return false;    
                    }
                case PriceType.payment:
                    // 이전 결제 진행 프로세스 있는지 체크
                    // 이 처리를 무시하고 구매 진행 시, UnityPurchasing에서 OnPurchaseFailed로 실패 때림   
                    if (UnityPurchaser.IsExistingPurchasePending)
                    {
                        reason = FailedReason.ExistingPurchasePending;
                        return false;
                    }
                    break;
                case PriceType.gem:
                    // 잼 충분 여부 체크 
                    if (UserData.My.Resources.uranium < priceValue)
                    {
                        reason = FailedReason.NotEnoughGem;
                        return false;
                    }
                    break;
                case PriceType.ad:
                    // 광고 쿨타임 체크 
                    if (GetRemainCoolTime(idx, sheet) > 0)
                    {
                        reason = FailedReason.CoolTime;
                        return false;
                    } 
                    break;
                case PriceType.mileage:
                    // 마일리지 충분 여부 체크 
                    if (UserData.My.Resources.mileage < priceValue)
                    {
                        reason = FailedReason.NotEnoughMileage;
                        return false;
                    }
                    break;
                case PriceType.energy:
                    // 에너지 충분 여부 체크 
                    if (UserData.My.Missiles.AncientEnergy < priceValue)
                    {
                        reason = FailedReason.NotEnoughEnergy;
                        return false;
                    }
                    break;
                case PriceType.chipset:
                    // 강화칩 충분 여부 체크 
                    if (UserData.My.Items[RewardType.REWARD_CHIPSET] < priceValue)
                    {
                        reason = FailedReason.NotEnoughChipset;
                        return false;
                    }
                    break;
            }

            // 위의 모든 체크를 통과헀다면, 구매할 수 있다는 것 
            return true;
        }

        public static void Purchase(string idx, string sheet, Action<bool, FailedReason> callback)
        {
            // 구매 여부 체크 
            if (!CanPurchase(idx, sheet, out var reason))
            {
                callback(false, reason);
                return;
            }

            // price_type 체크
            var priceType = GetPriceType(idx, sheet);
            var priceValue = GetPriceValue(idx, sheet);
            switch (priceType)
            {
                case PriceType.payment:
                {
                    // 결제
                    var productId = GetProductId(idx, sheet);
                    UnityPurchaser.Purchase(productId, (success, id) =>
                    {
                        if (!success || productId != id)
                        {
                            // 결제 실패
                            callback(false, FailedReason.FailedIAP);
                            return;
                        }
                
                        // 상품 획득
                        CommonPurchase(idx, sheet);
                        
                        // 결제 성공
                        callback(true, FailedReason.NotFailed);
                    });
                }
                    break;
                
                case PriceType.gem:
                {
                    // 잼 차감
                    ResourceController.Instance.SubtractUranium(priceValue, $"{LogEvent.Purchase}_{idx}");
                    
                    // 상품 획득
                    CommonPurchase(idx, sheet);
                    
                    // 성공
                    callback(true, FailedReason.NotFailed);
                }
                    break;
                
                case PriceType.ad:
                {
                    // 광고 보기 (광고 제거 있으면, 안에서 바로 success 떨어짐)
                    AdsMediator.Instance.ShowVideoAds((success) =>
                    {
                        if (!success)
                        {
                            callback(false, FailedReason.FailedAd);
                            return;
                        }
                        
                        // 상품 획득
                        CommonPurchase(idx, sheet);
                        
                        // 성공
                        callback(true, FailedReason.NotFailed);
                    });
                }
                    break;
                
                case PriceType.mileage:
                {
                    // 마일리지 차감
                    UserData.My.Resources.mileage -= priceValue;
                    
                    // 로그 
                    ServerLogger.Log(RewardType.REWARD_MILEAGE, $"{LogEvent.Purchase}_{idx}", -priceValue, UserData.My.Resources.mileage);
                    
                    // 상품 획득
                    CommonPurchase(idx, sheet);
                    
                    // 성공
                    callback(true, FailedReason.NotFailed);
                }
                    break;
                
                case PriceType.energy:
                {
                    // 에너지 차감
                    UserData.My.Missiles.SubEnergy(priceValue);
                    
                    // 로그 
                    ServerLogger.Log(RewardType.REWARD_ENERGY, $"{LogEvent.Purchase}_{idx}", -priceValue, UserData.My.Missiles.AncientEnergy);
                    
                    // 상품 획득
                    CommonPurchase(idx, sheet);
                    
                    // 성공
                    callback(true, FailedReason.NotFailed);
                }
                    break;
                
                case PriceType.chipset:
                {
                    // 강화칩 차감 
                    UserData.My.Items[RewardType.REWARD_CHIPSET] -= priceValue;
            
                    // 로그 
                    ServerLogger.LogItem(RewardType.REWARD_CHIPSET, $"{LogEvent.Purchase}_{idx}", -priceValue);
                    
                    // 상품 획득
                    CommonPurchase(idx, sheet);
                    
                    // 성공
                    callback(true, FailedReason.NotFailed);
                }
                    break;
            }
        }

        public static void ShowFailedMessage(FailedReason reason)
        {
            switch (reason)
            {
                case FailedReason.NotFailed:
                    break;
                case FailedReason.NotEnoughMileage:
                    MessagePopup.Show("shop_08");
                    break;
                case FailedReason.NotEnoughGem:
                    MessagePopup.Show("info_237");
                    break;
                case FailedReason.NotEnoughEnergy:
                    MessagePopup.Show("shop_42");
                    break;
                case FailedReason.NotEnoughChipset:
                    MessagePopup.Show("shop_43");
                    break;
                case FailedReason.FailedAd:
                    MessagePopup.Show("shop_07");
                    break;
                case FailedReason.CoolTime:
                    MessagePopup.Show("shop_26");
                    break;
                case FailedReason.ExistingPurchasePending:
                    MessagePopup.Show("info_369");
                    break;
                case FailedReason.LimitedCount:
                case FailedReason.FailedIAP:
                    MessagePopup.Show("shop_25");
                    break;
            }
        }
        
        private static void CommonPurchase(string idx, string sheet)
        {
            // 결제 프로세스가 연동돼있기에, 결제 프로세스 전에 구매 체크를 선행할 것 
            // 이 함수가 호출됐다면, 이미 결제가 진행된 후 넘어왔다고 간주
            
            // 해당 상품 획득  
            var rewardsData = GetRewardsData(idx, sheet);
            
            // 트랜잭터에게 보내서 획득 처리 
            var productId = GetProductId(idx, sheet);
            var logIdx = string.IsNullOrEmpty(productId) ? idx : productId;
            RewardTransactor.Transact(rewardsData, $"{LogEvent.Purchase}_{logIdx}");
            
            // 구매 타임스탬프 넣기  
            UserData.My.Purchase.StampLastPurchasedTime(idx);

            // 구매 횟수 증가 
            UserData.My.Purchase.AddPurchasedCount(idx);

            // 유저 데이터 즉각 저장
            UserData.My.SaveToServer();
        }

        public static void RestoreProduct(string productId)
        {
            // 해당 productId를 가지는 상품 찾기 
            var iapAllIdxes = DataboxController.GetAllIdxes(Table.Shop, Sheet.iap);
            foreach (var iapIdx in iapAllIdxes)
            {
                var google = DataboxController.GetDataString(Table.Shop, Sheet.iap, iapIdx, Column.google);
                var ios = DataboxController.GetDataString(Table.Shop, Sheet.iap, iapIdx, Column.ios);

                if (productId == google || productId == ios)
                {
                    // 해당 iapIdx를 가지는 시트와 인덱스 찾기 
                    FindIdxAndSheet(iapIdx, out var foundIdx, out var foundSheet);
                    
                    // 구매 처리 
                    CommonPurchase(foundIdx, foundSheet);
                    
                    break;
                }
            }
        }

        private static void FindIdxAndSheet(string iapIdx, out string foundIdx, out string foundSheet)
        {
            // missile
            if (TryFindIdxBy(iapIdx, Sheet.missile, out foundIdx))
            {
                foundSheet = Sheet.missile;
                return;
            }
            
            // ancient
            if (TryFindIdxBy(iapIdx, Sheet.ancient, out foundIdx))
            {
                foundSheet = Sheet.ancient;
                return;
            }

            // goods
            if (TryFindIdxBy(iapIdx, Sheet.goods, out foundIdx))
            {
                foundSheet = Sheet.goods;
                return;
            }
            
            // bazooka
            if (TryFindIdxBy(iapIdx, Sheet.bazooka, out foundIdx))
            {
                foundSheet = Sheet.bazooka;
                return;
            }
            
            // gem
            if (TryFindIdxBy(iapIdx, Sheet.gem, out foundIdx))
            {
                foundSheet = Sheet.gem;
                return;
            }
            
            // mileage
            if (TryFindIdxBy(iapIdx, Sheet.mileage, out foundIdx))
            {
                foundSheet = Sheet.mileage;
                return;
            }
            
            // battle_pass
            if (TryFindIdxBy(iapIdx, Sheet.BattlePass, out foundIdx))
            {
                foundSheet = Sheet.BattlePass;
                return;
            }
            
            // special
            if (TryFindIdxBy(iapIdx, Sheet.special, out foundIdx))
            {
                foundSheet = Sheet.special;
                return;
            }
            
            // season
            if (TryFindIdxBy(iapIdx, Sheet.season, out foundIdx))
            {
                foundSheet = Sheet.season;
                return;
            }
            
            // stone
            if (TryFindIdxBy(iapIdx, Sheet.stone, out foundIdx))
            {
                foundSheet = Sheet.stone;
                return;
            }
            
            // stone2
            if (TryFindIdxBy(iapIdx, Sheet.stone2, out foundIdx))
            {
                foundSheet = Sheet.stone2;
                return;
            }
            
            // event
            if (TryFindIdxBy(iapIdx, Sheet.@event, out foundIdx))
            {
                foundSheet = Sheet.@event;
                return;
            }
            
            foundSheet = "";
        }

        private static bool TryFindIdxBy(string iapIdx, string sheet, out string foundIdx)
        {
            var allIdxes = DataboxController.GetAllIdxes(Table.Shop, sheet);
            foreach (var idx in allIdxes)
            {
                var foundIapIdx = DataboxController.GetDataString(Table.Shop, sheet, idx, Column.iap_idx);
                if (foundIapIdx != iapIdx) 
                    continue;
                
                foundIdx = idx;

                return true;
            }

            foundIdx = "";
            return false;
        }


        public static int GetPriceValue(string idx, string sheet)
        {
            return DataboxController.GetDataInt(Table.Shop, sheet, idx, Column.price_value);    
        }

        public static int GetRewardValue(string idx, string sheet)
        {
            return DataboxController.GetDataInt(Table.Shop, sheet, idx, Column.reward_value);
        }

        public static PriceType GetPriceType(string idx, string sheet)
        {
            return (PriceType)Enum.Parse(typeof(PriceType), DataboxController.GetDataString(Table.Shop, sheet, idx, Column.price_type));
        }

        public static int GetRemainCoolTime(string idx, string sheet)
        {
            // 한 번도 구매 안 했다면, 쿨타임 없음
            var lastPurchasedTimestamp = GetLastPurchasedTimestamp(idx);
            if (lastPurchasedTimestamp == 0)
                return 0;

            var coolTime = DataboxController.GetDataInt(Table.Shop, sheet, idx, Column.ad_cooltime);
            var endCoolTime = lastPurchasedTimestamp + coolTime;

            // 현재 시간이 쿨타임 종료 시간을 넘어섰다면, 남은 시간 없음 
            if (ServerTime.Instance.NowUnscaled.ToTimestamp() > endCoolTime)
            {
                return 0;
            }
            // 그렇지 않고, 쿨타임이 아직 남았다면
            else
            {
                // 쿨타임 남은 시간 반환
                return (int)(endCoolTime - ServerTime.Instance.NowUnscaled.ToTimestamp());
            }
        }

        public static void GetPriceInfo(string idx, string sheet, out PriceType type, out string icon, out int value)
        {
            type = GetPriceType(idx, sheet);
            icon = DataboxController.GetDataString(Table.Shop, sheet, idx, Column.price_icon);
            value = GetPriceValue(idx, sheet);
        }

        public static bool HasAnyCanPurchasePriceType(string sheet, PriceType priceType)
        {
            var allIdxes = DataboxController.GetAllIdxes(Table.Shop, sheet);
            foreach (var idx in allIdxes)
            {
                // price_type 다르면 continue
                if (GetPriceType(idx, sheet) != priceType) continue;

                // 상품 중, 구매 가능한 게 하나라도 있다면, true 반환
                if (CanPurchase(idx, sheet, out _)) return true;
            }

            // 하나도 없으면 false 반환
            return false;
        }

        public static int GetGachaCount(string idx, string sheet)
        {
            return DataboxController.GetDataInt(Table.Shop, sheet, idx, Column.reward_gacha_count);
        }
        
        public static string GetGachaGroup(string idx, string sheet)
        {
            return DataboxController.GetDataString(Table.Shop, sheet, idx, Column.reward_gacha_group);
        }
    }
}