﻿using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using MEC;
using UI.Popup;
using UI.Popup.Option;
using UniRx;
using UnityEngine;

namespace InGame.Controller
{
    public class AutoPowerSavingController : MonoBehaviourSingleton<AutoPowerSavingController>
    {
        private CoroutineHandle _coroutineHandle;
        private bool _isInitialize = false;
        private float _autoPowerSavingDelayTime = 900f;

        public void StartAutoPowerSaving()
        {
            if (_isInitialize) return;
            _isInitialize = true;

            _autoPowerSavingDelayTime = 900f; // 15분*60초

            if (ExtensionMethods.IsDevelopmentBuild() || ExtensionMethods.IsUnityEditor())
            {
                _autoPowerSavingDelayTime = 5*60;
            }
            
            // 유저 터치 때마다 오토 타이머 리셋 
            Observable.EveryUpdate()
                .Where(_ => Input.GetMouseButtonDown(0))
                .Subscribe(xs => { ResetAutoTimer(); })
                .AddTo(Instance.gameObject);

            // 최초의 오토 타이머 시작
            ResetAutoTimer();
        }

        private void ResetAutoTimer()
        {
            Timing.KillCoroutines(_coroutineHandle);
            _coroutineHandle = Timing.RunCoroutine(_DelayAndShowPopup(), Segment.RealtimeUpdate);
        }

        private IEnumerator<float> _DelayAndShowPopup()
        {
            yield return Timing.WaitForSeconds(_autoPowerSavingDelayTime);
            yield return Timing.WaitForOneFrame; // 프레임간 차이가 생길 수 있으니 잠깐 기다려줌.
            
            // 예외처리_ 절전모드 대기 시간이 있어서 발생하지는 않을 것 같긴 한데, 게임실행하자마자 키고 가만히 쭉 놔두면 생길 수 있어서 추가.
            if (TutorialPopup.Instance != null && TutorialPopup.IsPlaying) yield break;
            
            // 절전 모드 보여줄 수 있는가 
            if (!PowerSavingPopup.CanShow) yield break;

            // 이미 보여지고 있다면, 보여주지 않음 
            var powerSavingPopup = UIPopup.GetPopup("Power Saving");
            if (!UIPopup.VisiblePopups.Contains(powerSavingPopup))
                PowerSavingPopup.Show();
        }
    }
}