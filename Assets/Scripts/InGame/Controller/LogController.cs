using System.Collections.Generic;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using UI.Popup;
using UnityEngine;

namespace InGame
{
    public class LogController : MonoBehaviourSingleton<LogController>
    {
        public void StartAutoLog()
        {
            UserData.My.Stage.OnChangedBestStage += OnChangedBestStage;
            UserData.My.Resources.OnChangedDiabloKey += OnChangedDiabloKey;
        }
        
        public void StopAutoLog()
        {
            UserData.My.Stage.OnChangedBestStage -= OnChangedBestStage;
            UserData.My.Resources.OnChangedDiabloKey -= OnChangedDiabloKey;
        }
        
        private void OnChangedDiabloKey()
        {
            var changedDiabloKeyCount = PlayerPrefs.GetInt("changedDiabloKeyCount", 0);
            if (changedDiabloKeyCount <= 100)
            {
                changedDiabloKeyCount += 1;
                PlayerPrefs.SetInt("changedDiabloKeyCount", changedDiabloKeyCount);
                
                // 파이어베이스 미사일 연속뽑기 트래킹용도
                var firebaseEventTrackingCount = new List<int> { 5, 10, 20, 30, 40, 50, 60, 70, 80, 90, 100 };
                if (firebaseEventTrackingCount.Contains(changedDiabloKeyCount))
                {
                    WebLog.Instance.ThirdPartyLog($"get_diablo_key_count_{changedDiabloKeyCount}", new Dictionary<string, object>());
                }
            }
        }
        
        private void OnChangedBestStage(int bestStage)
        {
            if (bestStage == 51 || bestStage == 201 || bestStage == 501 || (bestStage >= 1001 && bestStage % 1000 == 1))
            {
                WebLog.Instance.AllLog("stage_clear", new Dictionary<string, object>{{"stage", (bestStage - 1)}});
            }

            if (bestStage % 100 == 1)
            {
                var bestStageRenewalCount = PlayerPrefs.GetInt("bestStageRenewalCount", 0);
                if (bestStageRenewalCount <= 10)
                {
                    bestStageRenewalCount += 1;
                    PlayerPrefs.SetInt("bestStageRenewalCount", bestStageRenewalCount);
                    WebLog.Instance.ThirdPartyLog($"bestStage_Renewal_{bestStageRenewalCount}", new Dictionary<string, object>());
                }
            }
                    
            WebLog.Instance.ThirdPartyLog("best_stage", new Dictionary<string, object>{{"stage",bestStage.ToLookUpString()}});
        }
    }
}