using System;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using SA.iOS.StoreKit;
using UI.Popup;
using UnityEngine;

namespace InGame.Controller
{
    public class ReviewController : MonoBehaviourSingleton<ReviewController>
    {
        public void StartToCheck()
        {
            UserData.My.Stage.OnChangedBestStage += OnChangedBestStage;
        }
        
        public void StopToCheck()
        {
            UserData.My.Stage.OnChangedBestStage -= OnChangedBestStage;
        }

        private void OnChangedBestStage(int bestStage)
        {
#if UNITY_ANDROID
            // 안드로이드_ 로그인 하지 않은 유저는 불가 
            if (!PlayGamesController.Authenticated) return;
#endif
            
            // 튜토리얼 상태에서는 리뷰 팝업이 나오지 않도록 막음. (튜토리얼중 리뷰팝업이 나오면 게임 진행이 안되어서 처리)
            if (TutorialPopup.Instance != null && TutorialPopup.IsPlaying) return;

            var isWatchedReviewPopup = UserData.My.UserInfo.GetFlag(UserFlags.IsWatchedReviewPopup);
            if (isWatchedReviewPopup) return;

            // 파베에 등록된 스테이지일 경우, 리뷰 팝업 띄움 
            var reviewStage = FirebaseRemoteConfigController.inAppReviewStage;
            if (ExtensionMethods.IsUnityEditor())
            {
                reviewStage =
 DataboxController.GetDataInt(Table.Cheat, Sheet.Cheat, Row.Test, Column.review_stage, 1001);
            }

            if (bestStage >= reviewStage)
            {
                UserData.My.UserInfo.SetFlag(UserFlags.IsWatchedReviewPopup, this);
                
                YesOrNoPopup.Show("info_331", "info_330", "info_332", "info_142",
                    ()=>
                    {
                        #if UNITY_ANDROID
                            PlayGamesController.Instance.RequestReview();
                        #elif UNITY_IOS
                            ISN_SKStoreReviewController.RequestReview();
                        #endif
                    }, null);
            }
        }
    }
}