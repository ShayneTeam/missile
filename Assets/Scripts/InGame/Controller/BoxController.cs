﻿using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Global;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using Server;
using TMPro;
using UI;
using UnityEngine;

namespace InGame
{
    public class BoxController : MonoBehaviourSingleton<BoxController>
    {
        public static event IntDelegate OnGachaBox;


        public static int GetBoxCount(string boxIdx)
        {
            return UserData.My.Boxes[boxIdx];
        }

        public static void AddBox(string boxIdx, int amount = 1)
        {
            var boxMaxStr = DataboxController.GetConstraintsData("STAGE_BOX_COUNT_MAX", "999");
            var boxMax = int.Parse(boxMaxStr);

            var currentBoxCount = GetBoxCount(boxIdx);
            var addedBoxCount = Mathf.Clamp(currentBoxCount + amount, 0, boxMax);
            
            UserData.My.Boxes[boxIdx] = addedBoxCount;
        }
	
        public static IEnumerable<RewardData> GachaBox(string boxIdx)
		{
			// 뽑히는 보상 담아둘 컨테이너
			var pickedRewards = new List<RewardData>();
			
			// 전부 박스깡
			var boxCount = GetBoxCount(boxIdx);
			for (var count = 0; count < boxCount; count++)
			{
				// 박스의 번들 가져오기 
				var bundle = DataboxController.GetDataString(Table.Box, Sheet.Box, boxIdx, Column.Bundle);
				var allIdxesBundle = DataboxController.GetAllIdxesByGroup(Table.Box, Sheet.BoxBundle, Column.Bundle, bundle);
				foreach (var bundleIdx in allIdxesBundle)
				{
					PickInTheBundle(bundleIdx, ref pickedRewards);
				}
			}

			/* 만약 아이템이 많이 뽑히는 경우 그만큼 loop를 돌며 하나하나 아이템이 들어가게 되어있고.
			 * 수천개의 아이템이 들어갈때에 에디터 및 AOS, IOS기기에서 아주 장시간 끊기는 현상이 발생하여 'GroupBy'를 적용, loop의 수를 줄여서 끊기지 않도록 만든다.
			 * 2021.08.17 테스트 결과 11000개 기준 45초동안 에디터가 끊겼는데, 0.8초로 줄어든것을 확인했음. */
			var orderedRewards = pickedRewards.GroupBy(x => x.Idx)
				.Select(g => new RewardData
				{
					Idx = g.Key,
					Type = g.First().Type,
					Count = g.Sum(x => x.Count)
				}).OrderByDescending(x => x.Idx);
			
			// 뽑힌 아이템 DB 반영 
			RewardTransactor.Transact(orderedRewards, LogEvent.BoxGacha);
				
			// 전부 박스깡했으니, 박스 개수 전부 차감 
			UserData.My.Boxes[boxIdx] = 0;	
			
			// 바로 저장 
			UserData.My.SaveToServer();
			
			// 이벤트 
			OnGachaBox?.Invoke(boxCount);
			
			// 뽑힌 아이템 반환 
			return orderedRewards;
		}

        private static void PickInTheBundle(string bundleIdx, ref List<RewardData> pickedRewardItems)
        {
	        // 가챠 갯수 결정 
	        var countMin = DataboxController.GetDataInt(Table.Box, Sheet.BoxBundle, bundleIdx, "count_min");
	        var countMax = DataboxController.GetDataInt(Table.Box, Sheet.BoxBundle, bundleIdx, "count_max");
	        var pickCount = Random.Range(countMin, countMax + 1);

	        var rewardType = DataboxController.GetDataString(Table.Box, Sheet.BoxBundle, bundleIdx, Column.reward_type); 
	        var rewardGroup = DataboxController.GetDataString(Table.Box, Sheet.BoxBundle, bundleIdx, "reward_group"); 

	        // 가챠 
	        for (var count = 0; count < pickCount; count++)
	        {
		        switch (rewardType)
		        {
			        case "REWARD_MISSILE_GROUP":
			        {
				        var pickedRewardIdx = DataboxController.RandomPick(Table.Box, Sheet.BoxMissile, "group", rewardGroup, Column.chance);

				        var rewardItem = new RewardData
				        {
					        Idx = DataboxController.GetDataString(Table.Box, Sheet.BoxMissile, pickedRewardIdx, "missile_idx"), 
					        Type = DataboxController.GetDataString(Table.Box, Sheet.BoxMissile, pickedRewardIdx, Column.reward_type),
					        Count = DataboxController.GetDataInt(Table.Box, Sheet.BoxMissile, pickedRewardIdx, Column.reward_count)
				        };

				        pickedRewardItems.Add(rewardItem);
			        }
				        break;

			        case "REWARD_ITEM_GROUP":
			        {
				        var pickedRewardIdx = DataboxController.RandomPick(Table.Box, Sheet.BoxItem, "group", rewardGroup, Column.chance);

				        var rewardItem = new RewardData
				        {
					        Idx = "",	// 일반 아이템은 타입이 곧 Idx
					        Type = DataboxController.GetDataString(Table.Box, Sheet.BoxItem, pickedRewardIdx, Column.reward_type),
					        Count = DataboxController.GetDataInt(Table.Box, Sheet.BoxItem, pickedRewardIdx, Column.reward_count)
				        };

				        pickedRewardItems.Add(rewardItem);
			        }
				        break;
		        }
	        }
        }
    }
}