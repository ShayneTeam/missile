using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using UI.Popup;
using UnityEngine;

namespace InGame
{
    public class LeaderBoardController : MonoBehaviourSingleton<LeaderBoardController>
    {
        public void StartAutoReport()
        {
            UserData.My.Stage.OnChangedBestStage += OnChangedBestStage;
        }
        
        public void StopAutoReport()
        {
            UserData.My.Stage.OnChangedBestStage -= OnChangedBestStage;
        }

        private void OnChangedBestStage(int bestStage)
        {
#if UNITY_ANDROID
            PlayGamesController.ReportScore(bestStage);
#endif
        }
    }
}