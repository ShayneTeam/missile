﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using Databox.Dictionary;
using Global;
using InGame.Data;
using InGame.Enemy;
using InGame.Global;
using MEC;
using Server;
using UnityEngine;

namespace InGame.Controller
{
    public class OfflineRewardController : MonoBehaviourSingleton<OfflineRewardController>
    {
        private CoroutineHandle autoStamp;
        
        //  오프라인 보상을 획득한 다음부터 갱신돼야 함 
        public void RegisterAutoStampLastPlayedTime()
        {
            Timing.KillCoroutines(autoStamp);
            autoStamp = Timing.RunCoroutine(AutoStampLastPlayedTimeFlow().CancelWith(gameObject), Segment.RealtimeUpdate);
        }
		
        private static IEnumerator<float> AutoStampLastPlayedTimeFlow()
        {
            while (Instance.enabled)
            {
                // 이 함수가 호출되자마자 바로 저장 (오프라인 보상 받고, 바로 끄고 재접속하면 오프라인 스탬프가 갱신안되서 다시 받을 수 있음) 
                UserData.My.UserInfo.StampLastPlayedTime(ServerTime.Instance.NowSecUnscaled);
                
                // 1분마다 현재 시간 기록 
                yield return Timing.WaitForSeconds(60f); // 오프라인 처리가 분 단위 처리므로 이렇게 기록 
            }
        }
        
        public static bool CanEarnRewards()
        {
            // 유저가 신규 가입 됐을 땐 못 오프라인 보상 못 받게 처리  
            if (UserData.My.UserInfo.LastPlayedTimestamp == 0)
                return false;
            
            var offlineTime = GetOfflineTimeMinutes();
            var min = int.Parse(DataboxController.GetConstraintsData(Constraints.OFFLINE_REWARD_TIME_MIN, "10"));
            
            // 오프라인 보상 최소 기준 시간보다 짧으면 false
            if (offlineTime < min)
                return false;
            
            return true;
        }

        public static int GetOfflineTimeMinutes()
        {
            var lastPlayedTimestamp = UserData.My.UserInfo.LastPlayedTimestamp;

            // 현재와 차이 비교 
            var diff = ServerTime.Instance.NowSecUnscaled - lastPlayedTimestamp;

            // 초 단위를 분으로 변환 
            var offlineTimeMinutes = (int)(diff / 60);

            // 오프라인 시간 리미트 적용 
            return Mathf.Clamp(offlineTimeMinutes, 0, GetMaxOfflineTime());
        }

        public static int GetMaxOfflineTime()
        {
            return int.Parse(DataboxController.GetConstraintsData(Constraints.OFFLINE_REWARD_TIME_MAX, "600"));
        }

        public static void GetRewards(out double mineral, out OrderedDictionary<string, int> boxes)
        {
            var offlineTimeMin = GetOfflineTimeMinutes();

            // 미네랄 계산
            var offlineTimeKillPerMin = int.Parse(DataboxController.GetConstraintsData(Constraints.OFFLINE_REWARD_TIME_KILL, "1"));
            var offlineKillMonsterCount = offlineTimeMin * offlineTimeKillPerMin;

            var stageMonsterIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.Monster);
            var firstMonsterIdx = stageMonsterIdxes.First();
            var firstMonsterRewardMineral = DataboxController.GetDataInt(Table.Stage, Sheet.Monster, firstMonsterIdx, Column.RewardMineral);
            var bestStageMonsterMineral = InGameControlHub.My.StageController.ModifyPlanetAndStageWeightByBestStage(firstMonsterRewardMineral, Column.reward_mineral_per, Column.reward_mineral_stage_weight);

            mineral = offlineKillMonsterCount * bestStageMonsterMineral;
			
		    // 박스 계산 
		    var offlineRewardBoxPerMin = int.Parse(DataboxController.GetConstraintsData(Constraints.OFFLINE_REWARD_TIME_BOX, "1"));
            var offlineBoxCount = offlineTimeMin / offlineRewardBoxPerMin;
            
            boxes = new OrderedDictionary<string, int>();
            for (var i = 0; i < offlineBoxCount; i++)
            {
                var pickedIdx = DataboxController.RandomPick(Table.Box, Sheet.box_offline, Column.chance);
                var boxIdx = DataboxController.GetDataString(Table.Box, Sheet.box_offline, pickedIdx, Column.box_idx);
			
                if (!boxes.ContainsKey(boxIdx))
                    boxes[boxIdx] = 0;
				
                boxes[boxIdx]++;
            }
        }
    }
}