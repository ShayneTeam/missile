﻿using System.Collections.Generic;
using InGame.Data;
using InGame.Global;
using Server;
using UI.Popup.Menu.Event;
using UnityEngine;

namespace InGame.Controller.Event
{
    public partial class EventController : MonoBehaviourSingleton<EventController>
    {
        public class NAttendance
        {
            public static bool CanClaim => UserData.My.Event.NextAttendanceTimestamp == 0 || ServerTime.Instance.NowUnscaled.ToTimestamp() > UserData.My.Event.NextAttendanceTimestamp;

            public static void Claim(string rewardType, int rewardCount)
            {
                if (!CanClaim)
                {
                    ULogger.Log("출석 보상을 받을 수 없는 상황입니다.");
                    return;
                }
            
                var reward = new RewardData
                {
                    Idx = "",	// 일반 아이템은 타입이 곧 Idx
                    Type = rewardType,
                    Count = rewardCount
                };
                RewardTransactor.Transact(reward, LogEvent.Attendance);

                // 출석일 증가 
                UserData.My.Event.AddAttendance(DailyTimeController.GetEndTimeOf(ServerTime.Instance.NowUnscaled).ToTimestamp());

                // 로그
                WebLog.Instance.AllLog($"{LogEvent.Attendance}_Reward_Received_log", new Dictionary<string, object>{{"reward", reward},{"reward_date_num", UserData.My.Event.AttendanceCount}});

                // 유저 데이터 즉각 저장
                UserData.My.SaveToServer();

                // 통지
                OnReceivedEvent?.Invoke();
            }

            public static void Reset()
            {
                // 이벤트 출석 초기화 조건을 만족한 경우, 변수를 초기화하여 다시 받을 수 있도록 함.
                if (UserData.My.Event.AttendanceCount == 7 && 
                    UserData.My.Event.NextAttendanceTimestamp != 0 && 
                    ServerTime.Instance.NowUnscaled.ToTimestamp() > UserData.My.Event.NextAttendanceTimestamp) // 다음에 받을 수 있는 시간-현재 서버시간의 날짜가 1일이상 차이가 날 경우. 
                {
                    UserData.My.Event.ResetAttendanceTime();
                    ULogger.Log("이벤트 7일 출석이 초기화 됨.");
                }
                else
                {
                    ULogger.Log("이벤트 7일 출석이 초기화 되지 않음 (조건 부합)");
                }
            }

            public static void ShowPopupIfCan()
            {
                if (!CanClaim) return;
                
                // 팝업 열기 
                EventMenuPopup.Show(EventTab.Attendance);
            }
        }

        public readonly NAttendance Attendance = new NAttendance();
    }
}