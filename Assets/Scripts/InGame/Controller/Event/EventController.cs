﻿using System.Collections.Generic;
using Global;
using InGame.Data;
using InGame.Global;
using Server;
using UI;
using UI.Popup;
using UnityEngine;

namespace InGame.Controller.Event
{
    public partial class EventController : MonoBehaviourSingleton<EventController>
    {
        public static event ServerEventDelegate OnReceivedEvent;

        public bool HasAnyEventToReceive()
        {
            // 킬 포인트가 300 이상인 경우
            if (HasEventHunterShopToReceive())
                return true;

            // 출석 이벤트를 수신할 수 있는 경우
            if (NAttendance.CanClaim)
                return true;

            // 배틀패스 1
            if (HasBattlePassToReceive(BattlePassMenu.BattlePassType.SpaceA))
                return true;

            // 배틀패스 2
            if (HasBattlePassToReceive(BattlePassMenu.BattlePassType.SpaceB))
                return true;

            // 몬스터 헌터
            if (HasBattlePassToReceive(BattlePassMenu.BattlePassType.MonsterHunterA))
                return true;

            return false;
        }

        #region 배틀패스 관련 부분

        public bool HasBattlePassToReceive(BattlePassMenu.BattlePassType passType)
        {
            return UserData.My.BattlePass.PossibleAnyReceiveCheck(passType);
        }
        
        public bool HasPreEventBattlePassToReceive(string battlePassType, string sheetName, string idx, int condition)
        {
            return HasEventBattlePassToReceive(battlePassType, sheetName, idx, condition, true);
        }
        
        public bool HasEventBattlePassToReceive(string battlePassType, string sheetName, string idx, int condition, bool prePurchaseCheck = false)
        {
            return UserData.My.BattlePass.PossibleReceiveCheck(battlePassType, sheetName, idx, condition, prePurchaseCheck);
        }

        public void PurchaseApplyBattlePass(string battlePassType, string sheetName, string idx)
        {
            // 적용
            UserData.My.BattlePass.PurchaseApplyBattlePass(battlePassType);
            
            // 구매 보상 지급
            var rewardType = DataboxController.GetDataString(Table.Event, sheetName, idx, Column.reward_type);
            var rewardCount = DataboxController.GetDataInt(Table.Event, sheetName, idx, Column.reward_count_buy); 
            
            // 보상 지급
            var reward = new RewardData
            {
                Idx = "",	// 일반 아이템은 타입이 곧 Idx
                Type = rewardType,
                Count = rewardCount
            };
            RewardTransactor.Transact(reward, $"{LogEvent.BattlePass}_{idx}");
        }
        
        public void BattlePassToReceive(string battlePassType, string sheetName, string idx, int condition)
        {
            // 배틀패스 클리어 처리
            var receiveReward = UserData.My.BattlePass.ReceiveReward(battlePassType, sheetName, idx, condition);
            if (!receiveReward) return;
            
            // 보상 확인
            var rewardType = DataboxController.GetDataString(Table.Event, sheetName, idx, Column.reward_type);
            var rewardCount = DataboxController.GetDataInt(Table.Event, sheetName, idx, Column.reward_count);
                
            // 보상 수령에 성공했습니다.
            MessagePopup.Show("info_303");

            // 보상 지급
            var reward = new RewardData
            {
                Idx = "",	// 일반 아이템은 타입이 곧 Idx
                Type = rewardType,
                Count = rewardCount
            };
            RewardTransactor.Transact(reward, $"{LogEvent.BattlePass}_{idx}");
            
            WebLog.Instance.AllLog($"{LogEvent.BattlePass}_{battlePassType}_Received_log", new Dictionary<string, object>{{"battlepass_type", battlePassType}, {"condition", condition.ToLookUpString()}, {"reward",reward}});
                
            OnReceivedEvent?.Invoke();
        }

        #endregion

        #region 헌터상점 관련 부분

        public void HunterShopReceive(string rewardType, int rewardCount, int price, string rewardIdx = "")
        {
            if (!HunterShopCheckIfCanPurchase(price))
            {
                MessagePopup.Show("shop_16");
                return;
            }
            
            // 킬포인트 감소
            UserData.My.Resources.killpoint -= price;

            // rewardIdx 가 빈값이 아닌 경우에는 일반상품, 값이 있으면  ‘DT_MS_Box’테이블의 ‘box’시트를 참조하여 ‘reward_idx’값과 동일한 박스 지급한다.
            if (!string.IsNullOrWhiteSpace(rewardIdx) && rewardIdx != "none")
            {
                BoxController.AddBox(rewardIdx, rewardCount);

                var boxSlotComponent = new GameObject().AddComponent<BoxSlot>();
                boxSlotComponent.ConsumeBox(rewardIdx);
                Destroy(boxSlotComponent);
            }
            else
            {
                // 상품 구매에 성공했습니다.
                MessagePopup.Show("shop_24");
		        
                // 보상 지급
                var reward = new RewardData
                {
                    Idx = "",	// 일반 아이템은 타입이 곧 Idx
                    Type = rewardType,
                    Count = rewardCount
                };
                RewardTransactor.Transact(reward, LogEvent.HunterShop);
            }
            
            // 유저 데이터 즉각 저장
            UserData.My.SaveToServer();
            
            // 사운드
            SoundController.Instance.PlayEffect("sfx_ui_bt_upgrade_01");
            
            OnReceivedEvent?.Invoke();
        }
        
        private bool HunterShopCheckIfCanPurchase(int price)
        {
            return InGameControlHub.My.Data.Resources.killpoint >= price;
        }
        
        public bool HasEventHunterShopToReceive()
        {
            return InGameControlHub.My.Data.Resources.killpoint >= 300;
        }

        #endregion
    }
    
    public delegate void ServerEventDelegate();

}