﻿using Global;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller
{
    public class WeightController : MonoBehaviourSingleton<WeightController>
    {
        public static double ModifyWeight(double baseValue, string table, string sheet, string idx, string weightColumn)
        {
            // 가중치 (파싱 타입 통일 중요!. 가중치 컬럼이 없으면 바로 0이 되서 알 수 있도록 defaultValue 따로 설정)
            var weight = DataboxController.GetDataDouble(table, sheet, idx, weightColumn, -1);
            
            // 없으면, FloatType으로 찾아봄 
            if (weight < 0)
                weight = DataboxController.GetDataFloat(table, sheet, idx, weightColumn, -1);
            
            // 없으면, IntType으로 찾아봄 
            if (weight < 0)
                weight = DataboxController.GetDataInt(table, sheet, idx, weightColumn);
		
            return baseValue * weight;
        }
    }
}