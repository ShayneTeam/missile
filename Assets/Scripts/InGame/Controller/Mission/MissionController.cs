﻿using System;
using System.Collections;
using System.Collections.Generic;
using BackEnd;
using Global;
using InGame.Data;
using InGame.Enemy;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace InGame.Controller
{
    // 미션컨트롤러는 미션데이터 이벤트를 받아 처리 함
    // 그러므로 미션데이터보다 먼저 생성돼있어야 함
    public partial class MissionController : MonoBehaviourSingleton<MissionController>
    {
        private CoroutineHandle _playtime;
        private static readonly List<string> _commonMissions = new List<string> { Sheet.daily, Sheet.battle, Sheet.enchant };

        public void StartMissionCheck()
        {
            // UserData가 확실히 생성되는 Start에서 이벤트 등록 
            InGameControlHub.My.DiabloRelicController.OnGacha += OnGachaDiabloRelic;
            UserData.My.DiabloRelic.OnLevelUp += OnLevelUpDiabloRelic;
            UserData.My.NormalRelic.OnEarned += OnEarnedRelic;
            UserData.My.NormalRelic.OnLevelUp += OnLevelUpNormalRelic;
            UserData.My.Stage.OnChangedBestStage += OnChangedBestStage;
            UserData.My.Bazooka.OnLevelUp += OnLevelUpBazooka;
            UserData.My.Bazooka.OnEnhance += OnEnhanceBazooka;
            UserData.My.Bazooka.OnLevelUpParts += OnLevelUpParts;
            UserData.My.Missiles.OnLevelUp += OnLevelUpMissile;
            UserData.My.Resources.OnEarnedDiabloKey += OnEarnedDiabloKey;
            Enemies.OnSpawnEnemyDied += OnEnemyDied;
            ShopController.MissileDelegator.OnGacha += OnGacha;
            MissileControl.OnSynthesize += OnSynthesizeMissile;
            AdsMediator.OnWatched += OnWatchedAds;
            PlunderControl.OnEndedPlunder += OnEndedPlunder;

            // 플레이타임 미션 처리용
            _playtime = Timing.RunCoroutine(PlaytimeFlow().CancelWith(gameObject), Segment.RealtimeUpdate);
        }

        private void OnDestroy()
        {
            StopMissionCheck();
        }

        public void StopMissionCheck()
        {
            // 이벤트 해제 
            InGameControlHub.My.DiabloRelicController.OnGacha -= OnGachaDiabloRelic;
            UserData.My.DiabloRelic.OnLevelUp -= OnLevelUpDiabloRelic;
            UserData.My.NormalRelic.OnEarned -= OnEarnedRelic;
            UserData.My.NormalRelic.OnLevelUp -= OnLevelUpNormalRelic;
            UserData.My.Stage.OnChangedBestStage -= OnChangedBestStage;
            UserData.My.Bazooka.OnLevelUp -= OnLevelUpBazooka;
            UserData.My.Bazooka.OnEnhance -= OnEnhanceBazooka;
            UserData.My.Bazooka.OnLevelUpParts -= OnLevelUpParts;
            UserData.My.Missiles.OnLevelUp -= OnLevelUpMissile;
            UserData.My.Resources.OnEarnedDiabloKey -= OnEarnedDiabloKey;
            Enemies.OnSpawnEnemyDied -= OnEnemyDied;
            ShopController.MissileDelegator.OnGacha -= OnGacha;
            MissileControl.OnSynthesize -= OnSynthesizeMissile;
            AdsMediator.OnWatched -= OnWatchedAds;
            PlunderControl.OnEndedPlunder -= OnEndedPlunder;

            // 플레이타임
            Timing.KillCoroutines(_playtime);
        }

        [PublicAPI]
        public static bool IsAnyFinishedMission()
        {
            return GetFinishedMissionsCount() > 0;
        }

        public static int GetFinishedMissionsCount()
        {
            var sum = 0;
            
            // daily
            var allDailies = DataboxController.GetAllIdxes(Table.Mission, Sheet.daily);
            foreach (var dailyIdx in allDailies)
            {
                // 획득하지 않았지만, 조건은 달성한 미션이 있다면 바로 return true; 
                if (CanEarnDaily(dailyIdx))
                    sum++;
            }
            
            // battle
            var allBattles = DataboxController.GetAllIdxes(Table.Mission, Sheet.battle);
            foreach (var battleIdx in allBattles)
            {
                // 획득하지 않았지만, 조건은 달성한 미션이 있다면 바로 return true; 
                if (CanEarnCommon(UserData.MissionData.CommonMissionType.battle, battleIdx))
                    sum++;
            }
            
            // enchant
            var allEnchants = DataboxController.GetAllIdxes(Table.Mission, Sheet.enchant);
            foreach (var enchantIdx in allEnchants)
            {
                // 획득하지 않았지만, 조건은 달성한 미션이 있다면 바로 return true; 
                if (CanEarnCommon(UserData.MissionData.CommonMissionType.enchant, enchantIdx))
                    sum++;
            }

            return sum;
        }

        public static void CollectAllMissions()
        {
            // daily
            var allDailies = DataboxController.GetAllIdxes(Table.Mission, Sheet.daily);
            foreach (var dailyIdx in allDailies)
            {
                EarnDaily(dailyIdx);
            }
            
            // battle
            var allBattles = DataboxController.GetAllIdxes(Table.Mission, Sheet.battle);
            foreach (var battleIdx in allBattles)
            {
                EarnCommonBatch(UserData.MissionData.CommonMissionType.battle, battleIdx);
            }
            
            // enchant
            var allEnchants = DataboxController.GetAllIdxes(Table.Mission, Sheet.enchant);
            foreach (var enchantIdx in allEnchants)
            {
                EarnCommonBatch(UserData.MissionData.CommonMissionType.enchant, enchantIdx);
            }
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }

        private static bool IsRepeat(string sheet, string idx)
        {
            return DataboxController.GetDataString(Table.Mission, sheet, idx, Column.Type) == "repeat";
        }
        
        private static bool IsMissionFinished(string sheet, string idx, int currentValue)
        {
            var type = DataboxController.GetDataString(Table.Mission, sheet, idx, Column.m_type);
            var value = DataboxController.GetDataInt(Table.Mission, sheet, idx, Column.m_value);
				
            // 스테이지 클리어 미션은 최고 도달 스테이지가 해당 스테이지를 넘어서야 함 
            if (type == MISSION_STAGE_CLEAR)
            {
                return UserData.My.Stage.bestStage >= value;
            }
            // 그외 미션들은 정해진 카운트를 채워야 함 
            else
            {
                return currentValue >= value;
            }
        }

        private void AddValue(string missionType, int count = 1)
        {
            // 동일 타입에 대해서만 감지하고 증가시킴
            
            // guide
            {
                // 현재 진행되고 있는 가이드의 미션 타입과 일치하면 AddValue
                UserData.My.Mission.GetGuideState(out var currentGuideIdx, out _);
                var dataMissionType = DataboxController.GetDataString(Table.Mission, Sheet.guide, currentGuideIdx, Column.m_type);
                if (dataMissionType == missionType)
                {
                    UserData.My.Mission.AddGuideValue(count);
                }
            }

            // common
            foreach (var commonMission in _commonMissions)
            {
                var allMissions = DataboxController.GetAllIdxes(Table.Mission, commonMission);
                foreach (var missionIdx in allMissions)
                {
                    var dataMissionType = DataboxController.GetDataString(Table.Mission, commonMission, missionIdx, Column.m_type);
                    if (dataMissionType == missionType)
                    {
                        // HACK. CommonMissionType과 Sheet가 일치하다는 전제하에 쓰인 코드
                        var commonType = (UserData.MissionData.CommonMissionType)Enum.Parse(typeof(UserData.MissionData.CommonMissionType), commonMission);
                        UserData.My.Mission.AddValue(commonType, missionIdx, count);
                    }
                }
            }
        }


        #region InGame Events

        private void OnChangedBestStage(int bestStage)
        {
            AddValue(MISSION_STAGE_CLEAR);
        }

        private void OnEarnedRelic(string relicIdx)
        {
            AddValue(MISSION_RELIC_BUY_COUNT);
        }
        
        private void OnGachaDiabloRelic(int count)
        {
            AddValue(MISSION_RELIC_BUY_COUNT, count);
        }
        
        private void OnLevelUpBazooka(int count)
        {
            AddValue(MISSION_BAZOOKA_UPGRADE_COUNT, count);
        }
        
        private void OnEnhanceBazooka()
        {
            AddValue(MISSION_BAZOOKA_GRADE_VALUE);
        }
        
        private void OnLevelUpParts(string partsIdx, int count)
        {
            AddValue(MISSION_PARTS_UPGRADE_COUNT, count);
        }

        private void OnEnemyDied(Enemy.Enemy enemy)
        {
            AddValue(MISSION_MONSTER_KILL);
            
            // 타입 세분화 
            if (enemy as Diablo)
            {
                AddValue(MISSION_DIABLO_KILL);
            }
            else if (enemy as Golem)
            {
                AddValue(MISSION_GOLEM_KILL);
            }
            else if (enemy as Portal)
            {
                AddValue(MISSION_GATE_KILL);
            }
            else if (enemy as Boss)
            {
                AddValue(MISSION_BOSS_KILL);
            }
            else if (enemy as Elite)
            {
                AddValue(MISSION_ELITE_KILL);
            }
            else if (enemy as Monster)
            {
                AddValue(MISSION_NORMAL_KILL);
            }
        }
        
        private void OnGacha()
        {
            AddValue(MISSION_GACHA_MISSILE);
        }
        
        private void OnSynthesizeMissile(int count)
        {
            AddValue(MISSION_MISSILE_MIXED_COUNT, count);
        }
        
        private void OnEarnedDiabloKey()
        {
            AddValue(MISSION_D_KEY_GET);
        }
        
        private void OnWatchedAds()
        {
            AddValue(MISSION_AD_COUNT);
        }

        private void OnLevelUpNormalRelic(string relicIdx, int count)
        {
            AddValue(MISSION_RELIC_UPGRADE_COUNT, count);
        }
        
        private void OnLevelUpDiabloRelic(string dRelicIdx)
        {
            AddValue(MISSION_D_RELIC_EVO_COUNT);
        }
        
        private void OnLevelUpMissile(string missileIdx)
        {
            AddValue(MISSION_MISSILE_UPGRADE_COUNT);
        }

        private IEnumerator<float> PlaytimeFlow()
        {
            // 플레이타임 미션 처리용 (딜레이 너무 짧으면 데이터 자주 바껴서, 서버 동기화 자주 됨)
            const int intervalSec = 10;
            while (true)
            {
                yield return Timing.WaitForSeconds(intervalSec);
                
                AddValue(MISSION_PLAY_TIME, intervalSec);
            }
        }
        
        private void OnEndedPlunder()
        {
            AddValue(MISSION_PLUNDER_COUNT);
        }

        private const string MISSION_STAGE_CLEAR = "MISSION_STAGE_CLEAR";
        private const string MISSION_RELIC_BUY_COUNT = "MISSION_RELIC_BUY_COUNT";
        private const string MISSION_BAZOOKA_UPGRADE_COUNT = "MISSION_BAZOOKA_UPGRADE_COUNT";
        private const string MISSION_MONSTER_KILL = "MISSION_MONSTER_KILL";
        
        private const string MISSION_DAILY_CLEAR_COUNT = "MISSION_DAILY_CLEAR_COUNT";
        private const string MISSION_PLAY_TIME = "MISSION_PLAY_TIME";
        private const string MISSION_GACHA_MISSILE = "MISSION_GACHA_MISSILE";
        private const string MISSION_MISSILE_MIXED_COUNT = "MISSION_MISSILE_MIXED_COUNT";
        private const string MISSION_D_KEY_GET = "MISSION_D_KEY_GET";
        private const string MISSION_AD_COUNT = "MISSION_AD_COUNT";
        
        private const string MISSION_BAZOOKA_GRADE_VALUE = "MISSION_BAZOOKA_GRADE_VALUE";
        private const string MISSION_PARTS_UPGRADE_COUNT = "MISSION_PARTS_UPGRADE_COUNT";
        
        private const string MISSION_RELIC_UPGRADE_COUNT = "MISSION_RELIC_UPGRADE_COUNT";
        private const string MISSION_D_RELIC_EVO_COUNT = "MISSION_D_RELIC_EVO_COUNT";
        
        private const string MISSION_MISSILE_UPGRADE_COUNT = "MISSION_MISSILE_UPGRADE_COUNT";
        
        private const string MISSION_NORMAL_KILL = "MISSION_NORMAL_KILL";
        private const string MISSION_GATE_KILL = "MISSION_GATE_KILL";
        private const string MISSION_ELITE_KILL = "MISSION_ELITE_KILL";
        private const string MISSION_BOSS_KILL = "MISSION_BOSS_KILL";
        private const string MISSION_DIABLO_KILL = "MISSION_DIABLO_KILL";
        private const string MISSION_GOLEM_KILL = "MISSION_GOLEM_KILL";
        
        private const string MISSION_PLUNDER_COUNT = "MISSION_PLUNDER_COUNT";   // 약탈
        
        
        #endregion
    }
}