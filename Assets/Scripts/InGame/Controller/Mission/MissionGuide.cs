﻿using System;
using System.Collections;
using BackEnd;
using Global;
using InGame.Data;
using InGame.Global;
using Server;

namespace InGame.Controller
{
    public partial class MissionController : MonoBehaviourSingleton<MissionController>
    {
        public static bool IsGuideAllClear
        {
            get
            {
                // 다음 미션 인덱스가 없으면, 모든 미션 클리어했다고 간주 
                UserData.My.Mission.GetGuideState(out var currentGuideIdx, out _);
                var nextIdx = GetNextIdx(currentGuideIdx);
                return string.IsNullOrEmpty(nextIdx);
            }
        }

        public static bool IsCurrentGuideFinished
        { 
            get
            {
                if (IsGuideAllClear) return true;
                
                UserData.My.Mission.GetGuideState(out var currentGuideIdx, out var currentGuideValue);

                return IsMissionFinished(Sheet.guide, currentGuideIdx, currentGuideValue);
            }
        }

        public static int GetCurrentGuideReward()
        {
            UserData.My.Mission.GetGuideState(out var currentGuideIdx, out _);
            return DataboxController.GetDataInt(Table.Mission, Sheet.guide, currentGuideIdx, Column.reward_gem);
        }

        public static void ClearCurrentGuide()
        {
            UserData.My.Mission.GetGuideState(out var currentGuideIdx, out _);
            
            var nextIdx = GetNextIdx(currentGuideIdx);
            
            // 리워드 획득 
            var reward = GetCurrentGuideReward();
            ResourceController.Instance.AddUranium(reward, $"{LogEvent.GuideMission}_{currentGuideIdx}");
            
            // 다음 가이드 미션으로 진행 
            UserData.My.Mission.ChangeGuide(nextIdx);
            
            // 저장 
            UserData.My.SaveToServer();
        }

        public static string GetCurrentGuideText()
        {
            UserData.My.Mission.GetGuideState(out var currentGuideIdx, out var currentGuideValue);
            var formatKey = DataboxController.GetDataString(Table.Mission, Sheet.guide, currentGuideIdx, Column.name);
            var value = DataboxController.GetDataInt(Table.Mission, Sheet.guide, currentGuideIdx, Column.m_value);
            var type = DataboxController.GetDataString(Table.Mission, Sheet.guide, currentGuideIdx, Column.m_type);
            if (type == MissionController.MISSION_STAGE_CLEAR)
            {
                var bestStage = UserData.My.Stage.bestStage;
                return Localization.GetFormatText(formatKey, bestStage.ToString(), value.ToString());
            }
            else
            {
                return Localization.GetFormatText(formatKey, currentGuideValue.ToString(), value.ToString());
            }
        }

        private static string GetNextIdx(string currentIdx)
        {
            return DataboxController.GetDataString(Table.Mission, Sheet.guide, currentIdx, Column.next_idx);
        }
    }
}