﻿using System;
using System.Collections;
using BackEnd;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Global;
using Server;

namespace InGame.Controller
{
    public partial class MissionController : MonoBehaviourSingleton<MissionController>
    {
        public static bool IsEarnedCommon(UserData.MissionData.CommonMissionType type, string missionIdx)
        {
            UserData.My.Mission.GetState(type, missionIdx, out _, out _, out var isEarned);
            return isEarned;
        }

        public static bool IsRepeatCommon(UserData.MissionData.CommonMissionType type, string missionIdx)
        {
            // HACK. CommonMissionType과 Sheet가 일치하다는 전제하에 쓰인 코드 
            return IsRepeat(type.ToEnumString(), missionIdx);
        }

        public static bool IsFinishedCommon(UserData.MissionData.CommonMissionType type, string missionIdx)
        {
            UserData.My.Mission.GetState(type, missionIdx, out var currentValue, out _, out _);

            // HACK. CommonMissionType과 Sheet가 일치하다는 전제하에 쓰인 코드
            return IsMissionFinished(type.ToEnumString(), missionIdx, currentValue);
        }

        public static int GetRewardGemCommon(UserData.MissionData.CommonMissionType type, string missionIdx)
        {
            // HACK. CommonMissionType과 Sheet가 일치하다는 전제하에 쓰인 코드
            return DataboxController.GetDataInt(Table.Mission, type.ToEnumString(), missionIdx, Column.reward_gem);
        }

        private static bool CanEarnCommon(UserData.MissionData.CommonMissionType type, string missionIdx)
        {
            // 미션 조건 미달성 시, 획득 불가
            if (!IsFinishedCommon(type, missionIdx))
                return false;

            // 반복 미션 아닌데 이미 획득했다면, 획득 불가
            if (!IsRepeatCommon(type, missionIdx) && IsEarnedCommon(type, missionIdx))
                return false;

            return true;
        }

        private static int GetMissionValue(UserData.MissionData.CommonMissionType type, string missionIdx)
        {
            // HACK. CommonMissionType과 Sheet가 일치하다는 전제하에 쓰인 코드
            return DataboxController.GetDataInt(Table.Mission, type.ToEnumString(), missionIdx, Column.m_value);
        }

        public static bool EarnCommonOnce(UserData.MissionData.CommonMissionType type, string missionIdx)
        {
            if (!CanEarnCommon(type, missionIdx))
                return false;

            // Repeat 시, 카운트 차감
            if (IsRepeatCommon(type, missionIdx))
            {
                // HACK. CommonMissionType과 Sheet가 일치하다는 전제하에 쓰인 코드
                var value = GetMissionValue(type, missionIdx);
                UserData.My.Mission.SubtractValue(type, missionIdx, value);
            }

            // 리워드 획득 
            var uranium = GetRewardGemCommon(type, missionIdx);
            ResourceController.Instance.AddUranium(uranium, $"{LogEvent.Mission}_{type.ToEnumString()}_{missionIdx}");

            // 획득 처리 
            UserData.My.Mission.Earn(type, missionIdx);
            
            // 바로 저장 
            UserData.My.SaveToServer();

            return true;
        }

        private static int EarnCommonBatch(UserData.MissionData.CommonMissionType type, string missionIdx)
        {
            if (!CanEarnCommon(type, missionIdx))
                return 0;

            var earnCount = 1;

            // Repeat 시, 카운트 차감
            if (IsRepeatCommon(type, missionIdx))
            {
                UserData.My.Mission.GetState(type, missionIdx, out var currentValue, out _, out _);
                
                var needValue = GetMissionValue(type, missionIdx);

                earnCount = currentValue / needValue;
                
                UserData.My.Mission.SubtractValue(type, missionIdx, needValue * earnCount);
            }

            // 리워드 획득 
            var uranium = GetRewardGemCommon(type, missionIdx) * earnCount;
            ResourceController.Instance.AddUraniumWithOutEffect(uranium, $"{LogEvent.Mission}_{type.ToEnumString()}_{missionIdx}_x{earnCount.ToLookUpString()}");

            // 획득 처리 
            UserData.My.Mission.Earn(type, missionIdx);

            return earnCount;
        }
    }
}