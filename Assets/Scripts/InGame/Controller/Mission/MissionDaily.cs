﻿using System;
using System.Collections;
using BackEnd;
using Global;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using Server;
using UnityEngine;

// ReSharper disable InconsistentNaming

namespace InGame.Controller
{
    public partial class MissionController : MonoBehaviourSingleton<MissionController>
    {
        public static void ResetDailyMission()
        {
            UserData.My.Mission.ResetDaily();
        }

        private static bool IsEarnedDaily(string dailyIdx)
        {
            UserData.My.Mission.GetState(UserData.MissionData.CommonMissionType.daily, dailyIdx, out var currentValue, out _, out var isEarned);

            return isEarned;
        }

        public static bool IsDailyFinished(string dailyIdx)
        {
            UserData.My.Mission.GetState(UserData.MissionData.CommonMissionType.daily, dailyIdx, out var currentValue, out _, out _);

            return IsMissionFinished(Sheet.daily, dailyIdx, currentValue);
        }

        private static bool CanEarnDaily(string dailyIdx)
        {
            return !IsEarnedDaily(dailyIdx) && IsDailyFinished(dailyIdx);
        }

        public static RewardData GetDailyReward(string dailyIdx)
        {
            var reward = new RewardData
            {
                Type = DataboxController.GetDataString(Table.Mission, Sheet.daily, dailyIdx, Column.reward_type),
                Count = DataboxController.GetDataInt(Table.Mission, Sheet.daily, dailyIdx, Column.reward_count)
            };
            return reward;
        }

        public static bool EarnDaily(string dailyIdx)
        {
            if (!CanEarnDaily(dailyIdx))
                return false;

            // 리워드 획득 
            var reward = GetDailyReward(dailyIdx);
            RewardTransactor.Transact(reward, $"{LogEvent.Mission}_{UserData.MissionData.CommonMissionType.daily.ToEnumString()}_{dailyIdx}");

            // 획득 처리 
            UserData.My.Mission.Earn(UserData.MissionData.CommonMissionType.daily, dailyIdx);

            // 데일리 미션을 클리어하는 미션
            Instance.AddValue(MISSION_DAILY_CLEAR_COUNT);

            return true;
        }
    }
}