﻿using BackEnd;
using InGame.Controller.Princess;
using InGame.Data;
using Server;
using UnityEngine;
using Utils;

namespace InGame.Controller
{
    public delegate void ControlHubDelegate(InGameControlHub hub);
    
    public class InGameControlHub : MonoBehaviourMultiton<InGameControlHub>
    {
        /*
         * 멀티톤 
         */
        public class Indexer
        {
            public InGameControlHub this[string inDate]
            {
                get
                {
                    // Test Framework 처리 
                    var instance = GetInstance(inDate);
                    instance.Init();
                    return instance;
                }
            }
        }
        public static readonly Indexer Other = new Indexer();
        
        public static InGameControlHub My => Other[ServerMyInfo.InDate];
        
        /*
         * inDate를 idx로 사용 -> Multiton에서 idx를 name으로 저장 
         */
        public string InDate
        {
            get
            {
                _inDate ??= name;
                return _inDate;
            }
        }

        private string _inDate;

        public UserData Data => UserData.Other[InDate];
        
        /*
         * 이벤트 
         */
        public static ControlHubDelegate OnAwake;

        /*
         * 컨트롤
         */
        public AbilityControl AbilityController { get; private set; }
        public CostumeControl CostumeController { get; private set; }
        public DiabloRelicControl DiabloRelicController { get; private set; }
        public NormalRelicControl NormalRelicController { get; private set; }
        public MissileControl MissileController { get; private set; }
        public PlunderControl PlunderController { get; private set; }
        public SoldierControl SoldierController { get; private set; }
        public StageControl StageController { get; private set; }
        public WorldMapControl WorldMapController { get; private set; }
        public InfStoneControl InfStoneController { get; private set; }
        public PrincessControl PrincessController { get; private set; }
        
        // 인게임 모드 별 스위칭되는 컨트롤러
        public SkillControl SkillController { get; set; }
        public TargetControl TargetController { get; set; }
        public BazookaControl BazookaController { get; set; }

        private bool _isInit;

        private void Awake()
        {
            Init();
        }

        private void Init()
        {
            if (_isInit) return;
            _isInit = true;
            
            AbilityController = gameObject.AddComponent<AbilityControl>();
            BazookaController = gameObject.AddComponent<BazookaControl>();
            CostumeController = gameObject.AddComponent<CostumeControl>();
            DiabloRelicController = gameObject.AddComponent<DiabloRelicControl>();
            NormalRelicController = gameObject.AddComponent<NormalRelicControl>();
            MissileController = gameObject.AddComponent<MissileControl>();
            PlunderController = gameObject.AddComponent<PlunderControl>();
            SoldierController = gameObject.AddComponent<SoldierControl>();
            StageController = gameObject.AddComponent<StageControl>();
            WorldMapController = gameObject.AddComponent<WorldMapControl>();
            InfStoneController = gameObject.AddComponent<InfStoneControl>();
            SkillController = gameObject.AddComponent<EnemyModeSkillControl>();
            TargetController = gameObject.AddComponent<EnemyModeTargetControl>();
            PrincessController = gameObject.AddComponent<PrincessControl>();

            // 컨트롤허브와 데이타는 분리돼있음
            // 허나, 둘 다 유저 InDate로 네이밍됨
            // 둘의 공통 부모를 만들고, 둘을 묶어 하이어라키에서 보기 편하게 함     
            var newParent = new GameObject
            {
                // 주의. 아래 옵션 지정 시, FindObjectOfType로 검색되지 않음 
                hideFlags = HideFlags.DontSave,
                name = InDate
            };
            
            // NOTICE. DontDestroyOnLoad를 하면, EnterPlayMode시 파괴가 되면서 
            // 자식으로 들어간 UserData도 파괴됨 
            // EnterPlayMode 자체가 인스턴스 재사용을 목적으로 하므로, 날아가지 않게 함 
            transform.SetParent(newParent.transform);
            Data.transform.SetParent(newParent.transform);
            
            // 이벤트 
            OnAwake?.Invoke(this);
        }

        public static InGameControlHub Find(Transform transform)
        {
            // 컨트롤허브 붙어있는지 검색 
            var ownerControlHub = transform.GetComponentInParent<InGameControlHub>();
            if (ownerControlHub != null)
                return ownerControlHub;
            
            // 인데이트 컴포넌트 붙어있는지 검색 
            var ownerInDate = transform.GetComponentInParent<OwnerUserInDate>();
            if (ownerInDate != null && !string.IsNullOrEmpty(ownerInDate.InDate))
                return GetInstance(ownerInDate.InDate);

            // InDate 컴포넌트 없거나, InDate가 유효하지 않을 시, 플레이어꺼 반환
            return GetInstance(ServerMyInfo.InDate);
        }

        public InGameControl ReplaceController<T>() where T : InGameControl
        {
            var newController = gameObject.GetComponent<T>();
            if (newController == null)
                newController = gameObject.AddComponent<T>();
            
            return newController;
        }
    }
}