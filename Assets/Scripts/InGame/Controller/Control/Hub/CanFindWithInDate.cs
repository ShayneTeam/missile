﻿using System;
using System.Collections.Generic;
using InGame.Data;
using UnityEngine;

namespace InGame.Controller.Control
{
    // 켜지자마자 본인이 속한 인데이트를 찾아, 해당 인데이트 자신을 찾을 수 있도록 등록시킴
    // 오브젝트 재활용 시, 인데이트가 바뀔 수 있어서 OnEnable/OnDisable에서 등록/해제 시켜야 함 
    public abstract class CanFindWithInDate<T> : FindControlHubBehaviour where T : Component
    {
        private static readonly Dictionary<string, T> _instances = new Dictionary<string, T>();

        public static T My => GetInstance(UserData.My.InDate);
        
        public class Indexer { public T this[string inDate] => GetInstance(inDate); }
        public static readonly Indexer Other = new Indexer();
	    
        private static T GetInstance(string inDate)
        {
            if (_instances.TryGetValue(inDate, out var instance)) 
                return instance;

            return null;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            
            // 등록
            _instances[FoundControlHub.InDate] = this as T;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            // 해제
            if (FoundControlHub == null) return;
            _instances.Remove(FoundControlHub.InDate);
        }
    }
}