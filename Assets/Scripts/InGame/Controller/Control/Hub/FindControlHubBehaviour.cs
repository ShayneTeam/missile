﻿using System;
using System.Collections.Generic;
using InGame.Data;
using UnityEngine;

namespace InGame.Controller.Control
{
    // 이걸 컴포넌트로 빼자니, 각자 하나씩 들고 있어야 해서, 배보다 배꼽이 터짐 
    // 사용원하는 곳마다 Abstract 패턴처럼 상속해서 사용한다 
    public abstract class FindControlHubBehaviour : MonoBehaviour
    {
        private InGameControlHub _foundControlHub;
        public InGameControlHub FoundControlHub
        {
            get
            {
                // parent 위에 달려있는 inDate 기반으로 InGameControlHub 찾아옴 
                if (_foundControlHub == null)
                    _foundControlHub = InGameControlHub.Find(transform);

                return _foundControlHub;
            }
        }

        // LeanPool 사용하는 클래스가 재활용 될 때, 부모 인데이트가 바뀔 수 있으니 null 세팅 
        protected virtual void OnEnable()
        {
            // 꺼질 떄 초기화가 아닌, 다시 켜질 때 초기화해서, 사용하는 타이밍에 다시 찾게 만듬 
            _foundControlHub = null;
        }
        
        protected virtual void OnDisable()
        {
            // 여기서 처리하면 밑에서 InDate로 뭔가를 하려 할 때, NRE가 떠버림 
            //_foundControlHub = null;
        }
    }
}