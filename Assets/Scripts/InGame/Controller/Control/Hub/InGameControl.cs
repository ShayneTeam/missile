﻿using UnityEngine;

namespace InGame.Controller
{
    public class InGameControl : MonoBehaviour
    {
        private InGameControlHub hub; 
        
        // 이 컨트롤을 소유하고 있는 InGameControllerHub 찾기 
        public InGameControlHub OwnerControlHub
        {
            get
            {
                if (hub == null)
                    hub = transform.GetComponent<InGameControlHub>();

                return hub;
            }
        }
    }
}