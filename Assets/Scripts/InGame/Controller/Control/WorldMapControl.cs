﻿using System.Collections.Generic;
using System.Globalization;
using BackEnd;
using Global;
using InGame.Data;
using InGame.Global;
using Server;

namespace InGame.Controller
{
    public abstract class CostActionWithoutIdx
    {
        public abstract bool IsEnoughCost();
        public abstract int GetNeedCost();
        public abstract bool CanDoIt();
        public abstract bool IsMax();
        public abstract void DoIt();
        
        protected readonly InGameControlHub Controllers;

        protected CostActionWithoutIdx(InGameControlHub controllers)
        {
            this.Controllers = controllers;
        }
    }

    public class WorldMapControl : InGameControl
    {
        public LevelUpGas GasFactoryLevelUp;
        public LevelUpStone StoneFactoryLevelUp;

        private void Awake()
        {
            GasFactoryLevelUp = new LevelUpGas(OwnerControlHub);
            StoneFactoryLevelUp = new LevelUpStone(OwnerControlHub);
        }

        public class LevelUpGas : CostActionWithoutIdx
        {
            public LevelUpGas(InGameControlHub controllers) : base(controllers)
            {
            }
            
            public override bool IsEnoughCost()
            {
                return Controllers.Data.Resources.uranium >= GetNeedCost();
            }

            public override int GetNeedCost()
            {
                Controllers.Data.WorldMap.GetState(out var gasLevel, out _);
                return DataboxController.GetDataInt(Table.WorldMap, Sheet.factory_gas, gasLevel.ToLookUpString(), Column.levelup_gem);
            }

            public override bool CanDoIt()
            {
                if (!IsEnoughCost())
                    return false;

                return true;
            }

            public override bool IsMax()
            {
                Controllers.Data.WorldMap.GetState(out var gasLevel, out _);

                var allLevels = DataboxController.GetAllIdxes(Table.WorldMap, Sheet.factory_gas);
                
                return gasLevel >= allLevels.Count;
            }

            public override void DoIt()
            {
                if (!CanDoIt())
                    return;
		        
                // 차감
                var needCost = GetNeedCost();
                ResourceController.Instance.SubtractUranium(needCost, LogEvent.LevelUpGasFactory);
		        
                // 레벨업
                Controllers.Data.WorldMap.LevelUpGasFactory();
            }
        }
        
        
        
        
        public class LevelUpStone : CostActionWithoutIdx
        {
            public LevelUpStone(InGameControlHub controllers) : base(controllers)
            {
            }
            
            public override bool IsEnoughCost()
            {
                return Controllers.Data.Resources.uranium >= GetNeedCost();
            }

            public override int GetNeedCost()
            {
                Controllers.Data.WorldMap.GetState(out _, out var stoneLevel);
                return DataboxController.GetDataInt(Table.WorldMap, Sheet.factory_stone, stoneLevel.ToLookUpString(), Column.levelup_gem);
            }

            public override bool CanDoIt()
            {
                if (!IsEnoughCost())
                    return false;

                return true;
            }

            public override bool IsMax()
            {
                Controllers.Data.WorldMap.GetState(out _, out var stoneLevel);
                
                var allLevels = DataboxController.GetAllIdxes(Table.WorldMap, Sheet.factory_stone);
                
                return stoneLevel >= allLevels.Count;
            }

            public override void DoIt()
            {
                if (!CanDoIt())
                    return;
		        
                // 차감
                var needCost = GetNeedCost();
                ResourceController.Instance.SubtractUranium(needCost, LogEvent.LevelUpStoneFactory);
		        
                // 레벨업
                Controllers.Data.WorldMap.LevelUpStoneFactory();
            }
        }
        


        private static float GetStoneTimeBonusPer(int stoneLevel)
        {
            return DataboxController.GetDataInt(Table.WorldMap, Sheet.factory_stone, stoneLevel.ToLookUpString(), Column.bonus_per);
        }

        private static float GetGasTimeBonusPer(int gasLevel)
        {
            return DataboxController.GetDataInt(Table.WorldMap, Sheet.factory_gas, gasLevel.ToLookUpString(), Column.bonus_per);
        }

        public Dictionary<string, double> GetMiningAmountsPerHour()
        {
            var timeMiningPerHour = new Dictionary<string, double>
            {
                {RewardType.REWARD_GAS, 0},
                {RewardType.REWARD_STONE, 0},
                {RewardType.REWARD_GEM, 0},
            };
            
            var planetsTimestamp = OwnerControlHub.Data.WorldMap.GetAllPlanetTimestamp();
            foreach (var planetTimestamp in planetsTimestamp)
            {
                var planetIdx = planetTimestamp.Key;

                // 리소스 1
                {
                    var res1Type = DataboxController.GetDataString(Table.WorldMap, Sheet.planet, planetIdx, Column.resource_1);
                    var res1Value = (double) DataboxController.GetDataInt(Table.WorldMap, Sheet.planet, planetIdx, Column.resource_1_value);

                    MergeMiningValue(ref timeMiningPerHour, res1Type, res1Value);
                }

                // 리소스 2
                {
                    var res2Type = DataboxController.GetDataString(Table.WorldMap, Sheet.planet, planetIdx, Column.resource_2);
                    var res2Value = (double) DataboxController.GetDataInt(Table.WorldMap, Sheet.planet, planetIdx, Column.resource_2_value);

                    MergeMiningValue(ref timeMiningPerHour, res2Type, res2Value);
                }
            }
            
            // 레벨업 보너스 적용 
            OwnerControlHub.Data.WorldMap.GetState(out var gasLevel, out var stoneLevel);
            ModifyLevelAndAbilitiesBonus(ref timeMiningPerHour, stoneLevel, gasLevel);

            return timeMiningPerHour;
        }

        public Dictionary<string, double> GetMiningSumUntilNow()
        {
            var planetsTimestamp = OwnerControlHub.Data.WorldMap.GetAllPlanetTimestamp();
            OwnerControlHub.Data.WorldMap.GetState(out var gasLevel, out var stoneLevel);

            // 현재까지의 채굴량 합산 가져오기
            var miningSumUntilNow = InternalGetMiningSumUntilNow(planetsTimestamp);
            
            // 레벨업 & 어빌리티 보너스 적용 
            ModifyLevelAndAbilitiesBonus(ref miningSumUntilNow, stoneLevel, gasLevel);

            return miningSumUntilNow;
        }
        
        private static Dictionary<string, double> InternalGetMiningSumUntilNow(Dictionary<string, long> planetsTimestamp)
        {
            var miningAmountsUntilNow = new Dictionary<string, double>
            {
                {RewardType.REWARD_GAS, 0},
                {RewardType.REWARD_STONE, 0},
                {RewardType.REWARD_GEM, 0},
            };
            
            /*
             * 각 행성마다 획득한 타임스탬프 기준, 현재까지의 채굴량 계산 
             */
            
            foreach (var planetTimestamp in planetsTimestamp)
            {
                var planetIdx = planetTimestamp.Key;
                var timestamp = planetTimestamp.Value;

                var elapsedTimeSec = ServerTime.Instance.NowSecUnscaled - timestamp;
                
                // 리소스 1
                {
                    var res1Type = DataboxController.GetDataString(Table.WorldMap, Sheet.planet, planetIdx, Column.resource_1);
                    var res1Value = (double) DataboxController.GetDataInt(Table.WorldMap, Sheet.planet, planetIdx, Column.resource_1_value);
                    
                    // 초당 채굴량 환산 
                    var miningPerSec = res1Value / 3600d;
                    
                    // 현재까지의 채굴량 계산
                    var miningAmountUntilNow = elapsedTimeSec * miningPerSec;
                    
                    // 행성 당 최대 채굴량 제한
                    var limitedValue = LimitValueByPlanet(miningAmountUntilNow, planetIdx, res1Type);
                    
                    MergeMiningValue(ref miningAmountsUntilNow, res1Type, limitedValue);
                }

                // 리소스 2
                {
                    var res2Type = DataboxController.GetDataString(Table.WorldMap, Sheet.planet, planetIdx, Column.resource_2);
                    var res2Value = (double) DataboxController.GetDataInt(Table.WorldMap, Sheet.planet, planetIdx, Column.resource_2_value);
                    
                    // 초당 채굴량 환산 
                    var miningPerSec = res2Value / 3600d;
                    
                    // 현재까지의 채굴량 계산
                    var miningAmountUntilNow = elapsedTimeSec * miningPerSec;
                
                    // 행성 당 최대 채굴량 제한
                    var limitedValue = LimitValueByPlanet(miningAmountUntilNow, planetIdx, res2Type);

                    MergeMiningValue(ref miningAmountsUntilNow, res2Type, limitedValue);
                }
            } 
            
            return miningAmountsUntilNow;
        }

        private static double LimitValueByPlanet(double value, string planetIdx, string rewardType)
        {
            var limitedValue = value;
            switch (rewardType)
            {
                case RewardType.REWARD_GAS:
                {
                    var max = DataboxController.GetDataInt(Table.WorldMap, Sheet.planet, planetIdx, Column.gas_max);
                    limitedValue = limitedValue > max ? max : limitedValue;
                }
                    break;
                case RewardType.REWARD_STONE:
                {
                    var max = DataboxController.GetDataInt(Table.WorldMap, Sheet.planet, planetIdx, Column.stone_max);
                    limitedValue = limitedValue > max ? max : limitedValue;
                }
                    break;
                case RewardType.REWARD_GEM:
                {
                    var max = DataboxController.GetDataInt(Table.WorldMap, Sheet.planet, planetIdx, Column.gem_max);
                    limitedValue = limitedValue > max ? max : limitedValue;
                }
                    break;
            }

            return limitedValue;
        }

        private static void MergeMiningValue(ref Dictionary<string, double> refDic, string type, double value)
        {
            if (refDic.ContainsKey(type))
                refDic[type] += value;
            else
                refDic[type] = value;
        }

        private void ModifyLevelAndAbilitiesBonus(ref Dictionary<string, double> refDic, int stoneLevel, int gasLevel)
        {
            var abAllPlanetPer = OwnerControlHub.AbilityController.GetUnifiedValue("GAIN_ALL_PLANET");
            var abInfStoneSpacePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.WORLDMAP_HARVEST);
            
            var gasBonusPer = GetGasTimeBonusPer(gasLevel);
            if (refDic.ContainsKey("REWARD_GAS"))
            {
                var abGasPlanetPer = OwnerControlHub.AbilityController.GetUnifiedValue("GAIN_GAS_PLANET");
                
                refDic["REWARD_GAS"] *= 1f + (abAllPlanetPer + gasBonusPer + abGasPlanetPer + abInfStoneSpacePer) / 100f;
            }

            var stoneBonusPer = GetStoneTimeBonusPer(stoneLevel);
            if (refDic.ContainsKey("REWARD_STONE"))
            {
                var abStonePlanetPer = OwnerControlHub.AbilityController.GetUnifiedValue("GAIN_STONE_PLANET");
                
                refDic["REWARD_STONE"] *= 1f + (abAllPlanetPer + stoneBonusPer + abStonePlanetPer + abInfStoneSpacePer) / 100f;
            }
        }

        public void Mining()
        {
            // 현재까지의 채굴량   
            var untilNow = GetMiningSumUntilNow();
            
            // 약탈 반영 
            untilNow[RewardType.REWARD_GAS] -= OwnerControlHub.Data.Plunder.PlunderedGasSum;
            
            // 약탈 반영 후, 현재까지 당한 약탈 양 초기화 
            OwnerControlHub.Data.Plunder.PlunderedGasSum = 0d;

            // 리워드트랜잭터에게 보내도록 리스트화
            var rewards = new List<RewardData>();
            foreach (var key in untilNow.Keys)
            {
                var newReward = new RewardData
                {
                    Type = key,
                    Idx = "",
                    Count = untilNow[key],
                };
                rewards.Add(newReward);
            }
            
            // 트랜잭터에게 보내서 획득 처리 
            RewardTransactor.Transact(rewards, LogEvent.WorldMap);

            // 획득 행성들의 타임스탬프 찍기 
            var planetsTimestamp = OwnerControlHub.Data.WorldMap.GetAllPlanetTimestamp();
            foreach (var planetTimestamp in planetsTimestamp)
            {
                var planetIdx = planetTimestamp.Key;

                OwnerControlHub.Data.WorldMap.StampTime(planetIdx);
            }
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }

        public double GetGasMiningSumUntilNow()
        {
            var miningAmountsUntilNow = OwnerControlHub.WorldMapController.GetMiningSumUntilNow();
            return miningAmountsUntilNow["REWARD_GAS"];
        }
        
        public bool IsUnlocked()
        {
            // 특정 스테이지 클리어 전까지 불가능
            return OwnerControlHub.StageController.IsOwned(NecessaryPlanetIdx);
        }
        
        private static string NecessaryPlanetIdx => DataboxController.GetConstraintsData("WORLD_MAP_NECESSARY_PLANET");
    }
}