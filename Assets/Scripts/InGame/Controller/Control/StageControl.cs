﻿using System;
using System.Collections.Generic;
using System.Diagnostics.CodeAnalysis;
using System.Linq;
using Firebase.Analytics;
using Global;
using InGame.Global;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame.Controller
{
    [SuppressMessage("ReSharper", "InconsistentNaming")]
    public enum ScriptType
    {
        first,
        normal_elite,
        boss,
        diablo
    }
        
    public static class ScriptTypeExtensions
    {
        public static string ToEnumString(this ScriptType type)
        {
            return type switch
            {
                ScriptType.first => nameof(ScriptType.first),
                ScriptType.normal_elite => nameof(ScriptType.normal_elite),
                ScriptType.boss => nameof(ScriptType.boss),
                ScriptType.diablo => nameof(ScriptType.diablo),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }
    
    /*
     * 인게임로직 - 데이터 사이의 상호동작 관리 
     */
    public class StageControl : InGameControl
    {
        private (int isolatedStage, int isolatedCount) isolatedInfo;
        private static readonly List<string> _scriptIdxesByType = new List<string>();
        private static readonly List<string> _allScriptIdx = new List<string>();

        #region Get 
                    
        public void GetCurrentStageAndPlanetData(out int outStage, out string outPlanetIdx)
        {
            outStage = OwnerControlHub.Data.Stage.currentStage;

            // 어떤 행성에 있는지 찾기 (현재 데이터에 저장된 스테이지 기준으로)
            GetPlanetIdxByStage(OwnerControlHub.Data.Stage.currentStage, out outPlanetIdx);
        }

        public int GetCurrentPlanetIndex()
        {
            return GetPlanetIndex(OwnerControlHub.Data.Stage.currentStage);
        }
        
        public int GetBestPlanetIndex()
        {
            return GetPlanetIndex(OwnerControlHub.Data.Stage.bestStage);
        }

        public static int GetPlanetIndex(int stage)
        {
            GetPlanetIdxByStage(stage, out var currentPlanetIdx);
            
            return GetPlanetIndex(currentPlanetIdx);
        }

        public static int GetPlanetIndex(string planetIdx)
        {
            return DataboxController.GetDataInt(Table.Stage, Sheet.planet, planetIdx, Column.planet);
        }

        public bool IsAvailable(string planetIdx)
        {
            // 최고 스테이지 이하의 행성들은 이동할 수 있음 
            var planetIndex = StageControl.GetPlanetIndex(planetIdx);
            var bestPlanetIndex = InGameControlHub.My.StageController.GetBestPlanetIndex();
            return planetIndex <= bestPlanetIndex;
        }

        public static void GetPlanetIdxByStage(int targetStage, out string outPlanetIdx)
        {
            // 어떤 행성에 있는지 찾기 (현재 데이터에 저장된 스테이지 기준으로)
            const string defaultPlanetIdx = "30001";
            outPlanetIdx = defaultPlanetIdx;

            // 행성 시트 검색
            var allPlanetIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.planet);
            foreach (var planetIdx in allPlanetIdxes)
            {
                var stageMin = DataboxController.GetDataInt(Table.Stage, Sheet.planet, planetIdx, "stage_min", 1);
                var stageMax = DataboxController.GetDataInt(Table.Stage, Sheet.planet, planetIdx, "stage_max", 1);

                // 스테이지를 포함하는 행성 찾기  
                if (stageMin <= targetStage && targetStage <= stageMax)
                {
                    outPlanetIdx = planetIdx;
                    break;
                }
            }
        }

        public int GetStageCountByCurrentPlanet()
        {
            GetCurrentStageAndPlanetData(out var stage, out var planetIdx);
            GetStageMinMaxData(planetIdx, out var stageMin, out _);

            return stage - stageMin;
        }

        public int GetFirstStage(string planetIdx)
        {
            return DataboxController.GetDataInt(Table.Stage, Sheet.planet, planetIdx, "stage_min", 1);
        }

        public int GetFinalizedFirstStage(string planetIdx)
        {
            var firstStage = GetFirstStage(planetIdx);
            
            // 어빌리티 적용
            var abilityStageCount = (int)OwnerControlHub.AbilityController.GetUnifiedValue("BOSS_KILL_AFTER_STAGE_JUMP");
            
            return firstStage + abilityStageCount;
        }

        public double ModifyPlanetWeightByCurrentStage(double baseValue, string weightColumn)
        {
            // 현재 스테이지의 행성 기준으로 계산 
            GetCurrentStageAndPlanetData(out _, out var planetIdx);
		
            return InternalModifyPlanetWeight(baseValue, planetIdx, weightColumn);
        }
        
        public static double ModifyPlanetWeight(double baseValue, int stage, string weightColumn)
        {
            GetPlanetIdxByStage(stage, out var planetIdx);
		
            return InternalModifyPlanetWeight(baseValue, planetIdx, weightColumn);
        }

        public double ModifyPlanetAndStageWeightByCurrentStage(double baseValue, string planetWeightColumn, string stageWeightColumn)
        {
            var currentStage = OwnerControlHub.Data.Stage.currentStage;

            return ModifyPlanetAndStageWeight(baseValue, currentStage, planetWeightColumn, stageWeightColumn);
        }

        public double ModifyPlanetAndStageWeightByBestStage(double baseValue, string planetWeightColumn, string stageWeightColumn)
        {
            var bestStage = OwnerControlHub.Data.Stage.bestStage;

            return ModifyPlanetAndStageWeight(baseValue, bestStage, planetWeightColumn, stageWeightColumn);
        }
        
        public static double ModifyPlanetAndStageWeight(double baseValue, int stage, string planetWeightColumn, string stageWeightColumn)
        {
            // 최고 스테이지의 행성 기준으로 계산
            GetPlanetIdxByStage(stage, out var planetIdx);
            GetStageMinMaxData(planetIdx, out var stageMin, out _);
            
            // 행성 가중치
            var planetWeightedValue = InternalModifyPlanetWeight(baseValue, planetIdx, planetWeightColumn);
            
            // 스테이지 가중치 
            var stageCountInPlanet = stage - stageMin;
            var stageWeightedValue = InternalModifyStageWeight(planetWeightedValue, planetIdx, stageCountInPlanet, stageWeightColumn);
            return stageWeightedValue;
        }

        private static double InternalModifyPlanetWeight(double baseValue, string planetIdx, string weightColumn)
        {
            // 행성 가중치
            return WeightController.ModifyWeight(baseValue, Table.Stage, Sheet.planet, planetIdx, weightColumn);
        }

        private static double InternalModifyStageWeight(double baseValue, string planetIdx, int stageCountInPlanet, string weightColumn)
        {
            // 스테이지 가중치 
            var targetWeight = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, weightColumn);
            
            return WeightController.ModifyWeight(baseValue, Table.Stage, Sheet.weight, stageCountInPlanet.ToLookUpString(), targetWeight);
        }
				
        public static string GetScript(string planetIdx, ScriptType scriptType)
        {
            var groups = DataboxController.GetDataStringList(Table.Stage, Sheet.planet, planetIdx, Column.CharacterScript);

            // 리스트 합침
            _allScriptIdx.Clear();
            foreach (var group in groups)
            {
                _allScriptIdx.AddRange(DataboxController.GetAllIdxesByGroup(Table.Stage, Sheet.Script, Column.Group, group));
            }

            // 리스트에서 해당 타겟 골라내기 
            _scriptIdxesByType.Clear();
            foreach (var scriptIdx in _allScriptIdx)
            {
                var type = DataboxController.GetDataString(Table.Stage, Sheet.Script, scriptIdx, Column.Type);
                if (type == scriptType.ToEnumString())
                    _scriptIdxesByType.Add(scriptIdx);
            }

            // 랜덤으로 하나 뽑기 
            if (_scriptIdxesByType.Count == 0) return "";

            var randomScriptIdx = _scriptIdxesByType[Random.Range(0, _scriptIdxesByType.Count)];
            return DataboxController.GetDataString(Table.Stage, Sheet.Script, randomScriptIdx, Column.ScriptId);
        }
        
        #endregion
        
        
        #region Check 
        
        public static bool IsValidStage(int stageToCheck)
        {
            // 모든 행성 인덱스 가져오기 
            var allPlanetIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.planet);

            // 첫 번째 행성 인덱스의 stage_min 
            var firstPlanetIdx = allPlanetIdxes.ElementAt(0);
            var stageMin = DataboxController.GetDataInt(Table.Stage, Sheet.planet, firstPlanetIdx, "stage_min", 1);

            // 마지막 행성 인덱스의 stage_max
            var lastPlanetIdx = allPlanetIdxes.ElementAt(allPlanetIdxes.Count - 1);
            var stageMax = DataboxController.GetDataInt(Table.Stage, Sheet.planet, lastPlanetIdx, "stage_max", 1);

            // 범위 체크 결과 반환
            return stageMin <= stageToCheck && stageToCheck <= stageMax;
        }

        public bool IsLastStage(int stageToCheck, string planetIdx)
        {
            GetStageMinMaxData(planetIdx, out _, out var stageMax);

            // 현 스테이지가 마지막 스테이지인지 체크
            return stageToCheck == stageMax;
        }
        
        public static bool IsLastPlanet(int planetIndex)
        {
            var allPlanetIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.planet);

            return planetIndex >= allPlanetIdxes.Count;
        }
        
        public bool IsBestPlanet(string planetIdx)
        {
            // 현재 나의 도달 최고 스테이지에 해당하는 행성 인덱스 찾기 
            GetPlanetIdxByStage(OwnerControlHub.Data.Stage.bestStage, out var bestPlanetIdx);

            return planetIdx == bestPlanetIdx;
        }

        public bool IsOwned(string planetIdx)
        {
            return OwnerControlHub.Data.Stage.IsOwned(planetIdx);
        }
        
        #endregion

        
        #region Control
        
        public void AddStageData()
        {
            GetCurrentStageAndPlanetData(out _, out var planetIdx);

            GetStageMinMaxData(planetIdx, out var stageMin, out var stageMax);

            OwnerControlHub.Data.Stage.currentStage = Mathf.Clamp(OwnerControlHub.Data.Stage.currentStage + 1, stageMin, stageMax);
        }

        public void SubtractStageData()
        {
            GetCurrentStageAndPlanetData(out _, out var planetIdx);

            GetStageMinMaxData(planetIdx, out var stageMin, out var stageMax);

            if (isolatedInfo.isolatedStage == OwnerControlHub.Data.Stage.currentStage)
            {
                isolatedInfo.isolatedCount += 1;

                if (isolatedInfo.isolatedCount == 10)
                {
                    WebLog.Instance.AllLog("isolated_stage_data", new Dictionary<string, object>{{"stage",isolatedInfo.isolatedStage}});
                }
            }
            else // init
            {
                isolatedInfo = (OwnerControlHub.Data.Stage.currentStage, 1);
            }

            WebLog.Instance.ThirdPartyLog("defeat_stage_data", new Dictionary<string, object>{{"current_stage",OwnerControlHub.Data.Stage.currentStage.ToLookUpString()}});

            OwnerControlHub.Data.Stage.currentStage = Mathf.Clamp(OwnerControlHub.Data.Stage.currentStage - 1, stageMin, stageMax);
        }

        public static void GetStageMinMaxData(string planetIdx, out int outStageMin, out int outStageMax)
        {
            outStageMin = DataboxController.GetDataInt(Table.Stage, Sheet.planet, planetIdx, "stage_min", 1);
            outStageMax = DataboxController.GetDataInt(Table.Stage, Sheet.planet, planetIdx, "stage_max", 1);
        }

        public void SetStageToPlanetFirstStage(string targetPlanetIdx)
        {
            var firstStageOfTargetPlanet = DataboxController.GetDataInt(Table.Stage, Sheet.planet, targetPlanetIdx, "stage_min", 1);
            OwnerControlHub.Data.Stage.currentStage = firstStageOfTargetPlanet;
        }

        public void SetStage(int targetStage, out string outPlanetIdx)
        {
            OwnerControlHub.Data.Stage.currentStage = targetStage;

            GetPlanetIdxByStage(targetStage, out outPlanetIdx);
        }

        public bool MoveToNextPlanet(out string outPrevPlanetIdx, out string outNextPlanetIdx)
        {
            // 어떤 행성에 있는지 찾기 (현재 데이터에 저장된 스테이지 기준으로)
            var planetListIndex = 0;

            var allPlanetIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.planet);

            // 행성 시트 검색
            for (var i = 0; i < allPlanetIdxes.Count; i++)
            {
                var planetIdx = allPlanetIdxes.ElementAt(i);

                var stageMin = DataboxController.GetDataInt(Table.Stage, Sheet.planet, planetIdx, "stage_min", 1);
                var stageMax = DataboxController.GetDataInt(Table.Stage, Sheet.planet, planetIdx, "stage_max", 1);

                // 현재 스테이지를 포함하는 행성 찾기  
                if (stageMin <= OwnerControlHub.Data.Stage.currentStage && OwnerControlHub.Data.Stage.currentStage <= stageMax)
                {
                    planetListIndex = i;

                    break;
                }
            }

            outPrevPlanetIdx = allPlanetIdxes.ElementAt(planetListIndex);

            // 찾은 행성 리스트 인덱스를 증가 
            var clampedPlanetIndex = Mathf.Clamp(planetListIndex + 1, 0, allPlanetIdxes.Count - 1);

            // 행성 인덱스가 변경완료됐다면, 성공
            var successToMove = planetListIndex != clampedPlanetIndex;

            var nextPlanetIdx = allPlanetIdxes.ElementAt(clampedPlanetIndex);
            outNextPlanetIdx = nextPlanetIdx;

            // 이동된 스테이지 첫 번째 스테이지로 플레이   
            SetStageToPlanetFirstStage(nextPlanetIdx);

            //
            return successToMove;
        }

        public void EarnPlanet(string planetIdx)
        {
            // 행성 획득 
            OwnerControlHub.Data.Stage.Earn(planetIdx);
            
            // 월드맵 타임스탬프
            OwnerControlHub.Data.WorldMap.StampTime(planetIdx);
        }
        
        #endregion

        
        #region Reward

        

        public void RandomPickRewardBox(string rewardGroup, out string outPickedIdx)
        {
            // 현재 행성 인덱스
            var currentPlanetIndex = GetCurrentPlanetIndex();
            
            // 스테이지 리워드 시트 가져오기  
            var allRewards = DataboxController.GetAllIdxes(Table.Stage, Sheet.reward);

            // 박스들 주사위 리스트에 넣기   
            var rollList = new List<CommonRoll>();

            foreach (var rewardIdx in allRewards)
            {
                // 같은 리워드그룹이어야 함
                var groupOfReward = DataboxController.GetDataString(Table.Stage, Sheet.reward, rewardIdx, Column.RewardGroup);
                if (groupOfReward != rewardGroup) 
                    continue;
                
                // 같은 행성 이어야 함
                var planet = DataboxController.GetDataInt(Table.Stage, Sheet.reward, rewardIdx, Column.planet);
                if (planet != currentPlanetIndex) 
                    continue;
                
                var rewardChance = DataboxController.GetDataInt(Table.Stage, Sheet.reward, rewardIdx, Column.chance, 100);
                GlobalFunction.AddRoll(ref rollList, rewardIdx, rewardChance);
            }
            
            // 주사위 돌리기 
            var roll = GlobalFunction.Roll(ref rollList);
						
            outPickedIdx = roll.Name;
        }

        #endregion
    }
}