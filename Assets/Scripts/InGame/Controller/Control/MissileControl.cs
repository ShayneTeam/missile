﻿using System;
using System.Collections.Generic;
using System.Linq;
using Databox.Dictionary;
using Firebase.Analytics;
using Global;
using InGame.Data;
using InGame.Global;
using Server;
using UI.Popup;
using UI.Popup.Menu;
using Random = UnityEngine.Random;

namespace InGame.Controller
{
    /*
     * 인게임로직과 데이터 간의 상호동작 관리 
     */
    public class MissileControl : InGameControl
    {
        private const int AbilityMax = 3;

        public event VoidDelegate OnChangedAbilities;
        public static event IntDelegate OnSynthesize;
        public static event VoidDelegate OnSell;

        private void Start()
        {
            OwnerControlHub.Data.Missiles.OnEnhance += OnChangedData;
            OwnerControlHub.Data.Missiles.OnEarned += OnChangedData;
            OwnerControlHub.Data.Missiles.OnLevelUp += OnChangedData;
            OwnerControlHub.Data.Missiles.OnEquip += OnChangedData;
        }

        private void OnDestroy()
        {
            OwnerControlHub.Data.Missiles.OnEnhance -= OnChangedData;
            OwnerControlHub.Data.Missiles.OnEarned -= OnChangedData;
            OwnerControlHub.Data.Missiles.OnLevelUp -= OnChangedData;
            OwnerControlHub.Data.Missiles.OnEquip -= OnChangedData;
        }

        private void OnChangedData(string missileIdx)
        {
            OnChangedAbilities?.Invoke();
        }


        public void Earn(string missileIdx, int amount)
        {
            // 획득 처리 
            OwnerControlHub.Data.Missiles.Earn(missileIdx, amount);

            // 자동 장착 처리 
            {
                OwnerControlHub.Data.Missiles.GetState(missileIdx, out _, out _, out _, out var isEquipped);
                
                // 이미 장착중이라면 아래 처리 생략
                if (isEquipped) return;
                
                // 고대 등급이면 강제 장착
                var rarity = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.rarity);
                if (rarity == MissileRarity.ancient.ToEnumString())
                {
                    OwnerControlHub.Data.Missiles.Equip(missileIdx, true);
                    // 바로 종료
                    return;
                }
                
                // 최대 장착 갯수 체크 (고대 제외)
                var equipMax = int.Parse(DataboxController.GetConstraintsData("MISSILE_EQUIP_MAX", "6"));
                var allEquippedMissiles = AllIdxesEquippedWithoutAncient;
                if (allEquippedMissiles.Count() < equipMax)
                {
                    OwnerControlHub.Data.Missiles.Equip(missileIdx, true);
                }
            }
        }

        public void Equip(string equipMissileIdx, string disrobeMissileIdx)
        {
            OwnerControlHub.Data.Missiles.Equip(equipMissileIdx, true);
            OwnerControlHub.Data.Missiles.Equip(disrobeMissileIdx, false);
        }

        public OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
            // 인덱스 기준으로 정렬
            var sortedByIdx = new SortedList<string, float>(); // idx, value

            // 보유 미사일
            var allIdxesEarned = OwnerControlHub.Data.Missiles.GetAllIdxesEarned();
            foreach (var missileIdx in allIdxesEarned)
            {
                OwnerControlHub.Data.Missiles.GetState(missileIdx, out _, out _, out var level, out _);

                for (var i = 1; i <= AbilityMax; i++)
                {
                    // 보유 효과
                    var indexStr = i.ToLookUpString();
                    GetAbilityState(missileIdx, $"b_aidx_{indexStr}", $"b_aidx_{indexStr}_gain", level, out var abilityIdx, out var finalizedValue);

                    if (sortedByIdx.ContainsKey(abilityIdx))
                        sortedByIdx[abilityIdx] += finalizedValue;
                    else
                        sortedByIdx[abilityIdx] = finalizedValue;
                }
            }

            // 장착 미사일  
            var allIdxesEquipped = OwnerControlHub.Data.Missiles.GetAllIdxesEquipped();
            foreach (var missileIdx in allIdxesEquipped)
            {
                OwnerControlHub.Data.Missiles.GetState(missileIdx, out _, out _, out var level, out _);

                for (var i = 1; i <= AbilityMax; i++)
                {
                    // 장착 효과
                    var indexStr = i.ToLookUpString();
                    GetAbilityState(missileIdx, $"a_aidx_{indexStr}", $"a_aidx_{indexStr}_gain", level, out var abilityIdx, out var finalizedValue);

                    if (sortedByIdx.ContainsKey(abilityIdx))
                        sortedByIdx[abilityIdx] += finalizedValue;
                    else
                        sortedByIdx[abilityIdx] = finalizedValue;
                }
            }

            // 동일한 어빌리티 타입을 가지는 인덱스들의 밸류값 총합 계산 
            var mergedValuesByType = new OrderedDictionary<string, float>(); // type, value 
            AbilityControl.MergeByType(ref mergedValuesByType, sortedByIdx);

            return mergedValuesByType;
        }

        public static void GetAbilityState(string missileIdx, string abilityIdxColumn, string weightColumnName, int level, out string outAbilityIdx, out float outFinalizedValue)
        {
            var abilityIdx = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, abilityIdxColumn);

            // 어빌리티 기본 밸류  
            var abilityValue = AbilityControl.GetValue(abilityIdx);

            // 가중치 
            var weightColumn = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, weightColumnName);
            var weight = DataboxController.GetDataFloat(Table.Missile, Sheet.gain, level.ToLookUpString(), weightColumn, 1f);

            // 최종 어빌리티 값 
            outAbilityIdx = abilityIdx;
            outFinalizedValue = abilityValue * weight;
        }


        #region Upgrade

        public bool IsMaxLevel(string missileIdx)
        {
            OwnerControlHub.Data.Missiles.GetState(missileIdx, out _, out var grade, out var level, out _);

            var defaultMaxLevel = DataboxController.GetDataInt(Table.Missile, Sheet.missile, missileIdx, "max_level");
            var addMaxLevelForEachGrade = DataboxController.GetDataInt(Table.Missile, Sheet.missile, missileIdx, "upgrade_max_level");

            var finalizedMaxLevel = defaultMaxLevel + (addMaxLevelForEachGrade * grade);

            return level >= finalizedMaxLevel;
        }

        public double GetRequiredGasForUpgrade(string missileIdx)
        {
            OwnerControlHub.Data.Missiles.GetState(missileIdx, out _, out _, out var level, out _);

            var defaultGas = DataboxController.GetDataInt(Table.Missile, Sheet.missile, missileIdx, "enchant_gas");

            var weightColumn = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, "enchant_gas_gain");
            var weight = DataboxController.GetDataFloat(Table.Missile, Sheet.gain, level.ToLookUpString(), weightColumn);
            
            var needGas = defaultGas * weight;
            
            var rarity = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.rarity);

            var abAllCostDownModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_MISSILE_ALL");
            float abCostDownModifiedPer = 0;
            switch (rarity)
            {
                case nameof(MissileRarity.normal):
                    abCostDownModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_MISSILE_NORMAL");
                    break;
                case nameof(MissileRarity.magic):
                    abCostDownModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_MISSILE_MAGIC");
                    break;
                case nameof(MissileRarity.rare):
                    abCostDownModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_MISSILE_RARE");
                    break;
                case nameof(MissileRarity.hero):
                    abCostDownModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_MISSILE_HERO");
                    break;
                case nameof(MissileRarity.legend):
                    abCostDownModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_MISSILE_LEGEND");
                    break;
                case nameof(MissileRarity.myth):
                    abCostDownModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_MISSILE_MYTH");
                    break;
            }

            return needGas / (1f + (abAllCostDownModifiedPer + abCostDownModifiedPer) / 100f);
        }

        public bool CanUpgrade(string missileIdx)
        {
            if (!OwnerControlHub.Data.Missiles.IsEarned(missileIdx))
                return false;

            if (IsMaxLevel(missileIdx))
                return false;

            var needGas = GetRequiredGasForUpgrade(missileIdx);
            var ownedGas = OwnerControlHub.Data.Resources.gas;

            return ownedGas >= needGas;
        }

        public void Upgrade(string missileIdx)
        {
            if (!CanUpgrade(missileIdx))
                return;

            // 업그레이드 전에 필요 가스 차감
            var needGas = GetRequiredGasForUpgrade(missileIdx);
            ResourceController.Instance.SubtractGas(needGas);

            // 업그레이드
            OwnerControlHub.Data.Missiles.LevelUp(missileIdx, 1);
            
            var isEarned = OwnerControlHub.Data.Missiles.GetState(missileIdx, out _, out var _, out var level, out var outIsEquipped);
            if (isEarned && outIsEquipped)
            {
                WebLog.Instance.ThirdPartyLog("equipment_upgrade", new Dictionary<string, object>
                {
                    {"type","missile_upgrade"},
                    {"missile_idx", missileIdx}, 
                    {"missile_level", level.ToLookUpString()},
                });
            }
        }

        #endregion


        #region Enhance

        public bool IsMaxGrade(string missileIdx)
        {
            OwnerControlHub.Data.Missiles.GetState(missileIdx, out _, out var grade, out _, out _);

            return grade >= int.Parse(DataboxController.GetConstraintsData("MISSILE_GRADE_MAX", "3"));
        }

        public bool CanEnhance(string missileIdx)
        {
            if (!OwnerControlHub.Data.Missiles.IsEarned(missileIdx)) return false;

            if (IsMaxGrade(missileIdx)) return false;
            
            var ownedChipsetCount = OwnerControlHub.Data.Items[RewardType.REWARD_CHIPSET];
            var needChipsetCount = GetNeedChipsetCountForEnhance(missileIdx);

            return ownedChipsetCount >= needChipsetCount;
        }

        public static int GetNeedChipsetCountForEnhance(string missileIdx)
        {
            return DataboxController.GetDataInt(Table.Missile, Sheet.missile, missileIdx, "upgrade_cost");
        }

        public void Enhance(string missileIdx)
        {
            if (!CanEnhance(missileIdx))
                return;

            // 강화 전에 필요 칩셋 차감
            var needChipset = GetNeedChipsetCountForEnhance(missileIdx);
            OwnerControlHub.Data.Items[RewardType.REWARD_CHIPSET] -= needChipset;

            // 강화 
            OwnerControlHub.Data.Missiles.Enhance(missileIdx, 1);
            
            // 로그
            OwnerControlHub.Data.Missiles.GetState(missileIdx, out _, out var grade, out _, out _);
            ServerLogger.LogItem(RewardType.REWARD_CHIPSET, $"{LogEvent.MissileEnhance}_{grade.ToLookUpString()}", -needChipset);
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }

        #endregion


        #region Synthesis

        public static int GetNeedMissileCountForSynthesis()
        {
            var synthesisCountStr = DataboxController.GetConstraintsData("MISSILE_SYNTHESIS_COUNT", "5");
            return int.Parse(synthesisCountStr);
        }

        public static bool IsLastMissileIdx(string missileIdx)
        {
            var allIdxes = DataboxController.GetAllIdxes(Table.Missile, Sheet.missile);
            var lastMissileIdx = allIdxes.ElementAt(allIdxes.Count - 1);
            return lastMissileIdx == missileIdx;
        }

        public bool CanSynthesize(string missileIdx)
        {
            if (!OwnerControlHub.Data.Missiles.IsEarned(missileIdx))
                return false;

            // 가장 마지막 미사일이라면 불가
            if (IsLastMissileIdx(missileIdx))
                return false;

            // 합성할 수 있는 갯수 미달 시 불가능  
            OwnerControlHub.Data.Missiles.GetState(missileIdx, out var ownedCount, out _, out _, out _);

            var needCount = GetNeedMissileCountForSynthesis();

            return ownedCount >= needCount;
        }

        public bool Synthesize(string missileIdx, out string outResultMissileIdx, int amount = 1)
        {
            if (!InternalSynthesize(missileIdx, amount, out outResultMissileIdx)) return false;

            // 이벤트 
            OnSynthesize?.Invoke(1);

            return true;
        }

        private bool InternalSynthesize(string missileIdx, int amount, out string resultMissileIdx)
        {
            resultMissileIdx = "";

            if (!CanSynthesize(missileIdx))
                return false;

            // 합성 전에 필요 갯수 차감
            var needCount = GetNeedMissileCountForSynthesis();
            OwnerControlHub.Data.Missiles.Consume(missileIdx, needCount * amount);

            // 합성  
            /*
             * 야매. 다음 미사일과의 인덱스 차이는 +1이므로, 그냥 인덱스에서 +1 처리함 
             */
            var synthesisMissileIdxInt = int.Parse(missileIdx) + 1;
            var synthesisMissileIdx = synthesisMissileIdxInt.ToLookUpString();
            Earn(synthesisMissileIdx, amount);

            resultMissileIdx = synthesisMissileIdx;
            return true;
        }

        public List<RewardData> BatchSynthesize()
        {
            var needCount = GetNeedMissileCountForSynthesis();
            
            // 배치 전 상태 기록
            var beforeStates = GetStateAllOwnedMissiles();

            var synthesisSum = 0;
            
            var allIdxes = DataboxController.GetAllIdxes(Table.Missile, Sheet.missile);
            foreach (var idx in allIdxes)
            {
                if (!CanSynthesize(idx))
                    continue;

                OwnerControlHub.Data.Missiles.GetState(idx, out var ownedCount, out _, out _, out _);

                var synthesisCount = ownedCount / needCount;

                if (InternalSynthesize(idx, synthesisCount, out _))
                    synthesisSum += synthesisCount;
            }
            
            // 배치 후 상태 
            var afterStates = GetStateAllOwnedMissiles();

            // 합성을 통해 갯수가 증가한 미사일만 결과에 넣음
            var results = new List<RewardData>();

            foreach (var afterState in afterStates)
            {
                // 배치 전에도 획득해있던 미사일인 경우,
                if (beforeStates.TryGetValue(afterState.Key, out var beforeCount))
                {
                    // 배치 후 갯수가 배치 전 갯수보다 적다면, 결과에 넣지 않음 
                    if (afterState.Value <= beforeCount)
                        continue;
                }
                
                // 배치 후 새로 생성된 미사일인 경우, 결과에 추가 됨 
                var result = new RewardData
                {
                    Idx = afterState.Key, 
                    Type = RewardType.REWARD_MISSILE,
                    Count = afterState.Value - beforeCount
                };
                results.Add(result);
            }
            
            // 이벤트 
            OnSynthesize?.Invoke(synthesisSum);

            // 결과
            return results;
        }

        private Dictionary<string, int> GetStateAllOwnedMissiles()
        {
            var states = new Dictionary<string, int>();

            var allIdxesEarned = OwnerControlHub.Data.Missiles.GetAllIdxesEarned();
            foreach (var missileIdx in allIdxesEarned)
            {
                OwnerControlHub.Data.Missiles.GetState(missileIdx, out var ownedCount, out _, out _, out _);
                
                states[missileIdx] = ownedCount;
            }
            
            return states;
        }

        #endregion

        #region Ancient

        public IEnumerable<string> AllIdxesAncient
        {
            get { return DataboxController.GetAllIdxes(Table.Missile, Sheet.missile).Where(missile => DataboxController.GetDataString(Table.Missile, Sheet.missile, missile, Column.rarity) == MissileRarity.ancient.ToEnumString()); }
        }

        public IEnumerable<string> AllIdxesEquippedWithoutAncient
        {
            get
            {
                var allEquippedMissiles = OwnerControlHub.Data.Missiles.GetAllIdxesEquipped();
                return allEquippedMissiles.Where(missile => DataboxController.GetDataString(Table.Missile, Sheet.missile, missile, Column.rarity) != MissileRarity.ancient.ToEnumString());
            }
        }

        public static int GetSellUranium(string missileIdx)
        {
            return DataboxController.GetDataInt(Table.Missile, Sheet.missile, missileIdx, Column.sell_uranium);
        }

        public bool SellAncient(string missileIdx)
        {
            var rarity = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.rarity);
            var isAncient = rarity == MissileRarity.ancient.ToEnumString();
            if (!isAncient) return false;
            
            // 획득된 미사일 아니거나, 수량이 0개일 경우, 종료
            var isEarned = OwnerControlHub.Data.Missiles.GetState(missileIdx, out var count, out _, out _, out _);
            if (!isEarned || count == 0) return false;
             
            // 미사일 수량 차감
            OwnerControlHub.Data.Missiles.Consume(missileIdx,1);
            
            // 우라늄 획득
            var sellUranium = GetSellUranium(missileIdx);
            ResourceController.Instance.AddUranium(sellUranium, LogEvent.SellMissile);
            
            // 저장 
            OwnerControlHub.Data.SaveToServer();
            
            // 이벤트 
            OnSell?.Invoke();

            return true;
        }
        
        #endregion

        public double GetBaseDamage()
        {
            return OwnerControlHub.BazookaController.GetFinalizedDamage();
        }

        public bool TryCritical(double damage, out double outCriticalDamage)
        {
            // 치명타 안 뜰 경우, 치명타 데미지는 기본 데미지가 됨 
            outCriticalDamage = damage;

            // 치명타 주사위 돌리기 
            var criticalChance = OwnerControlHub.BazookaController.GetFinalizedCriticalChance();
            var rolledCriticalChance = Random.Range(0.01f, 100f);
            var isCritical = rolledCriticalChance <= criticalChance; // 돌린 주사위 수가 최종 치명타 확률 범위라면, 치명타 
            if (!isCritical)
                return false;

            // 치명타 데미지 
            outCriticalDamage = OwnerControlHub.BazookaController.ApplyCriticalDamage(damage);

            return true;
        }

        public double GetMaxDamage()
        {
            // 최종 바주카 데미지
            var finalBazookaDamage = GetBaseDamage();

            // 몬스터 타입별 피해 증가 (가장 큰 타입 데미지 값 적용)
            var abilityDamageUpPer = 0d;
            
            var monsterAndGolemPer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.VS_DAMAGE_UP_MONSTER_GOLEM);
            if (monsterAndGolemPer > abilityDamageUpPer)
                abilityDamageUpPer = monsterAndGolemPer;

            var portalPer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.VS_DAMAGE_UP_PORTAL);
            if (portalPer > abilityDamageUpPer)
                abilityDamageUpPer = portalPer;

            var diabloPer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.VS_DAMAGE_UP_DIABLO);
            if (diabloPer > abilityDamageUpPer)
                abilityDamageUpPer = diabloPer;

            var bossAndElitePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.VS_DAMAGE_UP_ELITE_BOSS);
            if (bossAndElitePer > abilityDamageUpPer)
                abilityDamageUpPer = bossAndElitePer;

            var typeDamage = finalBazookaDamage + (finalBazookaDamage * (abilityDamageUpPer / 100d));

            // 치명타 데미지 (치명타 확률이 0만 아니라면 적용)
            var criticalChance = OwnerControlHub.BazookaController.GetFinalizedCriticalChance();
            if (criticalChance > 0)
            {
                var criticalDamage = OwnerControlHub.BazookaController.ApplyCriticalDamage(typeDamage);
                
                // 증폭 데미지 (치명타가 적용돼야만 증폭 가능)
                var ampleChance = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ALL_DAMAGE_DOUBLE_RATE);
                if (ampleChance > 0)
                {
                    // 증폭 데미지 반영 
                    var abilityAllDamageAmpleModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ALL_DAMAGE_DOUBLE_POWER);
                    var ampleDamage = criticalDamage + (criticalDamage * (abilityAllDamageAmpleModifiedPer / 100d));

                    return ampleDamage;
                }
                else
                {
                    return criticalDamage;
                }
            }
            else
            {
                return typeDamage;
            }
        }
    }
}