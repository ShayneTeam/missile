﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BackEnd;
using Global;
using Global.Extensions;
using InGame.Data;
using InGame.Data.Scene;
using InGame.Global;
using MEC;
using Server;
using UI.Popup;
using UI.Popup.Menu;
using UI.Popup.Menu.Plunder.Revenge;
using UI.Popup.Plunder;
using UnityEngine;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace InGame.Controller
{
    public class PlunderControl : InGameControl
    {
        // 유저 데이터 캐싱 여부 
        // 유저의 테이블 데이터 가져오는게 무겁기 때문에,
        // 테이블 데이터를 한 번 가져왔다면, 다시 가져오지 않음 
        // (허나 테이블 데이터를 가져왔지만, 약탈에서 쓰일 테이블 데이터는 안 가져왔을 수 있으므로
        // 이 캐싱 처리는 약탈에서만 추가하고, 약탈에서만 사용한다)
        // (UserData는 멀티톤이므로, 이 데이터 체크도 어디서든 똑같아야하므로 static 처리)
        private static readonly HashSet<string> _usersLoadDataHistory = new HashSet<string>();

        [RuntimeInitializeOnLoadMethod]
        private static void EnterPlayMode()
        {
            _usersLoadDataHistory.Clear();
        }
        
        
        private static string NecessaryPlanetIdx => DataboxController.GetConstraintsData("PLUNDER_NECESSARY_PLANET");
        
        public static event VoidDelegate OnEndedPlunder;
        


        public static void EnterPlunderScene()
        {
            SceneController.DespawnAll();
            
            // 씬 로드 
            SceneManager.LoadScene("Plunder");
            
            // GC 수동 호출
            GCController.CollectFull();
        }

        public static void ExitPlunderScene()
        {
            Timing.RunCoroutine(_ExitPlunderFlow());
        }
        
        public static void ExitPlunderSceneFromRevenge()
        {
            Timing.RunCoroutine(_ExitRevengeFlow());
        }

        private static IEnumerator<float> _ExitPlunderFlow()
        {
            // 스테이지 씬으로 돌아가기 
            yield return Timing.WaitUntilDone(SceneController._ReturnToStageFlow());

            // 한 프레임 쉬기 (중요)
            // 씬 돌아조마자 팝업 뛰우니, 뭔가 UI 안에 프리팹이 제대로 세팅이 안되는 문제 발생 
            // 버프 스피드의 타임스케일 변경 때문에 그럴 수도 있음
            // 무튼 씬 돌아와서 한 프레임 쉬어주는거 매우 중요 
            yield return Timing.WaitForOneFrame;
            
            // 약탈 메뉴 다시 열기 
            InGameMenu.Open("Plunder Menu");
        }
        
        private static IEnumerator<float> _ExitRevengeFlow()
        {
            // 스테이지 씬으로 돌아가기 
            yield return Timing.WaitUntilDone(_ExitPlunderFlow());
            
            // 한 프레임 쉬기 (중요)
            yield return Timing.WaitForOneFrame;
            
            // 복수 노트 열기
            RevengePopup.Show();
        }

        public bool IsUnlocked()
        {
            // 화성 스테이지 클리어 전까지 불가능
            return OwnerControlHub.StageController.IsOwned(NecessaryPlanetIdx);
        }

        public bool HasFlag()
        {
            // 해적 깃발이 0이면, 불가능 
            return OwnerControlHub.Data.Items[RewardType.REWARD_PLUNDER_FLAG] > 0;
        }

        public bool HasRevengeTicket()
        {
            // 0이면, 불가능 
            return OwnerControlHub.Data.Items[RewardType.REWARD_REVENGE_TICKET] > 0;
        }

        public bool HasRematchingCost()
        {
            return OwnerControlHub.Data.Resources.uranium >= GetRematchingCost();
        }

        public static int GetRematchingCost()
        {
            return int.Parse(DataboxController.GetConstraintsData(Constraints.PLUNDER_REMATCHING_GEM_COST));
        }

        public IEnumerator<float> MatchIfHasNotMatched()
        {
            if (OwnerControlHub.Data.Plunder.MatchedUsers.Count != 0) yield break;
            
            // 리매칭
            yield return Timing.WaitUntilDone(_RequestNewMatching());
        }

        public IEnumerator<float> _Rematching()
        {
            if (!HasRematchingCost()) yield break;
            
            // 비용 차감 
            var cost = GetRematchingCost();
            ResourceController.Instance.SubtractUranium(cost, LogEvent.PlunderRematching);
            
            // 리매칭
            yield return Timing.WaitUntilDone(_RequestNewMatching());
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }
        
        private IEnumerator<float> _RequestNewMatching()
        {
            // 약탈 미해금 시, 종료
            if (!IsUnlocked()) yield break;;
        
            // 매칭 (될 때까지 무한 요청)
            List<RankUserData> matchedUsers = null;
            while (matchedUsers == null)
            {
                yield return Timing.WaitUntilDone(ServerPlunder._MatchMakingPlunder(resultMatchedUsers => matchedUsers = resultMatchedUsers));
            }    

            // 매칭된 유저들 데이터 요청
            yield return Timing.WaitUntilDone(_RequestMatchedUsersData(matchedUsers));
        }
        
        public IEnumerator<float> _RequestNewMatchingOnce()
        {
            // 약탈 미해금 시, 종료
            if (!IsUnlocked()) yield break;;
        
            // 매칭 
            List<RankUserData> matchedUsers = null;
            yield return Timing.WaitUntilDone(ServerPlunder._MatchMakingPlunder(resultMatchedUsers => matchedUsers = resultMatchedUsers));
            
            // 매칭 실패 시, 종료
            if (matchedUsers == null) yield break;

            // 매칭된 유저들 데이터 요청
            yield return Timing.WaitUntilDone(_RequestMatchedUsersData(matchedUsers));
        }

        private IEnumerator<float> _RequestMatchedUsersData(IEnumerable<RankUserData> matchedUsers)
        {
            // 새로운 매칭이 됐다면, 기존 매칭 유저 데이터 날리기 
            if (matchedUsers.Any())
                OwnerControlHub.Data.Plunder.MatchedUsers.Clear();

            // 매칭 유저들의 테이블 데이타 기반으로 약탈에 쓸 데이터 정리 
            foreach (var matchedUser in matchedUsers)
            {
                // 테이블 요청 (비동기 대기)
                yield return Timing.WaitUntilDone(_RequestUserData(matchedUser.gamerInDate));

                // 인데이트, 닉네임
                var matchedUserInfo = new UserData.PlunderData.MatchedUserSchema
                {
                    inDate = matchedUser.gamerInDate,
                    nickname = matchedUser.nickname,
                    plunderableGas = CalcPlunderableGas(OwnerControlHub.InDate, matchedUser.gamerInDate)
                };

                // 저장 
                OwnerControlHub.Data.Plunder.MatchedUsers[matchedUser.gamerInDate] = matchedUserInfo;
            }
        }

        public static IEnumerator<float> _RequestUserData(string userInDate)
        {
            // 이미 요청 내역이 있다면, 요청하지 않음 (무겁기 때문)
            if (_usersLoadDataHistory.Contains(userInDate)) yield break;
            
            // 유저 테이블 요청 
            var matchedUserData = UserData.Other[userInDate];
            var loadTableList = GetNeededTablesList();
            yield return Timing.WaitUntilDone(matchedUserData._RequestServerData(loadTableList));
            
            // 캐싱 
            _usersLoadDataHistory.Add(userInDate);
        }

        private static double CalcPlunderableGas(string ownerUserInDate, string targetUserInDate)
        {
            // 상대 월드맵 가스량 계산 
            var miningSumUntilNow = InGameControlHub.Other[targetUserInDate].WorldMapController.GetMiningSumUntilNow();
            var miningGasSum = miningSumUntilNow[RewardType.REWARD_GAS];
                    
            // 나의 최고 스테이지 행성 인덱스 기준 
            StageControl.GetPlanetIdxByStage(InGameControlHub.My.Data.Stage.bestStage, out var planetIdx);
            var planetIndex = StageControl.GetPlanetIndex(planetIdx).ToLookUpString();

            var gasLimit = DataboxController.GetDataDouble(Table.Plunder, Sheet.limited, planetIndex, Column.gas_limit);
            var gasMin = DataboxController.GetDataDouble(Table.Plunder, Sheet.limited, planetIndex, Column.gas_min);
            var gasMax = DataboxController.GetDataDouble(Table.Plunder, Sheet.limited, planetIndex, Column.gas_max);

            double plunderableGas;
                    
            // 가스 제한량을 초과할 경우
            if (miningGasSum > gasLimit)
            {
                // 제한량 보다 커지지 않게 제한 
                plunderableGas = gasLimit;
            }
            // 가스 최솟값보다 작은 경우
            else if (miningGasSum < gasMin)
            {
                // Min / Max 랜덤값으로 지정 
                plunderableGas = Random.Range((float)gasMin, (float)gasMax);
            }
            // 제한량과 최솟값 사이에 있다면, 그냥 그걸로 씀 
            else
            {
                plunderableGas = miningGasSum;
            }

            return plunderableGas;
        }
        
        public IEnumerator<float> _RequestMatchedUsersData()
        {
            // 약탈 미해금 시, 종료
            if (!IsUnlocked()) yield break;

            // 약탈 매칭된 유저들의 데이터 요청 
            foreach (var matchedUser in OwnerControlHub.Data.Plunder.MatchedUsers.Values)
            {
                yield return Timing.WaitUntilDone(_RequestUserData(matchedUser.inDate));
            }
        }

        private static List<string> GetNeededTablesList()
        {
            return new List<string>
            {
                // 어빌리티 위해 필요
                ServerTable.Costume,
                ServerTable.Bazooka,
                ServerTable.Missiles,
                ServerTable.NormalRelic,
                ServerTable.DiabloRelic,
                ServerTable.Soldiers,
                ServerTable.InfinityStone,

                // 가스 계산
                ServerTable.WorldMap,

                // 행성 표기
                ServerTable.Stage,
            };
        }

        public IEnumerator<float> _SaveResultPlunder(string targetUserInDate, string targetNickname, double plunderedGas, string infStoneIdx, int sweepCount)
        {
            if (!HasFlag()) yield break;

            // 깃발 차감 
            OwnerControlHub.Data.Items[RewardType.REWARD_PLUNDER_FLAG] -= sweepCount;
            
            // 로그 
            ServerLogger.LogItem(RewardType.REWARD_PLUNDER_FLAG, $"{LogEvent.Plunder}_{targetNickname}", -sweepCount);
            
            // 가스 획득 
            OwnerControlHub.Data.Resources.gas += plunderedGas;
            
            // 스톤 획득 
            UserData.My.InfinityStone.AddStone(infStoneIdx, sweepCount);
            
            // 약탈 매칭 새롭게 요청 
            yield return Timing.WaitUntilDone(_RequestNewMatching());
            
            // 이벤트 
            OnEndedPlunder?.Invoke();

            // 상대에게 약탈 로그 적음 (상대가 약탈 컨텐츠 미해금 상태라면, 남기지 않음)
            if (!InGameControlHub.Other[targetUserInDate].PlunderController.IsUnlocked()) yield break;

            // 상대가 내게 복수할 때 얻을 수 있는 가스량 계산 
            var revengeGainGas = CalcPlunderableGas(targetUserInDate, OwnerControlHub.InDate);
            
            // 약탈 로그 남김 (로그 남기기 성공할 때까지 무한 요청) 
            var isSucceededToLog = false;
            while (!isSucceededToLog)
            {
                yield return Timing.WaitUntilDone(ServerPlunder._WriteLog(targetUserInDate, plunderedGas, revengeGainGas,
                    success => isSucceededToLog = success));
            } 
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }

        public IEnumerator<float> _SaveResultRevenge(string revengeLogInDate, double regainGas, string infStoneIdx)
        {
            if (!HasRevengeTicket()) yield break;
            
            // 복수 티켓 차감 
            OwnerControlHub.Data.Items[RewardType.REWARD_REVENGE_TICKET] -= 1;
            
            // 가스 획득 
            OwnerControlHub.Data.Resources.gas += regainGas;
            
            // 스톤 획득 
            UserData.My.InfinityStone.AddStone(infStoneIdx, 1);
            
            // 복수했다고 체크해 둠 (체크 성공할 때까지 무한 요청)
            var isSucceededToMark = false;
            while (!isSucceededToMark)
            {
                yield return Timing.WaitUntilDone(ServerPlunder.Instance._MarkAvengedLog(revengeLogInDate,
                    success => isSucceededToMark = success));
            }
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }

        public bool HasNewLog()
        {
            // 로그 요청 한 번 함 
            ServerPlunder.Instance.GetLogsOnBehind();
            
            // 일단 현재 가지고 있는 로그로 결과 반환해줌
            return ServerPlunder.Instance.HasNewLog();
        }

        public IEnumerator<float> _ApplyLogs(double collectedGas, Action<double, double> callback)
        {
            // 유저의 약탈 방어율  
            var protectPercentage = GetProtectGasPercentage();
            
            // 방어도가 적용된 보장 가스량 (아무리 차감되도 이 밑으로는 떨이지지 않음) 
            var guaranteedGas = collectedGas * (protectPercentage / 100f);
            
            // 약탈 양 총합  
            var plunderedGasSum = ServerPlunder.Instance.GetSumPlunderedGasByNewLogs();

            // 약탈 양 총합이 유저의 수집한 가스 양보다 커지지 않게 함 
            var clampedPlunderedGasSum = plunderedGasSum > collectedGas ? collectedGas : plunderedGasSum;
            
            // 차감 (보장된 가스양 밑으로 떨어지지 않게 함) 
            var subtractedGas = collectedGas - clampedPlunderedGasSum;
            var remainingGas = subtractedGas < guaranteedGas ? guaranteedGas : subtractedGas;
            
            // 최종적으로 차감된 양을 저장해둠 
            var plunderedGas = collectedGas - remainingGas;
            OwnerControlHub.Data.Plunder.PlunderedGasSum += plunderedGas;
            
            // 이 계산에서 사용된 약탈 양 총합이 다시 사용되지 않도록 마킹 
            yield return Timing.WaitUntilDone(ServerPlunder.Instance._MarkAppliedNewLogs());
            
            callback?.Invoke(plunderedGas, remainingGas);
        }

        public bool CanProtect()
        {
            return OwnerControlHub.Data.UserInfo.GetFlag(UserFlags.CanDefendPlunder);
        }

        public float GetProtectGasPercentage()
        {
            if (CanProtect())
                return float.Parse(DataboxController.GetConstraintsData(Constraints.PLUNDER_PROTECT_GAS_PERCENT), CultureInfo.InvariantCulture);
            else
                return float.Parse(DataboxController.GetConstraintsData(Constraints.PLUNDER_DEFENDER_GAS_PERCENT_MAX), CultureInfo.InvariantCulture);
        }

        public string GetProtectText()
        {
            var protectPercentage = GetProtectGasPercentage();
            var strKey = CanProtect() ? "info_184" : "info_258";
            var percentStr = protectPercentage.ToString("0");
            return Localization.GetFormatText(strKey, percentStr); 
        }
        
        public static bool IsEntered()
        {
            return Plunder.Plunder.Instance != null;
        }

        public int MaxCountToSweep
        {
            get
            {
                var flag = OwnerControlHub.Data.Items[RewardType.REWARD_PLUNDER_FLAG];
                var max = int.Parse(DataboxController.GetConstraintsData(Constraints.PLUNDER_SWEEP_FLAG_COUNT));
                return Math.Min(flag, max);
            }
        }
    }
}