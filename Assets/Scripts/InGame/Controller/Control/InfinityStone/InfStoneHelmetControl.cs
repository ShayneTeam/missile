﻿using System.Collections.Generic;
using Databox.Dictionary;
using Firebase.Analytics;
using Global;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller
{
    public partial class InfStoneControl : InGameControl
    {
	    public readonly HelmetControl Helmet;

	    public class HelmetControl
	    {
		    private readonly InfStoneControl _parent;

		    public HelmetControl(InfStoneControl parent)
		    {
			    _parent = parent;
		    }

		    public int Level => _parent.OwnerControlHub.Data.InfinityStone.HelmetLevel;

		    public string Idx
		    {
			    get
			    {
				    var helmetLevel = Level;
				    return GetIdx(helmetLevel);
			    }
		    }

		    public int LevelSumRequirementForLevelUp => DataboxController.GetDataInt(Table.Stone, Sheet.helmet, Idx, Column.need_stone_lv_sum);


		    public float AllDamageFactor => GetDamageWeight(Idx);
		    public float MineralFactor => GetMineralWeight(Idx);
		    public float AmpleDamagePer => GetAmpleWeight(Idx);

		    public bool TryGetNextLevelInfo(out float damage, out float mineral, out float ample)
		    {
			    damage = mineral = ample = -1f;

			    if (IsMaxLevel()) return false;
			    
			    var nextLevel = Level + 1;
			    var nextLevelIdx = GetIdx(nextLevel);
			    
			    damage = GetDamageWeight(nextLevelIdx);
			    mineral = GetMineralWeight(nextLevelIdx);
			    ample = GetAmpleWeight(nextLevelIdx);

			    return true;
		    }

		    private static string GetIdx(int level)
		    {
			    return DataboxController.GetIdxByColumn(Table.Stone, Sheet.helmet, Column.helmet_level, level.ToLookUpString());
		    }

		    public bool IsMaxLevel()
		    {
			    var nextHelmetLevel = Level + 1;

			    // 다음 단계 헬멧 인덱스가 없다면, 현재가 최대 레벨인 것
			    return string.IsNullOrEmpty(GetIdx(nextHelmetLevel));
		    }

		    public bool CanLevelUp()
		    {
			    if (IsMaxLevel())
				    return false;

			    if (!HasMetLevelRequirement())
				    return false;
		    
			    return true;
		    }

		    public bool HasMetLevelRequirement()
		    {
			    // 스톤 총합 레벨이 조건보다 높아야 함 
			    return _parent.Stone.GetAllStonesLevelSum() >= LevelSumRequirementForLevelUp;
		    }

		    public void LevelUp()
		    {
			    if (!CanLevelUp())
				    return;
			    
			    // 레벨업 
			    _parent.OwnerControlHub.Data.InfinityStone.LevelUpHelmet();

			    // 로그 
			    FirebaseAnalytics.LogEvent("helmet_level_upgrade", new[]
			    {
				    new Parameter("helmet_level", Level),
			    });
		    }

		    private static float GetDamageWeight(string idx)
		    {
			    return DataboxController.GetDataFloat(Table.Stone, Sheet.helmet, idx, Column.damage_weight);
		    }

		    private static float GetMineralWeight(string idx)
		    {
			    return DataboxController.GetDataFloat(Table.Stone, Sheet.helmet, idx, Column.mineral_weight);
		    }

		    private static float GetAmpleWeight(string idx)
		    {
			    return DataboxController.GetDataFloat(Table.Stone, Sheet.helmet, idx, Column.cri_double_weight);
		    }
	    }
    }
}