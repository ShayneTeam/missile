﻿using System.Collections.Generic;
using Databox.Dictionary;
using Firebase.Analytics;
using Global;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller
{
    public partial class InfStoneControl : InGameControl
    {
        private static string NecessaryPlanetIdx => DataboxController.GetConstraintsData(Constraints.INFINITY_STONE_NECESSARY_PLANET);
        
        public event VoidDelegate OnChangedAbilities;

        public InfStoneControl()
        {
            Helmet = new HelmetControl(this);
            Stone = new StoneControl(this);
        }

        private void Start()
        {
            OwnerControlHub.Data.InfinityStone.OnLevelUpStone += OnLevelUpStone;
            OwnerControlHub.Data.InfinityStone.OnLevelUpHelmet += OnLevelUpHelmet;
        }

        private void OnDestroy()
        {
            OwnerControlHub.Data.InfinityStone.OnLevelUpStone -= OnLevelUpStone;
            OwnerControlHub.Data.InfinityStone.OnLevelUpHelmet -= OnLevelUpHelmet;
        }

        private void OnLevelUpStone(string soldierType)
        {
            OnChangedAbilities?.Invoke();
        }

        private void OnLevelUpHelmet()
        {
            OnChangedAbilities?.Invoke();
        }

        public OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
            return Stone.GetAllAppliedAbilities();
        }
        
        public bool IsUnlocked()
        {
            // 조건 행성 클리어 전까지 불가능
            return OwnerControlHub.StageController.IsOwned(NecessaryPlanetIdx);
        }
    }
}