﻿using System.Collections.Generic;
using System.Linq;
using Databox.Dictionary;
using Firebase.Analytics;
using Global;
using InGame.Data;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller
{
    public partial class InfStoneControl : InGameControl
    {
	    public readonly StoneControl Stone;

	    public class StoneControl
	    {
		    private readonly InfStoneControl _parent;

		    public StoneControl(InfStoneControl parent)
		    {
			    _parent = parent;
		    }

		    public static int GetIndex(string idx)
		    {
			    return DataboxController.GetDataInt(Table.Stone, Sheet.stone, idx, Column.index);
		    }

		    #region LevelUp

		    private static int NeedStoneForLevelUp => 1;

		    public int MaxLevel => DataboxController.GetDataInt(Table.Stone, Sheet.helmet, _parent.Helmet.Idx, Column.stone_level_max);

		    public int ChanceForLevelUp => DataboxController.GetDataInt(Table.Stone, Sheet.helmet, _parent.Helmet.Idx, Column.upgrade_chance);

		    public bool IsMaxLevel(string idx)
		    {
			    _parent.OwnerControlHub.Data.InfinityStone.GetStoneState(idx, out var level, out _);

			    return level >= MaxLevel;
		    }

		    public bool HasEnoughAmountForLevelUp(string idx)
		    {
			    _parent.OwnerControlHub.Data.InfinityStone.GetStoneState(idx, out _, out var amount);
			    return amount >= NeedStoneForLevelUp;
		    }

		    public bool CanLevelUp(string idx)
		    {
			    if (IsMaxLevel(idx))
				    return false;

			    if (!HasEnoughAmountForLevelUp(idx))
				    return false;
		    
			    return true;
		    }

		    public bool RollForLevelUp(string idx)
		    {
			    if (!CanLevelUp(idx))
				    return false;

			    // 스톤 차감
			    _parent.OwnerControlHub.Data.InfinityStone.SubtractStone(idx, NeedStoneForLevelUp);
			    
			    // 주사위 돌리기 
			    return GlobalFunction.Roll(ChanceForLevelUp);
		    }

		    public void LevelUp(string idx)
		    {
			    // 레벨업  
			    _parent.OwnerControlHub.Data.InfinityStone.LevelUpStone(idx);

			    // 로그 
			    _parent.OwnerControlHub.Data.InfinityStone.GetStoneState(idx, out var level, out _);
			    FirebaseAnalytics.LogEvent("stone_level_upgrade", new[]
			    {
				    new Parameter("stone_idx", idx),
				    new Parameter("stone_level", level),
			    });
		    }

		    public int GetAllStonesLevelSum()
		    {
			    var allIdxes = DataboxController.GetAllIdxes(Table.Stone, Sheet.stone);

			    var sum = 0;
			    foreach (var idx in allIdxes)
			    {
				    _parent.OwnerControlHub.Data.InfinityStone.GetStoneState(idx, out var level, out _);
				    sum += level;
			    }

			    return sum;
		    }

		    public bool HasAnyStoneForLevelUp()
		    {
			    var allIdxes = DataboxController.GetAllIdxes(Table.Stone, Sheet.stone);
			    foreach (var idx in allIdxes)
			    {
				    if (CanLevelUp(idx))
					    return true;
			    }

			    return false;
		    }

		    #endregion

		    #region Ability

		    public OrderedDictionary<string, float> GetAllAppliedAbilities()
		    {
			    // 스톤
			    var allStoneIdxes = DataboxController.GetAllIdxes(Table.Stone, Sheet.stone);

			    // 인덱스 기준으로 정렬 
			    var sortedByIdx = new SortedList<string, float>(); // idx, value

			    foreach (var stoneIdx in allStoneIdxes)
			    {
				    // 어빌리티 1
				    {
					    var ab1Idx = GetAbility1Idx(stoneIdx);
					    var ab1FinalizedValue = GetFinalizedAbility1Value(stoneIdx);
					    
					    // 이미 들어가있는 어빌리티라면 기존 밸류에 추가함 
					    if (sortedByIdx.ContainsKey(ab1Idx)) sortedByIdx[ab1Idx] += ab1FinalizedValue;
					    else sortedByIdx[ab1Idx] = ab1FinalizedValue;
				    }
				    
				    // 어빌리티 2 (있는게 있고, 없는게 있음)
				    {
					    if (!HasAbility2(stoneIdx)) continue;
					    
					    var ab2Idx = GetAbility2Idx(stoneIdx);
					    var ab2FinalizedValue = GetFinalizedAbility2Value(stoneIdx);
					    
					    // 이미 들어가있는 어빌리티라면 기존 밸류에 추가함 
					    if (sortedByIdx.ContainsKey(ab2Idx)) sortedByIdx[ab2Idx] += ab2FinalizedValue;
					    else sortedByIdx[ab2Idx] = ab2FinalizedValue;
				    }
			    }

			    // 동일한 어빌리티 타입을 가지는 인덱스들의 밸류값 총합 계산 (없겠지만, 혹시나 나중에 그렇게 될 수 있으니 걍 돌림)
			    var sumValues = new OrderedDictionary<string, float>(); // type, value 
			    AbilityControl.MergeByType(ref sumValues, sortedByIdx);

			    return sumValues;
		    }

		    public static string GetAbility1Idx(string stoneIdx)
		    {
			    return DataboxController.GetDataString(Table.Stone, Sheet.stone, stoneIdx, Column.st_aidx);
		    }
		    
		    public static string GetAbility2Idx(string stoneIdx)
		    {
			    return DataboxController.GetDataString(Table.Stone, Sheet.stone, stoneIdx, Column.st_aidx_2);
		    }

		    public static bool HasAbility2(string stoneIdx)
		    {
			    var ab2Idx = GetAbility2Idx(stoneIdx);
			    return !string.IsNullOrEmpty(ab2Idx) && ab2Idx != "0";
		    }

		    public float GetFinalizedAbility1Value(string stoneIdx)
		    {
			    var abIdx = GetAbility1Idx(stoneIdx);
			    return InternalGetFinalizedAbilityValue(stoneIdx, abIdx, Column.st_aidx_gain);
		    }

		    public float GetFinalizedAbility2Value(string stoneIdx)
		    {
			    var abIdx = GetAbility2Idx(stoneIdx);
			    return InternalGetFinalizedAbilityValue(stoneIdx, abIdx, Column.st_aidx_2_gain);
		    }

		    private float InternalGetFinalizedAbilityValue(string stoneIdx, string abIdx, string abWeightColumn)
		    {
			    _parent.OwnerControlHub.Data.InfinityStone.GetStoneState(stoneIdx, out var level, out _);

			    var abilityValue = AbilityControl.GetValue(abIdx);

			    // 가중치 적용
			    var weightColumn = DataboxController.GetDataString(Table.Stone, Sheet.stone, stoneIdx, abWeightColumn);
			    var weightedValue = (float)WeightController.ModifyWeight(abilityValue, Table.Stone, Sheet.gain, level.ToLookUpString(), weightColumn);

			    return weightedValue;
		    }

		    #endregion

		    #region Mind Stone

		    public float MindStoneDamagePer
		    {
			    get
			    {
				    // 모든 용병의 레벨 + 바주카 레벨
				    var sumArmyLevel = GetSumArmyLevel();
				    
				    // 마인드 스톤 어빌리티 
				    var abMindStonePer = _parent.OwnerControlHub.AbilityController.GetUnifiedValue(Ability.BAZOOKA_ARMY_LEVEL_DAMAGE);

				    // 위 둘을 곱한게 최종값 
				    return sumArmyLevel * abMindStonePer;
			    }
		    }

		    public int GetSumArmyLevel()
		    {
			    return _parent.OwnerControlHub.SoldierController.GetAllSoldiersLevelSum() + _parent.OwnerControlHub.BazookaController.Level;
		    }

		    #endregion

		    #region Soul Stone

		    public float SoulStoneDamageFactor
		    {
			    get
			    {
				    // 현재 장착 바주카의 데미지 팩터 
				    var bazookaDamageFactor = _parent.OwnerControlHub.BazookaController.GetSumDamageRate();
				    
				    // 소울 스톤 어빌리티 
				    var abSoulStonePer = _parent.OwnerControlHub.AbilityController.GetUnifiedValue(Ability.BAZOOKA_ARMY_SOUL);

				    // 위 둘을 곱한게 최종값 
				    return 1f + bazookaDamageFactor * (abSoulStonePer / 100f);
			    }
		    }

		    #endregion

		    public static void Gacha(int count, out Dictionary<string, int> result)
		    {
			    result = new Dictionary<string, int>();
			    
			    // 가챠 
			    var allIdxes = DataboxController.GetAllIdxes(Table.Stone, Sheet.stone);
			    
			    for (var i = 0; i < count; i++)
			    {
				    var randomIdx = allIdxes.ElementAt(UnityEngine.Random.Range(0, allIdxes.Count));

				    if (result.ContainsKey(randomIdx)) result[randomIdx]++;
				    else result[randomIdx] = 1;
			    }
			    
			    // 지급
			    foreach (var stone in result)
			    {
				    UserData.My.InfinityStone.AddStone(stone.Key, stone.Value);    
			    }

			    // 유저 데이터 즉각 저장
			    UserData.My.SaveToServer();
		    }

		    public static string RandomPickStone()
		    {
			    // 스톤 중 하나 랜덤으로 뽑음 
			    var allIdxes = DataboxController.GetAllIdxes(Table.Stone, Sheet.stone);
			    return allIdxes.ElementAt(UnityEngine.Random.Range(0, allIdxes.Count));
		    }
	    }
    }
}