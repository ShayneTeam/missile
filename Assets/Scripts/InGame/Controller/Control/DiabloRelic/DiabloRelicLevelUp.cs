using System.Collections.Generic;
using Global;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller
{
    public partial class DiabloRelicControl : InGameControl
    {
        // NestedClass 정의
        public readonly DiabloRelicLevelUp Level;

        public class DiabloRelicLevelUp
        {
            private readonly DiabloRelicControl _parent;

            public DiabloRelicLevelUp(DiabloRelicControl parent)
            {
                _parent = parent;
            }

            public bool IsMaxLevel(string dRelicIdx)
            {
                return GetRemainLevelToMax(dRelicIdx) <= 0;
            }

            public int GetRemainLevelToMax(string dRelicIdx)
            {
                _parent.OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out var level, out _);

                var maxLevel = DataboxController.GetDataInt(Table.Relic, Sheet.DiabloRelic, dRelicIdx, "max_level");

                return maxLevel - level;
            }

            public int CalcRequiredCountToLevelUp(string dRelicIdx, int levelCount)
            {
                _parent.OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out var currentLevel, out _);

                // 요청된 레벨 횟수만큼 레벨별 필요 미네랄 양 합산
                var sum = 0;
                for (var i = 0; i < levelCount; i++)
                {
                    var level = currentLevel + i;

                    var weightColumn = DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, dRelicIdx, "evo_count_level");

                    sum += DataboxController.GetDataInt(Table.Relic, Sheet.level, level.ToLookUpString(), weightColumn);
                }

                return sum;
            }

            public int CalcRequiredCountToMaxLevel(string dRelicIdx)
            {
                var remainLevel = GetRemainLevelToMax(dRelicIdx);
                return CalcRequiredCountToLevelUp(dRelicIdx, remainLevel);
            }

            public bool HasEnoughCount(string dRelicIdx)
            {
                _parent.OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out _, out var ownedCount);

                return ownedCount >= CalcRequiredCountToLevelUp(dRelicIdx, 1);
            }

            public bool CanLevelUp(string dRelicIdx)
            {
                // 보유해야 함 
                if (!_parent.OwnerControlHub.Data.DiabloRelic.IsEarned(dRelicIdx))
                    return false;

                // 최대레벨 아니어야 함 
                if (IsMaxLevel(dRelicIdx))
                    return false;

                // 레벨업 가능한만큼의 보유 갯수 있어야 함 
                if (!HasEnoughCount(dRelicIdx))
                    return false;

                return true;
            }

            public void LevelUp(string dRelicIdx)
            {
                if (!CanLevelUp(dRelicIdx))
                    return;

                // 보유 갯수 차감 
                var requiredCount = CalcRequiredCountToLevelUp(dRelicIdx, 1);
                _parent.OwnerControlHub.Data.DiabloRelic.Consume(dRelicIdx, requiredCount);

                // 업그레이드 
                _parent.OwnerControlHub.Data.DiabloRelic.LevelUp(dRelicIdx);

                _parent.OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out var level, out _);

                WebLog.Instance.ThirdPartyLog("diablo_relic_upgrade", new Dictionary<string, object>
                {
                    { "relic_idx", dRelicIdx },
                    { "relic_level", level.ToLookUpString() },
                });
            }
        }
    }
}