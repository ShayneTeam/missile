﻿using System;
using System.Collections.Generic;
using System.Linq;
using Databox.Dictionary;
using Firebase.Analytics;
using Global;
using InGame.Global;
using Server;

namespace InGame.Controller
{
    public partial class DiabloRelicControl : InGameControl
    {
        // NestedClass 초기화
        public DiabloRelicControl()
        {
            Exchange = new DiabloRelicExchange(this);
            Gacha = new DiabloRelicGacha(this);
            Level = new DiabloRelicLevelUp(this);
        }
        
        
        public event VoidDelegate OnChangedAbilities;
	    
        private void Start()
        {
            OnGacha += OnGachaDiabloRelic;
            OwnerControlHub.Data.DiabloRelic.OnLevelUp += OnChangedData;
        }

        private void OnDestroy()
        {
            OnGacha -= OnGachaDiabloRelic;
            OwnerControlHub.Data.DiabloRelic.OnLevelUp -= OnChangedData;
        }

        private void OnGachaDiabloRelic(int count)
        {
            OnChangedAbilities?.Invoke();
        }

        private void OnChangedData(string soldierType)
        {
            OnChangedAbilities?.Invoke();
        }
        
        
        #region Ability

        public float GetFinalizedAbilityValue(string dRelicIdx, int index)
        {
            if (!OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out var level, out _))
                return -1;

            var weightColumn = DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, dRelicIdx, $"d_aidx_{index.ToLookUpString()}_gain");
            var weight = DataboxController.GetDataFloat(Table.Relic, Sheet.gain, level.ToLookUpString(), weightColumn);

            var abilityIdx = DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, dRelicIdx, $"d_aidx_{index.ToLookUpString()}");
            var abilityValue = AbilityControl.GetValue(abilityIdx);

            return abilityValue * weight;
        }
        
        public OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
            var allEarnedIdx = OwnerControlHub.Data.DiabloRelic.GetAllEarned();
            
            // 인덱스 기준으로 정렬 
            var sortedByIdx = new SortedList<string, float>(); // idx, value

            foreach (var dRelicIdx in allEarnedIdx)
            {
                // 어빌리티 
                for (var abilityIndex = 1; abilityIndex <= 2; abilityIndex++)
                {
                    var finalizedValue = GetFinalizedAbilityValue(dRelicIdx, abilityIndex);

                    var abilityIdx = DataboxController.GetDataString(Table.Relic, Sheet.DiabloRelic, dRelicIdx, $"d_aidx_{abilityIndex.ToLookUpString()}");

                    // 이미 들어가있는 어빌리티라면 기존 밸류에 추가함 
                    if (sortedByIdx.ContainsKey(abilityIdx))
                        sortedByIdx[abilityIdx] += finalizedValue;
                    else
                        sortedByIdx[abilityIdx] = finalizedValue;
                }
            }

            // 동일한 어빌리티 타입을 가지는 인덱스들의 밸류값 총합 계산 
            var sumValues = new OrderedDictionary<string, float>(); // type, value 
            AbilityControl.MergeByType(ref sumValues, sortedByIdx);
	        
            return sumValues;
        }

        public int GetAverageLevelOfAllEarned()
        {
            var allEarned = OwnerControlHub.Data.DiabloRelic.GetAllEarned();
            if (!allEarned.Any())
                return 1;   // 획득 유물이 제로라면 강제 1 반환 
            
            var sumLevel = 0;
            foreach (var dRelicIdx in allEarned)
            {
                OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out var level, out _);
                sumLevel += level;
            }

            return sumLevel / allEarned.Count();
        }
        
        #endregion



        public bool HasAllCollected()
        {
            var allDiabloRelicIdxes = DataboxController.GetAllIdxes(Table.Relic, Sheet.DiabloRelic);
            foreach (var dRelicIdx in allDiabloRelicIdxes)
            {
                // 하나라도 미획득 시, false
                if (!OwnerControlHub.Data.DiabloRelic.IsEarned(dRelicIdx))
                    return false;
            }

            return true;
        }

        private bool HasMaxCount(string dRelicIdx)
        {
            // 더 이상 보유할 수 없는 최대갯수까지 들고 있는가
            OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out _, out var ownedCount);

            // 앞으로 필요한 열쇠 갯수 = 남은 레벨 * 레벨업 당 필요 갯수 
            var requiredCount = Level.CalcRequiredCountToMaxLevel(dRelicIdx);

            // 보유한 갯수가 이상이라면 true 
            return ownedCount >= requiredCount;
        }

        public bool HasAllMaxCount
        {
            get
            {
                var allDiabloRelicIdxes = DataboxController.GetAllIdxes(Table.Relic, Sheet.DiabloRelic);
                foreach (var dRelicIdx in allDiabloRelicIdxes)
                {
                    // 하나라도 최대 갯수가 아니라면, false 
                    if (!HasMaxCount(dRelicIdx))
                        return false;
                }

                return true;
            }
        }
    }
}