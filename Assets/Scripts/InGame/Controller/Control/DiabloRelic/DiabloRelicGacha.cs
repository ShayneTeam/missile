using System;
using System.Collections.Generic;
using System.Linq;
using Global;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller
{
    public partial class DiabloRelicControl : InGameControl
    {
        // 이벤트 
        public event IntDelegate OnGacha;
        
        // NestedClass 정의
        public readonly DiabloRelicGacha Gacha;

        public class DiabloRelicGacha
        {
            private readonly DiabloRelicControl _parent;
            private static readonly List<RewardData> _emptyRewardDataList = new List<RewardData>();

            public DiabloRelicGacha(DiabloRelicControl parent)
            {
                _parent = parent;
            }

            public const int RequiredKeyToGacha = 1; // 1개 고정 

            public bool HasEnoughKey => _parent.OwnerControlHub.Data.Resources.diablokey >= RequiredKeyToGacha;

            public int CalcGachaCount()
            {
                // 현재 열쇠 갯수로 몇 번의 가챠를 할 수 있는가 
                var gachaCount = _parent.OwnerControlHub.Data.Resources.diablokey / RequiredKeyToGacha;

                // 보유 유물들이 최대 레벨까지 갯수가 얼마나 필요한가   
                var sumRequiredCount = 0;
                var allCollectedIdxes = _parent.OwnerControlHub.Data.DiabloRelic.GetAllEarned();
                foreach (var dRelicIdx in allCollectedIdxes)
                {
                    // 이미 최대 갯수 유물은 스킵  
                    if (_parent.HasMaxCount(dRelicIdx)) continue;
                    
                    var requiredCountToMaxLevel = _parent.Level.CalcRequiredCountToMaxLevel(dRelicIdx);

                    _parent.OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out _, out var ownedCount);

                    // 최대 레벨까지 필요한 갯수 - 현재 보유 갯수 
                    sumRequiredCount += requiredCountToMaxLevel - ownedCount;
                }

                // 열쇠가 넘쳐서 가챠를 더 많이 할 수 있어도, 딱 필요한 양만큼의 가챠 횟수 반환 
                return Math.Min(gachaCount, sumRequiredCount);
            }

            public bool CanGacha
            {
                get
                {
                    // 열쇠 없다면, 가챠 불가 
                    if (!HasEnoughKey) return false;

                    // 모든 유물이 최대갯수(=최대레벨) 라면, 가챠 불가 
                    if (_parent.HasAllMaxCount) return false;

                    return true;
                }
            }

            public bool GachaOnce(out string dRelicIdx)
            {
                dRelicIdx = "";
                
                if (!CanGacha) return false;

                // 가챠 비용 차감
                _parent.OwnerControlHub.Data.Resources.diablokey -= RequiredKeyToGacha;

                // 가챠
                if (!TryGachaOnce(out dRelicIdx)) return false; 

                // 이벤트
                _parent.OnGacha?.Invoke(1);

                return true;
            }

            public bool GachaBatch(out IEnumerable<RewardData> results)
            {
                results = _emptyRewardDataList;

                if (!CanGacha) return false;

                // 최대 가능한 가챠 횟수 계산
                var gachaCount = CalcGachaCount();

                // 일괄 가챠
                var tempList = new List<RewardData>();
                for (var i = 0; i < gachaCount; i++)
                {
                    // 도중에 실패 시, 즉각 종료 처리 
                    if (!TryGachaOnce(out var dRelicIdx))
                    {
                        return false;
                    }
                        
                    tempList.Add(new RewardData(RewardType.REWARD_DIABLO_RELIC, dRelicIdx, 1));
                }

                // 디아블로 열쇠 차감 
                _parent.OwnerControlHub.Data.Resources.diablokey -= RequiredKeyToGacha * gachaCount;

                // 이벤트
                _parent.OnGacha?.Invoke(gachaCount);

                // 가챠 결과 정렬 
                results = tempList.Where(t => t.Idx != null).GroupBy(x => x.Idx)
                    .Select(group => new RewardData
                    {
                        Idx = group.Key,
                        Type = group.First().Type,
                        Count = group.Sum(x => x.Count)
                    }).OrderByDescending(x => x.Idx);

                return true;
            }

            private bool TryGachaOnce(out string dRelicIdx)
            {
                // 뽑기 
                if (!TryRoll(out dRelicIdx))
                    return false;
                
                _parent.OwnerControlHub.Data.DiabloRelic.Earn(dRelicIdx);

                // 로그 
                WebLog.Instance.ThirdPartyLog("equipment_upgrade", new Dictionary<string, object>
                {
                    { "type", "diablo_relic_gacha" },
                    { "relic_idx", dRelicIdx },
                });

                WebLog.Instance.ThirdPartyLog("diablo_relic_gacha", new Dictionary<string, object> { { "relic_idx", dRelicIdx } });

                return true;
            }

            private bool TryRoll(out string pickedIdx)
            {
                pickedIdx = "";

                var rollList = new List<CommonRoll>();

                var allDiabloRelicIdxes = DataboxController.GetAllIdxes(Table.Relic, Sheet.DiabloRelic);
                foreach (var dRelicIdx in allDiabloRelicIdxes)
                {
                    // 최대 갯수라면 제외 
                    if (_parent.HasMaxCount(dRelicIdx)) continue;

                    // 최대 레벨 도달이라면 제외 
                    if (_parent.Level.IsMaxLevel(dRelicIdx)) continue;

                    GlobalFunction.AddRoll(ref rollList, dRelicIdx, 1);
                }

                // 더 이상 가챠할 수 있는 게 없다면, 가챠 안 함 
                if (rollList.Count == 0) return false;

                // 주사위 돌리기 
                var roll = GlobalFunction.Roll(ref rollList);

                pickedIdx = roll.Name;

                return true;
            }
        }
    }
}