using Server;
using UnityEngine;

namespace InGame.Controller
{
    public partial class DiabloRelicControl : InGameControl
    {
        // 이벤트
        public event VoidDelegate OnExchange;
        
        // NestedClass 정의
        public readonly DiabloRelicExchange Exchange;

        public class DiabloRelicExchange
        {      
            private readonly DiabloRelicControl _parent;

            public DiabloRelicExchange(DiabloRelicControl parent)
            {
                _parent = parent;
            }

            public const int ExchangeRate = 1; // 1개 고정 

            public bool HasEnoughKey => _parent.OwnerControlHub.Data.Resources.diablokey >= ExchangeRate;

            public bool CanExchange => CalcAllExchangeAmount() > 0;

            public int CalcAllExchangeAmount()
            {
                // 최대 갯수 넘은 것들 판매 가능 
                var exchangeCount = CalcExceededCount();
            
                // 모든 유물이 만렙이라면, 열쇠도 판매 가능  
                var exchangeKeys = CalcExchangeKeys();

                // 합산 값 반환 
                return exchangeCount + exchangeKeys;
            }

            public int Exchange()
            {
                var total = ExchangeExceededCount() + ExchangeKeys();
                
                // 이벤트
                _parent.OnExchange?.Invoke();
                
                return total;
            }

            private int CalcExceededCount()
            {
                var sum = 0;
                
                var allCollectedIdxes = _parent.OwnerControlHub.Data.DiabloRelic.GetAllEarned();
                foreach (var dRelicIdx in allCollectedIdxes)
                {
                    // 최대레벨 유물만 체크 
                    if (!_parent.Level.IsMaxLevel(dRelicIdx)) continue;
                    
                    _parent.OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out _, out var ownedCount);
            
                    // 앞으로 필요한 갯수 = 남은 레벨 * 레벨업 당 필요 갯수 
                    var requiredCount = _parent.Level.CalcRequiredCountToMaxLevel(dRelicIdx);

                    // 필요한 갯수보다 초과된 갯수 
                    var exceededCount = ownedCount - requiredCount;
                    
                    sum += exceededCount;
                }

                return sum;
            }

            private int ExchangeExceededCount()
            {
                var sum = 0;
                
                var allCollectedIdxes = _parent.OwnerControlHub.Data.DiabloRelic.GetAllEarned();
                foreach (var dRelicIdx in allCollectedIdxes)
                {
                    // 최대레벨 유물만 체크 
                    if (!_parent.Level.IsMaxLevel(dRelicIdx)) continue;
                    
                    _parent.OwnerControlHub.Data.DiabloRelic.GetState(dRelicIdx, out _, out var ownedCount);
            
                    // 앞으로 필요한 갯수 = 남은 레벨 * 레벨업 당 필요 갯수 
                    var requiredCount = _parent.Level.CalcRequiredCountToMaxLevel(dRelicIdx);

                    // 필요한 갯수보다 초과된 갯수 
                    var exceededCount = ownedCount - requiredCount;

                    // 초과 갯수 없으면 넘김
                    if (exceededCount <= 0) continue;
                    
                    // 우라늄 환전 
                    ResourceController.Instance.AddUranium(exceededCount, $"{LogEvent.ExchangeCount}_{dRelicIdx}");

                    // 갯수 차감
                    _parent.OwnerControlHub.Data.DiabloRelic.Consume(dRelicIdx, exceededCount);
                    
                    //
                    sum += exceededCount;
                }
            
                // 즉각 저장 
                _parent.OwnerControlHub.Data.SaveToServer();

                // 
                return sum;
            }

            private int CalcExchangeKeys()
            {
                return _parent.OwnerControlHub.Data.Resources.diablokey * ExchangeRate;
            }

            private int ExchangeKeys()
            {
                // 환전양 
                var exchangeAmount = CalcExchangeKeys();

                // 우라늄 환전 
                ResourceController.Instance.AddUranium(exchangeAmount, LogEvent.ExchangeKeys);

                // 열쇠 차감 
                _parent.OwnerControlHub.Data.Resources.diablokey -= exchangeAmount;
            
                // 즉각 저장 
                _parent.OwnerControlHub.Data.SaveToServer();

                return exchangeAmount;
            }
        }
    }
}