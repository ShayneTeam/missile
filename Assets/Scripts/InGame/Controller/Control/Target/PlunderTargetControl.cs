﻿using System.Collections.Generic;
using InGame.Data;
using InGame.Data.Scene;
using InGame.Plunder;
using UnityEngine;

namespace InGame.Controller
{
    public class PlunderTargetControl : TargetControl
    {
        public override Transform GetTarget(float range)
        {
            var isMine = OwnerControlHub.InDate == UserData.My.InDate;
            
            // TODO. HACK. 이 클래스는 PVP 클래스지만, 약탈 컨텐츠를 참조하고 있음
            // 아군이면, 상대 유저의 캐릭터를 
            // 상대면, 기계를 타게팅 

            // 아직 시작 아니라면, null 반환 
            if (!Plunder.Plunder.Instance.IsStarted)
                return null;
            
            Transform target;
            if (isMine)
            {
                SceneData.Instance.Plunder.GetTargetUserData(out var userInDate, out _, out _);
                var enemyArmy = Army.Other[userInDate] as ArmyPlunder;
                if (enemyArmy == null || enemyArmy.Character == null)
                    return null;
                
                target = enemyArmy.Character.transform;
            }
            else
            {
                target = Plunder.Plunder.Instance.Machine.transform;
            }

            return target;
        }
    }
}
