﻿using System.Collections.Generic;
using InGame.Data;
using InGame.Data.Scene;
using InGame.Raid;
using UnityEngine;

namespace InGame.Controller
{
    public class RaidTargetControl : TargetControl
    {
        public override Transform GetTarget(float range)
        {
            // 아직 시작 아니라면, null 반환 
            if (!Raid.Raid.Instance.IsStarted) return null;
            
            // 보스 죽었다면 타게팅 불가 
            if (Raid.Raid.Instance.boss.IsDied) return null;
            
            // 레이드 보스를 타게팅 
            // 각 위치별 다른 위치를 쏘게 해야 함 
            var targetIndex = UserTargetingPos.Other[OwnerControlHub.InDate]?.targetingIndex ?? 0;
            return Raid.Raid.Instance.boss.GetTargetingPos(targetIndex);
        }
    }
}
