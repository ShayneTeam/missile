﻿using UnityEngine;

namespace InGame.Controller
{
    public class EnemyModeTargetControl : TargetControl
    {
        public override Transform GetTarget(float range)
        {
            Transform target = null;
            var targetableEnemies = Enemies.Instance.GetTargetableEnemies();

            // 가장 최단거리 적 찾아내기 
            var smallestDistance = 99999f;
            foreach (var enemy in targetableEnemies)
            {
                var distance = Vector2.Distance(transform.position, enemy.transform.position);
                // ReSharper disable once InvertIf
                if (distance < range && distance < smallestDistance)
                {
                    target = enemy.transform;
                    smallestDistance = distance;
                }
            }

            return target;
        }
    }
}
