﻿using UnityEngine;

namespace InGame.Controller
{
    public abstract class TargetControl : InGameControl
    {
        public abstract Transform GetTarget(float range);
    }
}
