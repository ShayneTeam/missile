using System;
using System.Collections.Generic;
using System.Linq;
using Global;
using InGame.Data;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller.Princess
{
    public partial class PrincessControl : InGameControl
    {
        // NestedClass 정의
        public readonly TreasureLevel Level;

        public class TreasureLevel
        {
            /*
             * 공통
             */
            private readonly PrincessControl _parent;

            public TreasureLevel(PrincessControl parent)
            {
                _parent = parent;
            }
            
            /*
             * 이벤트
             */
            public event VoidDelegate OnLevelUp;
            
            /*
             * 함수
             */
             
            public int GetMax(string treasureIdx)
            {
                UserData.My.Princess.TryGetStateTreasure(treasureIdx, out _, out var currentGrade, out _);

                // 등급 
                var grade = currentGrade;
                
                // 최대 등급이라면
                if (_parent.Grade.IsMax(treasureIdx))
                {
                    // 마지막 등급으로 선택
                    grade = int.Parse(DataboxController.GetAllIdxes(Table.Princess, Sheet.Grade).Last());
                }
                
                // 등급의 최대 레벨 반환 
                return DataboxController.GetDataInt(Table.Princess, Sheet.Grade, grade.ToLookUpString(), Column.max_level);
            }

            public bool IsMax(string treasureIdx)
            {
                return GetRemainLevelToMax(treasureIdx) <= 0;
            }
            
            private int GetRemainLevelToMax(string treasureIdx)
            {
                UserData.My.Princess.TryGetStateTreasure(treasureIdx, out _, out _, out var level);
                var maxLevel = GetMax(treasureIdx);
                return maxLevel - level;
            }

            public bool HasEnoughCount(string treasureIdx, int count)
            {
                UserData.My.Princess.TryGetStateTreasure(treasureIdx, out var ownedCount, out _, out _);
                return ownedCount >= GetRequiredCount(treasureIdx, count);
            }
            
            public static int GetRequiredCount(string treasureIdx, int count)
            {
                return DataboxController.GetDataInt(Table.Princess, Sheet.girl, treasureIdx, Column.upgrade_cost) * count;
            }
            
            public bool CanUp(string treasureIdx, int count)
            {
                // 획득 여부 체크 
                var isCollected = UserData.My.Princess.TryGetStateTreasure(treasureIdx, out var ownedCount, out _, out _);
                if (!isCollected) return false;

                // 요구 레벨업 횟수가 최대 레벨을 넘어섰다면, 불가
                var remainLevelToMax = GetRemainLevelToMax(treasureIdx);
                if (remainLevelToMax < count) return false;

                // 보유 갯수 체크 
                return ownedCount >= GetRequiredCount(treasureIdx, count);
            }

            /// <summary>
            /// 업그레이드 할 수 있는 최대양 반환
            /// </summary>
            private int CalcUpCountMax(string treasureIdx)
            {
                var isCollected = UserData.My.Princess.TryGetStateTreasure(treasureIdx, out var ownedCount, out _, out _);
                if (!isCollected) return 0;

                var upCountMax = ownedCount / GetRequiredCount(treasureIdx, 1);
                var remainLevelToMax = GetRemainLevelToMax(treasureIdx);
                
                return Math.Min(upCountMax, remainLevelToMax);
            }

            public void Up(string treasureIdx, int count)
            {
                if (!CanUp(treasureIdx, count)) return;

                InternalUp(treasureIdx, count);
                
                // 이벤트 
                OnLevelUp?.Invoke();
            }

            private void InternalUp(string treasureIdx, int count)
            {
                // 필요 갯수 차감 
                var requiredCount = GetRequiredCount(treasureIdx, count);
                _parent.OwnerControlHub.Data.Princess.ConsumeTreasure(treasureIdx, requiredCount);

                // 업그레이드
                _parent.OwnerControlHub.Data.Princess.LevelUpTreasure(treasureIdx, count);

                _parent.OwnerControlHub.Data.Princess.TryGetStateTreasure(treasureIdx, out _, out _, out var level);
                WebLog.Instance.ThirdPartyLog("equipment_upgrade", new Dictionary<string, object>
                {
                    { "type", "treasure_upgrade" },
                    { "treasure_idx", treasureIdx },
                    { "treasure_level", level.ToLookUpString() },
                });
            }

            public bool UpBatch()
            {
                // 하나라도 레벨업 했는가
                var hasUpAny = false;
                
                var allIdxes = DataboxController.GetAllIdxes(Table.Princess, Sheet.girl);
                foreach (var idx in allIdxes)
                {
                    // 레벨업 가능한 최대갯수로 진행 
                    var upCount = CalcUpCountMax(idx);
                    if (upCount <= 0) continue;
                    
                    InternalUp(idx, upCount);
                    hasUpAny = true;
                }
                
                // 이벤트
                if (hasUpAny) OnLevelUp?.Invoke();

                return hasUpAny;
            }
            
            public int AverageOfAllCollected
            {
                get
                {
                    var allCollected = DataboxController.GetAllIdxes(Table.Princess, Sheet.girl);
                    // 획득 유물이 제로라면 강제 1 반환
                    if (!allCollected.Any()) return 1;  

                    var sumLevel = 0;
                    foreach (var idx in allCollected)
                    {
                        if (!_parent.OwnerControlHub.Data.Princess.TryGetStateTreasure(idx, out _, out _, out var level)) continue;
                        sumLevel += level;
                    }

                    // 0이 되지 않게 함 
                    return Math.Max(1, sumLevel / allCollected.Count);
                }
            }
        }
    }
}