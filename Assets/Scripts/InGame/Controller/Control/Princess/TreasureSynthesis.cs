using System.Collections.Generic;
using System.Linq;
using Global;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller.Princess
{
    public partial class PrincessControl : InGameControl
    {
        // NestedClass 정의
        public readonly TreasureSynthesis Synthesis;

        public class TreasureSynthesis
        {
            /*
             * 공통
             */
            private readonly PrincessControl _parent;

            public TreasureSynthesis(PrincessControl parent)
            {
                _parent = parent;
            }

            /*
             * 이벤트  
             */
            public static event IntDelegate OnSynthesized;

            /*
             * 함수
             */
            public static int RequiredCount => int.Parse(DataboxController.GetConstraintsData(Constraints.PRINCESS_TREASURE_SYNTHESIS_COUNT));

            private static bool IsLast(string treasureIdx)
            {
                var nextTreasureIdx = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.synthesis);
                return nextTreasureIdx == "" || nextTreasureIdx == "none";
            }

            public bool CanSynthesize(string treasureIdx)
            {
                if (!_parent.OwnerControlHub.Data.Princess.TryGetStateTreasure(treasureIdx, out var count, out _, out _))
                    return false;
                
                // 합성할 수 있는 갯수 미달 시, 불가
                if (count < RequiredCount) return false;

                // 가장 마지막 보물이라면, 불가
                if (IsLast(treasureIdx)) return false;

                // 모든 체크 통과 시, 가능 
                return true;
            }

            private bool InternalSynthesize(string treasureIdx, int amount, out string resultTreasureIdx)
            {
                resultTreasureIdx = "";

                if (!CanSynthesize(treasureIdx)) return false;

                // 합성 전에 필요 갯수 차감
                var requiredCount = RequiredCount;
                _parent.OwnerControlHub.Data.Princess.ConsumeTreasure(treasureIdx, requiredCount * amount);

                // 합성  
                var nextTreasureIdx = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, Column.synthesis);
                _parent.EarnTreasure(nextTreasureIdx, amount);

                resultTreasureIdx = nextTreasureIdx;
                return true;
            }

            public List<RewardData> SynthesizeBatch()
            {
                var needCount = RequiredCount;

                // 일괄합성 전 상태 기록
                var beforeStates = GetStateAllOwnedTreasures();

                var synthesisSum = 0;

                var allIdxes = DataboxController.GetAllIdxes(Table.Princess, Sheet.girl);
                foreach (var idx in allIdxes)
                {
                    if (!CanSynthesize(idx)) continue;

                    _parent.OwnerControlHub.Data.Princess.TryGetStateTreasure(idx, out var ownedCount, out _, out _);

                    var synthesisCount = ownedCount / needCount;

                    if (InternalSynthesize(idx, synthesisCount, out _))
                        synthesisSum += synthesisCount;
                }

                // 일괄합성 후 상태 
                var afterStates = GetStateAllOwnedTreasures();

                // 합성을 통해 갯수가 증가한 미사일만 결과에 넣음
                var results = new List<RewardData>();

                foreach (var afterState in afterStates)
                {
                    // 일괄합성 전에도 획득해있던 미사일인 경우,
                    if (beforeStates.TryGetValue(afterState.Key, out var beforeCount))
                    {
                        // 배치 후 갯수가 배치 전 갯수보다 적다면, 결과에 넣지 않음 
                        if (afterState.Value <= beforeCount) continue;
                    }

                    // 일괄합성 후 새로 생성된 미사일인 경우, 결과에 추가 됨 
                    var result = new RewardData
                    {
                        Idx = afterState.Key,
                        Type = RewardType.REWARD_GIRL,
                        Count = afterState.Value - beforeCount
                    };
                    results.Add(result);
                }

                // 이벤트 
                OnSynthesized?.Invoke(synthesisSum);

                // 결과
                return results;
            }

            private Dictionary<string, int> GetStateAllOwnedTreasures()
            {
                var states = new Dictionary<string, int>();

                var allIdxesEarned = _parent.OwnerControlHub.Data.Princess.AllTreasureIdxesCollected;
                foreach (var idx in allIdxesEarned)
                {
                    _parent.OwnerControlHub.Data.Princess.TryGetStateTreasure(idx, out var ownedCount, out _, out _);

                    states[idx] = ownedCount;
                }

                return states;
            }
        }
    }
}