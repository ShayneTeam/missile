using Global;
using InGame.Data;
using InGame.Global;
using Server;
using UnityEngine;

namespace InGame.Controller.Princess
{
    public partial class PrincessControl : InGameControl
    {
        // NestedClass 정의
        public readonly TreasureGrade Grade;

        public class TreasureGrade
        {
            /*
             * 공통
             */
            private readonly PrincessControl _parent;

            public TreasureGrade(PrincessControl parent)
            {
                _parent = parent;
            }
            
            /*
             * 함수
             */
            public bool IsMax(string treasureIdx)
            {
                UserData.My.Princess.TryGetStateTreasure(treasureIdx, out _, out var grade, out _);
                var maxGrade = DataboxController.GetAllIdxes(Table.Princess, Sheet.Grade).Count;
                return grade >= maxGrade;
            }
            
            public bool CanUp(string treasureIdx)
            {
                if (IsMax(treasureIdx)) return false;

                return HasEnoughCount(treasureIdx);
            }

            public bool HasEnoughCount(string treasureIdx)
            {
                var ownedChipsetCount = _parent.OwnerControlHub.Data.Items[RewardType.REWARD_CHIPSET];
                var needChipsetCount = GetRequiredChipsetCount(treasureIdx);

                return ownedChipsetCount >= needChipsetCount;
            }

            public static int GetRequiredChipsetCount(string treasureIdx)
            {
                return DataboxController.GetDataInt(Table.Princess, Sheet.girl, treasureIdx, Column.enchant_chip);
            }

            public void Up(string treasureIdx)
            {
                if (!CanUp(treasureIdx)) return;

                // 강화 전에 필요 칩셋 차감
                var requiredCount = GetRequiredChipsetCount(treasureIdx);
                _parent.OwnerControlHub.Data.Items[RewardType.REWARD_CHIPSET] -= requiredCount;

                // 강화 
                _parent.OwnerControlHub.Data.Princess.GradeUpTreasure(treasureIdx, 1);
            
                // 로그
                _parent.OwnerControlHub.Data.Princess.TryGetStateTreasure(treasureIdx, out _, out var grade, out _);
                ServerLogger.LogItem(RewardType.REWARD_CHIPSET, $"{LogEvent.GradeUpTreasure}_{grade.ToLookUpString()}", -requiredCount);
            
                // 바로 저장 
                UserData.My.SaveToServer();
            }
        }
    }
}