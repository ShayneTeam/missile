﻿using System;
using System.Collections.Generic;
using System.Linq;
using Databox.Dictionary;
using Global;
using InGame.Data;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller.Princess
{
    public partial class PrincessControl : InGameControl
    {
        // NestedClass 초기화
        public PrincessControl()
        {
            Level = new TreasureLevel(this);
            Grade = new TreasureGrade(this);
            Synthesis = new TreasureSynthesis(this);
        }
        
        public bool IsLocked => OwnerControlHub.Data.Princess.IsLocked;
        

        private void Start()
        {
            RaidController.OnVictory += OnVictoryRaid;

            OwnerControlHub.Data.Princess.OnEarned += OnChangedData;
            OwnerControlHub.PrincessController.Level.OnLevelUp += OnLevelUp;
            OwnerControlHub.Data.Princess.OnGradeUp += OnChangedData;
            OwnerControlHub.Data.Princess.OnCollectedCostume += OnCollectedCostume;
        }

        private void OnDestroy()
        {
            RaidController.OnVictory -= OnVictoryRaid;

            OwnerControlHub.Data.Princess.OnEarned -= OnChangedData;
            OwnerControlHub.PrincessController.Level.OnLevelUp -= OnLevelUp;
            OwnerControlHub.Data.Princess.OnGradeUp -= OnChangedData;
            OwnerControlHub.Data.Princess.OnCollectedCostume -= OnCollectedCostume;
        }
        
        /*
         * 어빌리티
         */

        #region Abilities
        
        private static SortedSet<string> _set;

        public event VoidDelegate OnChangedAbilities;
        
        private void OnLevelUp()
        {
            OnChangedAbilities?.Invoke();
        }

        private void OnChangedData(string treasureIdx)
        {
            OnChangedAbilities?.Invoke();
        }
        
        private void OnCollectedCostume(string costumeIdx)
        {
            OnChangedAbilities?.Invoke();
        }
        
        public static IEnumerable<string> GetAllAbilitiesType()
        {
            _set ??= new SortedSet<string>();
            
            // 이미 한 번 세팅했으면 바로 반환 
            // 바뀌지 않은 데이터값 반환이라 재계산 필요 없음
            if (_set.Count > 0) return _set;
            
            var allIdxes = DataboxController.GetAllIdxes(Table.Princess, Sheet.girl);
            foreach (var treasureIdx in allIdxes)
            {
                var ab1Idx = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, "a_aidx");
                var ab2Idx = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, "b_aidx");

                var ab1Type = AbilityControl.GetType(ab1Idx);
                var ab2Type = AbilityControl.GetType(ab2Idx);

                _set.Add(ab1Type);
                _set.Add(ab2Type);
            }

            return _set;
        }

        public OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
            // 인덱스 기준으로 정렬
            var abIdxAndValue = new SortedList<string, float>(); // idx, value

            // 보유 보물
            var allIdxesCollected = OwnerControlHub.Data.Princess.AllTreasureIdxesCollected;
            foreach (var treasureIdx in allIdxesCollected)
            {
                // 보유효과 1
                GetAbility1IdxAndValue(treasureIdx, out var ab1Idx, out var ab1Value);

                if (abIdxAndValue.ContainsKey(ab1Idx))
                    abIdxAndValue[ab1Idx] += ab1Value;
                else
                    abIdxAndValue[ab1Idx] = ab1Value;
                
                // 보유효과 2
                GetAbility2IdxAndValue(treasureIdx, out var ab2Idx, out var ab2Value);

                if (abIdxAndValue.ContainsKey(ab2Idx))
                    abIdxAndValue[ab2Idx] += ab2Value;
                else
                    abIdxAndValue[ab2Idx] = ab2Value;
            }
            
            // 보유 코스튬
            foreach (var idx in OwnerControlHub.Data.Princess.AllCostumeIdxesCollected)
            {
                // 어빌리티 인덱스 가져오기 
                var abilityIdx = DataboxController.GetDataString(Table.Costume, Sheet.princess, idx, "c_aidx");
                if (string.IsNullOrEmpty(abilityIdx)) continue;
                
                var abValue = AbilityControl.GetValue(abilityIdx);
                
                if (abIdxAndValue.ContainsKey(abilityIdx)) abIdxAndValue[abilityIdx] += abValue; else abIdxAndValue[abilityIdx] = abValue;
            }

            // 동일한 어빌리티 타입을 가지는 인덱스들의 밸류값 총합 계산 
            var abTypeAndValue = new OrderedDictionary<string, float>(); // type, value 
            AbilityControl.MergeByType(ref abTypeAndValue, abIdxAndValue);

            return abTypeAndValue;
        }
        
        private static void GetAbilityIdxAndValue(string treasureIdx, string abIdxColumn, string weightColumnName, int level, out string abIdx, out float finalizedValue)
        {
            abIdx = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, abIdxColumn);

            // 어빌리티 기본 밸류  
            var abilityValue = AbilityControl.GetValue(abIdx);

            // 가중치 
            var weightColumn = DataboxController.GetDataString(Table.Princess, Sheet.girl, treasureIdx, weightColumnName);
            var weight = DataboxController.GetDataFloat(Table.Princess, Sheet.gain, level.ToLookUpString(), weightColumn, 1f);

            // 최종 어빌리티 값 
            finalizedValue = abilityValue * weight;
        }

        public void GetAbility1IdxAndValue(string treasureIdx, out string abIdx, out float abValue)
        {
            OwnerControlHub.Data.Princess.TryGetStateTreasure(treasureIdx, out _, out _, out var level);
            GetAbilityIdxAndValue(treasureIdx, "a_aidx", "a_aidx_gain", level, out abIdx, out abValue);
        }
        
        public void GetAbility2IdxAndValue(string treasureIdx, out string abIdx, out float abValue)
        {
            OwnerControlHub.Data.Princess.TryGetStateTreasure(treasureIdx, out _, out _, out var level);
            GetAbilityIdxAndValue(treasureIdx, "b_aidx", "b_aidx_gain", level, out abIdx, out abValue);
        }

        #endregion

        private void OnVictoryRaid()
        {
            if (!OwnerControlHub.Data.Princess.IsLocked) return;
            
            // 레이드 승리 시, 공주 잠금 해제 
            OwnerControlHub.Data.Princess.Unlock();
        }
        
        public void EarnTreasure(string treasureIdx, int amount)
        {
            // 획득 
            OwnerControlHub.Data.Princess.EarnTreasure(treasureIdx, amount);
        }
        
        /*
         * 코스튬
         */

        #region Costume

        public bool IsWornCostume(string costumeIdx)
        {
            return OwnerControlHub.Data.Princess.WornCostume == costumeIdx;
        }

        #endregion
    }
}