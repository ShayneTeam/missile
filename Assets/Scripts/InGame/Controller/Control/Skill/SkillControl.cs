﻿using System.Collections.Generic;
using Global;
using Global.Extensions;
using InGame.Controller.Mode;
using InGame.Global;
using MEC;
using Server;
using UnityEngine;

namespace InGame.Controller
{
    /*
     * 인게임로직과 데이터 간의 상호동작 관리 
     */
    public abstract class SkillControl : InGameControl, IUseSkillMine
    {
        public event StringDelegate OnUseSkill;
        
        public virtual bool IsUnlocked(string skillIdx)
        {
            // 에디터용 치트
            if (ExtensionMethods.IsUnityEditor())
            {
                var testAllUnlockedSkills = DataboxController.GetDataBool(Table.Cheat, Sheet.Cheat, Row.Test, Column.all_unlocked_skills);
                if (testAllUnlockedSkills)
                    return true;
            }
            
            // 잠금 타입 
            var unlockType = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, "unlock_type", "missile");
            switch (unlockType)
            {
                case "missile":
                    // 미사일 획득했는지 체크 
                    var unlockMissileIdx = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, "unlock_missile_idx", "10012");
                    return OwnerControlHub.Data.Missiles.IsEarned(unlockMissileIdx);

                case "d_relic":
                    // 모든 종류의 유물을 획득해야만 스킬 활성화 
                    return OwnerControlHub.DiabloRelicController.HasAllCollected();
                
                case "girl":
                    return !OwnerControlHub.PrincessController.IsLocked;
            }

            return false;
        }

        public bool CanUseSkill()
        {
            // 캐릭터 없으면 스킬 사용하지 않음 
            if (!CheckToHasArmy()) return false;

            // 타겟이 한 마리도 없으면 스킬 사용하지 않음 
            const float infinityRange = 9999f;
            var target = OwnerControlHub.TargetController.GetTarget(infinityRange);
            if (target == null) return false;

            return true;
        }

        protected virtual bool CheckToHasArmy()
        {
            return Army.Other[OwnerControlHub.InDate].Character != null;
        }

        public bool UseSkill(string skillIdx)
        {
            if (!CanUseSkill()) return false;

            // 스킬 사용 타임스탬프 찍기 
            StampSkillTimestamp(skillIdx);

            // 스킬 사용 캐릭터 말풍선 
            ShowSpeechBubble(skillIdx);

            // 어빌리티 : 스킬 쿨타임 캔슬
            var abCoolTimeCancelChance = OwnerControlHub.AbilityController.GetUnifiedValue("SKILL_COOLTIME_CANCEL_RATE");
            var dice = Random.Range(0.01f, 100f);
            var cancelCoolTime = dice <= abCoolTimeCancelChance;
            if (ExtensionMethods.IsUnityEditor())
            {
                var testAllUnlockedSkills = DataboxController.GetDataBool(Table.Cheat, Sheet.Cheat, Row.Test, Column.cancel_cooltime);
                cancelCoolTime = testAllUnlockedSkills || cancelCoolTime;
            }
            if (cancelCoolTime)
            {
                // 스킬 지속시간 기다린 후, 타임스탬프 초기화 
                // 코루틴이므로, 게임 꺼지면 타임스탬프 초기화 안됨
                Timing.RunCoroutine(_ResetSkillTimestamp(skillIdx, GetDurationTimeSec(skillIdx)).CancelWith(gameObject));
            }
            
            // 이벤트 
            OnUseSkill?.Invoke(skillIdx);

            return true;
        }

        protected virtual void ShowSpeechBubble(string skillIdx)
        {
            var scriptIdx = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, Column.UseScriptId);

            var character = Army.Other[OwnerControlHub.InDate].Character;
            character.ShowSpeechBubble(scriptIdx);
        }

        private IEnumerator<float> _ResetSkillTimestamp(string skillIdx, float duration)
        {
            yield return Timing.WaitForSeconds(duration);
            
            ResetSkillTimestamp(skillIdx);
        }
        
        private long GetElapsedTimeSec(string idx)
        {
            var timestamp = GetSkillTimestamp(idx);
            return ServerTime.Instance.NowSec - timestamp;
        }

        public bool IsCoolTime(string idx, float coolTimeSec)
        {
            return GetRemainCoolTimeSec(idx, coolTimeSec) > 0;
        }

        public bool IsActivating(string idx)
        {
            return GetRemainDurationTimeSec(idx) > 0;
        }

        public long GetRemainDurationTimeSec(string idx)
        {
            // 한 번도 사용 안해서 타임스탬프 없다면, 바로 사용 가능 
            var timestamp = GetSkillTimestamp(idx);
            if (timestamp == 0)
                return 0;

            var elapsedTimeSec = GetElapsedTimeSec(idx);
            var durationTimeSec = GetDurationTimeSec(idx);

            var remainDurationTimeSec = durationTimeSec - elapsedTimeSec;

            return remainDurationTimeSec;
        }

        public static int GetDurationTimeSec(string idx)
        {
            return DataboxController.GetDataInt(Table.Skill, Sheet.Skill, idx, "duration_time");
        }

        public float GetRemainCoolTimeSec(string idx, float coolTimeSec)
        {
            // 한 번도 사용 안해서 타임스탬프 없다면, 바로 사용 가능
            var timestamp = GetSkillTimestamp(idx);
            if (timestamp == 0)
                return 0;
            
            if (ExtensionMethods.IsUnityEditor())
            {
                // 테스트. 스킬 노쿨타임 시, 바로 사용 가능 
                var testSkillNoCool = DataboxController.GetDataBool(Table.Cheat, Sheet.Cheat, Row.Test, Column.skill_no_cool_time);
                if (testSkillNoCool)
                    return 0;
            }

            var elapsedTimeSec = (float)GetElapsedTimeSec(idx);

            return coolTimeSec - elapsedTimeSec;
        }

        // TODO. 리팩토링 필요한 부분 
        // T999, 공주는 별개의 계산식을 자신의 스킬 클래스에서 구현하고 있음 
        // 그외의 다른 스킬들의 계산식은 이 클래스를 참조하고 있는데, 
        // 다형성으로 처리시키든, 혹은 아예 별개의 독립적 처리 클래스로 빼는게 나음 
        // 이 클래스는 모든 클래스에서 공통적으로 수행하는 것들만 들어있어야 함  
        #region TODO

        public int GetSkillLevel(string skillIdx)
        {
            // 테스트. 스킬 레벨 
            if (ExtensionMethods.IsUnityEditor())
            {
                var testSkillLevel = DataboxController.GetDataInt(Table.Cheat, Sheet.Cheat, Row.Test, Column.all_skills_level);
                if (testSkillLevel > 0)
                    return testSkillLevel;
            }
            
            var type = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, "type");
            var abilityLevelName = type + "_LEVEL";
            var level = (int)OwnerControlHub.AbilityController.GetUnifiedValue(abilityLevelName); 
	        
            return level;
        }

        public float GetSkillLevelValue(string idx)
        {
            // 스킬 기본 밸류
            var skillBaseValue = DataboxController.GetDataInt(Table.Skill, Sheet.Skill, idx, "value_1", 1);

            // 스킬 레벨 가중치 
            var skillLevelWeight = GetSkillLevelWeight(idx);

            // 스킬 레벨 밸류 
            return skillBaseValue * skillLevelWeight;
        }

        private float GetSkillLevelWeight(string idx)
        {
            var abilityMissileLevel = GetSkillLevel(idx);

            return GetLevelWeight(idx, abilityMissileLevel);
        }

        #endregion

        public static float GetLevelWeight(string skillIdx, int level)
        {
            var weightColumn = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, "value_1_gain");
            return DataboxController.GetDataFloat(Table.Skill, Sheet.gain, level.ToLookUpString(), weightColumn, 1f);
        }

        /*
         * 모드별 구현이 달라지는 함수들  
         */
        public abstract void StampSkillTimestamp(string skillIdx);
        public abstract long GetSkillTimestamp(string skillIdx);
        public abstract void ResetSkillTimestamp(string skillIdx);
        public abstract float GetSkillCoolTimeSec(string skillIdx);
    }   
}