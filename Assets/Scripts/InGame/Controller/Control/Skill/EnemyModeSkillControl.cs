﻿using InGame.Controller.Mode;

namespace InGame.Controller
{
    public class EnemyModeSkillControl : SkillControl
    {
        /*
         * PvE 용 스킬 사용 처리 
         */
        public override void StampSkillTimestamp(string skillIdx)
        {
            (EnemyMode.Behaviour as IUseSkillMine)?.StampSkillTimestamp(skillIdx);
        }

        public override long GetSkillTimestamp(string skillIdx)
        {
            return (EnemyMode.Behaviour as IUseSkillMine)?.GetSkillTimestamp(skillIdx) ?? 0;
        }

        public override void ResetSkillTimestamp(string skillIdx)
        {
            (EnemyMode.Behaviour as IUseSkillMine)?.ResetSkillTimestamp(skillIdx);
        }

        public override float GetSkillCoolTimeSec(string skillIdx)
        {
            return (EnemyMode.Behaviour as IUseSkillMine)?.GetSkillCoolTimeSec(skillIdx) ?? 0;
        }
    }
}