﻿using InGame.Controller.Mode;

namespace InGame.Controller
{
    public class PlunderSkillControl : SkillControl
    {
        /*
         * PvP 용 스킬 사용 처리 
         */
        public override void StampSkillTimestamp(string skillIdx)
        {
            PlunderMode.Instance.StampSkillTimestamp(OwnerControlHub.InDate, skillIdx);
        }

        public override long GetSkillTimestamp(string skillIdx)
        {
            return PlunderMode.Instance.GetSkillTimestamp(OwnerControlHub.InDate, skillIdx);
        }

        public override void ResetSkillTimestamp(string skillIdx)
        {
            PlunderMode.Instance.ResetSkillTimestamp(OwnerControlHub.InDate, skillIdx);
        }

        public override float GetSkillCoolTimeSec(string skillIdx)
        {
            return PlunderMode.Instance.GetSkillCoolTimeSec(OwnerControlHub.InDate, skillIdx);
        }
    }
}