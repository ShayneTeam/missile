﻿using InGame.Controller.Mode;
using InGame.Raid;
using InGame.Skill;
using UnityEngine;

namespace InGame.Controller
{
    public class RaidSkillControl : SkillControl
    {
        public override void StampSkillTimestamp(string skillIdx)
        {
            RaidMode.Instance.StampSkillTimestamp(OwnerControlHub.InDate, skillIdx);
        }

        public override long GetSkillTimestamp(string skillIdx)
        {
            return RaidMode.Instance.GetSkillTimestamp(OwnerControlHub.InDate, skillIdx);
        }

        public override void ResetSkillTimestamp(string skillIdx)
        {
            RaidMode.Instance.ResetSkillTimestamp(OwnerControlHub.InDate, skillIdx);
        }

        public override float GetSkillCoolTimeSec(string skillIdx)
        {
            return RaidMode.Instance.GetSkillCoolTimeSec(OwnerControlHub.InDate, skillIdx);
        }

        public override bool IsUnlocked(string skillIdx)
        {
            switch (skillIdx)
            {
                // 자이언트, 융단폭격, T999 사용불가
                case SkillIdx.BigBeautifulMissile:
                case SkillIdx.Bombing:
                case SkillIdx.T999:
                    return false;
                
                // 다른 스킬들은 똑같이 사용 가능    
                default:
                    return base.IsUnlocked(skillIdx);
            }
        }
        
        public bool CanSpawnT999 => base.IsUnlocked(SkillIdx.T999);

        protected override void ShowSpeechBubble(string skillIdx)
        {
            // 레이드 모드선 스킬 쓴다고 말풍선 띄우지 않음 
        }

        protected override bool CheckToHasArmy()
        {
            // 나 먹히면 스킬 못 쓰게 해야 함
            return RaidUserSlot.Other[OwnerControlHub.InDate]?.IsAlive ?? false;
        }
    }
}