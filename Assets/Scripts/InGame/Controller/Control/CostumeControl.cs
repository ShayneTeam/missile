﻿using System;
using System.Collections.Generic;
using Databox.Dictionary;
using Global;
using InGame.Data;
using InGame.Global;
using Server;
using UnityEngine;

namespace InGame.Controller
{
	public class CostumeControl : InGameControl
    {
        public event VoidDelegate OnChangedAbilities;
	    
        private void Start()
        {
            OwnerControlHub.Data.Costume.OnEarned += OnChangedData;
        }

        private void OnDestroy()
        {
            OwnerControlHub.Data.Costume.OnEarned -= OnChangedData;
        }

        private void OnChangedData(string type)
        {
            OnChangedAbilities?.Invoke();
        }
        
        
        
        public void GetState(out string outHead, out string outSkin, out string outBody)
        {
            outHead = OwnerControlHub.Data.Costume.GetEquipped(UserData.CostumeType.head);
            outSkin = OwnerControlHub.Data.Costume.GetEquipped(UserData.CostumeType.skin);
            outBody = OwnerControlHub.Data.Costume.GetEquipped(UserData.CostumeType.body);
        }
        
        #region Abilities
                
        public OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
            var allAbilities = new List<string>();

            allAbilities.AddRange(GetAppliedAbilities(UserData.CostumeType.head));
            allAbilities.AddRange(GetAppliedAbilities(UserData.CostumeType.skin));
            allAbilities.AddRange(GetAppliedAbilities(UserData.CostumeType.body));

            // 인덱스 기준 정렬 
            var sortedByIdx = new SortedList<string, float>(); // idx, value
            AbilityControl.SortByIdx(ref sortedByIdx, allAbilities);
            
            // 타입 기준 병합  
            var sumValues = new OrderedDictionary<string, float>(); // type, value 
            AbilityControl.MergeByType(ref sumValues, sortedByIdx);
            return sumValues;
        }
        
        private List<string> GetAppliedAbilities(UserData.CostumeType type)
        {
            var appliedAbilities = new List<string>();

            // 획득한 코스튬 리스트 가져오기 
            var earnedList = OwnerControlHub.Data.Costume.GetAllEarned(type);
            foreach (var idx in earnedList)
            {
                // 해당 코스튬 타입만 선별
                var costumeType = DataboxController.GetDataString(Table.Costume, Sheet.costume, idx, "type");
                if (costumeType != type.ToEnumString())
                    continue;;
                
                // 어빌리티 인덱스 가져오기 
                var abilityIdx = DataboxController.GetDataString(Table.Costume, Sheet.costume, idx, "c_aidx");
                if (string.IsNullOrEmpty(abilityIdx))
	                continue;
                
                appliedAbilities.Add(abilityIdx);
            }

            return appliedAbilities;
        }

	    public OrderedDictionary<string, float> GetAppliedAbilitiesMerged(UserData.CostumeType type)
	    {
		    var allAbilities = new List<string>();

		    allAbilities.AddRange(GetAppliedAbilities(type));

		    // 인덱스 기준 정렬 
		    var sortedByIdx = new SortedList<string, float>(); // idx, value
		    AbilityControl.SortByIdx(ref sortedByIdx, allAbilities);

		    // 타입 기준 병합  
		    var sumValues = new OrderedDictionary<string, float>(); // type, value 
		    AbilityControl.MergeByType(ref sumValues, sortedByIdx);

		    return sumValues;
	    }

        #endregion





        #region Purchase

        public bool HasMoney(string idx)
        {
	        var cost = GetNeedCost(idx);

	        return OwnerControlHub.Data.Resources.uranium >= cost;
        }

        public static int GetNeedCost(string idx)
        {
	        return DataboxController.GetDataInt(Table.Costume, Sheet.costume, idx, "cost");
        }

        public bool CanPurchase(string idx)
        {
	        if (IsEarned(idx))
		        return false;

	        if (!HasMoney(idx))
		        return false;

	        return true;
        }

        public void Purchase(string idx)
        {
	        if (!CanPurchase(idx))
		        return;
		        
	        // 차감
	        var needCost = GetNeedCost(idx);
	        ResourceController.Instance.SubtractUranium(needCost, $"{LogEvent.Costume}_{idx}");
		        
	        // 구매
	        Earn(idx);
        }

        #endregion
        

        public void Equip(string costumeIdx)
        {
	        var type = GetCostumeType(costumeIdx);
	        OwnerControlHub.Data.Costume.Equip(type, costumeIdx);
        }

        public bool IsEarned(string costumeIdx)
        {
	        var type = GetCostumeType(costumeIdx);
	        return OwnerControlHub.Data.Costume.IsEarned(type, costumeIdx);
        }
        
        public bool IsEquipped(string costumeIdx)
        {
	        var type = GetCostumeType(costumeIdx);
	        return OwnerControlHub.Data.Costume.GetEquipped(type) == costumeIdx;
        }

        public string GetEquipped(UserData.CostumeType type)
        {
	        return OwnerControlHub.Data.Costume.GetEquipped(type);
        }
        
        public void Earn(string costumeIdx)
        {
	        var type = GetCostumeType(costumeIdx);
	        OwnerControlHub.Data.Costume.Earn(type, costumeIdx);
        }

        private static UserData.CostumeType GetCostumeType(string costumeIdx)
        {
	        var costumeType = DataboxController.GetDataString(Table.Costume, Sheet.costume, costumeIdx, "type");
	        return (UserData.CostumeType)Enum.Parse(typeof(UserData.CostumeType), costumeType);
        }

        public void GetCostumeSprite(out Sprite outHead, out Sprite outSkin, out Sprite outBody)
        {
	        GetState(out var headIdx, out var skinIdx, out var bodyIdx);

			GetCostumeSprite(headIdx, skinIdx, bodyIdx, out outHead, out outSkin, out outBody);   
        }

        public static void GetCostumeSprite(string headIdx, string skinIdx, string bodyIdx, out Sprite outHead, out Sprite outSkin, out Sprite outBody)
        {
	        var headResource = DataboxController.GetDataString(Table.Costume, Sheet.costume, headIdx, Column.resource);
	        var skinResource = DataboxController.GetDataString(Table.Costume, Sheet.costume, skinIdx, Column.resource);
	        var bodyResource = DataboxController.GetDataString(Table.Costume, Sheet.costume, bodyIdx, Column.resource);
        
	        outHead = AtlasController.GetSprite(headResource);
	        outSkin = AtlasController.GetSprite(skinResource);
	        outBody = AtlasController.GetSprite(bodyResource);
        }
    }
}