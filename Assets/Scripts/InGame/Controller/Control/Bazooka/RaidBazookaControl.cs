﻿using System.Globalization;
using Global;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller
{
    public class RaidBazookaControl : BazookaControl
    {
        // HACK. 레이드에서는 바주카의 업그레이드가 이루어지지 않는다 
        // 레이드 끝나고 스테이지로 돌아가면, RaidBazookaControl은 삭제되지 않고, InGameControlHub에 Component로 있게 되는데 
        // 이때 바주카 업그레이드가 되면 쓸데없이 두 번 어빌리티 갱신이 이뤄지게 됨 (어빌리티 갱신은 매우 비쌈)
        // 물론 모든 이벤트에 대해 아무 처리 안 하는 것이므로, 나중에 어빌리티가 아닌 다른 목적의 이벤트가 추가되면 주의해야 함
        protected override void RegisterEvents()
        {
            
        }

        protected override void UnregisterEvents()
        {
            
        }

        protected override float ClampShootCountPerSec(float shootCountPerSec)
        {
            return ClampShootCountPerSecStatic(shootCountPerSec);
        }

        public static float ClampShootCountPerSecStatic(float shootCountPerSec)
        {
            // 기본 제한 
            var shootCountMaxPerSec = DataboxController.GetConstraintsData("BAZOOKA_SPEED_MAX", "20");
            var clampedCount = Mathf.Clamp(shootCountPerSec, shootCountPerSec, float.Parse(shootCountMaxPerSec, CultureInfo.InvariantCulture));
            
            // 레이드 추가 제한
            var additiveNerf = float.Parse(DataboxController.GetConstraintsData(Constraints.RAID_ATK_SPEED_GAIN), CultureInfo.InvariantCulture);
            var nerfCount = clampedCount * (additiveNerf / 100f);
            
            return nerfCount;
        }
    }
}