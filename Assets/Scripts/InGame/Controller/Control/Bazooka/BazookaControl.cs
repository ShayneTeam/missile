﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Databox.Dictionary;
using Global;
using InGame.Data;
using InGame.Global;
using InGame.Skill;
using Server;
using UnityEngine;

namespace InGame.Controller
{
    public partial class BazookaControl : InGameControl, IChangedAbilities
    {
	    public event VoidDelegate OnChangedAbilities;
	    
	    public BazookaControl()
	    {
		    Parts = new PartsControl(this);
	    }
	    
	    private void Start()
	    {
		    RegisterEvents();

		    // AbilityControl과 강한 커플링
		    // BazookaControl이 런타임에 바뀌므로, 새로운 BazookaControl이 생성되면, 직접 AbilityControl에 자신을 등록하게 만든다 
		    // 이런 커플링 코드를 매우 싫어하지만, 여기서 안하면, 다른 모드 클래스에서 직접 따로따로 해줘야 함 
		    // 그렇게 하는 것보다, 여기서 심플하게 처리하는 게 낫기 때문에 이렇게 함 
		    OwnerControlHub.AbilityController.RegisterController(this);
	    }

	    private void OnDestroy()
	    {
		    UnregisterEvents();

		    OwnerControlHub.AbilityController.UnregisterController(this);
	    }

	    protected virtual void RegisterEvents()
	    {
		    OwnerControlHub.Data.Bazooka.OnEquipped += OnChangedData;
		    OwnerControlHub.Data.Bazooka.OnEnhance += OnChangedData;
		    OwnerControlHub.Data.Bazooka.OnEarned += OnChangedData;
		    OwnerControlHub.Data.Bazooka.OnLevelUp += OnLevelUp;
		    OwnerControlHub.Data.Bazooka.OnLevelUpParts += OnLevelUpParts;
	    }

	    protected virtual void UnregisterEvents()
	    {
		    OwnerControlHub.Data.Bazooka.OnEquipped -= OnChangedData;
		    OwnerControlHub.Data.Bazooka.OnEnhance -= OnChangedData;
		    OwnerControlHub.Data.Bazooka.OnEarned -= OnChangedData;
		    OwnerControlHub.Data.Bazooka.OnLevelUp -= OnLevelUp;
		    OwnerControlHub.Data.Bazooka.OnLevelUpParts -= OnLevelUpParts;
	    }

	    private void OnLevelUp(int count)
	    {
		    OnChangedAbilities?.Invoke();
	    }
	    
	    private void OnLevelUpParts(string idx, int count)
	    {
		    OnChangedAbilities?.Invoke();
	    }

	    private void OnChangedData()
	    {
		    OnChangedAbilities?.Invoke();
	    }

	    private void OnChangedData(string bazookaIdx)
	    {
		    OnChangedAbilities?.Invoke();
	    }
	    
	    public OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
	        // 인덱스 기준으로 정렬
	        var sortedByIdx = new SortedList<string, float>(); // idx, value
	        
	        // 획득 바주카
	        var allBazookaIdxes = DataboxController.GetAllIdxes(Table.Bazooka, Sheet.bazooka);
	        foreach (var bazookaIdx in allBazookaIdxes)
	        {
		        // 미획득 시 제외
		        if (!OwnerControlHub.Data.Bazooka.IsEarned(bazookaIdx))
			        continue;
		        
		        // 어빌리티 없다면, 스킵
		        if (!HasAbility(bazookaIdx))
			        continue;;

		        var abIdx = GetAbilityIdx(bazookaIdx);
		        var abValue = AbilityControl.GetValue(abIdx);
		        
		        if (sortedByIdx.ContainsKey(abIdx)) sortedByIdx[abIdx] += abValue; else sortedByIdx[abIdx] = abValue;
	        }
	        
	        // 획득 파츠 
	        var allPartsIdxes = DataboxController.GetAllIdxes(Table.Bazooka, Sheet.Parts);
	        foreach (var partsIdx in allPartsIdxes)
	        {
		        // 언락돼있는 파츠는 스킵 
		        if (!Parts.IsUnlock(partsIdx))
			        continue;

		        var abIdx = PartsControl.GetPartsAbilityIdx(partsIdx);
		        var abValue = GetFinalizedAbilityValue(partsIdx);
		        
		        if (sortedByIdx.ContainsKey(abIdx)) sortedByIdx[abIdx] += abValue; else sortedByIdx[abIdx] = abValue;
	        }
            
            // 동일한 어빌리티 타입을 가지는 인덱스들의 밸류값 총합 계산 
            var mergedValuesByType = new OrderedDictionary<string, float>(); // type, value 
            AbilityControl.MergeByType(ref mergedValuesByType, sortedByIdx);
					    
            return mergedValuesByType;
        }

	    public static bool HasAbility(string bazookaIdx)
	    {
		    var abilityIdx = GetAbilityIdx(bazookaIdx);
		    
		    return !string.IsNullOrEmpty(abilityIdx);
	    }

	    public float GetFinalizedAbilityValue(string partsIdx)
	    {
		    var level = OwnerControlHub.Data.Bazooka.GetPartsLevel(partsIdx);

		    var abilityIdx = PartsControl.GetPartsAbilityIdx(partsIdx);
		    var abilityValue = DataboxController.GetDataFloat(Table.Ability, Sheet.Ability, abilityIdx, Column.value);
		    var abilityWeight = PartsControl.GetLevelWeight(partsIdx, level);

		    return abilityValue * abilityWeight;
	    }


	    #region Upgrade

	    public bool IsMaxLevel()
	    {
		    return GetRemainLevelToMax() <= 0;
	    }
	    
	    private int GetRemainLevelToMax()
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out _, out _, out var level);

		    var maxLevelStr = DataboxController.GetConstraintsData("BAZOOKA_LEVEL_MAX", "9999");
		    var maxLevel = int.Parse(maxLevelStr);

		    return maxLevel - level;
	    }

	    public double GetRequiredMineralToUpgrade(int count)
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out _, out _, out var currentLevel);
		    
		    // 요청된 레벨 횟수만큼 레벨별 필요 미네랄 양 합산
		    var sum = 0d;
		    for (var i = 0; i < count; i++)
		    {
			    var level = currentLevel + i;

			    var needMineral = DataboxController.GetDataDouble(Table.Bazooka, Sheet.level, level.ToLookUpString(), "bazooka_mineral");
			    var abilityCostDownModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_BAZOOKA");

			    sum += needMineral / (1 + (abilityCostDownModifiedPer / 100d));
		    }

		    return sum;
	    }

	    public bool CanUpgrade(int count)
	    {
		    // 요구 레벨업 횟수가 최대 레벨을 넘어섰다면 false
		    var remainLevelToMax = GetRemainLevelToMax();
		    if (count > remainLevelToMax)
			    return false;
		    
		    // 보유 미네랄이 부족하면 false 
		    var requiredMineral = GetRequiredMineralToUpgrade(count);
		    var userMineral = OwnerControlHub.Data.Resources.mineral;
		    
		    return userMineral >= requiredMineral;
	    }
		
	    public void Upgrade(int count)
	    {
		    if (!CanUpgrade(count))
			    return;

		    // 미네랄 차감 
		    var requiredMineral = GetRequiredMineralToUpgrade(count);
		    ResourceController.Instance.SubtractMineral(requiredMineral);

		    // 업그레이드 
		    OwnerControlHub.Data.Bazooka.LevelUp(count);
		    
		    // 로그 
		    OwnerControlHub.Data.Bazooka.GetState(out _, out _, out var level);
		    WebLog.Instance.ThirdPartyLog("bazooka_upgrade", new Dictionary<string, object> { { "level", level.ToLookUpString() } });

		    WebLog.Instance.ThirdPartyLog($"equipment_upgrade", new Dictionary<string, object>
		    {
			    { "type", "bazooka_upgrade" },
			    { "level", level.ToLookUpString() },
		    });
	    }

	    #endregion


	    #region Enhance

	    public bool IsMaxGrade()
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out _, out var grade, out _);

		    var gradeMaxStr = DataboxController.GetConstraintsData("BAZOOKA_GRADE_MAX", "20");
		    return grade >= int.Parse(gradeMaxStr);
	    }

	    public bool IsEnoughChipsetForEnhance()
	    {
		    var ownedChipsetCount = OwnerControlHub.Data.Items[RewardType.REWARD_CHIPSET];
		    var needChipsetCount = GetNeedChipsetCountForEnhance();

		    return ownedChipsetCount >= needChipsetCount;
	    }

	    public bool CanEnhance()
	    {
		    if (IsMaxGrade())
			    return false;
		    
		    if (!IsEnoughChipsetForEnhance())
			    return false;

		    return true;
	    }

	    public int GetNeedChipsetCountForEnhance()
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out _, out var grade, out _);
		    return DataboxController.GetDataInt(Table.Bazooka, Sheet.Grade, grade.ToLookUpString(), "upgrade_cost");   
	    }

	    public void Enhance()
	    {
		    if (!CanEnhance())
			    return;
            
		    // 강화 전에 필요 칩셋 차감
		    var needChipset = GetNeedChipsetCountForEnhance();
		    OwnerControlHub.Data.Items[RewardType.REWARD_CHIPSET] -= needChipset;

		    // 강화 
		    OwnerControlHub.Data.Bazooka.Enhance(1);
		    
		    // 로그
		    OwnerControlHub.Data.Bazooka.GetState(out _, out var grade, out _);
		    ServerLogger.LogItem(RewardType.REWARD_CHIPSET, $"{LogEvent.BazookaEnhance}_{grade.ToLookUpString()}", -needChipset);
		    
		    // 바로 저장 
		    UserData.My.SaveToServer();
	    }

	    #endregion

	    public double GetFinalizedDamage()
	    {
		    // 기본 데미지 
		    var baseDamage = GetBaseDamage();
            
		    // 어빌리티 
		    var abBazookaDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.BAZOOKA_DAMAGE);
		    var abAllDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ALL_DAMAGE);
		    var abInfStoneMindPer = OwnerControlHub.InfStoneController.Stone.MindStoneDamagePer;

		    var abilityDamage = baseDamage * (1d + (abBazookaDamagePer + abAllDamagePer + abInfStoneMindPer) / 100d);

		    // 인피니티 스톤 : 모든 데미지 배율 
		    var infStoneAllDamageFactor = OwnerControlHub.InfStoneController.Helmet.AllDamageFactor;
		    var infStoneDamage = abilityDamage * infStoneAllDamageFactor;
		    
		    // 공주 : 데미지 증폭 
		    var abDamageMorePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.BAZOOKA_DAMAGE_MORE);
		    var princessDamage = infStoneDamage * (1d + abDamageMorePer / 100d);
		    
		    // 고대 : 데미지+
		    var abAllDamagePlusPer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ALL_DAMAGE_PLUS);
		    var abDamagePlusPer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.BAZOOKA_DAMAGE_PLUS);
		    var ancientDamage = princessDamage * (1d + (abAllDamagePlusPer + abDamagePlusPer) / 100d);
		    
		    return ancientDamage;
	    }

	    public float GetBaseCriticalChance()
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out _, out var grade, out _);
		    return DataboxController.GetDataFloat(Table.Bazooka, Sheet.Grade, grade.ToLookUpString(), "base_critical", 2f);
	    }

	    public float GetCriticalChanceAppliedAbility()
	    {
		    // 치명타 최종 확률 연산
		    var baseCriticalChance = GetBaseCriticalChance();
		    var abilityBazookaCriticalChance = OwnerControlHub.AbilityController.GetUnifiedValue("BAZOOKA_CRI_RATE");
		    var abilityAllCriticalChance = OwnerControlHub.AbilityController.GetUnifiedValue("ALL_CRITICAL_RATE");
		    
		    return baseCriticalChance + abilityBazookaCriticalChance + abilityAllCriticalChance;
	    }

	    public float GetFinalizedCriticalChance()
	    {
		    var criticalChanceAppliedAbility = GetCriticalChanceAppliedAbility();
		    
		    // 벌크업 치명타 확률 보너스 
		    var isOnBulkUp = OwnerControlHub.SkillController.IsActivating(SkillIdx.BulkUp);
		    var bulkUpCriticalChanceBonus = isOnBulkUp ? SkillBulkUp.GetCriticalChanceBonus() : 0;

		    return criticalChanceAppliedAbility + bulkUpCriticalChanceBonus;
	    }

	    public float GetBaseCriticalDamageModifiedPer()
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out _, out var grade, out _);
		    return DataboxController.GetDataFloat(Table.Bazooka, Sheet.Grade, grade.ToLookUpString(), "base_critical_per", 125f);
	    }

	    public float GetCriticalDamagePerAppliedAbility()
	    {
		    // 치명타 데미지 반영
		    var baseCriticalDamagePer = GetBaseCriticalDamageModifiedPer();
		    var abilityBazookaCriticalDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue("BAZOOKA_CRI_DAMAGE");
		    var abilityAllCriticalDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue("ALL_CRITICAL_DAMAGE");
		    
		    return baseCriticalDamagePer + abilityBazookaCriticalDamagePer + abilityAllCriticalDamagePer;
	    }

	    public double ApplyCriticalDamage(double damage)
	    {
		    var criticalDamagePerAppliedAbility = GetCriticalDamagePerAppliedAbility();
		    
		    var finalizedDamage = damage * (1f + criticalDamagePerAppliedAbility / 100d);
		    
		    // 벌크업 치명타 데미지 보너스 
		    var isOnBulkUp = OwnerControlHub.SkillController.IsActivating(SkillIdx.BulkUp);
		    var bulkUpCriticalDamageBonus = isOnBulkUp ? SkillBulkUpFinder.Other[OwnerControlHub.InDate].Skill.GetCriticalDamageBonus() : 0;
		    
		    finalizedDamage += bulkUpCriticalDamageBonus;

		    return finalizedDamage;
	    }

	    private static double GetDamageLevelWeight(int grade, int level)
	    {
		    var levelWeightColumn = DataboxController.GetDataString(Table.Bazooka, Sheet.Grade, grade.ToLookUpString(), "dmg_level_gain");
		    return DataboxController.GetDataDouble(Table.Bazooka, Sheet.gain, level.ToLookUpString(), levelWeightColumn, 1);
	    }

	    public double GetBaseDamage()
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out _, out var grade, out var level);

		    return InternalGetBaseDamage(grade, level);
	    }

	    public double GetBaseDamageForNextLevel()
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out _, out var grade, out var level);

		    return InternalGetBaseDamage(grade, level + 1);
	    }

	    private double InternalGetBaseDamage(int grade, int level)
	    {
		    var damageByGrade = (double)DataboxController.GetDataInt(Table.Bazooka, Sheet.Grade, grade.ToLookUpString(), "base_damage", 100);
		    var damageRate = GetSumDamageRate();
		    var levelWeight = GetDamageLevelWeight(grade, level);

		    return damageByGrade * damageRate * levelWeight;
	    }

	    public float GetSumDamageRate()
	    {
		    var allIdxes = DataboxController.GetAllIdxes(Table.Bazooka, Sheet.bazooka);

		    var result = 0f;
		    foreach (var idx in allIdxes)
		    {
			    if (OwnerControlHub.Data.Bazooka.IsEarned(idx)) 
				    result += DataboxController.GetDataInt(Table.Bazooka, Sheet.bazooka, idx, Column.damage_rate);
		    }

		    return result;
	    }

	    public float CalcShootCounterPerSec()
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out _, out var grade, out _);
		    var baseSpeed = DataboxController.GetDataFloat(Table.Bazooka, Sheet.Grade, grade.ToLookUpString(), "base_speed", 1f);
		    var speedRate = GetBestSpeedRate();

		    // 초당 발사 갯수
		    var shootCountPerSec = baseSpeed * speedRate;

		    // 어빌리티 
		    var abilityBazookaSpeedModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("BAZOOKA_SPEED"); // 높을수록 초당 발사갯수가 많아짐 
		    shootCountPerSec *= (1f + (abilityBazookaSpeedModifiedPer / 100f));

		    return shootCountPerSec;
	    }

	    public float GetBestSpeedRate()
	    {
		    var allIdxes = DataboxController.GetAllIdxes(Table.Bazooka, Sheet.bazooka);

		    var max = 0f;
		    foreach (var idx in allIdxes)
		    {
			    if (OwnerControlHub.Data.Bazooka.IsEarned(idx)) 
				    max = Math.Max(max, DataboxController.GetDataFloat(Table.Bazooka, Sheet.bazooka, idx, "speed_rate", 1f));
		    }

		    return max;
	    }

	    public float DefaultShootIntervalSec()
	    {
		    var shootCountPerSec = CalcShootCounterPerSec();
		    return ToShootIntervalSecFrom(shootCountPerSec);
	    }

	    public float ToShootIntervalSecFrom(float shootCountPerSec)
	    {      
		    // 초당 발사 개수 제한
		    shootCountPerSec = ClampShootCountPerSec(shootCountPerSec);

		    // 한 발 당, 발사 시간 
		    var shootIntervalSec = 1 / shootCountPerSec;
	
		    // 반환  
		    return shootIntervalSec;
	    }

	    protected virtual float ClampShootCountPerSec(float shootCountPerSec)
	    {
		    var shootCountMaxPerSec = DataboxController.GetConstraintsData("BAZOOKA_SPEED_MAX", "20");
		    return Mathf.Clamp(shootCountPerSec, shootCountPerSec, float.Parse(shootCountMaxPerSec, CultureInfo.InvariantCulture));
	    }

	    public Sprite GetMySkinSprite()
	    {
		    OwnerControlHub.Data.Bazooka.GetState(out var equippedBazookaIdx, out _, out _);

		    return GetSkinSprite(equippedBazookaIdx);
	    }

	    public static Sprite GetSkinSprite(string bazookaIdx)
	    {
		    var bazookaResource = DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, bazookaIdx, "resource_id");
        
		    return AtlasController.GetSprite(bazookaResource);
	    }

	    public void Earn(string bazookaIdx)
	    {
		    if (OwnerControlHub.Data.Bazooka.IsEarned(bazookaIdx)) 
			    return;
		    
		    OwnerControlHub.Data.Bazooka.Earn(bazookaIdx);
	    }

	    public static string GetAbilityIdx(string bazookaIdx)
	    {
		    return DataboxController.GetDataString(Table.Bazooka, Sheet.bazooka, bazookaIdx, Column.bz_aidx);
	    }

	    public int Level
	    {
		    get
		    {
			    OwnerControlHub.Data.Bazooka.GetState(out _, out _, out var level); 
			    return level;
		    }
	    }
    }
}