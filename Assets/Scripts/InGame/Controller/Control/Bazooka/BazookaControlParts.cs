using System;
using System.Collections.Generic;
using Global;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller
{
    public partial class BazookaControl : InGameControl
    {
        public readonly PartsControl Parts;

        public class PartsControl
        {
            private readonly BazookaControl _parent;

            public PartsControl(BazookaControl parent)
            {
                _parent = parent;
            }
            
            public bool IsMaxLevel(string partsIdx)
            {
	            return GetRemainLevelToMax(partsIdx) <= 0;
            }

            private int GetRemainLevelToMax(string partsIdx)
            {
	            var level = _parent.OwnerControlHub.Data.Bazooka.GetPartsLevel(partsIdx);
	            var maxLevel = DataboxController.GetDataInt(Table.Bazooka, Sheet.Parts, partsIdx, "max_level");

	            return maxLevel - level;
            }

		    public bool IsUnlock(string partsIdx)
		    {
			    _parent.OwnerControlHub.Data.Bazooka.GetState(out _, out var grade, out _);
			    var needGrade = DataboxController.GetDataInt(Table.Bazooka, Sheet.Parts, partsIdx, "get_bzk_grade", 1);

			    return grade >= needGrade;
		    }
		    
		    public double GetRequiredMineralToUpgrade(string partsIdx, int count)
		    {
			    var currentLevel = _parent.OwnerControlHub.Data.Bazooka.GetPartsLevel(partsIdx);
			    
			    // 요청된 레벨 횟수만큼 레벨별 필요 미네랄 양 합산
			    var sum = 0d;
			    for (var i = 0; i < count; i++)
			    {
				    var level = currentLevel + i;

				    var requiredMineral = DataboxController.GetDataDouble(Table.Bazooka, Sheet.level, level.ToLookUpString(), "parts_mineral");

				    // 어빌리티
				    var abCostDownPer = _parent.OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_PARTS");

				    sum += requiredMineral / (1 + (abCostDownPer / 100d));
			    }

			    return sum;
		    }

		    public bool CanUpgrade(string partsIdx, int count)
		    {
			    if (!IsUnlock(partsIdx))
				    return false;
			    
			    // 요구 레벨업 횟수가 최대 레벨을 넘어섰다면 false
			    var remainLevelToMax = GetRemainLevelToMax(partsIdx);
			    if (count > remainLevelToMax)
				    return false;
			    
			    // 현재 미네랄로 레벨업 할 수 있는 양 
			    var requiredMineral = GetRequiredMineralToUpgrade(partsIdx, count);
			    var userMineral = _parent.OwnerControlHub.Data.Resources.mineral;
		    
			    return userMineral >= requiredMineral;
		    }

		    public void Upgrade(string partsIdx, int count)
		    {
			    if (!CanUpgrade(partsIdx, count))
				    return;

			    // 미네랄 차감 
			    var requiredMineral = GetRequiredMineralToUpgrade(partsIdx, count);
			    ResourceController.Instance.SubtractMineral(requiredMineral);

			    // 업그레이드 
			    _parent.OwnerControlHub.Data.Bazooka.LevelUpParts(partsIdx, count);
			    
			    // 로그 
			    var partsLevel = _parent.OwnerControlHub.Data.Bazooka.GetPartsLevel(partsIdx);
			    WebLog.Instance.ThirdPartyLog($"equipment_upgrade", new Dictionary<string, object>
			    {
				    { "type", "bazooka_parts_upgrade" },
				    { "parts_idx", partsIdx },
				    { "parts_level", partsLevel.ToLookUpString() },
			    });

			    WebLog.Instance.ThirdPartyLog("bazooka_parts_upgrade",
				    new Dictionary<string, object> { { "parts_idx", partsIdx }, { "parts_level", partsLevel.ToLookUpString() } });
		    }
		    
		    public static float GetLevelWeight(string partsIdx, int level)
		    {
			    var abilityWeightColumn = DataboxController.GetDataString(Table.Bazooka, Sheet.Parts, partsIdx, "pt_aidx_gain");
			    return DataboxController.GetDataFloat(Table.Bazooka, Sheet.gain, level.ToLookUpString(), abilityWeightColumn);
		    }

		    public static string GetPartsAbilityIdx(string partsIdx)
		    {
			    return DataboxController.GetDataString(Table.Bazooka, Sheet.Parts, partsIdx, "pt_aidx");
		    }
        }
    }
}