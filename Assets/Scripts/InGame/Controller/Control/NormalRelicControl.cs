﻿using System.Collections.Generic;
using System.Linq;
using Databox.Dictionary;
using Firebase.Analytics;
using Global;
using InGame.Data;
using InGame.Global;
using Server;

namespace InGame.Controller
{
    public class NormalRelicControl : InGameControl
    {
        public event VoidDelegate OnChangedAbilities;

        private void Start()
        {
            OwnerControlHub.Data.NormalRelic.OnEarned += OnEarned;
            OwnerControlHub.Data.NormalRelic.OnLevelUp += OnLevelUp;
        }

        private void OnDestroy()
        {
            OwnerControlHub.Data.NormalRelic.OnEarned -= OnEarned;
            OwnerControlHub.Data.NormalRelic.OnLevelUp -= OnLevelUp;
        }

        private void OnEarned(string relicIdx)
        {
            OnChangedAbilities?.Invoke();
        }
        
        private void OnLevelUp(string relicIdx, int count)
        {
            OnChangedAbilities?.Invoke();
        }


        #region Ability

        public OrderedDictionary<string, float> GetAllAppliedAbilities()
        {
            var allRelicIdxes = DataboxController.GetAllIdxes(Table.Relic, Sheet.Relic);

            // 인덱스 기준으로 정렬 
            var sortedByIdx = new SortedList<string, float>(); // idx, value

            foreach (var relicIdx in allRelicIdxes)
            {
                if (!OwnerControlHub.Data.NormalRelic.GetState(relicIdx, out _))
                    continue;

                var finalizedValue = GetFinalizedAbilityValue(relicIdx);

                var abilityIdx = DataboxController.GetDataString(Table.Relic, Sheet.Relic, relicIdx, "r_aidx");

                // 이미 들어가있는 어빌리티라면 기존 밸류에 추가함 
                if (sortedByIdx.ContainsKey(abilityIdx))
                    sortedByIdx[abilityIdx] += finalizedValue;
                else
                    sortedByIdx[abilityIdx] = finalizedValue;
            }

            // 동일한 어빌리티 타입을 가지는 인덱스들의 밸류값 총합 계산 
            var sumValues = new OrderedDictionary<string, float>(); // type, value 
            AbilityControl.MergeByType(ref sumValues, sortedByIdx);

            return sumValues;
        }

        public float GetFinalizedAbilityValue(string relicIdx)
        {
            if (!OwnerControlHub.Data.NormalRelic.GetState(relicIdx, out var level))
                return -1;

            var weightColumn = DataboxController.GetDataString(Table.Relic, Sheet.Relic, relicIdx, "r_aidx_gain");
            var weight = DataboxController.GetDataFloat(Table.Relic, Sheet.gain, level.ToLookUpString(), weightColumn);

            var abilityIdx = DataboxController.GetDataString(Table.Relic, Sheet.Relic, relicIdx, "r_aidx");
            var abilityValue = AbilityControl.GetValue(abilityIdx);

            return abilityValue * weight;
        }

        #endregion


        #region Earn

        public int GetNeedUraniumForGacha()
        {
            // 현재 유물 보유 개수 
            var earnedCount = OwnerControlHub.Data.NormalRelic.GetAllEarned().Count();

            // 유물 보유 갯수만큼 코스트 증가됨
            return DataboxController.GetDataInt(Table.Relic, Sheet.RelicCost, earnedCount.ToLookUpString(), "gem");
        }

        public bool IsEnoughUraniumForGacha()
        {
            var needUranium = GetNeedUraniumForGacha();

            return OwnerControlHub.Data.Resources.uranium >= needUranium;
        }

        public bool IsAllEarned()
        {
            var earnedCount = OwnerControlHub.Data.NormalRelic.GetAllEarned().Count();

            var allRelicCount = DataboxController.GetAllIdxes(Table.Relic, Sheet.Relic).Count;

            return earnedCount >= allRelicCount;
        }

        public bool CanGacha()
        {
            if (IsAllEarned())
                return false;

            if (!IsEnoughUraniumForGacha())
                return false;

            return true;
        }

        public void Gacha()
        {
            if (!CanGacha())
                return;

            // 가챠 비용 차감
            var cost = GetNeedUraniumForGacha();
            ResourceController.Instance.SubtractUranium(cost, LogEvent.NormalRelicGacha);

            // 뽑기 
            InternalGacha(out var relicIdx);
            OwnerControlHub.Data.NormalRelic.Earn(relicIdx);

            WebLog.Instance.ThirdPartyLog("equipment_upgrade", new Dictionary<string, object>
            {
                {"type", "normal_relic_gacha"},
                {"relic_idx", relicIdx},
            });

            WebLog.Instance.ThirdPartyLog("normal_relic_gacha", new Dictionary<string, object>{{"relic_idx",relicIdx}});
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }

        private void InternalGacha(out string outPickedIdx)
        {
            var allRelicIdxes = DataboxController.GetAllIdxes(Table.Relic, Sheet.Relic);

            var rollList = new List<CommonRoll>();

            foreach (var relicIdx in allRelicIdxes)
            {
                // 획득한 유물은 패스 
                if (OwnerControlHub.Data.NormalRelic.IsEarned(relicIdx))
                    continue;

                GlobalFunction.AddRoll(ref rollList, relicIdx, 1);
            }

            // 주사위 돌리기 
            var roll = GlobalFunction.Roll(ref rollList);

            outPickedIdx = roll.Name;
        }

        #endregion


        #region LevelUp

        public bool IsMaxLevel(string relicIdx)
        {
            return GetRemainLevelToMax(relicIdx) <= 0;
        }
        
        private int GetRemainLevelToMax(string relicIdx)
        {
            OwnerControlHub.Data.NormalRelic.GetState(relicIdx, out var level);

            var maxLevel = DataboxController.GetDataInt(Table.Relic, Sheet.Relic, relicIdx, "max_level");
	        
            return maxLevel - level;
        }

        public double GetRequiredStoneForLevelUp(string relicIdx, int count)
        {
            if (IsMaxLevel(relicIdx)) return -1;
            
            var weightColumn = DataboxController.GetDataString(Table.Relic, Sheet.Relic, relicIdx, "upgrade_stone_level");
            var abilityModifiedPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_RELIC");

            OwnerControlHub.Data.NormalRelic.GetState(relicIdx, out var currentLevel);
            
            var sum = 0d;
            for (var i = 0; i < count; i++)
            {
                var level = currentLevel + i;
                
                var requiredStone = DataboxController.GetDataInt(Table.Relic, Sheet.level, level.ToLookUpString(), weightColumn);
                
                sum += requiredStone / (1f + (abilityModifiedPer / 100f));
            }

            return sum;
        }

        public bool HasEnoughStoneForLevelUp(string relicIdx, int count)
        {
            var ownedStone = OwnerControlHub.Data.Resources.stone;
            var requiredStone = GetRequiredStoneForLevelUp(relicIdx, count);

            return ownedStone >= requiredStone;
        }

        public bool CanLevelUp(string relicIdx, int count)
        {
            if (!OwnerControlHub.Data.NormalRelic.IsEarned(relicIdx)) return false;

            // 요구 레벨업 횟수가 최대 레벨을 넘어섰다면, 불가
            var remainLevelToMax = GetRemainLevelToMax(relicIdx);
            if (remainLevelToMax < count) return false;

            // 스톤 부족 시, 불가
            if (!HasEnoughStoneForLevelUp(relicIdx, count)) return false;

            return true;
        }

        public void LevelUp(string relicIdx, int count)
        {
            if (!CanLevelUp(relicIdx, count)) return;

            // 스톤 차감 
            var requiredStone = GetRequiredStoneForLevelUp(relicIdx, count);
            ResourceController.Instance.SubtractStone(requiredStone);

            // 업그레이드 
            OwnerControlHub.Data.NormalRelic.LevelUp(relicIdx, count);

            OwnerControlHub.Data.NormalRelic.GetState(relicIdx, out var level);

            WebLog.Instance.ThirdPartyLog($"equipment_upgrade", new Dictionary<string, object>
            {
                {"type", "normal_relic_upgrade"},
                {"relic_idx", relicIdx},
                {"relic_level", level.ToLookUpString()},
            });
            
            WebLog.Instance.ThirdPartyLog("normal_relic_upgrade", new Dictionary<string, object>{{"relic_idx",relicIdx},{"relic_level",level.ToLookUpString()}});
        }

        #endregion
    }
}