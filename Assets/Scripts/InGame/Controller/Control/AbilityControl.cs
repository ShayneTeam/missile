﻿using System.Collections.Generic;
using System.Globalization;
using Databox.Dictionary;
using Global;
using InGame.Global;
using MEC;

namespace InGame.Controller
{
    public interface IChangedAbilities
    {
        public event VoidDelegate OnChangedAbilities;
    }
    
    public class AbilityControl : InGameControl, IChangedAbilities
    {
        public event VoidDelegate OnChangedAbilities;

        private OrderedDictionary<string, float> _allAbilities = new OrderedDictionary<string, float>();

        private bool _dirtyToUnify;

        #region Event

        private void Start()
        {
            OwnerControlHub.MissileController.OnChangedAbilities += OnChangedSubAbilities;
            OwnerControlHub.SoldierController.OnChangedAbilities += OnChangedSubAbilities;
            OwnerControlHub.NormalRelicController.OnChangedAbilities += OnChangedSubAbilities;
            OwnerControlHub.DiabloRelicController.OnChangedAbilities += OnChangedSubAbilities;
            OwnerControlHub.CostumeController.OnChangedAbilities += OnChangedSubAbilities;
            OwnerControlHub.InfStoneController.OnChangedAbilities += OnChangedSubAbilities;
            OwnerControlHub.PrincessController.OnChangedAbilities += OnChangedSubAbilities;
            
            // 다른 유저들의 데이터는 위의 이벤트가 호출되지 않음 (UI와 상호작용되는 이벤트)
            // 다른 유저들의 데이터가 새롭게 파싱됐을 때도 어빌리티를 재계산하는 로직 필요 
            // 해당 데이터가 처음 세팅됐을 땐, Start()에서 UnifyAbilities()가 호출되겠지만, 
            // 갱신된 테이블 데이터가 파싱 될 경우는 호출되지 않음. 그래서 만든 로직 
            // 단, 어빌리티와 연관된 테이블 데이터만 처리해야 함  
            OwnerControlHub.Data.Bazooka.OnFromString += OnFromString;
            OwnerControlHub.Data.Missiles.OnFromString += OnFromString;
            OwnerControlHub.Data.Soldiers.OnFromString += OnFromString;
            OwnerControlHub.Data.NormalRelic.OnFromString += OnFromString;
            OwnerControlHub.Data.DiabloRelic.OnFromString += OnFromString;
            OwnerControlHub.Data.Costume.OnFromString += OnFromString;
            OwnerControlHub.Data.InfinityStone.OnFromString += OnFromString;
            OwnerControlHub.Data.Princess.OnFromString += OnFromString;

            // 모든 어빌리티 합치기 
            UnifyAbilities();
        }

        private void OnDestroy()
        {
            OwnerControlHub.MissileController.OnChangedAbilities -= OnChangedSubAbilities;
            OwnerControlHub.SoldierController.OnChangedAbilities -= OnChangedSubAbilities;
            OwnerControlHub.NormalRelicController.OnChangedAbilities -= OnChangedSubAbilities;
            OwnerControlHub.DiabloRelicController.OnChangedAbilities -= OnChangedSubAbilities;
            OwnerControlHub.CostumeController.OnChangedAbilities -= OnChangedSubAbilities;
            OwnerControlHub.InfStoneController.OnChangedAbilities -= OnChangedSubAbilities;
            OwnerControlHub.PrincessController.OnChangedAbilities -= OnChangedSubAbilities;
            
            OwnerControlHub.Data.Bazooka.OnFromString -= OnFromString;
            OwnerControlHub.Data.Missiles.OnFromString -= OnFromString;
            OwnerControlHub.Data.Soldiers.OnFromString -= OnFromString;
            OwnerControlHub.Data.NormalRelic.OnFromString -= OnFromString;
            OwnerControlHub.Data.DiabloRelic.OnFromString -= OnFromString;
            OwnerControlHub.Data.Costume.OnFromString -= OnFromString;
            OwnerControlHub.Data.InfinityStone.OnFromString -= OnFromString;
            OwnerControlHub.Data.Princess.OnFromString -= OnFromString;
        }
        
        public void RegisterController(IChangedAbilities controller)
        {
            controller.OnChangedAbilities += OnChangedSubAbilities;
        }
        
        public void UnregisterController(IChangedAbilities controller)
        {
            controller.OnChangedAbilities -= OnChangedSubAbilities;
        }

        private void OnChangedSubAbilities()
        {
            ReserveToUnify();
        }
        
        private void OnFromString()
        {
            ReserveToUnify();
        }
        
        private void ReserveToUnify()
        {
            // 비싼 처리므로, 동일 프레임에서 두 번 이상 처리하지 않게 하기 위함  
            // 예약 잡아놓고, 다음 프레임에 딱 한 번만 처리한다 
            if (_dirtyToUnify) return;
            
            // 플래그 설정
            _dirtyToUnify = true;
            
            // 다음 프레임 연산 예약 
            Timing.CallDelayed(0f, () =>
            {
                // 모든 어빌리티 합치기 
                UnifyAbilities();
            
                // 이벤트 통지 
                OnChangedAbilities?.Invoke();
                
                // 플래그 풀기
                _dirtyToUnify = false;
            }, gameObject);
        }

        private void UnifyAbilities()
        {
            // 이전거 날리기 
            _allAbilities.Clear();
                
            // 어빌리티 합치기 
            Merge(ref _allAbilities, OwnerControlHub.BazookaController.GetAllAppliedAbilities());
            Merge(ref _allAbilities, OwnerControlHub.MissileController.GetAllAppliedAbilities());
            Merge(ref _allAbilities, OwnerControlHub.SoldierController.GetAllAppliedAbilities());
            Merge(ref _allAbilities, OwnerControlHub.NormalRelicController.GetAllAppliedAbilities());
            Merge(ref _allAbilities, OwnerControlHub.DiabloRelicController.GetAllAppliedAbilities());
            Merge(ref _allAbilities, OwnerControlHub.CostumeController.GetAllAppliedAbilities());
            Merge(ref _allAbilities, OwnerControlHub.InfStoneController.GetAllAppliedAbilities());
            Merge(ref _allAbilities, OwnerControlHub.PrincessController.GetAllAppliedAbilities());
        }

        public static void SortByIdx(ref SortedList<string, float> refSortList, List<string> abIdxes)
        {
            foreach (var abIdx in abIdxes)
            {
                var abilityValue = DataboxController.GetDataFloat(Table.Ability, Sheet.Ability, abIdx, Column.value);

                // 이미 들어가있는 어빌리티라면 기존 밸류에 추가함 
                if (refSortList.ContainsKey(abIdx))
                    refSortList[abIdx] += abilityValue;
                else
                    refSortList[abIdx] = abilityValue;
            }
        }

        public static void MergeByType(ref OrderedDictionary<string, float> refDic, IDictionary<string, float> abilities)
        {
            /*
             * idx, value 를
             * type, value 로 변환
             * 
             * OrderedDictionary는 들어가는 순서를 기억함
             * 일반 Dictionary는 정렬 보장이 되지 않음
             * SortedList로는 불가능한게, Ability Type은 idx처럼 숫자형태로 돼있지 않음
             * 위에 정렬한 인덱스 순서대로 타입 정렬이 들어가길 원해서 OrderedDictionary 사용
             */
            foreach (var ability in abilities)
            {
                var abilityType = DataboxController.GetDataString(Table.Ability, Sheet.Ability, ability.Key, Column.ab_type);
			    
                if (refDic.ContainsKey(abilityType))
                    refDic[abilityType] += ability.Value;
                else
                    refDic[abilityType] = ability.Value;
            }
        }

        public static string GetType(string abIdx)
        {
            return DataboxController.GetDataString(Table.Ability, Sheet.Ability, abIdx, Column.ab_type);
        }
        
        private static void Merge(ref OrderedDictionary<string, float> refDic, IDictionary<string, float> abilities)
        {
            foreach (var ability in abilities)
            {
                if (refDic.ContainsKey(ability.Key))
                    refDic[ability.Key] += ability.Value;
                else
                    refDic[ability.Key] = ability.Value;
            }
        }

        #endregion


        #region Get 

        public static void GetNameAndDesc(string abilityIdx, out string outName, out string outDesc)
        {
            outName = GetNameText(abilityIdx);
            outDesc = GetDescText(abilityIdx, GetValue(abilityIdx));
        }

        public static string GetNameText(string abilityIdx)
        {
            return Localization.GetText(DataboxController.GetDataString(Table.Ability, Sheet.Ability, abilityIdx, Column.name));
        }
        
        public static string GetDescText(string abilityIdx, float value)
        {
            var format = GetDescFormat(abilityIdx);

            return Localization.GetFormatText(format, value.ToUnitOrDecimalString());
        }

        public static string GetDescFormat(string abilityIdx)
        {
            return DataboxController.GetDataString(Table.Ability, Sheet.Ability, abilityIdx, Column.desc);
        }

        public static float GetValue(string abilityIdx)
        {
            return DataboxController.GetDataFloat(Table.Ability, Sheet.Ability, abilityIdx, Column.value);
        }

        public float GetUnifiedValue(string type)
        {
            /*
             * 어빌리티 테스트
             * 치트 시트에 있으면 그 값으로 사용
             */
#if UNITY_EDITOR
            var testAbilities = DataboxController.GetDataDictionaryString(Table.Cheat, Sheet.Cheat, Row.Test, Column.abilities);
            if (testAbilities != null && testAbilities.TryGetValue(type, out var testValue))
                return float.Parse(testValue, CultureInfo.InvariantCulture);
#endif
            
            return _allAbilities.TryGetValue(type, out var value) ? value : 0f;
        }
        
        public static string GetIdxByType(string abType)
        {
            return DataboxController.GetIdxByColumn(Table.Ability, Sheet.Ability, Column.ab_type, abType);
        }

        #endregion


        public double ModifyHpDown(double hp, string ability)
        {
            var abHpDownPer = GetUnifiedValue(ability);

            return hp / (1f + abHpDownPer / 100f);
        }
        
        public double ModifyHpAllDown(double hp)
        {
            var abHpDownPer = GetUnifiedValue(Ability.HP_DOWN_ALL);

            return hp * (1f - abHpDownPer / 100f);
        }
        
        public float GetAmpleDamagePer()
        {
            // 증폭 데미지 
            var abAllDamageAmplePer = GetUnifiedValue(Ability.ALL_DAMAGE_DOUBLE_POWER);

            // 인피니티 스톤 : 치명타 증폭 데미지 
            var infStoneAmplePer = OwnerControlHub.InfStoneController.Helmet.AmpleDamagePer;
            
            return abAllDamageAmplePer + infStoneAmplePer;
        }
    }

    // ReSharper disable InconsistentNaming
    public static class Ability
    {
        public const string VS_DAMAGE_UP_MONSTER_GOLEM = "VS_DAMAGE_UP_MONSTER_GOLEM";
        public const string VS_DAMAGE_UP_PORTAL = "VS_DAMAGE_UP_PORTAL";
        public const string VS_DAMAGE_UP_DIABLO = "VS_DAMAGE_UP_DIABLO";
        public const string VS_DAMAGE_UP_ELITE_BOSS = "VS_DAMAGE_UP_ELITE_BOSS";
        public const string ALL_DAMAGE_DOUBLE_RATE = "ALL_DAMAGE_DOUBLE_RATE";
        public const string ALL_DAMAGE_DOUBLE_POWER = "ALL_DAMAGE_DOUBLE_POWER";
        public const string FAIRY_BOX_RATE = "FAIRY_BOX_RATE";
        public const string FAIRY_ALL_RATE = "FAIRY_ALL_RATE";
        public const string ALL_DAMAGE = "ALL_DAMAGE";
        public const string ARMY_ALL_DAMAGE = "ARMY_ALL_DAMAGE";
        public const string ARMY_SHIP_DAMAGE = "ARMY_SHIP_DAMAGE";
        public const string ARMY_ARMOR_DAMAGE = "ARMY_ARMOR_DAMAGE";
        public const string ARMY_MACHINE_DAMAGE = "ARMY_MACHINE_DAMAGE";
        public const string ARMY_AIR_DAMAGE = "ARMY_AIR_DAMAGE";
        public const string ARMY_SOLDIER_DAMAGE = "ARMY_SOLDIER_DAMAGE";
        public const string GAIN_STONE_WORMHOLE = "GAIN_STONE_WORMHOLE";
        public const string WORLDMAP_HARVEST = "WORLDMAP_HARVEST";
        public const string HP_DOWN_ALL = "HP_DOWN_ALL";
        public const string BAZOOKA_ARMY_LEVEL_DAMAGE = "BAZOOKA_ARMY_LEVEL_DAMAGE";
        public const string BAZOOKA_ARMY_SOUL = "BAZOOKA_ARMY_SOUL";
        public const string BAZOOKA_DAMAGE = "BAZOOKA_DAMAGE";
        public const string BAZOOKA_DAMAGE_MORE = "BAZOOKA_DAMAGE_MORE";
        public const string ARMY_ALL_DAMAGE_MORE = "ARMY_ALL_DAMAGE_MORE";
        public const string ALL_BIGBANG_RATE = "ALL_BIGBANG_RATE";
        public const string ALL_BIGBANG_DAMAGE = "ALL_BIGBANG_DAMAGE";
        public const string BAZOOKA_DAMAGE_PLUS = "BAZOOKA_DAMAGE_PLUS";
        public const string ARMY_ALL_DAMAGE_PLUS = "ARMY_ALL_DAMAGE_PLUS";
        public const string ALL_DAMAGE_PLUS = "ALL_DAMAGE_PLUS";
        public const string GAIN_MINERAL_ALL_MORE = "GAIN_MINERAL_ALL_MORE";
    }
}