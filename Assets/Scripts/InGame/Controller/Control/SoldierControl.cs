﻿using System;
using System.Collections.Generic;
using Databox.Dictionary;
using Firebase.Analytics;
using Global;
using InGame.Data;
using InGame.Global;
using Server;

namespace InGame.Controller
{
    public class SoldierControl : InGameControl
    {
	    public event VoidDelegate OnChangedAbilities;
	    
	    private void Start()
	    {
		    OwnerControlHub.Data.Soldiers.OnPromoted += OnChangedData;
		    OwnerControlHub.Data.Soldiers.OnEarned += OnChangedData;
		    OwnerControlHub.Data.Soldiers.OnLevelUp += OnChangedData;
	    }

	    private void OnDestroy()
	    {
		    OwnerControlHub.Data.Soldiers.OnPromoted -= OnChangedData;
		    OwnerControlHub.Data.Soldiers.OnEarned -= OnChangedData;
		    OwnerControlHub.Data.Soldiers.OnLevelUp -= OnChangedData;
	    }

	    private void OnChangedData(string soldierType)
	    {
		    OnChangedAbilities?.Invoke();
	    }
	    
	    
	    
	    #region Abilities

	    public int GetUnlockedAbilitiesCount(string type)
	    {
		    OwnerControlHub.Data.Soldiers.GetState(type, out _, out var level);

		    // 용병의 어빌리티 총 개수 
		    var unlockedAbCount = 0;
		    for (var index = 1; index <= 8; index++)
		    {
			    var abilityColumnName = $"aidx_{index.ToLookUpString()}";

			    // 해당 어빌리티의 언락 레벨을 넘어섰는지 체크 
			    var unlockLevel = DataboxController.GetDataInt(Table.Soldier, Sheet.UnlockAbility, abilityColumnName, "unlock_level");

			    if (level >= unlockLevel)
				    unlockedAbCount++;
			    else
				    break;
		    }

		    return unlockedAbCount;
	    }
        
	    public OrderedDictionary<string, float> GetAllAppliedAbilities()
	    {
		    var allAbilities = new List<string>();

		    allAbilities.AddRange(GetAppliedAbilities("soldier"));
		    allAbilities.AddRange(GetAppliedAbilities("air"));
		    allAbilities.AddRange(GetAppliedAbilities("machine"));
		    allAbilities.AddRange(GetAppliedAbilities("armor"));
		    allAbilities.AddRange(GetAppliedAbilities("ship"));

		    // 인덱스 기준 정렬 
		    var sortedByIdx = new SortedList<string, float>(); // idx, value
		    AbilityControl.SortByIdx(ref sortedByIdx, allAbilities);

		    // 타입 기준 병합  
		    var sumValues = new OrderedDictionary<string, float>(); // type, value 
		    AbilityControl.MergeByType(ref sumValues, sortedByIdx);
	        
		    return sumValues;
	    }
        
	    private List<string> GetAppliedAbilities(string type)
	    {
		    OwnerControlHub.Data.Soldiers.GetState(type, out var grade, out _);

		    Soldier.GetGradeIdx(type, grade, out var gradeIdx);

		    // 먼저 달성 레벨을 통해 언락된 어빌리티 개수 체크 
		    var unlockedAbCount = GetUnlockedAbilitiesCount(type);

		    var unlockedAbilitiesIdx = new List<string>();

		    for (var i = 1; i <= unlockedAbCount; i++)
		    {
			    var abilityColumnName = $"aidx_{i.ToLookUpString()}";

			    var abilityIdx = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, abilityColumnName);

			    unlockedAbilitiesIdx.Add(abilityIdx);
		    }

		    return unlockedAbilitiesIdx;
	    }

	    #endregion
	    
	    
	    #region Enhance
        
        public bool IsMaxGrade(string type)
        {
            OwnerControlHub.Data.Soldiers.GetState(type, out var grade, out _);

            var gradeMaxStr = DataboxController.GetConstraintsData("SOLDIER_GRADE_MAX", "10");
            return grade >= int.Parse(gradeMaxStr);
        }

        public bool IsEnoughChipsetForEnhance(string type)
        {
            var ownedChipsetCount = OwnerControlHub.Data.Items[RewardType.REWARD_CHIPSET];
            var needChipsetCount = GetNeedChipsetCountForEnhance(type);

            return ownedChipsetCount >= needChipsetCount;
        }

        public bool CanPromote(string type)
        {
	        if (!OwnerControlHub.Data.Soldiers.IsEarned(type))
		        return false;
	        
            if (IsMaxGrade(type))
        	    return false;
            
            if (!IsEnoughChipsetForEnhance(type))
        	    return false;

            return true;
        }

        public int GetNeedChipsetCountForEnhance(string type)
        {
            OwnerControlHub.Data.Soldiers.GetState(type, out var grade, out _);
            Soldier.GetGradeIdx(type, grade, out var gradeIdx);
            
            return DataboxController.GetDataInt(Table.Soldier, Sheet.Grade, gradeIdx, "upgrade_cost");
        }

        public void Promote(string type)
        {
            if (!CanPromote(type))
        	    return;
            
            // 강화 전에 필요 칩셋 차감
            var needChipset = GetNeedChipsetCountForEnhance(type);
            OwnerControlHub.Data.Items[RewardType.REWARD_CHIPSET] -= needChipset;
            
            // 강화 
            OwnerControlHub.Data.Soldiers.Promote(type);
            
            OwnerControlHub.Data.Soldiers.GetState(type, out var grade, out _);
            
            // 로그 
            ServerLogger.LogItem(RewardType.REWARD_CHIPSET, $"{LogEvent.SoldierPromote}_{grade.ToLookUpString()}", -needChipset);
            
            // 파베 로그 
            WebLog.Instance.ThirdPartyLog("equipment_upgrade", new Dictionary<string, object>
            {
	            {"type", "soldier_promote_upgrade"},
	            {"soldier_type", type},
	            {"soldier_grade", grade.ToLookUpString()},
            });
            
            WebLog.Instance.ThirdPartyLog("soldier_promote_upgrade", new Dictionary<string, object>
            {
	            {"soldier_type", type},
	            {"soldier_grade", grade.ToLookUpString()},
            });
            
            // 바로 저장 
	        UserData.My.SaveToServer();
        }

        #endregion


        #region LevelUp

        public bool IsMaxLevel(string type)
	    {
		    return GetRemainLevelToMax(type) <= 0;
	    }
        
        private int GetRemainLevelToMax(string type)
        {
	        OwnerControlHub.Data.Soldiers.GetState(type, out _, out var level);

	        var maxLevel = int.Parse(DataboxController.GetConstraintsData("SOLDIER_LEVEL_MAX", "2000"));
	        
	        return maxLevel - level;
        }
			
	    public double GetRequiredMineralForLevelUp(string type, int count)
	    {
		    OwnerControlHub.Data.Soldiers.GetState(type, out _, out var currentLevel);
		    
		    // 요청된 레벨 횟수만큼 레벨별 필요 미네랄 양 합산
		    var sum = 0d;
		    for (var i = 0; i < count; i++)
		    {
			    var level = currentLevel + i;
			    
			    var requiredMineral = DataboxController.GetDataDouble(Table.Soldier, Sheet.level, level.ToLookUpString(), "level_up_mineral");

			    // 어빌리티
			    float abCostDownTypePer;
			    var soldierType = (SoldierType)Enum.Parse(typeof(SoldierType), type);
			    switch (soldierType)
			    {
				    case SoldierType.soldier:
					    abCostDownTypePer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_SOLDIER");
					    break;
				    case SoldierType.machine:
					    abCostDownTypePer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_MACHINE");
					    break;
				    case SoldierType.armor:
					    abCostDownTypePer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_ARMOR");
					    break;
				    case SoldierType.air:
					    abCostDownTypePer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_AIR");
					    break;
				    case SoldierType.ship:
					    abCostDownTypePer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_SHIP");
					    break;
				    default:
					    throw new ArgumentOutOfRangeException();
			    }

			    var abCostDownArmyAllPer = OwnerControlHub.AbilityController.GetUnifiedValue("COST_DOWN_ARMY_ALL");

			    sum += requiredMineral / (1f + (abCostDownTypePer + abCostDownArmyAllPer) / 100f);
		    }
		    
		    return sum;
	    }

	    public bool CanLevelUp(string type, int count)
	    {
		    if (!OwnerControlHub.Data.Soldiers.IsEarned(type))
			    return false;
		    
		    // 요구 레벨업 횟수가 최대 레벨을 넘어섰다면 false
		    var remainLevelToMax = GetRemainLevelToMax(type);
		    if (count > remainLevelToMax)
			    return false;

			// 보유 미네랄이 부족하면 false 
			var requiredMineral = GetRequiredMineralForLevelUp(type, count);
			var userMineral = OwnerControlHub.Data.Resources.mineral;

			return userMineral >= requiredMineral;
	    }
			
	    public void LevelUp(string type, int count)
	    {
		    if (!CanLevelUp(type, count))
			    return;

		    // 미네랄 차감 
		    var needMineral = GetRequiredMineralForLevelUp(type, count);
		    ResourceController.Instance.SubtractMineral(needMineral);

		    // 업그레이드 
		    OwnerControlHub.Data.Soldiers.LevelUp(type, count);
		    
		    // 로그 
		    OwnerControlHub.Data.Soldiers.GetState(type, out _, out var level);
		    FirebaseAnalytics.LogEvent($"equipment_upgrade", new[]
		    {
			    new Parameter("type", "soldier_level_upgrade"),
			    new Parameter("soldier_type", type),
			    new Parameter("soldier_level", level),
		    });

		    FirebaseAnalytics.LogEvent("soldier_level_upgrade", new[]
		    {
			    new Parameter("soldier_type", type),
			    new Parameter("soldier_level", level),
		    });
	    }

        #endregion
        
        

        public double GetBaseDamage(string soldierIdx)
        {
	        // 타입 알아오기 
	        var soldierType = (SoldierType)Enum.Parse(typeof(SoldierType),DataboxController.GetDataString(Table.Soldier, Sheet.Soldier, soldierIdx, "type", "soldier"));
            
	        // 기본 데미지
	        var baseDamage = (double)DataboxController.GetDataInt(Table.Soldier, Sheet.Soldier, soldierIdx, "base_damage", 100);
	        
	        // 인피니티 스톤 : 소울 스톤 데미지 배율 
	        baseDamage *= OwnerControlHub.InfStoneController.Stone.SoulStoneDamageFactor;
		
	        // 레벨, 등급 가중치 
	        OwnerControlHub.Data.Soldiers.GetState(soldierType.ToEnumString(), out var grade, out var level);

	        var soldierDamageLevelWeightColumn = DataboxController.GetDataString(Table.Soldier, Sheet.Soldier, soldierIdx, "dmg_level_gain", "dmg_lv_gain_1");
	        var soldierDamageLevelWeight = DataboxController.GetDataDouble(Table.Soldier, Sheet.gain, level.ToLookUpString(), soldierDamageLevelWeightColumn, 1f);

	        var soldierDamageGradeWeightColumn = DataboxController.GetDataString(Table.Soldier, Sheet.Soldier, soldierIdx, "dmg_grade_gain", "dmg_grade_gain_1");
	        var soldierDamageGradeWeight = DataboxController.GetDataDouble(Table.Soldier, Sheet.gain, grade.ToLookUpString(), soldierDamageGradeWeightColumn, 1f);

	        return baseDamage * soldierDamageLevelWeight * soldierDamageGradeWeight;
        }

        public double GetFinalizedDamage(string soldierIdx)
        {
	        // 기본 데미지
	        var baseDamage = GetBaseDamage(soldierIdx);

	        // 타입 알아오기 
	        var soldierType = (SoldierType)Enum.Parse(typeof(SoldierType),DataboxController.GetDataString(Table.Soldier, Sheet.Soldier, soldierIdx, "type", "soldier"));
	        
	        // 어빌리티
	        double abArmyDamagePer;
	        switch (soldierType)
	        {
		        case SoldierType.soldier:
			        abArmyDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ARMY_SOLDIER_DAMAGE);
			        break;
		        case SoldierType.air:
			        abArmyDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ARMY_AIR_DAMAGE);
			        break;
		        case SoldierType.machine:
			        abArmyDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ARMY_MACHINE_DAMAGE);
			        break;
		        case SoldierType.armor:
			        abArmyDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ARMY_ARMOR_DAMAGE);
			        break;
		        case SoldierType.ship:
			        abArmyDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ARMY_SHIP_DAMAGE);
			        break;
		        default:
			        throw new ArgumentOutOfRangeException();
	        }
            
	        var abArmyAllDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ARMY_ALL_DAMAGE);
	        var abAllDamagePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ALL_DAMAGE);
	        var abInfStoneMindPer = OwnerControlHub.InfStoneController.Stone.MindStoneDamagePer;
	
	        // 어빌리티 반영 데미지 
	        var abilityDamage = baseDamage * (1f + (abArmyDamagePer + abArmyAllDamagePer + abAllDamagePer + abInfStoneMindPer) / 100d);
	        
	        // 인피니티 스톤 : 모든 데미지 배율 
	        var infStoneAllDamageFactor = OwnerControlHub.InfStoneController.Helmet.AllDamageFactor;
	        var infStoneDamage = abilityDamage * infStoneAllDamageFactor;
	        
	        // 공주 : 용병 데미지 증폭 
	        var abArmyAllDamageMorePer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ARMY_ALL_DAMAGE_MORE);
	        var princessDamage = infStoneDamage * (1f + abArmyAllDamageMorePer / 100d);
	        
	        // 고대 : 데미지+
	        var abAllDamagePlusPer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ALL_DAMAGE_PLUS);
	        var abDamagePlusPer = OwnerControlHub.AbilityController.GetUnifiedValue(Ability.ARMY_ALL_DAMAGE_PLUS);
	        var ancientDamage = princessDamage * (1d + (abAllDamagePlusPer + abDamagePlusPer) / 100d);
	        
	        return ancientDamage;
        }

        public void GetFinalizedAtkDelay(string soldierIdx, SoldierType soldierType, out float min, out float max)
        {
	        // 어빌리티
	        float abilityArmyShootSpeedModifiedPer;
	        
	        switch (soldierType)
	        {
		        case SoldierType.soldier:
			        abilityArmyShootSpeedModifiedPer =
				        OwnerControlHub.AbilityController.GetUnifiedValue("ARMY_SOLDIER_SPEED");
			        break;
		        case SoldierType.machine:
			        abilityArmyShootSpeedModifiedPer =
				        OwnerControlHub.AbilityController.GetUnifiedValue("ARMY_MACHINE_SPEED");
			        break;
		        case SoldierType.armor:
			        abilityArmyShootSpeedModifiedPer =
				        OwnerControlHub.AbilityController.GetUnifiedValue("ARMY_ARMOR_SPEED");
			        break;
		        case SoldierType.air:
			        abilityArmyShootSpeedModifiedPer =
				        OwnerControlHub.AbilityController.GetUnifiedValue("ARMY_AIR_SPEED");
			        break;
		        case SoldierType.ship:
			        abilityArmyShootSpeedModifiedPer =
				        OwnerControlHub.AbilityController.GetUnifiedValue("ARMY_SHIP_SPEED");
			        break;
		        default:
			        throw new ArgumentOutOfRangeException();
	        }

	        var abilityArmyAllShootSpeedModifiedPer =
		        OwnerControlHub.AbilityController.GetUnifiedValue("ARMY_ALL_SPEED");
	        
	        var shootCounterPerSecMin =
		        DataboxController.GetDataFloat(Table.Soldier, Sheet.Soldier, soldierIdx, Column.atk_delay_min, 1f);
	        min = shootCounterPerSecMin /
	                  (1f + ((abilityArmyAllShootSpeedModifiedPer + abilityArmyShootSpeedModifiedPer) / 100f));

	        var shootCounterPerSecMax =
		        DataboxController.GetDataFloat(Table.Soldier, Sheet.Soldier, soldierIdx, Column.atk_delay_max, 1f);
	        max = shootCounterPerSecMax /
	                  (1f + ((abilityArmyAllShootSpeedModifiedPer + abilityArmyShootSpeedModifiedPer) / 100f));
        }

        public int GetAllSoldiersLevelSum()
        {
	        OwnerControlHub.Data.Soldiers.GetState(SoldierType.air.ToEnumString(), out _, out var airLevel);
	        OwnerControlHub.Data.Soldiers.GetState(SoldierType.armor.ToEnumString(), out _, out var armorLevel);
	        OwnerControlHub.Data.Soldiers.GetState(SoldierType.machine.ToEnumString(), out _, out var machineLevel);
	        OwnerControlHub.Data.Soldiers.GetState(SoldierType.ship.ToEnumString(), out _, out var shipLevel);
	        OwnerControlHub.Data.Soldiers.GetState(SoldierType.soldier.ToEnumString(), out _, out var soldierLevel);

	        return airLevel + armorLevel + machineLevel + shipLevel + soldierLevel;
        }
    }
}