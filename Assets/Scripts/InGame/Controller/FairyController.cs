﻿using System.Collections.Generic;
using Global;
using InGame.Data;
using InGame.Global;
using Server;
using UnityEngine;

namespace InGame.Controller
{
    public class FairyController : MonoBehaviourSingleton<FairyController>
    {
        public static bool CanAutoCatch => UserData.My.UserInfo.GetFlag(UserFlags.CanAutoCatchFairy);

        public static void Collect(string rewardItemIdx, string rewardBoxIdx, int stage, bool isAd)
        {
            var factor = isAd ? 2 : 1;
            
            // 아이템 획득 
            if (rewardItemIdx != null)
            {
                FairyController.GetItemInfo(rewardItemIdx, stage, out var type, out var count, out _, out _);
                
                var reward = new RewardData
                {
            	    Idx = "",	// 일반 아이템은 타입이 곧 Idx
            	    Type = type,
            	    Count = count * factor
                };
                RewardTransactor.Transact(reward, LogEvent.Fairy);
            }

            // 박스 획득 
            if (rewardBoxIdx != null)
            {
                FairyController.GetBoxInfo(rewardBoxIdx, out var boxIdx, out var amount, out _, out _);
                BoxController.AddBox(boxIdx, amount * factor);
            }
            
            // 로그 
            var log = isAd ? "fairy_ads_earn" : "fairy_earn";
            WebLog.Instance.AllLog(log, new Dictionary<string, object>());
        }

        public static void AutoCollect(string rewardItemIdx, string rewardBoxIdx, int stage)
        {
            // 광고제거 상품 구매했을 시, 광고 획득으로 처리 
            var canAdRemove = UserData.My.UserInfo.GetFlag(UserFlags.CanAdRemove);
            Collect(rewardItemIdx, rewardBoxIdx, stage, canAdRemove);
        }
        
        public static void GetBoxInfo(string rewardBoxIdx, out string boxIdx, out int count, out string iconPath, out string nameKey)
        {
            // 박스 인덱스
            boxIdx = DataboxController.GetDataString(Table.Fairy, Sheet.RewardBox, rewardBoxIdx, Column.box_idx);
	        
            // 획득량
            count = DataboxController.GetDataInt(Table.Fairy, Sheet.RewardBox, rewardBoxIdx, Column.reward_count);

            // 아이콘
            iconPath = DataboxController.GetDataString(Table.Box, Sheet.Box, boxIdx, Column.icon);
	        
            // 이름
            nameKey = DataboxController.GetDataString(Table.Box, Sheet.Box, boxIdx, Column.name);
        }
        
        public static void GetItemInfo(string rewardItemIdx, int stage, out string type, out double count, out string iconPath, out string nameKey)
        {
            // 리워드 타입 
            type = DataboxController.GetDataString(Table.Fairy, Sheet.RewardItem, rewardItemIdx, Column.reward_type);
	        
            // 획득량
            var originCount = DataboxController.GetDataInt(Table.Fairy, Sheet.RewardItem, rewardItemIdx, Column.reward_count);
            count = GetItemAmountModifiedWeight(rewardItemIdx, type, originCount, stage);

            // 아이콘
            iconPath = DataboxController.GetDataString(Table.Reward, Sheet.reward, type, Column.icon);
	        
            // 이름
            nameKey = DataboxController.GetDataString(Table.Reward, Sheet.reward, type, Column.name);
        }

        private static double GetItemAmountModifiedWeight(string rewardItemIdx, string type, double baseValue, int stage)
        {
            var targetWeight = DataboxController.GetDataString(Table.Fairy, Sheet.RewardItem, rewardItemIdx, Column.reward_planet_weight);
            var planetIndex = StageControl.GetPlanetIndex(stage);

            // 가중치 적용 안 하는 값은 기본 값 반환 
            if (targetWeight == "none" || string.IsNullOrEmpty(targetWeight))
                return baseValue;
	        
            return WeightController.ModifyWeight(baseValue, Table.Fairy, Sheet.weight, planetIndex.ToLookUpString(), targetWeight);
        }

        public static void GetIconAndName(string idx, out Sprite iconSpr, out string nameStr)
        {
            // 아이콘 
            var spritePath = DataboxController.GetDataString(Table.Fairy, Sheet.Fairy, idx, Column.resource_id);
            iconSpr = AtlasController.GetSprite(spritePath);

            // 타이틀
            var key = DataboxController.GetDataString(Table.Fairy, Sheet.Fairy, idx, Column.name);
            nameStr = Localization.GetText(key);
        }
    }
}