using UnityEngine;

namespace InGame.Controller.Common
{
    // NestedClass를 가지는 partial 클래스 폼
    public partial class NestedClassForm : MonoBehaviour
    {
        // NestedClass 정의
        public readonly NestedClass Class;

        public class NestedClass
        {
            /*
             * 공통
             */
            private readonly NestedClassForm _parent;

            public NestedClass(NestedClassForm parent)
            {
                _parent = parent;
            }
            
            /*
             * 함수
             */
        }

        // NestedClass 초기화
        public NestedClassForm()
        {
            Class = new NestedClass(this);
        }
    }
}