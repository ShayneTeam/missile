﻿using UnityEngine;
using UnityEngine.UI;

namespace InGame.Controller
{
    public class BackgroundController : MonoBehaviourSingletonPersistent<BackgroundController>
    {
        [SerializeField] private SpriteRenderer bg;

        public void SetBg(Sprite sprite)
        {
            bg.sprite = sprite;
        }

        public void SetActive(bool active)
        {
            bg.gameObject.SetActive(active);
        }
    }
}