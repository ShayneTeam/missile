﻿using System.Collections.Generic;
using Global;
using InGame.Data;
using InGame.Global;
using MEC;
using Server;
using UnityEngine;

namespace InGame.Controller
{
    public class DailyChipController : MonoBehaviourSingleton<DailyChipController>
    {
        public static int RewardAmount => int.Parse(DataboxController.GetConstraintsData(Constraints.EVENT_TWOHOUR_CHIP_DAY));

        public static bool CanCollect => UserData.My.DailyChip.CanCollect();
        public static bool HasCollected => UserData.My.DailyChip.HasCollected;
        public static bool HasRemainTime => RemainTimeToCollect > 0;
        public static int RemainTimeToCollect => UserData.My.DailyChip.RemainTimeToCollect;

        private static CoroutineHandle _timer;


        public static void ResetToCollectAgain()
        {
            UserData.My.DailyChip.Reset();
        }

        public static void StartTimer()
        {
            Timing.KillCoroutines(_timer);
            _timer = Timing.RunCoroutine(_Timer(), Segment.RealtimeUpdate);
        }

        private static IEnumerator<float> _Timer()
        {
            // 남은 시간 0 될 때까지 루프
            while (HasRemainTime)
            {
                // 기다림
                const int interval = 1;
                yield return Timing.WaitForSeconds(interval);
                    
                // 남은 시간 차감
                UserData.My.DailyChip.ElapseTime(interval);
            }
        }

        public static void Collect()
        {
            if (!CanCollect) return;
            
            // 획득 처리 
            UserData.My.DailyChip.Collect();
            
            // 강화칩 지급 
            UserData.My.Items[RewardType.REWARD_CHIPSET] += RewardAmount;
            
            // 로그 
            ServerLogger.LogItem(RewardType.REWARD_CHIPSET, LogEvent.DailyChip, RewardAmount);
            
            // 저장 
            UserData.My.SaveToServer();
        }
    }
}