﻿using System;
using Global.Extensions;
using UnityEngine;
using UnityEngine.Scripting;

namespace InGame.Controller
{
    public class GCController : MonoBehaviourSingleton<GCController>
    {
        // 증분 GC를 발동 할 주기
        public static float IntervalSec = 1f;

        // 증분 GC 한 번 당, 할당 할 최대 시간
        private static ulong _maximumBudgetMs = 3;

        private float _timeToCollect;
        private ulong _prevBudge;

        public void SetBudget(ulong budget)
        {
            _prevBudge = _maximumBudgetMs;
            _maximumBudgetMs = budget;
        }

        public void RollbackBudget()
        {
            if (_prevBudge == 0) return;

            _maximumBudgetMs = _prevBudge;
        }

        private void Update()
        {
            if (Time.unscaledTime <= _timeToCollect) return;

            GarbageCollector.CollectIncremental(_maximumBudgetMs * GarbageCollector.incrementalTimeSliceNanoseconds);

            _timeToCollect = Time.unscaledTime + IntervalSec;
        }

        public static void CollectFull()
        {
            // 2022.01.27 iOS 처리, 해당 구문 미사용시 Doozy 초기화와 함께 메모리에 전부 올려서, 타이틀에서 OutOfMemory 출력됨.
            // 안드로이드는 검증이 되지 않았고, iOS는 처음 빌드부터 적용된 상태로 올라간 것이라, iOS만 처리했음.
            if (ExtensionMethods.IsIOS())
            {
                // 사용하지 않는 에셋 번들에 대한 처리
                Resources.UnloadUnusedAssets();
            }

            GC.Collect();
        }
    }
}