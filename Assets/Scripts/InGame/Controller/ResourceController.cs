﻿using System;
using System.Globalization;
using InGame.Global;
using Server;
using UI;
using UnityEngine;
using UserData = InGame.Data.UserData;

namespace InGame.Controller
{
    public class ResourceController : MonoBehaviourSingleton<ResourceController>
    {
        // 스테이지 씬에서 세팅됨 
        // 그래서 Null 처리 사용 주의 
        public ResourcesWithEffect Resources { get; set; }


        public void AddMineral(double addMineral)
        {
            // UserData에 즉각 반영
            UserData.My.Resources.mineral += addMineral;
        
            // UI 갱신
            if (Resources != null)
                Resources.mineral.Init(UserData.My.Resources.mineral.ToUnitString(), addMineral.ToUnitString());
        }
    
        public void AddGas(double addGas)
        {
            // UserData에 즉각 반영
            UserData.My.Resources.gas += addGas;

            // UI 갱신
            if (Resources != null)
                Resources.gas.Init(UserData.My.Resources.gas.ToUnitString(), addGas.ToUnitString());
        }
    
        public void AddStone(double addStone)
        {
            // UserData에 즉각 반영
            UserData.My.Resources.stone += addStone;

            // UI 갱신
            if (Resources != null)
                Resources.stone.Init(UserData.My.Resources.stone.ToUnitString(), addStone.ToUnitString());
        }
    
        public void AddUranium(double addUranium, string logEvent)
        {
            InternalAddUranium(addUranium, logEvent);

            // UI 갱신
            if (Resources != null)
                Resources.uranium.Init(Math.Floor(UserData.My.Resources.uranium).ToString("0"), Math.Floor(addUranium).ToString("0"));
        }
        
        public void AddUraniumWithOutEffect(double addUranium, string logEvent)
        {
            InternalAddUranium(addUranium, logEvent);

            // UI 갱신
            if (Resources != null)
                Resources.uranium.InitWithOutEffect(Math.Floor(UserData.My.Resources.uranium).ToString("0"));
        }

        private static void InternalAddUranium(double addUranium, string logEvent)
        {
            // UserData에 즉각 반영
            UserData.My.Resources.uranium += addUranium;

            // 로그 (게임에서 굉장히 중요한 재화므로, 재화를 깎는 곳마다 로그 책임까지 맡는다)
            ServerLogger.Log(RewardType.REWARD_GEM, logEvent, addUranium, UserData.My.Resources.uranium);
        }


        public void SubtractMineral(double amount)
        {
            // UserData에 즉각 반영
            UserData.My.Resources.mineral -= amount;
        
            // UI 갱신
            if (Resources != null)
                Resources.mineral.InitWithOutEffect(UserData.My.Resources.mineral.ToUnitString());
        }
    
        public void SubtractGas(double amount)
        {
            // UserData에 즉각 반영
            UserData.My.Resources.gas -= amount;

            // UI 갱신
            if (Resources != null)
                Resources.gas.InitWithOutEffect(UserData.My.Resources.gas.ToUnitString());
        }
    
        public void SubtractStone(double amount)
        {
            // UserData에 즉각 반영
            UserData.My.Resources.stone -= amount;

            // UI 갱신
            if (Resources != null)
                Resources.stone.InitWithOutEffect(UserData.My.Resources.stone.ToUnitString());
        }
    
        public void SubtractUranium(double amount, string logEvent)
        {
            // UserData에 즉각 반영
            UserData.My.Resources.uranium -= amount;
            
            // 로그 (게임에서 굉장히 중요한 재화므로, 재화를 깎는 곳마다 로그 책임까지 맡는다)
            ServerLogger.Log(RewardType.REWARD_GEM, logEvent, -amount, UserData.My.Resources.uranium);

            // UI 갱신
            if (Resources != null)
                Resources.uranium.InitWithOutEffect(Math.Floor(UserData.My.Resources.uranium).ToString("0"));
        }
    }
}
