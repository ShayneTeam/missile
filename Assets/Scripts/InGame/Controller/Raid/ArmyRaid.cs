﻿using System.Collections.Generic;
using UnityEngine;

namespace InGame.Controller
{
    public class ArmyRaid : Army
    {
        public override Character Character => null;
        public override List<Soldier> Soldiers => null;
        public override InGame.Princess Princess => null;
        public override void PickOneWithoutAirShip(out Ally outAlly)
        {
            outAlly = null;
        }

        public override void DespawnAll()
        {
        }
    }
}