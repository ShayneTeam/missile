﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Doozy.Engine.UI;
using Global;
using InGame.Data;
using InGame.Global;
using InGame.Global.Buff;
using MEC;
using Server;
using UI.Popup;
using UI.Popup.Menu;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace InGame.Controller
{
    public partial class RaidController : MonoBehaviourSingleton<RaidController>
    {
        // NestedClass 초기화
        public RaidController()
        {
            raidTicket = new RaidTicket(this);
        }
        
        // 꼼수
        private static int _prevBuffSpeedLevel;
        private static int _prevBuffDamageLevel;
        private static int _prevBuffMineralLevel;
        
        // 씬에 진입했는가
        public static bool HasEnteredToScene => Raid.Raid.Instance != null;
        
        // 이벤트 
        public static event VoidDelegate OnVictory;

        // 특정 스테이지 클리어 전까지 불가능
        public static bool IsUnlocked => InGameControlHub.My.StageController.IsOwned(NecessaryPlanetIdx);

        private static string NecessaryPlanetIdx => DataboxController.GetConstraintsData(Constraints.RAID_NECESSARY_PLANET);

        private static int DifficultyMax => int.Parse(DataboxController.GetConstraintsData(Constraints.RAID_DIFFICULTY_MAX));

        public static void AddDifficultyCursor()
        {
            ChangeDifficultyCursor(UserData.My.Raid.DifficultyCursor + 1);
        }
        
        public static void SubDifficultyCursor()
        {
            ChangeDifficultyCursor(UserData.My.Raid.DifficultyCursor - 1);
        }

        public static void ChangeDifficultyCursor(int wantDifficulty)
        {
            UserData.My.Raid.DifficultyCursor = Mathf.Clamp(wantDifficulty, 1, DifficultyMax);
        }
        
        public static void EnterRaidScene()
        {
            // 버프 저장 및 레이드에선 1로 설정 
            _prevBuffSpeedLevel = BuffSpeed.Instance.Level;
            _prevBuffDamageLevel = BuffDamage.Instance.Level;
            _prevBuffMineralLevel = BuffMineral.Instance.Level;

            BuffSpeed.Instance.Level = 1;
            BuffDamage.Instance.Level = 1;
            BuffMineral.Instance.Level = 1;

            //
            SceneController.DespawnAll();
            
            // 씬 로드 
            SceneManager.LoadScene("Raid");
            
            // 레이드에서 쓰이지 않는 Persistent 끄기 
            SetActiveUselessUI(false);
            
            // GC 수동 호출
            GCController.CollectFull();
        }

        public static void ExitRaidScene()
        {
            // 버프 롤백 
            if (_prevBuffSpeedLevel > 0)
            {
                BuffSpeed.Instance.Level = _prevBuffSpeedLevel;
                _prevBuffSpeedLevel = 0;
            }
            if (_prevBuffDamageLevel > 0)
            {
                BuffDamage.Instance.Level = _prevBuffDamageLevel;
                _prevBuffDamageLevel = 0;
            }            
            if (_prevBuffMineralLevel > 0)
            {
                BuffMineral.Instance.Level = _prevBuffMineralLevel;
                _prevBuffMineralLevel = 0;
            }
            
            
            // 나가기 
            Timing.RunCoroutine(_ExitRaidScene());
        }

        private static IEnumerator<float> _ExitRaidScene()
        {
            // Persistent 다시 켜기 
            SetActiveUselessUI(true);
            
            // 스테이지 씬으로 돌아가기 
            yield return Timing.WaitUntilDone(SceneController._ReturnToStageFlow());

            // 한 프레임 쉬기 (중요)
            // 씬 돌아조마자 팝업 뛰우니, 뭔가 UI 안에 프리팹이 제대로 세팅이 안되는 문제 발생 
            // 버프 스피드의 타임스케일 변경 때문에 그럴 수도 있음
            // 무튼 씬 돌아와서 한 프레임 쉬어주는거 매우 중요 
            yield return Timing.WaitForOneFrame;
            
            // 팝업 다시 열기 
            RaidPopup.Show();
        }

        private static void SetActiveUselessUI(bool active)
        {
            // 배경 
            BackgroundController.Instance.SetActive(active);
            
            // 미사일 바운더리 
            MissileBoundary.Instance.gameObject.SetActive(active);
            
            // 탑 캔버스
            var canvasTop = UICanvas.Database.SingleOrDefault(canvas => canvas.CanvasName == "Top");
            if (canvasTop != null)
            {
                canvasTop.Canvas.enabled = active;
                canvasTop.GetComponent<GraphicRaycaster>().enabled = active;
            }
        }

        public static bool TryPickRandomReward(out string rewardIdx, out string rewardGirlIdx)
        {
            rewardIdx = rewardGirlIdx = "";
                
            // 방 정보 가져오기 
            if (!ServerRaid.TryGetRoomInfo(out _, out _, out var difficulty, out _)) return false;
                
            var raidIdx = GetRaidIdx(difficulty);
            var group = DataboxController.GetDataString(Table.Raid, Sheet.raid, raidIdx, Column.reward_group);
            
            // 일반 보상
            rewardIdx = DataboxController.RandomPick(Table.Raid, Sheet.reward, Column.Group, group, Column.chance);
            
            // 공주 보상 
            rewardGirlIdx = DataboxController.RandomPick(Table.Raid, Sheet.reward_girl, Column.Group, group, Column.chance);
                
            return true;
        }

        public static double GetBossHp(int difficulty)
        {
            var raidIdx = GetRaidIdx(difficulty);
            return DataboxController.GetDataDouble(Table.Raid, Sheet.raid, raidIdx, Column.monster_hp);
        }

        private static string GetRaidIdx(int difficulty)
        {
            return DataboxController.GetIdxByColumn(Table.Raid, Sheet.raid, Column.difficulty, difficulty.ToLookUpString());
        }

        private static float GetHpDownRatioPerSec(float winLoseRatio)
        {
            var allIdxes = DataboxController.GetAllIdxes(Table.Raid, Sheet.hp_control);

            // 기본 인덱스
            var foundIdx = allIdxes.First();
			
            foreach (var idx in allIdxes)
            {
                var ratio = DataboxController.GetDataFloat(Table.Raid, Sheet.hp_control, idx, Column.hp_ratio);

                // winLoseRatio 값이 탐색한 ratio 구간보다 크다면
                if (winLoseRatio >= ratio)
                {
                    // 현재 인덱스 캐싱
                    foundIdx = idx;
                }
                // winLoseRatio 값이 탐색한 ratio 구간보다 작다면
                else 
                {
                    // 탐색 종료 
                    break;
                }
            }
                
            // 찾은 인덱스의 hp_down_speed 값 반환
            return DataboxController.GetDataFloat(Table.Raid, Sheet.hp_control, foundIdx, Column.hp_down_speed);
        }

        public static void CalcBattleResult(int difficulty, IEnumerable<ServerRaid.UserInfo> users, out bool victory, out float hpDownRatioPerSec)
        {
            GetBattleResultValues(difficulty, users, out _, out _, out _, out _, out var calc, out victory, out hpDownRatioPerSec);
        }

        public static void GetBattleResultValues(int difficulty, IEnumerable<ServerRaid.UserInfo> users, out double sumMaxDamage, out double monsterHp, out float maxShootIntervalSec, out float damageBonusToNerf, out double calc, out bool victory, out float hpDownRatioPerSec)
        {
            // [ A ] : 모든 유저의 최대 데미지 합
            sumMaxDamage = users.Sum(user => InGameControlHub.Other[user.inDate].MissileController.GetMaxDamage());

            // [ B ] : 레이드 보스 생명력
            monsterHp = GetBossHp(difficulty);

            // [ C ] : 최대 초당 발사 개수 
            var maxShootCountPerSec = users.Select(user => InGameControlHub.Other[user.inDate].BazookaController.CalcShootCounterPerSec()).Prepend(0f).Max();
            maxShootIntervalSec = RaidBazookaControl.ClampShootCountPerSecStatic(maxShootCountPerSec);
                
            // [ T ] : 레이드 시간 
            var time = RaidTime;
            
            // [ D ] : 최대 치명타 & 증폭 확률
            var maxCriticalChance = users.Select(user => InGameControlHub.Other[user.inDate].BazookaController.GetCriticalChanceAppliedAbility()).Prepend(0f).Max();
            var maxAmpleChance = users.Select(user => InGameControlHub.Other[user.inDate].AbilityController.GetUnifiedValue(Ability.ALL_DAMAGE_DOUBLE_RATE)).Prepend(0f).Max();
            // 둘 다 100은 못 넘기게 함 
            var clampedCriticalChance = Math.Min(maxCriticalChance, 100f) / 100f;
            var clampedAmpleChance = Math.Min(maxAmpleChance, 100f) / 100f;
            damageBonusToNerf = (clampedCriticalChance * clampedAmpleChance) * 100f;

            // 결과(R) = B / ( A * C )
            calc = monsterHp / ((sumMaxDamage * maxShootIntervalSec * time) * damageBonusToNerf);
            
            // 승패 기준 값 
            var raidIdx = GetRaidIdx(difficulty);
            var winLose = DataboxController.GetDataFloat(Table.Raid, Sheet.raid, raidIdx, Column.win_lose);

            // 승리 = R ≤ win_lose 
            // 패배 = R > win_lose 
            victory = calc <= winLose;
                
            // HP 감소 속도 결정 
            hpDownRatioPerSec = GetHpDownRatioPerSec((float)calc);
        }

        public static float RaidTime => float.Parse(DataboxController.GetConstraintsData(Constraints.RAID_BATTLE_TIME), CultureInfo.InvariantCulture);

        public static void SaveVictory(string rewardIdx, string rewardGirlIdx, int difficulty, bool canRewardDouble)
        {
            // 티켓 차감 
            UserData.My.Items[RewardType.REWARD_RAID_TICKET] -= 1;
                
            // 보상 지급 
            {
                var rewards = new List<RewardData>();
            
                // 일반 보상
                rewards.Add(new RewardData
                {
                    Idx = DataboxController.GetDataString(Table.Raid, Sheet.reward, rewardIdx, Column.reward_idx),
                    Type = DataboxController.GetDataString(Table.Raid, Sheet.reward, rewardIdx, Column.reward_type),
                    Count = GetRewardCount(rewardIdx, difficulty) * (canRewardDouble ? 2d : 1d),
                });
                // 공주 보상
                rewards.Add(new RewardData
                {
                    Idx = DataboxController.GetDataString(Table.Raid, Sheet.reward_girl, rewardGirlIdx, Column.reward_idx),
                    Type = DataboxController.GetDataString(Table.Raid, Sheet.reward_girl, rewardGirlIdx, Column.reward_type),
                    Count = DataboxController.GetDataInt(Table.Raid, Sheet.reward_girl, rewardGirlIdx, Column.reward_count) * (canRewardDouble ? 2d : 1d),
                });
                RewardTransactor.Transact(rewards, LogEvent.Raid);
            }
            
            // 저장 
            UserData.My.SaveToServer();
            
            // 이벤트 
            OnVictory?.Invoke();
        }

        public static double GetRewardCount(string rewardIdx, int difficulty)
        {
            var rewardCount = DataboxController.GetDataInt(Table.Raid, Sheet.reward, rewardIdx, Column.reward_count);
            var rewardType = DataboxController.GetDataString(Table.Raid, Sheet.reward, rewardIdx, Column.reward_type);

            var raidIdx = GetRaidIdx(difficulty);
            
            // 가중치 적용
            double rewardCountWeighted = rewardCount;
            switch (rewardType)
            {
                case RewardType.REWARD_INF_STONE:
                    rewardCountWeighted = WeightController.ModifyWeight(rewardCount, Table.Raid, Sheet.raid, raidIdx, Column.reward_inf_stone_weight);
                    break;
                case RewardType.REWARD_GAS:
                    rewardCountWeighted = WeightController.ModifyWeight(rewardCount, Table.Raid, Sheet.raid, raidIdx, Column.reward_gas_weight);
                    break;
                case RewardType.REWARD_STONE:
                    rewardCountWeighted = WeightController.ModifyWeight(rewardCount, Table.Raid, Sheet.raid, raidIdx, Column.reward_stone_weight);
                    break;
                case RewardType.REWARD_GEM:
                    rewardCountWeighted = WeightController.ModifyWeight(rewardCount, Table.Raid, Sheet.raid, raidIdx, Column.reward_gem_weight);
                    break;
            }
            
            return rewardCountWeighted;
        }

        public static void ShowFailedMessage(string message)
        {
            switch (message)
            {
                case "hasNotNextRoomInfo":
                case "notAttendee": 
                case "notFound":
                    MessagePopup.Show("raid_019");
                    return;
                case "notFoundAnyRoom":
                    MessagePopup.Show("raid_040");
                    return;
                case "full":
                    MessagePopup.Show("raid_018");
                    return;
                case "disconnected":
                    MessagePopup.Show("raid_056");
                    break;
                case "alreadyStarted":
                    MessagePopup.Show("raid_059");
                    break;
                case "success":
                    return;
            }
        }
    }
}