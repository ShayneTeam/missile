using System.Linq;
using Global;
using InGame.Data;
using InGame.Global;
using Server;
using UI.Popup;
using UnityEngine;

namespace InGame.Controller
{
    public partial class RaidController : MonoBehaviourSingleton<RaidController>
    {
        public enum FailedBuyReason
        {
            NotFailed,
            LimitedBuyCount,
            NotEnoughUranium,
        }

        public class RaidTicket
        {
            /*
             * 공통
             */
            private readonly RaidController _parent;

            public RaidTicket(RaidController parent)
            {
                _parent = parent;
            }
            
            /*
             * 정의
             */
            
            // 티켓 보유 체크
            public static bool HasTicket => UserData.My.Items[RewardType.REWARD_RAID_TICKET] > 0;

            public static int Count => UserData.My.Items[RewardType.REWARD_RAID_TICKET];

            // 하루 티켓 구매 제한 횟수 체크
            public static bool IsLimitedBuyCount => UserData.My.Raid.TicketBuyCount >= MaxBuyCount;

            private static int MaxBuyCount => DataboxController.GetAllIdxes(Table.Raid, Sheet.ticket).Count;


            public static void ResetBuyCount()
            {
                UserData.My.Raid.ResetTicketBuyCount();
            }

            public static FailedBuyReason BuyTicket()
            {
                // 일일 구매 제한 횟수에 도달 시, 구매 불가 
                if (IsLimitedBuyCount) return FailedBuyReason.LimitedBuyCount;
            
                // 우라늄 체크
                if (!HasEnoughUraniumToBuy) return FailedBuyReason.NotEnoughUranium;
            
                // 우라늄 차감
                ResourceController.Instance.SubtractUranium(RequiredGemToBuy, LogEvent.BuyRaidTicket);

                // 티켓 증가 
                UserData.My.Items[RewardType.REWARD_RAID_TICKET] += 1;

                // 티켓 구매 횟수 증가 
                UserData.My.Raid.AddTicketBuyCount();
                
                // 저장 
                UserData.My.SaveToServer();

                return FailedBuyReason.NotFailed;
            }

            public static void ShowFailedBuyTicketMessage(FailedBuyReason reason)
            {
                switch (reason)
                {
                    case RaidController.FailedBuyReason.LimitedBuyCount:
                        // 티켓 구매 제한 도달 
                        MessagePopup.Show("raid_036");
                        break;

                    case RaidController.FailedBuyReason.NotEnoughUranium:
                        // 우라늄 부족함
                        MessagePopup.Show("raid_023");
                        break;
                }
            }

            public static bool HasEnoughUraniumToBuy
            {
                get
                {
                    // 구매 횟수 제한에 도달했다면, false 
                    if (IsLimitedBuyCount) return false;
                
                    // 티켓 구매를 위한 우라늄 체크 (구매 횟수 기반으로 체크)
                    return UserData.My.Resources.uranium >= RequiredGemToBuy;
                }
            }

            public static int RequiredGemToBuy
            {
                get
                {
                    var idx = UserData.My.Raid.TicketBuyCount.ToLookUpString();
                    if (IsLimitedBuyCount)
                    {
                        idx = DataboxController.GetAllIdxes(Table.Raid, Sheet.ticket).Last();
                    }
                    
                    return DataboxController.GetDataInt(Table.Raid, Sheet.ticket, idx, Column.need_gem);
                }
            }
        }

        private readonly RaidTicket raidTicket;
    }
}