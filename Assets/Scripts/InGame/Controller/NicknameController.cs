﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using BackEnd;
using BackEnd.Tcp;
using BestHTTP;
using DG.Tweening;
using Global;
using Global.Extensions;
using InGame.Data;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;
using Constraints = InGame.Global.Constraints;

namespace InGame.Controller
{
    public class NicknameController : MonoBehaviourSingleton<NicknameController>
    {
        public static event StringStringDelegate OnChangedNickname;
        
        private const string ChannelGroupName = "Nickname";
        private const char FilterCharacter = '좇';

        
        private CoroutineHandle _process;
        private static CoroutineHandle _join;

        public static double RequiredCostToChangeNickname => double.Parse(DataboxController.GetConstraintsData(Constraints.NICKNAME_CHANGE_GEM_COST), CultureInfo.InvariantCulture);
        public static bool CanChangeNickname => UserData.My.Resources.uranium >= RequiredCostToChangeNickname;


        public static void ReadyFiltering()
        {
            Timing.KillCoroutines(_join);
            _join = Timing.RunCoroutine(_JoinNicknameChannel());
        }

        private static IEnumerator<float> _JoinNicknameChannel()
        {
            // 먼저 메인 채팅을 빠져나오게 해야 함 
            // 닉네임 필터링을 위해 뒤끝 채팅 기능을 이용하기에, 닉네임 전용 채팅 채널로 이동시켜야 함 
            yield return Timing.WaitUntilDone(ServerChat.Instance._Deactivate());
            
            // 요청
            var channelsAll = new List<ChannelNodeObject>();

            var bro = Backend.Chat.GetGroupChannelList(ChannelGroupName);
            if (!bro.IsSuccess())
            {
                ULogger.Log($@"[서버상태] 채팅방 리스트를 가져오지 못했습니다.");
                yield break;
            }

            // 정리 
            var rows = bro.GetReturnValuetoJSON()["rows"];
            for (var i = 0; i < rows.Count; i++)
            {
                var data = rows[i];

                var channelNode = new ChannelNodeObject(ChannelType.Public, ChannelGroupName, data["inDate"].ToString(),
                    (int)data["joinedUserCount"], (int)data["maxUserCount"],
                    data["serverAddress"].ToString(), data["serverPort"].ToString(), data["alias"].ToString());

                channelsAll.Add(channelNode);

                ULogger.Log("이름 : " + data["alias"].ToString() + "\n inDate : " + data["inDate"].ToString());
            }

            // 들어갈 채널 있는지 체크 
            var channelsCanJoin = channelsAll.Where(t => t.joinedUserCount < t.maxUserCount);
            if (!channelsCanJoin.Any())
            {
                ULogger.Log($@"들어갈 채팅방이 없습니다.");
                yield break;
            }

            // 닉네임을 유저의 인데이트로 설정
            // 닉네임이 존재해야만 채팅 채널 입장 가능 
            // 유저 인데이트의 끝에서 15글자를 잘라다가 임시 닉네임으로 사용 
            if (string.IsNullOrEmpty(Backend.UserNickName))
            {
                const int tempNicknameLength = 15;
                var tempNicknameToJoinChannel = Backend.UserInDate.Substring(Backend.UserInDate.Length - tempNicknameLength, tempNicknameLength);
                var updateTempNickname = Backend.BMember.UpdateNickname(tempNicknameToJoinChannel);
                if (!updateTempNickname.IsSuccess())
                {
                    ULogger.Log($"Failed Set Temp Nickname : {updateTempNickname.GetMessage()}");
                    yield break;
                }
            }
            
            // 채팅서버 찐접속 여부는 OnJoinChannel 이벤트로 검증되므로, 지역함수로 등록해둠
            var isDone = false;
            void JoinedChannel(JoinChannelEventArgs args) { isDone = true; }
            Backend.Chat.OnJoinChannel += JoinedChannel;

            // 채널 접속 
            var channel = channelsCanJoin.OrderBy(t => Guid.NewGuid()).First();
            ULogger.Log($"입장 : {channel.type} / {channel.host} / {channel.port.ToString()} / {channel.groupName} / {channel.inDate}");
            Backend.Chat.JoinChannel(channel.type, channel.host, channel.port, channel.groupName, channel.inDate, out var info);
            
            // 접속 완료될 때까지 대기 
            while (!isDone) yield return Timing.WaitForOneFrame;
            
            // 등록해둔 지역 함수 이벤트 해제 
            Backend.Chat.OnJoinChannel -= JoinedChannel;

            // 필터링 사용
            Backend.Chat.SetFilterUse(true);

            // 필터링 대체 글자 설정 
            Backend.Chat.SetFilterReplacementChar(FilterCharacter);
        }

        public static bool IsNicknameValidated()
        {
            // 닉네임 체크 도중에 나가졌다면, 임시 닉네임으로 세팅돼있음 
            // 그 임시 닉네임으로 세팅돼있는지 체크 
            return !Backend.UserInDate.Contains(Backend.UserNickName) && !string.IsNullOrEmpty(Backend.UserNickName);
        }

        public static void EndFiltering()
        {
            // 접속해놨던 닉네임 검증용 채팅 채널 떠남
            if (Backend.Chat.IsChatConnect(ChannelType.Public))
                Backend.Chat.LeaveChannel(ChannelType.Public);
            
            // 필터링 통과했다면 설정해놨던 대체 글자 다시 되돌림
            // 이거 안해주면 인게임에서 채팅 필터링 글자가 계속 FilterCharacter로 나오게 됨
            Backend.Chat.SetFilterReplacementChar('*');
            
            // 서버채팅 재개 (활성시키면 알아서 자동 재접속 타기 때문에 비하인드로 호출)
            ServerChat.Instance.ActivateBehind();
        }


        #region Validation

        public void StartValidation(string text)
        {
            // 버튼 누를 때마다 새로운 검증 프로세스 실행 
            // 마지막 검증 프로세스에서 이 팝업이 닫혀야만 닉네임 검증이 완료됨
            Timing.KillCoroutines(_process);
            _process = Timing.RunCoroutine(_Validate(text).CancelWith(gameObject));
        }

        private IEnumerator<float> _Validate(string text)
        {
            // 1단계. 클라이언트 필터링 검증
            if (!_DoFilterClient(text)) yield break;

            // 2단계. 뒤끝 비속어 필터링 검증
            yield return Timing.WaitUntilDone(_DoFilterWithChat(text).CancelWith(gameObject));
            
            // 3단계. 닉네임 업데이트 검증
            RequestToUpdateNickname(text);
        }

        private bool _DoFilterClient(string text)
        {
            var clientChatFilterList = new List<string>
            {
                // 클라이언트 필터링 단어들을 여기에 추가합니다.
            };
            
            // 운영자 닉네임 키워드는 추가로 사용하지 못함.
            if(!ExtensionMethods.IsDevelopmentBuild() && !ExtensionMethods.IsDev())
                clientChatFilterList.Add(FirebaseRemoteConfigController.gmNickNameKeyword);

            var isSucceededFilterCheck = clientChatFilterList.All(x=>!text.Contains(x));
            
            if(!isSucceededFilterCheck)
                MessagePopup.Show("info_217");

            return isSucceededFilterCheck;
        }

        private static IEnumerator<float> _DoFilterWithChat(string text)
        {
            var isConnect = Backend.Chat.IsChatConnect(ChannelType.Public);
            
            ULogger.Log(isConnect ? "채팅 연결이 되어 있습니다." : "채팅 연결이 되어있지 않습니다.");

            // 채팅 연결 안돼있으면, 채팅 비속어 필터링 프로세스 재낌 
            if (!isConnect)
                yield break;
            
            // 채팅 이벤트 등록
            var isSucceededFilterCheck = false;
            Backend.Chat.OnChat = (chat) =>
            {
                ULogger.Log($"OnChat {chat.ErrInfo}");

                if (chat.ErrInfo != ErrorInfo.Success) 
                    return;
                
                // 자신의 메시지일 경우
                if (chat.From.IsRemote) 
                    return;
                
                ULogger.Log("나 : " + chat.Message);

                // 메시지 안에 필터링 메시지가 들어가있다면, 체크 실패
                if (chat.Message.Contains(FilterCharacter))
                {
                    MessagePopup.Show("info_217");
                }
                // 메시지 안에 필터링 메시지 없이 들어왔다면
                else
                {
                    // 필터링 체크 성공 
                    isSucceededFilterCheck = true;
                }
            };
            
            // 검증용 채팅 보내기
            Backend.Chat.ChatToChannel(ChannelType.Public, text);
            
            // 필터링 체크 못하면 다음 프로세스 진행하지 못하게 막음 (채팅 이벤트가 오지 않아도 플로우 진행 못하게 막음)
            while (!isSucceededFilterCheck)
                yield return Timing.WaitForOneFrame;
        }

        private static void RequestToUpdateNickname(string text)
        {
            var prevNickname = ServerMyInfo.NickName;
            
            var result = Backend.BMember.UpdateNickname(text);
            switch (result.GetStatusCode())
            {
                case "409": // 중복 닉네임 
                    MessagePopup.Show("info_218");
                    break;
                case "400": // 20자 이상 or 앞뒤공백 or 이상한 형식 
                    MessagePopup.Show("info_217");
                    break;
                case "204": // 성공
                {
                    // 이벤트  
                    OnChangedNickname?.Invoke(prevNickname, text);

                    // 우리 서버에도 보냄 
                    var req = new HTTPRequest(ServerUri.updateNickname, HTTPMethods.Post);
                    req.AddField("nickname", Backend.UserNickName);
                    req.Send();
                }
                    break;
            }
        }

        #endregion

        public void DoProcessAfterChangedNickname(string prev, string curr)
        {
            // 우라늄 차감 
            var cost = RequiredCostToChangeNickname;
            ResourceController.Instance.SubtractUranium(cost, $"{LogEvent.Nickname}_{prev}_{curr}");
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }
    }
}