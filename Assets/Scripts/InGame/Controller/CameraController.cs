﻿using System;
using Global;
using UnityEngine;

namespace InGame.Controller
{
    public class CameraController : MonoBehaviourSingletonPersistent<CameraController>
    {
        public Camera main;

        private void Start()
        {
            if (!FirebaseRemoteConfigController.isOnCameraOrthographicSize) return;
            
            var aspectRatioDesign = (16f / 9f);
            var orthographicStartSize = 3.6f;
            
            var inverseAspectRatio = 1 / aspectRatioDesign;
            var currentAspectRatio = (float)Screen.width / (float)Screen.height;
            
            if (currentAspectRatio > aspectRatioDesign)
            {
                currentAspectRatio -= (currentAspectRatio - aspectRatioDesign);
            } else if (currentAspectRatio < inverseAspectRatio)
            {
                currentAspectRatio += (currentAspectRatio - inverseAspectRatio);
            }
            
            main.orthographicSize = aspectRatioDesign * (orthographicStartSize / currentAspectRatio);
        }
    }
}