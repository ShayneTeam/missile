﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Firebase.Analytics;
using Global;
using Global.Extensions;
using InGame.Controller.Mode;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InGame.Controller
{
    public class WormHoleController : MonoBehaviourSingleton<WormHoleController>
    {
        public static void EnterWormHole()
        {
            SceneController.DespawnAll();
            
            // 웜홀 씬 로드 
            SceneManager.LoadScene("WormHole");
            
            // GC 수동 호출
            GCController.CollectFull();
        }
        
        public static void ExitWormHole()
        {
            Timing.RunCoroutine(_ExitWormHoleFlow().CancelWith(Instance));
        }

        private static IEnumerator<float> _ExitWormHoleFlow()
        {
            // 스테이지 씬으로 돌아가기 
            yield return Timing.WaitUntilDone(SceneController._ReturnToStageFlow());
            
            // 한 프레임 쉬기 
            yield return Timing.WaitForOneFrame;
            
            // 웜홀 메뉴 다시 열기 
            WormHolePopup.Show();
        }
        
        public static bool IsCleared(int difficulty)
        {
            return UserData.My.WormHole.IsCleared(difficulty);
        }

        public static bool IsValid(int difficulty)
        {
            // 0 이하면 false 
            if (difficulty <= 0)
                return false;

            // 최대 난이도 넘어설 경우 false
            var allDifficulty = DataboxController.GetAllIdxes(Table.WormHole, Sheet.wormhole);
            var difficultyMax = DataboxController.GetDataInt(Table.WormHole, Sheet.wormhole, allDifficulty.Last(), Column.difficulty_max);
            if (difficulty > difficultyMax)
                return false;

            // 그외라면 true
            return true;
        }

        public static bool CanSweep(int difficulty)
        {
            // 해당 난이도 클리어 했다면, 소탕 가능 
            return IsCleared(difficulty);
        }

        public static bool HasAnyTicket()
        {
            // 웜홀 티켓이 0이면, 불가능 
            if (UserData.My.Items[RewardType.REWARD_WH_TICKET] <= 0)
                return false;

            return true;
        }

        public static bool ChangeDifficultyCursor(int wantDifficulty)
        {
            if (!IsValid(wantDifficulty))
                return false;

            // 최고 달성 난이도에서 +1 이상인 난이도라면 false (에디터에선 자유 난이도 설정하게 함) 
            if (!ExtensionMethods.IsUnityEditor())  
            {
                if (wantDifficulty > UserData.My.WormHole.BestDifficulty + 1)
                    return false;    
            }
            
            UserData.My.WormHole.DifficultyCursor = wantDifficulty;

            return true;
        }

        public static void Victory(int difficulty, int earnedChip)
        {
            if (!HasAnyTicket())
                return;
            
            // 티켓 차감 (클리어했을 때만 차감)
            const int ticketCost = 1;
            UserData.My.Items[RewardType.REWARD_WH_TICKET] -= ticketCost;
            
            // 칩 획득 
            UserData.My.Items[RewardType.REWARD_CHIPSET] += earnedChip;
            
            // 로그 (웜홀 티켓)
            var logEvent = $"{LogEvent.WormHole}_{difficulty.ToLookUpString()}";
            ServerLogger.LogItem(RewardType.REWARD_WH_TICKET, logEvent, -ticketCost);
            
            // 로그 (강화칩)
            ServerLogger.LogItem(RewardType.REWARD_CHIPSET, $"{LogEvent.WormHole}_{WormHole.WormHole.Instance.Difficulty.ToLookUpString()}", earnedChip);
            
            // 보상 획득 
            GetRewards(difficulty, out var rewardStone, out var rewardUranium);
            
            // 지옥석 
            ResourceController.Instance.AddStone(rewardStone);
            
            // 우라늄 (최초 클리어일 때만 획득) 
            if (!IsCleared(difficulty))
            {
                ResourceController.Instance.AddUranium(rewardUranium, logEvent);
            }

            // 클리어 저장 
            UserData.My.WormHole.Clear(difficulty);
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }

        public static void Sweep(int difficulty, out int outEarnedChip)
        {
            outEarnedChip = 0;
            
            if (!HasAnyTicket())
                return;
            
            if (!CanSweep(difficulty))
                return;
            
            // 티켓 차감 (클리어했을 때만 차감)
            const int ticketCost = 1;
            UserData.My.Items[RewardType.REWARD_WH_TICKET] -= ticketCost;
            
            // 보상 획득 
            GetRewards(difficulty, out var rewardStone, out _);
            
            // 지옥석 
            ResourceController.Instance.AddStone(rewardStone);
            
            // 강화칩 (시뮬레이션 돌려서 획득)
            var simulatedChip = SimulateEarnedChip(difficulty);
            UserData.My.Items[RewardType.REWARD_CHIPSET] += simulatedChip;
            
            // 로그 
            var logEvent = $"{LogEvent.WormHole}_{difficulty.ToLookUpString()}";
            ServerLogger.LogItem(RewardType.REWARD_WH_TICKET, logEvent, -ticketCost);
            ServerLogger.LogItem(RewardType.REWARD_CHIPSET, logEvent, simulatedChip);

            WebLog.Instance.AllLog("wormhole_sweep", new Dictionary<string, object>{{"stage",difficulty.ToLookUpString()}});

            outEarnedChip = simulatedChip;
            
            // 바로 저장 
            UserData.My.SaveToServer();
        }

        public static string GetWormHoleIdxByDifficulty(int difficulty)
        {
            var allWormHoleIdxes = DataboxController.GetAllIdxes(Table.WormHole, Sheet.wormhole);
             
            foreach (var wormHoleIdx in allWormHoleIdxes)
            {
                var difficultyMin = DataboxController.GetDataInt(Table.WormHole, Sheet.wormhole, wormHoleIdx, Column.difficulty_min);
                var difficultyMax = DataboxController.GetDataInt(Table.WormHole, Sheet.wormhole, wormHoleIdx, Column.difficulty_max);

                // 난이도를 포함하는 웜홀 찾기   
                if (difficultyMin <= difficulty && difficulty <= difficultyMax)
                {
                    return wormHoleIdx;
                }
            }
            
            // 포함되는 웜홀 없으면, 첫 번째 웜홀 반환 
            return allWormHoleIdxes.First();
        }

        public static void GetRewards(int difficulty, out double outStone, out double outUranium)
        {
            var wormHoleIdx = GetWormHoleIdxByDifficulty(difficulty);
            
            var stone = DataboxController.GetDataInt(Table.WormHole, Sheet.wormhole, wormHoleIdx, Column.reward_stone);
            outStone = GetRewardStone(difficulty, stone);
            
            var uranium = DataboxController.GetDataInt(Table.WormHole, Sheet.wormhole, wormHoleIdx, Column.reward_gem);
            outUranium = GetRewardUranium(difficulty, uranium);
        }

        public static double GetRewardStone(int difficulty, int basicStone)
        {
            var rewardStone = basicStone * GetDifficultyWeight(difficulty, Column.reward_stone_weight);
            
            var abStoneBonusPer = InGameControlHub.My.AbilityController.GetUnifiedValue(Ability.GAIN_STONE_WORMHOLE);
            
            return rewardStone * (1f + (abStoneBonusPer) / 100f);
        }

        public static double GetRewardUranium(int difficulty, int basicUranium)
        {
            return basicUranium * GetDifficultyWeight(difficulty, Column.reward_gem_weight);
        }

        public static void GetRewardChipRate(int difficulty, out float outBaseChipRate, out float outBonusChipRate)
        {
            var wormHoleIdx = GetWormHoleIdxByDifficulty(difficulty);
            
            outBaseChipRate = DataboxController.GetDataFloat(Table.WormHole, Sheet.wormhole, wormHoleIdx, Column.reward_chip_rate);
            outBonusChipRate = GetDifficultyWeight(difficulty, Column.reward_chip_rate_weight);
        }

        public static float GetDifficultyWeight(int difficulty, string column)
        {
            var wormHoleIdx = GetWormHoleIdxByDifficulty(difficulty);
            var weightColumn = DataboxController.GetDataString(Table.WormHole, Sheet.wormhole, wormHoleIdx, column);
            
            return DataboxController.GetDataFloat(Table.WormHole, Sheet.weight, difficulty.ToLookUpString(), weightColumn, 1f);
        }

        private static int SimulateEarnedChip(int difficulty)
        {
            var earnedChip = 0;
            
            var allBosses = DataboxController.GetAllIdxes(Table.WormHole, Sheet.boss);
            foreach (var bossIdx in allBosses)
            {
                // 주사위 돌리기 
                if (!RollChipRate(difficulty))
                    continue;
                
                earnedChip += DataboxController.GetDataInt(Table.WormHole, Sheet.boss, bossIdx, Column.reward_chip, 1);                
            }

            return earnedChip;
        }

        public static bool RollChipRate(int difficulty)
        {
            GetRewardChipRate(difficulty, out var baseChipRate, out var bonusChipRate);
            var finalizedChipRate = baseChipRate + bonusChipRate;
            
            // 테스트. 에디터에서 확률 100%
            /*if (ExtensionMethods.IsUnityEditor())
                return true;*/

            return GlobalFunction.RollFloat(finalizedChipRate);
        }

        public static bool HasEnteredToScene => WormHole.WormHole.Instance != null;

        public static bool IsUnlocked()
        {
            // 특정 스테이지 클리어 전까지 불가능
            return InGameControlHub.My.StageController.IsOwned(NecessaryPlanetIdx);
        }
        
        private static string NecessaryPlanetIdx => DataboxController.GetConstraintsData("WORMHOLE_NECESSARY_PLANET");
    }
}