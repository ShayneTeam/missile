﻿using System;
using System.Collections.Generic;
using Global;
using InGame.Data;
using InGame.Global;
using Server;
using UI.Popup;
using UnityEngine;

namespace InGame.Controller
{
    public class ThirtyDaysGiftController : MonoBehaviourSingleton<ThirtyDaysGiftController>
    {
        private static readonly string ShopIdx = "950004";
        private static readonly int DayMax = 30;

        public static int DailyRewardUranium => int.Parse(DataboxController.GetConstraintsData(Constraints.EVENT_30DAY_URANIUM_DAY));
        public static int DailyRewardChip => int.Parse(DataboxController.GetConstraintsData(Constraints.EVENT_30DAY_CHIP_DAY));


        private static DateTime EndTimeOfPurchasedDay
        {
            get
            {
                var lastPurchasedTimestamp = ShopController.GetLastPurchasedTimestamp(ShopIdx);
                var lastPurchasedDateTime = GlobalFunction.ToDateTime(lastPurchasedTimestamp);
                return DailyTimeController.GetEndTimeOf(lastPurchasedDateTime);
            }
        }

        private static int RemainTimeToCollectNext => DailyTimeController.GetRemainTimeBeforeToReset(ServerTime.Instance.NowUnscaled);

        public static string GetRemainTimeTextToCollectNext()
        {
            // 오늘 초기화 타임까지 남은 시간 반환 
            var remainTimeTodayEnd = RemainTimeToCollectNext;
            var hh = remainTimeTodayEnd / 60 / 60;
            var mm = remainTimeTodayEnd / 60 % 60;
            var ss = remainTimeTodayEnd % 60;

            var hhStr = hh.ToLookUpFormatString("00");
            var mmStr = mm.ToLookUpFormatString("00");
            var ssStr = ss.ToLookUpFormatString("00");
            return $"{hhStr}:{mmStr}:{ssStr}";
        }

        public static double RewardUraniumIfPurchase
        {
            get
            {
                // 구매 시, 바로 지급되는 우라늄 
                var rewardsData = ShopController.GetRewardsData(ShopIdx, Sheet.special);

                // 예외 처리 
                if (rewardsData.Count == 0)
                    return 0;

                // 야매 주의. 첫 번째 보상을 우라늄으로 간주 
                return rewardsData[0].Count;
            }
        }

        public static void GetSumRewardAllDays(out double uranium, out int chip)
        {
            // 기간 동안의 매일 출석 시 받을 수 있는 총합 
            uranium = DailyRewardUranium * DayMax;
            chip = DailyRewardChip * DayMax;
        }

        private static bool HasEverPurchased => ShopController.HasEverPurchased(ShopIdx);

        public static bool IsClosed
        {
            get
            {
                // 미구매시, 종료됐다고 판정
                if (!HasEverPurchased)
                    return true;

                // 구매 했지만, 기간이 모두 종료됐는가

                // 구매하자마자 1일차가 시작되며, DayMax - 1인 날의 endTime이 종료일임 
                var closeTime = EndTimeOfPurchasedDay.AddDays(DayMax - 1);

                // 현재 시간이 종료 시간을 넘어서면, true	
                // 마지막 날의 보상 + 재구매 시 1일차 보상. 이렇게 하루에 두 번 받을 수 없도록 하기 위함 
                return ServerTime.Instance.NowUnscaled.ToTimestamp() > closeTime.ToTimestamp();
            }
        }

        private static int CalcDaysCollectable()
        {
            // 미구매 시, 0 반환 
            if (!HasEverPurchased) return 0;

            var endTimeOfPurchasedDay = EndTimeOfPurchasedDay;

            // 구매한 날부터 1일차 보상 획득 가능 
            var collectableDays = 1;
            
            // DayMax - 1인 날이 마지막 보상을 받는 날 
            for (var day = 0; day < DayMax - 1; day++)
            {
                var dailyEndTime = endTimeOfPurchasedDay.AddDays(day);

                // 현재 시간이 해당 날의 초기화 시간을 넘어섰다면, 그 날의 보상 획득 할 수 있음 
                if (ServerTime.Instance.NowUnscaled.ToTimestamp() > dailyEndTime.ToTimestamp())
                    collectableDays++;
            }

            return collectableDays;
        }

        public static int CollectableCount
        {
            get
            {
                // 시간 계산상 받을 수 있는 횟수가 있어야 하며,
                // 현재 받은 횟수가 받을 수 있는 횟수보다 미만이여야 함
                var collectableDays = CalcDaysCollectable();
                if (collectableDays <= 0)
                    return 0;

                // 받을 수 있는 총 횟수 - 지금까지 획득한 보상 횟수
                return collectableDays - UserData.My.ThirtyDaysGift.CollectedCount;
            }
        }

        public static int RemainDays
        {
            get
            {
                // 미구매 시, 0 반환 
                if (!HasEverPurchased) return 0;

                // 종료일까지 몇 일 남았는가
                var collectableDays = CalcDaysCollectable();

                return DayMax - collectableDays;
            }
        }

        public static bool CanCollect => CollectableCount > 0;

        public static bool Collect()
        {
            if (!CanCollect) return false;
            
            // 획득 처리 
            UserData.My.ThirtyDaysGift.Collect();
            
            // 데일리 우라늄 지급 
            ResourceController.Instance.AddUranium(DailyRewardUranium, LogEvent.ThirtyDaysGift);
            
            // 데일리 강화칩 지급 
            UserData.My.Items[RewardType.REWARD_CHIPSET] += DailyRewardChip;
            
            ServerLogger.LogItem(RewardType.REWARD_CHIPSET, LogEvent.ThirtyDaysGift, DailyRewardChip);
            
            // 바로 저장 
            UserData.My.SaveToServer();

            return true;
        }

        public static bool CanPurchase
        {
            get
            {
                // 구매 한 번도 안 했다면, 구매 가능
                if (!HasEverPurchased) return true;

                // 아직 획득할 수 있는 게 남아있다면, 구매 불가 
                if (CanCollect) return false;

                // 구매했는데, 종료됐다면  
                if (IsClosed) return true;

                return false;
            }
        }

        public static void Purchase(Action<bool> callback)
        {
            if (!CanPurchase)
            {
                callback?.Invoke(false);
                return;
            }
            
            ShopController.Purchase(ShopIdx, Sheet.special, (success, reason) =>
            {
                if (!success)
                {
                    // 구매 실패 메시지
                    ShopController.ShowFailedMessage(reason);
                    callback?.Invoke(false);
                    return;
                }

                WebLog.Instance.AllLog("purchase_30days", new Dictionary<string, object>{{"idx", ShopIdx}});

                // 구매 성공 메시지 
                MessagePopup.Show("shop_24");
                
                // 구매 시, 획득한 갯수 초기화 
                UserData.My.ThirtyDaysGift.Reset();
                
                // 콜백
                callback?.Invoke(true);
            });
        }

        public static string LocalizedPriceText
        {
            get
            {
                var priceText = ShopController.GetLocalizedPriceText(ShopIdx, Sheet.special);
                return !string.IsNullOrEmpty(priceText) ? priceText : "";
            }
        }
    }
}