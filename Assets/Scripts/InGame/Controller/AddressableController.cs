﻿using System.Collections.Generic;
using Global;
using InGame.Global;
using UnityEngine;
using UnityEngine.AddressableAssets;

namespace InGame.Controller
{
    public class AddressableController : MonoBehaviourSingleton<AddressableController>
    {
        private readonly Dictionary<string, GameObject> _cachedAsset = new Dictionary<string, GameObject>();

        public static GameObject LoadAssetSync(string address)
        {
            if (Instance._cachedAsset.TryGetValue(address, out var go))
                return go;
            
            // 리소스로 로드 시도 
            go = Resources.Load(address) as GameObject;
            if (go == null) return null;
            
            Instance._cachedAsset.Add(address, go);

            return go;
        }

        public static void PreLoad()
        {
            // TODO 원래는 모든 어드레서블을 캐싱하는 목적 
            // 허나 인게임 코드에서 건드려야 할 부분이 워낙 많아서, 
            // 몬스터 쪽만 해놓는다

            PreLoadSheet(Table.Stage, Sheet.Monster);
            PreLoadSheet(Table.Stage, Sheet.boss);
            
            PreLoadSheet(Table.WormHole, Sheet.Monster);
            PreLoadSheet(Table.WormHole, Sheet.boss);
        }

        private static void PreLoadSheet(string table, string sheet)
        {
            var allIdxes = DataboxController.GetAllIdxes(table, sheet);
            foreach (var idx in allIdxes)
            {
                LoadAssetSync(idx);
            }
        }
    }
}