﻿using System;
using System.Linq;
using Doozy.Engine.UI;
using Global;
using InGame.Data;
using InGame.Global;
using Server;
using UnityEngine;

namespace InGame.Controller
{
    public class GuideController : MonoBehaviourSingleton<GuideController>
    {
        public static void MarkGuide(string idx, string sheet)
        {
            // 이미 마킹 돼있으면 안 함 
            if (UserData.My.Guide.IsMarkedGuide(idx)) return;

            // 보상 획득 
            var reward = GetRewardValue(idx, sheet);
            ResourceController.Instance.AddUranium(reward, $"{LogEvent.Guide}_{idx}");
            
            // 마킹 
            UserData.My.Guide.MarkGuide(idx);
            
            // 저장 
            UserData.My.SaveToServer();
        }

        public static int GetRewardValue(string idx, string sheet)
        {
            return DataboxController.GetDataInt(Table.Guide, sheet, idx, Column.reward_value);
        }
    
        public static void ShowRandomGuideOneIfCan()
        {
            // 다른 팝업이 떠있을 땐 띄우지 않음 
            if (UIPopup.AnyPopupVisible)
                return;
            
            // 튜토리얼 진행 중이라면 뜨지않음 
            if (TutorialPopup.Instance != null && TutorialPopup.IsPlaying) 
                return;

            // 
            if (InternalShowGuidePopup(Sheet.guide))
                return;
            
            // 하나만 띄움 
            if (InternalShowGuidePopup(Sheet.billing))
                return;
        }

        private static bool InternalShowGuidePopup(string sheet)
        {
            // 모든 인덱스 조회 (랜덤)
            var allIdxes = DataboxController.GetAllIdxes(Table.Guide, sheet).OrderBy(a => Guid.NewGuid());

            foreach (var idx in allIdxes)
            {
                // 스테이지 클리어 조건 부합 되는가 
                var stage = DataboxController.GetDataInt(Table.Guide, sheet, idx, Column.stage);
                if (UserData.My.Stage.bestStage <= stage) continue;
                
                // 이미 본 가이드 인가?
                var isMarked = UserData.My.Guide.IsMarkedGuide(idx);
                if (isMarked) continue;
                
                // 팝업 띄우기
                var popupName = DataboxController.GetDataString(Table.Guide, sheet, idx, Column.popup);
                var popup = UIPopup.GetPopup(popupName);
                if (popup == null) continue;
                
                popup.Show();
                return true;
            }

            return false;
        }
    }
}