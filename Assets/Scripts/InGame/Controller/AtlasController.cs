﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.U2D;

public class AtlasController : MonoBehaviourSingletonPersistent<AtlasController>
{
    [SerializeField] private SpriteAtlas atlas;

    private static readonly Dictionary<string, Sprite> _cachedSprites = new Dictionary<string, Sprite>();

    [RuntimeInitializeOnLoadMethod]
    private static void EnterPlayMode()
    {
        // 아틀라스 에셋은 유니티 엔진 내에서 관리되는 에셋 
        // 플레이모드가 꺼지면 스프라이트는 자동 해제 된다 
        // 플레이모드 재시작시, 들고 있던 스프라이트 캐싱 비워줘야 NRE 안 뜸 
        _cachedSprites.Clear();
    }

    public static Sprite GetSprite(string spriteName)
    {
        if (_cachedSprites.TryGetValue(spriteName, out var spr))
            return spr;

        var sprite = Instance.atlas.GetSprite(spriteName);
        _cachedSprites.Add(spriteName, sprite);
        return sprite;
    }
}