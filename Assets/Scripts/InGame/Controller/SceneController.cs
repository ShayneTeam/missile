﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using MEC;
using UI.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InGame.Controller
{
    public class SceneController : MonoBehaviourSingleton<SceneController>
    {
        private static IEnumerator<float> _CommonFlow()
        {
            // 커튼 치기 
            var curtainShowAsync = CurtainScreenEffect.Instance.Show();
            while (!curtainShowAsync.IsDone)
                yield return Timing.WaitForOneFrame;
            
            // 모두 디스폰
            DespawnAll(); 
        }

        public static void DespawnAll()
        {
            // 심플하게 모든 것 싹 다 반납
            LeanPool.DespawnAll();

            // 아군, 적 전부 날리기 (린풀에서의 반납과 클래스 상에서의 캐싱리스트 반납과는 별개임)
            Enemies.Instance.DespawnAll();
            Army.My.DespawnAll();
        }

        public static IEnumerator<float> _ReturnToStageFlow()
        {
            // 공통 플로우
            yield return Timing.WaitUntilDone(_CommonFlow());
            
            // 스테이지 씬 로드
            SceneManager.LoadScene("Stage");
            
            // GC 수동 호출
            GCController.CollectFull();
        }
        
        public static void ReturnToTitleScene()
        {
            // 타이틀 이동은 커튼 처리 하지 않고, 즉각 이동 함 
            DespawnAll();
            
            // 스테이지 씬 로드
            SceneManager.LoadScene("Title");
            
            // GC 수동 호출
            GCController.CollectFull();
        }
    }
}