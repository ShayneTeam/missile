﻿using Global;
using InGame.Data;
using InGame.Global;
using UnityEngine;

namespace InGame.Controller
{
    public class ExpController : MonoBehaviourSingleton<ExpController>
    {
        public static void FindPrevAndNextExp(int currentExp, string table, string levelSheet, string expColumn, out int prevLevelExp, out int nextLevelExp)
        {
            GetLevel(currentExp, table, levelSheet, expColumn, out var currentLevel, out var nextLevel);
            prevLevelExp = DataboxController.GetDataInt(table, levelSheet, currentLevel.ToLookUpString(), expColumn);
            nextLevelExp = DataboxController.GetDataInt(table, levelSheet, nextLevel.ToLookUpString(), expColumn);
        }

        public static void GetLevel(int currentExp, string table, string levelSheet, string expColumn, out int currentLevel, out int nextLevel)
        {
            var allLevels = DataboxController.GetAllIdxes(table, levelSheet);

            currentLevel = 1;
            nextLevel = allLevels.Count;
			
            foreach (var levelIdx in allLevels)
            {
                var levelExp = DataboxController.GetDataInt(table, levelSheet, levelIdx, expColumn);

                // 현재 경험치가 탐색한 레벨의 경험치 구간보다 크다면
                if (currentExp >= levelExp)
                {
                    // 현재 레벨로 캐싱
                    currentLevel = int.Parse(levelIdx);
                }
                // 현재 경험치가 탐색한 레벨의 경험치 구간보다 작다면
                else 
                {
                    // 다음 레벨임. 그리고 탐색 종료 
                    nextLevel = int.Parse(levelIdx);
                    break;
                }
            }
        }

        public static void GetExpProgress(int prevLevelExp, int currentExp, int nextLevelExp, out float ratio, out int progress, out int max)
        {
            progress = currentExp - prevLevelExp;
            max = nextLevelExp - prevLevelExp;
            ratio = progress >= max ? 1f: (float) (progress) / (float) (max);
        }
    }
}