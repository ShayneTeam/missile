﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Firebase.Analytics;
using Global;
using Global.Extensions;
using InGame.Controller.Mode;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InGame.Controller
{
    public class BattleshipController : MonoBehaviourSingleton<BattleshipController>
    {
        private const int TicketCost = 1;

        public static bool HasAnyTicket => UserData.My.Items[RewardType.REWARD_BATTLESHIP_TICKET] > 0;

        public static bool HasEnteredToScene => Battleship.Battleship.Instance != null;

        private static string NecessaryPlanetIdx => DataboxController.GetConstraintsData(Constraints.BATTLESHIP_NECESSARY_PLANET);
        public static bool IsUnlocked => InGameControlHub.My.StageController.IsOwned(NecessaryPlanetIdx);

        public static string OwnedTicketText => $"{UserData.My.Items[RewardType.REWARD_BATTLESHIP_TICKET].ToLookUpString()} / {DataboxController.GetConstraintsData(Constraints.BATTLESHIP_TICKET_RECHARGE_MAX)}";

        public static void RechargeTicket()
        {
            var currentTicketCount = UserData.My.Items[RewardType.REWARD_BATTLESHIP_TICKET];
            var rechargingMax = int.Parse(DataboxController.GetConstraintsData(Constraints.BATTLESHIP_TICKET_RECHARGE_MAX));
            var remainCountToMax = rechargingMax - currentTicketCount;
            
            var dailyFreeCount = int.Parse(DataboxController.GetConstraintsData(Constraints.BATTLESHIP_TICKET_DAILY_COUNT));

            // 딱 최대갯수를 넘지 않는, 데일리 충전 갯수 계산 
            var rechargingCount = Math.Min(remainCountToMax, dailyFreeCount);
            if (rechargingCount <= 0) return;

            // 무료 충전 갯수보다 작다면, 강제로 그 갯수만큼 충전시켜줌
            UserData.My.Items[RewardType.REWARD_BATTLESHIP_TICKET] += rechargingCount;
            
            // 로그 
            ServerLogger.LogItem(RewardType.REWARD_BATTLESHIP_TICKET, LogEvent.DailyFree, rechargingCount);
        }

        public static void EnterBattleship()
        {
            SceneController.DespawnAll();
            
            // 배틀쉽 씬 로드 
            SceneManager.LoadScene("Battleship");
            
            // GC 수동 호출
            GCController.CollectFull();
        }

        public static void ExitBattleship()
        {
            Timing.RunCoroutine(_ExitBattleshipFlow().CancelWith(Instance));
        }

        private static IEnumerator<float> _ExitBattleshipFlow()
        {
            // 스테이지 씬으로 돌아가기 
            yield return Timing.WaitUntilDone(SceneController._ReturnToStageFlow());
            
            // 한 프레임 쉬기 
            yield return Timing.WaitForOneFrame;
            
            // 배틀쉽 메뉴 다시 열기 
            BattleshipPopup.Show();
        }

        public static bool HasCompleted(int difficulty)
        {
            return UserData.My.Battleship.HasCompleted(difficulty);
        }

        public static bool IsValidDifficulty(int difficulty)
        {
            // 0 이하면 false 
            if (difficulty <= 0) return false;

            // 최대 난이도 넘어설 경우 false
            var allDifficulty = DataboxController.GetAllIdxes(Table.Battleship, Sheet.difficulty);
            return difficulty <= int.Parse(allDifficulty.Last());
        }

        public static bool IsValidBattleship(int battleshipIndex)
        {
            // 0 이하면 false 
            if (battleshipIndex < 0) return false;

            // 최대 배틀쉽 넘어설 경우 false
            var allBattleship = DataboxController.GetAllIdxes(Table.Battleship, Sheet.battleship);
            return battleshipIndex < allBattleship.Count;
        }

        public static bool CanSweep(int difficulty)
        {
            // 해당 난이도 클리어 했다면, 소탕 가능 
            return HasCompleted(difficulty);
        }

        public static bool ChangeDifficultyCursor(int wantDifficulty)
        {
            // 유효성 체크
            if (!IsValidDifficulty(wantDifficulty)) return false;

            // 에디터에선 자유 난이도 설정 가능 
            if (!ExtensionMethods.IsUnityEditor())  
            {
                // 최고 달성 난이도에서 +1 이상인 난이도라면 false 
                if (wantDifficulty > UserData.My.Battleship.BestDifficulty + 1) return false;    
            }
            
            UserData.My.Battleship.DifficultyCursor = wantDifficulty;
            
            return true;
        }

        public static bool ChangeBattleshipCursor(int wantBattleship)
        {
            // 유효성 체크
            if (!IsValidBattleship(wantBattleship)) return false;

            // 최고 배틀쉽에서 +1 이상이라면 false 
            UserData.My.Battleship.BattleshipCursor = wantBattleship;
            
            return true;
        }

        public static bool Victory(string battleshipIdx, int difficulty)
        {
            // 티켓 없으면 실패
            if (!HasAnyTicket) return false;

            // 우라늄 획득 
            GetRewards(battleshipIdx, difficulty, out var rewardUranium, out var rewardInfStoneIdx, out var rewardInfStoneAmount);

            var logEvent = $"{LogEvent.Battleship}_{difficulty.ToLookUpString()}";
            ResourceController.Instance.AddUranium(rewardUranium, logEvent);
            
            // 인피니티스톤 획득 
            UserData.My.InfinityStone.AddStone(rewardInfStoneIdx, rewardInfStoneAmount);
            
            // 티켓 차감 (클리어했을 때만 차감)
            UserData.My.Items[RewardType.REWARD_BATTLESHIP_TICKET] -= TicketCost;
            ServerLogger.LogItem(RewardType.REWARD_BATTLESHIP_TICKET, logEvent, -TicketCost);
            
            // 클리어 저장 
            UserData.My.Battleship.Complete(difficulty);
            
            // 바로 저장 
            UserData.My.SaveToServer();

            return true;
        }

        public static bool Sweep(string battleshipIdx, int difficulty)
        {
            if (!CanSweep(difficulty)) return false;

            return Victory(battleshipIdx, difficulty);
        }

        public static void GetRewards(string battleshipIdx, int difficulty, out double uranium, out string infStoneIdx, out int infStoneAmount)
        {
            uranium = DataboxController.GetDataInt(Table.Battleship, Sheet.difficulty, difficulty.ToUnitString(), Column.reward_uranium);
            
            infStoneIdx = DataboxController.GetDataString(Table.Battleship, Sheet.battleship, battleshipIdx, Column.reward_stone_idx);
            infStoneAmount = DataboxController.GetDataInt(Table.Battleship, Sheet.difficulty, difficulty.ToUnitString(), Column.reward_stone);
        }

        public static string BattleshipIdxByCursor
        {
            get
            {
                var battleshipCursor = UserData.My.Battleship.BattleshipCursor;
                var allBattleship = DataboxController.GetAllIdxes(Table.Battleship, Sheet.battleship);
                return allBattleship.ElementAtOrDefault(battleshipCursor);
            }
        }
    }
}