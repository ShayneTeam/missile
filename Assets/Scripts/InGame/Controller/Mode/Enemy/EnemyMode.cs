﻿using System;
using System.Collections.Generic;
using InGame.Data;
using Mono.CSharp;
using UnityEngine;
using UnityEngine.SceneManagement;
using Object = System.Object;

namespace InGame.Controller.Mode
{
    /*
     * FindObjectType는 interface를 찾을 수 없음
     * interface를 상속받는 MonoBehaviour를 만들어서, 그걸 FindObjectType 함
     */
    public abstract class EnemyModeBehaviour : MonoBehaviour, IMainTable
    {
        public abstract string MainTable { get; }
    }
    
    // 코드 상에서 FindObjectOfType<ModeBehaviour> 일일히 하지 않기 위해 만든 싱글턴
    // 씬 전환을 트리거 해야하므로, 이 클래스는 반드시 Persistent로 미리 씬에 상주되고 있어야 함
    public class EnemyMode : MonoBehaviourSingletonPersistent<EnemyMode>
    {
        [SerializeField] private List<string> pvpScenes;
        
        /*
         * 캐싱
         */
        #region Caching

        private static EnemyModeBehaviour _behaviour;
        
        private new void Awake()
        {
            base.Awake();
            
            // 씬 로딩될 때마다 재검색 하도록 함  
            // sceneLoaded 이벤트는 로딩된 씬의 Awake 호출보다 늦게 호출됨 
            // 로딩된 씬 오브젝트들의 Awake에서 요 클래스를 사용하지 않도록 해야 함 
            // Start 단계서 초기화하도록 할 것 
            SceneManager.sceneLoaded += (scene, mode) =>
            {
                _behaviour = null;
                
                // PVE 씬 전환 시, 특정 컨트롤러들 변경 
                if (pvpScenes.Contains(scene.name))
                    ChangeControlsToPvE();
            };
        }

        public static EnemyModeBehaviour Behaviour
        {
            get
            {
                if (_behaviour == null)
                    _behaviour = FindObjectOfType<EnemyModeBehaviour>();

                return _behaviour;
            }
        }


        private void ChangeControlsToPvE()
        {
            // 씬 전환 될 때, InGameControlHub는 inDate별로 없을 수 있으나
            // UserData만큼은 inDate별로 있는게 보장됨
            // 그래서 UserData를 전부 가져와서 그 inDate로 InGameControlHub에 접근
            foreach (var userData in UserData.Instances)
            {
                var inDate = userData.InDate;
                var controlHub = InGameControlHub.Other[inDate];
                
                // 바꿔치기 
                ReplaceControllers(controlHub);
            }
        }
        
        private static void ReplaceControllers(InGameControlHub controlHub)
        {
            controlHub.SkillController = controlHub.ReplaceController<EnemyModeSkillControl>() as EnemyModeSkillControl;
            controlHub.TargetController = controlHub.ReplaceController<EnemyModeTargetControl>() as EnemyModeTargetControl;
            controlHub.BazookaController = controlHub.ReplaceController<BazookaControl>() as BazookaControl;
        }

        #endregion

    }
}