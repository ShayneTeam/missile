﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bolt;
using Global;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using Server;
using UnityEngine;

namespace InGame.Controller.Mode
{
    public class WormHoleBehaviour : EnemyModeBehaviour, IUseSkillMine, IGetEnemyHp, IGetSummonListSolo, IEarnBossReward, IDefeat, IEnd
    {
        public override string MainTable => Table.WormHole;
        
        #region Skill

        // 매 모드 때마다 파괴 후 생성될 오브젝트라 초기화 안 해줘도 됨 
        private readonly SortedSet<string> _usedSkills = new SortedSet<string>();

        public void StampSkillTimestamp(string skillIdx)
        {
            UserData.My.Skills.StampTime(UserData.SkillMode.WormHole, skillIdx);
            
            // 사용 기록
            _usedSkills.Add(skillIdx);
        }

        public long GetSkillTimestamp(string skillIdx)
        {
            if (!_usedSkills.Contains(skillIdx))
            {
                // 첫 스킬 사용 쿨타임을 테이블 쿨타임 적용하기 위함 
                // 이러면 각 스킬은 모드 시작하자마자 쿨타임이 적용되게 됨 
                return WormHole.WormHole.Instance.StartTimestamp;
            }
            else
            {
                return UserData.My.Skills.GetTimestamp(UserData.SkillMode.WormHole, skillIdx);
            }
        }

        public void ResetSkillTimestamp(string skillIdx)
        {
            UserData.My.Skills.ResetTimeStamp(UserData.SkillMode.WormHole, skillIdx);
        }

        public float GetSkillCoolTimeSec(string skillIdx)
        {
            // 첫 사용 쿨타임은 테이블 쿨타임 적용. 그 다음부턴 기본 쿨타임 적용  
            if (!_usedSkills.Contains(skillIdx))
                return DataboxController.GetDataInt(Table.WormHole, Sheet.Skill, skillIdx, Column.CoolTime);
            else
                return DataboxController.GetDataInt(Table.Skill, Sheet.Skill, skillIdx, Column.CoolTime);
        }

        #endregion
        
        public double GetEnemyHp(double basicHp)
        {
            // 웜홀 가중치
            var wormHoleWeight = DataboxController.GetDataDouble(Table.WormHole, Sheet.wormhole, WormHole.WormHole.Instance.WormHoleIdx, Column.monster_hp_per, 1);

            // 난이도 가중치
            var difficultyWeight = WormHoleController.GetDifficultyWeight(WormHole.WormHole.Instance.Difficulty, Column.hp_difficulty_weight); 
            
            // 합산
            return basicHp * wormHoleWeight * difficultyWeight;
        }


        public void EarnBossReward(int _, int chip, int __, string ___, Vector3 effectSpawnPos)
        {
            // 칩 획득은 주사위 돌려서 획득  
            if (!WormHoleController.RollChipRate(WormHole.WormHole.Instance.Difficulty))
                return;
            
            // UI 결과 표시용. 강화칩 획득 히스토리 
            WormHole.WormHole.Instance.EarnedChipset++;

            // 보상 이펙트
            var newRewardEffect = LeanPool.Spawn(InGamePrefabs.Instance.bossRewardEffectPrefab, effectSpawnPos, Quaternion.identity);
            newRewardEffect.InitChip();
        }

        public IEnumerable<string> GetSummonListSolo()
        {
            return DataboxController.GetAllIdxes(Table.WormHole, Sheet.Monster);
        }

        public void Defeat()
        {
            WormHole.WormHole.Instance.Fail();
        }

        public void End()
        {
            _usedSkills.Clear();
            
            // 모든 스킬 쿨타임 초기화 
            var allSkillIdxes = DataboxController.GetAllIdxes(Table.Skill, Sheet.Skill);
            foreach (var skillIdx in allSkillIdxes) ResetSkillTimestamp(skillIdx);
        }
    }
}