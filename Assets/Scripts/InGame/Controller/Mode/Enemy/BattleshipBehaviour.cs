﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bolt;
using Global;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using Server;
using UnityEngine;

namespace InGame.Controller.Mode
{
    public class BattleshipBehaviour : EnemyModeBehaviour, IGetEnemyHp, IGetSummonListSolo, IGetSummonListDiablo, IDefeat
    {
        public override string MainTable => Table.Battleship;
        
        private static readonly List<string> _summonMonsterTypesDiablo = new List<string> { "normal", "elite" };
        private static readonly List<string> _summonMonsterTypesSolo = new List<string> { "normal" };
        private List<string> _summonList;

        public double GetEnemyHp(double basicHp)
        {
            // 난이도 가중치
            var difficultyWeight = DataboxController.GetDataDouble(Table.Battleship, Sheet.difficulty, Battleship.Battleship.Instance.Difficulty.ToLookUpString(), Column.monster_hp_per, 1);

            // 합산
            return basicHp * difficultyWeight;
        }

        public IEnumerable<string> GetSummonListDiablo()
        {
            return GetSummonList(_summonMonsterTypesDiablo);
        }

        public IEnumerable<string> GetSummonListSolo()
        {
            return GetSummonList(_summonMonsterTypesSolo);
        }

        public void Defeat()
        {
            Battleship.Battleship.Instance.Fail();
        }
        
        private IEnumerable<string> GetSummonList(IEnumerable<string> summonMonsterTypes)
        {
            var allMonsterIdxes = DataboxController.GetAllIdxes(Table.Battleship, Sheet.Monster);

            // 원하는 타입만 솎아내기 
            _summonList ??= new List<string>();
            _summonList.Clear();
            foreach (var idx in allMonsterIdxes)
            {
                var type = DataboxController.GetDataString(Table.Battleship, Sheet.Monster, idx, "type");
                if (summonMonsterTypes.Contains(type)) _summonList.Add(idx);
            }

            return _summonList;
        }
    }
}