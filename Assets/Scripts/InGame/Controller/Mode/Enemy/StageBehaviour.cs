﻿using System;
using System.Collections.Generic;
using System.Linq;
using Global;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame.Controller.Mode
{
    public class StageBehaviour : EnemyModeBehaviour, IUseSkillMine, IGetEnemyHp, IGetSummonListDiablo, IGetSummonListSolo, IEarnBossReward, IDefeat, IGetEnemyReward
    {
        public override string MainTable => Table.Stage;
        
        private static readonly List<string> _summonMonsterTypesDiablo = new List<string> { "normal", "elite" };
        private static readonly List<string> _summonMonsterTypesSolo = new List<string> { "normal" };
        private List<string> _summonList;

        #region Skill

        public void StampSkillTimestamp(string skillIdx)
        {
            UserData.My.Skills.StampTime(UserData.SkillMode.Stage, skillIdx);
        }

        public long GetSkillTimestamp(string skillIdx)
        {
            return UserData.My.Skills.GetTimestamp(UserData.SkillMode.Stage, skillIdx);
        }

        public void ResetSkillTimestamp(string skillIdx)
        {
            UserData.My.Skills.ResetTimeStamp(UserData.SkillMode.Stage, skillIdx);
        }

        public float GetSkillCoolTimeSec(string skillIdx)
        {
            return DataboxController.GetDataInt(Table.Skill, Sheet.Skill, skillIdx, Column.CoolTime);
        }

        #endregion

        public double GetEnemyHp(double basicHp)
        {
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out _, out var planetIdx);

            // 스테이지 가중치
            var stageCountInPlanet = InGameControlHub.My.StageController.GetStageCountByCurrentPlanet();
            var stageHpWeightColumn = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, Column.hp_stage_weight);
            var stageHpWeight = DataboxController.GetDataFloat(Table.Stage, Sheet.weight, stageCountInPlanet.ToLookUpString(), stageHpWeightColumn);

            // 행성 가중치
            var planetWeight = DataboxController.GetDataDouble(Table.Stage, Sheet.planet, planetIdx, Column.monster_hp_per);

            //
            return basicHp * stageHpWeight * planetWeight;
        }

        #region Reward

        public double GetRewardMineral(int basicMineral)
        {
            // 스테이지 기본 미네랄 
            var stageMineral = InGameControlHub.My.StageController.ModifyPlanetAndStageWeightByCurrentStage(basicMineral, Column.reward_mineral_per, Column.reward_mineral_stage_weight);
            
            // 인피니티 스톤 : 미네랄 
            var infStoneMineralFactor = InGameControlHub.My.InfStoneController.Helmet.MineralFactor;
            var infStoneMineral = stageMineral * infStoneMineralFactor;
            
            // 공주 : 미네랄 획득량+
            var princessMineralFactor = InGameControlHub.My.AbilityController.GetUnifiedValue(Ability.GAIN_MINERAL_ALL_MORE);
            
            return infStoneMineral * (1d + princessMineralFactor / 100d);
        }

        public double GetRewardGas(int basicGas)
        {
            return InGameControlHub.My.StageController.ModifyPlanetWeightByCurrentStage(basicGas, Column.reward_gas_per);
        }

        public double GetRewardStone(int basicStone)
        {
            return InGameControlHub.My.StageController.ModifyPlanetWeightByCurrentStage(basicStone, Column.reward_stone_per);
        }

        public void EarnBossReward(int diabloKey, int _, int killpoint, string rewardGroup, Vector3 effectSpawnPos)
        {
            // 디아블로 열쇠 획득 
            UserData.My.Resources.diablokey += diabloKey;
            
            // 킬포인트 획득
            UserData.My.Resources.killpoint += killpoint;

            // 상자 획득 
            InGameControlHub.My.StageController.RandomPickRewardBox(rewardGroup, out var rewardIdx);
            var boxIdx = DataboxController.GetDataString(Table.Stage, Sheet.reward, rewardIdx, Column.box_idx, "91001");

            BoxController.AddBox(boxIdx);

            // 보상 이펙트
            var bossRewardEffect = LeanPool.Spawn(InGamePrefabs.Instance.bossRewardEffectPrefab, effectSpawnPos, Quaternion.identity);
            bossRewardEffect.InitBoxAndDiabloKey(boxIdx);
        }

        #endregion

        public IEnumerable<string> GetSummonListDiablo()
        {
            return GetSummonList(_summonMonsterTypesDiablo);
        }

        public IEnumerable<string> GetSummonListSolo()
        {
            return GetSummonList(_summonMonsterTypesSolo);
        }

        public void Defeat()
        {
            Stage.Instance.Fail();
        }

        private IEnumerable<string> GetSummonList(IEnumerable<string> summonMonsterTypes)
        {
            // 현재 행성 알아오기
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out _, out var planetIdx);

            // 웨이브에서 소환할 몬스터 랜덤 하나 뽑기 
            var waveSheet = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, "wave_group");
            var allIdxesInWave = DataboxController.GetAllIdxes(Table.Wave, waveSheet);

            // 원하는 타입만 솎아내기 
            _summonList ??= new List<string>();
            _summonList.Clear();
            foreach (var idx in allIdxesInWave)
            {
                var type = DataboxController.GetDataString(Table.Wave, waveSheet, idx, "type");
                var monsterIdx = DataboxController.GetDataString(Table.Wave, waveSheet, idx, "summon_idx");
                if (summonMonsterTypes.Contains(type))
                    _summonList.Add(monsterIdx);
            }

            return _summonList;
        }
    }
}