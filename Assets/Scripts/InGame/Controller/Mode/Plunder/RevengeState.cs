﻿using System;
using System.Collections.Generic;
using Bolt;
using Global;
using InGame.Data;
using InGame.Data.Scene;
using InGame.Global;
using Lean.Pool;
using MEC;
using UI.Popup;
using UnityEngine;

namespace InGame.Controller.Mode
{
    public class RevengeState : PlunderBehaviour
    {
        public override void End()
        {
            Timing.RunCoroutine(_SaveResult(), Segment.RealtimeUpdate);
        }

        private IEnumerator<float> _SaveResult()
        {
            // 결과 저장 (처리 성공 못할 시, 무한 대기)
            var logInDate = SceneData.Instance.Plunder.RevengeLogInDate;
            
            LoadingIndicator.Show();
            
            yield return Timing.WaitUntilDone(
                InGameControlHub.My.PlunderController._SaveResultRevenge(logInDate, Plunder.Plunder.Instance.FinalizedGasMe, Plunder.Plunder.Instance.RewardInfStoneIdx));
            
            LoadingIndicator.Hide();

            // 약탈 씬 나가기 
            PlunderControl.ExitPlunderSceneFromRevenge();
        }
    }
}