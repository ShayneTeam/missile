﻿using System;
using System.Collections.Generic;
using Bolt;
using Global;
using InGame.Data;
using InGame.Data.Scene;
using InGame.Global;
using Lean.Pool;
using MEC;
using UI.Popup;
using UnityEngine;

namespace InGame.Controller.Mode
{
    public class PlunderState : PlunderBehaviour
    {
        public override void End()
        {
            Timing.RunCoroutine(_SaveResult(), Segment.RealtimeUpdate);
        }

        private IEnumerator<float> _SaveResult()
        {
            LoadingIndicator.Show();
            
            // 결과 저장 (처리 성공 못할 시, 무한 대기)
            SceneData.Instance.Plunder.GetTargetUserData(out var userInDate, out var nickname, out _);
            
            yield return Timing.WaitUntilDone(
                InGameControlHub.My.PlunderController._SaveResultPlunder(userInDate, nickname, Plunder.Plunder.Instance.FinalizedGasMe,
                    Plunder.Plunder.Instance.RewardInfStoneIdx, SceneData.Instance.Plunder.SweepCount));
            
            LoadingIndicator.Hide();

            // 약탈 씬 나가기 
            PlunderControl.ExitPlunderScene();
        }
    }
}