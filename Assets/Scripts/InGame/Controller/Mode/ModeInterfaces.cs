﻿using System.Collections.Generic;
using UnityEngine;


namespace InGame.Controller.Mode
{
    /*
     * Scene 마다 아래 인터페이스를 구현하여 하이어라키에 올려둠
     * 씬이 변경될 때마다 올려놓은 Mode 오브젝트가 변경되면서 구현이 변경되는 원리  
     */
    public interface IMainTable
    {
        string MainTable { get; }
    }

    public interface IDefeat
    {
        void Defeat();
    }
    
    public interface IEnd
    {
        void End();
    }

    public interface IUseSkillMine
    {
        void StampSkillTimestamp(string skillIdx);
        long GetSkillTimestamp(string skillIdx);
        void ResetSkillTimestamp(string skillIdx);
        float GetSkillCoolTimeSec(string skillIdx);
    }
    
    public interface IUseSkillEachUser
    {
        void StampSkillTimestamp(string inDate, string skillIdx);
        long GetSkillTimestamp(string inDate, string skillIdx);
        void ResetSkillTimestamp(string inDate, string skillIdx);
        float GetSkillCoolTimeSec(string inDate, string skillIdx);
    }

    public interface IGetEnemyHp
    {
        double GetEnemyHp(double basicHp);
    }

    public interface IGetEnemyReward
    {
        double GetRewardMineral(int basicMineral);
        double GetRewardGas(int basicGas);
        double GetRewardStone(int basicStone);
    }

    public interface IGetSummonListDiablo
    {
        IEnumerable<string> GetSummonListDiablo();
    }
    
    public interface IGetSummonListSolo
    {
        IEnumerable<string> GetSummonListSolo();
    }
    
    public interface IEarnBossReward
    {
        void EarnBossReward(int diabloKey, int chip, int killpoint, string rewardGroup, Vector3 effectSpawnPos);
    }
}