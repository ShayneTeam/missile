﻿using System;
using System.Collections.Generic;
using Global;
using InGame.Data;
using InGame.Global;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace InGame.Controller.Mode
{
    // 코드 상에서 FindObjectOfType<ModeBehaviour> 일일히 하지 않기 위해 만든 싱글턴
    // 씬 전환을 트리거 해야하므로, 이 클래스는 반드시 Persistent로 미리 씬에 상주되고 있어야 함
    public class RaidMode : MonoBehaviourSingletonPersistent<RaidMode>, IUseSkillEachUser, IEnd
    {
        [SerializeField] private List<string> raidScenes;

        
        #region Caching

        private new void Awake()
        {
            base.Awake();
            
            // 씬 로딩될 때마다 재검색 하도록 함  
            // sceneLoaded 이벤트는 로딩된 씬 오브젝트들의 Awake 호출보다 늦게 호출됨 
            // 로딩된 씬 오브젝트들의 Awake에서 요 클래스를 사용하지 않도록 해야 함 
            // Start 단계서 초기화하도록 할 것
            SceneManager.sceneLoaded += (scene, mode) =>
            {
                // PVP 씬 전환 시, 특정 컨트롤러들 변경 
                if (raidScenes.Contains(scene.name))
                    ChangeControllers();
            };
            
            // 컨트롤허브 생성 이벤트 
            InGameControlHub.OnAwake += OnAwake;
        }

        private void OnDestroy()
        {
            // 이벤트 해지 
            InGameControlHub.OnAwake -= OnAwake;
        }
        
        private void ChangeControllers()
        {
            // 씬 전환 될 때, InGameControlHub는 inDate별로 없을 수 있으나
            // UserData만큼은 inDate별로 있는게 보장됨
            // 그래서 UserData를 전부 가져와서 그 inDate로 InGameControlHub에 접근
            foreach (var userData in UserData.Instances)
            {
                var inDate = userData.InDate;
                var controlHub = InGameControlHub.Other[inDate];
                
                // 바꿔치기 
                ReplaceControllers(controlHub);
            }
        }

        private void OnAwake(InGameControlHub hub)
        {
            // 레이드 씬 일때만 처리 
            var activeScene = SceneManager.GetActiveScene();
            if (!raidScenes.Contains(activeScene.name)) return;
                
            // 약탈은 미리 상대 데이터가 세팅되서 넘어오기에, 씬이 변경될 때만 컨트롤러를 바꿔주면 됨 
            // 허나 레이드는 레이드 씬 안에서 다른 유저 데이터가 세팅되므로, 들어올 때마다 컨트롤러를 세팅해줘야 함 
            ReplaceControllers(hub);
        }

        private static void ReplaceControllers(InGameControlHub controlHub)
        {
            controlHub.SkillController = controlHub.ReplaceController<RaidSkillControl>() as RaidSkillControl;
            controlHub.TargetController = controlHub.ReplaceController<RaidTargetControl>() as RaidTargetControl;
            controlHub.BazookaController = controlHub.ReplaceController<RaidBazookaControl>() as RaidBazookaControl;
        }

        #endregion


        #region Skill

        private readonly Dictionary<string, SortedSet<string>> _usedSkills = new Dictionary<string, SortedSet<string>>();

        public void StampSkillTimestamp(string inDate, string skillIdx)
        {
            UserData.Other[inDate].Skills.StampTime(UserData.SkillMode.Raid, skillIdx);
            
            // 사용 처리 기록
            if (!_usedSkills.ContainsKey(inDate)) _usedSkills[inDate] = new SortedSet<string>();
            _usedSkills[inDate].Add(skillIdx);
        }

        public long GetSkillTimestamp(string inDate, string skillIdx)
        {
            if (!_usedSkills.TryGetValue(inDate, out var set) || !set.Contains(skillIdx))
            {
                // 첫 사용 쿨타임을 테이블 쿨타임 적용하기 위함
                // 이러면 각 스킬은 모드 시작하자마자 쿨타임이 적용되게 됨 
                return Raid.Raid.Instance.StartTimestamp;;
            }
            else
            {
                return UserData.Other[inDate].Skills.GetTimestamp(UserData.SkillMode.Raid, skillIdx);
            }
        }

        public void ResetSkillTimestamp(string inDate, string skillIdx)
        {
            UserData.Other[inDate].Skills.ResetTimeStamp(UserData.SkillMode.Raid, skillIdx);
        }

        public float GetSkillCoolTimeSec(string inDate, string skillIdx)
        {
            // 첫 사용 쿨타임은 테이블 쿨타임 적용. 그 다음부턴 기본 쿨타임 적용
            if (!_usedSkills.TryGetValue(inDate, out var set) || !set.Contains(skillIdx))
                return DataboxController.GetDataInt(Table.Raid, Sheet.Skill, skillIdx, Column.CoolTime);
            else
                return DataboxController.GetDataInt(Table.Skill, Sheet.Skill, skillIdx, Column.CoolTime);
        }

        #endregion

        public void End()
        {
            // 초기화 
            _usedSkills.Clear();
            
            // 모든 스킬 쿨타임 초기화 
            var allSkillIdxes = DataboxController.GetAllIdxes(Table.Skill, Sheet.Skill);
            foreach (var skillIdx in allSkillIdxes) ResetSkillTimestamp(UserData.My.InDate, skillIdx);
        }
    }
}
