﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame.Enemy
{
    public class SummonRepeat : MonoBehaviour
    {
        private readonly List<Enemy> _spawnedMonster = new List<Enemy>();
        private CoroutineHandle _summoning;


        public void StartSummon(float delay, float intervalSecMin, float intervalSecMax, float moveSpeedFactor, IEnumerable<string> spawnList)
        {
            if (spawnList == null) return;
            
            _spawnedMonster.Clear();
            
            _summoning = Timing.RunCoroutine(_SummonRepeatFlow(delay, intervalSecMin, intervalSecMax, moveSpeedFactor, spawnList).CancelWith(gameObject));
        }

        public void EndSummon()
        {
            // 꺼졌다고 소환된 애들 죽게 하면 안됨. 단지 소환 코루틴만 종료하면 됨
            Timing.KillCoroutines(_summoning);
            
            // DieAll이거나, 새로 소환을 시작했을 때만 Clear() 시킴 
            // 여기서 날리면, 솔로가 죽었을 때, DieAll 때 소환시켰던 애들이 안 죽는 현상 발생 
            //_spawnedMonster.Clear();
        }

        private IEnumerator<float> _SummonRepeatFlow(float delay, float intervalSecMin, float intervalSecMax, float moveSpeedFactor, IEnumerable<string> spawnList)
        {
            // 초기 딜레이 
            yield return Timing.WaitForSeconds(delay);
            
            // 스폰 아레나
            const string spawnArena = "boss_arena_3"; // 일단 직접 지정 
            Enemies.Instance.GetArenaBounds(spawnArena, out var arenaBounds);

            while (true)
            {
                // 랜덤으로 하나 뽑기 
                var monsterIdx = spawnList.ElementAt(Random.Range(0, spawnList.Count()));
                
                // 스폰 위치 
                var spawnPos = Vector2.zero;
                spawnPos.x = Random.Range(arenaBounds.min.x, arenaBounds.max.x); 
                spawnPos.y = Random.Range(arenaBounds.min.y, arenaBounds.max.y); 
	
                // 스폰 
                var enemy = Enemies.Instance.SpawnMonster(monsterIdx, spawnPos);
	
                // 스폰 몬스터 이동 속도 배율 적용 
                enemy.MultiplyMoveSpeed(moveSpeedFactor);
                enemy.OnDie += OnDieEnemy;
	
                // 리스트에 저장 
                _spawnedMonster.Add(enemy);
	
                // 스폰 딜레이 
                var summonIntervalSec = Random.Range(intervalSecMin, intervalSecMax);
                yield return Timing.WaitForSeconds(summonIntervalSec);
                yield return Timing.WaitForOneFrame;
            }
        }
        
        private void OnDieEnemy(Enemy enemy)
        {
            _spawnedMonster.Remove(enemy);
		
            enemy.OnDie -= OnDieEnemy;
        }
    
        public void DieAll()
        {
            EndSummon();
            
            // 스폰시킨 몬스터들 다 죽게 하기 
            /*
             * Die 시, OnDieEnemy 호출되면서 Remove 호출됨
             * Foreach 안에서 Remove 시 익셉션 뜨므로, For를 역순으로 돌린다 
             */
            for (var index = _spawnedMonster.Count - 1; index >= 0; index--)
            {
                var enemy = _spawnedMonster[index];
                enemy.Die();
            }

            _spawnedMonster.Clear();
        }
    }
}