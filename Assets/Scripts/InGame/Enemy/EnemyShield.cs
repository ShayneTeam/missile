﻿using System.Collections.Generic;
using DG.Tweening;
using InGame.Common;
using MEC;
using UnityEngine;

namespace InGame.Enemy
{
    public class EnemyShield : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            MissileRemover.Remove(other, transform);
        }

        public void Show(float time)
        {
            var go = gameObject;
            go.SetActive(true);
            Timing.RunCoroutine(_WaitAndHide(time).CancelWith(go));
        }

        private IEnumerator<float> _WaitAndHide(float time)
        {
            // 최초 스폰 때 호출 하면, 트윈 씹히므로, 한 프레임 기다림
            yield return Timing.WaitForOneFrame;
            
            // 트윈
            DOTween.Restart(gameObject, "appear");
            
            // 시간만큼 기다림
            yield return Timing.WaitForSeconds(time);
            
            // 트윈                                     
            FadeOut();
        }

        public void FadeOut()
        {
            // 페이드 아웃 끝나면, 트윈에서 자동으로 Hide 호출됨
            DOTween.Restart(gameObject, "fadeOut");
        }

        public void Hide()
        {
            gameObject.SetActive(false);
        }
    }
}