﻿using Global;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Global;

namespace InGame.Enemy
{
    public class Elite : Enemy
    {
        public override int BasicHp => Monster.GetBasicHp(DataIdx);
        public override string FxResourceId => Monster.GetFxResourceId(DataIdx);
        public override float MoveSpeedRatio => Monster.GetMoveSpeedRatio(DataIdx);
        public override string MoveType => Monster.GetMoveType(DataIdx);
        public override float ImmuneTime => Monster.GetImmuneTime(DataIdx);
        public override string ResourceId => Monster.GetResourceId(DataIdx);
        public override int RewardMineral => Monster.GetRewardMineral(DataIdx);

        public override void Init(string dataIdx)
        {
            InitCommon(dataIdx);
        
            // 기본
            InitBasic();
        
            // 이동 
            InitMove();
        
            // 무적 시간 후, 체력바 자동 보여주기 
            var immuneTime = DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.Monster, DataIdx, Column.ImmuneTime);
            DelayShowHealthBar(immuneTime);
        }
        
        protected override void InitShield(float immuneTime)
        {
            // 야매. 무적시간과 보호막 연출 시간을 분리시키기 위함 
            // 한방컷이 나면 그냥 죽어버리게 하고 
            // 한방컷이 안 나면 보호막이 보이도록 함 
            if (shield != null) shield.Show(0.5f);
        }
        
        protected override double GetFinalizedHp()
        {
            var abilityHp = InGameControlHub.My.AbilityController.ModifyHpDown(GetWeightedHp(), "HP_DOWN_ELITE_BOSS");
            return InGameControlHub.My.AbilityController.ModifyHpAllDown(abilityHp);
        }
        
        protected override double GetFinalizedRewardMineral()
        {
            var stageRewardMineral = GetWeightedRewardMineral();
            
            var abAllMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_ALL");
            var abEliteBossMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_ELITE_BOSS");
            
            return stageRewardMineral * (1f + (abAllMineralModifiedPer + abEliteBossMineralModifiedPer) / 100f);
        }
    }
}
