using System;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace InGame.Enemy
{
    public class EnemyTongue : MonoBehaviour
    {
        [Header("혓바닥 초당 이동 거리")] [SerializeField] private float tongueSpeedPerSec;

        [Header("혓바닥")] [SerializeField] private Transform tongue;
        [SerializeField] private Transform mouse;

        [SerializeField] private Ease tongueEase;
        
        private Vector3 _tongueOriginScale;
        private Tween _tongueTween;

        protected void Awake()
        {
            _tongueOriginScale = tongue.localScale;
        }

        private void OnDisable()
        {
            // 혀 초기화
            ResetToBegin();
        }

        public void ResetToBegin()
        {
            _tongueTween?.Kill();
            tongue.localScale = _tongueOriginScale;
        }

        public IEnumerator<float> _Eat(Vector3 to, Action<Vector3, float, Ease> arrived)
        {
            var from = mouse.position;
		
            // 대상에게 회전 TODO 
            /*
             * 이게 왜 되는지 이해 안됨
             * 그저 씬에서 어떤 각도로 회전해야 맞는지 이래저래 만지작대다 얻어걸림
             */
            tongue.rotation = Quaternion.Euler(0,0,from.y - to.y);
		
            // 혀 늘리기 
            var distance = Vector2.Distance(to, from);
            var duration = distance / tongueSpeedPerSec;
			
            var tongueLine = (Vector2)to - (Vector2)from;
            // 부모의 스케일이 크면 혀가 길어짐
            // 부모 스케일이 크면 혀가 작아지도록 처리 
            var tongueScaleX = tongueLine.magnitude / transform.parent.lossyScale.x;
            _tongueTween = tongue.DOScaleX(tongueScaleX, duration)
                .SetEase(tongueEase);
			
            // 혀 다 늘려질 때까지 대기 
            yield return Timing.WaitForSeconds(duration);
		
            // 도착 콜백  
            arrived?.Invoke(from, duration, tongueEase);
			
            // 혀 늘린거 다시 복귀 
            _tongueTween = tongue.DOScaleX(0f, duration)
                .SetEase(tongueEase);
	        
            // 혀 복귀 기다리기 
            yield return Timing.WaitForSeconds(duration);
        }
    }
}