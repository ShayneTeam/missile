﻿using System;
using System.Collections;
using System.Collections.Generic;
using MEC;
using UnityEngine;

namespace InGame.Enemy
{
    public class MoveLeft : MonoBehaviour
    {
        public float moveSpeed;
        private Transform _transform;

        private void Awake()
        {
            _transform = transform;
        }

        private void FixedUpdate()
        {
            var moveSpeedPerFrame = moveSpeed * Time.fixedDeltaTime;
            
            var newPos = _transform.position;
            newPos.x -= moveSpeedPerFrame;
            _transform.position = newPos;
        }
    }
}
