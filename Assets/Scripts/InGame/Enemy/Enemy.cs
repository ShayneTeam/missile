﻿using System;
using System.Collections.Generic;
using AsCoroutine;
using DG.Tweening;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Global.Buff;
using Lean.Pool;
using Ludiq;
using MEC;
using UI;
using UnityEngine;
using UnityEngine.Serialization;
using IPoolable = Lean.Pool.IPoolable;

namespace InGame.Enemy
{
    // 왠만하면 IMonster 라는 인터페이스로 하려고 했지만, 중복으로 쓰이는 함수와 멤버변수가 너무 많다 
    // 중복코드를 줄이고자 abstract class로 바꾼다
    public abstract class Enemy : MonoBehaviour, IPoolable
    {
        [FormerlySerializedAs("spriteRenderer")] [SerializeField] public SpriteRenderer skin;
        [SerializeField] public SpriteRenderer shadow;
        [SerializeField] public Animator animator;
        [SerializeField] protected Transform shakeTransform;
    
        [NonSerialized] public float ImmuneEndTime;
        [NonSerialized] public string DataIdx;

        [SerializeField] protected HealthBar healthBarPrefab;
        [SerializeField] protected Transform healthBarPivot;

        [SerializeField] protected float deadEffectScale = 0.5f;

        [SerializeField] public EnemyShield shield;

        private Vector3 DeadEffectPos => shadow.transform.position;

        private HealthBar _healthBar;
        protected HealthBar HealthBar
        {
            get => _healthBar;
            set
            {
                _healthBar = value;
            
                _healthBar.FollowTarget(healthBarPivot);
            
                RefreshHealthBar();
            }
        }
        private double _initHp;

        protected double InitHp
        {
            get => _initHp;
            set
            {
                _initHp = _currentHp = value;

                //
                RefreshHealthBar();
            }
        }
        private double _currentHp;

        public bool IsDied { get; private set; }

        public bool IsImmuneTime => Time.time <= ImmuneEndTime;

        public event EnemyDelegate OnDie;

        private Tweener _shaking;

        private Vector3 _originShakeLocalPos;
        private MoveLeft _moveLeft;
        
        private float _delayToShowHealthBar;


        #region Data Table Value

        public virtual int BasicHp { get; } = 0;
        public virtual string FxResourceId { get; set;  } = ""; // Enemy가 가진 속성이라기 보단, 외부에서 사용하기 쉽게 제공되는 속성에 가까움. 그러므로 set도 허용 
        public virtual float MoveSpeedRatio { get; } = 1f;
        public virtual string MoveType { get; } = "";
        public virtual float ImmuneTime { get; } = 0f;
        public virtual string ResourceId { get; } = "";
        public virtual int RewardMineral { get; } = 0;
        public virtual int RewardGas { get; } = 0;
        public virtual int RewardStone { get; } = 0;

        #endregion



        protected virtual void Awake()
        {
            // 이 Awake 함수를 상속 클래스에서 override 하면 안됨... 

            if (shakeTransform != null) _originShakeLocalPos = shakeTransform.localPosition;
        }


        public virtual void Init(string dataIdx)
        {
            
        }

        protected void InitCommon(string dataIdx)
        {
            DataIdx = dataIdx;
        }

        protected void InitBasic()
        {
            // 체력바 
            CreateHealthBar();
            
            // HP
            FinalizeAndInitHp();

            // 페이드인 
            var immuneTime = ImmuneTime;
            FadeIn(immuneTime);
            
            // 쉴드 
            InitShield(immuneTime);
        
            // 무적 타임 
            ImmuneEndTime = Time.time + immuneTime;
        }

        protected virtual void InitShield(float immuneTime)
        {
            if (shield != null)
                shield.Show(immuneTime);
        }

        protected void InitMove()
        {
            // 이동 속도 
            const float baseMoveSpeed = 0.8f; 
            var moveSpeed = baseMoveSpeed * MoveSpeedRatio;

            if (_moveLeft == null)
                _moveLeft = transform.parent.AddComponent<MoveLeft>();
            _moveLeft.moveSpeed = moveSpeed;

            // 이동 애니메이션  
            if (animator != null) animator.SetTrigger(MoveType); 
        }

        public void MultiplyMoveSpeed(float factor)
        {
            if (_moveLeft != null) _moveLeft.moveSpeed *= factor;
        }

        protected void FinalizeAndInitHp()
        {
            InitHp = GetFinalizedHp();
        }

        protected virtual double GetFinalizedHp()
        {
            return GetWeightedHp();
        }

        protected double GetWeightedHp()
        {
            return (EnemyMode.Behaviour as IGetEnemyHp)?.GetEnemyHp(BasicHp) ?? 0;
        }

        protected void CreateHealthBar()
        {
            HealthBar = LeanPool.Spawn(healthBarPrefab, InGamePrefabs.Instance.healthBarParent);
        }

        public virtual bool CanTarget()
        {
            return true;
        }

        protected virtual void FadeIn(float fadeInTime)
        {
            skin.DOFade(1f, fadeInTime).From(0f);
        
            // 그림자도 페이드인 처리해줘야 자연스러움 
            shadow.DOFade(1f, fadeInTime).From(0f);
        }

        public virtual void OnSpawn()
        {
            IsDied = false;
        }

        public virtual void OnDespawn()
        {
            // 체력바 생성은 각 클래스에서 하지만, 반납은 여기서 해준다 
            LeanPool.Despawn(HealthBar);

            //
            if (shakeTransform != null) shakeTransform.localPosition = _originShakeLocalPos; 
            
            // 중요. 이벤트 책임은 += 한쪽이 -= 까지 책임지는게 맞으나, 
            // LeanPool 특성상 DespawnAll을 언제든 호출할 수 있는게 문제 
            // 죽지 않았는데도 Despawn이 될 수 있어서, 이벤트가 +=는 되고, -=는 안되는 상황 연속적으로 발생할 수 있음
            // 그러므로, Despawn 되는 객체에서 Despawn될 때, 이벤트를 초기화 시킨다
            OnDie = null;
        }

        public virtual void Damage(double damage)
        {
            // 아직 무적 시간이라면 데미지 들어가지 않음 
            if (IsImmuneTime) return;
            
            _currentHp -= damage;

            RefreshHealthBar();
        
            // 흔들기
            Shake();
        
            // 데스 체크
            if (_currentHp <= 0)
                Die();
        }

        private void RefreshHealthBar()
        {
            if (HealthBar == null)
                return;

            // 쓸데없는 가비지 생성 방지 
            if (_currentHp < 0)
            {
                HealthBar.SetHp("0", 0);
                return;
            }
        
            var hpText = _currentHp.ToUnitString();
            var ratio = _currentHp / _initHp;
            HealthBar.SetHp(hpText, (float)ratio);
        }

        public virtual void Die()
        {
            IsDied = true;

            // 반드시 LeanPool.Despawn 전에 호출
            // 그래야만 등록된 이벤트들이 제대로 호출됨 
            // OnDespawn에서 초기화 되기 때문 
            OnDie?.Invoke(this);

            AfterDie();
            
            // 반납하면 부모가 끊어지기에 transform.position의 값에서 부모 transform 값이 빠지게 됨
            // 다른 처리 다 하고, 맨 마지막에 디스폰 할 것 
            LeanPool.Despawn(transform.parent.gameObject);  // 프리팹 안에 프리팹이 들어가는 구조라, 최상단 루트를 반납해야 한다 
        }

        private void AfterDie()
        {
            ShowDeadEffect();

            PlayDeadSound();

            EarnRewardMineral();
            EarnRewardGas();
            EarnRewardStone();
        }

        protected virtual void PlayDeadSound()
        {
            SoundController.Instance.PlayEffect("sfx_ui_die_enemy");
        }

        protected virtual void ShowDeadEffect()
        {
            DeadEffectDirector.Instance.ShowEnemyDead(DeadEffectPos, deadEffectScale);
        }

        protected void DelayShowHealthBar(float delay)
        {
            _delayToShowHealthBar = delay;
            HealthBar.gameObject.SetActive(false);
        }

        private void Update()
        {
            // 코루틴 안 쓰기 위한, Update 필살기
            
            // 딜레이가 0이면 바로 처리시키기 위해서, <= 을 쓰지 않고 < 을 씀 
            if (_delayToShowHealthBar < 0) return;
            _delayToShowHealthBar -= Time.deltaTime;
            if (_delayToShowHealthBar <= 0 && HealthBar != null) HealthBar.gameObject.SetActive(true);
        }

        private void Shake()
        {
            const float shakeDuration = 0.5f;
            
            if (_shaking == null)
            {
                _shaking = shakeTransform.DOShakePosition(shakeDuration, 0.15f, 50)
                    .SetAutoKill(false);
            }
            else
            {
                _shaking.Restart();
            }
        }

        protected virtual void EarnRewardMineral()
        {
            // 획득 할 미네랄 양이 없다면 바로 끝내기 
            if (RewardMineral == 0)
                return;

            // 미네랄 버프 
            var finalizedRewardMineral = GetFinalizedRewardMineral() * BuffMineral.Instance.MineralFactor;
			
            // 보상 추가시키기 
            ResourceController.Instance.AddMineral(finalizedRewardMineral);
        }

        protected virtual double GetFinalizedRewardMineral()
        {
            return GetWeightedRewardMineral();
        }

        protected double GetWeightedRewardMineral()
        {
            return (EnemyMode.Behaviour as IGetEnemyReward)?.GetRewardMineral(RewardMineral) ?? 0;
        }

        protected virtual void EarnRewardGas()
        {
            // 획득 할 미네랄 양이 없다면 바로 끝내기 
            if (RewardGas == 0)
                return;
            
            var finalizedRewardGas = GetFinalizedRewardGas();
			
            // 보상 추가시키기 
            ResourceController.Instance.AddGas(finalizedRewardGas);
        }
        
        protected virtual double GetFinalizedRewardGas()
        {
            return GetWeightedRewardGas();
        }
        
        protected double GetWeightedRewardGas()
        {
            return (EnemyMode.Behaviour as IGetEnemyReward)?.GetRewardGas(RewardGas) ?? 0;
        }
        
        protected virtual void EarnRewardStone()
        {
            // 획득 할 미네랄 양이 없다면 바로 끝내기 
            if (RewardStone == 0)
                return;

            var finalizedRewardStone = GetFinalizedRewardStone();
			
            // 보상 추가시키기 
            ResourceController.Instance.AddStone(finalizedRewardStone);
        }
        
        protected virtual double GetFinalizedRewardStone()
        {
            return GetWeightedRewardStone();
        }
        
        protected double GetWeightedRewardStone()
        {
            return (EnemyMode.Behaviour as IGetEnemyReward)?.GetRewardStone(RewardStone) ?? 0;
        }
    }

    public delegate void EnemyDelegate(Enemy arg);
}