﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Global;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame.Enemy
{
    public class Portal : Enemy
    {
        public override int BasicHp => Monster.GetBasicHp(DataIdx);
        public override string FxResourceId => Monster.GetFxResourceId(DataIdx);
        public override float MoveSpeedRatio => 0f;
        public override string MoveType => Monster.GetMoveType(DataIdx);
        public override float ImmuneTime => Monster.GetImmuneTime(DataIdx);
        public override string ResourceId => Monster.GetResourceId(DataIdx);
        public override int RewardMineral => Monster.GetRewardMineral(DataIdx);

        private CoroutineHandle _summoning;
        private bool _successFirstSummon;
        private List<Vector2> _spawnPosMonsters;

        public override void Init(string dataIdx)
        {
            InitCommon(dataIdx);

            // 
            InitBasic();

            // 무적 시간 후, 체력바 자동 보여주기 
            var immuneTime = DataboxController.GetDataFloat(Table.Stage, Sheet.Monster, DataIdx, Column.ImmuneTime);
            DelayShowHealthBar(immuneTime);

            // 시작 애니 (리스폰 후 다시 시작하게 만들기 위함)
            var tween = skin.GetComponent<DOTweenAnimation>();
            tween.DORestart();

            // 반복 소환
            _summoning = Timing.RunCoroutine(SummonRepeat().CancelWith(gameObject));
        }

        protected override void InitShield(float immuneTime)
        {
            // 야매. 무적시간과 보호막 연출 시간을 분리시키기 위함 
            // 한방컷이 나면 그냥 죽어버리게 하고 
            // 한방컷이 안 나면 보호막이 보이도록 함 
            if (shield != null) shield.Show(0.5f);
        }

        protected override double GetFinalizedHp()
        {
            var abilityHp = InGameControlHub.My.AbilityController.ModifyHpDown(GetWeightedHp(), "HP_DOWN_PORTAL");
            return InGameControlHub.My.AbilityController.ModifyHpAllDown(abilityHp);
        }

        protected override double GetFinalizedRewardMineral()
        {
            var stageRewardMineral = GetWeightedRewardMineral();

            var abAllMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_ALL");
            var abPortalMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_PORTAL");

            return stageRewardMineral * (1f + (abAllMineralModifiedPer + abPortalMineralModifiedPer) / 100f);
        }

        public override void OnDespawn()
        {
            base.OnDespawn();

            // 반복 소환 중지
            if (_summoning.IsRunning) Timing.KillCoroutines(_summoning);
        }

        public override void Damage(double damage)
        {
            // 첫 소환 전까진 데미지 받지 않음 
            if (!_successFirstSummon) return;
            
            base.Damage(damage);
        }

        private IEnumerator<float> SummonRepeat()
        {
            _successFirstSummon = false;

            // 
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out _, out var planetIdx);
            Wave.RandomPickMonster(planetIdx, "normal", out var monsterIdx, out var countMin, out var countMax, out var delayMin, out var delayMax);

            Enemies.Instance.GetSpawnArenaBounds(monsterIdx, Sheet.Monster, out var monsterArenaBounds);

            // 
            _spawnPosMonsters ??= new List<Vector2>();
            _spawnPosMonsters.Clear();

            while (true)
            {
                var spawnCount = Random.Range(countMin, countMax);

                for (var count = 0;
                    count < spawnCount;
                    count++)
                {
                    // 랜덤 위치 
                    Wave.GetRandomSpawnPos(ref monsterArenaBounds, ref _spawnPosMonsters, out var monsterSpawnPos);

                    // 일반 몬스터 스폰 
                    Enemies.Instance.SpawnMonster(monsterIdx, monsterSpawnPos);

                    // 각 마리 당, 스폰도 악간의 랜덤 간격을 둠 
                    var summonDelay = Random.Range(delayMin, delayMax) / Time.timeScale;
                    yield return Timing.WaitForSeconds(summonDelay);
                }

                _successFirstSummon = true;

                // 이 주기마다 랜덤 간격 소환 
                const float summonInterval = 8f;
                yield return Timing.WaitForSeconds(summonInterval);
            }
        }
    }
}