﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Global;
using InGame.Controller.Mode;
using InGame.Global;
using MEC;
using UI;
using UnityEngine;

namespace InGame.Enemy
{
    public class ManEater : Boss
    {
        [Header("혓바닥 초당 이동 거리")] [SerializeField] private float tongueSpeedPerSec;

        [Header("혓바닥")] [SerializeField] private Transform tongue;
        [SerializeField] private Transform mouse;

        [SerializeField] private Ease tongueEase;

        [Header("손")] 
        [SerializeField] private SpriteRenderer leftHand;
        [SerializeField] private SpriteRenderer rightHand;
        
        private TimeBar _timeBar;
        private Vector3 _tongueOriginScale;
        private Tween _tongueTween;

        protected override void Awake()
        {
	        base.Awake();

	        _tongueOriginScale = tongue.localScale;
        }

        public override void Init(string dataIdx)
        {
            base.Init(dataIdx);
            
            // 기본 초기화
            InitBasic();
            
            // 쿨타임바 갖고오기
            _timeBar = HealthBar.GetComponent<TimeBar>();

            // 스킬 시작 
            Timing.RunCoroutine(SkillFlow().CancelWith(gameObject));
        }

        public override void OnDespawn()
        {
	        base.OnDespawn();

	        // 혀 초기화
	        _tongueTween?.Kill();
	        tongue.localScale = _tongueOriginScale;
        }

        protected override void FadeIn(float fadeInTime)
        {
	        base.FadeIn(fadeInTime);
	        
	        if (leftHand != null)
				leftHand.DOFade(1f, fadeInTime).From(0f);
	        
	        if (rightHand != null)
				rightHand.DOFade(1f, fadeInTime).From(0f);
        }

        private IEnumerator<float> SkillFlow()
        {
	        // 먹보 페이드인 기다리기 
	        yield return Timing.WaitForSeconds(ImmuneTime);	
	        
            // 스킬 사용 횟수 
            var skillUsingCount = 0;  
					
            // 스킬 쿨타임 
            var coolTimeSec = DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "boss_skill_value_1", 8f);
					
            // 스킬 사용 될때마다, 스킬 사용 횟수 * 감소 쿨타임 시간 
            var coolTimeDecreasingSec = DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "boss_skill_value_2", 1f);
					
            // 쿨타임 최소치 
            var coolTimeMin = DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "boss_skill_value_3", 1f);
					
            // 모든 용병 가져오기 
            var soldiers = Army.My.Soldiers;
					
            while (soldiers.Count > 0)
            {
	            // 용병 하나 랜덤 뽑기 
	            var soldier = soldiers[Random.Range(0, soldiers.Count - 1)];

	            // 스킬 사용 
	            yield return Timing.WaitUntilDone(Eat(soldier).CancelWith(gameObject));

	            // 스킬 사용 횟수 
	            skillUsingCount++;

	            // 쿨타임 차감 시간 
	            var decreasingCoolTimeSec = skillUsingCount * coolTimeDecreasingSec;

	            // 차감된 쿨타임 시간 
	            var decreasedCoolTime = coolTimeSec - decreasingCoolTimeSec;

	            // 쿨타임 최소치 보정
	            coolTimeSec = Mathf.Clamp(decreasedCoolTime, coolTimeMin, decreasedCoolTime);

	            // 쿨타임 기다리기 
	            var remainTimeSec = coolTimeSec;
	            while (remainTimeSec > 0f)
	            {
		            remainTimeSec -= Time.deltaTime;
		            var remainTimeRate = remainTimeSec / coolTimeSec;
		            _timeBar.SetAmount(remainTimeRate);

		            yield return Timing.WaitForOneFrame;
	            }
	            _timeBar.SetAmount(0f);
            }
					
            // 모든 용병 다 죽였다면, 마지막으로 캐릭터 죽이기 
            yield return Timing.WaitUntilDone(Eat(Army.My.Character).CancelWith(gameObject));
					
            // 실패 처리 
            (EnemyMode.Behaviour as IDefeat)?.Defeat();
        }
        
        private IEnumerator<float> Eat(Ally ally)
        {
	        if (ally == null) yield break;
	        
	        var from = mouse.position;
	        var to = ally.transform.position;
		
	        // 대상에게 회전 TODO 
	        /*
	         * 이게 왜 되는지 이해 안됨
	         * 그저 씬에서 어떤 각도로 회전해야 맞는지 이래저래 만지작대다 얻어걸림
	         */
	        tongue.rotation = Quaternion.Euler(0,0,from.y - to.y);
		
	        // 혀 늘리기 
	        var distance = Vector2.Distance(to, from);
	        var duration = distance / tongueSpeedPerSec;
			
	        var tongueLine = to - from;
	        _tongueTween = tongue.DOScaleX(tongueLine.magnitude, duration)
		        .SetEase(tongueEase);
			
	        // 혀 다 늘려질 때까지 대기 
	        yield return Timing.WaitForSeconds(duration);
		
	        // 혀 다 늘려졌다면 대상 죽이기 
	        ally.Die();
			
	        // 혀 늘린거 다시 복귀 
	        _tongueTween = tongue.DOScaleX(0f, duration)
		        .SetEase(tongueEase);
	        
	        // 혀 복귀 기다리기 
	        yield return Timing.WaitForSeconds(duration);
        }
    }
}