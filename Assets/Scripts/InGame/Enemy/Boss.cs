﻿using System.Collections.Generic;
using Global;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using UnityEngine;

namespace InGame.Enemy
{
    public class Boss : Enemy
    {
        public override int BasicHp => DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.BasicHp);
        public override string FxResourceId => DataboxController.GetDataString(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "fx_resource_id", "dmg_fx_enemy_01");
        public override float MoveSpeedRatio => DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.MoveSpeed, 1f);
        public override string MoveType => DataboxController.GetDataString(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.MoveType, "hold");
        public override float ImmuneTime => DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.ImmuneTime);
        public override string ResourceId => DataboxController.GetDataString(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.resource_id);
        public override int RewardMineral => DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.RewardMineral, 100);

        protected virtual int RewardDiabloKey => DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.RewardDiablokey, 1);
        protected virtual int RewardKillPoint => DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.RewardKillpoint, 1);
        protected virtual string RewardGroup => DataboxController.GetDataString(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.RewardGroup, "reward_group_1");
        protected virtual int RewardChip => DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, Column.reward_chip, 1);

        public override void Init(string dataIdx)
        {
            InitCommon(dataIdx);
        }

        public override void Die()
        {
            // 보스 보상 획득
            EarnBossReward();
            
            // 보스의 Show Dead Effect는 상자 가챠 처리 후에 나오게 해야 함 
            base.Die();
        }

        protected override void ShowDeadEffect()
        {
            base.ShowDeadEffect();
            
            // 보스 전용 데드 이펙트 
            EffectSpawner.Instance.Spawn("boss_die", transform.position);
        }
        
        protected override void PlayDeadSound()
        {
            SoundController.Instance.PlayEffect("sfx_ui_die_boss");
        }

        protected virtual void EarnBossReward()
        {
            (EnemyMode.Behaviour as IEarnBossReward)?.EarnBossReward(RewardDiabloKey, RewardChip, RewardKillPoint, RewardGroup, GetBossRewardEffectPos());
        }

        protected virtual Vector3 GetBossRewardEffectPos()
        {
            return shadow.transform.position;
        }
        
        protected override double GetFinalizedHp()
        {
            var abilityHp = InGameControlHub.My.AbilityController.ModifyHpDown(GetWeightedHp(), "HP_DOWN_ELITE_BOSS");
            return InGameControlHub.My.AbilityController.ModifyHpAllDown(abilityHp);
        }
        
        protected override double GetFinalizedRewardMineral()
        {
            var weightedRewardMineral = GetWeightedRewardMineral();
            
            var abAllMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_ALL");
            var abEliteBossMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_ELITE_BOSS");
            
            return weightedRewardMineral * (1f + (abAllMineralModifiedPer + abEliteBossMineralModifiedPer) / 100f);
        }
    }
}