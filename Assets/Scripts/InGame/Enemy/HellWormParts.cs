﻿using Lean.Pool;
using UnityEngine;

namespace InGame.Enemy
{
    public class HellWormParts : Boss
    {
        public override void Init(string dataIdx)
        {
            base.Init(dataIdx);
            
            //
            InitCommon(dataIdx);
	
            // 기본 
            InitBasic();
            
            // 이동
            InitMove();
            
            // 체력바는 한 대 맞은 후부터 보여준다
            HealthBar.gameObject.SetActive(false);   
        }
        
        public override void Damage(double damage)
        {
            base.Damage(damage);
	    
            // 받은 뒤에야 체력바 보여주기 
            HealthBar.gameObject.SetActive(true);
        }

        protected override void EarnRewardMineral()
        {
            // 헬웜 파츠는 미네랄 획득 보상 처리 하지 않음 
        }

        protected override void EarnBossReward()
        {
            // 헬웜 파츠는 보스 보상 획득 하지 않음 
        }
    }
}