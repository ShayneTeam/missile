﻿using System.Collections;
using System.Collections.Generic;
using Global;
using InGame.Controller.Mode;
using InGame.Global;
using Lean.Pool;
using MEC;
using UnityEngine;

namespace InGame.Enemy
{
    public class HellWorm : Boss
    {
        [SerializeField] private GameObject partsPrefab;

        [SerializeField] private float eachPartsDistancingX = 1f;

        private readonly List<Enemy> _parts = new List<Enemy>();

        private Vector3 _lastDiedPartsShadowPos;

        // 헬웜이 너무 빨리 죽을 때, 파츠가 다 생성되지도 않았는데 Die 호출되는거 방지  
        private int _partsCount;

        public override void Init(string dataIdx)
        {
            base.Init(dataIdx);

            //
            Timing.RunCoroutine(CreatePartsFlow().CancelWith(gameObject));
        }

        public override bool CanTarget()
        {
            // 헬웜 본체는 타게팅이 되지 않는 추상적인 존재임
            // 타게팅은 헬웜 파츠만 가능 
            return false;
        }

        private IEnumerator<float> CreatePartsFlow()
        {
            // 바디갯수만큼 생성 
            var bodyCount = DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "boss_skill_value_1", 6);
            _partsCount = bodyCount;
            
            // 소환 딜레이 
            var partsDelay = DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "boss_skill_value_2", 0.3f);

            for (var i = 0; i < bodyCount; i++)
            {
                /*
                 * 헬웜 본체와 헬웜 파츠는 독립적으로 동작하게 함 
                 * 본체와 파츠를 부모자식으로 두는게 직관적이나, LeanPool 특성상 부모가 Despawn 될 시 자식 전부 IPoolable의 OnDespawn 호출
                 * 디버깅상으로 알 수 없는 LeanPool index 에러가 발생함
                 * LeanPool 사용의 일관성을 맞추기 위해 LeanPool의 별도 사용처리는 안하고, 헬웜의 본체와 파츠 프리팹이 다르니, 처리도 다르게 하는게 맞다고 생각하고 그렇게 처리
                 */
                var newParts = LeanPool.Spawn(partsPrefab, transform.position, Quaternion.identity, Enemies.Instance.transform);

                // 바디 이름을 인덱스로 만들어서, 기존 Enemy들과의 계층구조 일관성 맞춰주기 
                newParts.name = $"HellWorm Parts ({(i + 1).ToLookUpString()})";

                // 바디간의 간격 주기 
                var distancing = i * eachPartsDistancingX;

                var distancingPos = newParts.transform.position;
                distancingPos.x += distancing;

                newParts.transform.position = distancingPos;

                //
                var enemyOfParts = newParts.GetComponentInChildren<Enemy>();
                enemyOfParts.Init(DataIdx);
                enemyOfParts.skin.sortingOrder = -i; // 먼저 생성될수록 먼저 보이게 하기 
                enemyOfParts.OnDie += OnDieParts;
                
                // 두 번째 파츠부터는 쉴드 끔 
                if (i > 0)
                {
                    if (enemyOfParts.shield != null)
                        enemyOfParts.shield.Hide();
                }

                //
                _parts.Add(enemyOfParts);
                
                // 타게팅할 수 있도록 Enemies에 추가 
                Enemies.Instance.AddEnemy(enemyOfParts);
                
                /*
                 * 파츠간 생성 딜레이
                 * 딜레이가 있어야 애니메이션끼리 차이가 생겨, 지렁이 같은 꿈틀꿈틀 처리 가능 
                 */
                yield return Timing.WaitForSeconds(partsDelay);
            }
        }

        private void OnDieParts(Enemy enemyOfParts)
        {
            _parts.Remove(enemyOfParts);

            // 이벤트 빼주기 (중요)
            enemyOfParts.OnDie -= OnDieParts;
            
            //
            Enemies.Instance.RemoveEnemy(enemyOfParts);
            
            // 파츠가 하나씩 날라갈 때마다 이속 증가 
            var speedFactor = DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "boss_skill_value_3", 1.2f);
            foreach (var parts in _parts)
            {
                parts.MultiplyMoveSpeed(speedFactor);
            }

            // 파츠 하나도 없다면 사망 처리 
            _partsCount--;
            if (_partsCount <= 0)
            {
                 // 가장 마지막 파츠의 그림자 위치에 보상 이펙트 출력 
                _lastDiedPartsShadowPos = enemyOfParts.shadow.transform.position;
                
                Die();
            }
        }
        
        protected override void ShowDeadEffect()
        {
            // 헬웜 본체는 데드이펙트를 별도의 위치로 뿌림
        }
        
        protected override void PlayDeadSound()
        {
            
        }

        public override void OnDespawn()
        {
            base.OnDespawn();
            
            // 바디 스크립트의 부모를 반납해야 함 
            foreach (var parts in _parts)
            {
                LeanPool.Despawn(parts.transform.parent);
                
                Enemies.Instance.RemoveEnemy(parts);
            }

            _parts.Clear();
        }

        protected override Vector3 GetBossRewardEffectPos()
        {
            return _lastDiedPartsShadowPos;
        }
    }
}