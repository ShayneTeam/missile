﻿using Global;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Global;

namespace InGame.Enemy
{
    public class Monster : Enemy
    {
        /*
     * 클래스의 로직 공유 방법
     * 1. static 함수
     * 2. 최상단 루트(Enemy)에서 base 함수 작성
     * 
     * 몬스터는 최상단 루트가 아니기에, static 함수로 공유 
     */

        #region Data Table Value

        public override int BasicHp => GetBasicHp(DataIdx);
        public static int GetBasicHp(string idx)
        {
            return DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.Monster, idx, Column.BasicHp);
        }

        public override string FxResourceId => GetFxResourceId(DataIdx);
        public static string GetFxResourceId(string idx)
        {
            return DataboxController.GetDataString(EnemyMode.Behaviour.MainTable, Sheet.Monster, idx, "fx_resource_id", "dmg_fx_enemy_01");
        }
    
        public override float MoveSpeedRatio => GetMoveSpeedRatio(DataIdx);
        public static float GetMoveSpeedRatio(string idx)
        {
            return DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.Monster, idx, Column.MoveSpeed, 1f);
        }
    
        public override string MoveType => GetMoveType(DataIdx);
        public static string GetMoveType(string idx)
        {
            return DataboxController.GetDataString(EnemyMode.Behaviour.MainTable, Sheet.Monster, idx, Column.MoveType, "hold");
        }

        public override float ImmuneTime => GetImmuneTime(DataIdx);
        public static float GetImmuneTime(string idx)
        {
            return DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.Monster, idx, Column.ImmuneTime);
        }

        public override string ResourceId => GetResourceId(DataIdx);
        public static string GetResourceId(string idx)
        {
            return DataboxController.GetDataString(EnemyMode.Behaviour.MainTable, Sheet.Monster, idx, Column.resource_id);
        }

        public override int RewardMineral => GetRewardMineral(DataIdx);
        public static int GetRewardMineral(string idx)
        {
            return DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.Monster, idx, Column.RewardMineral);
        }
        
        public override int RewardGas => GetRewardGas(DataIdx);
        public static int GetRewardGas(string idx)
        {
            return DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.Monster, idx, Column.reward_gas);
        }
        
        public override int RewardStone => GetRewardStone(DataIdx);
        public static int GetRewardStone(string idx)
        {
            return DataboxController.GetDataInt(EnemyMode.Behaviour.MainTable, Sheet.Monster, idx, Column.reward_stone);
        }

        #endregion

        public override void Init(string dataIdx)
        {
            InitCommon(dataIdx);

            // 기본 
            InitBasic();

            // 이동 
            InitMove();
        
            // 몬스터 체력바는 한 대 맞은 후부터 보여준다
            HealthBar.gameObject.SetActive(false);   
        }

        public override void Damage(double damage)
        {
            base.Damage(damage);
        
            // 일반 잡몹은 데미지가 한방 들어와야 보여줌 
            // 물론 거의 한방이라 보여질 틈도 없을꺼임
            HealthBar.gameObject.SetActive(true);
        }

        protected override double GetFinalizedHp()
        {
            var abilityHp = InGameControlHub.My.AbilityController.ModifyHpDown(GetWeightedHp(), "HP_DOWN_MONSTER_GOLEM");
            return InGameControlHub.My.AbilityController.ModifyHpAllDown(abilityHp);
        }

        protected override double GetFinalizedRewardMineral()
        {
            var stageRewardMineral = GetWeightedRewardMineral();
            
            var abAllMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_ALL");
            
            return stageRewardMineral * (1f + (abAllMineralModifiedPer / 100f));
        }
    }
}