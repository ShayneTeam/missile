﻿using System;
using System.Collections.Generic;
using System.Linq;
using Global;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Global;
using Lean.Pool;
using MEC;
using UI;
using UnityEngine;
using Random = UnityEngine.Random;


namespace InGame.Enemy
{
    [RequireComponent(typeof(SummonRepeat))]
    public class Diablo : Boss
    {
        public override int BasicHp => DataboxController.GetDataInt(Table.Stage, Sheet.Diablo, DataIdx, Column.BasicHp);
        public override string FxResourceId => DataboxController.GetDataString(Table.Stage, Sheet.Diablo, DataIdx, "fx_resource_id", "dmg_fx_enemy_01");
        public override float MoveSpeedRatio => DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, DataIdx, Column.MoveSpeed, 1f);
        public override string MoveType => DataboxController.GetDataString(Table.Stage, Sheet.Diablo, DataIdx, Column.MoveType, "hold");
        public override float ImmuneTime => DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, DataIdx, Column.ImmuneTime);
        public override string ResourceId => DataboxController.GetDataString(Table.Stage, Sheet.Diablo, DataIdx, Column.resource_id);
        public override int RewardMineral => DataboxController.GetDataInt(Table.Stage, Sheet.Diablo, DataIdx, Column.RewardMineral, 100);
        protected override int RewardDiabloKey => DataboxController.GetDataInt(Table.Stage, Sheet.Diablo, DataIdx, Column.RewardDiablokey, 1);
        protected virtual int RewardKillPoint => DataboxController.GetDataInt(Table.Stage, Sheet.Diablo, DataIdx, Column.RewardKillpoint, 0);
        protected override string RewardGroup => DataboxController.GetDataString(Table.Stage, Sheet.Diablo, DataIdx, Column.RewardGroup, "reward_group_1");

        private enum Phase
        {
            Summon,
            Shield,
            Stun,
            Fire
        }

        private Phase _currentPhase;

        /*
         * 시간제한 관련
         */
        private TimeBar _timeBar;
        private CoroutineHandle _limitTime;

        /*
         * 페이즈 1. 소환 관련
         */
        private SummonRepeat _summon;

        /*
         * 각 페이즈 스킬 중 하나가 발동되면, 다른 스킬 공통 일시정지 
         */
        private bool _isPhaseCoolTime;
        private float _phaseCoolTimeSec;

        /*
         * 페이즈 2. 보호막 관련 
         */
        [SerializeField] private Transform shieldPivot;

        [SerializeField] private GameObject shieldPrefab;

        /*
         * 페이즈 3. 스턴 관련 
         */
        [SerializeField] private DiabloStunHand stunHand;

        [SerializeField] private Sprite[] stunAnimSprites;

        [SerializeField] private float stunAnimIntervalSec;

        /*
         * 페이즈 4. 발사 관련  
         */
        [SerializeField] private Transform firePivot;
        [SerializeField] private GameObject ballPrefab;

        private readonly List<Enemy> _balls = new List<Enemy>();

        public static int UnlockPlanetIndex => int.Parse(DataboxController.GetConstraintsData(Constraints.DIABLO_UNLOCK_PLANET_INDEX));


        protected override void Awake()
        {
            base.Awake();
            
            _summon = GetComponent<SummonRepeat>();
        }

        public override void Init(string dataIdx)
        {
            base.Init(dataIdx);

            // 기본 초기화
            InitBasic();
            
            // 발사 페이즈 애니 자동 꺼두기 
            firePivot.gameObject.SetActive(false);

            // 첫 페이즈 시작 
            _currentPhase = Phase.Summon;
            Timing.RunCoroutine(SummonFlow().CancelWith(gameObject));
        }

        public override bool CanTarget()
        {
            return Time.time > ImmuneEndTime;
        }

        protected override double GetFinalizedHp()
        {
            var abilityHp = InGameControlHub.My.AbilityController.ModifyHpDown(GetWeightedHp(), "HP_DOWN_DIABLO");
            return InGameControlHub.My.AbilityController.ModifyHpAllDown(abilityHp);
        }

        protected override double GetFinalizedRewardMineral()
        {
            var stageRewardMineral = GetWeightedRewardMineral();
            
            var abAllMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_ALL");
            var abDiabloMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_DIABLO");
            
            return stageRewardMineral * (1f + (abAllMineralModifiedPer + abDiabloMineralModifiedPer) / 100f);
        }
        
        public override void Die()
        {
            switch (_currentPhase)
            {
                case Phase.Summon:
                {
                    _currentPhase = Phase.Shield;
                    Timing.RunCoroutine(ShieldFlow().CancelWith(gameObject).CancelWith(() => !IsDied));
                    SoundController.Instance.PlayEffect("sfx_ui_die_boss");
                }
                    break;

                case Phase.Shield:
                {
                    _currentPhase = Phase.Stun;
                    Timing.RunCoroutine(StunFlow().CancelWith(gameObject).CancelWith(() => !IsDied));
                    SoundController.Instance.PlayEffect("sfx_ui_die_boss");
                }
                    break;

                case Phase.Stun:
                {
                    _currentPhase = Phase.Fire;
                    Timing.RunCoroutine(FireFlow().CancelWith(gameObject).CancelWith(() => !IsDied));
                    SoundController.Instance.PlayEffect("sfx_ui_die_boss");
                }
                    break;

                case Phase.Fire:
                {
                    // 소환된 모든 몬스터 제거 
                    _summon.DieAll();

                    // 볼 제거 (Foreach가 아닌, for 역순으로 돌려야, OnDieBall 이벤트에서 Remove 시킬 수 있음) 
                    for (var i = _balls.Count-1; i >= 0; i--)
                    {
                        _balls[i].Die();
                    }

                    // 마지막 페이즈에서 죽으면 진짜 죽기
                    /*
                     * 모든 처리가 끝난 후, 가장 마지막에 Die 호출해야 함
                     * 볼은 Die 호출 시, Enemies OnEliminated을 호출하지 않음
                     * 수동으로 AddEnemy 했기 때문
                     * OnEliminated을 호출할 수 있는건 Enemies에 스폰한 Diablo 본체의 Die만 가능
                     */
                    base.Die();
                }
                    break;

                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #region Phase

        private void InitPhase(string dataIdx)
        {
            // 새로운 인덱스로 데이터 재갱신하기 위한 대입 
            DataIdx = dataIdx;

            // 스킨 갱신
            skin.sprite = AtlasController.GetSprite(ResourceId);

            // 체력 갱신 
            FinalizeAndInitHp();

            // 타임바 갱신 
            var limitTimeSec = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, dataIdx, "limited_time", 30f);
            Timing.KillCoroutines(_limitTime); // 기존 타이머 날림 
            _limitTime = Timing.RunCoroutine(LimitTimeFlow(limitTimeSec).CancelWith(gameObject)); // 새로운 페이즈의 시간제한으로 재설정
        }

        private IEnumerator<float> LimitTimeFlow(float timeSec)
        {
            if (_timeBar == null)
                _timeBar = HealthBar.GetComponent<TimeBar>();

            var remainTimeSec = timeSec;
            while (remainTimeSec > 0f)
            {
                remainTimeSec -= Time.deltaTime;
                var remainTimeRate = remainTimeSec / timeSec;

                _timeBar.SetAmount(remainTimeRate);

                yield return Timing.WaitForOneFrame;
            }

            _timeBar.SetAmount(0f);

            // 타임오버되면 바로 실패 
            Stage.Instance.Fail();
        }

        private IEnumerator<float> SummonFlow()
        {
            var allPhaseIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.Diablo);
            var summonPhaseIdx = allPhaseIdxes.ElementAt((int) Phase.Summon);

            InitPhase(summonPhaseIdx);

            // 소환 간격 Min
            var intervalSecMin = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, summonPhaseIdx, "boss_special_value_1", 0.1f);

            // 소환 간격 Max 
            var intervalSecMax = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, summonPhaseIdx, "boss_special_value_2", 0.3f);

            // 이동 속도 배율 
            var moveSpeedFactor = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, summonPhaseIdx, "boss_special_value_3", 2f);

            var summonList = (EnemyMode.Behaviour as IGetSummonListDiablo)?.GetSummonListDiablo();
            _summon.StartSummon(ImmuneTime, intervalSecMin, intervalSecMax, moveSpeedFactor, summonList);

            yield return Timing.WaitForOneFrame;
        }

        private IEnumerator<float> ShieldFlow()
        {
            var allPhaseIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.Diablo);
            var shieldPhaseIdx = allPhaseIdxes.ElementAt((int) Phase.Shield);

            InitPhase(shieldPhaseIdx);

            // 스킬 쿨타임 간격 Min
            var skillCoolTimeSecMin = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, shieldPhaseIdx, "boss_special_value_1", 0.1f);
            // 스킬 쿨타임 간격 Max 
            var skillCoolTimeSecMax = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, shieldPhaseIdx, "boss_special_value_2", 0.3f);
            // 속도 배율 
            var moveSpeedFactor = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, shieldPhaseIdx, "boss_special_value_3", 2f);
            // 페이즈 쿨타임 
            _phaseCoolTimeSec = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, shieldPhaseIdx, "phase_action_delay", 2f);

            while (true)
            {
                // 보호막 스폰 
                var shield = LeanPool.Spawn(shieldPrefab, shieldPivot.position, Quaternion.identity);
                var moveLeft = shield.GetComponent<MoveLeft>();
                moveLeft.moveSpeed *= moveSpeedFactor;

                // 보호막 자동 반납 (여유있게 10초 있다가 반납)
                LeanPool.Despawn(shield, 10f);

                // 페이즈 쿨타임 돌리기 
                Timing.RunCoroutine(PhaseCoolTimeFlow().CancelWith(gameObject));

                // 랜덤 스킬 쿨타임 간격
                var skillCoolTimeSec = Random.Range(skillCoolTimeSecMin, skillCoolTimeSecMax);
                yield return Timing.WaitUntilDone(SkillCoolTimeFlow(skillCoolTimeSec).CancelWith(gameObject));
                yield return Timing.WaitForOneFrame;
            }
        }

        private IEnumerator<float> StunFlow()
        {
            var allPhaseIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.Diablo);
            var stunPhaseIdx = allPhaseIdxes.ElementAt((int) Phase.Stun);

            InitPhase(stunPhaseIdx);

            // 스킬 쿨타임 간격 Min
            var skillCoolTimeSecMin = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, stunPhaseIdx, "boss_special_value_1", 0.1f);
            // 스킬 쿨타임 간격 Max 
            var skillCoolTimeSecMax = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, stunPhaseIdx, "boss_special_value_2", 0.3f);
            // 스턴 시간  
            var stunTimeSec = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, stunPhaseIdx, "boss_special_value_3", 2f);
            // 페이즈 쿨타임 
            _phaseCoolTimeSec = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, stunPhaseIdx, "phase_action_delay", 2f);

            while (true)
            {
                // 기존 스킨 저장 
                var originSkin = skin.sprite;

                // 애니메이션 
                foreach (var sprite in stunAnimSprites)
                {
                    // 애니 스프라이트 전환 
                    skin.sprite = sprite;

                    // 스프라이트 전환 간격
                    yield return Timing.WaitForSeconds(stunAnimIntervalSec);
                }

                // 지상 대상 찾기 
                Army.My.PickOneWithoutAirShip(out var ally);

                if (ally != null)
                {
                    // 대상 스턴 시키기 
                    ally.StopAutoShoot();

                    // 손 솟아오르는 연출 
                    var async = stunHand.ComeUp(ally.ShadowPos, stunTimeSec, ally.body);
                    while (!async.IsDone)
                        yield return Timing.WaitForOneFrame; // 연출 끝날 때까지 기다리기 

                    // 대상 스턴 품 
                    ally.StartAutoShoot();
                }

                // 원래 스킨으로 돌아감 
                skin.sprite = originSkin;

                // 페이즈 쿨타임 돌리기
                Timing.RunCoroutine(PhaseCoolTimeFlow().CancelWith(gameObject));

                // 랜덤 스킬 쿨타임 간격
                var skillCoolTimeSec = Random.Range(skillCoolTimeSecMin, skillCoolTimeSecMax);
                yield return Timing.WaitUntilDone(SkillCoolTimeFlow(skillCoolTimeSec).CancelWith(gameObject));
            }
        }

        private IEnumerator<float> FireFlow()
        {
            var allPhaseIdxes = DataboxController.GetAllIdxes(Table.Stage, Sheet.Diablo);
            var firePhaseIdx = allPhaseIdxes.ElementAt((int) Phase.Fire);

            InitPhase(firePhaseIdx);
            
            // 발사 위치에 애니
            firePivot.gameObject.SetActive(true);

            // 스킬 쿨타임 간격 Min
            var skillCoolTimeSecMin = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, firePhaseIdx, "boss_special_value_1", 0.1f);
            // 스킬 쿨타임 간격 Max 
            var skillCoolTimeSecMax = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, firePhaseIdx, "boss_special_value_2", 0.3f);
            // 구체 이동속도 배율  
            var speedFactor = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, firePhaseIdx, "boss_special_value_3", 2f);
            // 구체 체력 배율  
            var hpRate = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, firePhaseIdx, "boss_special_value_4", 0.2f);
            var ballHp = InitHp * hpRate;
            // 페이즈 쿨타임 
            _phaseCoolTimeSec = DataboxController.GetDataFloat(Table.Stage, Sheet.Diablo, firePhaseIdx, "phase_action_delay", 2f);

            while (true)
            {
                // 구체 스폰 
                var spawn = LeanPool.Spawn(ballPrefab, firePivot.position, Quaternion.identity);
                var ball = spawn.GetComponentInChildren<DiabloBall>();
                ball.Init(ballHp, speedFactor);
                ball.FxResourceId = FxResourceId;
                ball.OnDie += OnDieBall;
                
                _balls.Add(ball);
                
                // 타게팅할 수 있도록 추가  
                Enemies.Instance.AddEnemy(ball);
                
                // 페이즈 쿨타임 돌리기
                Timing.RunCoroutine(PhaseCoolTimeFlow().CancelWith(gameObject));

                // 랜덤 스킬 쿨타임 간격
                var skillCoolTimeSec = Random.Range(skillCoolTimeSecMin, skillCoolTimeSecMax);
                yield return Timing.WaitUntilDone(SkillCoolTimeFlow(skillCoolTimeSec).CancelWith(gameObject));
            }
        }

        private void OnDieBall(Enemy ball)
        {
            Enemies.Instance.RemoveEnemy(ball);
            
            ball.OnDie -= OnDieBall;

            _balls.Remove(ball);
        }

        private IEnumerator<float> PhaseCoolTimeFlow()
        {
            // 이 코루틴이 한 번 더 들어올 때의 예외사항 처리 
            if (_isPhaseCoolTime)
                yield break;

            _isPhaseCoolTime = true;

            // 페이즈 쿨타임 시간 동안 스킬 사용 못하게 막음  
            yield return Timing.WaitForSeconds(_phaseCoolTimeSec);

            _isPhaseCoolTime = false;
        }

        private IEnumerator<float> SkillCoolTimeFlow(float skillCoolTimeSec)
        {
            // 다른 페이즈나, 현재 페이즈의 사용으로 페이즈 쿨타임 발동 시, 대기 
            /*
             * 요부분은 해킹에 가까움
             * 구현을 알지 못하면 이해하기 어려움
             * 그럼에도 이렇게 구현한 이유는, OOP로 분리구현 시 지금보다 더 복잡해지기 때문  
             */
            var remainTimeSec = skillCoolTimeSec;
            while (remainTimeSec > 0f)
            {
                if (!_isPhaseCoolTime)
                    remainTimeSec -= Time.deltaTime;

                yield return Timing.WaitForOneFrame;
            }
        }

        #endregion
    }
}