﻿using System.Collections;
using UnityEngine;

namespace InGame.Enemy
{
    public class DiabloBall : Enemy
    {
        public void Init(double hp, float speedRate)
        {
            CreateHealthBar();

            InitHp = hp;

            // 이동 
            InitMove();

            // 이동 속도 배율 반영  
            MultiplyMoveSpeed(speedRate);

            // 체력바는 한 대 맞은 후부터 보여준다
            HealthBar.gameObject.SetActive(false);
        }

        public override void Damage(double damage)
        {
            base.Damage(damage);

            // 데미지가 한방 들어와야 보여줌 
            HealthBar.gameObject.SetActive(true);
        }

        protected override void ShowDeadEffect()
        {
            // 보스 전용 데드 이펙트 
            EffectSpawner.Instance.Spawn("boss_die", transform.position);
        }
        
        protected override void PlayDeadSound()
        {
            
        }
    }
}