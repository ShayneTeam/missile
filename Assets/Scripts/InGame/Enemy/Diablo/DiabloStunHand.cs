﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Global;
using MEC;
using UnityEngine;

namespace InGame.Enemy
{
    public class DiabloStunHand : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer hand;
        [SerializeField] private SpriteRenderer dirt;
	
        [SerializeField] private float dirtFadeInDuration;
        
        [SerializeField] private float comeUpDuration;
        [SerializeField] private Ease comeUpEase;
		
	
        [SerializeField] private float comeBackDuration;
        [SerializeField] private Ease comeBackEase;
		
        [SerializeField] private float dirtFadeOutDuration;
        
        [SerializeField] private float targetUpYValue;
		
        private Vector3 _handOriginLocalPos;

        private readonly CustomAsync _async = new CustomAsync();
        private CoroutineHandle _comeUp;

        private Vector3 _originLocalPos;
        private Tweener _targetUp;

        private void Awake()
        {
	        _originLocalPos = transform.position;
	        _handOriginLocalPos = hand.transform.localPosition;
        }

        private void OnDisable()
        {
	        // 위치 리셋 
	        transform.position = _originLocalPos;
	        
	        // 타겟 복원
	        if (_targetUp != null)
	        {
		        _targetUp.Rewind();
		        _targetUp.Kill();
	        }
        }

        public CustomAsync ComeUp(Vector3 pos, float duration, Transform target)
        {
	        _async.IsDone = _async.IsSuccess = false;
		        
	        // 타겟 위치로 이동
	        transform.position = pos;

	        // 연출 
	        _comeUp = Timing.RunCoroutine(_ComeUp(duration, target).CancelWith(gameObject));

	        return _async;
        }
		
        private IEnumerator<float> _ComeUp(float duration, Transform target)
        {
	        // 흙더미 페이드인
	        dirt.DOFade(1f, dirtFadeInDuration)
		        .From(0f);

	        // 손 뚫고 나오기 
	        hand.transform.DOLocalMoveY(_handOriginLocalPos.y, comeUpDuration)
		        .From(Vector3.down * 2)
		        .SetEase(comeUpEase);
	        
	        // 타겟 올리기
	        _targetUp = target.DOLocalMoveY(targetUpYValue, comeUpDuration)
		        .SetEase(comeUpEase)
		        .SetRelative(true)
		        .SetAutoKill(false);

	        yield return Timing.WaitForSeconds(comeUpDuration);

	        // 손 꺼내있는 시간 
	        yield return Timing.WaitForSeconds(duration);

	        // 손 밑으로 보내기 
	        hand.transform.DOLocalMoveY(-2f, comeBackDuration)
		        .SetEase(comeBackEase);
	        
	        // 타겟 내리기 
	        _targetUp.SmoothRewind();

	        yield return Timing.WaitForSeconds(comeBackDuration);
	        
	        _targetUp.Kill();

	        // 흙더미 페이드아웃
	        dirt.DOFade(0f, dirtFadeOutDuration);
	        
	        // 밖에다 완료처리 알려주기 
	        _async.IsDone = _async.IsSuccess = true;
        }
    }
}