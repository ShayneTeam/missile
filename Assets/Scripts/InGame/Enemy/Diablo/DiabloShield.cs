﻿using InGame.Common;
using Lean.Pool;
using UnityEngine;

namespace InGame.Enemy
{
    public class DiabloShield : MonoBehaviour
    {
        private void OnTriggerEnter2D(Collider2D other)
        {
            MissileRemover.Remove(other, transform);
        }
    }
}