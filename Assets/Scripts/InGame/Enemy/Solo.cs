﻿using System.Collections;
using System.Collections.Generic;
using Global;
using InGame.Controller.Mode;
using InGame.Global;
using UnityEngine;

namespace InGame.Enemy
{
	[RequireComponent(typeof(SummonRepeat))]
    public class Solo : Boss
    {
        private readonly List<Enemy> _spawnedMonster = new List<Enemy>();
        
        private SummonRepeat _summon;

        protected override void Awake()
        {
	        base.Awake();
	        
	        _summon = GetComponent<SummonRepeat>();
        }
        
        public override void Init(string dataIdx)
        {
            base.Init(dataIdx);
            
            // 기본 초기화
            InitBasic();
            
            // 이동
            InitMove();

            // 소환 
            {
	            // 소환 간격 Min
	            var intervalSecMin = DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "boss_skill_value_1", 0.1f);

	            // 소환 간격 Max 
	            var intervalSecMax = DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "boss_skill_value_2", 0.3f);

	            // 이동 속도 배율 
	            var moveSpeedFactor = DataboxController.GetDataFloat(EnemyMode.Behaviour.MainTable, Sheet.boss, DataIdx, "boss_skill_value_3", 2f);

	            var summonList = (EnemyMode.Behaviour as IGetSummonListSolo)?.GetSummonListSolo();
	            _summon.StartSummon(ImmuneTime, intervalSecMin, intervalSecMax, moveSpeedFactor, summonList);
            }
        }

        public override void Die()
        {
	        base.Die();
		
	        // 스폰시킨 몬스터들 다 죽게 하기 
	        _summon.DieAll();
        }

        public override void OnDespawn()
        {
	        base.OnDespawn();
	        
	        _summon.EndSummon();
        }
    }
}