﻿using InGame.Controller;
using UnityEngine;

namespace InGame.Enemy
{
    public class Golem : Monster
    {
        protected override double GetFinalizedRewardMineral()
        {
            var stageRewardMineral = GetWeightedRewardMineral();
            
            var abAllMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_ALL");
            var abGolemMineralModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_MINERAL_GOLEM");
            
            var modifiedAbRewardMineral = stageRewardMineral * (1f + (abAllMineralModifiedPer + abGolemMineralModifiedPer) / 100f);

            // 골렘 어빌리티 2배
            return CheckReward2X() ? modifiedAbRewardMineral * 2d : modifiedAbRewardMineral;
        }
        
        protected override double GetFinalizedRewardGas()
        {
            var stageRewardGas = GetWeightedRewardGas();
            
            var abAllGasModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_GAS_ALL");
            var abGolemGasModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_GAS_GOLEM");
            
            var modifiedAbRewardGas = stageRewardGas * (1f + (abAllGasModifiedPer + abGolemGasModifiedPer) / 100f);

            // 골렘 어빌리티 2배
            return CheckReward2X() ? modifiedAbRewardGas * 2d : modifiedAbRewardGas;
        }
        
        protected override double GetFinalizedRewardStone()
        {
            var stageRewardStone = GetWeightedRewardStone();
            
            var abAllStoneModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_STONE_ALL");
            var abGolemStoneModifiedPer = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_STONE_GOLEM");
            
            var modifiedAbRewardStone = stageRewardStone * (1f + (abAllStoneModifiedPer + abGolemStoneModifiedPer) / 100f);

            // 골렘 어빌리티 2배
            return CheckReward2X() ? modifiedAbRewardStone * 2d : modifiedAbRewardStone;
        }

        private static bool CheckReward2X()
        {
            var abGolemReward2XChance = InGameControlHub.My.AbilityController.GetUnifiedValue("GAIN_GOLEM_REWARD_2X_RATE");
            
            var dice = Random.Range(0.01f, 100f);
            
            return dice <= abGolemReward2XChance;
        }
    }
}