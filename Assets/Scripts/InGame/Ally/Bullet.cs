﻿using System;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Data;
using InGame.Enemy;
using InGame.Global;
using Lean.Pool;
using UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame
{
    [RequireComponent(typeof(SpriteRenderer))]
    public class Bullet : Projectile
    {
        /*
         * 불렛 데미지는 용병 시트를 참조함
         * 이상적인건 불렛 시트가 따로 빠져서, 용병 시트에서 불렛 시트를 가리키는게 맞음
         */
        private string _soldierIdx;

        private Soldier _owner;

        private bool _isFlip;

        public void Init(string soldierIdx, Soldier owner, bool flip)
        {
            _soldierIdx = soldierIdx;

            // x가 아닌, y를 플립 해야 하는 군
            GetComponent<SpriteRenderer>().flipY = flip;
            _isFlip = flip;

            _owner = owner;
        }
        
        protected override void ProcessDamage(Enemy.Enemy enemy)
        {
            if (CheckIfHeadshot(enemy))
                ProcessHeadshot(enemy);
            else
                base.ProcessDamage(enemy);
        }
	
        private bool CheckIfHeadshot(Enemy.Enemy target)
        {
            // 일반 몹만 헤드샷 가능 
            if (!(target is Monster)) return false;

            // 기본 확률
            var headshotChance = DataboxController.GetDataFloat(Table.Soldier, Sheet.Soldier, _soldierIdx, "headshoot_rate", 2.5f);

            // 어빌리티
            var abArmyHeadshotChance = FoundControlHub.AbilityController.GetUnifiedValue("ARMY_N_MONSTER_KILL_RATE");

            // 최종 확률
            var finalHeadshotChance = headshotChance + abArmyHeadshotChance;

            // 헤드샷 주사위 돌리기 
            return GlobalFunction.RollFloat(finalHeadshotChance);
        }
	
        private void ProcessHeadshot(Enemy.Enemy enemy)
        {
            // 대상 즉각 사망 
            enemy.Die();

            // 헤드샷 텍스트 대상 위에 
            ShowHeadshotTextEffect(enemy.transform.position);
        }
			
        private void ShowHeadshotTextEffect(Vector3 pos)
        {
            var headshotStr = Localization.GetText("army_01", "HEADSHOT!");
            NormalTextEffect.Show(headshotStr, Color.red, pos, 0.7f);
        }

        public override void Shot(Vector3 targetPos)
        {
            // 타입 알아오기 
            var bulletType = DataboxController.GetDataString(Table.Soldier, Sheet.Soldier, _soldierIdx, "bullet_type", "strike");
            switch (bulletType)
            {
                case "strike":
                {
                    ShotWithStraight(targetPos, _isFlip);
                }
                    break;
                case "curve":
                {
                    targetPos.y -= 1f;  // 타겟 위치보다 살짝 아래를 조준해서 지속시간 늘림
                    ShotWithCurve(targetPos, MissileCurve.Instance.Curve, _isFlip);
                }
                    break;;
            }
        }

        protected override double GetBaseDamage()
        {
            return FoundControlHub.SoldierController.GetFinalizedDamage(_soldierIdx);
        }

        protected override bool TryCritical(double damage, out double criticalDamage)
        {
            // 기본 초기화
            criticalDamage = damage;

            // 치명타 최종 확률 연산
            var baseCriticalChance = DataboxController.GetDataFloat(Table.Soldier, Sheet.Soldier, _soldierIdx, "base_critical", 2f);
            var abilityArmyAllCriticalChance = FoundControlHub.AbilityController.GetUnifiedValue("ARMY_ALL_CRI_RATE");
            var abilityAllCriticalChance = FoundControlHub.AbilityController.GetUnifiedValue("ALL_CRITICAL_RATE");
            var finalCriticalChance = baseCriticalChance + abilityArmyAllCriticalChance + abilityAllCriticalChance;

            // 치명타 주사위 돌리기 
            var rolledCriticalChance = Random.Range(0.01f, 100f);
            var isCritical = rolledCriticalChance <= finalCriticalChance; // 돌린 주사위 수가 최종 치명타 확률 범위라면, 치명타 
            if (!isCritical)
                return false;

            // 치명타 데미지 반영
            var baseCriticalDamageModifiedPer = DataboxController.GetDataFloat(Table.Soldier, Sheet.Soldier, _soldierIdx, "base_critical_per", 125f);
            var abilityArmyAllCriticalDamageModifiedPer = FoundControlHub.AbilityController.GetUnifiedValue("ARMY_ALL_CRI_DAMAGE");
            var abilityAllCriticalDamageModifiedPer = FoundControlHub.AbilityController.GetUnifiedValue("ALL_CRITICAL_DAMAGE");
            criticalDamage = damage + (damage * ((baseCriticalDamageModifiedPer + abilityArmyAllCriticalDamageModifiedPer + abilityAllCriticalDamageModifiedPer) / 100d));
            
            return true;
        }
    }
}