﻿using Lean.Pool;
using UnityEngine;

namespace InGame
{
    public class SpawnPrincess : MonoBehaviour
    {
        [Header("공주 스폰 위치"), SerializeField] public Transform spawnPosOfPrincess;
        [Header("공주 프리팹"), SerializeField] public Princess princessPrefab;
        
        public Princess Princess { get; set; }

        public void Spawn()
        {
            Princess = LeanPool.Spawn(princessPrefab, spawnPosOfPrincess.position, Quaternion.identity, transform);
        }

        public void Die()
        {
            if (Princess == null) return;
            Princess.Die();
            Princess = null;
        }
        
        public void Attack()
        {
            if (Princess != null) Princess.StartAutoShoot();
        }
        
        public void Stop()
        {
            if (Princess != null) Princess.StopAutoShoot();
        }
        
        public void Despawn()
        {
            // 공주
            if (Princess != null)
            {
                LeanPool.Despawn(Princess.gameObject);
                Princess = null;
            }
        }

        public void ReadyLanding()
        {
            if (Princess != null) Princess.ReadyLanding();
        }
        
        public void Landing(float duration)
        {
            if (Princess == null) return;
            Princess.Landing(duration);
        }
    }
}