﻿using System.Collections.Generic;
using Global;
using InGame.Global;
using Lean.Pool;
using UnityEngine;

namespace InGame
{
    public class SpawnSoldiers : MonoBehaviour
    {
        [Header("용병 스폰 위치"), SerializeField] public Transform soldierArenaParent;


        public List<Soldier> Soldiers { get; } = new List<Soldier>();

        private readonly Dictionary<string, GameObject> _cachedPrefabs = new Dictionary<string, GameObject>();
        private readonly Dictionary<GameObject, Soldier> _cachedSoldiers = new Dictionary<GameObject, Soldier>();


        public Soldier Spawn(int index, string gradeIdx)
        {
            // 용병 인덱스
            var soldierIdxValue = $"soldier_{index.ToLookUpString()}_idx";
            var soldierIdx = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, soldierIdxValue);

            // 용병 위치 
            var soldierPositionValue = $"soldier_{index.ToLookUpString()}_position";
            var soldierPosition = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, soldierPositionValue);
            var arenaTransform = soldierArenaParent.Find(soldierPosition);

            // 용병 스폰
            var resourceId = DataboxController.GetDataString(Table.Soldier, Sheet.Soldier, soldierIdx, "resource_id");
            
            if (_cachedPrefabs.ContainsKey(resourceId))
            {
                // ULogger.Log($@"{resourceId} ID 를 가진 솔저가 [오브젝트 풀 에서] 로드되었습니다.");
                return InternalSpawn(_cachedPrefabs[resourceId], soldierIdx, arenaTransform.position);
            }
            else
            {
                var go = Resources.Load<GameObject>(resourceId);
                if (go == null) return null;

                if (!_cachedPrefabs.ContainsKey(resourceId))
                {
                    _cachedPrefabs.Add(resourceId, go);
                }

                // ULogger.Log($@"{resourceId} ID 를 가진 솔저가 [어드레서블 에서] 로드되었습니다.");
                return InternalSpawn(go, soldierIdx, arenaTransform.position); 
            }
        }
        
        private Soldier InternalSpawn(GameObject prefab, string idx, Vector3 position)
        {
            var spawn = LeanPool.Spawn(prefab, position, Quaternion.identity, transform);
            if (!_cachedSoldiers.TryGetValue(spawn, out var soldier))
            {
                soldier = spawn.GetComponentInChildren<Soldier>();
                _cachedSoldiers[spawn] = soldier;
            }
            soldier.Init(idx);
                
            
            // 바로 랜딩 준비 
            soldier.ReadyLanding();

            Soldiers.Add(soldier);

            return soldier;
        }
        
        public void DieAll()
        {
            /*
             * Die 시, OnDieSoldier 호출되면서 _soldiers.Remove 호출됨
             * Foreach 안에서 Remove 시 익셉션 뜨므로, For를 역순으로 돌린다 
             */
            for (var index = Soldiers.Count - 1; index >= 0; index--)
            {
                var soldier = Soldiers[index];
                soldier.Die();
            }

            Soldiers.Clear();
        }
        
        public void Attack()
        {
            foreach (var soldier in Soldiers) soldier.StartAutoShoot();
        }
        
        public void Stop()
        {
            foreach (var soldier in Soldiers) soldier.StopAutoShoot();
        }
        
        public void DespawnAll()
        {
            foreach (var soldier in Soldiers) LeanPool.Despawn(soldier.transform.parent.gameObject); // 프리팹 안에 프리팹이 들어가는 구조라, 최상단 루트 반납
            Soldiers.Clear();
        }

        public void ReadyLandingAll()
        {
            foreach (var soldier in Soldiers) soldier.ReadyLanding();
        }
        
        public void Landing(float duration)
        {
            foreach (var soldier in Soldiers) soldier.Landing(duration);
        }
    }
}