﻿using Lean.Pool;
using UnityEngine;

namespace InGame
{
    public class SpawnCharacter : MonoBehaviour
    {
        [Header("캐릭터 스폰 위치"), SerializeField] public Transform spawnPosOfCharacter;
        [Header("캐릭터 프리팹"), SerializeField] public Character characterPrefab;

        public Character Character { get; set; }

        public Character Spawn()
        {
            var character = LeanPool.Spawn(characterPrefab, spawnPosOfCharacter.position, Quaternion.identity, transform);
            Character = character;
            return character;
        }
        
        public void Die()
        {
            if (Character == null) return;
            Character.Die();
            Character = null;
        }
        
        public void Attack()
        {
            if (Character != null) Character.StartAutoShoot();
        }
        
        public void Stop()
        {
            if (Character != null) Character.StopAutoShoot();
        }
        
        public void Despawn()
        {
            if (Character != null)
            {
                LeanPool.Despawn(Character.gameObject);
                Character = null;
            }
        }

        public void ReadyLanding()
        {
            if (Character != null) Character.ReadyLanding();
        }
        
        public void Landing(float duration)
        {
            if (Character == null) return;
            Character.Landing(duration);
        }
    }
}