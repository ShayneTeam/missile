﻿using UnityEngine;

namespace InGame
{
    public class SpawnLandingTime : MonoBehaviour
    {
        [Header("랜딩 시간"), SerializeField] public float landingTime = 0.5f;
    }
}