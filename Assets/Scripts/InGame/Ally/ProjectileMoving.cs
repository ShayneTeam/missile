﻿using System.Collections.Generic;
using Lean.Pool;
using MEC;
using UnityEngine;

namespace InGame
{
    public class ProjectileMoving : MonoBehaviour
    {
        [Header("초당 이동 거리")] [SerializeField] private float speedPerSec;

        [Header("초기 기준 회전 값")] [SerializeField] private float originRot;
        private float OriginRot => IsFlip ? -originRot : originRot;
        
        public bool IsFlip { get; set; }

        private CoroutineHandle _moving;

        private Rigidbody2D _rigidbody2D;

        private void Awake()
        {
            _rigidbody2D = GetComponent<Rigidbody2D>();
        }

        public void Curve(Vector3 targetPos, AnimationCurve curve)
        {
            Timing.KillCoroutines(_moving);
            _moving = Timing.RunCoroutine(_Curve(targetPos, curve).CancelWith(gameObject));
        }

        public void Straight(Vector3 targetPos)
        {
            Timing.KillCoroutines(_moving);
            _moving = Timing.RunCoroutine(_Straight(targetPos).CancelWith(gameObject));
        }

        public void ForceToDirection(Vector2 direction)
        {
            _moving = Timing.RunCoroutine(_Force(direction).CancelWith(gameObject), Segment.FixedUpdate);
        }
        
        private IEnumerator<float> _Force(Vector2 direction)
        {
            // fixedDeltaTime 한 번 기다림. 스폰 될 때 먹인 회전 각 설정이 되고 나서 AddForce 함  
            // 기다림 없이 바로 AddForce 하니, 스폰 될 때 먹인 회전 각이 자꾸 초기화 됨 
            // 기다림 없이 Physics2D.SyncTransforms()도 해보고, cacheRigidbody.rotation 직접 수정까지 해봤는데도 안 됨
            // 걍 1 fixedDeltaTime 기다리는게 확실하더라
            yield return Timing.WaitForOneFrame;
            
            // 발사 
            _rigidbody2D.AddForce(direction.normalized * speedPerSec * 1000f * Time.fixedDeltaTime);

            // 지속 시간만큼 진행
            const float duration = 3f;
            var elapsedTime = 0.0f;
            while (elapsedTime < duration)
            {
                elapsedTime += Time.fixedDeltaTime;

                // 이전 위치 기억
                var prevPos = _rigidbody2D.position;
                
                // 프레임 반복  
                yield return Timing.WaitForOneFrame;
                
                // 현재 위치 
                var currentPos = _rigidbody2D.position;
                
                // 회전 (이전 위치에서 현재 위치로 달라진 각도만큼 회전)
                var angle = Mathf.Atan2(currentPos.y - prevPos.y, currentPos.x - prevPos.x) * Mathf.Rad2Deg;
                _rigidbody2D.rotation = angle - OriginRot;
            }

            // 삭제 
            LeanPool.Despawn(gameObject);
        }

        public void Stop()
        {
            // 종료
            Timing.KillCoroutines(_moving);
        }

        private IEnumerator<float> _Curve(Vector3 targetPos, AnimationCurve curve)
        {
            var startPos = _rigidbody2D.position;
            var endPos = targetPos;
            /*
             * 거리가 가까워도 속도가 늦어지지 않기 위한 처리
             * 거리에 따라 duration을 의도적으로 줄인다
             * duration이 줄은만큼 커브 총 길이도 짧아지므로, 속도가 빨라지게 됨
             * 이 처리를 안할 시, 거리가 짧든 멀든, 항상 같은 시간이 걸리게 시뮬레이션 됨
             */
            var distance = Vector2.Distance(startPos, endPos);
            var duration = distance / speedPerSec;
            var elapsedTime = 0.0f;
            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                var progressingRate = elapsedTime / duration;   // 진행도. 0:시작, 1:종료
                var height = curve.Evaluate(progressingRate);
                var nextPos = Vector2.Lerp(startPos, endPos, progressingRate) + new Vector2(0.0f, height);
                
                // 회전 
                var pos = _rigidbody2D.position;
                var angle = Mathf.Atan2(nextPos.y - pos.y, nextPos.x - pos.x) * Mathf.Rad2Deg;
                _rigidbody2D.rotation = angle - OriginRot;
                
                // 이동
                _rigidbody2D.position = nextPos;
                
                // 프레임 반복  
                yield return Timing.WaitForOneFrame;
            }

            // 그냥 도달해서 사라진 경우, 아무 데미지 이펙트 
            EffectSpawner.SpawnRandomDamageEffect(endPos);
            
            // 도착 
            LeanPool.Despawn(gameObject);
        }
        
        private IEnumerator<float> _Straight(Vector3 targetPos)
        {
            var startPos = _rigidbody2D.position;
            var endPos = (Vector2)targetPos;
            var dir = endPos - startPos;
            
            const float duration = 5f;
            var elapsedTime = 0.0f;

            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                
                var speedPerFrame = speedPerSec * Time.deltaTime;
                
                var currentPos = _rigidbody2D.position;
                var nextPos = currentPos + (dir.normalized * speedPerFrame);
                
                // 회전 
                var angle = Mathf.Atan2(nextPos.y - currentPos.y, nextPos.x - currentPos.x) * Mathf.Rad2Deg;
                _rigidbody2D.rotation = angle - OriginRot;
                
                // 이동
                _rigidbody2D.position = nextPos;
                
                // 프레임 반복  
                yield return Timing.WaitForOneFrame;
            }
            

            // 도착 
            LeanPool.Despawn(gameObject);
        }
    }
}