﻿using System;
using DG.Tweening;
using MEC;
using UnityEngine;

namespace InGame
{
    public class LandingAir : Landing
    {
        [SerializeField] private Vector3 readyPosDiff;

        private bool _isInitOriginPos;
        private Vector3 _originPos;
        
        public override void Ready()
        {
            if (!_isInitOriginPos)
            {
                _originPos = transform.position;
                _isInitOriginPos = true;
            }
                
            var landingReadyPos = _originPos + readyPosDiff;
            transform.position = landingReadyPos;
        }

        public override void StartLanding(float duration, Action completeAction)
        {
            Timing.CallDelayed(0f,
                // 랜딩 시작
                () =>
                {
                    transform.DOMove(_originPos, duration)
                        .SetEase(Ease.InQuart).OnComplete(() =>
                        {
                            completeAction?.Invoke();
                        });        
                });
        }
    }
}