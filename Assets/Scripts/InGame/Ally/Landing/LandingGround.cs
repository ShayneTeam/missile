﻿using System;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Pool;
using MEC;
using UnityEngine;

namespace InGame
{
    public class LandingGround : Landing
    {
        [SerializeField] private float readyHeight = 5f;
        [SerializeField] private GameObject landingEffectPrefab;
        [SerializeField] private Transform landingPos;

        private bool _isInitOriginPos;
        private Vector3 _originPos;


        private void OnDisable()
        {
            // 약탈에서 오른쪽에서 스폰된 애들이 반납될 때, 이전 위치를 기억하고 계속 사용하는 이슈 
            // 반납되면 다시 새롭게 스폰된 위치 기준으로 잡히도록 초기화
            ResetPos();
        }

        public override void Ready()
        {
            if (!_isInitOriginPos)
            {
                _originPos = transform.position;
                _isInitOriginPos = true;
            }
                
            var landingReadyPos = _originPos;
            landingReadyPos.y += readyHeight;
            transform.position = landingReadyPos;
        }

        public override void StartLanding(float duration, Action completeAction)
        {
            Timing.RunCoroutine(_Landing(duration, completeAction).CancelWith(gameObject));
        }

        private IEnumerator<float> _Landing(float duration, Action completeAction)
        {
            // 랜딩 시작
            transform.DOMoveY(_originPos.y, duration)
                .SetEase(Ease.InQuart);
            
            // 랜딩 기다림 
            yield return Timing.WaitForSeconds(duration);
            
            // 랜딩 바닥 이펙트
            if (landingEffectPrefab != null)
            {
                if (landingPos == null)
                    landingPos = transform;
                        
                LeanPool.Spawn(landingEffectPrefab, landingPos);
            }
                    
            // 랜딩 다 끝난 후, 다음엔 다른 위치로 세팅됐을 때, 그 위치 기준으로 랜딩 위치 다시 잡도록 함 
            //_isInitOriginPos = false;
                    
            completeAction?.Invoke();
        }

        private void ResetPos()
        {
            if (!_isInitOriginPos) return;
            transform.position = _originPos;
            _isInitOriginPos = false;
        }
    }
}