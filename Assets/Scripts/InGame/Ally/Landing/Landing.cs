﻿using System;
using UnityEngine;

namespace InGame
{
    public abstract class Landing : MonoBehaviour
    {
        public abstract void Ready();
        public abstract void StartLanding(float duration, Action completeAction);
    }
}