﻿using InGame.Controller;
using InGame.Controller.Control;
using Lean.Pool;
using UnityEngine;

namespace InGame
{
    public abstract class Ally : FindControlHubBehaviour
    {
        public Transform body;
        [SerializeField] protected SpriteRenderer shadow;

        public Vector3 ShadowPos => shadow.transform.position;
        protected Vector3 DeadEffectPos => ShadowPos;
        
        public abstract void Die();

        public abstract bool IsGround { get; }

        public abstract void StartAutoShoot();
        public abstract void StopAutoShoot();

    }
}
