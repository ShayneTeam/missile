using System;
using System.Collections.Generic;
using AsCoroutine;
using DG.Tweening;
using Global;
using InGame.Global;
using Lean.Pool;
using MEC;
using UnityEngine;
using Enum = System.Enum;
using Random = UnityEngine.Random;

namespace InGame
{
    // ReSharper disable InconsistentNaming
    public enum SoldierType
    {
        soldier,
        air,
        machine,
        armor,
        ship
    }
    
    public static class SoldierTypeExtensions
    {
        public static string ToEnumString(this SoldierType type)
        {
            return type switch
            {
                SoldierType.soldier => nameof(SoldierType.soldier),
                SoldierType.air => nameof(SoldierType.air),
                SoldierType.machine => nameof(SoldierType.machine),
                SoldierType.armor => nameof(SoldierType.armor),
                SoldierType.ship => nameof(SoldierType.ship),
                _ => throw new ArgumentOutOfRangeException(nameof(type), type, null)
            };
        }
    }

    public class Soldier : Ally, ILanding, IPoolable
    {
        public event SoldierDelegate OnDie;

        [SerializeField] private SpriteRenderer skin;
        [SerializeField] private Sprite[] sprites;

        /*
         * Landing 클래스는 추상클래스라 RequireComponent 불가
         * 그래서 GetComponent 위험함
         * 명시적으로 링크하도록 함 
         */
        [SerializeField] private Landing landing;

        [SerializeField] private GameObject projectilePrefab;
        [SerializeField] private Transform muzzle;

        [SerializeField] public Transform textEffectPivot;

        [SerializeField] protected float deadEffectScale = 0.5f;

        private string _soldierIdx;
        private SoldierType soldierType;

        private Vector3 initPos;
        
        // 공격시
        public DOTweenAnimation _shotTweenAnimation;

        // Idle 일반 (Path)
        public DOTweenAnimation _IdleTweenAnimation;
        
        private void Awake()
        {
            initPos = gameObject.transform.localPosition;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            
            FoundControlHub.Data.Soldiers.OnLevelUp += OnChangedData;
            FoundControlHub.AbilityController.OnChangedAbilities += OnChangedAbilities;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
        
            if (FoundControlHub == null) return;
            FoundControlHub.Data.Soldiers.OnLevelUp -= OnChangedData;
            FoundControlHub.AbilityController.OnChangedAbilities -= OnChangedAbilities;
        }

        private void OnChangedAbilities()
        {
        }

        private void OnChangedData(string type)
        {
        }

        private void PlayShotTween()
        {
            if (IsGround && _shotTweenAnimation != null)
            {
                _shotTweenAnimation.tween.Rewind();
                _shotTweenAnimation.tween.OnComplete(() =>
                {
                    gameObject.transform.localPosition = initPos;
                }).Play();
            }

            if (!IsGround)
            {
                if (_IdleTweenAnimation != null && _IdleTweenAnimation.tween.IsPlaying()) _IdleTweenAnimation.tween.Pause();

                var xValue = 0f;
                var yValue = 0f;
                var duration = 0.3f;
                
                switch (soldierType)
                {
                    case SoldierType.air:
                        xValue = -0.03f;
                        yValue = 0.01f;
                        duration = 0.2f;
                        break;
                    case SoldierType.ship:
                        xValue = -0.1f;
                        yValue = 0.05f;
                        duration = 0.3f;
                        break;
                }
                
                transform.DOPunchPosition(new Vector3(xValue, yValue, 0f), duration, 1, 1).SetEase(Ease.OutBack).OnComplete(
                    () =>
                    {
                        if (_IdleTweenAnimation != null) _IdleTweenAnimation.tween.Play();
                    });
            }
        }

        private void SetDefaultIdleTween()
        {
            // Dotween Animation 이 있는 경우, 진행.
            if (_IdleTweenAnimation != null)
                _IdleTweenAnimation.tween.Play();

            switch (soldierType)
            {
                case SoldierType.soldier:
                    break;
                case SoldierType.air:
                    break;
                case SoldierType.machine:
                    break;
                case SoldierType.armor:
                    break;
                case SoldierType.ship:
                    // skin.material.DoMatValue("_DistortAmount", 0f, 0.2f, 1f).SetLoops(-1, LoopType.Yoyo);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        public void Init(string idx)
        {
            _soldierIdx = idx;
            soldierType = (SoldierType)Enum.Parse(typeof(SoldierType),
                DataboxController.GetDataString(Table.Soldier, Sheet.Soldier, _soldierIdx, "type", "soldier"));
        }

        public override bool IsGround
        {
            get
            {
                switch (soldierType)
                {
                    case SoldierType.soldier:
                    case SoldierType.machine:
                    case SoldierType.armor:
                        return true;
                    case SoldierType.air:
                    case SoldierType.ship:
                        return false;
                    default:
                        throw new ArgumentOutOfRangeException();
                }
            }
        }


        #region Landing

        public void ReadyLanding()
        {
            if (_IdleTweenAnimation != null)
                _IdleTweenAnimation.tween?.Rewind();

            landing.Ready();
        }

        public void Landing(float duration)
        {
            landing.StartLanding(duration, () =>
            {
                initPos = gameObject.transform.localPosition;
                SetDefaultIdleTween();
            });
        }

        #endregion


        #region Shoot

        private CoroutineHandle _autoShoot;
        
        private float _delayShootingAnim;

        private float CalcShootIntervalSec()
        {
            FoundControlHub.SoldierController.GetFinalizedAtkDelay(_soldierIdx, soldierType, out var min, out var max);

            var finalShootCountPerSec = Random.Range(min, max);
            
            return finalShootCountPerSec;
        }

        public override void StartAutoShoot()
        {
            // 발사 딜레이 유지를 위해, StopAutoShoot()이 호출되지 않고, StartAutoShoot()이 재호출 됐을 땐 초기화 하지 않음 
            if (_autoShoot.IsRunning) return;
            
            StopAutoShoot();
            _autoShoot = Timing.RunCoroutine(AutoShoot().CancelWith(gameObject), Segment.EndOfFrame);
        }

        public override void StopAutoShoot()
        {
            Timing.KillCoroutines(_autoShoot);
        }

        private IEnumerator<float> AutoShoot()
        {
            var range = DataboxController.GetDataFloat(Table.Soldier, Sheet.Soldier, _soldierIdx, "atk_range", 10f);

            while (gameObject != null && gameObject.activeInHierarchy)
            {
                // 타겟 검색 
                var target = FoundControlHub.TargetController.GetTarget(range);

                // 대상이 있으면  
                if (target != null)
                {
                    // 그 녀석에게 발사 
                    var targetPos = target.transform.position;

                    // 발사 간격 (매 발사 때마다 랜덤 간격) 
                    var shootIntervalSec = CalcShootIntervalSec();

                    // 여러 발 발사하는지 체크 
                    var bulletCount =
                        DataboxController.GetDataInt(Table.Soldier, Sheet.Soldier, _soldierIdx, "bullet_count", 1);
                    for (var count = 0; count < bulletCount; count++)
                    {
                        var bulletInterval = DataboxController.GetDataFloat(Table.Soldier, Sheet.Soldier, _soldierIdx,
                            "bullet_interval", 0.15f);

                        var animTime = bulletInterval > 0f ? bulletInterval * 0.5f : shootIntervalSec * 0.5f;

                        Shoot(targetPos, animTime);

                        yield return Timing.WaitForSeconds(bulletInterval);
                    }

                    // 발사 쿨타임 
                    yield return Timing.WaitForSeconds(shootIntervalSec);
                }
                // 만약 대상이 없으면
                else
                {
                    // 약간 쉬기  
                    yield return Timing.WaitForSeconds(0.1f);
                }
            }
        }

        private void Shoot(Vector3 targetPos, float animTime)
        {
            // 발사 애니 
            skin.sprite = sprites[1];
            _delayShootingAnim = animTime;

            // Shot tween
            PlayShotTween();
            
            // 총알 
            ShootBullet(targetPos);
        }

        private void Update()
        {
            // 코루틴 안 쓰기 위한, Update 필살기
            
            // 딜레이가 0이면 바로 처리시키기 위해서, <= 을 쓰지 않고 < 을 씀 
            if (_delayShootingAnim < 0) return;
            _delayShootingAnim -= Time.deltaTime;
            if (_delayShootingAnim <= 0) skin.sprite = sprites[0];
        }

        private void ShootBullet(Vector3 targetPos)
        {
            // 발사 
            // (스폰된 미사일이 InGameControlHub 찾을 수 있도록 부모 설정 해주는거 매우 중요)
            var newBullet = LeanPool.Spawn(projectilePrefab, muzzle.position, muzzle.rotation,
                FoundControlHub.transform);

            // 레이어 지정 
            newBullet.layer = Army.Other[FoundControlHub.InDate].ProjectileLayer;

            var bullet = newBullet.GetComponent<Bullet>();

            var isFlipped = transform.lossyScale.x < 0;
            bullet.Init(_soldierIdx, this, isFlipped);

            bullet.Shot(targetPos);
        }

        #endregion

        public override void Die()
        {
            // 데드 이펙트 
            /*
             * 랜딩처럼 클래스 컴포넌트로 빼서 분기하는게 이상적이지만, 사용되는 코드가 너무 짧으므로 여기서 타입 분기 처리
             */
            switch (soldierType)
            {
                case SoldierType.soldier:
                case SoldierType.machine:
                case SoldierType.armor:
                {
                    // 야매처리. x 스케일을 통해 반전 여부 체크 
                    var isPositionRight = transform.lossyScale.x < 0;
                    if (isPositionRight)
                        DeadEffectDirector.Instance.ShowAllyDeadGroundRight(DeadEffectPos, deadEffectScale);
                    else
                        DeadEffectDirector.Instance.ShowAllyDeadGroundLeft(DeadEffectPos, deadEffectScale);
                }
                    break;
                case SoldierType.air:
                case SoldierType.ship:
                    DeadEffectDirector.Instance.ShowExplosion(transform.position, deadEffectScale);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }

            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_die_friend");

            // 다이 이벤트 (반드시 LeanPool.Despawn 전에 호출)
            OnDie?.Invoke(this);

            // 부모 프리팹 반납 
            LeanPool.Despawn(transform.parent.gameObject);
        }

        public static void GetGradeIdx(string soldierType, int soldierGrade, out string outGradeIdx)
        {
            // type과 grade가 일치하는 녀석 뽑아오기 
            outGradeIdx = "";

            var allSoldierGradeIdxes = DataboxController.GetAllIdxes(Table.Soldier, Sheet.Grade);
            foreach (var gradeIdx in allSoldierGradeIdxes)
            {
                var type = DataboxController.GetDataString(Table.Soldier, Sheet.Grade, gradeIdx, "type");
                var grade = DataboxController.GetDataInt(Table.Soldier, Sheet.Grade, gradeIdx, "grade", 1);
                if (type != soldierType || grade != soldierGrade)
                    continue;

                outGradeIdx = gradeIdx;
                return;
            }
        }

        public void OnSpawn()
        {
            
        }

        public void OnDespawn()
        {
            // 중요
            OnDie = null;
        }
    }


    public delegate void SoldierDelegate(Soldier arg);
}