﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Pool;
using MEC;
using UnityEngine;

namespace InGame
{
    public class PrincessEnergyBeam : MonoBehaviour
    {
        [SerializeField] private GameObject effectPrefab;
        
        [SerializeField] private SpriteRenderer body;
        [SerializeField] private BoxCollider2D ball;

        [SerializeField] private DOTweenAnimation bazooka;

        [Header("발사 연출 정보")]
        [SerializeField] private float delay;
        
        [SerializeField] private float endPosXBall;
        
        [SerializeField] private float endPosXBody;
        [SerializeField] private float endSizeXBody;

        [SerializeField] private float movingTime;
        [SerializeField] private Ease movingEase;
        
        [SerializeField] private float holdingTime;
        
        [Header("발사 연출 정보")]
        [SerializeField] private float fadeTime;
        [SerializeField] private Ease fadeEase;
        

        /*
         * Private Fields
         */
        private CoroutineHandle _direction;

        private Vector3 _originPosBall;
        private Vector3 _originPosBody;
        private Vector2 _originSizeBody;

        private Vector3 _originScale;

        /*
         * Methods
         */
        private void Awake()
        {
            _originPosBall = ball.transform.localPosition;
            _originPosBody = body.transform.localPosition;
            _originSizeBody = body.size;

            _originScale = transform.localScale;
        }

        public void Show()
        {
            // 혹시나 싶어 호출
            DOTween.ClearCachedTweens();
            
            // 이펙트 하나 스폰 
            LeanPool.Spawn(effectPrefab, transform.position, Quaternion.identity);
            
            // 바주카 애니 중단 
            bazooka.DORewind();
            bazooka.DOPause();

            // 딜레이 후에 켜기 
            Timing.CallDelayed(delay, () => { gameObject.SetActive(true); }, gameObject);
        }

        private void OnEnable()
        {
            // 켜지면, 연출 시작 
            Timing.KillCoroutines(_direction);
            _direction = Timing.RunCoroutine(_Direction().CancelWith(gameObject));
        }

        private void OnDisable()
        {
            Timing.KillCoroutines(_direction);
            
            // 리셋 
            ball.transform.localPosition = _originPosBall;
            body.transform.localPosition = _originPosBody;
            body.size = _originSizeBody;

            transform.localScale = _originScale;
        }

        private IEnumerator<float> _Direction()
        {   
            // 이동 시작
            var moveXBall = ball.transform.DOLocalMoveX(endPosXBall, movingTime).SetEase(movingEase);
            var moveXBody = body.transform.DOLocalMoveX(endPosXBody, movingTime).SetEase(movingEase);

            var sizeXBody = DOTween.To(() => body.size.x, x => body.size = new Vector2(x, _originSizeBody.y), endSizeXBody, movingTime).SetEase(movingEase);

            yield return Timing.WaitUntilDone(moveXBall.WaitForCompletion(true));
            yield return Timing.WaitUntilDone(moveXBody.WaitForCompletion(true));
            yield return Timing.WaitUntilDone(sizeXBody.WaitForCompletion(true));
            
            // 홀딩
            yield return Timing.WaitForSeconds(holdingTime);
            
            // 사라지기 
            var fade = transform.DOScaleY(0f, fadeTime).SetEase(fadeEase);
            
            yield return Timing.WaitUntilDone(fade.WaitForCompletion(true));
            
            // 자동 비활성 
            gameObject.SetActive(false);
            
            // 바주카 애니 재개 
            bazooka.DORestart();
        }
    }
}