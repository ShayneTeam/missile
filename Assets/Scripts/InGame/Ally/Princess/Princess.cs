﻿using Global;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using UnityEngine;

namespace InGame
{
    public class Princess : Ally, ILanding
    {
        [Header("코스튬")]
        [SerializeField] private SpriteRenderer skin;

        [SerializeField] private SpriteRenderer bazooka;
    
        [Header("랜딩")]
        [SerializeField] private Landing landing;
        
        [Header("데드이펙트")]
        [SerializeField] protected float deadEffectScale = 1f;

        [Header("스킬")] 
        [SerializeField] private PrincessEnergyBeam energyBeam;
        
        
        public override bool IsGround => true;
        
        
        protected override void OnEnable()
        {
            base.OnEnable();
            
            // 코스튬 
            Wear(UserData.My.Princess.WornCostume);
            
            // 바주카 
            bazooka.sprite = BazookaControl.GetSkinSprite(UserData.My.Princess.EquippedBazooka);
            
            // 이벤트 
            FoundControlHub.Data.Princess.OnEquipped += OnEquipped;
            FoundControlHub.Data.Princess.OnWear += OnWear;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            // 이벤트
            if (FoundControlHub == null) return;
            FoundControlHub.Data.Princess.OnEquipped -= OnEquipped;
            FoundControlHub.Data.Princess.OnWear -= OnWear;
        }

        private void OnEquipped(string bazookaIdx)
        {
            bazooka.sprite = BazookaControl.GetSkinSprite(bazookaIdx);
        }

        private void OnWear(string costumeIdx)
        {
            Wear(costumeIdx);
        }

        private void Wear(string costumeIdx)
        {
            var resource = DataboxController.GetDataString(Table.Costume, Sheet.princess, costumeIdx, Column.resource);
            skin.sprite = AtlasController.GetSprite(resource);
        }

        public void ReadyLanding()
        {
            landing.Ready();
        }

        public void Landing(float duration)
        {
            landing.StartLanding(duration, ()=>{});
        }

        public override void Die()
        {
            // 데드 이펙트
            // Despawn 보다 먼저 호출돼야 함
            // Despawn 보다 이후에 호출하면 transform이 변경됨
            // Despawn 하는 순간부터 월드에 존재하지 않는다고 생각해야 함
            ShowDieEffect();

            // 반납 
            LeanPool.Despawn(gameObject);
        }

        private void ShowDieEffect(float duration = 999f)
        {
            var isFlipped = transform.lossyScale.x < 0;
            if (isFlipped)
                DeadEffectDirector.Instance.ShowAllyDeadGroundRight(DeadEffectPos, deadEffectScale, duration);
            else
                DeadEffectDirector.Instance.ShowAllyDeadGroundLeft(DeadEffectPos, deadEffectScale, duration);
        }
        
        public override void StartAutoShoot()
        {
        }

        public override void StopAutoShoot()
        {
        }

        public void ActivateSkill()
        {
            energyBeam.Show();
        }
    }
}
