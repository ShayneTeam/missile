﻿using Lean.Pool;
using UnityEngine;

namespace InGame
{
    [RequireComponent(typeof(ProjectileMoving))]
    public abstract class Projectile : TriggerEnemy, IPoolable
    {
        private ProjectileMoving _projectileMoving;

        private void Awake()
        {
            _projectileMoving = GetComponent<ProjectileMoving>();
        }
        
        
        public virtual void OnSpawn()
        {
            
        }

        public virtual void OnDespawn()
        {
            _projectileMoving.Stop();
        }

        public abstract void Shot(Vector3 targetPos);
        
        // 직접 구현한 함수 
        protected void ShotWithCurve(Vector3 targetPos, AnimationCurve curve, bool isFlip)
        {
            _projectileMoving.IsFlip = isFlip;
            _projectileMoving.Curve(targetPos, curve);
        }

        protected void ShotWithStraight(Vector3 targetPos, bool isFlip)
        {
            _projectileMoving.IsFlip = isFlip;
            _projectileMoving.Straight(targetPos);
        }
        
        // Rigidbody 사용한 함수 
        public void MoveToDirection(Vector2 direction, bool isFlip)
        {
            _projectileMoving.IsFlip = isFlip;
            _projectileMoving.ForceToDirection(direction);
        }
    }
    
    
    public enum DamageType
    {
        Normal,
        Critical,
        Ample,
        BigBang,
    }
}