﻿using System;
using System.Linq;
using DG.Tweening;
using Global;
using InGame.Controller;
using InGame.Controller.Control;
using InGame.Data;
using InGame.Global;
using InGame.Raid;
using InGame.Skill;
using JetBrains.Annotations;
using Lean.Pool;
using Server;
using UnityEngine;
using UnityEngine.Serialization;
using Utils;
using Random = UnityEngine.Random;

namespace InGame
{
    public class Bazooka : FindControlHubBehaviour
    {
        [FormerlySerializedAs("bazooka")] [SerializeField] private SpriteRenderer skin;

        [SerializeField] private Missile missilePrefab;

        [SerializeField] private Transform muzzle;

        [SerializeField] private Transform textEffectPivot;

        [SerializeField] private float maxDistance = 8f;
        [SerializeField] private float maxOffsetAngle = 20;

        private float _shootIntervalSec;
        private float _shootEndTime;
        
        public Vector3 TextEffectPos => textEffectPivot.position;

        private Transform _transform;
        private Quaternion _originRot;


        [NonSerialized] public bool CurveEnabled = true;
        private Tweener _punch;


        private void Awake()
        {
            // 초기 회전값 캐싱
            _originRot = transform.rotation;
        }

        protected override void OnEnable()
        {
            base.OnEnable();
            
            skin.sprite = FoundControlHub.BazookaController.GetMySkinSprite();
            
            // 이벤트 
            FoundControlHub.Data.Bazooka.OnEquipped += OnEquipped;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            // 회전 리셋
            transform.rotation = _originRot;
            
            // 트윈 초기화
            KillTween();
            
            if (FoundControlHub == null) return;
            FoundControlHub.Data.Bazooka.OnEquipped -= OnEquipped;
        }

        private void OnEquipped(string bazookaIdx)
        {
            skin.sprite = BazookaControl.GetSkinSprite(bazookaIdx);
        }
        
        public void ChangeBazookaMaterial(Material changeMaterial)
        {
            skin.material = changeMaterial;
        }

        public void SetShootIntervalSec(float shootIntervalSec)
        {
            // 스팀팩 효과 등으로 인해 외부에서 계산해서 바주카에게 세팅함 
            _shootIntervalSec = shootIntervalSec;
            
            // 다음 발사 때, 설정된 발사 간격의 트윈 애니가 적용하게 함 
            KillTween();
        }

        private void KillTween()
        {
            if (_punch == null) return;
            _punch.Kill(true);
            _punch = null;
        }

        public float Shoot(Vector3 targetPos)
        {
            // 게으른 초기화 
            // public 함수라, 초기화 안됐는데 사용되는 경우가 발생함
            if (_shootIntervalSec == 0f)
            {
                // 외부에서 초기화 되지 않았다면 기본 계산 사용 
                _shootIntervalSec = FoundControlHub.BazookaController.DefaultShootIntervalSec();
            }
            
            // 아직 발사 쿨타임 안 끝났다면, 발사 못함 
            if (_shootEndTime > Time.time) return 0f;
        
            // 발사 애니 
            if (_punch == null)
            {
                _punch = skin.transform.DOPunchRotation(
                    new Vector3(0, 0, 20),
                    _shootIntervalSec,
                    0,
                    0).SetAutoKill(false);
            }
            else
            {
                _punch.Restart();
            }

            //
            LaunchMissile(targetPos);
        
            // 쿨타임 적용 
            _shootEndTime = Time.time + _shootIntervalSec;

            // 쿨타임 반환 
            return _shootIntervalSec;
        }

        public void ShootSecond(Vector3 targetPos)
        {
            LaunchMissile(targetPos);
        }

        private void LaunchMissile(Vector3 targetPos)
        {
            // 랜덤 타겟 위치 (랜덤 곡선)
            const float randomRange = 0.1f;
            var randomX = Random.Range(targetPos.x - randomRange, targetPos.x + randomRange);
            var randomY = Random.Range(targetPos.y - randomRange, targetPos.y + randomRange);
            var targetRandomPos = new Vector2(randomX, randomY);

            if (_transform == null) _transform = transform;

            // 야매 플립 
            var isFlipped = _transform.lossyScale.x < 0;
            var position = _transform.position;

            // 보정 앵글 - 거리가 멀수록 포물선 각을 약간 높게 줌 
            var offsetAngle = CurveEnabled ? CalcOffsetAngle(position, targetRandomPos, isFlipped) : 0;

            // 타겟 향하도록 바주카 회전 (기본 앵글과 보정 앵글을 더함)
            var lookAtAngle = GetLookAtAngle(position, targetRandomPos, isFlipped) + offsetAngle;
            var lookAtRotation = Quaternion.Euler(0,0,lookAtAngle);
            _transform.rotation = lookAtRotation;

            // 미사일 발사 
            {
                // 랜덤 미사일 픽
                var equippedMissileList = FoundControlHub.Data.Missiles.GetAllIdxesEquipped();
	
                var randomIdx = Random.Range(0, equippedMissileList.Count());
                var randomPickedMissileIdx = equippedMissileList.ElementAt(randomIdx);
                
                // 미사일 스폰 (바주카 회전이 바뀌어도, 영향 안 받게 바주카를 부모로 설정하는 게 아닌, 좀 더 상위를 부모로 잡음)
                // (스폰된 미사일이 InGameControlHub 찾을 수 있도록 부모 설정 해주는거 매우 중요)
                var newMissile = LeanPool.Spawn(missilePrefab, muzzle.position, lookAtRotation, FoundControlHub.transform);
                
                // 미사일 스케일 
                var missileScale = UserMissileScale.Other[FoundControlHub.InDate]?.scale ?? 1f;
                
                // 레이어 지정
                newMissile.gameObject.layer = Army.Other[FoundControlHub.InDate].ProjectileLayer;
                newMissile.Init(randomPickedMissileIdx, isFlipped, CurveEnabled, missileScale);
            
                // Angle과 Direction은 서로 개별 처리 
                // rotation에는 플립 때문에 추가 각 보정 처리를 해줘야 함
                // Direction은 플립으로 인한 추가 각 보정 처리하지 않음 
                var basicAngle = GetBasicAngle(position, targetRandomPos);
                var direction = MathHelpers.DegreeToVector2(basicAngle + offsetAngle);
                newMissile.ShotWithDirection(direction);
            }
        }

        private static float GetBasicAngle(Vector2 from, Vector2 to)
        {
            var direction = to - from;
            var basicAngle = Mathf.Atan2(direction.normalized.y, direction.normalized.x) * Mathf.Rad2Deg;
            return basicAngle;
        }

        private float CalcOffsetAngle(Vector2 from, Vector2 to, bool isFlipped)
        {
            // 거리가 멀수록 보정 앵글 값 커짐 
            // 거리가 가까울수록 보정 앵글 값 작아짐
            var distance = Vector2.Distance(to, from);
            var distanceRatio = distance / maxDistance;
            var offsetAngle = Mathf.Lerp(0, maxOffsetAngle, distanceRatio);
            
            // TODO
            // 왼쪽에서 오른쪽을 보는 상태 (스케일 X > 0)일 땐, 회전각을 + 로 주면 위로 회전 되는데, 
            // 왜 오른쪽에서 왼쪽을 보는 상태 (스케일 X < 0)일 땐, 회전각을 + 로 주면 아래로 회전할까
            if (isFlipped) offsetAngle *= -1;
            
            return offsetAngle;
        }

        public static float GetLookAtAngle(Vector2 from, Vector2 to, bool isFlipped)
        {
            // 기본 앵글 - 타겟 좌표대로 기본 앵글 잡음  
            var basicAngle = GetBasicAngle(from, to);

            // TODO
            // 왼쪽에서 오른쪽을 보는 상태 (스케일 X > 0)일 땐, 회전각을 + 로 주면 위로 회전 되는데, 
            // 오른쪽에서 왼쪽을 보는 상태 (스케일 X < 0)일 땐, 회전각을 + 로 주면 아래로 회전함
            // 스케일이 반전 되면, 데칼코마니 처럼 딱 반전 되는 게 아닌, 평행반사?가 일어남
            // 음.. 이건 한 번 시간 내서 깊게 파서 이해 봐야 함  
            return isFlipped ? basicAngle + 180f : basicAngle;
        }
    }
}
