﻿using Lean.Pool;
using UnityEngine;

namespace InGame
{
    public class Character : Ally, ILanding, IPoolable
    {
        [Header("코스튬")]
        [SerializeField] private SpriteRenderer head;
        [SerializeField] private SpriteRenderer skin;
        [SerializeField] private SpriteRenderer cloth;

    
        [Header("랜딩")]
        [SerializeField] private Landing landing;
        
        [Header("데드이펙트")]
        [SerializeField] protected float deadEffectScale = 1f;
        
        [Header("바 위치")]
        [SerializeField] public Transform barPivot;
        
        [Header("말풍선")]
        [SerializeField] private SpeechBubble textBubblePrefab;
        [SerializeField] private Transform textBubblePivot;

        
        public event CharacterDelegate OnDie;
        public event VoidDelegate OnStartAutoShoot;
        public event VoidDelegate OnStopAutoShoot;
        public event FloatDelegate OnChangeShootIntervalSec;
        
        
        public override bool IsGround => true;


        protected override void OnEnable()
        {
            base.OnEnable();
            
            Wear();
            
            FoundControlHub.Data.Costume.OnEquipped += OnEquippedCostume;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            if (FoundControlHub == null) return;
            FoundControlHub.Data.Costume.OnEquipped -= OnEquippedCostume;
        }

        private void OnEquippedCostume(string costumeIdx)
        {
            Wear();
        }

        public void Wear()
        {
            FoundControlHub.CostumeController.GetCostumeSprite(out var headSpr, out var skinSpr, out var bodySpr);
            head.sprite = headSpr;
            skin.sprite = skinSpr;
            cloth.sprite = bodySpr;
        }

        #region 랜딩

        public void ReadyLanding()
        {
            landing.Ready();
        }

        public void Landing(float duration)
        {
            landing.StartLanding(duration, ()=>{});
        }

        #endregion



        public override void StartAutoShoot()
        {
            OnStartAutoShoot?.Invoke();
        }
        
        public override void StopAutoShoot()
        {
            OnStopAutoShoot?.Invoke();
        }

        public void ChangeShootIntervalSec(float shootIntervalSec)
        {
            OnChangeShootIntervalSec?.Invoke(shootIntervalSec);
        }

        public override void Die()
        {
            // 데드 이펙트
            // Despawn 보다 먼저 호출돼야 함
            // Despawn 보다 이후에 호출하면 transform이 변경됨
            // Despawn 하는 순간부터 월드에 존재하지 않는다고 생각해야 함
            ShowDieEffect();

            // 공통 처리
            DieCommon();
        }

        public void ShowDieEffect(float duration = 999f)
        {
            var isFlipped = transform.lossyScale.x < 0;
            if (isFlipped)
                DeadEffectDirector.Instance.ShowAllyDeadGroundRight(DeadEffectPos, deadEffectScale, duration);
            else
                DeadEffectDirector.Instance.ShowAllyDeadGroundLeft(DeadEffectPos, deadEffectScale, duration);
        }

        public void ShowExplosion()
        {
            DeadEffectDirector.Instance.ShowExplosion(transform.position, deadEffectScale);
        }

        public void DieExplosion()
        {
            // 폭파 
            ShowExplosion();

            // 공통 처리
            DieCommon();
        }

        private void DieCommon()
        {
            // 반드시 LeanPool.Despawn 보다 먼저 호출
            OnDie?.Invoke(this);
            
            // 반납 
            LeanPool.Despawn(gameObject);
        }

        public void ShowSpeechBubble(string textKey)
        {
            SpeechBubble.Show(textBubblePrefab, textKey, textBubblePivot);
        }

        public void OnSpawn()
        {
            
        }

        public void OnDespawn()
        {
            // 중요
            OnDie = null;
        }
    }
    
    public delegate void CharacterDelegate(Character arg);
}
