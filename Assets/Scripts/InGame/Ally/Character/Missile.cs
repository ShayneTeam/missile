﻿using Global;
using InGame.Global;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

namespace InGame
{
    [RequireComponent(typeof(Rigidbody2D))]
    public class Missile : Projectile
    {
        [SerializeField] private SpriteRenderer skin;

        private Vector3 _originLocalScale;

        private Rigidbody2D _rigidbody2D;
        private float _originGravityScale;
    
        private string _missileIdx;

        private bool _isFlip;

        public bool IsFlip
        {
            get => _isFlip;
            set => _isFlip = value;
        }

        public void Init(string dataIdx, bool flip, bool curve, float scale)
        {
            _missileIdx = dataIdx;

            // x가 아닌, y를 플립 해야 하는 군
            skin.flipY = flip;
            _isFlip = flip;

            ChangeSkin();
            
            // 커브 
            if (_rigidbody2D == null)
            {
                _rigidbody2D = GetComponent<Rigidbody2D>();
                _originGravityScale = _rigidbody2D.gravityScale;
            }
            _rigidbody2D.gravityScale = curve ? _originGravityScale : 0;
            
            // 스케일 
            if (_originLocalScale == Vector3.zero)
                _originLocalScale = transform.localScale;
            transform.localScale = _originLocalScale * scale;
        }

        private void ChangeSkin()
        {
            var missileResource = DataboxController.GetDataString(Table.Missile, Sheet.missile, _missileIdx, "resource_id");
            skin.sprite = AtlasController.GetSprite(missileResource);
        }

        public override void Shot(Vector3 targetPos)
        {
            // 캐릭터 미사일은 다른 함수 쓰게 테스트 
        }

        public void ShotWithDirection(Vector2 direction)
        {
            MoveToDirection(direction, _isFlip);
        }

        protected override double GetBaseDamage()
        {
            return FoundControlHub.MissileController.GetBaseDamage();
        }

        protected override bool TryCritical(double damage, out double criticalDamage)
        {
            return FoundControlHub.MissileController.TryCritical(damage, out criticalDamage);
        }

        public override void ProcessAfterDamage()
        {
            base.ProcessAfterDamage();
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_missile_boom");
        }
    }
}