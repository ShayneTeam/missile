﻿using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using DG.Tweening;
using Doozy.Engine.UI;
using Global;
using InGame;
using Lean.Pool;
using MEC;
using TMPro;
using UnityEngine;

namespace InGame
{
    public class SpeechBubble : MonoBehaviour
    {
        [SerializeField] private TextMeshPro text;
        
        private DOTweenAnimation _tween;


        public static SpeechBubble Show(SpeechBubble prefab, string textKey, Transform pivot)
        {
            var text = Localization.GetText(textKey);
            if (string.IsNullOrEmpty(text)) return null;
            
            var bubble = LeanPool.Spawn(prefab, pivot.position, Quaternion.identity);
            bubble.Init(text);

            return bubble;
        }

        private void Init(string textStr)
        {
            text.text = textStr;

            Timing.RunCoroutine(SpeechFlow().CancelWith(gameObject));
        }

        private IEnumerator<float> SpeechFlow()
        {
            if (_tween == null)
                _tween = GetComponent<DOTweenAnimation>();
            
            //
            _tween.DORestart();

            //
            var duration = float.Parse(DataboxController.GetConstraintsData("STAGE_SCRIPT_DURATION_TIME"), CultureInfo.InvariantCulture);
            yield return Timing.WaitForSeconds(duration);

            //
            _tween.DORewind();
            
            yield return Timing.WaitForSeconds(1f);
            
            // 
            LeanPool.Despawn(gameObject);
        }
    }
}