﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Controller.Control;
using InGame.Skill;
using Lean.Pool;
using MEC;
using UI;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame
{
    public class AutoBazooka : FindControlHubBehaviour
    {
        [Header("바주카")]
        [SerializeField] private Bazooka bazooka;

        [SerializeField] private bool showDoubleShotText = true;

        public Bazooka Bazooka => bazooka;
        
        private CoroutineHandle _autoShoot;

        protected override void OnDisable()
        {
            base.OnDisable();
            
            StopAutoShoot();
        }

        public void StartAutoShoot()
        {
            Timing.KillCoroutines(_autoShoot);
            _autoShoot = Timing.RunCoroutine(AutoBazookaFlow().CancelWith(gameObject));
        }
        
        public void StopAutoShoot()
        {
            Timing.KillCoroutines(_autoShoot);
        }
        
        private IEnumerator<float> AutoBazookaFlow()
        {
            var autoBazookaRangeStr = DataboxController.GetConstraintsData("BAZOOKA_ATTACK_RANGE", "10");
            var autoBazookaRange = float.Parse(autoBazookaRangeStr, CultureInfo.InvariantCulture);

            while (true)
            {
                var target = FoundControlHub.TargetController.GetTarget(autoBazookaRange);

                // 대상이 있으면  
                if (target != null)
                {
                    // 그 녀석에게 발사 
                    var targetPos = target.transform.position;
                    var shootCoolTime = bazooka.Shoot(targetPos);
                
                    // 한발 더 발사 할 수 있는지 체크 
                    DoubleShotIfCan(targetPos);
                    
                    // 사운드 
                    SoundController.Instance.PlayEffect("sfx_ui_bazooka_atk");
                
                    // 발사 쿨타임 
                    yield return Timing.WaitForSeconds(shootCoolTime);
                }
                // 만약 대상이 없으면
                else
                {
                    // 한 프레임 쉬기   
                    yield return Timing.WaitForOneFrame;
                }
            }
        }

        private void DoubleShotIfCan(Vector3 targetPos)
        {
            // 주사위 돌리기 
            var abilityBazookaDoubleShootRate = FoundControlHub.AbilityController.GetUnifiedValue("BAZOOKA_DOUBLE_SHOOT_RATE");
        	
            var doubleShotRoll = Random.Range(0.01f, 100f);
            var isDoubleShot = doubleShotRoll <= abilityBazookaDoubleShootRate;
            if (!isDoubleShot)  // 더블샷 주사위 실패 시, 바로 종료 
                return;
        
            // 더블샷 텍스트 
            if (showDoubleShotText)
            {
                NormalTextEffect.Show("Double", Color.green, bazooka.TextEffectPos);
            }
        
            // 한 발 더 발사
            bazooka.ShootSecond(targetPos);
        }
    }
}