﻿using System;
using System.Collections.Generic;
using AsCoroutine;
using Global;
using Global.Extensions;
using InGame.Controller;
using InGame.Controller.Control;
using InGame.Data;
using InGame.Enemy;
using InGame.Global;
using InGame.Global.Buff;
using Lean.Pool;
using MEC;
using UI;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace InGame
{
    public abstract class TriggerEnemy : FindControlHubBehaviour
    {
        private static readonly CachingDictionary<Collider2D, Enemy.Enemy> _cachedEnemies = new CachingDictionary<Collider2D, Enemy.Enemy>();
        
        private bool _isTriggered;

        // 데미지는 미사일이 '준다'는 개념이 더 맞음 
        // 고로, 미사일과 적과의 충돌처리는 미사일이 하는게 맞다 봄
        private void OnTriggerEnter2D(Collider2D other)
        {
            // 충돌되는건 미사일 밖에 없다고 가정할까... 아님 태그라도 넣을까...
            // 직관성과 잠재적 버그를 피하고 싶다면, 미사일만 처리한다는 단정문 필요 
            if (!other.CompareTag("Enemy")) return;
            
            // 캐싱 찾기 
            if (!_cachedEnemies.TryGetValue(other, out var enemy))
            {
                enemy = other.gameObject.GetComponentInParent<Enemy.Enemy>(); // 애니메이션 계층 구조 때문에 Enemy와 Rigidbody는 같은 계층에 있지 않음
                _cachedEnemies[other] = enemy;
            }
            
            // Physics Matrix 설정을 다 해줬지만, Enemy가 아닌 타겟과 부딪힐 가능성 있음
            if (enemy == null) return;
            
            if (!enemy.CanTarget()) return;

            // 같은 프레임에 Die 판정이 났다면, 통과시킴 
            if (enemy.IsDied) return;

            // 디아블로 쉴드같은걸로 인해서 강제로 반납된 경우, 처리 통과 
            if (!gameObject.activeSelf) return;

            // 데미지 처리 
            Damage(enemy, other);

            // 제거 예약
            _isTriggered = true;
        }

        private void Damage(Enemy.Enemy enemy, Collider2D other)
        {
            // 적이 무적 시간이 아니여야지만, 데미지 처리
            // 이전에는 무적 시간이면, 타게팅 되지도 않아서 아예 맞는 처리가 안 됐지만
            // 지금은 무적 시간이면 맞긴 해야 함 
            // 대신 데미지 판정은 무시함 
            if (!enemy.IsImmuneTime)
            {
                // 데미지 과정 처리  
                ProcessDamage(enemy);

                // 피격 이펙트 
                EffectSpawner.Instance.Spawn(enemy.FxResourceId, other.ClosestPoint(transform.position));
            }
        }

        private void Update()
        {
            // 코루틴 안 쓰기 위한, Update 필살기
            // MEC를 쓰던 Coroutine을 쓰던, CPU 오버헤드 발생
            // 둘 다 안 쓰는 전략 
            
            // 미사일 삭제
            // FixedUpdate 에서 transform 변경 시, OnTransformChanged 이벤트가 발생하여, 오버헤드가 크게 생김 
            // Update에서 처리시켜야 오버헤드 생기지 않음 
            if (!_isTriggered) return;
            ProcessAfterDamage();
            _isTriggered = false;
        }

        public virtual void ProcessAfterDamage()
        {
            LeanPool.Despawn(gameObject);
        }

        protected virtual void ProcessDamage(Enemy.Enemy enemy)
        {
            // 데미지 계산 
            var damage = GetDamageEnemy(enemy, out var damageType);

            // 대상에게 데미지 주기 
            enemy.Damage(damage);

            // 데미지 텍스트 (설정 꺼져있다면, 가비지 생성되지 않게 함)
            if (UserData.My.UserInfo.GetFlag(UserFlags.IsOnTextEffect, true))
            {
                DamageTextEffect.Spawn(damage.ToUnitString(), damageType, enemy.transform.position);
            }
        }

        public double GetDamageDefault(out DamageType damageType)
        {
            var finalizedDamage = FinalizeDamageDefault(out damageType);

            return ApplyBuffAndCheat(finalizedDamage);
        }

        private double GetDamageEnemy(Enemy.Enemy target, out DamageType damageType)
        {
            var finalizedDamage = FinalizeDamageEnemy(target, out damageType);

            return ApplyBuffAndCheat(finalizedDamage);
        }

        private static double ApplyBuffAndCheat(double finalizedDamage)
        {
            // 버프 데미지 (PVP 시, 상대 데미지에도 적용됨)
            finalizedDamage *= BuffDamage.Instance.Damage;

            // 데미지 치트
            if (ExtensionMethods.IsUnityEditor())
                finalizedDamage *= DataboxController.GetDataFloat(Table.Cheat, Sheet.Cheat, Row.Test, Column.damage_rate, 1f);

            return finalizedDamage;
        }

        private double FinalizeDamageDefault(out DamageType damageType)
        {
            // 최종 바주카 데미지
            var finalBazookaDamage= GetBaseDamage();
            
            // 치명타 & 증폭 데미지 
            return TryCriticalAndAmple(finalBazookaDamage, out damageType);
        }

        protected virtual double FinalizeDamageEnemy(Enemy.Enemy target, out DamageType damageType)
        {
            // 최종 바주카 데미지
            var finalBazookaDamage= GetBaseDamage();

            // 몬스터 타입별 피해 증가 
            var typeDamage = GetTypeDamage(target, finalBazookaDamage);

            // 치명타 & 증폭 데미지 
            return TryCriticalAndAmple(typeDamage, out damageType);
        }

        private double TryCriticalAndAmple(double damage, out DamageType damageType)
        {
            // 치명타 시도
            if (!TryCritical(damage, out var criticalDamage))
            {
                damageType = DamageType.Normal;
                return damage;
            }

            // 증폭 시도 (치명타 떠야만 가능)
            if (!TryAmple(criticalDamage, out var ampleDamage))
            {
                damageType = DamageType.Critical;
                return criticalDamage;
            }
            
            // 빅뱅 시도 (증폭 터져야만 가능)
            if (!TryBigBang(ampleDamage, out var bigBangDamage))
            {
                damageType = DamageType.Ample;
                return ampleDamage;
            }

            damageType = DamageType.BigBang;
            return bigBangDamage;
        }

        protected abstract double GetBaseDamage();

        private double GetTypeDamage(Enemy.Enemy enemy, double damage)
        {
            // 부딪힌 콜라이더의 부모로 찾아야 함 
            if (enemy is Monster || enemy is Golem)
            {
                var abDamageUpMonsterAndGolemPer = FoundControlHub.AbilityController.GetUnifiedValue(Ability.VS_DAMAGE_UP_MONSTER_GOLEM);
                return damage + (damage * (abDamageUpMonsterAndGolemPer / 100d));
            }

            if (enemy is Portal)
            {
                var abDamageUpPortalPer = FoundControlHub.AbilityController.GetUnifiedValue(Ability.VS_DAMAGE_UP_PORTAL);
                return damage + (damage * (abDamageUpPortalPer / 100d));
            }
            
            // 디아블로는 보스이기도 해서, 보스 체크보다 먼저 해줘야 디아블로 것이 적용됨 
            if (enemy is Diablo || enemy is DiabloBall)
            {
                var abDamageUpDiabloPer = FoundControlHub.AbilityController.GetUnifiedValue(Ability.VS_DAMAGE_UP_DIABLO);
                return damage + (damage * (abDamageUpDiabloPer / 100d));
            }
            
            if (enemy is Boss || enemy is Elite)
            {
                var abDamageUpBossAndElitePer = FoundControlHub.AbilityController.GetUnifiedValue(Ability.VS_DAMAGE_UP_ELITE_BOSS);
                return damage + (damage * (abDamageUpBossAndElitePer / 100d));
            }

            return damage;
        }

        protected abstract bool TryCritical(double damage, out double criticalDamage);

        private bool TryAmple(double damage, out double ampleDamage)
        {
            // 기본 초기화 
            ampleDamage = damage;

            // 증폭 주사위 돌리기 
            var abAllDamageAmpleChance = FoundControlHub.AbilityController.GetUnifiedValue(Ability.ALL_DAMAGE_DOUBLE_RATE);
            
            var rolledAmpleChance = Random.Range(0.01f, 100f);
            var isAmple = rolledAmpleChance <= abAllDamageAmpleChance; 
            if (!isAmple)
                return false;

            // 증폭 데미지  
            var abAllDamageAmplePer = FoundControlHub.AbilityController.GetAmpleDamagePer();

            ampleDamage = damage * (1f + (abAllDamageAmplePer) / 100d);

            return true;
        }
        
        private bool TryBigBang(double damage, out double bigBangDamage)
        {
            // 기본 초기화 
            bigBangDamage = damage;

            // 증폭 주사위 돌리기 
            var abBigBangChance = FoundControlHub.AbilityController.GetUnifiedValue(Ability.ALL_BIGBANG_RATE);
            
            var rolledAmpleChance = Random.Range(0.01f, 100f);
            var isBigBang = rolledAmpleChance <= abBigBangChance; 
            if (!isBigBang)
                return false;

            // 증폭 데미지  
            var abBigBangDamagePer = FoundControlHub.AbilityController.GetUnifiedValue(Ability.ALL_BIGBANG_DAMAGE);

            bigBangDamage = damage * (1f + (abBigBangDamagePer) / 100d);

            return true;
        }
    }
}