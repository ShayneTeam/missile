using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using MEC;
using UnityEngine;
using Utils;

public class AllIn1ShaderApply : MonoBehaviour
{
    [SerializeField] private SpriteRenderer skin;

    [SerializeField] private List<EffectStruct> _effectStructs = new List<EffectStruct>(); 

    [Serializable]
    struct EffectStruct
    {
        public string type;
        public float initValue;
        public float finalValue;
        public float duration;
        public float delay;
    }
    
    
    // Start is called before the first frame update
    void Start()
    {
        for (var i = 0; i < _effectStructs.Count; i++)
        {
            var curEffect = _effectStructs[i];
            Timing.RunCoroutine(Effect(curEffect).CancelWith(gameObject), Segment.RealtimeUpdate);
        }
    }
    
    IEnumerator<float> Effect(EffectStruct curEffect)
    {
        // init
        skin.material.SetFloat(curEffect.type, curEffect.initValue);

        yield return Timing.WaitForOneFrame;

        while (true)
        {
            // initValue -> finalValue
            skin.material.DoMatValue(curEffect.type, curEffect.initValue, curEffect.finalValue, curEffect.duration);
            
            yield return Timing.WaitForSeconds(curEffect.duration);
            
            // finalValue -> initValue
            skin.material.DoMatValue(curEffect.type, curEffect.finalValue, curEffect.initValue, curEffect.duration);
            
            yield return Timing.WaitForSeconds(curEffect.duration);

            yield return Timing.WaitForSeconds(curEffect.delay);
        }
    }

}
