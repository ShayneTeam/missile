using Lean.Pool;
using UnityEngine;

namespace InGame.Common
{
    public static class MissileRemover
    {
        public static void Remove(Collider2D other, Transform transform)
        {
            // 피직스 레이어로도 처리하겠지만, 태그로도 한번 더 걸러낸다 
            if (!other.tag.Contains("Missile"))
                return;

            // 크아미 미사일은 제거하지 않음 
            if (other.CompareTag("Big Beautiful Missile"))
                return;
            
            // 이미 반납됐다면, 종료
            if (!other.gameObject.activeSelf)
                return;

            // 바로 반납시켜버리기 
            LeanPool.Despawn(other.gameObject);

            // 그냥 반납시키면 밋밋하니 이펙트 보여주기 
            EffectSpawner.SpawnRandomDamageEffect(other.transform.position);
        }
    }
}