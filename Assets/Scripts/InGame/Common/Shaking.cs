﻿using System;
using DG.Tweening;
using UnityEngine;

namespace InGame.Common
{
    public class Shaking : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private float duration = 0.5f;
        
        [SerializeField] private float strength = 0.15f;
        [SerializeField] private int vibrato = 50;
        
        private Tweener _shaking;
        private Vector3 _originShakeLocalPos;
        
        
        
        private void OnEnable()
        {
            if (target != null) _originShakeLocalPos = target.localPosition;
        }

        private void OnDisable()
        {
            if (target != null) target.localPosition = _originShakeLocalPos; 
        }

        public void Shake()
        {
            const float shakeDuration = 0.5f;
            
            if (_shaking == null)
            {
                _shaking = target.DOShakePosition(shakeDuration, strength, vibrato)
                    .SetAutoKill(false);
            }
            else
            {
                _shaking.Restart();
            }
        }
    }
}