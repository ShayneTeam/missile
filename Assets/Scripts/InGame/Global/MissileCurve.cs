﻿using UnityEngine;

namespace InGame.Global
{
    public class MissileCurve : MonoBehaviourSingletonPersistent<MissileCurve>
    {
        [Header("미사일 공통 커브 곡선")] [SerializeField]
        public AnimationCurve Curve;
    }
}