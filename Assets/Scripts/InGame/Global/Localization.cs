﻿using System.Collections.Generic;
using Global;
using InGame.Data;
using InGame.Global;
using Ludiq;
using UnityEngine;

// UserData와 DataboxController를 사용해서 로컬라이징텍스트 반환 
namespace InGame
{
    public class Localization : MonoBehaviourSingletonPersistent<Localization>
    {
        // 캐싱 _ 로컬라이징은 GetDataSearchByAllTable 를 하면서 매우 호출 빈도가 많음. 그때마다 T 타입 + Linq 도는게 매우 비효율적이라서, 빈도가 높은 로컬라이징을 따로 뺌.
        private static Dictionary<(string, string), string> _localizingCachedDictionary =
            new Dictionary<(string, string), string>();

        [RuntimeInitializeOnLoadMethod]
        private static void EnterPlayMode()
        {
            _localizingCachedDictionary.Clear();
        }

        public static string GetSystemDefaultLanguage()
        {
            // ReSharper disable once SwitchStatementHandlesSomeKnownEnumValuesWithDefault
            switch (Application.systemLanguage)
            {
                case SystemLanguage.Korean: //한국
                    return "kr";
                case SystemLanguage.Spanish: //스페인
                    return "es";
                case SystemLanguage.Portuguese: //포르투갈
                    return "pt";
                case SystemLanguage.Italian: //이탈리아
                    return "it";
                // case SystemLanguage.Thai: // 태국
                // return "th";
                case SystemLanguage.Chinese: //중국
                case SystemLanguage.ChineseSimplified:
                case SystemLanguage.ChineseTraditional:
                    return "cn";
                case SystemLanguage.French: //프랑스
                    return "fr";
                case SystemLanguage.German: //독일
                    return "de";
                // case SystemLanguage.Ukrainian: // 우크라이나
                // case SystemLanguage.Russian: // 러시아
                // case SystemLanguage.Latvian: // 라트비아
                //     return "ru";
                default: // 기본
                    return "en";
            }
        }

        public static string Language
        {
            get
            {
                // 유저 데이터에 들어있는 언어 설정 값 가져옴
                var userLang = UserData.My.UserInfo.Language;
                if (!string.IsNullOrEmpty(userLang))
                    return userLang;

                // Config Databox에 들어있는 언어 설정 값 가져옴 (Config이 Preload 테이블에 등록돼있어야 함)
                var configLang = DataboxController.GetDataString(Table.Config, Sheet.config, Row.config, Column.lang);
                if (!string.IsNullOrEmpty(configLang))
                    return configLang;

                // 언어 설정한게 따로 없으면, 시스템 기본 언어 사용
                return GetSystemDefaultLanguage();
            }

            set
            {
                UserData.My.UserInfo.Language = value;

                // Config Databox에 넣어놓기
                // 유저가 언어 설정 헤놨다면, 유저 데이터를 서버에 가져오지 않아도 로컬데이터만으로 로컬라이제이션 되도록 함  
                DataboxController.SetDataString(Table.Config, Sheet.config, Row.config, Column.lang, value);
                DataboxController.Save(Table.Config);
            }
        }

        public static string GetText(string key, string defaultText = "")
        {
            if (_localizingCachedDictionary.TryGetValue((key, Language), out var cachedLocalizing))
            {
                ULogger.Log($"캐싱된 로컬라이징 : {key} / {Language} || 결과 : {cachedLocalizing}");
                return cachedLocalizing;
            }

            var localizingText = DataboxController.GetLocalizationData(key, Language, defaultText);
            _localizingCachedDictionary.Add((key, Language), localizingText);

            return localizingText;
        }

        public static string GetFormatText(string key, params object[] arg)
        {
            var format = GetText(key);
            return string.Format(format, arg);
        }
    }
}