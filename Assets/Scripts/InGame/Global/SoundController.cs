﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using DarkTonic.MasterAudio;
using Global;
using InGame.Data;
using InGame.Global;

public class SoundController : MonoBehaviourSingletonPersistent<SoundController>
{
    [Playlist] public string bgmPlayList;

    public float volumePercentage = 1;
    public float pitch = 1;
    public float delaySoundTime = 0;
    public string variationName = string.Empty;

    private string _prevBgm;

    public bool IsOnBGM
    {
        get
        {
            // Config Databox에 들어있는 설정 값 가져옴 (Config이 Preload 테이블에 등록돼있어야 함)
            // 설정을 건드리지 않으면, Config에 값이 들어가지 않으므로, 기본 true가 됨 
            var isOnInConfig = DataboxController.GetDataBool(Table.Config, Sheet.config, Row.config, UserFlags.IsOnBGM, true);
            
            // 유저 데이터에 들어있는 언어 설정 값 가져옴
            var isOnInUserData = UserData.My.UserInfo.GetFlag(UserFlags.IsOnBGM, true);

            // 두 설정 모두가 true 여야만 true 반환 
            // 유저가 설정을 건드리지 않았다면 defaultValue로 모두 true로 설정됨 (false라는 건, 설정을 건드렸다는 의미) 
            return isOnInConfig && isOnInUserData;
        }
        set
        {
            // Config
            DataboxController.SetDataBool(Table.Config, Sheet.config, Row.config, UserFlags.IsOnBGM, value);
            DataboxController.Save(Table.Config);
            
            // UserData
            UserData.My.UserInfo.SetFlag(UserFlags.IsOnBGM, value);
        }
    }
    
    public bool IsOnSFX
    {
        get
        {
            // Config Databox에 들어있는 설정 값 가져옴 (Config이 Preload 테이블에 등록돼있어야 함)
            // 설정을 건드리지 않으면, Config에 값이 들어가지 않으므로, 기본 true가 됨 
            var isOnInConfig = DataboxController.GetDataBool(Table.Config, Sheet.config, Row.config, UserFlags.IsOnSFX, true);
            
            // 유저 데이터에 들어있는 언어 설정 값 가져옴
            var isOnInUserData = UserData.My.UserInfo.GetFlag(UserFlags.IsOnSFX, true);

            // 두 설정 모두가 true 여야만 true 반환 
            // 유저가 설정을 건드리지 않았다면 defaultValue로 모두 true로 설정됨 (false라는 건, 설정을 건드렸다는 의미) 
            return isOnInConfig && isOnInUserData;
        }
        set
        {
            // Config
            DataboxController.SetDataBool(Table.Config, Sheet.config, Row.config, UserFlags.IsOnSFX, value);
            DataboxController.Save(Table.Config);
            
            // UserData
            UserData.My.UserInfo.SetFlag(UserFlags.IsOnSFX, value);
        }
    }

    public void Init()
    {
        InitBGM();
        InitSFX();
    }

    private void InitSFX()
    {
        var effectVolume = IsOnSFX ? 1f : 0f;

        SetSfxVolume(effectVolume);
    }

    private void InitBGM()
    {
        var bgmVolume = IsOnBGM ? 1 : 0;

        SetBgmVolume(bgmVolume);
    }
    
    public void PlayBGMAll()
    {
        MasterAudio.StartPlaylist(bgmPlayList);
    }

    public void StopAllSound()
    {
        MasterAudio.StopAllPlaylists();
    }

    public void PlayBGM(string soundName)
    {
        if (MasterAudio.OnlyPlaylistController != null && MasterAudio.OnlyPlaylistController.CurrentSong != null)
            _prevBgm = MasterAudio.OnlyPlaylistController.CurrentSong.songName;
        
        MasterAudio.TriggerPlaylistClip(soundName);
    }

    private void SetBgmVolume(float volume)
    {
        MasterAudio.PlaylistMasterVolume = volume;
    }

    public void RollbackBGM()
    {
        if (string.IsNullOrEmpty(_prevBgm))
            return;
        
        MasterAudio.TriggerPlaylistClip(_prevBgm);
    }

    // 이펙트재생 : 파일명과 같게 넣어주세요.
    public void PlayEffect(string soundName, float delay = 0f)
    {
        if (IsOnSFX)
            MasterAudio.PlaySound(soundName, volumePercentage, pitch, delay, variationName);
    }

    public void PlayCommonBtnSound()
    {
        PlayEffect("(SFX)GameButton");
    }

    // 이펙트 볼륨 설정
    private void SetSfxVolume(float soundLevel) 
    {
        MasterAudio.MasterVolumeLevel = soundLevel;
    }

    public void SoundStop(string soundName)
    {
        MasterAudio.StopAllOfSound(soundName);
    }

    public void MuteBus(string bus)
    {
        MasterAudio.MuteBus(bus);
    }
    
    public void UnmuteBus(string bus)
    {
        MasterAudio.UnmuteBus(bus);
    }

    //MasterAudio.StopAllOfSound(string soundGroupName);
    //이렇게하면 선택한 사운드 그룹 의 모든 변주곡 이 즉시 중지됩니다 .

    //MasterAudio.FadeOutAllOfSound(string soundGroupName, float fadeTime);
    //이렇게하면 지정된 사운드 그룹의 모든 변주곡이 X 초에 걸쳐 페이드 아웃됩니다.

    //MasterAudio.MasterVolumeLevel
    //이 속성은 읽거나 설정할 수 있습니다. 값은 0과 1 사이 입니다. 마스터 믹서 볼륨을 제어합니다 .

    //MasterAudio.GetGroupVolume(string soundType);
    //선택한 사운드 그룹의 볼륨을 나타내는 부동 소수점을 반환합니다.

    //MasterAudio.SetGroupVolume(string soundType, float volume);
    //선택한 사운드 그룹의 볼륨을 설정합니다.

    //MasterAudio.MuteGroup(string soundType);
    //선택한 사운드 그룹을 음소거합니다.

    //MasterAudio.UnmuteGroup(string soundType);
    //선택한 사운드 그룹의 음소거가 해제됩니다.

    //MasterAudio.SoloGroup(string soundType);
    //선택한 사운드 그룹을 솔로 (및 음소거 해제)합니다.

    //MasterAudio.UnsoloGroup(string soundType);
    //선택한 사운드 그룹을 해제합니다.

    //MasterAudio.GrabGroup(string soundType);
    //Retrigger Percentage 설정과 같은 Sound Group 의 다른 속성을 읽거나 조작하려는 경우이를 사용하여 Sound Group을 잡습니다 .

    //MasterAudio.SetBusVolumeByName(float volume, string busName);
    //이것은 버스 의 볼륨을 변경합니다 .

    //MasterAudio.GrabBusByName(string busName);
    //이를 사용하여 Bus 개체를 잡고 속성을 읽거나 변경합니다.
}