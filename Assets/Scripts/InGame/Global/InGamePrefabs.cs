﻿using InGame.Effect;
using UI;
using UI.Popup;
using UI.Popup.Menu;
using UI.Popup.Menu.Stone;
using UnityEngine;

namespace InGame
{
    public class InGamePrefabs : MonoBehaviourSingleton<InGamePrefabs>
    {
        public NormalTextEffect normalText;
        public DamageTextEffect damageText;
    
        public Transform healthBarParent;
        public Transform damageTextParent;
        public Transform fairyEffectParent;

        public BossRewardEffect bossRewardEffectPrefab;
        
        [Header("리소스 이펙트")] public FlyingResourceEffect flyingResourceEffectPrefab;

        [Header("인피니티 스톤 이펙트")] public InfStoneEffect infStoneEffectPrefab;
        [Header("인피니티 스톤 텍스트 이펙트")] public InfStoneTextEffect infStoneTextEffectPrefab;
        
        [Header("요정 자동 획득 이펙트")] public FairyCatchEffect fairyCatchEffectPrefab;
    }
}
