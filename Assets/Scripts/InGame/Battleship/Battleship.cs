﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bolt;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Data;
using InGame.Enemy;
using InGame.Global;
using InGame.WormHole;
using Lean.Pool;
using MEC;
using Server;
using TMPro;
using UI;
using UI.Popup;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;

namespace InGame.Battleship
{
    public class Battleship : MonoBehaviour
    {
        /*
         * 씬 내에서만 싱글턴 
         */

        #region Scene Object

        public static Battleship Instance;

        private void Awake()
        {
            Instance = this;

            // 타임바 일단 꺼두기
            timeBar.gameObject.SetActive(false);
        }

        #endregion

        /*
         * 주요 변수  
         */
        public int Difficulty { get; private set; }
        public string BattleshipIdx { get; private set; }

        /*
         * 인스펙터 지정
         */
        [SerializeField] private TextMeshProUGUI difficultyText;

        [SerializeField] private BarSlicedFill timeBar;
        [SerializeField] private TextMeshProUGUI timeText;

        /*
         * 실패 관련 
         */
        private CoroutineHandle _fail;

        /*
         * 보스 스폰
         */
        private CoroutineHandle _timer;

        /*
         * 이벤트
         */
        public static VoidDelegate OnStart;

        /*
         * 아미
         */
        private ArmyBattleship MyArmy => InGame.Army.My as ArmyBattleship;

        private List<Vector2> _spawnPosMonsters;
        private CoroutineHandle _waving;


        public void StartMode(string battleshipIdx, int difficulty)
        {
            // BGM
            SoundController.Instance.PlayBGM("bgm_dg_battle_01");

            // 세팅
            BattleshipIdx = battleshipIdx;
            Difficulty = difficulty;

            // 초기화 
            Timing.KillCoroutines(_waving);

            // UI 갱신 
            SetHUD();

            // 시작 플로우
            Timing.RunCoroutine(_StartFlow().CancelWith(gameObject));
        }

        private void SetHUD()
        {
            // 배경
            var bg = DataboxController.GetDataString(Table.Battleship, Sheet.battleship, BattleshipIdx, Column.bg_id);
            BackgroundController.Instance.SetBg(AtlasController.GetSprite(bg));

            // 난이도 이름 
            difficultyText.text = Localization.GetFormatText("wormhole_15", Difficulty.ToLookUpString());
        }

        private IEnumerator<float> _StartFlow()
        {
            // 커튼 치기
            var curtainShowAsync = CurtainScreenEffect.Instance.Show();
            while (!curtainShowAsync.IsDone) yield return Timing.WaitForOneFrame;

            // 심플하게 모든 것 싹 다 반납
            LeanPool.DespawnAll();

            // 아군, 적 전부 날리기 (린풀에서의 반납과 클래스 상에서의 캐싱리스트 반납과는 별개임)
            Enemies.Instance.DespawnAll();
            MyArmy.DespawnAll();

            // 한 프레임 쉬기
            yield return Timing.WaitForOneFrame;

            // 아군 부대 스폰 
            MyArmy.SpawnAll();
            MyArmy.ReadyLandingAll();

            // 커튼 딜레이
            yield return Timing.WaitForSeconds(CurtainScreenEffect.Instance.CurtainWaitingDelaySec);

            // 커튼 열기
            var curtainHideAsync = CurtainScreenEffect.Instance.Hide();
            while (!curtainHideAsync.IsDone) yield return Timing.WaitForOneFrame;

            // 낙하 연출        
            MyArmy.LandingAll(MyArmy.LandingTime);
            yield return Timing.WaitForSeconds(MyArmy.LandingTime);

            // 웨이브 시작 
            _waving = Timing.RunCoroutine(_Wave().CancelWith(gameObject));

            // 공격 시작 
            MyArmy.Attack();

            // 이벤트 
            OnStart?.Invoke();
        }

        private IEnumerator<float> _Wave()
        {
            var allNormalMonsterIdxes = DataboxController.GetAllIdxesByGroup(Table.Battleship, Sheet.Monster, Column.Type, "normal");
            var allEliteMonsterIdxes = DataboxController.GetAllIdxesByGroup(Table.Battleship, Sheet.Monster, Column.Type, "elite");

            _spawnPosMonsters ??= new List<Vector2>();
            _spawnPosMonsters.Clear();

            var allBattleIdxes = DataboxController.GetAllIdxes(Table.Battleship, Sheet.battle);
            
            // 타이머 보이기
            timeBar.gameObject.SetActive(true);

            // 모든 웨이브 
            var currentWave = 0;
            foreach (var battleIdx in allBattleIdxes)
            {
                // 엘리트 확률 
                var eliteRate = DataboxController.GetDataFloat(Table.Battleship, Sheet.battle, battleIdx, "elite_rate");

                // 소환 개수 
                var monsterCount = DataboxController.GetDataInt(Table.Battleship, Sheet.battle, battleIdx, "monster_count");

                // 소환 간격
                var summonDelay = DataboxController.GetDataFloat(Table.Battleship, Sheet.battle, battleIdx, "monster_delay");
                
                for (var i = 0; i < monsterCount; i++)
                {
                    var progressPerWave = 1f / allBattleIdxes.Count;
                    var progressPerMonster = progressPerWave / monsterCount;

                    // 타임바
                    var fillRate = progressPerWave * currentWave + progressPerMonster * i;
                    timeBar.SetFill(fillRate);
				
                    // 타임바 텍스트 
                    timeText.text = Localization.GetFormatText("info_421", (currentWave + 1).ToLookUpString());
                    
                    // 엘리트 확률 주사위 돌리기
                    if (GlobalFunction.RollFloat(eliteRate))
                    {
                        // 랜덤 엘리트 픽 
                        var eliteIdx = allEliteMonsterIdxes.ElementAt(Random.Range(0, allEliteMonsterIdxes.Count()));

                        // 엘리트 스폰 
                        Enemies.Instance.SpawnElite(eliteIdx);
                    }
                    // 엘리트 소환 아니라면, 일반몹 소환 
                    else
                    {
                        // 랜덤 몬스터 픽 
                        var monsterIdx = allNormalMonsterIdxes.ElementAt(Random.Range(0, allNormalMonsterIdxes.Count()));

                        // 랜덤 위치 
                        Enemies.Instance.GetSpawnArenaBounds(monsterIdx, Sheet.Monster, out var monsterArenaBounds);
                        Wave.GetRandomSpawnPos(ref monsterArenaBounds, ref _spawnPosMonsters, out var monsterSpawnPos);

                        // 일반 몬스터 스폰 
                        Enemies.Instance.SpawnMonster(monsterIdx, monsterSpawnPos);
                    }

                    // 소환 딜레이 
                    yield return Timing.WaitForSeconds(summonDelay);
                }

                // 몬스터 소환 끝나고, 보스 소환 
                var bossIdx = DataboxController.GetDataString(Table.Battleship, Sheet.battle, battleIdx, "boss_idx");
                if (bossIdx != "0") Enemies.Instance.SpawnBoss(bossIdx);
                
                currentWave++;
            }
            
            // 타임바 끄기 
            timeBar.gameObject.SetActive(false);

            // 모든 소환 종료 후, 디아블로 소환 
            // 디아블로 경고 연출 팝업
            if (!UIPopup.AnyPopupVisible)
            {
                var popup = WarningDiabloPopup.Show();
                yield return Timing.WaitUntilDone(popup.WaitHide().CancelWith(gameObject));
            }

            // 디아블로 소환 
            var diablo = Enemies.Instance.SpawnDiablo();
            diablo.OnDie += OnDieDiablo;
        }

        private void OnDieDiablo(Enemy.Enemy boss)
        {
            // 이벤트 해제 (린풀이라 빼줘야 함)
            boss.OnDie -= OnDieDiablo;

            // 승리 처리
            BattleshipController.Victory(BattleshipIdx, Difficulty);

            // 배틀쉽 팝업의 난이도 커서 갱신
            UserData.My.Battleship.DifficultyCursor = Difficulty;

            // 초기화 
            (EnemyMode.Behaviour as IEnd)?.End();

            // 승리 팝업 (승리 처리 후에 해야 티켓 차감 반영됨)
            BattleshipChallengePopup.Show();

            WebLog.Instance.AllLog("battleship_victory", new Dictionary<string, object> { { "difficulty", Difficulty }, { "battleshipIdx", BattleshipIdx } });
        }

        #region Fail

        public void Fail()
        {
            // 이거 한 번 들어오면 Enemy 충돌은 계속 들어올 수 있으니, 여기서 FailFlow 진행중이면 알아서 막아줘야 한다 
            if (_fail.IsRunning) return;

            _fail = Timing.RunCoroutine(_Fail().CancelWith(gameObject));

            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_battle_defaet");
        }

        private IEnumerator<float> _Fail()
        {
            // Army 전부 사망 처리  
            MyArmy.DieAll();

            // 약간의 딜레이 
            yield return Timing.WaitForSeconds(3f);

            // 타임바 끄기 
            timeBar.gameObject.SetActive(false);

            WebLog.Instance.AllLog("battleship_fail", new Dictionary<string, object> { { "difficulty", Difficulty }, { "battleshipIdx", BattleshipIdx } });

            // 웜홀 나가기 
            BattleshipController.ExitBattleship();
        }

        #endregion
    }
}