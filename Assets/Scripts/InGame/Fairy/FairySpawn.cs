﻿using UnityEngine;

namespace InGame.Fairy
{
    public class FairySpawn : MonoBehaviourSingletonPersistent<FairySpawn>
    {
        [SerializeField] private Transform spawn;
        [SerializeField] private Transform pathStart;
        [SerializeField] private Transform pathEnd;
        
        public static Vector3 Pos => Instance.spawn.position;
        public static Vector3 PathStart => Instance.pathStart.position;
        public static Vector3 PathEnd => Instance.pathEnd.position;
    }
}