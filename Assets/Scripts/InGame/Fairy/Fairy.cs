﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Global;
using InGame.Controller;
using InGame.Global;
using Lean.Pool;
using MEC;
using UI.Popup;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame.Fairy
{
	internal interface ITouchable
	{
		void OnTouched();
	}
	
    public class Fairy : MonoBehaviour, ITouchable, IPoolable
    {
	    [SerializeField] private float enterAndExitDuration;
	    
        [SerializeField] private int shuttleCount;
        [SerializeField] private float shuttleDurationPerOnce;

        [SerializeField] private float babyPathOffsetY;
					
        [SerializeField] private SpriteRenderer skin;
        
        private static readonly Dictionary<GameObject, Fairy> _cachedFairies = new Dictionary<GameObject, Fairy>();


        public string Idx { get; private set; }
        public int Stage { get; private set; }

        public string RewardItemIdx { get; private set; }
        public string RewardBoxIdx { get; private set; }

        public bool HasBoxReward => RewardBoxIdx != null;
        
        private CoroutineHandle _shuttle;


        public static Fairy Spawn(string idx, int stage, GameObject prefab, bool isBox, Vector3 position, Transform parent)
        {
            var go = LeanPool.Spawn(prefab, position, Quaternion.identity, parent);
            if (!_cachedFairies.TryGetValue(go, out var fairy))
            {
	            fairy = go.GetComponentInChildren<Fairy>();
	            _cachedFairies[go] = fairy;
            }
            fairy.Init(idx, stage, isBox);

            return fairy;
        }
        
        private void Init(string idx, int stage, bool isBox)
        {
            Idx = idx;
            Stage = stage;
            
            // 초기화 (셔틀 도중에 획득될 경우, 이전 플립 정보 유지됨)
            skin.flipX = false;

            // 아이템 보상
            var rewardItemGroup = DataboxController.GetDataString(Table.Fairy, Sheet.Fairy, Idx, Column.reward_item_id);
            RewardItemIdx = DataboxController.RandomPick(Table.Fairy, Sheet.RewardItem, Column.item_group, rewardItemGroup, Column.chance);
            
            // 박스 보상 
            RewardBoxIdx = null;	// 초기화
            if (isBox)
            {
	            var rewardBoxGroup = DataboxController.GetDataString(Table.Fairy, Sheet.Fairy, Idx, Column.reward_box_id);
	            RewardBoxIdx = DataboxController.RandomPick(Table.Fairy, Sheet.RewardBox, Column.box_group, rewardBoxGroup, Column.chance);
            }

            // 경로 왕복 
            Timing.KillCoroutines(_shuttle);
            _shuttle = Timing.RunCoroutine(_Shuttle().CancelWith(gameObject));

            // 로그 
            WebLog.Instance.ThirdPartyLog("fairy_show");
        }

        private IEnumerator<float> _Shuttle()
        {
            var spawn = FairySpawn.Pos;
            var pathStart = FairySpawn.PathStart;
            var pathEnd = FairySpawn.PathEnd;
            
            // 아기요정은 살짝 경로 위로 올림 
            var type = DataboxController.GetDataString(Table.Fairy, Sheet.Fairy, Idx, "type");
            var isBaby = type == "fairy_son";
            if (isBaby)
            {
	            pathStart.y += babyPathOffsetY;
	            pathEnd.y += babyPathOffsetY;
            }
							
            var root = transform.parent;
						
            // 경로1로 이동 
			yield return Timing.WaitUntilDone(_Move(root, pathStart, enterAndExitDuration).CancelWith(gameObject));
						
            // 경로1,2를 왕복횟수만큼 반복
            for (var count = 1; count <= shuttleCount; count++)
            {
	            // 경로 1
	            skin.flipX = false;
	            yield return Timing.WaitUntilDone(_Move(root, pathEnd, shuttleDurationPerOnce).CancelWith(gameObject));
	            
	            // 자동획득 있다면  
	            if (FairyController.CanAutoCatch)
	            {
		            // 랜덤 딜레이 주고, 자동 획득 처리 
		            var delay = Random.Range(0f, shuttleDurationPerOnce);
		            Timing.RunCoroutine(_AutoCatch(delay).CancelWith(gameObject));
	            }
	            
	            // 경로 2
	            skin.flipX = true;
	            yield return Timing.WaitUntilDone(_Move(root, pathStart, shuttleDurationPerOnce).CancelWith(gameObject));
            }
						
            // 스폰됐던 곳으로 돌아감 
            skin.flipX = true;
            yield return Timing.WaitUntilDone(_Move(root, spawn, enterAndExitDuration).CancelWith(gameObject));
            skin.flipX = false;
						
            // 자동 반납 
            LeanPool.Despawn(root);
        }

        private static IEnumerator<float> _Move(Transform root, Vector3 pos, float duration)
        {
	        var move = root.DOMove(pos, duration).SetEase(Ease.Linear);
	        
	        yield return Timing.WaitUntilDone(move.WaitForCompletion(true));
        }

        private IEnumerator<float> _AutoCatch(float delay)
        {
	        // 딜레이 
	        yield return Timing.WaitForSeconds(delay);

	        // 캐치
	        CatchByAuto();
        }

        private void OnDisable()
        {
	        /*
	         * 이 오브젝트가 꺼졌다해서 두트윈도 같이 꺼지지 않음
	         * 두트윈은 오브젝트에 부착되지 않고, 별도로 관리되기 때문 
	         */
	        transform.parent.DOKill();
        }

        public void OnSpawn()
        {
	        
        }

        public void OnDespawn()
        {
	        // 혹시 이 요정이 팝업으로 보여지고 있다면, 팝업 닫음
	        FairyAdsPopup.HideIfIsMe(this);
        }

        public void OnTouched()
        {
	        // 사운드 
	        SoundController.Instance.PlayEffect("sfx_ui_fairy_touch");
	        
	        // 팝업 초기화 & 보여주기 
	        FairyAdsPopup.Show(this, Catch, CatchByAds);
        }

        private void Catch()
        {
	        // 일반 획득 
	        FairyController.Collect(RewardItemIdx, RewardBoxIdx, Stage, false);
	        
	        // 공통 처리
	        CatchCommon();
        }

        private void CatchByAds()
        {
	        // 2배 획득 
	        FairyController.Collect(RewardItemIdx, RewardBoxIdx, Stage, true);

	        // 공통 처리
	        CatchCommon();
        }

        private void CatchByAuto()
        {
	        // 획득
	        FairyController.AutoCollect(RewardItemIdx, RewardBoxIdx, Stage);

	        // 공통 처리
	        CatchCommon();
        }

        private void CatchCommon()
        {
	        // 이펙트 
	        ShowCatchEffect();

	        // 자동 반납 
	        LeanPool.Despawn(transform.parent);
        }

        private void ShowCatchEffect()
        {
	        var catchEffect = LeanPool.Spawn(InGamePrefabs.Instance.fairyCatchEffectPrefab, InGamePrefabs.Instance.fairyEffectParent);
	        catchEffect.Init(transform.position);
        }
    }
}