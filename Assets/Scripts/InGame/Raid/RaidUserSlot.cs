﻿using System;
using Bolt;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using InGame.Controller;
using InGame.Controller.Control;
using InGame.Skill;
using Lean.Pool;
using Server;
using TMPro;
using UnityEngine;

namespace InGame.Raid
{
    [RequireComponent(typeof(RaidUserInDate))]
    public class RaidUserSlot : CanFindWithInDate<RaidUserSlot>
    {
        [SerializeField] private GameObject pivot;
        [SerializeField] private Character character;
        [SerializeField] private TextMeshPro nicknameText;
        [SerializeField] private DOTweenAnimation bazookaIdleAnim;
        [SerializeField] private GameObject owner;
        [SerializeField] private Transform skills;
        [SerializeField] private Canvas skillsCanvas;
        
        [Header("자이언트 미사일"), SerializeField] private RaidBigBeautifulMissile bigBeautifulMissilePrefab;
        [SerializeField] private Transform bigBeautifulMissileSpawn;
        
        private RaidUserInDate _raidUserInDate;
        public string InDate => _raidUserInDate.InDate;
        private Vector3 _originPivotPosition;
        private Tweener _eaten;


        public Vector3 CharacterPosition { get; private set; }
        public bool IsAlive => pivot.gameObject.activeSelf && (!_eaten?.IsPlaying() ?? true);

        private void Awake()
        {
            _raidUserInDate = GetComponent<RaidUserInDate>();
            CharacterPosition = character.transform.position;
            _originPivotPosition = pivot.transform.position;
        }

        public void Activate(string inDate, string nickname, int rank, bool isOwner)
        {
            // 인데이트 설정
            _raidUserInDate.SetUserInDate(inDate);
            
            //
            gameObject.SetActive(true);
            pivot.SetActive(true);
            
            // 랭크 & 닉네임
            nicknameText.text = Localization.GetFormatText("raid_041", rank.ToString(), nickname);

            // 캐릭터 세팅 
            // 캐릭터 켜기 전에 UserInDate와 UserData 세팅이 먼저 돼있어야 함
            character.Wear();
            
            // 방장 마크 
            owner.SetActive(isOwner);
            
            // 바주카 애니 
            bazookaIdleAnim.DORestart();
        }

        public void Deactivate()
        {
            gameObject.SetActive(false);
            pivot.SetActive(false);
        }

        public void StartAttack()
        {
            if (!pivot.gameObject.activeSelf) return;
            
            //
            bazookaIdleAnim.DORewind();
            bazookaIdleAnim.DOPause();
            
            //
            character.StartAutoShoot();
            
            // 스킬 초기화 
            ResetSkillBolt();
        }

        public void ShowSkillsHUD(bool show)
        {
            skillsCanvas.enabled = show;
            
            // 슬롯이 자주 껐다 켜지면서 볼트가 고장나는 현상 발생 
            // 동작 복구하게 함  
            if (show) ResetSkillBolt();
        }

        private void ResetSkillBolt()
        {
            // 시작 시간 초기화한 다음 호출해야만 함
            // 그래야 RemainCoolTimeSec이 제대로 계산됨 
            for (var i = 0; i < skills.childCount; i++)
            {
                var skill = skills.GetChild(i);
                CustomEvent.Trigger(skill.gameObject, "Reset");
            }
        }

        public void StopAttack()
        {
            if (!pivot.gameObject.activeSelf) return;
            
            character.StopAutoShoot();
        }

        public void DieWithEffect()
        {
            if (!pivot.gameObject.activeSelf) return;
            
            character.ShowDieEffect(DeadEffectDirector.Instance.duration);
            Die();
        }
        
        public void DieWithExplosion()
        {
            if (!pivot.gameObject.activeSelf) return;
            
            character.ShowExplosion();
            character.ShowDieEffect(DeadEffectDirector.Instance.duration);
            Die();
        }

        private void Die()
        {
            if (!pivot.gameObject.activeSelf) return;
            
            pivot.SetActive(false);
        }

        public void DieWithTongue(Vector3 to, float duration, Ease ease)
        {
            // 빨려 가는 와중에 레이드 이기거나 지면, 바로 자리에 되돌아 와야 함 
            Raid.Instance.OnEndStarted += OnRaidEnded;
            
            // 공격 중지 
            StopAttack();
            
            // 캐릭터 혓바닥으로 빨려들어감 (도착 시 사망 처리)
            _eaten = pivot.transform.DOMove(to, duration).SetEase(ease).OnComplete(Die);
        }

        private void OnRaidEnded()
        {
            Raid.Instance.OnEndStarted -= OnRaidEnded;
            
            // 캐릭터 위치 리셋
            _eaten.Kill();
            // null 처리를 안하니, Kill을 해줘도 IsPlaying이 true로 뜸; 재활용되서 그런가
            _eaten = null;
            
            pivot.transform.position = _originPivotPosition;
        }
        
        public void ShootBigBeautifulMissile()
        {
            if (!pivot.gameObject.activeSelf) return;
            
            // 미사일 스폰 (부모 설정 중요)
            var startPos = bigBeautifulMissileSpawn.position;
            var newMissile = LeanPool.Spawn(bigBeautifulMissilePrefab, startPos, Quaternion.identity, 
                InGameControlHub.Other[_raidUserInDate.InDate].transform);
            
            // 레이어 지정 
            var endPos = startPos;
            endPos.x += 50f;
            newMissile.Shot(endPos);
            
            // 사운드
            SoundController.Instance.PlayEffect("sfx_skill_use_01");
        }
    }
}