﻿using InGame.Data.Scene;
using Server;

namespace InGame.Raid
{
    public class RaidUserInDate : OwnerUserInDate
    {
        public override string InDate => _userInDate;
        
        // 일부러 위의 InDate 프로퍼티랑 사용을 분리하기 위해 함수로 처리 
        private string _userInDate = ServerMyInfo.InDate;

        public void SetUserInDate(string value)
        {
            _userInDate = value;
        }
    }
}