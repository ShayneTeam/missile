﻿using System;
using System.Collections.Generic;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI;
using UnityEngine;
using Utils;

namespace InGame.Raid
{
    [RequireComponent(typeof(ButtonSpriteSwapper))]
    public class RaidPromoteButton : MonoBehaviour
    {
        [SerializeField] private float coolTime;
        [SerializeField] private TextMeshProUGUI label;
        [SerializeField] private ChatViewBackEndForRaid chatViewBackEnd; 
        
        private ButtonSpriteSwapper _spriteSwapper;
        private CoroutineHandle _waitingCoolTime;

        private void Awake()
        {
            _spriteSwapper = GetComponent<ButtonSpriteSwapper>();
        }

        public void Init()
        {
            // 쿨타임 초기화 
            ResetCoolTime();
        }

        [PublicAPI]
        public void Promote()
        {
            // 쿨타임 중이라면 종료
            if (_waitingCoolTime.IsRunning) return;
            
            // 채팅으로 방 번호 날림 
            chatViewBackEnd.PromoteRoom();
            
            // 쿨타임 시작 
            _waitingCoolTime = Timing.RunCoroutine(_CoolTime().CancelWith(gameObject));
        }
        
        private IEnumerator<float> _CoolTime()
        {
            // 버튼 스프라이트 
            _spriteSwapper.Init(false);
            
            // 카운트 다운
            var endTime = Time.time + coolTime;
            while (Time.time < endTime)
            {
                var remainTime = endTime - Time.time;

                label.text = Localization.GetFormatText("info_411", Mathf.CeilToInt(remainTime).ToLookUpString());
                
                yield return Timing.WaitForOneFrame;
            }

            // 이때까지 버튼 입력 없었으면 자동 재참여  
            ResetCoolTime();
        }

        private void ResetCoolTime()
        {
            // 코루틴 종료 
            Timing.KillCoroutines(_waitingCoolTime);
            
            // 버튼 스프라이트 초기화 
            _spriteSwapper.Init(true);
            
            // 라벨 변경 
            label.text = Localization.GetText("info_410");
        }
    }
}