﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Raid
{
    public class UserDamageTextScale : CanFindWithInDate<UserDamageTextScale>
    {
        [SerializeField] public float scale;
    }
}