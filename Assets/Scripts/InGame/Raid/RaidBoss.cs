﻿using System;
using System.Collections.Generic;
using System.Diagnostics;
using System.Linq;
using Databox.OdinSerializer.Utilities;
using DG.Tweening;
using DG.Tweening.Core;
using DG.Tweening.Plugins.Options;
using Global;
using Global.Extensions;
using InGame.Common;
using InGame.Controller;
using InGame.Enemy;
using InGame.Global;
using Lean.Pool;
using MEC;
using UI;
using UnityEngine;
using Utils;
using Random = UnityEngine.Random;

namespace InGame.Raid
{
    public class RaidBoss : MonoBehaviour
    {
        [SerializeField] private HealthBar healthBar;
        [SerializeField] private Shaking shaking;
        [SerializeField] public EnemyShield shield;
        [SerializeField] private Transform targetingAreaParent;
        [SerializeField] private SpriteRenderer damageEffectArea;
        
        /*
         * 사망 연출 
         */
        [Header("사망 연출"), SerializeField] private SpriteRenderer deathEffectArea;
        [SerializeField] private float deathEffectTotalTime = 3f;
        [SerializeField] private int deathEffectCountMin = 5;
        [SerializeField] private int deathEffectCountMax = 5;
        [SerializeField] private float deathEffectInterval = 0.5f;
        
        [SerializeField] private float bigExplosionTime = 2f;
        [SerializeField] private GameObject bigExplosionEffect;
        
        /*
         * 행성 먹기 연출  
         */
        [Header("행성 먹기 연출"), SerializeField] private float movingTimeToDeadLine = 0.5f;
        [SerializeField] private Transform eatingStartPos;
        [SerializeField] private Transform eatingEndPos;
        [SerializeField] private float eatingStartTime = 1f;
        [SerializeField] private float eatingWaitTime = 1f;
        [SerializeField] private float eatingEndTime = 1f;
        [SerializeField] private float eatingInterval = 1f;
        [SerializeField] private float eatingTiming = 0.6f;
        [SerializeField] private Ease eatingEase;
        [SerializeField] private Ease eatingBackEase;
        
        /*
         * 패턴 : 이동 + 거대화 
         */
        [Header("패턴 : 이동 + 거대화"), SerializeField] private float maxScale = 2f;
        [SerializeField] private Ease movingEase;
        private Tween _moving;
        private Tween _scailing;
        
        /*
         * 패턴 : 에너지볼 
         */
        [Header("패턴 : 에너지볼"), SerializeField] private RaidBossBall ballPrefab;
        [SerializeField] private Transform ballSpawn; 
        [SerializeField] private float testBallCoolTime = 2;
        [SerializeField] private int testBallShootCount = 2;
        
        /*
         * 패턴 : 혓바닥
         */
        [Header("패턴 : 혓바닥"), SerializeField] private EnemyTongue tongue;
        [SerializeField] private float testTongueCoolTime = 3;
        
        /*
         * 패턴 : 무적 + 보호막 방출 
         */
        [Header("패턴 : 무적 + 보호막 방출"), SerializeField] private Transform shieldPivot;
        [SerializeField] private GameObject shieldPrefab;
        [SerializeField] private List<int> shieldTriggerHpPercents;
        private readonly Queue<int> _remainShieldHpPercents = new Queue<int>();
        private double _nextShieldTriggerHp;
        private bool _breakShield;

        /*
         * 테스팅
         */
        [Header("테스트"), SerializeField] private float testRaidTime = 3;
        

        /*
         * 일반 변수 
         */
        private double _initHp;
        private double _currentHp;

        private static readonly CachingDictionary<Collider2D, TriggerEnemy> _cachingDic = new CachingDictionary<Collider2D, TriggerEnemy>();
        private Vector3 _originLocalPos;
        private Vector3 _originLocalScale;
        private CoroutineHandle _eat;
        private CoroutineHandle _energyBallPattern;
        private CoroutineHandle _tonguePattern;
        private CoroutineHandle _shield;
        private CoroutineHandle _movingPattern;

        // 
        private bool _canHaveDamage;
        private Bounds _damageEffectBounds;
        private TweenerCore<Vector3, Vector3, VectorOptions> _eatingBack;
        private TweenerCore<Vector3, Vector3, VectorOptions> _eatingStart;
        private TweenerCore<Vector3, Vector3, VectorOptions> _movingToEatDirect;

        public bool IsDied { get; private set; }

        private static string SkillIdx
        {
	        get
	        {
		        var userCount = Raid.Instance.UserCount;
		        return DataboxController.GetIdxByColumn(Table.Raid, Sheet.boss, Column.team_number, userCount.ToLookUpString());
	        }
        }

        /*
         * 이벤트
         */
        public event VoidDelegate OnDie;

        /*
         * 함수
         */
        private void Awake()
        {
            var transformCached = transform;
            _originLocalPos = transformCached.localPosition;
            _originLocalScale = transformCached.localScale;            
        }

        public void Init(double hp)
        {
            _initHp = _currentHp = hp;
            
            RefreshHealthBar();
            
            IsDied = false;
        }

        public void StartPatterns()
        {
            transform.DOKill();
            // 트윈 Recycle에서 버그가 있음 
            // 내부적으로 어떻게 로직이 도는지는 모르겠지만, 처음 트윈 처리는 잘 되는데 
            // 그 다음 처리부터 트윈이 롤백되는 현상이 발생함
            // 두트윈 메뉴얼 보니 DOTween.Clear() 이라는게 있고, DOTween.ClearCachedTweens()가 있길래 봤더니
            // to 관련된 캐싱을 지우는 느낌이더라 
            // 그래서 호출해봤더니 트윈 롤백 되는 현상이 사라짐
            DOTween.ClearCachedTweens();
            
            // 체력바 보여주기
            healthBar.gameObject.SetActive(true);
            
            // 패턴 : 이동 + 거대화 
            _movingPattern = Timing.RunCoroutine(_MoveToEatingPosPattern().CancelWith(gameObject));

            // 패턴 : 쉴드 
            _remainShieldHpPercents.Clear();
            foreach (var percent in shieldTriggerHpPercents)
            {
                // 순차적으로 빼면서 쓰기 위해 큐에 넣어둠 
                _remainShieldHpPercents.Enqueue(percent);
            }
            DequeueNextShieldPercent();

            // 패턴 : 혓바닥
            _tonguePattern = Timing.RunCoroutine(_TonguePattern().CancelWith(gameObject));

            // 패턴 : 에너지볼
            _energyBallPattern = Timing.RunCoroutine(_EnergyBallPattern().CancelWith(gameObject));
            
            // 이때부터, 미사일 충돌이 일어나야만 뎀지 받을 수 있음 
            _canHaveDamage = false;
        }

        private void StopPatterns()
        {
            // 패턴 : 이동 + 거대화
            Timing.KillCoroutines(_movingPattern);
            _moving.Kill();
            _scailing.Kill();
            
            // 패턴 : 혓바닥
            Timing.KillCoroutines(_tonguePattern);
            tongue.ResetToBegin();
            
            // 패턴 : 에너지볼
            Timing.KillCoroutines(_energyBallPattern);
        }
        
        private void OnTriggerEnter2D(Collider2D other)
        {
            // 캐싱 찾기 
            if (!_cachingDic.TryGetValue(other, out var triggerEnemy))
            {
                triggerEnemy = other.GetComponent<TriggerEnemy>();
                _cachingDic[other] = triggerEnemy;
            }

            // 데미지 처리 할 수 없는게 충돌됐다면 캔슬  
            if (triggerEnemy == null) return;

            //var closestPoint = other.ClosestPoint(transform.position);
            // 한 방향에서 아무리 보스의 다른 곳을 때린다한들, 특정 부위들만 집중 사격받는 느낌이 강하게 듬 
            // 걍 맞았을 때 특정 범위의 랜덤 위치에 데미지 이펙트를 뿌리는게 자연스러운 듯
            var effectSpawnPos = new Vector2 { x = Random.Range(_damageEffectBounds.min.x, _damageEffectBounds.max.x), y = Random.Range(_damageEffectBounds.min.y, _damageEffectBounds.max.y) };

            // 데미지 (진짜 체력을 깎진 않음. 단순 연출용. 진짜 체력 깎는 건, Raid에서 함)
            var damage = triggerEnemy.GetDamageDefault(out var damageType);
            
            // 데미지 텍스트 스케일 
            var damageTextScale = UserDamageTextScale.Other[triggerEnemy.FoundControlHub.InDate]?.scale ?? 0f;

            // 데미지 텍스트
            if (damageTextScale > 0)
                DamageTextEffect.Spawn(damage.ToUnitString(), damageType, effectSpawnPos, damageTextScale);

            // 흔들기
            shaking.Shake();

            // 그냥 반납시키면 밋밋하니 이펙트 보여주기
            EffectSpawner.SpawnRandomDamageEffect(effectSpawnPos);

            // 알아서 반납할지 말지 처리  
            triggerEnemy.ProcessAfterDamage();
            
            // 외부웨서 오토데미지를 주더라도, 실제로 미사일이 맞은 순간부터 데미지 처리가 들어오게 처리 
            _canHaveDamage = true;

            // 보호막 중이라면, 부셔주기 시도 
            if (_shield.IsRunning) _breakShield = true;
        }

        private void Update()
        {
            // 비싼 계산이라 프레임당 한 번만 계산 
            _damageEffectBounds = damageEffectArea.bounds;
        }

        private void RefreshHealthBar()
        {
            var hpText = _currentHp.ToUnitString();
            var ratio = _currentHp / _initHp;
            healthBar.SetHp(hpText, (float)ratio);
        }
        
        public void Damage(double damage)
        {
            if (!_canHaveDamage) return;
        
            // 무적기 발동 중이라면 데미지 무시
            if (_shield.IsRunning) return;
            
            // 갱신
            _currentHp -= damage;

            // 무적기 발동 체크 
            if (_currentHp <= _nextShieldTriggerHp)
            {
                _shield = Timing.RunCoroutine(_ShieldPattern());
                DequeueNextShieldPercent();
            }

            // 체력바 갱신 
            RefreshHealthBar();
        
            // 다이 체크 
            if (_currentHp <= 0)
                Die();
        }

        private void Die()
        {
            if (IsDied) return;
        
            IsDied = true;
            
            // 체력바 바로 가리기 (0 이하로 떨어지는 체력 안 보이게 함)
            healthBar.gameObject.SetActive(false);
            
            // 통지 
            OnDie?.Invoke();
        }

        public void ResetToOrigin()
        {
            // 체력바 가리기 
            healthBar.gameObject.SetActive(false);
            
            // 위치 원래대로 돌아감 
            var transformCached = transform;
            transformCached.localPosition = _originLocalPos;
            transformCached.localScale = _originLocalScale;
            
            // 코루틴 종료 
            StopPatterns();
            Timing.KillCoroutines(_eat);
            
            _eatingStart.Kill();
            _eatingBack.Kill();
            _movingToEatDirect.Kill();
            
            // 적용된 트윈 다 종료 
            transform.DOKill();
        }

        public void ShowShield()
        {
            shield.Show(float.PositiveInfinity);
        }

        public void HideShield()
        {
            shield.FadeOut();
        }
        
        public Transform GetTargetingPos(int index)
        {
            // 예외처리
            if (index >= targetingAreaParent.childCount)
                return transform;

            return targetingAreaParent.GetChild(index);
        }

        /*
         * 패턴 : 이동 
         */
        private IEnumerator<float> _MoveToEatingPosPattern()
        {
            var raidTime = ExtensionMethods.IsUnityEditor() && testRaidTime > 0 ? testRaidTime : RaidController.RaidTime;
            _moving = transform.DOMoveX(eatingStartPos.position.x, raidTime).SetEase(movingEase);
            _scailing = transform.DOScale(maxScale, raidTime);
            yield return Timing.WaitUntilDone(_moving.WaitForKill(true));
            
            // 목적지에 도달했다면 레이드 강제 패배 
            Raid.Instance.Fail();
        }
        
        /*
         * 패턴 : 에너지볼
         */
        private IEnumerator<float> _EnergyBallPattern()
        {
            // 쿨타임
            var skillCollTime = DataboxController.GetDataFloat(Table.Raid, Sheet.boss, SkillIdx, Column.boss_skill_2_delay);
            var coolTime = ExtensionMethods.IsUnityEditor() && testBallCoolTime > 0 ? testBallCoolTime : skillCollTime;
            
            // 발사 수 
            var skillShootCount = DataboxController.GetDataInt(Table.Raid, Sheet.boss, SkillIdx, Column.boss_skill_2_target);
            var shootCount = ExtensionMethods.IsUnityEditor() && testBallShootCount > 0 ? testBallShootCount : skillShootCount;
            
            // 에너지볼 발사하지 않는 난이도라면, 스킵
            if (shootCount == 0) yield break;

            while (true)
            {
                // 쿨타임 (시작부터 반영)
                yield return Timing.WaitForSeconds(coolTime);
                
                // 발사할 녀석 찾기 
                var userSlots = Raid.Instance.PickUserSlot(shootCount);
                
                // 유저 없으면 레이드 실패 처리  
                if (userSlots.Count() == 0)
                {
                    Raid.Instance.Fail();
                    // 루틴도 종료
                    yield break;
                }
                
                // 발사
                foreach (var userSlot in userSlots)
                {
                    // 빗나감 주사위
                    var missChance = DataboxController.GetDataFloat(Table.Raid, Sheet.boss, SkillIdx, Column.boss_skill_2_rate);
                    var miss = GlobalFunction.RollFloat(missChance);

                    // 발사 
                    var ball = LeanPool.Spawn(ballPrefab, ballSpawn.position, Quaternion.identity);
                    ball.Init(userSlot, miss);
                }
            }
        }

        /*
         * 패턴 : 혓바닥 
         */
        private IEnumerator<float> _TonguePattern()
        {
	        // 쿨타임
	        var skillCollTime = DataboxController.GetDataFloat(Table.Raid, Sheet.boss, SkillIdx, Column.boss_skill_1_delay);
	        var coolTime = ExtensionMethods.IsUnityEditor() && testTongueCoolTime > 0 ? testTongueCoolTime : skillCollTime;
	        
            while (true)
            {
                // 쿨타임 (시작부터 반영)
                yield return Timing.WaitForSeconds(coolTime);

                // 먹을 녀석 찾기 
                var userSlots = Raid.Instance.PickUserSlot(1);

                // 유저 없으면 레이드 실패 처리  
                if (userSlots.Count() == 0)
                {
                    Raid.Instance.Fail();
                    // 루틴도 종료
                    yield break;
                }

                var target = userSlots.First();

                // 스킬 사용 
                yield return Timing.WaitUntilDone(tongue._Eat(target.CharacterPosition, (from, duration, ease) =>
                {
                    if (!target.IsAlive) return;
                    
                    // 끌려가는 사이에 구체가 내 슬롯 자리에 오면 엄한데서 터질 수 있음 
                    target.DieWithTongue(from, duration, ease);
                }).CancelWith(gameObject));
            }
        }

        /*
         * 패턴 : 무적 + 보호막 방출 
         */
        private void DequeueNextShieldPercent()
        {
            if (_remainShieldHpPercents.Count == 0)
            {
                // 더 이상의 쉴드 페이지 없을 경우, 발동 못 하도록 무한값 배정 
                _nextShieldTriggerHp = float.NegativeInfinity;
            }
            else
            {
                var percent = _remainShieldHpPercents.Dequeue();
                _nextShieldTriggerHp = _initHp * (percent / 100f);
            }
        }

        private IEnumerator<float> _ShieldPattern()
        {
            // 보호막 스폰 
            var shield = LeanPool.Spawn(shieldPrefab, shieldPivot.position, Quaternion.identity);
            var moveLeft = shield.GetComponent<MoveLeft>();
            var moveSpeedFactor = DataboxController.GetDataFloat(Table.Raid, Sheet.boss, SkillIdx, Column.boss_skill_3_move);
            moveLeft.moveSpeed *= moveSpeedFactor;

            // 보호막 자동 반납
            LeanPool.Despawn(shield, 10f);

            // 지속시간
            var duration = DataboxController.GetDataFloat(Table.Raid, Sheet.boss, SkillIdx, Column.boss_skill_3_time);
            yield return Timing.WaitForSeconds(duration);
            
            // 보호막이 끝나려면, 지속시간이 끝난 후, OnTrigger로 데미지가 한 번 들어와야 함. 그래야 자연스럽게 체력 깍이는 것처럼 보임 
            // 요 처리를 안 해주면, 안 맞았는데 체력이 깎이는 것처럼 보임 
            _breakShield = false;
            while (!_breakShield) yield return Timing.WaitForOneFrame;
        }
        
        /*
         * 사망 연출 
         */
        public IEnumerator<float> _DeathDirection()
        {
            // 모든 페이즈 종료 
            StopPatterns();
            
            // 선폭발 이펙트 
            var explosion = Timing.RunCoroutine(_ExplosionDirection().CancelWith(gameObject));
            yield return Timing.WaitForSeconds(deathEffectTotalTime);
            Timing.KillCoroutines(explosion);
            
            // 행성 폭발 이펙트 
            LeanPool.Spawn(bigExplosionEffect, deathEffectArea.transform.position, Quaternion.identity);
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_raid_boss_die");
            // 보스 위치 리셋 
            ResetToOrigin();
            // 딜레이 
            yield return Timing.WaitForSeconds(bigExplosionTime);
        }

        private IEnumerator<float> _ExplosionDirection()
        {
            var bounds = deathEffectArea.bounds;
            while (true)
            {
                // 이펙트
                var effectCount = Random.Range(deathEffectCountMin, deathEffectCountMax);
                for (var i = 0; i < effectCount; i++)
                {
                    var pos = new Vector2 { x = Random.Range(bounds.min.x, bounds.max.x), y = Random.Range(bounds.min.y, bounds.max.y) };
                    EffectSpawner.SpawnRandomDamageEffect(pos);
                }
                
                // 셰이킹 
                shaking.Shake();
                
                // 사운드  
                SoundController.Instance.PlayEffect("sfx_t999_explode_sound");

                // 이펙트 간격 
                yield return Timing.WaitForSeconds(deathEffectInterval);
            }
        }
        
        /*
         * 행성 먹기 연출 
         */
        public IEnumerator<float> _EatPlanet()
        {
            // 모든 페이즈 종료 
            StopPatterns();
            
            // 무적 
            
            // 데드라인까지 감 (혹시 스케일이 최대까지 안 됐다면 여기서 최대로 만듬)
            _movingToEatDirect = transform.DOMoveX(eatingStartPos.position.x, movingTimeToDeadLine);
            transform.DOScale(maxScale, movingTimeToDeadLine);
            yield return Timing.WaitUntilDone(_movingToEatDirect.WaitForKill(true));
            
            // 별도로 먹는 거 무한 반복 
            _eat = Timing.RunCoroutine(_EatPlanetRepeat().CancelWith(gameObject));
            
            // 행성 딱 먹는 시점까지 기다리기 
            yield return Timing.WaitForSeconds(eatingTiming);
        }

        private IEnumerator<float> _EatPlanetRepeat()
        {
            var prevPos = transform.position;
            
            while (true)
            {
                // 행성 먹기 (반복)
                _eatingStart = transform.DOMove(eatingEndPos.position, eatingStartTime).SetEase(eatingEase);
                yield return Timing.WaitUntilDone(_eatingStart.WaitForKill(true));
                
                // 입에 물고 있기 
                Timing.RunCoroutine(_Bite(eatingWaitTime).CancelWith(gameObject));
                yield return Timing.WaitForSeconds(eatingWaitTime);

                // 위치 롤백
                _eatingBack = transform.DOMove(prevPos, eatingEndTime).SetEase(eatingBackEase);
                yield return Timing.WaitUntilDone(_eatingBack.WaitForKill(true));

                // 딜레이 
                yield return Timing.WaitForSeconds(eatingInterval);
            }
        }

        private IEnumerator<float> _Bite(float duration)
        {
            var elapsedTime = 0f;
            while (elapsedTime < duration)
            {
                elapsedTime += Time.deltaTime;
                
                shaking.Shake();

                yield return Timing.WaitForOneFrame;
            }
        }
    }
}