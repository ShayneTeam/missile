﻿using System;
using System.Collections.Generic;
using System.Linq;
using Bolt;
using Databox.OdinSerializer.Utilities;
using DG.Tweening;
using Doozy.Engine.UI;
using Global;
using Global.Extensions;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Data;
using InGame.Data.Scene;
using InGame.Global;
using InGame.Plunder;
using InGame.Skill;
using JetBrains.Annotations;
using Lean.Pool;
using Ludiq;
using MEC;
using Server;
using TMPro;
using UI;
using UI.Popup;
using UI.Popup.Plunder;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame.Raid
{
    public class Raid : MonoBehaviour
    {
        /*
         * 씬 내에서만 싱글턴 
         */
        #region Scene Object

        public static Raid Instance;

        private void Awake()
        {
            Instance = this;
        }

        #endregion
        
        /*
         * 승리, 패배
         */
        private CoroutineHandle _victoryFlow;
        private CoroutineHandle _failFlow;
        
        [Header("실패 팝업 전, 딜레이"), SerializeField] private float delayToShowFail = 2f;
        
        /*
         * 시간 
         */
        [NonSerialized] public long StartTimestamp;
        
        public bool IsStarted { get; private set; }
        
        private CoroutineHandle _exit;
        private CoroutineHandle _direction;
        private CoroutineHandle _startingRaid;
        
        /*
         * 유저
         */
        [Header("유저"), SerializeField] private Transform userSlotsParent;
        private RaidUserSlot[] userSlots;
        
        [SerializeField] private GameObject userCellsParent;
        private RaidUserCell[] userCells;

        // IEnumerable로 만드니 원본 리스트를 참조해서 유저가 갱신돼버리네 
        // 시작 이후엔 유저 변동되지 못하도록 막기 위해, 유저들을 카피해놓는다 
        private List<ServerRaid.UserInfo> _usersOrdered;
        public int UserCount => _usersOrdered.Count();
        
        private string _ownerInDate;
        private bool AmIOwner => _ownerInDate == ServerMyInfo.InDate;
        
        /*
         * 방 정보 
         */
        [Header("방 정보"), SerializeField] private GameObject bottomParent;
        
        [Serializable]
        public class OwnerControl
        {
            public GameObject root;
            public TextMeshProUGUI roomNumText;
            public TextMeshProUGUI roomDifficultyText;
            public RaidPromoteButton promoteButton;
        }
        [SerializeField] private OwnerControl ownerControl;
            
        [SerializeField] private TextMeshProUGUI roomNumText;
        [SerializeField] private TextMeshProUGUI roomDifficultyText;

        [Header("방 결과 테스트 시뮬레이션"), SerializeField] private TextMeshProUGUI roomResultText;
        [SerializeField] private bool testShowBattleResult;
        
        /*
         * 티켓 
         */
        [Header("인벤"), SerializeField] private GameObject rightBottomParent;
        [SerializeField] private TextMeshProUGUI ownedTicketText;
        [SerializeField] private TextMeshProUGUI ownedUraniumText;
        [SerializeField] private TextMeshProUGUI ticketUraniumText;
        
        /*
         * 연출 
         */
        [Header("플레이어용 스킬"), SerializeField] private CanvasGroup skillsParent;
        [SerializeField] private Transform skills;
        
        [Header("보스"), SerializeField] public RaidBoss boss;
        [Header("공격 시작 후, 얼마 있다가 자동 데미지 처리 들어가는가"), SerializeField] private float autoDamageDelay = 1f;

        [Header("연출용 보스"), SerializeField] private Transform directingBoss;
        [SerializeField] private Transform directingBossStart;
        [SerializeField] private Transform directingBossEnd;
        [SerializeField] private float directingBossTime = 1f;
        
        [Header("보스 시작 위치"), SerializeField] private Transform bossStart;
        [Header("진짜 보스 등장하는 시간"), SerializeField] private float bossAppearTime = 1f;

        [Header("T999"), SerializeField] private T999 t999Prefab;
        [SerializeField] private SpriteRenderer t999SpawnZone;
        [Header("T999 스폰되서 하늘에서 떨어지는 시간"), SerializeField] private float t999LandingTime;
        [Header("T999 스폰 간격"), SerializeField] private float t999SpawnInterval;
        [Header("T999 전부 스폰 후 대기시간"), SerializeField] private float t999WaitingTime;
        
        [Header("자이언트 미사일 쏘고나서 대기시간"), SerializeField] private float bigBeautifulMissileTime = 3f;
        [Header("자이언트 미사일 쏘기 전 딜레이"), SerializeField] private float bigBeautifulMissileDelay = 1f;
        
        /*
         * 폭발 연출 관련 
         */
        [Header("폭발 연출"), SerializeField] private Transform planetExplosionPos;
        [SerializeField] private GameObject planetExplosionEffect;
        [SerializeField] private Sprite planetBeforeExplosion;
        [SerializeField] private Sprite planetAfterExplosion;
        [SerializeField] private SpriteRenderer planet;
        
        /*
         * 자동 나기기 
         */
        [Header("자동 나가기"), SerializeField] private float autoExitTime;
        [SerializeField] private TextMeshProUGUI autoExitCountDownText;
        private CoroutineHandle _autoExit;
        
        /*
         * 테스트
         */
        [Header("테스트"), SerializeField] private int testMyClone;
        [SerializeField] private bool forceRewardDouble;
        
        /*
         * 
         */
        private CoroutineHandle _autoDamage;

        public event VoidDelegate OnEndStarted;
        
        // 승리 팝업에서 보여줄 보상 캐싱 
        private string _rewardIdx;
        private string _rewardGirlIdx;
        private bool _victory;
        private int Difficulty { get; set; }
        private bool _canRewardDouble;
        
        private Queue<RaidUserSlot> _queue;
        private List<RaidUserSlot> _list;
        
        private CoroutineHandle _refreshUsersSlotAndCell;

        /*
         * 기본 함수 
         */
        private void Start()
        {
            // 레이드 정보 가져오기 
            // 레이드는 서버 레이드에게 의존 (그 반대는 되지 않도록 주의)
            if (!ServerRaid.TryGetRoomInfo(out _, out _, out _, out _))
            {
                Exit();
                return;
            }
            
            // 초기화 
            Init();
            
            // 갱신 
            RefreshUsersSlotAndCell();
            
            // 이벤트 
            ServerRaid.OnJoinedUser += OnJoinedUser;
            ServerRaid.OnLeftUser += OnLeftUser;
            ServerRaid.OnChangedOwner += OnChangedOwner;
            ServerRaid.OnDisconnected += OnDisconnected;
            ServerRaid.OnStartRoom += OnStartRaid;
            ServerRaid.OnChangedDifficulty += OnChangedDifficulty;
            boss.OnDie += OnDieBoss;
        }

        private void OnDestroy()
        {
            // 이벤트 
            ServerRaid.OnJoinedUser -= OnJoinedUser;
            ServerRaid.OnLeftUser -= OnLeftUser;
            ServerRaid.OnChangedOwner -= OnChangedOwner;
            ServerRaid.OnDisconnected -= OnDisconnected;
            ServerRaid.OnStartRoom -= OnStartRaid;
            ServerRaid.OnChangedDifficulty -= OnChangedDifficulty;
            boss.OnDie -= OnDieBoss;
            
            // 중요. 만약 결과 팝업에서 연결 끊어졌다면, 반드시 팝업 닫아줘야 함 
            // 뺵키가 안 먹고, 닫기 처리도 안 되기 때문 
            if (_resultPopup != null) _resultPopup.Hide();
        }

        private void Init()
        {
            if (!ServerRaid.TryGetRoomInfo(out var roomNum, out var ownerInDate, out var difficulty, out _))
                return;
            
            // BGM
            SoundController.Instance.PlayBGM("bgm_plunder");
            
            // HACK. 스테이지 씬에서 넘어올 때, 자꾸 포탈이 넘어와서 스폰되는 경우가 생김 
            // 찰나의 타이밍 이슈인 것 같은데, 그런 현상이 발생해도 바로 반납될 수 있도록 여기서 디스폰 처리시킴 
            // 다른 모드들도 이 현상이 발생하는데, 시작 시에 DespawnAll 처리가 있어서 그 현상이 안 보였던 것
            LeanPool.DespawnAll();
            
            // 난이도 
            roomDifficultyText.text = Localization.GetFormatText("raid_030", difficulty.ToLookUpString());
            
            // 방장 UI 온오프 (최초 시작과 방장 바뀔 때 해주면 됨)
            var isOwnerMe = UserData.My.InDate == ownerInDate;
            SetOwnerUI(isOwnerMe, roomNum, difficulty);
            
            // 티켓 
            RefreshTicketUI();

            // 로비 UI 켜기
            SwitchLobbyUI(true);
            
            // 보스 리셋  
            boss.ResetToOrigin();
            
            // 스킬 사용 못하게 기준 타임스탬프 0으로 초기화
            StartTimestamp = 0;
            
            // 플래그 초기화 
            IsStarted = false;
            
            // 행성 스프라이트 변경 
            planet.sprite = planetBeforeExplosion;
            
            // 자동 데미지 끄기 
            Timing.KillCoroutines(_autoDamage);

            // 유저 슬롯 
            userSlots ??= userSlotsParent.GetComponentsInChildren<RaidUserSlot>();
            
            // 유저 현황 
            userCells ??= userCellsParent.GetComponentsInChildren<RaidUserCell>();
            
            // 자동 나가기 분기
            BranchAutoExit();
        }

        private void SetOwnerUI(bool isOwnerMe, string roomNum, int difficulty)
        {
            // 방장 UI
            ownerControl.root.SetActive(isOwnerMe);
            if (isOwnerMe)
            {
                ownerControl.roomNumText.text = Localization.GetFormatText("raid_034", roomNum);
                ownerControl.roomDifficultyText.text = difficulty.ToLookUpString();
                ownerControl.promoteButton.Init();
            }

            // 참석자 UI 
            roomNumText.gameObject.SetActive(!isOwnerMe);
            if (!isOwnerMe)
            {
                roomNumText.text = Localization.GetFormatText("raid_034", roomNum);
            }
        }

        private void RefreshUsersSlotAndCell()
        {
            // 갱신은 한 프레임에 두 번 돌리지 않는다
            Timing.KillCoroutines(_refreshUsersSlotAndCell);
            _refreshUsersSlotAndCell = Timing.RunCoroutine(_RefreshUsersSlotAndCell().CancelWith(gameObject));
        }

        private IEnumerator<float> _RefreshUsersSlotAndCell()
        {
            // 방 정보 가져오기 
            if (!ServerRaid.TryGetRoomInfo(out _, out var ownerInDate, out var difficulty, out var users))
                yield break;

            // TEST. 지정인원 만큼 다 내껄로 채워넣기 
            if (ExtensionMethods.IsUnityEditor() && testMyClone > 0)
            {
                var myInfo = users.SingleOrDefault(info => info.inDate == ServerMyInfo.InDate);
                var test = users.ToList();
                var max = Math.Min(20 - users.Count(), testMyClone);
                for (var i = 0; i < max; i++)
                {
                    test.Add(myInfo);
                }

                _usersOrdered = test.OrderBy(user => user.rank).ToList();
            }
            // 정상 루트 
            else
            {
                // 유저 슬롯 랭크 순으로 정렬 (랭크가 클수록 되로)
                _usersOrdered = users.OrderBy(user => user.rank).ToList();
            }

            var userCount = _usersOrdered.Count();

            // 유저 현황
            var dataIdx = GetRankBonusDataIdx(userCount);
            var markingCutLine = DataboxController.GetDataInt(Table.Raid, Sheet.rank_bonus, dataIdx, Column.mark);
					
            // 유저셀 선초기화 
            foreach (var userCell in userCells) userCell.SetActive(false);
						
            // 랭크 정렬된 순서대로 돌려야 함 
            for (var i = 0; i < userCount; i++)
            {
                var userInfo = _usersOrdered.ElementAt(i);
                var myRankBonus = GetMyRankBonus(dataIdx, i);
                var canMark = i < markingCutLine;
                var isOwner = userInfo.inDate == ownerInDate;
                var maxDamage = InGameControlHub.Other[userInfo.inDate].MissileController.GetMaxDamage();

                // 유저셀 세팅 
                var userCell = userCells[i];
                userCell.SetData(userInfo.rank, userInfo.nickName, myRankBonus, canMark, isOwner, maxDamage);
                userCell.SetActive(true);
            }
            
            // 밸런싱 테스트 시뮬레이션 : 결과 출력  
            RefreshBattleResultText(difficulty);

            // 모든 슬롯 초기화 
            foreach (var userSlot in userSlots) userSlot.Deactivate();
            
            // 매우 중요한 딜레이 
            // 유저슬롯의 이전 인데이트 세팅을 안정적으로 해지시킴 (안 하면 예외 뜸) 
            yield return Timing.WaitForOneFrame;
            
            // 들어와있는 유저 슬롯 보여주기 
            for (var i = 0; i < userCount; i++)
            {
                var userInfo = _usersOrdered.ElementAt(i);
                var isOwner = userInfo.inDate == ownerInDate;
                userSlots[i].Activate(userInfo.inDate, userInfo.nickName, userInfo.rank, isOwner);
            }
            
            // 매우 중요한 딜레이
            // 유저슬롯의 변경된 인데이트 세팅을 안정적으로 등록시킴 (안 하면 예외 뜸)
            yield return Timing.WaitForOneFrame;
        }

        private void RefreshBattleResultText(int difficulty)
        {
            if (ExtensionMethods.IsUnityEditor() && testShowBattleResult)
            {
                RaidController.GetBattleResultValues(difficulty, _usersOrdered, out var sumMaxDamage, out var monsterHp, out var maxShootIntervalSec, out var damageBonusToNerf, out var result,
                    out var victory, out var hpDownRatioPerSec);

                roomResultText.gameObject.SetActive(true);
                roomResultText.text = $"{nameof(monsterHp)} : {monsterHp.ToUnitString()}\n" +
                                      $"{nameof(sumMaxDamage)} : {sumMaxDamage.ToUnitString()}\n" +
                                      $"{nameof(maxShootIntervalSec)} : {maxShootIntervalSec.ToString("0.00")}\n" +
                                      $"{nameof(damageBonusToNerf)} : {damageBonusToNerf.ToString("0.00")}\n" +
                                      $"{nameof(result)} : {result.ToString("0.000")}\n" +
                                      $"{nameof(victory)} : {victory.ToString()}\n" +
                                      $"{nameof(hpDownRatioPerSec)} : {hpDownRatioPerSec.ToString()}";
            }
            else
            {
                roomResultText.gameObject.SetActive(false);
            }
        }

        private void SwitchLobbyUI(bool active)
        {
            userCellsParent.gameObject.SetActive(active);
            rightBottomParent.SetActive(active);
            bottomParent.SetActive(active);            
            
            if (userSlots == null) return;
            // 로비UI 켜진다면  
            if (active)
            {
                // 모든 유저 슬롯의 스킬 캔버스 비활성화
                foreach (var userSlot in userSlots) userSlot.ShowSkillsHUD(false);
            }
            // 로비UI 꺼진다면
            else
            {
                // 내 유저 슬롯의 캔버스만 활성화 
                userSlots?.FirstOrDefault(slot => slot.InDate == ServerMyInfo.InDate)?.ShowSkillsHUD(true);    
            }
            
        }

        private static string GetRankBonusDataIdx(int userCount)
        {
            return DataboxController.GetIdxByColumn(Table.Raid, Sheet.rank_bonus, Column.team_number, userCount.ToLookUpString());
        }

        private static float GetMyRankBonus(string dataIdx, int i)
        {
            var myRankBonusColumn = $"team_rank_{(i + 1).ToLookUpString()}";
            return DataboxController.GetDataFloat(Table.Raid, Sheet.rank_bonus, dataIdx, myRankBonusColumn);
        }

        private void OnJoinedUser(string inDate, string nickname)
        {
            if (_startingRaid.IsRunning || _direction.IsRunning || IsStarted) return;
            
            RefreshUsersSlotAndCell();
            
            // 자동 나가기 분기
            BranchAutoExit();
        }
        
        private void OnLeftUser(string inDate, string nickname)
        {
            if (_startingRaid.IsRunning || _direction.IsRunning || IsStarted) return;
            
            RefreshUsersSlotAndCell();
            
            // 자동 나가기 분기
            BranchAutoExit();
        }
        
        private void OnChangedOwner(string inDate, string nickname)
        {
            if (_startingRaid.IsRunning || _direction.IsRunning || IsStarted) return;
            
            // 방 정보 가져오기 
            if (!ServerRaid.TryGetRoomInfo(out var roomNum, out var ownerInDate, out var difficulty, out _))
                return;
            
            // 내가 방장이 된 경우, 내 난이도 커서를 방 난이도 커서와 맞춤. 그래야 난이도 변경 시 튀는 현상 없음 
            var isOwnerMe = ownerInDate == ServerMyInfo.InDate;
            if (isOwnerMe)
                RaidController.ChangeDifficultyCursor(difficulty);
            
            // 방장 UI 온오프 (최초 시작과 방장 바뀔 때 해주면 됨)
            SetOwnerUI(isOwnerMe, roomNum, difficulty);
            
            // 갱신
            RefreshUsersSlotAndCell();
        }

        private void OnDisconnected()
        {
            // 연결 해지됐다는 메시지 
            MessagePopup.Show("raid_056");
            
            // 중요. 만약 결과 팝업에서 연결 끊어졌다면, 반드시 팝업 닫아줘야 함 
            // 뺵키가 안 먹고, 닫기 처리도 안 되기 때문 
            if (_resultPopup != null) _resultPopup.Hide();

            // 씬 탈출 
            // 레이드 소켓은 연결이 끊어지면, 소켓서버와 재연결은 보장하지만, 접속해있던 방과의 재접속 처리는 보장하지 않음 
            // 그래서 연결 끊기면 심플하게 나가게 함 
            RaidController.ExitRaidScene();
        }
        
        /*
         * 난이도 (방장만 설정)
         */
        public void OnClickDifficultyUp()
        {
            // 다음 난이도 세팅
            RaidController.AddDifficultyCursor();
            
            // 클릭 공통
            OnClickDifficultyBtnCommon();
        }

        public void OnClickDifficultyDown()
        {
            // 이전 난이도로 세팅
            RaidController.SubDifficultyCursor();

            // 클릭 공통
            OnClickDifficultyBtnCommon();
        }

        private void OnClickDifficultyBtnCommon()
        {
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_bt_basic");

            // 방장 클라 UI 미리 갱신
            var difficulty = UserData.My.Raid.DifficultyCursor;
            ownerControl.roomDifficultyText.text = difficulty.ToLookUpString();
            SetRoomDifficultyText(difficulty);
            
            // 배틀 결과 시뮬레이션 갱신 
            RefreshBattleResultText(difficulty);
            
            // 변경 소켓 메시지 보냄 
            ServerRaid.ChangeDifficulty(difficulty);
        }

        private void OnChangedDifficulty(int difficulty)
        {
            SetRoomDifficultyText(difficulty);
            
            // 배틀 결과 시뮬레이션 갱신 
            RefreshBattleResultText(difficulty);
        }

        private void SetRoomDifficultyText(int difficulty)
        {
            roomDifficultyText.text = Localization.GetFormatText("raid_030", difficulty.ToLookUpString());
        }

        /*
         * 티켓 구매 
         */
        public void BuyTicket()
        {
            // 티켓 구매
            var result = RaidController.RaidTicket.BuyTicket();
            switch (result)
            {
                case RaidController.FailedBuyReason.NotFailed:
                    // 갱신 
                    RefreshTicketUI();
                    break;
                default:
                    RaidController.RaidTicket.ShowFailedBuyTicketMessage(result);
                    break;
            }
        }

        private void RefreshTicketUI()
        {
            // 보유 티켓 
            ownedTicketText.text = Localization.GetFormatText("raid_024", RaidController.RaidTicket.Count.ToLookUpString());
            
            // 보유 우라늄
            ownedUraniumText.text = RewardType.GetString(RewardType.REWARD_GEM, UserData.My.Resources.uranium);
            
            // 티켓 우라늄 비용 
            var requiredGem = RaidController.RaidTicket.RequiredGemToBuy;
            var uraniumText = requiredGem == 0 ? Localization.GetText("raid_026") : requiredGem.ToLookUpString();
            ticketUraniumText.text = uraniumText;
        }

        /*
         * 나가기  
         */
        public void Exit()
        {
            // 자동 나가기 종료
            StopAutoExit();
            
            // 나가기 
            if (_exit.IsRunning) return;
            _exit = Timing.RunCoroutine(_Exit().CancelWith(gameObject));
        }

        private IEnumerator<float> _Exit()
        {
            // 방 나가기
            LoadingIndicator.Show();
            yield return Timing.WaitUntilDone(ServerRaid._LeaveRoom(null));
            LoadingIndicator.Hide();
            
            // 중요. 만약 결과 팝업에서 연결 끊어졌다면, 반드시 팝업 닫아줘야 함 
            // 뺵키가 안 먹고, 닫기 처리도 안 되기 때문 
            if (_resultPopup != null) _resultPopup.Hide();
            
            // 씬 탈출
            RaidController.ExitRaidScene();
        }
        
        /*
         * 시작 
         */
        public void StartRaid()
        {
            // 티켓 처리 
            if (!RaidController.RaidTicket.HasTicket)
            {
                // 보유하고 있지 않다면, 메시지 출력
                MessagePopup.Show("raid_017");
                return;
            }
            
            // 자동 나가기 강제 종료 
            StopAutoExit();
                
            // 서버에 시작 요청            
            if (_startingRaid.IsRunning) return;
            _startingRaid = Timing.RunCoroutine(_StartRaid().CancelWith(gameObject));
        }
        
        private IEnumerator<float> _StartRaid()
        {
            // 방 시작 
            // 정상 처리 되면, OnStartRaid로 결과 들어옴
            
            // 혹시 갱신 중이라면, 갱신 기다림 (유저 슬롯이 제대로 보여지기까지를 기다림)
            while (_refreshUsersSlotAndCell.IsRunning) yield return Timing.WaitForOneFrame;

            // 뺑글이 온
            LoadingIndicator.Show();
            
            // 방 정보 가져오기 
            if (!ServerRaid.TryGetRoomInfo(out _, out _, out _, out var users))
                yield break;
            
            // 치트 
            if (ExtensionMethods.IsUnityEditor() && testMyClone > 0)
            {
                // Init에서 미리 연산된 테스팅 룸 정보로 대체 
                users = _usersOrdered;
            }
            
            // 결과 연산 
            RaidController.CalcBattleResult(UserData.My.Raid.DifficultyCursor, users, out var victory, out var hpDownRatioPerSec); 
                
            // 요청 
            yield return Timing.WaitUntilDone(ServerRaid._StartRoom(victory, hpDownRatioPerSec, null));
            
            // 뺑글이 오프
            LoadingIndicator.Hide();
        }
        
        private void OnStartRaid(bool victory, int difficulty, float hpDownRatioPerSec)
        {
            if (_direction.IsRunning) return;
            
            // BGM
            SoundController.Instance.PlayBGM("bgm_raid_battle");

            // 결과 팝업 때 쓰기 위한 캐싱용
            // (ServerRaid에서는 룸 정보가 바뀌면 갱신시키지만, 레이드에서는 룸 정보가 바뀌어도 갱신하지 않을 것임)
            ServerRaid.TryGetRoomInfo(out _, out _ownerInDate, out _, out _);
            
            _victory = victory;
            Difficulty = difficulty;
            
            // 로비 UI 끄기
            SwitchLobbyUI(false);
            
            // 난이도 갱신 
            roomDifficultyText.text = Localization.GetFormatText("raid_030", difficulty.ToLookUpString());
            
            // 보스 체력 설정 
            var bossHp = RaidController.GetBossHp(difficulty);
            boss.Init(bossHp);
            
            // 초당 깎여야 하는 보스 체력량  
            var autoDamagePerSec = bossHp * (hpDownRatioPerSec / 100f);
            
            // 보상 선지급 
            if (victory)
            {
                // 랜덤 보상 뽑기 
                if (!RaidController.TryPickRandomReward(out var rewardIdx, out var rewardGirlIdx)) return;
                
                _rewardIdx = rewardIdx;
                _rewardGirlIdx = rewardGirlIdx;
                
                // 보상 두배 확률 주사위
                // 랭크 정렬된 순서대로 돌려야 함 
                _canRewardDouble = false;
                var usersCount = _usersOrdered.Count();
                var dataIdx = GetRankBonusDataIdx(usersCount);
                for (var i = 0; i < usersCount; i++)
                {
                    var userInfo = _usersOrdered.ElementAt(i);
                    
                    // 내꺼 찾기 
                    if (userInfo.inDate != ServerMyInfo.InDate) continue;
                    
                    var myRankBonus = GetMyRankBonus(dataIdx, i);
                    _canRewardDouble = GlobalFunction.RollFloat(myRankBonus);
                    break;
                }
                
                // 치트
                if (ExtensionMethods.IsUnityEditor() && forceRewardDouble) _canRewardDouble = true;

                // 저장 
                RaidController.SaveVictory(rewardIdx, rewardGirlIdx, difficulty, _canRewardDouble);
            }
            
            // 시작 플로우
            _direction = Timing.RunCoroutine(_Direction(autoDamagePerSec).CancelWith(gameObject));

            // 로그 
            WebLog.Instance.AllLog("raid_start", null);
        }
        
        private IEnumerator<float> _Direction(double autoDamagePerSec)
        {
            // 연출용 보스 위치 리셋
            directingBoss.position = directingBossStart.position;
            yield return Timing.WaitForOneFrame;
            
            // 워닝 팝업  
            WarningDiabloPopup.Show();

            // 연출용 보스 왼쪽에서 오른쪽으로 이동 
            directingBoss.DOMove(directingBossEnd.position, directingBossTime);
            yield return Timing.WaitForSeconds(directingBossTime);
            
            // T999
            var spawnBounds = t999SpawnZone.bounds;

            foreach (var userInfo in _usersOrdered)
            {
                // T999 스킬 잠긴 유저는 스킵 
                var raidSkillControl = InGameControlHub.Other[userInfo.inDate].SkillController as RaidSkillControl;
                var canSpawn = raidSkillControl != null && raidSkillControl.CanSpawnT999;
                if (!canSpawn) continue;

                // 스폰
                var spawnPos = Vector2.zero;
                spawnPos.x = Random.Range(spawnBounds.min.x, spawnBounds.max.x); 
                spawnPos.y = Random.Range(spawnBounds.min.y, spawnBounds.max.y);
                
                var t999 = LeanPool.Spawn(t999Prefab, spawnPos, Quaternion.identity, InGameControlHub.Other[userInfo.inDate].transform);
                t999.ReadyLanding();
                t999.Landing(t999LandingTime);
                
                // 랜딩하자마자 공격 예약
                Timing.CallDelayed(t999LandingTime, () => { t999.Shoot(); });
                
                // 스폰 간격 
                yield return Timing.WaitForSeconds(t999SpawnInterval);
            }
            // T999 스폰 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_cha_gen");
            
            // 약간의 딜레이 
            yield return Timing.WaitForSeconds(t999WaitingTime);
            
            // 진짜 보스 슬긍슬금 시작 위치로 이동 (보호막 켜고)
            var bossMoving = boss.transform.DOMove(bossStart.position, bossAppearTime);
            boss.ShowShield();
            yield return Timing.WaitUntilDone(bossMoving.WaitForKill(true));
            
            // 자이언트 미사일 딜레이 
            yield return Timing.WaitForSeconds(bigBeautifulMissileDelay);
            
            // 자이언트 미사일 발사
            foreach (var userSlot in userSlots) userSlot.ShootBigBeautifulMissile();
            yield return Timing.WaitForSeconds(bigBeautifulMissileTime);
            
            // 보스 페이즈 시작 
            boss.StartPatterns();
            
            // 보스 보호막 사라지게 함 
            boss.HideShield();

            // 공격 시작 전 타임스탬프 기록 (첫 스킬 강제 쿨타임 적용 위함)
            StartTimestamp = ServerTime.Instance.NowSec;
            
            // 시작 플래그 
            IsStarted = true;
            
            // 공격 시작 
            foreach (var userSlot in userSlots) userSlot.StartAttack();
            
            // 오토데미지 시작 
            Timing.KillCoroutines(_autoDamage);
            _autoDamage = Timing.RunCoroutine(_AutoDamage(autoDamagePerSec).CancelWith(gameObject));
        }

        private IEnumerator<float> _AutoDamage(double damagePerSec)
        {
            // 딜레이 
            yield return Timing.WaitForSeconds(autoDamageDelay);
            
            // 자동 데미지 시작 
            while (!boss.IsDied)
            {
                // 프레임 당 데미지 
                var damagePerFrame = damagePerSec * Time.deltaTime;
                boss.Damage(damagePerFrame);
                
                // 다음 프레임 
                yield return Timing.WaitForOneFrame;
            }
        }

        public IEnumerable<RaidUserSlot> PickUserSlot(int count)
        {
            // 캐릭터 살아있는 슬롯들만 추림 (랭킹역순위로 정렬)
            _queue ??= new Queue<RaidUserSlot>();
            _queue.Clear();
            for (var i = userSlots.Length - 1; i >= 0; i--)
            {
                var userSlot = userSlots[i];
                if (!userSlot.IsAlive) continue;
                
                _queue.Enqueue(userSlot);
            }
            
            // 몇 마리만 뺌 (넉넉하게 요청한 갯수의 두 배)
            var randomCount = count * 2;
            _list ??= new List<RaidUserSlot>();
            _list.Clear();
            for (var i = 0; i < randomCount; i++)
            {
                if (_queue.Count > 0) _list.Add(_queue.Dequeue());
                else break;
            }
            
            // 셔플 
            return _list.OrderBy(x => Guid.NewGuid()).Take(count);
        }
        
        /*
         * 승리
         */
        private void OnDieBoss()
        {
            BranchResult();
        }

        /*
         * 패배 
         */
        public void Fail()
        {
            BranchResult();
        }

        /*
         * 승패 분기 
         */
        private void BranchResult()
        {
            // 최악의 경우, 연출 상의 결과와 레이드 시작할 때 넘어온 결과가 다를 수 있음  
            // 그런 상황이 벌어져도 항상 '시작할 때 넘어온 결과'가 출력되도록 함 
            if (_victory)
            {
                // 플로우
                if (_victoryFlow.IsRunning) return;
                _victoryFlow = Timing.RunCoroutine(_VictoryFlow().CancelWith(gameObject));
                
                // 로그 
                WebLog.Instance.AllLog("raid_victory", null);
            }
            else
            {
                // 플로우
                if (_failFlow.IsRunning) return;
                _failFlow = Timing.RunCoroutine(_FailFlow().CancelWith(gameObject));
                
                // 로그 
                WebLog.Instance.AllLog("raid_fail", null);
            }
        }

        private IEnumerator<float> _VictoryFlow()
        {
            // 이벤트
            OnEndStarted?.Invoke();

            // 공격 중지 
            foreach (var userSlot in userSlots) userSlot.StopAttack();
            
            // 보스 사망 연출 
            yield return Timing.WaitUntilDone(boss._DeathDirection());
            
            // 승리 팝업 
            _resultPopup = RaidVictoryPopup.Show(AmIOwner, ToExit, ToRejoin, _rewardIdx, _rewardGirlIdx, Difficulty, _canRewardDouble);
        }
        
        private IEnumerator<float> _FailFlow()
        {
            // 이벤트
            OnEndStarted?.Invoke();
            
            // 오토 데미지 끔 
            Timing.KillCoroutines(_autoDamage);
            
            // 보스 행성 먹기 
            yield return Timing.WaitUntilDone(boss._EatPlanet());
            
            // 행성 폭발 이펙트 
            LeanPool.Spawn(planetExplosionEffect, planetExplosionPos.position, Quaternion.identity);
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_raid_planet_destroy");
            
            // 행성 이미지 교체 
            planet.sprite = planetAfterExplosion;
            
            // 아군 캐릭터 터트리기 
            foreach (var userSlot in userSlots) userSlot.DieWithEffect();
            
            // 딜레이
            yield return Timing.WaitForSeconds(delayToShowFail); 
            
            // 패배 팝업
            _resultPopup = RaidFailPopup.Show(AmIOwner, ToExit, ToRejoin);
        }


        /*
         * 결과 공통
         */
        private UIPopup _resultPopup;
        private CoroutineHandle _toExit;
        private CoroutineHandle _toRejoin;


        private void ToExit()
        {
            Timing.KillCoroutines(_toExit);
            _toExit = Timing.RunCoroutine(_ToExit().CancelWith(gameObject));
        }

        private IEnumerator<float> _ToExit()
        {
            // 나가기 (씬이 이동돼도 오브젝트는 바로 파괴되지 않기에, 아래 루틴까지 정상 처리된다)
            yield return Timing.WaitUntilDone(_Exit());
            
            // 처리 다 끝낸 후, 팝업 닫기 
            _resultPopup.Hide();
        }

        private void ToRejoin()
        {
            Timing.KillCoroutines(_toRejoin);
            _toRejoin = Timing.RunCoroutine(_ToRejoin().CancelWith(gameObject));
        }

        private IEnumerator<float> _ToRejoin()
        {
            // 방 이동 처리 
            LoadingIndicator.Show();
            
            // 내가 방장이라면, 바로 다음 방 이동 가능 
            var isSuccess = false;
            
            if (ServerRaid.IsRoomOwner)
                yield return Timing.WaitUntilDone(ServerRaid._MoveToNextRoomForOwner((success) => { isSuccess = success; }));
            else
            {
                var msg = "";
                yield return Timing.WaitUntilDone(ServerRaid._MoveToNextRoomForAttendee((success, reason) => { 
                    isSuccess = success;
                    msg = reason;
                }));
                
                // 결과 메시지 출력 
                RaidController.ShowFailedMessage(msg);
            }
            
            LoadingIndicator.Hide();
            
            // 만약 실패했다면, 아래 처리 하지 못하도록 함 
            // 팝업을 닫지 않았기에, 팝업에서 재시도 가능 
            if (!isSuccess) yield break;

            // 모드 종료 처리 (스킬 쿨타임 초기화) 
            RaidMode.Instance.End();
            
            // 방 이동 됐기에 레이드 다시 세팅  
            Init();
            RefreshUsersSlotAndCell();

            // 처리 다 끝낸 후, 팝업 닫기 
            _resultPopup.Hide();
        }
        
        /*
         * 자동 나가기 
         */
        private void BranchAutoExit()
        {
            // 방 정보 가져오기 
            if (!ServerRaid.TryGetRoomInfo(out _, out _, out _, out var users)) return;

            var canAutoExit = users.Count() == 1;

            if (canAutoExit)
                StartAutoExit();
            else
                StopAutoExit();
        }
        
        private void StartAutoExit()
        {
            // 이미 진행중이라면, 냅둠 
            if (_autoExit.IsRunning) return;

            // 진행 중 아니라면, 진행시킴 
            _autoExit = Timing.RunCoroutine(_AutoExitCountDown().CancelWith(gameObject));
        }
			
        private void StopAutoExit()
        {
            Timing.KillCoroutines(_autoExit);

            // 카운트다운 텍스트 초기화 
            autoExitCountDownText.text = "";
        }
			
        private IEnumerator<float> _AutoExitCountDown()
        {
            // 카운트 다운
            var endTime = Time.time + autoExitTime;
            while (Time.time < endTime)
            {
                var remainTime = endTime - Time.time;
                var min = (int)(remainTime / 60);
                var sec = (int)(remainTime % 60);

                autoExitCountDownText.text = Localization.GetFormatText("info_374", min.ToLookUpString(), sec.ToLookUpString());

                // 1초 마다 갱신 
                yield return Timing.WaitForSeconds(1);
            }

            // 이 떄까지 버튼 입력 없었으면 자동 스테이지 재시작  
            yield return Timing.WaitUntilDone(_Exit());
        }
    }
}