using TMPro;
using UnityEngine;

namespace InGame.Raid
{
    public class RaidUserCell : MonoBehaviour
    {
        [SerializeField] private TextMeshProUGUI rankText;
        [SerializeField] private TextMeshProUGUI nicknameText;
        [SerializeField] private TextMeshProUGUI bonusText;
        [SerializeField] private TextMeshProUGUI damageText;
        [SerializeField] private GameObject marking;
        [SerializeField] private GameObject owner;

        public void SetData(int rank, string nickname, float bonusPercent, bool mark, bool isOwner, double damage)
        {
            rankText.text = rank.ToLookUpString();
            nicknameText.text = nickname;
            bonusText.text = Localization.GetFormatText("info_244", bonusPercent.ToString("0.#"));
            marking.SetActive(mark);
            damageText.text = damage.ToUnitString();
            owner.SetActive(isOwner);
        }
        
        public void SetActive(bool aActive)
        {
            gameObject.SetActive(aActive);
        }
    }
}