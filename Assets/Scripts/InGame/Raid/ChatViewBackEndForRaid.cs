﻿using System;
using InGame.Raid;
using Server;
using UnityEngine;

namespace UI
{
    [RequireComponent(typeof(ChatViewBackEnd))]
    public class ChatViewBackEndForRaid : MonoBehaviour
    {
        private ChatViewBackEnd _chatViewBackEnd;

        private void Awake()
        {
            _chatViewBackEnd = GetComponent<ChatViewBackEnd>();
        }

        public void PromoteRoom()
        {
            // 레이드 씬 안에서 호출돼야 함 
            if (Raid.Instance == null) return;

            // 방 번호 홍보
            ServerRaid.TryGetRoomInfo(out var roomNum, out _, out _, out _);
            
            _chatViewBackEnd.Send(roomNum);
        }
    }
}