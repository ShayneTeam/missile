﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Raid
{
    public class UserTargetingPos : CanFindWithInDate<UserTargetingPos>
    {
        [SerializeField] public int targetingIndex;
    }
}