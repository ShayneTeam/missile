using System.Collections.Generic;
using Doozy.Engine.UI;
using Lean.Pool;
using MEC;
using UI;
using UnityEngine;

namespace InGame.Raid
{
    public class RaidBossBall : MonoBehaviour
    {
        public float moveSpeed;
        private CoroutineHandle _moving;
        private RaidUserSlot _target;
        private bool _miss;

        public void Init(RaidUserSlot target, bool miss)
        {
            _target = target;
            _miss = miss;
            _moving = Timing.RunCoroutine(_Move().CancelWith(gameObject), Segment.FixedUpdate);
        }

        private void OnDisable()
        {
            Timing.KillCoroutines(_moving);
        }

        private IEnumerator<float> _Move()
        {
            var transformCached = transform;
            var myPosition = transformCached.position;
            var targetPosition = _target.CharacterPosition;
            var dir = targetPosition - myPosition;
            
            // 프레임율에 따라 타겟 위치를 지나칠 수 있으므로, 항상 duration 기반으로 처리돼야 함
            var distance = Vector2.Distance(targetPosition, myPosition);
            var duration = distance / moveSpeed;
            var elapsedTime = 0.0f;
            while (elapsedTime < duration)
            {
                elapsedTime += Time.fixedDeltaTime;
            
                var moveSpeedPerFrame = moveSpeed * Time.fixedDeltaTime;
                var nextPos = transformCached.position + (dir.normalized * moveSpeedPerFrame);
            
                transformCached.position = nextPos;

                yield return Timing.WaitForOneFrame;
            }
            
            // 캐릭터 살아있을 때만 처리  
            if (_target.IsAlive)
            {
                // 빗나갔다면
                if (_miss)
                {
                    // 회피 성공 메시지 
                    NormalTextEffect.Show(Localization.GetText("raid_043"), Color.yellow, targetPosition);
                }
                // 빗나가지 않았다면
                else
                {
                    // 폭발 이펙트 
                    _target.DieWithExplosion();
                }
            }
            
            // 심심하니 이펙트 하나 
            EffectSpawner.SpawnRandomDamageEffect(transformCached.position);

            // 도달했다면 반납 
            LeanPool.Despawn(this);
        }
    }
}