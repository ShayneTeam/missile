﻿using System;
using InGame.Skill;
using Lean.Pool;
using UnityEngine;

namespace InGame.Raid
{
    [RequireComponent(typeof(T999))]
    public class RaidT999 : MonoBehaviour, IPoolable
    {
        [SerializeField] private GameObject explosionEffect;
        
        public void OnSpawn()
        {
            
        }

        public void OnDespawn()
        {
            LeanPool.Spawn(explosionEffect, transform.position, Quaternion.identity);
            
            // 사운드  
            SoundController.Instance.PlayEffect("sfx_t999_explode_sound");
        }
    }
}