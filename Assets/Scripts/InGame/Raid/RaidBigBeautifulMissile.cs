﻿using Lean.Pool;
using UnityEngine;

namespace InGame.Raid
{
    [RequireComponent(typeof(ProjectileMoving))]
    public class RaidBigBeautifulMissile : Projectile
    {
        [SerializeField] private GameObject explosionEffect;
        
        public override void Shot(Vector3 targetPos)
        {
            ShotWithStraight(targetPos, false);
        }
        
        protected override double GetBaseDamage()
        {
            return 0;
        }

        protected override bool TryCritical(double damage, out double criticalDamage)
        {
            criticalDamage = 0;
            return false;
        }

        public override void OnDespawn()
        {
            base.OnDespawn();
            
            // 좀 더 큰 이펙트 출력  
            // MissileRemover에 의해 삭제되므로, TriggerEnemy 처리는 들어오지 않음 
            // 그래서 그냥 꺼질 때 이펙트 크게 터트리도록 함 
            // OnDisable에서 처리하니 transform.position 위치가 달라져서 OnDespawn에서 해야 좌표값을 쓸 수 있음 
            LeanPool.Spawn(explosionEffect, transform.position, Quaternion.identity);
            
            // 사운드  
            SoundController.Instance.PlayEffect("sfx_t999_explode_sound");
        }
    }
}