﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Raid
{
    public class UserMissileScale : CanFindWithInDate<UserMissileScale>
    {
        [SerializeField] public float scale;
    }
}