﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Bolt;
using Doozy.Engine.UI;
using Firebase.Analytics;
using Global;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using MEC;
using Server;
using TMPro;
using UI;
using UI.Popup;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

namespace InGame.WormHole
{
    public class WormHole : MonoBehaviour
    {
        /*
         * 씬 내에서만 싱글턴 
         */
        #region Scene Object

        public static WormHole Instance;

        private void Awake()
        {
            Instance = this;
            
            // 타임바 일단 꺼두기
            timeBar.gameObject.SetActive(false);
        }

        #endregion

        /*
         * 웜홀 주요 변수  
         */
        public int Difficulty { get; private set;  }
        public string WormHoleIdx => WormHoleController.GetWormHoleIdxByDifficulty(Difficulty);
        
        /*
         * 인스펙터 지정
         */
        [SerializeField] private TextMeshProUGUI difficultyName;

        [SerializeField] private BarSlicedFill timeBar;
        [SerializeField] private TextMeshProUGUI timeText;
        [SerializeField] private Image bossIcon;

        [SerializeField] private Transform skills;
        
        /*
         * 실패 관련 
         */
        private CoroutineHandle _failFlow;
        
        /*
         * 보스 스폰
         */
        private readonly HashSet<string> _spawnedBosses = new HashSet<string>();
        private CoroutineHandle _timer;
        
        /*
         * 획득 강화칩 히스토리 저장용
         */
        public int EarnedChipset { get; set; }
        
        /*
         * 시간 
         */
        [NonSerialized] public long StartTimestamp;
        
        /*
         * 이벤트
         */
        public static VoidDelegate OnStart;
        
        /*
         * 아미
         */
        private ArmyWormhole MyArmy => InGame.Army.My as ArmyWormhole;
        

        public void StartMode(int difficulty)
        {
            // BGM
            SoundController.Instance.PlayBGM("bgm_dg_battle_01");
            
            // 난이도 세팅
            Difficulty = difficulty;
            
            // 초기화 
            EarnedChipset = 0;
            _spawnedBosses.Clear();
            
            // 0 초기화 매우 중요. 하지 않으면 재시작될 때, 잠금 걸려있는 스킬들까지 사용되는 현상 발생 
            StartTimestamp = 0;
            
            // UI 갱신 
            SetHUD();

            // 시작 플로우
            Timing.RunCoroutine(_StartFlow().CancelWith(gameObject));
        }

        private void SetHUD()
        {
            // 배경
            var bg = DataboxController.GetDataString(Table.WormHole, Sheet.wormhole, WormHoleIdx, Column.bg_id, "BG_01");
            BackgroundController.Instance.SetBg(AtlasController.GetSprite(bg));
            
            // 난이도 이름 
            difficultyName.text = Localization.GetFormatText("wormhole_11", Difficulty.ToLookUpString());
        }

        private IEnumerator<float> _StartFlow()
        {
            // 커튼 치기
            var curtainShowAsync = CurtainScreenEffect.Instance.Show();
            while (!curtainShowAsync.IsDone)
                yield return Timing.WaitForOneFrame;
            
            // 심플하게 모든 것 싹 다 반납
            LeanPool.DespawnAll();

            // 아군, 적 전부 날리기 (린풀에서의 반납과 클래스 상에서의 캐싱리스트 반납과는 별개임)
            Enemies.Instance.DespawnAll();
            MyArmy.DespawnAll();

            // 한 프레임 쉬기
            yield return Timing.WaitForOneFrame;

            // 아군 부대 스폰 
            MyArmy.SpawnAll();
            MyArmy.ReadyLandingAll();

            // 커튼 딜레이
            yield return Timing.WaitForSeconds(CurtainScreenEffect.Instance.CurtainWaitingDelaySec);

            // 커튼 열기
            var curtainHideAsync = CurtainScreenEffect.Instance.Hide();
            while (!curtainHideAsync.IsDone)
                yield return Timing.WaitForOneFrame;

            // 낙하 연출        
            MyArmy.LandingAll(MyArmy.LandingTime);
            yield return Timing.WaitForSeconds(MyArmy.LandingTime);
            
            // 첫 보스 스폰
            var allBosses = DataboxController.GetAllIdxes(Table.WormHole, Sheet.boss); 
            var firstBossIdx = allBosses.First();
            Timing.RunCoroutine(_SpawnBossFlow(firstBossIdx).CancelWith(gameObject));
			
            // 타이머 세팅
            ResetTimer(firstBossIdx);
            
            // 공격 시작 
            MyArmy.Attack();
            
            // 시작 시간 기록 
            StartTimestamp = ServerTime.Instance.NowSec;
            
            // 시작 시간 초기화한 다음 호출해야만 함
            // 그래야 RemainCoolTimeSec이 제대로 계산됨 
            for (var i = 0; i < skills.childCount; i++)
            {
                var skill = skills.GetChild(i);
                CustomEvent.Trigger(skill.gameObject, "Reset");
            }
            
            // 이벤트 
            OnStart?.Invoke();
        }

        private IEnumerator<float> _SpawnBossFlow(string bossIdx)
        {
            // 이미 스폰한 보스라면 넘김
            if (_spawnedBosses.Contains(bossIdx))
                yield break;
			
            // 보스 스폰 
            var boss = Enemies.Instance.SpawnBoss(bossIdx); 
            boss.OnDie += OnDieBoss;
				
            // 스폰한 보스 히스토리에 추가 
            _spawnedBosses.Add(bossIdx);
        }

        private void ResetTimer(string currentBossIdx)
        {
            // 타이머 리셋
            if (_timer.IsRunning) 
                Timing.KillCoroutines(_timer);

            // 다음 보스 타이머 예약
            var nextBossIdx = DataboxController.GetDataString(Table.WormHole, Sheet.boss, currentBossIdx, Column.next);
            
            // 스폰할 다음 보스 없다면
            if (string.IsNullOrEmpty(nextBossIdx) || nextBossIdx == "0")
            {
                // 타이머 안보이기
                timeBar.gameObject.SetActive(false);
            }
            // 스폰할 다음 보스 있다면
            else
            {
                // 타이머 보이기
                timeBar.gameObject.SetActive(true);
                
                // 타이머 시작
                _timer = Timing.RunCoroutine(_TimerFlow(nextBossIdx).CancelWith(gameObject));                
                
                // 다음 보스 아이콘 표시
                var nextBossIcon = DataboxController.GetDataString(Table.WormHole, Sheet.boss, nextBossIdx, Column.icon);
                bossIcon.SetSpriteWithOriginSIze(AtlasController.GetSprite(nextBossIcon));
            }
        }

        private IEnumerator<float> _TimerFlow(string nextBossIdx)
        {
            // 타이머 바 표시 
            var spawnInterval = (float)DataboxController.GetDataInt(Table.WormHole, Sheet.wormhole, WormHoleIdx, Column.boss_gen_time);
            var remainTimeSec = spawnInterval;
            while (remainTimeSec > 0f)
            {
                remainTimeSec -= Time.deltaTime;
                var remainTimeRate = remainTimeSec / spawnInterval;
                timeBar.SetFill(remainTimeRate);
                
                // 타임 텍스트 
                var timeStr = remainTimeSec.ToString("0.0");
                timeText.text = Localization.GetFormatText("wormhole_12",timeStr);
			
                yield return Timing.WaitForOneFrame;
            }
			
            timeBar.SetFill(0f);
			
            // 타이머 다 됐다면, 다음 보스 스폰 
            Timing.RunCoroutine(_SpawnBossFlow(nextBossIdx).CancelWith(gameObject));

            // 타이머 초기화
            ResetTimer(nextBossIdx);
        }

        private void OnDieBoss(Enemy.Enemy boss)
        {
            // 이벤트 해제 (린풀이라 빼줘야 함)
            boss.OnDie -= OnDieBoss;
			
            // 다음 스폰할 보스가 없다면
            var nextBossIdx = DataboxController.GetDataString(Table.WormHole, Sheet.boss, boss.DataIdx, Column.next);
            if (string.IsNullOrEmpty(nextBossIdx) || nextBossIdx == "0")
            {
                // 웜홀 승리 
                Timing.RunCoroutine(_VictoryFlow().CancelWith(gameObject));

                return;
            }
            
            // 다음 보스가 이미 스폰됐다면 그냥 종료
            if (_spawnedBosses.Contains(nextBossIdx))
                return;
            
            // 다음 보스 스폰 
            Timing.RunCoroutine(_SpawnBossFlow(nextBossIdx).CancelWith(gameObject));
            
            // 타이머 초기화
            ResetTimer(nextBossIdx);
        }

        private IEnumerator<float> _VictoryFlow()
        {
            // 웜홀 승리 처리 전에 최초 클리어하는 행성인지 여부 캐싱
            var isFirstClear = !WormHoleController.IsCleared(Difficulty);
            
            // 승리 처리
            WormHoleController.Victory(Difficulty, EarnedChipset);
            
            /*
             * 웜홀 팝업의 난이도 커서 갱신
             * 이걸 컨트롤러 Clear 안에서 해주려다, 웜홀과 커서는 독립적 개념이라 여기서 따로 처리시킴 
             */
            UserData.My.WormHole.DifficultyCursor = Difficulty;
            
            // 초기화 
            (EnemyMode.Behaviour as IEnd)?.End();
        
            // 승리 팝업 (승리 처리 후에 해야 티켓 차감 반영됨)
            WormHoleChallengePopup.Show(isFirstClear);

            WebLog.Instance.AllLog("wormhole_victory", new Dictionary<string, object>{{"stage",Difficulty}});

            yield break;
        }


        #region Fail

        public void Fail()
        {
            // 이거 한 번 들어오면 Enemy 충돌은 계속 들어올 수 있으니, 여기서 FailFlow 진행중이면 알아서 막아줘야 한다 
            if (_failFlow.IsRunning)
                return;

            _failFlow = Timing.RunCoroutine(_FailFlow().CancelWith(gameObject));
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_battle_defaet");
        }

        private IEnumerator<float> _FailFlow()
        {
            // Army 전부 사망 처리  
            MyArmy.DieAll();

            // 약간의 딜레이 
            yield return Timing.WaitForSeconds(3f);

            // 타이머 종료 및 안 보이기 
            if (_timer.IsRunning) 
                Timing.KillCoroutines(_timer);
            
            timeBar.gameObject.SetActive(false);

            WebLog.Instance.AllLog("wormhole_fail", new Dictionary<string, object>{{"stage",Difficulty}});

            // 웜홀 나가기 
            WormHoleController.ExitWormHole();
        }

        #endregion
        
        
    }
}