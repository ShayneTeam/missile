﻿using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using JetBrains.Annotations;
using UnityEngine;

public class CurtainScreenEffect : MonoBehaviourSingletonPersistent<CurtainScreenEffect>
{
    [SerializeField] private UIView viewCurtain;
    
    [SerializeField] private float curtainWaitingDelaySec = 0.3f;
    public float CurtainWaitingDelaySec => curtainWaitingDelaySec;
    
    private readonly CustomAsync _asyncShow = new CustomAsync();
    private readonly CustomAsync _asyncHide = new CustomAsync();

    public CustomAsync Show()
    {
        _asyncShow.IsDone = _asyncShow.IsSuccess = false;

        // 이미 보여지는 상태라면, 즉각 처리해줌 
        if (viewCurtain.IsVisible)
            FinishShow();
        else
            viewCurtain.Show();
				
        return _asyncShow;
    }

    public CustomAsync Hide()
    {
        _asyncHide.IsDone = _asyncHide.IsSuccess = false;

        // 이미 가려진 상태라면, 즉각 처리해줌 
        if (viewCurtain.IsHidden)
            FinishHide();
        else
            viewCurtain.Hide();
        
        return _asyncHide;
    }

    [PublicAPI]
    public void OnShowFinished()
    {
        FinishShow();
            
        // 커튼쳤을 때, 수동 가비지 회수 
        GCController.CollectFull();
    }

    [PublicAPI]
    public void OnHideFinished()
    {
        FinishHide();
    }

    private void FinishShow()
    {
        _asyncShow.IsDone = _asyncShow.IsSuccess = true;
    }

    private void FinishHide()
    {
        _asyncHide.IsDone = _asyncHide.IsSuccess = true;
    }
}
