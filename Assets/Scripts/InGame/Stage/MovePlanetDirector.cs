﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Doozy.Engine.UI;
using Global;
using MEC;
using UnityEngine;

public class MovePlanetDirector : MonoBehaviour
{
    #region 싱글톤

    public static MovePlanetDirector Instance;

    private void Awake()
    {
        Instance = this;

        // 새롭게 씬 로딩되면, 기존건 날리고 새로운 걸로 대체시키기 위함 
        //DontDestroyOnLoad(this);
    }

    #endregion
    
    
    
    
    
    [SerializeField] private UIView movePlanetView;

    [SerializeField] private PlanetSlot planetFrom;
    [SerializeField] private PlanetSlot planetTo;
    
    [SerializeField] private CharacterSlot character;
    
    
    
    
    
    private readonly CustomAsync _playing = new CustomAsync(); 
	
    private bool _isViewShowFinished;
	
	
    // 
    public void ViewShowFinished()
    {
	    _isViewShowFinished = true;
    }
	
    public void ViewHideFinished()
    {
	    _isViewShowFinished = false;
    }

    public CustomAsync Play(string planetIdxFrom, string planetIdxTo)
    {
	    _playing.IsDone = _playing.IsSuccess = false;
	    
	    planetFrom.Init(planetIdxFrom);
	    planetTo.Init(planetIdxTo);

	    // 캐릭터 세팅
	    character.WearMyCostume();
		
	    var fromPos = planetFrom.transform.position;
	    character.transform.position = fromPos;

	    // 연출 시작 
	    Timing.RunCoroutine(Direction().CancelWith(gameObject));

	    // 
	    return _playing;

    }

    private IEnumerator<float> Direction()
    {
		// UI View Show 
	    movePlanetView.Show();
		
	    // 뷰 보여질 때까지 기다리기 
	    while (!_isViewShowFinished)
		    yield return Timing.WaitForOneFrame;
	    
		// 이동 
	    yield return Timing.WaitUntilDone(_MoveCharacterSlot(character, planetTo.transform.position).CancelWith(gameObject));
		
	    // UI View Hide 
	    movePlanetView.Hide();
		
	    // 뷰 안 보여질 때까지 기다리기 
	    while (_isViewShowFinished)
		    yield return Timing.WaitForOneFrame;
		
	    // 밖에다 끝났다고 알리기
	    _playing.IsDone = _playing.IsSuccess = true;
    }

    public static IEnumerator<float> _MoveCharacterSlot(CharacterSlot character, Vector3 destination)
    {
	    // 연출 전 약간의 딜레이 (아예 없으면 너무 빠름)
	    const float delay = 0.2f;

	    yield return Timing.WaitForSeconds(delay);
	    
	    // 사운드 
	    SoundController.Instance.PlayEffect("sfx_ui_planet_move");
		
		// 연출 시간 		
	    const float duration = 2f;
	    
	    // 무빙 트윈
	    character.transform.DOMove(destination, duration);

	    // 스케일 트윈 
	    character.transform.DOPunchScale(Vector3.one * 1.5f, duration, 0, 0f).SetRelative(true);
		
	    // 트윈 끝날 때까지 기다리기 
	    yield return Timing.WaitForSeconds(duration);
	    
	    // 연출 후 약간의 딜레이 
	    yield return Timing.WaitForSeconds(delay);
    }
}
