﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using Bolt;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Controller.Event;
using InGame.Data;
using JetBrains.Annotations;
using Lean.Pool;
using Ludiq;
using MEC;
using TMPro;
using UI.Popup;
using UI.Popup.Menu;
using UnityEngine;
using Random = UnityEngine.Random;
using UserData = InGame.Data.UserData;
// ReSharper disable MemberCanBePrivate.Global


namespace InGame.Global
{
    public class Stage : MonoBehaviour
    {
        #region Scene Object

        public static Stage Instance;

        private void Awake()
        {
            Instance = this;
        }

        #endregion

        [SerializeField] private TextMeshProUGUI planetName;

        [SerializeField] private float failDelay = 1f;
        [SerializeField] private float showRewardDelay = 2f;

        private CoroutineHandle _failFlow;
        private CoroutineHandle _successFlow;
        private CoroutineHandle _nextStageWave;
        private CoroutineHandle _nextStageWaveWithEffect;
        private CoroutineHandle _moveToNextPlanet;
        private CoroutineHandle _jumpToFirstStage;
        private CoroutineHandle _moveStage;

        public static event VoidDelegate OnStart;
        
        private ArmyStage MyArmy => InGame.Army.My as ArmyStage;

        private void Start()
        {
            // BGM
            SoundController.Instance.PlayBGM("bgm_adventure");

            // 적 전멸 이벤트 등록
            Enemies.Instance.OnEliminated += OnEliminated;
            
            // 데이타 이벤트 
            UserData.My.Soldiers.OnPromoted += OnPromotedSoldier;
            
            // 데이터에 저장된 반복 모드 유무 설정 
            Stage.Instance.SetRepeat(UserData.My.Stage.IsRepeatMode);

            // 웨이브 시작
            Timing.RunCoroutine(_StartStageWaveFlow().CancelWith(gameObject));
            
            // 이벤트 
            OnStart?.Invoke();
        }

        private void OnDestroy()
        {
            // 이벤트 해제 
            Enemies.Instance.OnEliminated -= OnEliminated;
            
            UserData.My.Soldiers.OnPromoted -= OnPromotedSoldier;
        }

        private void OnPromotedSoldier(string type)
        {
            // 스테이지 예외사항이 많으므로, 용병 진급 시엔 랜딩 처리 다시 하기 위해 재시작함 
            Timing.RunCoroutine(_RestartPlanetFlow().CancelWith(gameObject));
        }
        

        private IEnumerator<float> _StartStageWaveFlow()
        {
            // 행성 배경 설정 
            SetBg();

            // HUD 갱신
            SetPlanetName();

            // 커튼 치기
            var curtainShowAsync = CurtainScreenEffect.Instance.Show();
            while (!curtainShowAsync.IsDone)
                yield return Timing.WaitForOneFrame;

            // 아군 부대 스폰 
            MyArmy.SpawnAll();
            yield return Timing.WaitForOneFrame;
            MyArmy.ReadyLandingAll();

            //
			yield return Timing.WaitForSeconds(CurtainScreenEffect.Instance.CurtainWaitingDelaySec);

            // 커튼 열기
            var curtainHideAsync = CurtainScreenEffect.Instance.Hide();
            while (!curtainHideAsync.IsDone)
                yield return Timing.WaitForOneFrame;

            // 낙하 연출        
            MyArmy.LandingAll(MyArmy.LandingTime);
            yield return Timing.WaitForSeconds(MyArmy.LandingTime);

            // 웨이브 시작 
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var stage, out var planetIdx);
            yield return Timing.WaitUntilDone(Wave.Instance.StartWave(stage, planetIdx).CancelWith(gameObject));
            
            // 말풍선 
            SpeechCharacterBubble();

            // 공격 시작 
            MyArmy.Attack();
            
            // 스테이지 맨 처음 시작 할 때,
            // 출석 팝업 보여주기
            EventController.NAttendance.ShowPopupIfCan();
            //가이드 팝업 띄어주기
            GuideController.ShowRandomGuideOneIfCan();
        }


        #region Success
        
        private void OnEliminated()
        {
            // 패배 처리가 먼저 들어왔다면, 승리 처리 안 함 
            if (_failFlow.IsRunning)
                return;
            
            // 스테이지 강제 이동하는 중이라면, 도중에 성공 처리 하게 막음 
            if (_moveStage.IsRunning)
                return;
            
            _successFlow = Timing.RunCoroutine(_ProcessSuccess().CancelWith(gameObject));
        }

        private IEnumerator<float> _ProcessSuccess()
        {
            // 용병 획득 체크 
            yield return Timing.WaitUntilDone(EarnNewSoldierIfCanFlow().CancelWith(gameObject));

            // 행성 획득 체크 
            yield return Timing.WaitUntilDone(EarnNewPlanetIfCanFlow().CancelWith(gameObject));
            
            // 웨이브별 전개 
            BranchWave();
        }

        private IEnumerator<float> EarnNewSoldierIfCanFlow()
        {
            // 획득 체크 
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var currentStage, out _);

            var earnSheet = DataboxController.GetAllIdxes(Table.Stage, Sheet.Earn);
            foreach (var earnStage in earnSheet)
            {
                // 클리어한 스테이지에서 획득 가능한 것이 없다면 넘김  
                if (earnStage != currentStage.ToLookUpString())
                    continue;

                var newSoldierType = DataboxController.GetDataString(Table.Stage, Sheet.Earn, earnStage, "soldier_type", "soldier");

                // 이미 획득했다면 패스
                if (UserData.My.Soldiers.IsEarned(newSoldierType))
                    continue;

                // 획득처리 
                UserData.My.Soldiers.Earn(newSoldierType);

                // 획득 팝업 
                var popup = UIPopup.GetPopup("Earned New Soldier");
                
                var earnedSoldierPopup = popup.GetComponent<EarnedSoldierPopup>();
                earnedSoldierPopup.Init(newSoldierType);
                
                popup.Show();
                
                // 팝업 닫힐 때까지 무한 대기
                yield return Timing.WaitUntilDone(popup.WaitHide().CancelWith(gameObject));

                // 획득된 녀석 스폰 
                MyArmy.SpawnSoldierType(newSoldierType, true);

                // 랜딩 기다리기
                yield return Timing.WaitForSeconds(MyArmy.LandingTime);
            }
        }

        private IEnumerator<float> EarnNewPlanetIfCanFlow()
        {
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var currentStage, out var currentPlanetIdx);

            // 현 스테이지가 마지막 스테이지가 아니라면 제외
            var isLastStage = InGameControlHub.My.StageController.IsLastStage(currentStage, currentPlanetIdx);
            if (!isLastStage)
                yield break;
	
            // 이미 획득한 행성이라면 제외 
            if (InGameControlHub.My.StageController.IsOwned(currentPlanetIdx))
                yield break;
		
            // 보유 처리
            InGameControlHub.My.StageController.EarnPlanet(currentPlanetIdx);
	
            // 새로운 행성 보유 팝업 
            var popup = UIPopup.GetPopup("Owned New Planet");
            var ownedNewPlanetPopup = popup.GetComponent<OwnedPlanetPopup>();
            ownedNewPlanetPopup.Init(currentPlanetIdx);

            popup.Show();

            // 획득 팝업 닫힐 때까지 무한 대기
            yield return Timing.WaitUntilDone(popup.WaitHide().CancelWith(gameObject));
        }
        
        private void BranchWave()
        {
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var currentStage, out var currentPlanetIdx);

            Wave.WhatIsThisStageWaveType(currentStage, currentPlanetIdx, out var waveType);
            
            var isLastStage = InGameControlHub.My.StageController.IsLastStage(currentStage, currentPlanetIdx);
            
            // 마지막 행성 예외 처리 
            {
                // 클리어한 스테이지가 마지막 스테이지이고, 마지막 행성이라면, 반복 모드 강제 활성 
                var currentPlanetIndex = StageControl.GetPlanetIndex(currentPlanetIdx);
                var isLastPlanet = StageControl.IsLastPlanet(currentPlanetIndex);
                if (isLastStage && isLastPlanet)
                {
                    SetRepeat(true);
                }
            }
            
            // 분기 처리 
            if (UserData.My.Stage.IsRepeatMode)
            {
                if (isLastStage)
                {
                    JumpToFirstStage();
                }
                else
                {
                    BranchStageCommon(waveType);
                }
            }
            else
            {
                if (isLastStage)
                {
                    MoveToNextPlanet();
                }
                else
                {
                    BranchStageCommon(waveType);
                }
            }
        }

        private void BranchStageCommon(Wave.WaveType waveType)
        {
            switch (waveType)
            {
                case Wave.WaveType.Default:
                case Wave.WaveType.Elite:
                    NextStageWave();
                    break;
                case Wave.WaveType.Boss:
                case Wave.WaveType.Diablo:
                    NextStageWaveWithEffect();
                    break;
            }
        }

        #endregion

        
        #region Branch 

        private void NextStageWave()
        {
            // 스테이지 강제 이동하는 중이라면, 도중에 성공 처리 하게 막음 
            if (_moveStage.IsRunning)
                return;
            
            // 다음 웨이브 진행 
            _nextStageWave = Timing.RunCoroutine(_NextStageWaveFlow().CancelWith(gameObject));
        }
	
        private void NextStageWaveWithEffect()
        {
            // 스테이지 강제 이동하는 중이라면, 도중에 성공 처리 하게 막음 
            if (_moveStage.IsRunning)
                return;
            
            // 다음 웨이브 진행 (with 커튼 연출)
            _nextStageWaveWithEffect = Timing.RunCoroutine(_NextStageWaveWithEffectFlow().CancelWith(gameObject));
        }

        private void MoveToNextPlanet()
        {
            // 다음 행성 이동 (with 커튼 연출)
            var isDiffPlanet = InGameControlHub.My.StageController.MoveToNextPlanet(out var prevPlanetIdx, out var nextPlanetIdx);

            // 다른 행성 이동이라면, 행성 이동 연출 보여주기
            // 같은 행성 이동이라면, 행성 재시작
            _moveToNextPlanet = Timing.RunCoroutine(isDiffPlanet ? _MoveToTargetPlanetFlow(prevPlanetIdx, nextPlanetIdx, showRewardDelay) : _RestartPlanetFlow().CancelWith(gameObject));
        }
        
        private void JumpToFirstStage()
        {
            // 해당 행성 첫 스테이지로 이동 
            _jumpToFirstStage = Timing.RunCoroutine(_JumpToFirstStageFlow().CancelWith(gameObject));
        }

        
        private IEnumerator<float> _NextStageWaveFlow()
        {
            // 클리어해서 넘어왔으므로, Army 스폰과 랜딩처리는 뺄 수 있음 

            // 스테이지 증가 
            InGameControlHub.My.StageController.AddStageData();

            SetPlanetName();

            // 웨이브 시작 
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var stage, out var planetIdx);
            yield return Timing.WaitUntilDone(Wave.Instance.StartWave(stage, planetIdx).CancelWith(gameObject));

            // 튜토리얼_ 스테이지에 도달했을때 진행하는 튜토리얼이 있는지 체크한다.
            TutorialPopup.Instance.TutorialConditionCheck(TutorialPopup.GuideConditionType.STAGE, stage);
            
            // 말풍선 
            SpeechCharacterBubble();

            // 공격 시작 
            MyArmy.Attack();

            yield return Timing.WaitForOneFrame;
        }

        private IEnumerator<float> _NextStageWaveWithEffectFlow()
        {
            // 좀 약간의 보상 보이는 딜레이 주기
            yield return Timing.WaitForSeconds(showRewardDelay);

            // 스테이지 증가 
            InGameControlHub.My.StageController.AddStageData();

            // 커튼 치기
            var curtainShowAsync = CurtainScreenEffect.Instance.Show();
            while (!curtainShowAsync.IsDone)
                yield return Timing.WaitForOneFrame;

            // 공통 재시작 플로우 
            yield return Timing.WaitUntilDone(_CommonRestartFlow().CancelWith(gameObject));
            
            // 가이드 보여줄 수 있다면 보여줌 (보스 클리어 후)
            GuideController.ShowRandomGuideOneIfCan();
        }
        
        private IEnumerator<float> _MoveToTargetPlanetFlow(string prevPlanetIdx, string targetPlanetIdx, float delay)
        {
            // 약간 딜레이 주기 
            yield return Timing.WaitForSeconds(delay);
            
            // 커튼 치기
            var curtainShowAsync = CurtainScreenEffect.Instance.Show();
            while (!curtainShowAsync.IsDone)
                yield return Timing.WaitForOneFrame;

            // 행성 이동
            var movePlanetAsync = MovePlanetDirector.Instance.Play(prevPlanetIdx, targetPlanetIdx);
            while (!movePlanetAsync.IsDone)
                yield return Timing.WaitForOneFrame;

            // 공통 재시작 플로우 
            yield return Timing.WaitUntilDone(_CommonRestartFlow().CancelWith(gameObject));
            
            // 가이드 보여줄 수 있다면 보여줌 (다른 행성 이동 후)
            GuideController.ShowRandomGuideOneIfCan();
        }

        private IEnumerator<float> _JumpToFirstStageFlow()
        {
            // 약간 딜레이 주기 
            yield return Timing.WaitForSeconds(showRewardDelay);
            
            // 해당 행성 첫 스테이지 얻기
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var currentStage, out var currentPlanetIdx);
            
            var moveStage = InGameControlHub.My.StageController.GetFinalizedFirstStage(currentPlanetIdx);

            // 스테이지 데이터 변경 
            InGameControlHub.My.StageController.SetStage(moveStage, out _);

            // 행성 재시작 
            yield return Timing.WaitUntilDone(_RestartPlanetFlow().CancelWith(gameObject));
        }

        #endregion


        #region Fail

        public void Fail()
        {
            // 이거 한 번 들어오면 Enemy 충돌은 계속 들어올 수 있으니, 여기서 FailFlow 진행중이면 알아서 막아줘야 한다 
            if (_failFlow.IsRunning)
                return;

            // 성공 처리가 먼저 들어왔다면, 실패 처리 안 함 
            if (_successFlow.IsRunning)
                return;

            _failFlow = Timing.RunCoroutine(_Fail().CancelWith(gameObject));
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_battle_defaet");
        }

        private IEnumerator<float> _Fail()
        {
            // Army 전부 사망 처리  
            MyArmy.DieAll();

            // 약간의 딜레이 
            yield return Timing.WaitForSeconds(failDelay);

            // 스테이지 실패 팝업  
            UIPopup popup = null;
            if (!UIPopup.AnyPopupVisible)   // 메뉴 떠 있을 때, 뜨면 성가심 
                popup = StageFailPopup.Show();
            else // 메뉴 떠있으면, 즉각 이동 
                yield return Timing.WaitUntilDone(StageFailPopup._MoveRetryStage());
            
            // 주의. 이 구문은 항상 밑에 있어야 함
            // 팝업 닫히는 걸 기다리고 플로우가 종료돼야, Fail이 또 들어오지 않음
            if (popup != null)
                yield return Timing.WaitUntilDone(popup.WaitHide().CancelWith(gameObject));
        }

        #endregion


        #region Move

        public bool MoveStage(int targetStage)
        {
            // Move처럼 일반적인 스테이지 시퀀스 도중에 꼽사리 되는 호출을 주의할 것 
            // 버그가 발생 시, 디버깅도 어렵고, 재현도 어렵다
            // 이런 꼽사리 로직들은 기존 시퀀스들을 확실하게 처리 할 책임이 있음  
            
            // 데이터 상으로 가능한 스테이지인지 체크 
            var canStage = StageControl.IsValidStage(targetStage);
            if (!canStage) return false;

            // 성공 플로우 중이라면, 스테이지 이동 막음
            // 성공 플로우 도중에 Move가 꼽사리 되면 SetStage가 반영 되면서, 이동한 스테이지가 자동 클리어 처리 됨
            // (실패 플로우는 안에서 Move 로직을 태우므로, Move가 되게 해야 함)
            if (_successFlow.IsRunning /*|| _failFlow.IsRunning*/) return false;

            // Move하고 다시 Move 바로 들어오면, 이전 플로우 캔슬 
            Timing.KillCoroutines(_moveStage);

            // 현재 행성 스테이지 +1 가능성 있는 코루틴 강제 종료 (매우 중요. 유저 악용 가능)
            Timing.KillCoroutines(_nextStageWave);
            Timing.KillCoroutines(_nextStageWaveWithEffect);
            
            // 잠재적 예외 가능성 때문에 플로우 전부 다 종료
            Timing.KillCoroutines(_moveToNextPlanet);
            Timing.KillCoroutines(_jumpToFirstStage);
            
            // 모든 걸 먼저 싹 다 반납해놔야, 연출 도중에 다른 스테이지 처리가 꼽사리 끼지 않음 
            LeanPool.DespawnAll();

            // 기존 스테이지와 기존 행성 캐싱 
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out _, out var prevPlanetIdx);

            // 스테이지 이동
            InGameControlHub.My.StageController.SetStage(targetStage, out var nextPlanetIdx);

            //
            var isDiffPlanet = nextPlanetIdx != prevPlanetIdx;

            // 재시작
            _moveStage = Timing.RunCoroutine(isDiffPlanet ? _MoveToTargetPlanetFlow(prevPlanetIdx, nextPlanetIdx, 0f) : _RestartPlanetFlow().CancelWith(gameObject));

            return true;
        }

        #endregion
        
        
        #region Common
        
        private IEnumerator<float> _RestartPlanetFlow()
        {
            // 커튼 치기
            var curtainShowAsync = CurtainScreenEffect.Instance.Show();
            while (!curtainShowAsync.IsDone)
                yield return Timing.WaitForOneFrame;

            // 공통 재시작 플로우 
            yield return Timing.WaitUntilDone(_CommonRestartFlow().CancelWith(gameObject));
            
            // 가이드 보여줄 수 있다면 보여줌 (같은 행성 이동)
            GuideController.ShowRandomGuideOneIfCan();
        }
        
        private IEnumerator<float> _CommonRestartFlow()
        {
            // 다음 Fail 처리가 될 수 있게 만듬
            Timing.KillCoroutines(_failFlow);
            
            // 행성 배경 설정 
            SetBg();

            // HUD 갱신 
            SetPlanetName();

            // 심플하게 모든 것 싹 다 반납
            LeanPool.DespawnAll();

            // 아군, 적 전부 날리기 (린풀에서의 반납과 클래스 상에서의 캐싱리스트 반납과는 별개임)
            Enemies.Instance.DespawnAll();
            MyArmy.DespawnAll();

            // 한 프레임 쉬기
            yield return Timing.WaitForOneFrame;

            // 아군 다시 스폰 
            MyArmy.SpawnAll();
            yield return Timing.WaitForOneFrame;
            MyArmy.ReadyLandingAll();

            //
            yield return Timing.WaitForSeconds(CurtainScreenEffect.Instance.CurtainWaitingDelaySec);

            // 커튼 열기
            var curtainHideAsync = CurtainScreenEffect.Instance.Hide();
            while (!curtainHideAsync.IsDone)
                yield return Timing.WaitForOneFrame;

            // 낙하 연출
            MyArmy.LandingAll(MyArmy.LandingTime);
            yield return Timing.WaitForSeconds(MyArmy.LandingTime);

            // 웨이브 시작 
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var stage, out var planetIdx);
            yield return Timing.WaitUntilDone(Wave.Instance.StartWave(stage, planetIdx).CancelWith(gameObject));
            
            // 말풍선 
            SpeechCharacterBubble();

            // 공격 시작 
            MyArmy.Attack();
        }

        private void SpeechCharacterBubble()
        {
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var stage, out var planetIdx);
            
            Wave.WhatIsThisStageWaveType(stage, planetIdx, out var waveType);

            switch (waveType)
            {
                case Wave.WaveType.Default:
                {
                    // 첫 스테이지라면 
                    var firstStage = UserData.My.Stage.IsRepeatMode ? InGameControlHub.My.StageController.GetFinalizedFirstStage(planetIdx) : InGameControlHub.My.StageController.GetFirstStage(planetIdx);
                    if (stage == firstStage)
                    {
                        // 100% 출력 
                        var scriptIdx = StageControl.GetScript(planetIdx, ScriptType.first);
                        MyArmy.Character.ShowSpeechBubble(scriptIdx);
                    }
                    // 일반 스테이지라면 
                    else
                    {
                        // 확률 돌려서 출력 
                        var scriptChance = float.Parse(DataboxController.GetConstraintsData("STAGE_NORMAL_SCRIPT_RATE"), CultureInfo.InvariantCulture);
				
                        var dice = Random.Range(0.01f, 100f);
                        if (dice <= scriptChance)
                        {
                            var scriptIdx = StageControl.GetScript(planetIdx, ScriptType.normal_elite);
                            MyArmy.Character.ShowSpeechBubble(scriptIdx);
                        }
                    }
                }
                    break;
                case Wave.WaveType.Elite:
                {
                    // 확률 돌려서 출력 
                    var scriptChance = float.Parse(DataboxController.GetConstraintsData("STAGE_ELITE_SCRIPT_RATE"), CultureInfo.InvariantCulture);
				
                    var dice = Random.Range(0.01f, 100f);
                    if (dice <= scriptChance)
                    {
                        var scriptIdx = StageControl.GetScript(planetIdx, ScriptType.normal_elite);
                        MyArmy.Character.ShowSpeechBubble(scriptIdx);
                    }
                }
                    break;
                case Wave.WaveType.Boss:
                {
                    // 100% 출력 
                    var scriptIdx = StageControl.GetScript(planetIdx, ScriptType.boss);
                    MyArmy.Character.ShowSpeechBubble(scriptIdx);
                }
                    break;
                case Wave.WaveType.Diablo:
                {
                    // 100% 출력 
                    var scriptIdx = StageControl.GetScript(planetIdx, ScriptType.diablo);
                    MyArmy.Character.ShowSpeechBubble(scriptIdx);
                }
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }

        #endregion


        #region Set

        public void SetRepeat(bool isRepeat)
        {
            UserData.My.Stage.IsRepeatMode = isRepeat;
        }

        private void SetBg()
        {
            // UserData에게 현 Stage 데이타를 얻어오기 
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out _, out var planetIdx);

            var bg = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, "bg_id", "BG_01");
            BackgroundController.Instance.SetBg(AtlasController.GetSprite(bg));
        }

        public void SetPlanetName()
        {
            var planetNameStr = GetStageTitleStr();

            planetName.text = planetNameStr;
        }

        public string GetStageTitleStr()
        {
            // UserData에게 현 Stage 데이타를 얻어오기 
            InGameControlHub.My.StageController.GetCurrentStageAndPlanetData(out var stage, out var planetIdx);

            // 볼트 상태머신에 의해 설정되는 변수 
            var planetNameColumnKey = UserData.My.Stage.IsRepeatMode ? "repeat_name" : "planet_name";
            var planetNameKey = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, planetNameColumnKey);
            var planetNameStr = Localization.GetFormatText(planetNameKey, stage.ToLookUpString());
            
            return planetNameStr;
        }

        #endregion
    }
}