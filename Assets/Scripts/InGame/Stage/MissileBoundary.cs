using System;
using InGame.Common;
using UnityEngine;

namespace InGame.Global
{
    public class MissileBoundary : MonoBehaviourSingletonPersistent<MissileBoundary>
    {
        private void OnTriggerExit2D(Collider2D other)
        {
            MissileRemover.Remove(other, transform);
        }
    }
}