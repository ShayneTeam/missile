﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using Doozy.Engine.UI;
using Global;
using Global.Extensions;
using InGame.Controller;
using InGame.Data;
using InGame.Enemy;
using InGame.Fairy;
using Lean.Pool;
using MEC;
using UI.Popup;
using UI.Popup.Menu;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Random = UnityEngine.Random;

namespace InGame.Global
{
    public class Wave : MonoBehaviour
    {
        #region Scene Object

        public static Wave Instance;

        private void Awake()
        {
            Instance = this;
        }

        private void OnDestroy()
        {
            // 다른 씬으로 바꼈는데도, 포탈이 스폰되는 이슈가 있음 
            // 타이밍 이슈가 있는 듯 싶어서 날아갈 때, 바로 날려봄
            // 이거 안되면 OnLoadedScene 이벤트 받아서 해보자
            Timing.KillCoroutines(_defaultFlow);
        }

        #endregion

        private CoroutineHandle _defaultFlow;
        

        public enum WaveType
        {
            Default,
            Elite,
            Boss,
            Diablo
        }

        public IEnumerator<float> StartWave(int stage, string planetIdx)
        {
            // 현 스테이지의 웨이브 모드 알아오기
            WhatIsThisStageWaveType(stage, planetIdx, out var waveType);

            // 웨이브 모드대로 진행 
            switch (waveType)
            {
                case WaveType.Default:
                {
                    _defaultFlow = Timing.RunCoroutine(_DefaultFlow(stage, planetIdx).CancelWith(gameObject));
                } break;

                case WaveType.Elite:
                {
                    SpawnElite(planetIdx);
                } break;

                case WaveType.Boss:
                {
                    yield return Timing.WaitUntilDone(_BossFlow(planetIdx).CancelWith(gameObject));
                } break;

                case WaveType.Diablo:
                {
                    yield return Timing.WaitUntilDone(_DiabloFlow().CancelWith(gameObject));
                } break;

                default:
                {
                    SpawnPortal(planetIdx);
                } break;
            }
        }

        public static void WhatIsThisStageWaveType(int stage, string planetIdx, out WaveType outWaveType)
        {
            // 1 스테이지 단위 - 포탈 & 몬스터 
            outWaveType = WaveType.Default;

            // 10 스테이지 단위 - 엘리트
            var isEliteStage = stage % 10 == 0;
            if (isEliteStage)
                outWaveType = WaveType.Elite;

            // 100 스테이지 단위 - 보스
            var isBossStage = stage % 100 == 0;
            if (isBossStage)
                outWaveType = WaveType.Boss;

            // 마지막 스테이지 - 디아블로 (특정 행성 이상부터 등장)
            var planetIndex = StageControl.GetPlanetIndex(planetIdx);
            if (planetIndex < Diablo.UnlockPlanetIndex)
                return;
        
            var isLastStage = InGameControlHub.My.StageController.IsLastStage(stage, planetIdx);
            if (isLastStage)
                outWaveType = WaveType.Diablo;
        }

        private IEnumerator<float> _DefaultFlow(int stage, string planetIdx)
        {
            // 요정 스폰 
            Timing.RunCoroutine(_RollAndSpawnFairy("38001", stage,"FAIRY_LITTLE_RATE", "FAIRY_LITTLE_BOX_RATE").CancelWith(gameObject));
            Timing.RunCoroutine(_RollAndSpawnFairy("38002",stage, "FAIRY_BIG_RATE", "FAIRY_BIG_BOX_RATE").CancelWith(gameObject));
            
            // 골렘 소환 플로우 동안은 전멸 이벤트가 떠서 스테이지가 전환되지 않도록 막는다
            Enemies.Instance.PauseEliminate();
            
            // 미네랄 골렘 스폰 
            yield return Timing.WaitUntilDone(_RollAndSpawnGolemFlow(planetIdx, "STAGE_GOLEM_MINERAL_RATE", "GOLEM_MINERAL_RATE", "golem_mineral").CancelWith(gameObject));

            // 가스 골렘 스폰 
            yield return Timing.WaitUntilDone(_RollAndSpawnGolemFlow(planetIdx, "STAGE_GOLEM_GAS_RATE", "GOLEM_GAS_RATE", "golem_gas").CancelWith(gameObject));

            // 지옥석 골렘 스폰 
            yield return Timing.WaitUntilDone(_RollAndSpawnGolemFlow(planetIdx, "STAGE_GOLEM_STONE_RATE", "GOLEM_STONE_RATE", "golem_stone").CancelWith(gameObject));

            // 골렘 전부 스폰 후, 포탈 스폰
            SpawnPortal(planetIdx);
            
            // 포탈이 소환 된 후 부터 다시 전멸 이벤트가 뜰 수 있게 한다
            Enemies.Instance.ResumeEliminate();
        }
        
        private IEnumerator<float> _BossFlow(string planetIdx)
        {
            // 경고 연출 팝업
            if (!UIPopup.AnyPopupVisible)
            {
                var popup = UIPopup.GetPopup("Warning Boss");
                popup.Show();

                yield return Timing.WaitUntilDone(popup.WaitHide().CancelWith(gameObject));
            }
            
            // 경고 연출 후, 보스 스폰 
            SpawnBoss(planetIdx);
        }
        
        private IEnumerator<float> _DiabloFlow()
        {
            // 경고 연출 팝업
            if (!UIPopup.AnyPopupVisible)
            {
                var popup = WarningDiabloPopup.Show();
                yield return Timing.WaitUntilDone(popup.WaitHide().CancelWith(gameObject));
            }
            
            // 경고 연출 후, 디아블로 스폰 
            SpawnDiablo();
        }
		
        private IEnumerator<float> _RollAndSpawnGolemFlow(string planetIdx, string defaultChanceKey, string abilityName, string type)
        {
            var golemDefaultChance = float.Parse(DataboxController.GetConstraintsData(defaultChanceKey, "4"), CultureInfo.InvariantCulture);
            var abilityGolemAddChance = InGameControlHub.My.AbilityController.GetUnifiedValue(abilityName);
            var abilityAllGolemAddChance = InGameControlHub.My.AbilityController.GetUnifiedValue("GOLEM_ALL_RATE");
            var finalGolemChance = golemDefaultChance + abilityGolemAddChance + abilityAllGolemAddChance;

            var rolledChance = Random.Range(0.01f, 100f);
            var canSpawn = rolledChance <= finalGolemChance;
            if (!canSpawn)
                yield break;


            // 현재 웨이브 찾기 
            var waveGroup = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, "wave_group");

            // 웨이브 시트에서 골렘 찾기 
            RandomPick(planetIdx, type, out var golemWaveIdx);

            // 골렘 인덱스 
            var golemIdx = DataboxController.GetDataString(Table.Wave, waveGroup, golemWaveIdx, "summon_idx");

            var delayMin = DataboxController.GetDataFloat(Table.Wave, waveGroup, golemWaveIdx, "summon_delay_min");
            var delayMax = DataboxController.GetDataFloat(Table.Wave, waveGroup, golemWaveIdx, "summon_delay_max");

            // 골렘 스폰 
            Enemies.Instance.SpawnGolem(golemIdx);

            // 딜레이만큼 대기
            var delaySec = Random.Range(delayMin, delayMax);
            yield return Timing.WaitForSeconds(delaySec);
        }
    
        private Dictionary<string, GameObject> fairyDictionary = new Dictionary<string, GameObject>();
        private static List<string> _listInWave;

        private IEnumerator<float> _RollAndSpawnFairy(string fairyIdx, int stage, string abilitySpawnChanceName, string abilityBoxChanceName)
        {
            // 스폰 체크 
            var spawnChance = DataboxController.GetDataFloat(Table.Fairy, Sheet.Fairy, fairyIdx, Column.spawn_chance);
            var abilitySpawnAddChance = InGameControlHub.My.AbilityController.GetUnifiedValue(abilitySpawnChanceName);
            var abilityAllFairySpawnAddChance = InGameControlHub.My.AbilityController.GetUnifiedValue(Ability.FAIRY_ALL_RATE);

            var finalizedSpawnChance = spawnChance + abilitySpawnAddChance + abilityAllFairySpawnAddChance;

            var rolledSpawnChance = Random.Range(0.01f, 100f);
            var canSpawn = rolledSpawnChance <= finalizedSpawnChance;
            if (ExtensionMethods.IsUnityEditor())
            {
                var isAlwaysSpawn = DataboxController.GetDataBool(Table.Cheat, Sheet.Cheat, Row.Test, Column.fairy_always_spawn);
                if (isAlwaysSpawn) canSpawn = true;
            }
            if (!canSpawn)
                yield break;

            // 박스 체크 
            var boxChance = DataboxController.GetDataFloat(Table.Fairy, Sheet.Fairy, fairyIdx, Column.box_chance);
            var abilityBoxAddChance = InGameControlHub.My.AbilityController.GetUnifiedValue(abilityBoxChanceName);
            var abilityCommonBoxAddChance = InGameControlHub.My.AbilityController.GetUnifiedValue(Ability.FAIRY_BOX_RATE);

            var finalizedBoxChance = boxChance + abilityBoxAddChance + abilityCommonBoxAddChance;

            var rolledBoxChance = Random.Range(0.01f, 100f);
            var isBox = rolledBoxChance <= finalizedBoxChance;

            // 스폰시키기
            if (!fairyDictionary.ContainsKey(fairyIdx))
            {
                var spawnAsync = Addressables.LoadAssetAsync<GameObject>(fairyIdx);
                while (!spawnAsync.IsDone) yield return Timing.WaitForOneFrame;
                if (spawnAsync.Status != AsyncOperationStatus.Succeeded)
                    yield break;
                
                fairyDictionary.Add(fairyIdx, spawnAsync.Result);
            }

            Fairy.Fairy.Spawn(fairyIdx, stage, fairyDictionary[fairyIdx], isBox, FairySpawn.Pos, transform);
        }

        
        private void SpawnPortal(string planetIdx)
        {
            // 일반 몬스터와 포탈 한 종을 뽑기 
            var waveGroup = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, "wave_group");
        
            RandomPick(planetIdx, "portal", out var randomPortalWaveIdx);

            // 뽑힌 녀석 
            var portalIdx = DataboxController.GetDataString(Table.Wave, waveGroup, randomPortalWaveIdx, "summon_idx");

            Enemies.Instance.SpawnPortal(portalIdx);
        }

        private void SpawnElite(string planetIdx)
        {
            RandomPick(planetIdx, "elite", out var randomEliteWaveIdx);

            // 뽑힌 녀석 
            var waveGroup = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, "wave_group");
            var eliteIdx = DataboxController.GetDataString(Table.Wave, waveGroup, randomEliteWaveIdx, "summon_idx");

            Enemies.Instance.SpawnElite(eliteIdx);
        }

        private void SpawnBoss(string planetIdx)
        {
            // 해당 웨이브의 보스 찾기 
            RandomPick(planetIdx, "boss", out var bossWaveIdx);
	
            // 
            var waveGroup = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, "wave_group");
            var bossIdx = DataboxController.GetDataString(Table.Wave, waveGroup, bossWaveIdx, "summon_idx");
	
            // 스폰 
            Enemies.Instance.SpawnBoss(bossIdx);
        }

        private void SpawnDiablo()
        {
            Enemies.Instance.SpawnDiablo();
        }

        // 포탈이 pull 방식으로 Wave에게 자신이 어떤 몬스터를 소환해야하는지 요청 
        // 어드레서블 비동기 로딩 방식이라 웨이브가 포탈에게 Push 방식으로 전달 불가 
        public static void RandomPickMonster(string planetIdx, string pickType, out string outMonsterIdx, out int outSpawnMin, out int outSpawnMax, out float outDelayMin, out float outDelayMax)
        {
            var waveGroup = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, "wave_group");
        
            RandomPick(planetIdx, pickType, out var randomMonsterWaveIdx);

            outSpawnMin = DataboxController.GetDataInt(Table.Wave, waveGroup, randomMonsterWaveIdx, "summon_count_min");
            outSpawnMax = DataboxController.GetDataInt(Table.Wave, waveGroup, randomMonsterWaveIdx, "summon_count_max");

            outDelayMin = DataboxController.GetDataFloat(Table.Wave, waveGroup, randomMonsterWaveIdx, "summon_delay_min");
            outDelayMax = DataboxController.GetDataFloat(Table.Wave, waveGroup, randomMonsterWaveIdx, "summon_delay_max");

            outMonsterIdx = DataboxController.GetDataString(Table.Wave, waveGroup, randomMonsterWaveIdx, "summon_idx");
        }

        private static void RandomPick(string planetIdx, string pickType, out string outPickedIdx)
        {
            // 웨이브 테이블 얻어오기  
            var waveGroup = DataboxController.GetDataString(Table.Stage, Sheet.planet, planetIdx, "wave_group");

            var allIdxesInWave = DataboxController.GetAllIdxes(Table.Wave, waveGroup);

            // 분리 
            _listInWave ??= new List<string>();
            _listInWave.Clear();
            
            foreach (var idx in allIdxesInWave)
            {
                var type = DataboxController.GetDataString(Table.Wave, waveGroup, idx, "type");
                if (type == pickType) _listInWave.Add(idx);
            }

            // 하나 뽑기 
            var randomIndex = Random.Range(0, _listInWave.Count);
            outPickedIdx = _listInWave[randomIndex];
        }

        public static void GetRandomSpawnPos(ref Bounds monsterArenaBounds, ref List<Vector2> posOfMonstersBefore, out Vector2 outRandomSpawnPos)
        {
            // 거리두기 거리 
            const float wantDistancing = 0.3f;

            var spawnPos = Vector2.zero;

            var possible = false;
            var tryCount = 0;
            const int tryCountMax = 10;
            while (!possible)
            {
                spawnPos.x = Random.Range(monsterArenaBounds.min.x, monsterArenaBounds.max.x);
                spawnPos.y = Random.Range(monsterArenaBounds.min.y, monsterArenaBounds.max.y);

                possible = true;

                foreach (var posOfMonster in posOfMonstersBefore)
                {
                    // 하나라도 거리두기에 걸리면 다시 뽑기 
                    var distance = Vector2.Distance(spawnPos, posOfMonster);
                    if (distance < wantDistancing)
                    {
                        possible = false;

                        break;
                    }
                }

                // 무한루프에 빠지지 않도록, 특정 횟수 돌렸으면 강제 종료 
                tryCount++;
                if (tryCount >= tryCountMax)
                {
                    break;
                }
            }

            // 루프 탈출 했다면 반환 
            outRandomSpawnPos = spawnPos;

            // 다음 녀석 스폰 계산에서도 쓸 수 있게 추가시킴
            posOfMonstersBefore.Add(spawnPos);
        }
    }
}