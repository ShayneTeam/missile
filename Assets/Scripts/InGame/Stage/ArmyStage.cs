﻿using System;
using System.Collections.Generic;
using Global;
using InGame.Global;
using Lean.Pool;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame
{
    [RequireComponent(typeof(SpawnCharacter))]
    [RequireComponent(typeof(SpawnSoldiers))]
    [RequireComponent(typeof(SpawnPrincess))]
    [RequireComponent(typeof(SpawnLandingTime))]
    public class ArmyStage : Army
    {
        public override Character Character => _spawnCharacter.Character;
        public override List<Soldier> Soldiers => _spawnSoldiers.Soldiers;
        public override Princess Princess => _spawnPrincess.Princess;
        public float LandingTime => _spawnLandingTime.landingTime;

        private SpawnCharacter _spawnCharacter;
        private SpawnSoldiers _spawnSoldiers;
        private SpawnPrincess _spawnPrincess;
        private SpawnLandingTime _spawnLandingTime;

        private List<Ally> _pickList;

        protected void Awake()
        {
            _spawnCharacter = GetComponent<SpawnCharacter>();
            _spawnSoldiers = GetComponent<SpawnSoldiers>();
            _spawnPrincess = GetComponent<SpawnPrincess>();
            _spawnLandingTime = GetComponent<SpawnLandingTime>();
        }


        public void SpawnAll()
        {
            // 캐릭터 스폰
            var character = _spawnCharacter.Spawn();
            character.OnDie += OnDieCharacter;
            
            // 공주 스폰 
            if (!FoundControlHub.PrincessController.IsLocked) _spawnPrincess.Spawn();

            // 용병 스폰 
            var allEarnedTypes = FoundControlHub.Data.Soldiers.GetAllEarnedTypes();
            foreach (var soldierType in allEarnedTypes) SpawnSoldierType(soldierType);
        }
        
        public void SpawnSoldierType(string soldierType, bool autoLanding = false)
        {
            var soldierData = FoundControlHub.Data.Soldiers[soldierType];

            // grade 테이블 인덱스를 알아야 스폰 위치, 스폰할 솔져 인덱스 알아올 수 있음 
            Soldier.GetGradeIdx(soldierType, soldierData.grade, out var gradeIdx);

            // 현재 용병 타입의 등급에 해당하는 스폰 갯수
            var count = DataboxController.GetDataInt(Table.Soldier, Sheet.Grade, gradeIdx, "soldier_count", 1);
            for (var index = 1; index <= count; index++)
            {
                var soldier = _spawnSoldiers.Spawn(index, gradeIdx);
                soldier.OnDie += OnDieSoldier;
            
                // 자동 랜딩
                if (autoLanding) soldier.Landing(LandingTime);
            }
        }

        private void OnDieSoldier(Soldier soldier)
        {
            _spawnSoldiers.Soldiers.Remove(soldier);

            // 사망 이벤트를 이 클래스에서 등록시켰으므로, 빼는 것도 이 클래스에서 빼주기 
            soldier.OnDie -= OnDieSoldier;
        }

        private void OnDieCharacter(Character character)
        {
            _spawnCharacter.Character = null;

            character.OnDie -= OnDieCharacter;
            
            // 공주도 같이 죽기 
            _spawnPrincess.Die();
        }

        public override void DespawnAll()
        {
            // 캐릭터
            _spawnCharacter.Despawn();
            
            // 공주
            _spawnPrincess.Despawn();

            // 용병
            _spawnSoldiers.DespawnAll();
        }

        public void ReadyLandingAll()
        {
            // 캐릭터
            _spawnCharacter.ReadyLanding();
            
            // 공주 
            _spawnPrincess.ReadyLanding();

            // 용병
            _spawnSoldiers.ReadyLandingAll();
        }

        public void LandingAll(float duration)
        {
            // 캐릭터
            _spawnCharacter.Landing(duration);
            
            // 공주 
            _spawnPrincess.Landing(duration);
            
            // 용병
            _spawnSoldiers.Landing(duration);

            // 사운드 
            PlayLandingSound(duration);
        }

        public void Attack()
        {
            // 캐릭터
            _spawnCharacter.Attack();
            
            // 공주 
            _spawnPrincess.Attack();

            // 용병
            _spawnSoldiers.Attack();
        }
        
        public void Stop()
        {
            // 캐릭터
            _spawnCharacter.Stop();
            
            // 공주 
            _spawnPrincess.Stop();

            // 용병
            _spawnSoldiers.Stop();
        }

        public void DieAll()
        {
            // 캐릭터
            _spawnCharacter.Die();
            
            // 공주 
            _spawnPrincess.Die();

            // 용병
            _spawnSoldiers.DieAll();
        }

        public override void PickOneWithoutAirShip(out Ally outAlly)
        {
            _pickList ??= new List<Ally>();
            _pickList.Clear();

            // 캐릭터 넣기
            if (_spawnCharacter.Character != null)
                _pickList.Add(_spawnCharacter.Character);

            // 용병 넣기 
            foreach (var soldier in Soldiers)
            {
                // 그라운드 타입만 넣기
                if (soldier.IsGround)
                    _pickList.Add(soldier);
            }

            // 랜덤 픽 
            outAlly = _pickList.Count == 0 ? null : _pickList[Random.Range(0, _pickList.Count)];
        }
    }
}