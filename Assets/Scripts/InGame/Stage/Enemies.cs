﻿using System;
using System.Collections.Generic;
using System.Linq;
using Global;
using InGame.Controller;
using InGame.Controller.Mode;
using InGame.Enemy;
using InGame.Global;
using Lean.Pool;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using Random = UnityEngine.Random;

namespace InGame
{
    public class Enemies : MonoBehaviourSingletonPersistent<Enemies>
    {
        [SerializeField] private Transform arenaParent;

        private readonly Dictionary<string, Bounds> _arenaBounds = new Dictionary<string, Bounds>();

        private readonly List<Enemy.Enemy> _enemies = new List<Enemy.Enemy>();

        public static event EnemyDelegate OnSpawnEnemyDied;

        private readonly Dictionary<string, GameObject> _enemiesPrefab = new Dictionary<string, GameObject>();
        private readonly Dictionary<string, AsyncOperationHandle<GameObject>> _asyncHandles = new Dictionary<string, AsyncOperationHandle<GameObject>>();

        private readonly Dictionary<GameObject, Enemy.Enemy> _cachedEnemies = new Dictionary<GameObject, Enemy.Enemy>(); 

        // 전멸 이벤트 
        public event VoidDelegate OnEliminated;
        private bool _isPausedEliminate;

        public Enemy.Enemy SpawnGolem(string idx)
        {
            GetSpawnArenaBounds(idx, Sheet.Monster, out var bounds);
            
            // 스폰 위치 
            var spawnPos = Vector2.zero;
            spawnPos.x = Random.Range(bounds.min.x, bounds.max.x); 
            spawnPos.y = Random.Range(bounds.min.y, bounds.max.y); 
            
            return Spawn(idx, spawnPos);
        }

        public Enemy.Enemy SpawnPortal(string idx)
        {
            GetSpawnArenaBounds(idx, Sheet.Monster, out var bounds);

            return Spawn(idx, bounds.center);
        }
    
        public Enemy.Enemy SpawnMonster(string idx, Vector3 position)
        {
            return Spawn(idx, position);   
        }
    
        public Enemy.Enemy SpawnElite(string idx)
        {
            GetSpawnArenaBounds(idx, Sheet.Monster, out var bounds);

            return Spawn(idx, bounds.center);
        }
    
        public Enemy.Enemy SpawnBoss(string idx)
        {
            GetSpawnArenaBounds(idx, Sheet.boss, out var bounds);

            return Spawn(idx, bounds.center);
        }
        
        public Enemy.Enemy SpawnDiablo()
        {
            var allDiabloIdxes = DataboxController.GetAllIdxes(EnemyMode.Behaviour.MainTable, Sheet.Diablo);
            var idx = allDiabloIdxes.ElementAt(0); // 디아블로 시트에 가장 첫번째 것을 넘기는게 가장 심플해서 이렇게 함  
            GetSpawnArenaBounds(idx, Sheet.Diablo, out var bounds);
            return Spawn(idx, bounds.center);
        }

        private Enemy.Enemy Spawn(string idx, Vector3 position)
        {
            GameObject prefab = null;
            if (_enemiesPrefab.ContainsKey(idx))
            {
                // ULogger.Log($@"{idx} ID 를 가진 적이 [오브젝트 풀 에서] 로드되었습니다.");
                prefab = _enemiesPrefab[idx];
            }
            else
            {
                // 어드레서블 동기 로딩 방식 
                // 다른 씬으로 이동했는데, 어드레서블 후속 로딩되는 경우가 잦아서 예외 상황 자주 발생
                // 아예 그런 예외케이스가 발생되지 않도록, 동기 처리
                var go = AddressableController.LoadAssetSync(idx);
                if (go == null) return null;
                
                if (!_enemiesPrefab.ContainsKey(idx)) _enemiesPrefab.Add(idx, go);
                
                // ULogger.Log($@"{idx} ID 를 가진 적이 [어드레서블 에서] 로드되었습니다.");
                prefab = go;
            }
            
            var spawn = LeanPool.Spawn(prefab, position, Quaternion.identity, transform);
            if (!_cachedEnemies.TryGetValue(spawn, out var enemy))
            {
                enemy = spawn.GetComponentInChildren<Enemy.Enemy>();
                _cachedEnemies[spawn] = enemy;
            }
                
            enemy.Init(idx);
            enemy.OnDie += OnDieEnemy;
        
            _enemies.Add(enemy);

            return enemy;
        }
    
        public void DespawnAll()
        {
            foreach (var enemy in _enemies)
            {
                LeanPool.Despawn(enemy.transform.parent.gameObject);    // 프리팹 안에 프리팹이 들어가는 구조라, 최상단 루트를 반납해야 한다      
            }
        
            // 이 함수를 호출하는 궁극적 목적. 타게팅 가능한 몹이 없도록 함
            _enemies.Clear();
        }
    
        public IEnumerable<Enemy.Enemy> GetTargetableEnemies()
        {
            return _enemies.Where(enemy => enemy.CanTarget());
        }

        private void OnDieEnemy(Enemy.Enemy enemy)
        {
            _enemies.Remove(enemy);
        
            // 사망 이벤트를 이 클래스에서 등록시켰으므로, 빼는 것도 이 클래스에서 해줘야 함 (실은 이벤트 구독 해지는 안에서 못해서 그럼)
            enemy.OnDie -= OnDieEnemy;
        
            // 총 에너미 개수 0개 되면, 엘리 이벤트 
            if (_enemies.Count <= 0 && !_isPausedEliminate)
            {
                OnEliminated?.Invoke();
            }
            
            // 이벤트 
            OnSpawnEnemyDied?.Invoke(enemy);
        }

        public void PauseEliminate()
        {
            _isPausedEliminate = true;
        }
        
        public void ResumeEliminate()
        {
            _isPausedEliminate = false;
        }

        public void GetSpawnArenaBounds(string idx, string tableSheet, out Bounds outBounds)
        {
            var summonPosition = DataboxController.GetDataString(EnemyMode.Behaviour.MainTable, tableSheet, idx, "summon_position", "normal_arena");
            GetArenaBounds(summonPosition, out outBounds);
        }

        public void GetArenaBounds(string arenaName, out Bounds bounds)
        {
            if (_arenaBounds.TryGetValue(arenaName, out bounds)) 
                return;
            
            var arenaTransform = arenaParent.Find(arenaName);
            var arena = arenaTransform.GetComponent<SpriteRenderer>();

            bounds = arena.bounds;

            _arenaBounds[arenaName] = bounds;
        }

        public void AddEnemy(Enemy.Enemy enemy)
        {
            _enemies.Add(enemy);
        }

        public void RemoveEnemy(Enemy.Enemy enemy)
        {
            _enemies.Remove(enemy);
        }
    }
}
