﻿using System.Collections.Generic;
using InGame.Controller.Control;
using UnityEngine;
using Utils.Layer;

namespace InGame
{
    public abstract class Army : CanFindWithInDate<Army>
    {
        /*
         * 아미 공통
         */
        [Header("발사하는 투사체의 레이어"), SerializeField, Layer] private int projectileLayer;
        public int ProjectileLayer => projectileLayer;

        /*
         * 용병
         */
        public abstract List<Soldier> Soldiers { get; }

        /*
         * 캐릭터
         */
        public abstract Character Character { get; }
        
        /*
         * 공주 
         */
        public abstract Princess Princess { get; }
        
        
        

        protected static void PlayLandingSound(float delay)
        {
            SoundController.Instance.PlayEffect("sfx_ui_cha_gen", delay);
        }

        public abstract void PickOneWithoutAirShip(out Ally outAlly);
        public abstract void DespawnAll();
    }

    public interface ILanding
    {
        void ReadyLanding();
        void Landing(float duration);
    }
}