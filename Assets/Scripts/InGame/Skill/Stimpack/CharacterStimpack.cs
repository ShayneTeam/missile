﻿using System;
using Bolt;
using InGame.Raid;
using Lean.Pool;
using UnityEngine;

namespace InGame.Skill
{
    public class CharacterStimpack : CharacterSkill
    {
        protected override string Idx => SkillIdx.Stimpack;

        /*
         * 스팀팩 바 
         */
        [SerializeField] private bool showStimpackBar = true;
        [SerializeField] private StimpackBar stimpackBarPrefab;
        
        /*
         * 바주카
         */
        [SerializeField] private Bazooka bazookaLeft;
        [SerializeField] private Bazooka bazookaRight;
        
        /*
         * 이펙트 
         */
        [HideInInspector, SerializeField] private GameObject bulkUpEffect;

        [SerializeField] private bool showStimpackEffect = true;
        [SerializeField] private ParticleSystem stimpackParticleEffect;

        [SerializeField] private bool forceStraightShot;
        
        private StimpackBar _stimpackBar;

        private float DefaultShootSpeed => Character.FoundControlHub.BazookaController.DefaultShootIntervalSec();
        private float StimpackShootSpeed => SkillStimpackFinder.Other[Character.FoundControlHub.InDate].Skill.GetStimpackShootIntervalSec();


        protected override void OnEveryStart()
        {
            base.OnEveryStart();
            
            // 이벤트 등록
            Character.FoundControlHub.Data.Bazooka.OnEarned += OnEarnedBazooka;
            Character.FoundControlHub.AbilityController.OnChangedAbilities += OnChangedAbilities;
        }

        protected override void OnDisable()
        {
            base.OnDisable();

            // 이벤트 해제
            if (Character == null || Character.FoundControlHub == null) return;
            Character.FoundControlHub.Data.Bazooka.OnEarned -= OnEarnedBazooka;
            Character.FoundControlHub.AbilityController.OnChangedAbilities -= OnChangedAbilities;
            
            // 캐릭터 꺼졌다면, 스팀팩바도 같이 꺼주기 (위에 캐릭터 null 체크 필요) 
            HideStimpackBar();
        }

        protected override void ActivateSkill()
        {
            // 스팀팩 바 보여주기 
            ShowStimpackBar();
            
            if (showStimpackEffect) stimpackParticleEffect.gameObject.SetActive(true);

            // 모든 바주카 발사 속도 빠르게 갱신 
            SetShootIntervalSec(StimpackShootSpeed);
            
            // 바주카 직선샷 설정
            SetCurveShoot(false);
        }

        protected override void DeactivateSkill()
        {
            // 스팀팩 바 꺼주기 
            HideStimpackBar();
            
            stimpackParticleEffect.gameObject.SetActive(false);

            // 바주카 발사 속도 기본 속도로 바꾸기 
            SetShootIntervalSec(DefaultShootSpeed);
            
            // 바주카 직선샷 해제
            SetCurveShoot(true);
        }

        private void ShowStimpackBar()
        {
            if (!showStimpackBar) return;
            
            // 행성 이동시 반납될 수 있음
            // 그래서 다시 켜주는 함수 필요
            _stimpackBar = LeanPool.Spawn(stimpackBarPrefab, InGamePrefabs.Instance.healthBarParent);
            _stimpackBar.Init(Character);
        }

        private void HideStimpackBar()
        {
            LeanPool.Despawn(_stimpackBar);
        }
        
        private void OnEarnedBazooka(string bazookaIdx)
        {
            RefreshShootSpeed();
        }

        private void OnChangedAbilities()
        {
            RefreshShootSpeed();
        }

        private void RefreshShootSpeed()
        {
            SetShootIntervalSec(IsActivating() ? StimpackShootSpeed : DefaultShootSpeed);
        }

        private void SetShootIntervalSec(float shootIntervalSec)
        {
            Character.ChangeShootIntervalSec(shootIntervalSec);
        }

        private void SetCurveShoot(bool isOn)
        {   
            // HACK. 강제 직선샷 
            // 가장 이상적인 구현 구조는 모드를 참조해서 적용하는 것 
            // 근데 지금 구조상 PVE, PVP 모드 구조를 각각 잡아놔서, 이게 어떤 모드인지 아닌지 판별하는 구조는 없음 
            if (forceStraightShot) isOn = false;
            
            bazookaLeft.CurveEnabled = isOn;
            bazookaRight.CurveEnabled = isOn;
        }
    }
}