﻿namespace InGame.Skill
{
    public class SkillStimpack : Skill
    {
        public override bool UseSkill()
        {
            if (!base.UseSkill())
                return false;
            
            // 사운드
            SoundController.Instance.PlayEffect("sfx_skill_use_04");

            return true;
        }
        
        public float GetStimpackShootIntervalSec()
        {
            // 바주카 기본 초당 발사 갯수
            var shootCountPerSec = FoundControlHub.BazookaController.CalcShootCounterPerSec();

            // 스팀팩 효과 (초당 발사 갯수가 % 만큼 증가)
            var shootCountBonusPer = GetShootCountBonusPer();
            shootCountPerSec *= (1f + (shootCountBonusPer / 100f));

            // 초당 발사 간격으로 변환 
            return FoundControlHub.BazookaController.ToShootIntervalSecFrom(shootCountPerSec);
        }

        public float GetShootCountBonusPer()
        {
            return FoundControlHub.SkillController.GetSkillLevelValue(skillIdx);
        }
    }
    
    public static partial class SkillIdx
    {
        public const string Stimpack = "45004";
    }
}