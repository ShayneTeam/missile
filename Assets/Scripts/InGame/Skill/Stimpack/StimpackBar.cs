﻿using System;
using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace InGame.Skill
{
    [RequireComponent(typeof(RectTransform))]
    public class StimpackBar : MonoBehaviour
    {
        [SerializeField] private Image fill;
        
        private Character _character;
        private UICanvas _canvas;
        private RectTransform _rect;

        private void Awake()
        {
            _canvas = UICanvas.GetUICanvas("InGame");
            _rect = GetComponent<RectTransform>();
        }

        public void Init(Character character)
        {
            _character = character;
            
            FollowCharacter();
        }

        private void OnEnable()
        {
            // 다시 켜졌을 때, 최대치로 초기화 
            SetGauge(1f);
            
            // 위치 갱신 
            FollowCharacter();
            
            // 게이지 자동 갱신
            Timing.RunCoroutine(_AutoRefreshRemainGauge().CancelWith(gameObject));
        }

        private void OnDisable()
        {
            _character = null;
        }

        private void Update()
        {
            /*
             * 오브젝트 꺼지면, 코루틴 걸어놨던게 풀림
             * 그러므로 Update에서 갱신 처리
             */
            FollowCharacter();
        }

        private void SetGauge(float amount)
        {
            fill.fillAmount = amount;
        }

        private void FollowCharacter()
        {
            // 카메라가 껐다 다시 켜져도 타게팅 제대로 잡힐 수 있도록 매번 카메라 가져옴
            var mainCamera = Camera.main;
            
            if (_character == null || mainCamera == null)
                return;

            _rect.FromWorldPosition(_character.barPivot.position, mainCamera, _canvas.Canvas);
        }
        
        private IEnumerator<float> _AutoRefreshRemainGauge()
        {
            while (_character == null)
                yield return Timing.WaitForOneFrame;

            var controlHub = _character.FoundControlHub;
            
            // 스킬 스팀팩 인스턴스 직접참조해서 남은시간 알아옴 
            var durationSec = SkillStimpackFinder.Other[controlHub.InDate].Skill.GetDurationTimeSec();
            var remainTimeSec = SkillStimpackFinder.Other[controlHub.InDate].Skill.GetRemainDurationTimeSec();

            // 바가 꺼지거나, 이 스크립트 활성이 꺼지면 알아서 코루틴도 종료되게 함 (린풀반납 신경쓰기)
            while (remainTimeSec > 0f && enabled)
            {
                remainTimeSec -= Time.deltaTime;
                var remainTimeRate = remainTimeSec / durationSec;
                SetGauge(remainTimeRate);

                yield return Timing.WaitForOneFrame;
            }

            SetGauge(0f);
        }

    }
}