﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    [RequireComponent(typeof(SkillStimpack))]
    public class SkillStimpackFinder : CanFindWithInDate<SkillStimpackFinder>
    {
        private SkillStimpack _skill;

        public SkillStimpack Skill
        {
            get
            {
                if (_skill == null)
                    _skill = GetComponent<SkillStimpack>();

                return _skill;
            }
        }
    }
}