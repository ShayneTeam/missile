﻿using System;
using InGame.Controller;
using InGame.Global;
using Server;
using UnityEngine;

namespace InGame.Skill
{
    public class MySkillsZone : MonoBehaviourSingletonPersistent<MySkillsZone>
    {
        private void Start()
        {
            LogoutController.OnLogout += OnLogout;
            Stage.OnStart += OnStartStage;
        }

        private void OnDestroy()
        {
            LogoutController.OnLogout -= OnLogout;
            Stage.OnStart -= OnStartStage;
        }
        
        private void OnLogout()
        {
            // HACK. 하위 자식들 꺼주기 
            // CanFindWithInDate에 등록된거 날려주고, 새롭게 갱신된 인데이트 넣어주기 위함
            gameObject.SetActive(false);
        }

        private void OnStartStage()
        {
            // HACK. 하위 자식들 다시 켜주기
            // NOTICE. 스킬존의 사용은 이 함수를 호출하는 시점 이후에 해야 함 
            // 만약 이보다 더 빠르게 사용되면, 새롭게 로그인한 유저의 인데이트로 Instanced Id 등록이 되지 않아서, NRE가 뜨게 됨  
            gameObject.SetActive(true);
        }
    }
}