﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    [RequireComponent(typeof(SkillBigBeautifulMissile))]
    public class SkillBigBeautifulMissileFinder : CanFindWithInDate<SkillBigBeautifulMissileFinder>
    {
        private SkillBigBeautifulMissile _skill;

        public SkillBigBeautifulMissile Skill
        {
            get
            {
                if (_skill == null)
                    _skill = GetComponent<SkillBigBeautifulMissile>();

                return _skill;
            }
        }
    }
}