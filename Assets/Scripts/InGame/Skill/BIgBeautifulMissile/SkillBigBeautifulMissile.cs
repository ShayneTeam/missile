﻿using InGame.Controller;
using Lean.Pool;
using UnityEngine;

namespace InGame.Skill
{
    public class SkillBigBeautifulMissile : Skill
    {
        [SerializeField] private BigBeautifulMissile missilePrefab;
	
        public override bool UseSkill()
        {
            if (!base.UseSkill())
                return false;

            // 미사일 스폰 (부모 설정 중요)
            var newMissile = LeanPool.Spawn(missilePrefab, BigBeautifulMissileSpawn.Other[FoundControlHub.InDate].StartPos, Quaternion.identity, FoundControlHub.transform);
            
            // 레이어 지정 
            newMissile.gameObject.layer = Army.Other[FoundControlHub.InDate].ProjectileLayer;
            newMissile.Init(skillIdx);
            newMissile.Shot(BigBeautifulMissileSpawn.Other[FoundControlHub.InDate].EndPos);
            
            // 사운드
            SoundController.Instance.PlayEffect("sfx_skill_use_01");

            return true;
        }

        public float GetDamageModifiedPer()
        {
            return FoundControlHub.SkillController.GetSkillLevelValue(skillIdx);
        }
    }
    
    public static partial class SkillIdx
    {
        public const string BigBeautifulMissile = "45001";
    }
}