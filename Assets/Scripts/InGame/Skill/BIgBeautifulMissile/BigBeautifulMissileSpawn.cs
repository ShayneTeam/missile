﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    public class BigBeautifulMissileSpawn : CanFindWithInDate<BigBeautifulMissileSpawn>
    {
        [SerializeField] private Transform start;
        [SerializeField] private Transform end;
        
        public Vector3 StartPos => start.position;
        public Vector3 EndPos => end.position;
    }
}