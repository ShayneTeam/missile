﻿using UnityEngine;

namespace InGame.Skill
{
    [RequireComponent(typeof(ProjectileMoving))]
    public class BigBeautifulMissile : Projectile
    {
        private string _skillIdx;
        
	    
        
        public void Init(string skillIdx)
        {
            _skillIdx = skillIdx;
        }
	
        public override void Shot(Vector3 targetPos)
        {
            ShotWithStraight(targetPos, false);
        }
        
        protected override double GetBaseDamage()
        {
            return FoundControlHub.MissileController.GetBaseDamage();
        }

        protected override bool TryCritical(double damage, out double criticalDamage)
        {
            return FoundControlHub.MissileController.TryCritical(damage, out criticalDamage);
        }

        public override void ProcessAfterDamage()
        {
            // 데미지 준 후, 삭제시키지 않음 
        }
		
        protected override double FinalizeDamageEnemy(Enemy.Enemy target, out DamageType damageType)
        {
            // 미사일 데미지 
            var baseCalcDamage = base.FinalizeDamageEnemy(target, out damageType);
            
            // 스킬 레벨 데미지 
            var skillLevelDamagePer = SkillBigBeautifulMissileFinder.Other[FoundControlHub.InDate].Skill.GetDamageModifiedPer();

            // 최종 스킬 데미지 
            return baseCalcDamage * (skillLevelDamagePer / 100d);
        }
    }
}