﻿using System.Collections;
using System.Collections.Generic;
using System.Linq;
using Global;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using Lean.Pool;
using MEC;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame.Skill
{
    public class SkillBombing : Skill
    {
        [SerializeField] private BombingMissile missilePrefab;

        
	
        [SerializeField] private float missileMovingDistance;

        [SerializeField] private float missileRot;
	
	
        public override bool UseSkill()
        {
	        if (!base.UseSkill())
		        return false;

	        // 미사일 소환 코루틴
	        Timing.RunCoroutine(SpawnMissiles().CancelWith(gameObject));
	        
	        // 사운드
	        SoundController.Instance.PlayEffect("sfx_skill_use_02");

	        return true;
        }
	
        private IEnumerator<float> SpawnMissiles()
        {
	        var bombingSpawn = BombingSpawn.Other[FoundControlHub.InDate];
	        var bounds = bombingSpawn.Bounds;
	        
	        // 랜덤 미사일 픽
	        var equippedMissileList = FoundControlHub.Data.Missiles.GetAllIdxesEquipped();

	        // 스킬 유지 시간
	        var remainDurationSec = GetRemainDurationTimeSec();

	        // 미사일 스폰 간격 
	        var intervalSec = DataboxController.GetDataFloat(Table.Skill, Sheet.Skill, skillIdx, "value_2", 0.2f);
	        
	        while (remainDurationSec > 0)
	        {
		        // 랜덤 스폰 위치 
		        var spawnPos = Vector2.zero;
		        spawnPos.x = Random.Range(bounds.min.x, bounds.max.x);
		        spawnPos.y = Random.Range(bounds.min.y, bounds.max.y);

		        // 미사일 종료 지점 
		        /*
		         * Vector3 회전 = 쿼터니언
		         * Vector2 회전 = 삼각함수
		         *
		         * Vector2 회전을 쿼터니언으로 했다가 안되서 삽질했음 
		         */
		        var radian = missileRot * Mathf.Deg2Rad;
		        var radius = missileMovingDistance;
		        
		        /*
		         * 중점에서 각이 주어졌을 때, 각 변의 비율 계산
		         * 그 비율을 통해 꼭지점(x,y)의 위치 계산 
		         */
		        var corner = Vector2.zero;		
		        corner.x = Mathf.Cos(radian) * radius;
		        corner.y = Mathf.Sin(radian) * radius;

		        var center = spawnPos;
		        var endPos = center + corner; 
		        
		        // 랜덤 미사일 픽
		        var randomPickedMissileIdx = equippedMissileList.ElementAt(Random.Range(0, equippedMissileList.Count()));

		        // 미사일 스폰 (부모 설정 중요)
		        var newMissile = LeanPool.Spawn(missilePrefab, spawnPos, Quaternion.identity, FoundControlHub.transform);
		        
		        // 레이어 지정 
		        newMissile.gameObject.layer = Army.Other[FoundControlHub.InDate].ProjectileLayer;
		        newMissile.Init(skillIdx, randomPickedMissileIdx);
		        newMissile.Shot(endPos);

		        // 인터벌만큼 딜레이 
		        yield return Timing.WaitForSeconds(intervalSec);

		        // 시간 차감 
		        remainDurationSec -= intervalSec;
	        }
        }

        public float GetDamageModifiedPer()
        {
	        return FoundControlHub.SkillController.GetSkillLevelValue(skillIdx);
        }
    }
    
    public static partial class SkillIdx
    {
	    public const string Bombing = "45002";
    }
}