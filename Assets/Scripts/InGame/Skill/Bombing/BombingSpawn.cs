﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    public class BombingSpawn : CanFindWithInDate<BombingSpawn>
    {
        [SerializeField] private SpriteRenderer range;
        
        public Bounds Bounds => range.bounds;
    }
}