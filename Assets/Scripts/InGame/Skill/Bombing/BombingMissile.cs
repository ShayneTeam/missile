﻿using System.Collections;
using Bolt;
using Global;
using InGame.Global;
using Lean.Pool;
using UnityEngine;

namespace InGame.Skill
{
    public class BombingMissile : Projectile
    {
        [SerializeField] private SpriteRenderer skin;
	
        private string _skillIdx;
        

        public override void OnDespawn()
        {
            base.OnDespawn();
            
            // 그냥 반납시키면 밋밋하니 이펙트 보여주기 
            EffectSpawner.SpawnRandomDamageEffect(transform.position);
        }

        public void Init(string skillIdx, string missileIdx)
        {
            _skillIdx = skillIdx;
            
            // 미사일 스킨 교체 
            var missileResource = DataboxController.GetDataString(Table.Missile, Sheet.missile, missileIdx, Column.resource_id);
            skin.sprite = AtlasController.GetSprite(missileResource);
        }
	
        public override void Shot(Vector3 targetPos)
        {
            ShotWithStraight(targetPos, false);
        }
        
        protected override double GetBaseDamage()
        {
            return FoundControlHub.MissileController.GetBaseDamage();
        }

        protected override bool TryCritical(double damage, out double criticalDamage)
        {
            return FoundControlHub.MissileController.TryCritical(damage, out criticalDamage);
        }
        
        protected override double FinalizeDamageEnemy(Enemy.Enemy target, out DamageType damageType)
        {
            // 미사일 데미지 
            var baseCalcDamage = base.FinalizeDamageEnemy(target, out damageType);
            
            // 스킬 레벨 데미지 
            var skillDamageModifiedPer = SkillBombingFinder.Other[FoundControlHub.InDate].Skill.GetDamageModifiedPer();

            // 최종 스킬 데미지 
            return baseCalcDamage * (skillDamageModifiedPer / 100d);
        }

        public override void ProcessAfterDamage()
        {
            base.ProcessAfterDamage();
            
            // 사운드 
            SoundController.Instance.PlayEffect("sfx_ui_missile_boom");
        }
    }
}