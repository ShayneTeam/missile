﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    [RequireComponent(typeof(SkillBombing))]
    public class SkillBombingFinder : CanFindWithInDate<SkillBombingFinder>
    {
        private SkillBombing _skill;

        public SkillBombing Skill
        {
            get
            {
                if (_skill == null)
                    _skill = GetComponent<SkillBombing>();

                return _skill;
            }
        }
    }
}