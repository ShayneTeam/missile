﻿using System.Collections.Generic;
using UnityEngine;

namespace InGame.Skill
{
    public class CharacterDoubleBazooka : CharacterSkill
    {
        protected override string Idx => SkillIdx.DoubleBazooka;

        /*
         * 바주카 
         */
        [SerializeField] private AutoBazooka autoBazookaLeft;
        [SerializeField] private AutoBazooka autoBazookaRight;

        private readonly List<AutoBazooka> _bazookas = new List<AutoBazooka>();
        private List<AutoBazooka> CurrentBazookas
        {
            get
            {
                _bazookas.Clear();
                
                // 항상 왼쪽 바주카는 디폴트
                _bazookas.Add(autoBazookaLeft);
                
                // 스킬 활성 중일 때만, 오른쪽 바주카 추가 
                if (IsActivating())
                    _bazookas.Add(autoBazookaRight);

                return _bazookas;
            }
        }

        protected override void OnEveryStart()
        {
            base.OnEveryStart();

            Character.OnStartAutoShoot += OnStartAutoShoot;
            Character.OnStopAutoShoot += OnStopAutoShoot;
            Character.OnChangeShootIntervalSec += OnChangeShootIntervalSec;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            Character.OnStartAutoShoot -= OnStartAutoShoot;
            Character.OnStopAutoShoot -= OnStopAutoShoot;
            Character.OnChangeShootIntervalSec -= OnChangeShootIntervalSec;
        }

        protected override void ActivateSkill()
        {
            autoBazookaRight.gameObject.SetActive(true);
            autoBazookaRight.StartAutoShoot();
        }

        protected override void DeactivateSkill()
        {
            autoBazookaRight.gameObject.SetActive(false);
            autoBazookaRight.StopAutoShoot();
        }

        private void OnStartAutoShoot()
        {
            foreach (var autoBazooka in CurrentBazookas) autoBazooka.StartAutoShoot();
        }

        private void OnStopAutoShoot()
        {
            foreach (var autoBazooka in CurrentBazookas) autoBazooka.StopAutoShoot();
        }

        private void OnChangeShootIntervalSec(float shootIntervalSec)
        {
            // 발사속도가 변경된 경우, 더블바주카가 꺼져있어도 속도 세팅은 해둠 
            // 언제든 켜졌을 떄, 바뀐 속도로 쏠 수 있게 
            autoBazookaLeft.Bazooka.SetShootIntervalSec(shootIntervalSec);
            autoBazookaRight.Bazooka.SetShootIntervalSec(shootIntervalSec);
        }

        protected override float GetRemainDurationTime()
        {
            // 더블 바주카는 스킬 지속시간이 효과 
            return SkillDoubleBazookaFinder.Other[Character.FoundControlHub.InDate].Skill.GetRemainDurationTimeSec();
        }

        protected override bool IsActivating()
        {
            // 더블 바주카는 스킬 컨트롤러에게 이 체크를 위임하면 안 됨 
            // 스킬 슬롯과 이 클래스에서 예외적으로 지속시간 체크해서 넘겨줘야 함 
            return GetRemainDurationTime() > 0;
        }
    }
}