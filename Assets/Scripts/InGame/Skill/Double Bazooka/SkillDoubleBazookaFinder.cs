﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    [RequireComponent(typeof(SkillDoubleBazooka))]
    public class SkillDoubleBazookaFinder : CanFindWithInDate<SkillDoubleBazookaFinder>
    {
        private SkillDoubleBazooka _skill;

        public SkillDoubleBazooka Skill
        {
            get
            {
                if (_skill == null)
                    _skill = GetComponent<SkillDoubleBazooka>();

                return _skill;
            }
        }
    }
}