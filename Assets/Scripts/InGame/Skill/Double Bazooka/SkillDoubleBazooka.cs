﻿namespace InGame.Skill
{
    public class SkillDoubleBazooka : Skill
    {
        public override bool UseSkill()
        {
            if (!base.UseSkill())
                return false;
            
            // 사운드
            SoundController.Instance.PlayEffect("sfx_skill_use_03");

            return true;
        }

        public override bool IsDurationTime()
        {
            return GetRemainDurationTimeSec() > 0;
        }

        public override float GetRemainDurationTimeSec()
        {
            // 한 번도 사용하지 않았다면 0 반환
            // 이 처리 안할 시, 계속 0 + 보너스 시간이 되어, 시작하자마자 활성화 처리 됨 
            if (FoundControlHub.SkillController.GetSkillTimestamp(skillIdx) == 0)
                return 0;
            
            // 더블 바주카는 스킬 지속시간이 효과 
            var baseDurationTimeSec = FoundControlHub.SkillController.GetRemainDurationTimeSec(skillIdx);

            var skillDurationBonusSec = GetDurationTimeBonusSec();

            return baseDurationTimeSec + skillDurationBonusSec;
        }
        
        public float GetDurationTimeBonusSec()
        {
            return FoundControlHub.SkillController.GetSkillLevelValue(skillIdx);
        }
    }
    
    public static partial class SkillIdx
    {
        public const string DoubleBazooka = "45003";
    }
}