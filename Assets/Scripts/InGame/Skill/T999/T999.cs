﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using InGame.Controller.Control;
using Lean.Pool;
using MEC;
using UnityEngine;

namespace InGame.Skill
{
    public class T999 : FindControlHubBehaviour, ILanding
    {
        [SerializeField] private Landing landing;
        
        [SerializeField] private SpriteRenderer skin;
        [SerializeField] private Sprite[] sprites;

        [SerializeField] private GameObject shockAttackPrefab;
        [SerializeField] private Transform shockAttackSpawnPos;
			
        [SerializeField] private float shockSpawnIntervalSec = 0.1f;
        [SerializeField] private float shockSpawnIntervalDistance = 1f;
        [SerializeField] private int shockSpawnCount = 4;

        public _2dxFX_BurnFX _burnFX;
        
        public void ReadyLanding()
        {
            if (_burnFX != null)
                _burnFX.Destroyed = 0f;
            landing.Ready();
        }

        public void Landing(float duration)
        {
            landing.StartLanding(duration, () =>
            {
                // Camera.main.transform.DOPunchPosition(new Vector3(0, -3f, 0f), 0.2f);
            });
        }

        public void Shoot()
        {
            var intervalSec = SkillT999Finder.Other[FoundControlHub.InDate].Skill.GetFinalizedShootIntervalSec();
            Shoot(intervalSec);
        }
        
        public void Shoot(float intervalSec)
        {
            Timing.RunCoroutine(ShootFlow(intervalSec).CancelWith(gameObject));
        }

        private IEnumerator<float> ShootFlow(float intervalSec)
        {
            // 인터벌만큼 스프라이트 교체 간격 조절 
            var spriteIntervalSec = intervalSec / sprites.Length;

            while (true)
            {
                // 스프라이트 애니 
                for (var i=1; i<sprites.Length; i++)
                {
                    skin.sprite = sprites[i];
                    yield return Timing.WaitForSeconds(spriteIntervalSec);
                }
					
                // 일단 스프라이트 애니 돌고 발사 
                Timing.RunCoroutine(ShockAttackFlow().CancelWith(gameObject));

                yield return Timing.WaitForOneFrame;
            }
        }
        
        private IEnumerator<float> ShockAttackFlow()
        {
            // 사운드
            SoundController.Instance.PlayEffect("sfx_skill_use_06_fx");
            
            // 이펙트 
            var firstSpawnPos = shockAttackSpawnPos;
            var spawnPos = firstSpawnPos.position;
            
            // 플립 여부 체크 
            var isFlipped = transform.lossyScale.x < 0;

            for (var i = 0; i < shockSpawnCount; i++)
            {
                var spawnedAttack = LeanPool.Spawn(shockAttackPrefab, spawnPos, Quaternion.identity, FoundControlHub.transform);
                
                // 레이어 지정 
                spawnedAttack.layer = Army.Other[FoundControlHub.InDate].ProjectileLayer;
                
                if (isFlipped)
                    spawnPos.x -= shockSpawnIntervalDistance;
                else
                    spawnPos.x += shockSpawnIntervalDistance;

                yield return Timing.WaitForSeconds(shockSpawnIntervalSec);
            }
        }
    }
}