﻿using System.Collections;
using Bolt;
using Lean.Pool;
using UnityEngine;

namespace InGame.Skill
{
    public class T999ShockAttack : TriggerEnemy
    {
	
        [SerializeField] private string skillIdx;
        

        

        
        
        protected override double GetBaseDamage()
        {
            return FoundControlHub.MissileController.GetBaseDamage();
        }

        protected override bool TryCritical(double damage, out double criticalDamage)
        {
            return FoundControlHub.MissileController.TryCritical(damage, out criticalDamage);
        }

        protected override double FinalizeDamageEnemy(Enemy.Enemy target, out DamageType damageType)
        {
            // 미사일 데미지 
            var baseCalcDamage = base.FinalizeDamageEnemy(target, out damageType);
            
            // 스킬 데미지 보정율  
            var damageModifiedPer = SkillT999Finder.Other[FoundControlHub.InDate].Skill.GetDamageModifiedPer();

            // 최종 스킬 데미지 
            return baseCalcDamage * (damageModifiedPer / 100d);
        }

        public override void ProcessAfterDamage()
        {
            // 데미지 준 후, 삭제시키지 않음 
            /*
             * T999 쇼크어택은 SpriteEffectOnce에서 애니 다 돌고 자동 반납됨
             */
        }
    }
}