﻿using System.Collections.Generic;
using DG.Tweening;
using Global;
using Global.Extensions;
using InGame.Controller;
using InGame.Global;
using Lean.Pool;
using MEC;
using UnityEngine;

namespace InGame.Skill
{
    public class SkillT999 : Skill
    {
        [SerializeField] private T999 t999Prefab;
			
        [SerializeField] private float landingTime = 0.5f;

        [SerializeField] private float attackStartDelay = 1f;
        private CoroutineHandle _spawn;


        protected override void OnEnable()
        {
            base.OnEnable();

            WormHole.WormHole.OnStart += OnStartWormHole;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            WormHole.WormHole.OnStart -= OnStartWormHole;
        }

        public override bool UseSkill()
        {
            if (!base.UseSkill())
                return false;

            // 시작 
            SpawnT999();
            
            // 사운드
            SoundController.Instance.PlayEffect("sfx_skill_use_06");

            return true;
        }

        private void SpawnT999()
        {
            _spawn = Timing.RunCoroutine(_SpawnT999().CancelWith(gameObject));
        }

        private IEnumerator<float> _SpawnT999()
        {
            // T-999 스폰 
            var t999Spawn = T999Spawn.Other[FoundControlHub.InDate];
            var t999 = LeanPool.Spawn(t999Prefab, t999Spawn.Pos, Quaternion.identity, t999Spawn.transform);
            t999.ReadyLanding();
            t999.Landing(landingTime);

            yield return Timing.WaitForSeconds(landingTime);
            
            yield return Timing.WaitForSeconds(attackStartDelay);

            // 공격 간격 
            var finalizedIntervalSec = GetFinalizedShootIntervalSec();

            t999.Shoot(finalizedIntervalSec); 
            
            // 스킬 유지 시간
            var remainDurationSec = GetRemainDurationTimeSec();
            while (remainDurationSec > 0f)
            {
                yield return Timing.WaitForOneFrame;

                remainDurationSec -= Time.deltaTime;
                
                // T999가 스킬 도중에 디스폰된 경우, 재시작 각 잡음 
                if (!t999.gameObject.activeInHierarchy && FoundControlHub.SkillController.CanUseSkill())
                {
                    SpawnT999();
                    yield break;
                }
            }

            // 스킬 끝나면 버닝 이펙트
            DOVirtual.Float(0, 1, 2, value =>
            {
                t999._burnFX.Destroyed = value;
            }).OnComplete(() =>
            {
                // 끝나면 T999 반납 
                LeanPool.Despawn(t999);
            });
        }

        public float GetFinalizedShootIntervalSec()
        {
            var intervalSec = DataboxController.GetDataFloat(Table.Skill, Sheet.Skill, skillIdx, "value_2", 0.2f);

            // 어빌리티
            var abT999AtkSpdModifiedPer = FoundControlHub.AbilityController.GetUnifiedValue("SKILL_DIABLO_SPEED");
            var finalizedIntervalSec = intervalSec / (1f + (abT999AtkSpdModifiedPer / 100f));
            return finalizedIntervalSec;
        }

        public float GetDamageModifiedPer()
        {
            /*
             * T-999는 특별스킬이라 예외적으로 처리
             * 다른 녀석들은 레벨을 어빌리티로 계산하지만, T-999만 디아블로 유물로 계산함  
             */
            
            // 스킬 기본 밸류
            var skillBaseValue = DataboxController.GetDataInt(Table.Skill, Sheet.Skill, skillIdx, "value_1", 1);

            // 스킬 레벨 가중치 
            var skillLevelWeight = SkillControl.GetLevelWeight(skillIdx, GetSkillLevel());

            // 스킬 레벨 밸류 
            return skillBaseValue * skillLevelWeight;
        }
        
        public int GetSkillLevel()
        {
            // 테스트. 스킬 레벨 
            if (ExtensionMethods.IsUnityEditor())
            {
                var testSkillLevel = DataboxController.GetDataInt(Table.Cheat, Sheet.Cheat, Row.Test, Column.all_skills_level);
                if (testSkillLevel > 0)
                    return testSkillLevel;
            }
            
            return FoundControlHub.DiabloRelicController.GetAverageLevelOfAllEarned();
        }

        public override float GetFinalizedCoolTimeSec()
        {
            var baseCoolTimeSec = GetBaseCoolTimeSec();

            var abT999CoolTimeDownSec = FoundControlHub.AbilityController.GetUnifiedValue("SKILL_DIABLO_COOLTIME_DN");

            return baseCoolTimeSec - abT999CoolTimeDownSec;
        }

        private void OnStartWormHole()
        {
            // 웜홀 특별 예외 처리 
            // 웜홀은 웜홀 씬을 유지하면서 재도전이나 다음 라운드 진행 시, 스킬을 리셋해야 함 
            // 타임스탬프 처리는 공통적으로 할 수 있는데, 소환되는 T999나 융단 폭격은 예외적으로 처리해줘야만 함
            Timing.KillCoroutines(_spawn);
        }
    }
    
    public static partial class SkillIdx
    {
        public const string T999 = "45006";
    }
}