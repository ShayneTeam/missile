﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    public class T999Spawn : CanFindWithInDate<T999Spawn>
    {
        public Vector3 Pos => transform.position;
    }
}