﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    [RequireComponent(typeof(SkillT999))]
    public class SkillT999Finder : CanFindWithInDate<SkillT999Finder>
    {
        private SkillT999 _skill;

        public SkillT999 Skill
        {
            get
            {
                if (_skill == null)
                    _skill = GetComponent<SkillT999>();

                return _skill;
            }
        }
    }
}