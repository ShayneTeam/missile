﻿using System.Collections.Generic;
using MEC;
using UnityEngine;
using Utils;

namespace InGame.Skill
{
    [RequireComponent(typeof(Character))]
    public abstract class CharacterSkill : EveryStartBehaviour
    {
        private Character _character;
        private CoroutineHandle _autoDeactivate;

        protected Character Character
        {
            get
            {
                if (_character == null)
                    _character = GetComponent<Character>();
                
                return _character;
            }
        }

        protected abstract string Idx { get; }
        
        
        protected override void OnEveryStart()
        {
            if (IsActivating())
            {
                DoSkillProcess();
            }
            else
            {
                DeactivateSkill();
            }
            
            // 이벤트 등록 
            Character.FoundControlHub.SkillController.OnUseSkill += OnUseSkill;
        }

        protected override void OnDisable()
        {
            base.OnDisable();
            
            // 이벤트 해제
            Character.FoundControlHub.SkillController.OnUseSkill -= OnUseSkill;
        }
        
        protected virtual bool IsActivating()
        {
            return Character.FoundControlHub.SkillController.IsActivating(Idx);
        }

        protected virtual float GetRemainDurationTime()
        {
            return Character.FoundControlHub.SkillController.GetRemainDurationTimeSec(Idx);
        }

        protected abstract void ActivateSkill();
        protected abstract void DeactivateSkill();

        private void OnUseSkill(string skillIdx)
        {
            if (skillIdx == Idx)
                DoSkillProcess();
        }

        private void DoSkillProcess()
        {
            // 활성
            ActivateSkill();

            // 자동 비활성
            Timing.KillCoroutines(_autoDeactivate);
            _autoDeactivate = Timing.RunCoroutine(_AutoDeactivate().CancelWith(gameObject));
        }

        private IEnumerator<float> _AutoDeactivate()
        {
            // 지속시간 대기
            var remainDurationTime = GetRemainDurationTime();
            yield return Timing.WaitForSeconds(remainDurationTime);
            
            // 비활성
            DeactivateSkill();
        }
    }
}