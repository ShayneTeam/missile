﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bolt;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Controller.Control;
using InGame.Controller.Mode;
using InGame.Data;
using InGame.Global;
using JetBrains.Annotations;
using MEC;
using Server;
using TMPro;
using UI.Popup;
using UnityEngine;
using UnityEngine.UI;

namespace InGame.Skill
{
	// abstract 키워드 붙이는게 맞음
	// 볼트에서 abstract 붙이면 Skill.Function() 이런 식으로 호출 못해서 키워드 뺌 
	public class Skill : FindControlHubBehaviour
    {
	    [SerializeField] protected string skillIdx;
        
        [SerializeField] TextMeshProUGUI durationTime;
        [SerializeField] TextMeshProUGUI coolTime;

        [SerializeField] private Image enabledIcon;

        private CoroutineHandle _showDuration;
        private CoroutineHandle _showCoolTimeFill;
        private CoroutineHandle _showCoolTimeText;


        protected override void OnEnable()
        {
	        base.OnEnable();
	        FoundControlHub.Data.Missiles.OnEarned += OnEarnedMissile;
	        FoundControlHub.DiabloRelicController.OnGacha += OnGacha;
        }

        protected override void OnDisable()
        {
	        base.OnDisable();
	        
	        if (FoundControlHub == null) return;
	        FoundControlHub.Data.Missiles.OnEarned -= OnEarnedMissile;
	        FoundControlHub.DiabloRelicController.OnGacha -= OnGacha;
        }

        private void OnEarnedMissile(string idx)
        {
	        // 잠금 상태 해제를 위한 이벤트 트리거
	        CustomEvent.Trigger(gameObject, "OnEarnedMissile");
        }
        
        private void OnGacha(int count)
        {
	        // 잠금 상태 해제를 위한 이벤트 트리거
	        CustomEvent.Trigger(gameObject, "OnEarnedDiabloRelic");
        }

        [PublicAPI]
        public bool CheckIfCanUnlock()
        {
	        return FoundControlHub.SkillController.IsUnlocked(skillIdx);
        }
        
        [PublicAPI]
        public void ShowLockMessage()
        {
	        ShowLockMessage(skillIdx);
        }

        public static void ShowLockMessage(string skillIdx)
        {
	        var lockMessageKey = DataboxController.GetDataString(Table.Skill, Sheet.Skill, skillIdx, "lock_massage");

	        MessagePopup.Show(lockMessageKey);
        }

        [PublicAPI]
        public virtual bool UseSkill()
        {
	        return FoundControlHub.SkillController.UseSkill(skillIdx);
        }

        /*
         * Duration
         */
        #region Duration
        
        [PublicAPI]
        public virtual bool IsDurationTime()
        {
	        return FoundControlHub.SkillController.IsActivating(skillIdx);
        }
        
        public virtual float GetRemainDurationTimeSec()
        {
	        return FoundControlHub.SkillController.GetRemainDurationTimeSec(skillIdx);
        }

        [PublicAPI]
        public void ShowDurationTime()
        {
	        Timing.KillCoroutines(_showDuration);
	        _showDuration = Timing.RunCoroutine(_ShowDurationTime().CancelWith(gameObject));
        }
        
        [PublicAPI]
        public void HideDurationTime()
        {
	        Timing.KillCoroutines(_showDuration);
        }
        
        private IEnumerator<float> _ShowDurationTime()
        {
	        var remainDurationTimeSec = GetRemainDurationTimeSec();
	        while (remainDurationTimeSec > 0)
	        {
		        remainDurationTimeSec -= Time.deltaTime;

		        var secStr = remainDurationTimeSec.ToString("0.00");
		        durationTime.text = $"{secStr}s";

		        // 프레임 반복 
		        yield return Timing.WaitForOneFrame;
	        }

	        durationTime.text = "Waiting";
        }

        public float GetDurationTimeSec()
        {
	        return DataboxController.GetDataInt(Table.Skill, Sheet.Skill, skillIdx, Column.DurationTime);
        }

        #endregion

        
        /*
         * CoolTime
         */
        #region CoolTime

        [PublicAPI]
        public bool IsCoolTime()
        {
	        return FoundControlHub.SkillController.IsCoolTime(skillIdx, GetFinalizedCoolTimeSec());
        }
        
        [PublicAPI]
        public float GetRemainCoolTimeSec()
        {
	        return FoundControlHub.SkillController.GetRemainCoolTimeSec(skillIdx, GetFinalizedCoolTimeSec());
        }

        [PublicAPI]
        public void ShowCoolTime()
        {
	        Timing.KillCoroutines(_showCoolTimeFill);
	        _showCoolTimeFill = Timing.RunCoroutine(_ShowCoolTimeFilled().CancelWith(gameObject));
	        
	        Timing.KillCoroutines(_showCoolTimeText);
	        _showCoolTimeText = Timing.RunCoroutine(_ShowCoolTimeText().CancelWith(gameObject));
        }
        
        [PublicAPI]
        public void HideCoolTime()
        {
	        Timing.KillCoroutines(_showCoolTimeFill);
	        Timing.KillCoroutines(_showCoolTimeText);
	        enabledIcon.fillAmount = 1f;
        }
        
        private IEnumerator<float> _ShowCoolTimeFilled()
        {
	        var coolTimeSec = GetFinalizedCoolTimeSec();
	        var remainCoolTimeSec = GetRemainCoolTimeSec();
	        while (remainCoolTimeSec > 0)
	        {
		        remainCoolTimeSec -= Time.deltaTime;
		        
		        // 이미지 차오르는 연출 
		        enabledIcon.fillAmount = 1f - (remainCoolTimeSec / coolTimeSec);

		        // 프레임 반복 
		        yield return Timing.WaitForOneFrame;
	        }
	        
	        enabledIcon.fillAmount = 1f;
        }
        
        private IEnumerator<float> _ShowCoolTimeText()
        {
	        var remainCoolTimeSec = GetRemainCoolTimeSec();
	        while (remainCoolTimeSec > 0)
	        {
		        // 쿨타임 텍스트
		        var mm = (int)(remainCoolTimeSec / 60f);
		        var ss = (int)(remainCoolTimeSec % 60f);

		        var mmStr = mm.ToLookUpString();
		        var ssStr = ss.ToLookUpFormatString("00");
		        coolTime.text = $"{mmStr} : {ssStr}";
		        
		        // 3배속을 해도 1초씩 차감되는 것처럼 보이려면, 1초 주기로 돌리면 안됨
		        yield return Timing.WaitForSeconds(0.5f);
		        remainCoolTimeSec = GetRemainCoolTimeSec();
	        }
	        
	        coolTime.text = "Waiting";
        }
        
        public float GetBaseCoolTimeSec()
        {
	        return FoundControlHub.SkillController.GetSkillCoolTimeSec(skillIdx);
        }

        public virtual float GetFinalizedCoolTimeSec()
        {
	        return GetBaseCoolTimeSec();
        }

        #endregion
    }
}