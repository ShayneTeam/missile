﻿using Bolt;
using Ludiq;
using UnityEngine;

namespace InGame.Skill
{
    public class CharacterBulkUp : CharacterSkill
    {
        protected override string Idx => SkillIdx.BulkUp;

        /*
         * 이펙트 
         */
        [SerializeField] private GameObject bulkUpEffect;

        /*
         * 바주카 
         */
        [SerializeField] private Bazooka bazookaLeft;
        [SerializeField] private Bazooka bazookaRight;

        [SerializeField] private Material bazookaDefaultMaterial;
        [SerializeField] private Material bazookaBulkUpMaterial;

        protected override void ActivateSkill()
        {
            bulkUpEffect.gameObject.SetActive(true);
            
            bazookaLeft.ChangeBazookaMaterial(bazookaBulkUpMaterial);
            bazookaRight.ChangeBazookaMaterial(bazookaBulkUpMaterial);
        }

        protected override void DeactivateSkill()
        {
            bulkUpEffect.gameObject.SetActive(false);
            
            bazookaLeft.ChangeBazookaMaterial(bazookaDefaultMaterial);
            bazookaRight.ChangeBazookaMaterial(bazookaDefaultMaterial);
        }
    }
}