﻿using Global;
using InGame.Global;

namespace InGame.Skill
{
    public class SkillBulkUp : Skill
    {
        public override bool UseSkill()
        {
            if (!base.UseSkill())
                return false;
            
            // 사운드
            SoundController.Instance.PlayEffect("sfx_skill_use_05");

            return true;
        }

        /*
         * 처음엔 OOP 디자인을 위해 바주카 연산 함수를 스위칭하는 식으로 접근했음
         * 허나 나중에 연산 공식의 순서가 바뀌거나 다른 뭔가가 추가되면, 되려 수정하기 어려운 구조가 된다고 판단
         * 바주카에서 어빌리티를 알고 있는 것처럼, 벌크업도 알고 있게 구조 잡음
         * 커플링은 심해지지만, 연산 자체가 바뀔 때 수정하기 편해짐 
         */
        public static float GetCriticalChanceBonus()
        {
            return DataboxController.GetDataFloat(Table.Skill, Sheet.Skill, SkillIdx.BulkUp, "value_2", 25f);
        }
        
        public float GetCriticalDamageBonus()
        {
            return FoundControlHub.SkillController.GetSkillLevelValue(skillIdx);
        }
    }
    
    public static partial class SkillIdx
    {
        public const string BulkUp = "45005";
    }
}