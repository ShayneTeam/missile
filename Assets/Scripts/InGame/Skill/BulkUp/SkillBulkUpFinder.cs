﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    [RequireComponent(typeof(SkillBulkUp))]
    public class SkillBulkUpFinder : CanFindWithInDate<SkillBulkUpFinder>
    {
        private SkillBulkUp _skill;

        public SkillBulkUp Skill
        {
            get
            {
                if (_skill == null)
                    _skill = GetComponent<SkillBulkUp>();

                return _skill;
            }
        }
    }
}