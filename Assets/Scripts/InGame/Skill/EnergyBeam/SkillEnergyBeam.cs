﻿using Global;
using Global.Extensions;
using InGame.Controller;
using InGame.Global;
using Lean.Pool;
using UnityEngine;

namespace InGame.Skill
{
    public class SkillEnergyBeam : Skill
    {
        public override bool UseSkill()
        {
            if (!base.UseSkill()) return false;
            
            // 심플하게 여기서 공주 찾은 후, 스킬 사용 
            var princess = Army.My.Princess;
            if (princess == null) return false;
            princess.ActivateSkill();

            return true;
        }

        public float DamagePer
        {
            get
            {
                // 스킬 기본 밸류
                var skillBaseValue = DataboxController.GetDataInt(Table.Skill, Sheet.Skill, skillIdx, "value_1", 1);

                // 스킬 레벨 가중치 
                var skillLevelWeight = SkillControl.GetLevelWeight(skillIdx, GetSkillLevel());

                // 스킬 레벨 밸류 
                return skillBaseValue * skillLevelWeight;
            }
        }

        private int GetSkillLevel()
        {
            // 테스트. 스킬 레벨 
            if (ExtensionMethods.IsUnityEditor())
            {
                var testSkillLevel = DataboxController.GetDataInt(Table.Cheat, Sheet.Cheat, Row.Test, Column.all_skills_level);
                if (testSkillLevel > 0) return testSkillLevel;
            }
            
            return FoundControlHub.PrincessController.Level.AverageOfAllCollected;
        }
    }
    
    public static partial class SkillIdx
    {
        public const string EnergyBeam = "45007";
    }
}