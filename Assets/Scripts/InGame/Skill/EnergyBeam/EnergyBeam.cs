﻿using System.Collections;
using Bolt;
using Lean.Pool;
using UnityEngine;

namespace InGame.Skill
{
    public class EnergyBeam : TriggerEnemy
    {
        protected override double GetBaseDamage()
        {
            return FoundControlHub.MissileController.GetBaseDamage();
        }

        protected override bool TryCritical(double damage, out double criticalDamage)
        {
            return FoundControlHub.MissileController.TryCritical(damage, out criticalDamage);
        }

        protected override double FinalizeDamageEnemy(Enemy.Enemy target, out DamageType damageType)
        {
            // 미사일 데미지
            var baseCalcDamage = base.FinalizeDamageEnemy(target, out damageType);
            
            // 스킬 데미지 보정율
            var damagePer = SkillEnergyBeamFinder.My.Skill.DamagePer;

            // 최종 스킬 데미지
            return baseCalcDamage * (damagePer / 100d);
        }

        public override void ProcessAfterDamage()
        {
            // 데미지 준 후, 삭제시키지 않음 
        }
    }
}