﻿using InGame.Controller.Control;
using UnityEngine;

namespace InGame.Skill
{
    [RequireComponent(typeof(SkillEnergyBeam))]
    public class SkillEnergyBeamFinder : CanFindWithInDate<SkillEnergyBeamFinder>
    {
        private SkillEnergyBeam _skill;

        public SkillEnergyBeam Skill
        {
            get
            {
                _skill ??= GetComponent<SkillEnergyBeam>();
                return _skill;
            }
        }
    }
}