﻿using Global;
using JetBrains.Annotations;

namespace InGame.Global.Buff
{
	public class BuffDamage : BuffSingletonPersistent<BuffDamage>
	{
		public override string Idx => $"8100{Level.ToLookUpString()}";

		[PublicAPI]
		public float Damage { get; set; } = 1f;

		public override event IntDelegate OnChangedLevel;
		private int _level = 1;
		public override int Level
		{
			get => _level;
			set
			{
				// 중요 (동일하면 이벤트 호출하지 않게 하기 위함) 
				if (_level == value)
					return;
					
				_level = value;
				
				// 데이터에서 버프 값 가져옴 
				Damage = DataboxController.GetDataFloat(Table.Buff, Sheet.buff, Idx, Column.value);
				
				// 이벤트
				OnChangedLevel?.Invoke(value);
			}
		}
	}
}