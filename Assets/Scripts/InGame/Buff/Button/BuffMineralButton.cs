﻿using UnityEngine;

namespace InGame.Global.Buff
{
    public class BuffMineralButton : BuffButton
    {
        protected override Buff Buff => BuffMineral.Instance;
    }
}