﻿using JetBrains.Annotations;
using UnityEngine;

namespace InGame.Global.Buff
{
    public class BuffSpeedButton : BuffButton
    {
        protected override Buff Buff => BuffSpeed.Instance;
        
        [PublicAPI]
        public bool CheckIfCan3X()
        {
            return BuffSpeed.Instance.CheckIfCan3X();
        }
    }
}