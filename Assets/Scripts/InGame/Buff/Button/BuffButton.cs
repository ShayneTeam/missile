﻿using System;
using System.Collections;
using System.Collections.Generic;
using Bolt;
using Doozy.Engine.UI;
using Firebase.Analytics;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup;
using UnityEngine;

namespace InGame.Global.Buff
{
    public abstract class BuffButton : MonoBehaviour
    {
        [SerializeField] protected TextMeshProUGUI levelText;
        [SerializeField] protected TextMeshProUGUI timeText;

        private string Idx => Buff.Idx;
        private float RemainTimeSec => Buff.RemainTimeSec;

        protected abstract Buff Buff { get; }

        private CoroutineHandle _showRemainTime;

        protected virtual void Start()
        {
            Buff.OnChangedLevel += OnChangedLevel;
            // 로그아웃 처리용
            Stage.OnStart += Refresh;
            
            // 레벨 텍스트
            Refresh();
        }

        private void OnDestroy()
        {
            Buff.OnChangedLevel -= OnChangedLevel;
            Stage.OnStart -= Refresh;
        }

        private void OnChangedLevel(int level)
        {
            CustomEvent.Trigger(gameObject, "OnChangedLevel", level);
        }

        [PublicAPI]
        public int Level
        {
            get => Buff.Level;
            set => Buff.Level = value;
        }

        [PublicAPI]
        public void Activate()
        {
            Buff.Activate();
        }

        [PublicAPI]
        public void Deactivate()
        {
            Buff.Deactivate();
        }

        [PublicAPI]
        public bool IsEnoughDuration()
        {
            return Buff.IsEnoughDuration();
        }

        [PublicAPI]
        public void OpenAdsPopup() 
        {
            // 볼트서 야매로 테이블 버프 인덱스 보냄 
			
            // 팝업에게 보낼 데이터 준비 
            var buffTime = GetBuffTimeOfCurrentLevel();

            GetExp(out var prevLevelExp, out var currentExp, out var nextLevelExp);

            // 팝업 초기화 & 보여주기 
            var buffAdsPopup = BuffAdsPopup.Show();
            buffAdsPopup.Init(Idx, buffTime, currentExp, prevLevelExp, nextLevelExp, () =>
            {
                AdsComplete();
                
                // 갱신 
                RefreshLevelText();
            });
        }

        [PublicAPI]
        public bool CheckIfCanBeInfinityTime()
        {
            return Buff.CheckIfCanBeInfinityTime();
        }

        private void RefreshRemainTimeText()
        {
            Timing.KillCoroutines(_showRemainTime);
            _showRemainTime = Timing.RunCoroutine(_ShowRemainTime().CancelWith(gameObject), Segment.RealtimeUpdate);
        }

        private IEnumerator<float> _ShowRemainTime()
        {
            if (CheckIfCanBeInfinityTime())
            {
                // 무제한 상품 구매한 상태라면, 시간을 무제한 표기 
                timeText.text = Localization.GetText("info_069", "-");
            }
            else
            {
                while (true)
                {
                    if (RemainTimeSec <= 0f)
                    {
                        // 남은 시간 0초 이하면 아예 안 보여줌 
                        timeText.text = "";
                    }
                    else
                    {
                        var min = (int)RemainTimeSec / 60;
                        var sec = RemainTimeSec % 60;

                        var minStr = min.ToLookUpFormatString("00");
                        var secStr = sec.ToString("00");
                        timeText.text = $"{minStr}:{secStr}";
                    }

                    // 프레임 반복 
                    yield return Timing.WaitForSeconds(1f);
                }
            }
        }

        private void Refresh()
        {
            RefreshLevelText();
            
            // 로그아웃하고, 다른 계정이 접속됐을 때, 갱신시키기 위함  
            RefreshRemainTimeText();
        }

        private void RefreshLevelText()
        {
            GetLevel(out var currentLevel, out _);
            levelText.text = Localization.GetFormatText("info_231", currentLevel.ToLookUpString());
        }

        private void GetLevel(out int currentLevel, out int nextLevel)
        {
            Buff.GetLevel(out currentLevel, out nextLevel);
        }

        private void GetExp(out int prevLevelExp, out int currentExp, out int nextLevelExp)
        {
            Buff.GetExp(out prevLevelExp, out currentExp, out nextLevelExp);
        }

        private int GetBuffTimeOfCurrentLevel()
        {
            return Buff.GetBuffTimeOfCurrentLevel();
        }

        public void AdsComplete()
        {
            // 광고 봤으면 시간 충전 및 경험치 증가			
            ChargeTimeAndAddExp(); 
            
            // 버프 광고시청 로그
            WebLog.Instance.AllLog("ads_buff", new Dictionary<string, object>{{"idx",Idx}});
            
            // 파이어베이스 트래킹용
            var buffString = $"ads_buff_count_{Idx}";
            var adsBuffCount = PlayerPrefs.GetInt(buffString, 0);
            if (adsBuffCount <= 10)
            {
                WebLog.Instance.ThirdPartyLog($"{buffString}_{adsBuffCount.ToLookUpString()}");
            }
			
            // 볼트에게 상태 전환 이벤트 보냄 
            CustomEvent.Trigger(gameObject, "Watched Ads");
        }

        private void ChargeTimeAndAddExp()
        {
            Buff.ChargeTimeAndAddExp();
        }
    }
}