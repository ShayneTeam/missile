﻿using UnityEngine;

namespace InGame.Global.Buff
{
    public class BuffDamageButton : BuffButton
    {
        protected override Buff Buff => BuffDamage.Instance;
    }
}