﻿using System.Collections;
using System.Collections.Generic;
using Bolt;
using CodeStage.AntiCheat.ObscuredTypes;
using Doozy.Engine.UI;
using Global;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using MEC;
using TMPro;
using UI.Popup;
using UnityEngine;

namespace InGame.Global.Buff
{
	public abstract class BuffSingletonPersistent<T> : Buff where T : Component
	{
		public static T Instance { get; private set; }
	
		public virtual void Awake ()
		{
			if (Instance == null) {
				Instance = this as T;
				DontDestroyOnLoad(gameObject);
			} else {
				Destroy (gameObject);
			}
		}
	}

    public abstract class Buff : MonoBehaviour
    {
	    [SerializeField] protected string buffType;

	    public abstract event IntDelegate OnChangedLevel;
	    public abstract int Level { get; set; }

	    public abstract string Idx { get; }


	    public ObscuredFloat RemainTimeSec
		{
			get => UserData.My.Buffs.GetRemainTime(buffType);
			private set => UserData.My.Buffs.SetRemainTime(buffType, value);
		}

	    private CoroutineHandle _activating;


		public virtual bool CheckIfCanBeInfinityTime()
		{
			return UserData.My.UserInfo.GetFlag(UserFlags.CanBeInfinityTimeAllBuff);
		}

		public bool IsEnoughDuration()
		{
			return UserData.My.Buffs.IsEnoughRemainTime(buffType);
		}

		public float GetRemainTime()
		{
			return UserData.My.Buffs.GetRemainTime(buffType);
		}
		
		public void ConsumeTime(float consumeTime)
		{
			// 볼트 코루틴에서 호출됨 
			RemainTimeSec -= consumeTime;
		}

		public void Activate()
		{
			if (_activating.IsRunning) return;
			
			_activating = Timing.RunCoroutine(_ConsumeTime().CancelWith(gameObject), Segment.RealtimeUpdate);
		}

		public void Deactivate()
		{
			Timing.KillCoroutines(_activating);
		}

		private IEnumerator<float> _ConsumeTime()
		{
			while (enabled)
			{
				RemainTimeSec -= 1f;

				yield return Timing.WaitForSeconds(1);
			}
		}

		public void ChargeTimeAndAddExp()
		{
			// 시간 충전시키고, 경험치 증가시키기 
			var chargeTime = GetBuffTimeOfCurrentLevel();

			UserData.My.Buffs.SetRemainTime(buffType, chargeTime);
			UserData.My.Buffs.AddExp(buffType);
			
			// 바로 저장 
			UserData.My.SaveToServer();
		}

		public int GetBuffTimeOfCurrentLevel()
		{
			GetLevel(out var currentLevel, out _);
			return DataboxController.GetDataInt(Table.Buff, Sheet.AdLevel, currentLevel.ToLookUpString(), "buff_time");
		}

		public void GetExp(out int prevLevelExp, out int currentExp, out int nextLevelExp)
		{
			currentExp = UserData.My.Buffs.GetExp(buffType);
			ExpController.FindPrevAndNextExp(currentExp, Table.Buff, Sheet.AdLevel, Column.exp, out prevLevelExp, out nextLevelExp);
		}

		public void GetLevel(out int currentLevel, out int nextLevel)
		{
			var currentExp = UserData.My.Buffs.GetExp(buffType);
			ExpController.GetLevel(currentExp, Table.Buff, Sheet.AdLevel, Column.exp, out currentLevel, out nextLevel);
		}

		public void Reset()
		{
			Deactivate();
			Level = 1;
		}
    }
}