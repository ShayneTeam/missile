﻿using Global;
using InGame.Data;
using JetBrains.Annotations;
using UnityEngine;

namespace InGame.Global.Buff
{
	public class BuffSpeed : BuffSingletonPersistent<BuffSpeed>
	{
		public override string Idx => $"8000{Level.ToLookUpString()}";
		
		[PublicAPI]
		public float GameSpeed { get; set; } = 1f;

		private int _level = 1;
		public override event IntDelegate OnChangedLevel;

		public override int Level
		{
			get => _level;
			set
			{
				// 중요 (동일하면 이벤트 호출하지 않게 하기 위함) 
				if (_level == value)
					return;
				
				_level = value;
				
				// 데이터에서 버프 값 가져옴 
				GameSpeed = DataboxController.GetDataFloat(Table.Buff, Sheet.buff, Idx, Column.value);
				
				// 이벤트
				OnChangedLevel?.Invoke(value);
			}
		}
		
		
		public bool CheckIfCan3X()
		{
			return UserData.My.UserInfo.GetFlag(UserFlags.CanBeSpeedBuff3x);
		}

		private void FixedUpdate()
		{
			// 일시정지가 아닌 이상, 다른 곳에서 간섭못하게 함 
			if (Time.timeScale != 0)
			{
				Time.timeScale = GameSpeed;
			}
		}
	}
}