﻿using System;
using DG.Tweening;
using Lean.Pool;
using UnityEngine;

namespace InGame
{
    public class Spill : MonoBehaviour, IPoolable
    {
        [SerializeField] private float spillingDuration = 0.1f;

        [SerializeField] SpriteRenderer spriteRenderer;

        [SerializeField] private Transform pivot;

        private Tween _spilling;
        private Vector3 _originPivotPos;

        private void Awake()
        {
            _originPivotPos = pivot.localPosition;
        }


        public void PlayLeft(float duration, float delay, float scale)
        {
            PlayLeftCommon(duration, delay, scale);
            
            // 페이드 
            spriteRenderer.DOFade(0f, duration).From(1f).SetDelay(delay);
        }

        public void PlayLeftNoFade(float duration, float delay, float scale)
        {
            PlayLeftCommon(duration, delay, scale);
        }

        public void PlayRight(float duration, float delay, float scale)
        {
            PlayRightCommon(duration, delay, scale);

            // 페이드 
            spriteRenderer.DOFade(0f, duration).From(1f).SetDelay(delay);
        }

        public void PlayRightNoFade(float duration, float delay, float scale)
        {
            PlayRightCommon(duration, delay, scale);
        }

        private void PlayLeftCommon(float duration, float delay, float scale)
        {
            // Pivot의 x 위치 뒤집음 
            var originPivotPosReverseX = _originPivotPos;
            originPivotPosReverseX.x *= -1f;
            pivot.localPosition = originPivotPosReverseX;
            
            // 스필 애니
            Play(-1f, duration, delay, scale);
        }

        private void PlayRightCommon(float duration, float delay, float scale)
        {
            // Pivot의 x 위치 다시 기본으로 잡음 
            pivot.localPosition = _originPivotPos;
            
            // 스필 애니
            Play(1f, duration, delay, scale);
        }

        private void Play(float direction, float duration, float delay, float scale)
        {
            // 스필 처리 
            // 스케일을 루트에 적용하면 맞춰놓은 Pivot 위치가 어긋나면서 스케일됨  
            // Pivot 위치는 유지하면서 스필의 이미지만 줄이면 됨 
            spriteRenderer.transform.localScale = Vector3.one * scale;  // y 스케일 선세팅
            spriteRenderer.transform.DOScaleX(direction * scale, spillingDuration)
                .From(0f);

            // 자동 반납 
            LeanPool.Despawn(gameObject, duration + delay);
        }

        public void OnSpawn()
        {
        }

        public void OnDespawn()
        {
        }
    }
}