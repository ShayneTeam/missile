﻿using InGame;
using Lean.Pool;
using UnityEngine;

public class DeadEffectDirector : MonoBehaviourSingletonPersistent<DeadEffectDirector>
{
    [Header("그라운드 전용")]
    [SerializeField] private Spill[] enemySpillPrefabs;
    [SerializeField] private Spill[] allySpillPrefabs;

    [SerializeField] private Prop[] enemyPropPrefabs;
    [SerializeField] private Prop[] allyPropPrefabs;

    [SerializeField] private int propCount = 4;

    [SerializeField] private float randomYOffsetRange = 2f;

    [SerializeField] public float delay = 1f;
    [SerializeField] public float duration = 1f;

    private const float InfinityDuration = 999f;

    [Header("에어 전용")]
    [SerializeField] private GameObject explosionPrefab;


    public void ShowEnemyDead(Vector3 pos, float scale = 1f)
    {
        ShowEnemyDeadInternal(pos, scale, duration);
    }
    
    public void ShowEnemyDeadNoFade(Vector3 pos, float scale = 1f)
    {
        ShowEnemyDeadInternal(pos, scale, InfinityDuration);
    }

    private void ShowEnemyDeadInternal(Vector3 pos, float scale, float durationInternal)
    {
        // 스필  
        var spillPrefab = enemySpillPrefabs[Random.Range(0, enemySpillPrefabs.Length - 1)];
        var spawnSpill = SpawnSpill(spillPrefab, pos);
        spawnSpill.PlayRight(durationInternal, delay, scale);

        // 프랍  
        for (var i = 0; i < propCount; i++)
        {
            var propPrefab = enemyPropPrefabs[Random.Range(0, enemyPropPrefabs.Length - 1)];
            var spawnProp = SpawnProp(propPrefab, pos, Vector3.one * scale);
            spawnProp.RollRight(durationInternal, delay);
        }
    }
    
    public void ShowAllyDeadGroundLeft(Vector3 pos, float scale = 1f, float durationCustom = InfinityDuration)
    {
        // 스필  
        var spillPrefab = allySpillPrefabs[Random.Range(0, allySpillPrefabs.Length - 1)];
        var spawnSpill = SpawnSpill(spillPrefab, pos);
        spawnSpill.PlayLeft(durationCustom, delay, scale);

        // 프랍  
        for (var i = 0; i < propCount; i++)
        {
            var propPrefab = allyPropPrefabs[Random.Range(0, allyPropPrefabs.Length - 1)];
            var spawnProp = SpawnProp(propPrefab, pos, Vector3.one * scale);
            spawnProp.RollLeft(durationCustom, delay);
        }
    }
    
    public void ShowAllyDeadGroundRight(Vector3 pos, float scale = 1f, float durationCustom = InfinityDuration)
    {
        // 스필  
        var spillPrefab = allySpillPrefabs[Random.Range(0, allySpillPrefabs.Length - 1)];
        var spawnSpill = SpawnSpill(spillPrefab, pos);
        spawnSpill.PlayRight(durationCustom, delay, scale);

        // 프랍  
        for (var i = 0; i < propCount; i++)
        {
            var propPrefab = allyPropPrefabs[Random.Range(0, allyPropPrefabs.Length - 1)];
            var spawnProp = SpawnProp(propPrefab, pos, Vector3.one * scale);
            spawnProp.RollRight(durationCustom, delay);
        }
    }

    public void ShowExplosion(Vector3 pos, float scale = 1f)
    {
        var effect = LeanPool.Spawn(explosionPrefab, pos, Quaternion.identity, transform);
        effect.transform.localScale *= scale;
    }

    private Spill SpawnSpill(Spill prefab, Vector3 pos)
    {
        var spawnSpill = LeanPool.Spawn(prefab, pos, Quaternion.identity, transform);
        return spawnSpill;
    }
    
    private Prop SpawnProp(Prop prefab, Vector3 pos, Vector3 scale)
    {
        var offsetY = Random.Range(-randomYOffsetRange / 2f, randomYOffsetRange / 2f);
        var revisionPos = pos;
        revisionPos.y += offsetY;
        
        var spawnProp = LeanPool.Spawn(prefab, revisionPos, Quaternion.identity, transform);
        spawnProp.transform.localScale = scale;
        return spawnProp;
    }
}