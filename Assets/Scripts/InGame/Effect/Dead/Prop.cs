﻿using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using Lean.Pool;
using UnityEngine;

[RequireComponent(typeof(SpriteRenderer))]
[RequireComponent(typeof(CircleCollider2D))]
[RequireComponent(typeof(Rigidbody2D))]
public class Prop : MonoBehaviour
{
    [SerializeField] private float powerMin = 100f;
    [SerializeField] private float powerMax = 200f;
    
    private float _c;
    private Transform _cacheTransform;


    private SpriteRenderer _spriteRenderer;
    private CircleCollider2D _circleCollider;
    private Rigidbody2D _rigidbody;

    private Vector3 _rotAxis;


    private void Awake()
    {
        _spriteRenderer = GetComponent<SpriteRenderer>();
        _circleCollider = GetComponent<CircleCollider2D>();
        _rigidbody = GetComponent<Rigidbody2D>();
    }


    private void Start()
    {
        _c = 2 * Mathf.PI * _circleCollider.radius;

        _cacheTransform = transform;
    }
    
    public void RollLeft(float duration, float delay)
    {
        Play(Vector2.left, Vector3.forward, duration, delay);
        
        // 페이드 
        _spriteRenderer.DOFade(0f, duration).From(1f).SetDelay(delay);
    }

    public void RollLeftNoFade(float duration, float delay)
    {
        Play(Vector2.left, Vector3.forward, duration, delay);
    }

    public void RollRight(float duration, float delay)
    {
        Play(Vector2.right, Vector3.back, duration, delay);

        // 페이드 
        _spriteRenderer.DOFade(0f, duration).From(1f).SetDelay(delay);
    }
    
    public void RollRightNoFade(float duration, float delay)
    {
        Play(Vector2.right, Vector3.back, duration, delay);
    }

    private void Play(Vector2 direction, Vector3 rotAxis, float duration, float delay)
    {
        // 굴리기
        var power = RandomPower();
        _rigidbody.AddForce(direction * power);
        _rotAxis = rotAxis;

        // 자동 반납
        LeanPool.Despawn(gameObject, duration + delay);
    }


    private float RandomPower()
    {
        return Random.Range(powerMin, powerMax);
    }

    private void Update()
    {
        var angle = (_rigidbody.velocity.magnitude * 360 / _c);
        _cacheTransform.Rotate(_rotAxis, angle * Time.deltaTime, Space.World);
    }
}