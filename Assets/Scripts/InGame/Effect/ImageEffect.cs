﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace InGame.Effect
{
	public class ImageEffect : MonoBehaviour
	{
		[SerializeField] private Image image;
		[SerializeField] private Sprite[] effectSprites;
	
		[SerializeField] private float intervalSec = 0.2f;

		private CoroutineHandle _animation;
    
		private void OnEnable()
		{
			_animation = Timing.RunCoroutine(_Animation().CancelWith(gameObject));
		}

		private void OnDisable()
		{
			Timing.KillCoroutines(_animation);
		}

		private IEnumerator<float> _Animation()
		{
			while (true)
			{
				foreach (var effectSprite in effectSprites)
				{
					image.sprite = effectSprite;

					yield return Timing.WaitForSeconds(intervalSec);
				}
			}
		}
	}
}
