﻿using System;
using InGame.Data;
using Lean.Pool;
using UnityEngine;
using Random = UnityEngine.Random;

namespace InGame
{
    public class EffectSpawner : MonoBehaviourSingletonPersistent<EffectSpawner>
    {
        [Serializable]
        public class EffectSchema
        {
            public string name;
            public GameObject prefab;
        }

        [SerializeField] private EffectSchema[] effects;

        private static readonly string[] _damageEffects = { "dmg_fx_enemy_01", "dmg_fx_enemy_02", "dmg_fx_enemy_03", "dmg_fx_enemy_04", "dmg_fx_enemy_05" };

        public void Spawn(string effectIdx, Vector3 pos, Transform parent = null)
        {
            // 이펙트 온오프 설정 
            if (!UserData.My.UserInfo.GetFlag(UserFlags.IsOnDamageEffect, true)) return; 
            
            // 스폰
            foreach (var effect in effects)
            {
                if (effect.name.Contains(effectIdx))
                {
                    LeanPool.Spawn(effect.prefab, pos, Quaternion.identity, parent == null ? Instance.transform : parent);
                    break;
                }
            }
        }

        public static void SpawnRandomDamageEffect(Vector3 pos)
        {
            // 스폰
            Instance.Spawn(_damageEffects[Random.Range(0, _damageEffects.Length)], pos);
        }
    }
}