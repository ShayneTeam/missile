﻿using Global;
using InGame.Global;
using UnityEngine;

namespace InGame
{
    public class BossRewardEffect : MonoBehaviour
    {
        [SerializeField] private SpriteRenderer box;
        [SerializeField] private SpriteRenderer key;
        [SerializeField] private GameObject chip;

        public void InitBoxAndDiabloKey(string boxIdx)
        {
            // 박스
            box.gameObject.SetActive(true);
            var icon = DataboxController.GetDataString(Table.Box, Sheet.Box, boxIdx, "icon");
            box.sprite = AtlasController.GetSprite(icon);
            
            // 키
            key.gameObject.SetActive(true);
            
            // 칩
            chip.SetActive(false);
        }

        public void InitChip()
        {
            // 박스
            box.gameObject.SetActive(false);
            
            // 키
            key.gameObject.SetActive(false);
            
            // 칩
            chip.SetActive(true);
        }
    }
}