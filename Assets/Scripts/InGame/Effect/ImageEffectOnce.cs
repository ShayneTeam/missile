﻿using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace InGame.Effect
{
	public class ImageEffectOnce : MonoBehaviour
	{
		[SerializeField] private Image image;
		[SerializeField] private Sprite[] effectSprites;
	
		[SerializeField] private float intervalSec = 0.2f;
	
    
    
		private void OnEnable()
		{
			Timing.RunCoroutine(Animation().CancelWith(gameObject));
		}

		private IEnumerator<float> Animation()
		{
			foreach (var effectSprite in effectSprites)
			{
				image.sprite = effectSprite;

				yield return Timing.WaitForSeconds(intervalSec);
			}
	    
			// 애니 다 끝나고, 풀링 회수 
			LeanPool.Despawn(this);
		}
	}
}
