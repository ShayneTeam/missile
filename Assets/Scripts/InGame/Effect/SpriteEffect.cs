﻿using System;
using System.Collections;
using System.Collections.Generic;
using Lean.Pool;
using MEC;
using UnityEngine;

public class SpriteEffect : MonoBehaviour
{
    [SerializeField] private SpriteRenderer spriteRenderer;
    [SerializeField] private Sprite[] effectSprites;
	
    [SerializeField] private float intervalSec = 0.2f;

    private CoroutineHandle _animation;
	
    private void OnEnable()
    {
	    _animation = Timing.RunCoroutine(LoopAnimation().CancelWith(gameObject));
    }

    private void OnDisable()
    {
	    Timing.KillCoroutines(_animation);
    }

    private IEnumerator<float> LoopAnimation()
    {
		while (true)
	    {
		    foreach (var effectSprite in effectSprites)
		    {
			    spriteRenderer.sprite = effectSprite;

			    yield return Timing.WaitForSeconds(intervalSec);
		    }

		    yield return Timing.WaitForOneFrame;
	    }
    }
}
