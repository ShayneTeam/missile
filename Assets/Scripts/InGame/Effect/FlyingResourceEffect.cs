﻿using System;
using System.Collections;
using System.Collections.Generic;
using DG.Tweening;
using InGame.Global;
using Lean.Pool;
using MEC;
using UnityEngine;
using UnityEngine.UI;

namespace InGame.Effect
{
    public class FlyingResourceEffect : MonoBehaviour
    {
        [SerializeField] private Image icon;
        [SerializeField] private Ease ease;
        
        [Header("처음 스케일 애니 (기존 스케일에서 얼만큼 + 시킨 값으로 펀치할건지)"), SerializeField] private float initPunchAddScale = 1.2f; 
        [SerializeField] private float initPunchScaleDuration = 0.2f; 

        private Action _startCallback;
        private Action _endCallback;
        private RectTransform _rectTransform;

        public static FlyingResourceEffect Spawn(string type, float duration, Vector2 startPos, Vector2 endPos, Transform parent, Action startCallback, Action endCallback)
        {
            var effect = LeanPool.Spawn(InGamePrefabs.Instance.flyingResourceEffectPrefab, startPos, Quaternion.identity, parent);
            effect.Init(type, endPos, duration, startCallback, endCallback);

            return effect;
        }

        private void Init(string type, Vector2 endPos, float duration, Action startCallback, Action endCallback)
        {
            _startCallback = startCallback;
            _endCallback = endCallback;
            
            // 아이콘 
            icon.SetSpriteWithOriginSIze(AtlasController.GetSprite(RewardTransactor.GetIcon(type)));

            // 이동 플로우
            Timing.RunCoroutine(MoveFlow(endPos, duration).CancelWith(gameObject));
        }

        private IEnumerator<float> MoveFlow(Vector2 endPos, float duration)
        {
            // 시작 콜백
            _startCallback?.Invoke();
            
            // UI는 RectTransform으로 해야 함
            // transform으로 처리하니 처음 실행할 땐 잘 되지만, 재활용될 때 좌표가 이상해짐  
            if (_rectTransform == null) _rectTransform = GetComponent<RectTransform>();
            
            // 첫 시작 때, 약간 펀치 스케일
            var startPunchScale = _rectTransform.DOPunchScale(Vector3.one * initPunchAddScale, initPunchScaleDuration, 0, 0);
            yield return Timing.WaitUntilDone(startPunchScale.WaitForCompletion(true));
            
            // 이동
            var move = _rectTransform.DOMove(endPos, duration).SetEase(ease);
            yield return Timing.WaitUntilDone(move.WaitForCompletion(true));
            
            // 종료 콜백 
            _endCallback?.Invoke();
            
            // 도달해서도 한 번 펀치 스케일 
            var endPunchScale = _rectTransform.DOPunchScale(Vector3.one * initPunchAddScale, initPunchScaleDuration, 0, 0);
            yield return Timing.WaitUntilDone(endPunchScale.WaitForCompletion(true));
            
            // 반납
            LeanPool.Despawn(gameObject);
        }
    }
}