﻿using System.Collections;
using System.Collections.Generic;
using BackEnd;
using Doozy.Engine.UI;
using InGame.Data;
using LitJson;
using MEC;
using Server;
using UI.Popup;
using UI.Title;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{
    [SerializeField] private List<RankUserSlot> rankUserSlots;
    
    
    private IEnumerator<float> _ShowRankUsersFlow()
    {
        // 랭킹 유저 요청 
        var rankers = ServerRanking.GetRankers(3);
        for (var i = 0; i < rankers.Count; i++)
        {/*
            // 동기에서 비동기 처리로 변경되면서, 아래 로직도 변경돼야 함
            var ranker = rankers[i];
            ServerRanking.RequestRankersTables(ranker.gamerInDate);
            var rankUserData = UserData.Other[ranker.gamerInDate];
            
            // 코스튬
            var head = rankUserData.Costume.GetEquipped(UserData.CostumeType.head);
            var skin = rankUserData.Costume.GetEquipped(UserData.CostumeType.skin);
            var body = rankUserData.Costume.GetEquipped(UserData.CostumeType.body);
            
            // 바주카
            rankUserData.Bazooka.GetState(out var equippedBazookaIdx, out _, out _);
            
            // 슬롯 세팅 
            var slot = rankUserSlots[i];
            slot.Init(ranker.rank, ranker.nickname, head, skin, body, equippedBazookaIdx);*/
        }

        yield return Timing.WaitForOneFrame;
    }
}