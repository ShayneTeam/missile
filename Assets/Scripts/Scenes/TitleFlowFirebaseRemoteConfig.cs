﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using BackEnd;
using Doozy.Engine.UI;
using Firebase;
using Firebase.Extensions;
using Firebase.RemoteConfig;
using Global;
using Global.Extensions;
using Gpm.WebView;
using InGame;
using InGame.Data;
using LitJson;
using MEC;
using Mono.CSharp;
using Newtonsoft.Json;
using Server;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{
    private bool _hasFinishedFirebaseFlow;
    
    private IEnumerator<float> _FirebaseRemoteConfigFlow()
    {
        if (_hasFinishedFirebaseFlow) yield break;
        
        var firebaseCheckDependenciesCheck = false;
        var firebaseDependenciesStatus = DependencyStatus.UnavilableMissing;
        FirebaseApp.CheckAndFixDependenciesAsync().ContinueWithOnMainThread(task =>
        {
            firebaseCheckDependenciesCheck = true;
            firebaseDependenciesStatus = task.Result;
        });

        yield return Timing.WaitUntilTrue(() => firebaseCheckDependenciesCheck);

        if (firebaseDependenciesStatus != DependencyStatus.Available) yield break;

        // [START set_defaults]
        var defaults = new Dictionary<string, object>
        {
            // These are the values that are used if we haven't fetched data from the
            { "notice", string.Empty },
            { "weblog", string.Empty },
            { "isOn_weblog", false },
            { "saveToServerDelay", 30 },
            { "saveToServerDelayNightMode", 600 },
            { "GC_interval", 1 },
            { "GC_budget_inGame", 3 },
            { "GC_budget_menu", 1000 },
            { "gm_nickname_keyword", "GM" },
            { "isOn_dailyChip", true },
            { "isOn_CameraOrthographicSize", false },
            { "tutorial_show", false },
            { "gm_chat_color", "<color=#54F4FF>{0}</color>" },
            { "raid_url", string.Empty },
        };

        var isFirebaseInitialized = false;

        FirebaseRemoteConfig.DefaultInstance.SetDefaultsAsync(defaults)
            .ContinueWithOnMainThread(task =>
            {
                // [END set_defaults]
                ULogger.Log("RemoteConfig configured and ready!");
                isFirebaseInitialized = true;
            });

        yield return Timing.WaitUntilTrue(() => isFirebaseInitialized);

        var fetchComplete = false;

        var fetchTask = FirebaseRemoteConfig.DefaultInstance.FetchAsync(TimeSpan.Zero);
        fetchTask.ContinueWithOnMainThread(task =>
        {
            if (fetchTask.IsCanceled)
            {
                ULogger.Log("Fetch canceled.");
            }
            else if (fetchTask.IsFaulted)
            {
                ULogger.Log("Fetch encountered an error.");
            }
            else if (fetchTask.IsCompleted)
            {
                ULogger.Log("Fetch completed successfully!");
            }

            fetchComplete = true;
        });

        yield return Timing.WaitUntilTrue(() => fetchComplete);

        var info = FirebaseRemoteConfig.DefaultInstance.Info;
        if (info.LastFetchStatus != LastFetchStatus.Success) yield break;

        var firebaseRemoteActivate = false;

        FirebaseRemoteConfig.DefaultInstance.ActivateAsync().ContinueWithOnMainThread(task => { firebaseRemoteActivate = true; });
        yield return Timing.WaitUntilTrue(() => firebaseRemoteActivate);

        //
        var inAppReviewRemoteValue = FirebaseRemoteConfig.DefaultInstance.GetValue("inAppReviewStage");
        if (inAppReviewRemoteValue.Source == ValueSource.RemoteValue)
        {
            var inAppStageValue = inAppReviewRemoteValue.LongValue;
            FirebaseRemoteConfigController.inAppReviewStage = inAppStageValue;
        }
        
        //
        FirebaseRemoteConfigController.saveToServerDelay = FirebaseRemoteConfig.DefaultInstance.GetValue("saveToServerDelay").LongValue;
        
        //
        FirebaseRemoteConfigController.saveToServerDelayForOurServer = FirebaseRemoteConfig.DefaultInstance.GetValue("saveToServerDelay_forOurServer").LongValue;
        FirebaseRemoteConfigController.server_url = FirebaseRemoteConfig.DefaultInstance.GetValue("server_url").StringValue;
        
        //
        FirebaseRemoteConfigController.saveToServerDelayNightMode = FirebaseRemoteConfig.DefaultInstance.GetValue("saveToServerDelayNightMode").LongValue;
        
        //
        FirebaseRemoteConfigController.gcInterval = (ulong)FirebaseRemoteConfig.DefaultInstance.GetValue("GC_interval").LongValue;
        
        //
        FirebaseRemoteConfigController.gcBudgetInGame = (ulong)FirebaseRemoteConfig.DefaultInstance.GetValue("GC_budget_inGame").LongValue;
        
        //
        FirebaseRemoteConfigController.gcBudgetMenu = (ulong)FirebaseRemoteConfig.DefaultInstance.GetValue("GC_budget_menu").LongValue;
        
        //
        FirebaseRemoteConfigController.isOnDailyChip = FirebaseRemoteConfig.DefaultInstance.GetValue("isOn_dailyChip").BooleanValue;
        
        //
        FirebaseRemoteConfigController.isOnCameraOrthographicSize = FirebaseRemoteConfig.DefaultInstance.GetValue("isOn_CameraOrthographicSize").BooleanValue;
        
        //
        FirebaseRemoteConfigController.tutorialShow = FirebaseRemoteConfig.DefaultInstance.GetValue("tutorial_show").BooleanValue;
        
        //
        FirebaseRemoteConfigController.isOnWeblog = FirebaseRemoteConfig.DefaultInstance.GetValue("isOn_weblog").BooleanValue;

        //
        var gmNickNameKeywordRemoteValue = FirebaseRemoteConfig.DefaultInstance.GetValue("gm_nickname_keyword");
        FirebaseRemoteConfigController.gmNickNameKeyword = gmNickNameKeywordRemoteValue.StringValue;
        
        //
        var gmChatColorRemoteValue = FirebaseRemoteConfig.DefaultInstance.GetValue("gm_chat_color");
        FirebaseRemoteConfigController.gmChatMessageColor = gmChatColorRemoteValue.StringValue;

        //
        var webLogUrlRemoteValue = FirebaseRemoteConfig.DefaultInstance.GetValue("weblog_url");
        if (webLogUrlRemoteValue.Source == ValueSource.RemoteValue)
        {
            var weblogUrlValue = webLogUrlRemoteValue.StringValue;
            FirebaseRemoteConfigController.webLogUrl = weblogUrlValue;
        }
        
        // 
        FirebaseRemoteConfigController.raidUrl = FirebaseRemoteConfig.DefaultInstance.GetValue("raid_url").StringValue;
        
        //
        FirebaseRemoteConfigController.enable_sentry_backend = FirebaseRemoteConfig.DefaultInstance.GetValue("enable_sentry_backend").BooleanValue;

        //
        var noticeRemoteValue = FirebaseRemoteConfig.DefaultInstance.GetValue("notice");
        if (noticeRemoteValue.Source == ValueSource.RemoteValue)
        {
            var noticeValue = noticeRemoteValue.StringValue;
            var noticeConvertToJsonDictionary = JsonConvert.DeserializeObject<Dictionary<string, bool>>(noticeValue);

            var noticeCountry = ServerMyInfo.CountryCode == "KR" ? "KR" : "US";

            if (noticeConvertToJsonDictionary.ContainsKey(noticeCountry) && noticeConvertToJsonDictionary[noticeCountry])
            {
                var appName = Localization.GetText("info_224");

                GpmWebView.ShowUrl(
                    $"http://rpg-2-22867869.web.app/notice_{noticeCountry.ToLower()}.html",
                    new GpmWebViewRequest.Configuration
                    {
                        style = GpmWebViewStyle.FULLSCREEN,
                        isClearCookie = true,
                        isClearCache = true,
                        isNavigationBarVisible = true,
                        navigationBarColor = "#E6370C",
                        title = appName,
                        isBackButtonVisible = true,
                        isForwardButtonVisible = true,
#if UNITY_IOS
                        contentMode = GpmWebViewContentMode.MOBILE
#elif UNITY_ANDROID
                supportMultipleWindows = true
#endif
                    },
                    error => { },
                    error => { },
                    new List<string>() { },
                    (data, error) => { });
            }
        }

        yield return Timing.WaitForOneFrame;

        _hasFinishedFirebaseFlow = true;
    }
}