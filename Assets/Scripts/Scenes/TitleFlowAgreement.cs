﻿using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
using InGame.Data;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{
    private bool _finishAgreement;

    private IEnumerator<float> _AgreementFlow()
    {
        // 이미 동의 처리했다면 스킵 
        if (UserData.My.UserInfo.GetFlag(UserFlags.IsAgreeService) && UserData.My.UserInfo.GetFlag(UserFlags.IsAgreePrivacy))
            yield break;
        
        // 팝업 보여주기 
        var agreementPopup = AgreementPopup.Show();
        var popup = agreementPopup.GetComponent<UIPopup>();

        // 팝업 닫힐 때까지 무한 대기
        yield return Timing.WaitUntilDone(popup.WaitHide().CancelWith(gameObject));

        // 팝업 닫혔다면 모두 동의했다고 간주됨 
        UserData.My.UserInfo.SetFlag(UserFlags.IsAgreeService, true);
        UserData.My.UserInfo.SetFlag(UserFlags.IsAgreePrivacy, true);
        
        // 푸쉬 동의 했다고 간주하고, 푸쉬 등록 
        BackEndManager.PutDeviceToken();
    }
}