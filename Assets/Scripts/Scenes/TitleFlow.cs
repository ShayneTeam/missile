﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Diagnostics;
using System.Globalization;
using System.Numerics;
using BackEnd;
using BestHTTP;
using CodeStage.AntiCheat.ObscuredTypes;
using DarkTonic.MasterAudio;
using DG.Tweening;
using Doozy.Engine.Progress;
using Doozy.Engine.UI;
using Doozy.Engine.UI.Settings;
using Firebase.Analytics;
using Firebase.Crashlytics;
using Global;
using Global.Extensions;
using Gpm.WebView;
using InGame;
using InGame.Controller;
using InGame.Data;
using JetBrains.Annotations;
using LitJson;
using Ludiq;
using LunarConsolePlugin;
using MEC;
using Newtonsoft.Json;
using Server;
using TMPro;
using UI.Popup;
using UI.Title;
using UnityEngine;
using UnityEngine.AddressableAssets;
using UnityEngine.ResourceManagement.AsyncOperations;
using UnityEngine.SceneManagement;
using UnityEngine.UDP;
using Utils;
using Random = UnityEngine.Random;

public partial class TitleFlow : MonoBehaviour
{
    [SerializeField] private TextMeshProUGUI loadingStateText;

    [SerializeField] private UIView backendLogo;

    [SerializeField] private GameObject titleLogoKr;
    [SerializeField] private GameObject titleLogoEn;

    [SerializeField] private TextMeshProUGUI versionText;

    [Header("뒤끝 로고 보여주는 타이밍에 로딩할 데이터박스들")] [SerializeField]
    private List<string> databoxPreLoadList;

    [Header("터치 - 시작")] [SerializeField] private GameObject touch;
    private bool _isTouch;

    [Header("개발 - GPM Profiler")]
    [SerializeField] private GameObject devGpmProfiler;

    private void Start()
    {
        if (ExtensionMethods.IsAndroid() || ExtensionMethods.IsIOS())
        {
            QualitySettings.vSyncCount = 0;
            Application.targetFrameRate = 60;
            Screen.sleepTimeout = SleepTimeout.NeverSleep;
            HTTPManager.KeepAliveDefaultValue = false;
        }
        
        // 타이틀 플로우 시작 
        Timing.RunCoroutine(_Flow().CancelWith(gameObject));
    }

    private IEnumerator<float> _ShowLoadingStateText(string key)
    {
        loadingStateText.text = Localization.GetFormatText(key, Random.Range(1, 99).ToLookUpString()); // 연출용
        yield return Timing.WaitForOneFrame; // 밑에가 동기 호출들이라 한 프레임 기다리지 않으면, 텍스트 안 보여짐 
    }

    private void InitLogic()
    {
        // 로그를 남기지 않음.
        MasterAudio.Instance.disableLogging = true;
        // MasterAudio.Instance.logOutOfVoices = true;

        DOTween.SetTweensCapacity(2000, 100);
    }

    private void InitTestLogic()
    {
        if(!ExtensionMethods.IsDev()) return;
        
        devGpmProfiler.SetActive(true);

        var lunarObject = new GameObject("Lunar Console");
        lunarObject.AddComponent<LunarConsole>();
        lunarObject.AddComponent<LunarConsoleManager>();
        DontDestroyOnLoad(lunarObject.gameObject);
    }

    private IEnumerator<float> _ShowBackEndLogoFlow()
    {
        // 뒤끝 로고 보여주기 
        backendLogo.gameObject.SetActive(true);

        // 뒤끝 로그 1초 보여 줌 
        if (!ExtensionMethods.IsUnityEditor())
            yield return Timing.WaitForSeconds(1f);

        // 로고 보여주는 동안, 필요한 데이터박스 로딩 
        var stopwatch = new Stopwatch();
        stopwatch.Start();

        // 로고 보여질 때 로컬라이즈 테이블 및 가벼운 사이즈의 테이블들 로딩 
        DataboxController.Instance.Load(databoxPreLoadList);

        stopwatch.Stop();
        var elapsedSeconds = stopwatch.ElapsedMilliseconds / 1000f;
        ULogger.Log($"데이터 선로딩 소요 시간 : {elapsedSeconds.ToString()}", Color.yellow);

        // 타이틀 로고 
        RefreshTitleLogo();

        // 뒤끝 로고 가리기 
        backendLogo.Hide();
        yield return Timing.WaitUntilDone(backendLogo._WaitHide().CancelWith(gameObject));
    }

    private void RefreshTitleLogo()
    {
        var isLanguageKorea = Localization.Language == "kr";

        titleLogoKr.SetActive(isLanguageKorea);
        titleLogoEn.SetActive(!isLanguageKorea);
    }

    private IEnumerator<float> _Flow()
    {
        // 버전 텍스트
        versionText.text = $"Ver {Application.version}";

        // 빈문자열로 상태 메시지 시작
        loadingStateText.text = "";

        // 미디에이션 초기화 (2021.11.01 원래 하단부분에 있었으나, 오프라인 팝업이 출력될때 (광고보고 x2) 초기화가 안되어서, 최대한 위쪽으로 옮김.
        AdsMediator.Instance.Init();

        // 뒤끝 스플래시 스크린 1초 (로컬라이즈 테이블 파싱을 여기서 함)
        yield return Timing.WaitUntilDone(_ShowBackEndLogoFlow().CancelWith(gameObject), Segment.RealtimeUpdate);

        // BGM (최소 Config Databox 로드한 후, 설정 토대로 재생)
        SoundController.Instance.Init();
        SoundController.Instance.PlayBGM("bgm_title");

        // 뒤끝 초기화 플로우
        yield return Timing.WaitUntilDone(_InitBackEndFlow().CancelWith(gameObject));

        // 기본 로직 초기화
        InitLogic();

        // 개발 테스트 로직 초기화 (뒤끝 초기화 필요)
        InitTestLogic();

        // 필수 업데이트 버전 체크
        yield return Timing.WaitUntilDone(_ShowLoadingStateText("system_030").CancelWith(gameObject)); // 버전 체크 상태 메시지

        yield return Timing.WaitUntilDone(_CheckUpdateFlow().CancelWith(gameObject));

        // 파이어베이스 Remote Config 플로우
        yield return Timing.WaitUntilDone(_FirebaseRemoteConfigFlow().CancelWith(gameObject));

        // 로그인 (차단 유저 처리 포함)
        ServerUri.ServerUrl = FirebaseRemoteConfigController.server_url;
        yield return Timing.WaitUntilDone(_LoginFlow().CancelWith(gameObject));
        
        // Firebase 유저 ID 설정
        Crashlytics.SetUserId(Backend.UserInDate);
        FirebaseAnalytics.SetUserId(Backend.UserInDate);

        // 로그인이 된 후 UserData.My가 내 InDate로 동작되는 이후부터 호출
        ReviewController.Instance.StartToCheck();
        LeaderBoardController.Instance.StartAutoReport();
        LogController.Instance.StartAutoLog();

        // GC 예산 설정 
        GCController.IntervalSec = FirebaseRemoteConfigController.gcInterval;
        GCController.Instance.SetBudget(FirebaseRemoteConfigController.gcBudgetInGame);

        // 닉네임 변경 (신규유저만 가능)
        yield return Timing.WaitUntilDone(_NicknameFlow().CancelWith(gameObject));

        // 공지사항 & 차트 플로우 
        yield return Timing.WaitUntilDone(_NoticeFlow().CancelWith(gameObject));

        // 서버 시간 받기 (각종 일일 초기화를 위해 필수)
        ServerTime.Instance.GetServerTimeSync();
        
        // Doozy 데이터베이스 로딩 
        UIPopupSettings.PreLoadDatabase();

        // 데이터박스 로딩
        yield return Timing.WaitUntilDone(_ShowLoadingStateText("system_033").CancelWith(gameObject)); // 데이터 로딩 상태 메시지 
        yield return Timing.WaitUntilDone( DataboxController.Instance.Load((percent) =>
        {
            loadingStateText.text = Localization.GetFormatText("system_033", $"{(percent * 100f).ToString("0")}");
        })); // :0% .NET 구문은 100%%라 표기되서 안씀

        // 유저 데이터 동기화 
        yield return Timing.WaitUntilDone(_SyncUserData().CancelWith(gameObject));
        
        // 내 랭킹 요청 (웹로그, 랭킹팝업, 채팅에서 바로 보여주기용)
        ServerRanking.Instance.StartAutoGettingMyRank();

        // 어드레서블 선로딩 (반드시 데이터박스 로딩 이후에 해야 함 + 서버 데이터 로딩 후에도 해야 함. 그래야 UserData 값에 이상한 값 안 들어감)
        AddressableController.PreLoad();

        // 로그인 + 유저데이터 싱크 된 이후 부터, 미션 체크 시작 (UserData.My가 내 InDate로 동작 + 유저 데이터)
        MissionController.Instance.StartMissionCheck();

        // 광고, IAP 초기화 (유저데이타 필요)
        yield return Timing.WaitUntilDone(_InitOtherPlatformFlow().CancelWith(gameObject));
        
        // 출석 처리 
        yield return Timing.WaitUntilDone(BackEndManager.Attendance().CancelWith(gameObject));
        
        // 데일리 기프트 시작 (DailyProcess 이후에 할 것)
        DailyGiftController.StartTimer();
        DailyChipController.StartTimer();

        // 약탈 매칭된 유저들의 데이타 요청
        // 매일 한 번 DailyProcess가 호출됐다면, RequestNewMatching이 호출됐기에 필요없지만
        // DailyProcess가 호출되지 않았다면, 호출 필요함 
        // (데이터 중복 로드 처리는 되지 않음. 내부적으로 캐싱 처리함)
        yield return Timing.WaitUntilDone(InGameControlHub.My.PlunderController._RequestMatchedUsersData()
            .CancelWith(gameObject));

        // 우편 받기 
        ServerPost.Instance.GetPostList(null);
        
        // 유저데이터 자동 저장 시작 (앵간한 데이터 처리 다 받은 이후부터 시작) 
        UserData.My.StartAutoSaveToServer();

        // 이용약관 팝업 (유저데이터 받고 처리 해야 함)
        yield return Timing.WaitUntilDone(_AgreementFlow().CancelWith(gameObject));
        
        // 오프라인 보상 처리
        yield return Timing.WaitUntilDone(_OfflineRewardFlow().CancelWith(gameObject));
        
        // 인게임 지속 오브젝트 씬 로딩 
        // 이 씬에 등록된 오브젝트들은 전부 DontDestroyObject들만 모아져있음
        // 씬 전환이 Single로 이어져도, 삭제되지 않음
        // (일관성 맞추려면, Title 씬에 올라간 DontDestroyObject들도 "Title (Persistent)" 씬으로 따로 빼는게 맞음)
        AsyncOperation inGameSceneAsync = null;
        yield return Timing.WaitUntilDone(_LoadSceneAsync("InGame (Persistent)", LoadSceneMode.Additive, (async) => { inGameSceneAsync = async; }));

        // 스테이지 씬 비동기 로딩 
        AsyncOperation stageSceneAsync = null;
        yield return Timing.WaitUntilDone(_LoadSceneAsync("Stage", LoadSceneMode.Single, (async) => { stageSceneAsync = async; }));
        
        // "화면을 터치하면 전장으로 이동합니다" 메시지
        yield return Timing.WaitUntilDone(_ShowLoadingStateText("system_034").CancelWith(gameObject));

        // 터치할 때까지 대기 
        touch.SetActive(true);
        // 메시지 깜빡거림
        loadingStateText.GetComponent<DOTweenAnimation>().DOPlay(); 
        // 에디터는 자동 넘김
        if (ExtensionMethods.IsUnityEditor()) _isTouch = true;

        while (!_isTouch) yield return Timing.WaitForOneFrame;

        // 게임 시작 사운드 
        SoundController.Instance.PlayEffect("sfx_ui_titile_gamestart");

        // 인트로 플로우 
        yield return Timing.WaitUntilDone(_IntroFlow().CancelWith(gameObject));
        
        // 스테이지 씬 넘어가기 전에 자동 랭킹 갱신 등록 
        ServerRanking.Instance.StartAutoUpdatingMyRank();

        // 약탈 로그 요청  
        ServerPlunder.Instance.GetLogsOnBehind();

        // 채팅 서버 접속 
        ServerChat.Instance.ActivateBehind();

        // 자동 절전모드 적용
        AutoPowerSavingController.Instance.StartAutoPowerSaving();
        
        // 웹로그 시작
        WebLog.Instance.StartWebLog();
        WebLog.Instance.AllLog("backend_success_login", null);

        // 스테이지 씬으로 넘어감
        inGameSceneAsync.allowSceneActivation = true;
        stageSceneAsync.allowSceneActivation = true;

        // 넘어가기 전, GC 한 번 수동 호출 
        GCController.CollectFull();
    }

    private static IEnumerator<float> _SyncUserData()
    {
        // 유저데이타 싱크 (실패할 경우, 최대 3번은 시도해봄)
        UserData.SaveToServerDelay = FirebaseRemoteConfigController.saveToServerDelay;
        
        // 우리 서버 관련 세팅
        UserData.SaveToServerDelayForOurServer = FirebaseRemoteConfigController.saveToServerDelayForOurServer;
    
        //
        var successSyncUserData = false;

        // 무한 호출용 로컬펑션  
        void SyncUserData()
        {
            if (UserData.My.SyncServerDataAllForHost())
                successSyncUserData = true;
            else
                OkPopup.Show("system_051", "system_052", "system_016", SyncUserData);
        }

        // 최초 1회 호출
        SyncUserData();

        // 성공하기 전까진 다음 플로우 못 넘어감
        while (!successSyncUserData) yield return Timing.WaitForOneFrame;
    }

    private IEnumerator<float> _LoadSceneAsync(string sceneName, LoadSceneMode mode, Action<AsyncOperation> callback)
    {
        // !Unity 2019 known bug
        // LoadSceneAsync 하기 전, yield return new WaitForSeconds(0.1f) 넣어줘야 
        // allowSceneActivation = false 동작함 
        // 넣지 않으면, 로딩되자마자 바로 씬 전환됨
        yield return Timing.WaitForSeconds(0.1f);

        // 씬 비동기 로딩 
        var sceneAsync = SceneManager.LoadSceneAsync(sceneName, mode);
        
        // 로딩 후 씬 전환 자동으로 되지 못하게 함
        sceneAsync.allowSceneActivation = false;
        
        while (!sceneAsync.isDone)
        {
            if (sceneAsync.progress >= 0.9f)
            {
                // 완료되면 콜백 전달
                callback(sceneAsync);
                yield break;
            }

            // 씬 로딩 전까지 계속 대기 
            yield return Timing.WaitForOneFrame;
        }
    }

    [PublicAPI]
    public void StartStage()
    {
        _isTouch = true;
        touch.SetActive(false);
    }
}