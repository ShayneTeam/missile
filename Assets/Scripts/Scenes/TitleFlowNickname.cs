﻿using System.Collections;
using System.Collections.Generic;
using BackEnd;
using Doozy.Engine.UI;
using Global.Extensions;
using InGame.Controller;
using InGame.Data;
using MEC;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{

    private IEnumerator<float> _NicknameFlow()
    {
        // 닉네임 체크 도중에 나가졌거나, 유효하지 않은 문자열이라면 닉네임 설정해야 함 
        var isNicknameValidated = NicknameController.IsNicknameValidated();
        if (ExtensionMethods.IsUnityEditor())
        {
            //isNicknameValidated = false;   // TODO. 강제 테스트
        } 
        // 닉네임 유효할 경우, 스킵 
        if (isNicknameValidated) yield break;

        // 팝업 보여주기 
        var nicknamePopup = NicknamePopup.Show();
        var popup = nicknamePopup.GetComponent<UIPopup>();

        // 팝업 닫힐 때까지 무한 대기
        yield return Timing.WaitUntilDone(popup.WaitHide().CancelWith(gameObject));
    }
}