using System;
using System.Collections;
using System.Collections.Generic;
using System.Globalization;
using BackEnd;
using BestHTTP;
using Cysharp.Threading.Tasks;
using Global;
using Global.Extensions;
using InGame;
using JetBrains.Annotations;
using LitJson;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;
using Utils;

public partial class TitleFlow : MonoBehaviour
{
    private IEnumerator<float> _LoginFlow()
    {
        BackendReturnObject loginResult = null;

        // 에디터일 경우
        if (ExtensionMethods.IsUnityEditor())
        {
            loginResult = ServerMyInfo.Instance.customLogin ? LoginCustom() : LoginGuest();
        }
        // 개발 빌드일 경우
        else if (ExtensionMethods.IsDevelopmentBuild() || ExtensionMethods.IsDev())
        {
            PlayerPrefs.DeleteKey("lastLogin");

            if (ExtensionMethods.IsAndroid())
            {
                // 구글 로그인 진행 (GPGS 처리가 비동기므로, out 키워드가 아닌 콜백으로 결과 받음)
                Timing.RunCoroutine(_LoginGoogleFlow((result) => { loginResult = result; })
                    .CancelWith(gameObject));
            }

            if (ExtensionMethods.IsIOS())
            {
                // 게임센터 로그인 진행 (GameCenter 처리가 비동기므로, out 키워드가 아닌 콜백으로 결과 받음)
                Timing.RunCoroutine(_LoginAppleIDFlow((result) => { loginResult = result; })
                    .CancelWith(gameObject));
            }
            
            // 게스트 로그인 활성화 
            Timing.RunCoroutine(_LoginGuestFlow((result) => { loginResult = result; })
                .CancelWith(gameObject));
        }
        // 기기일 경우, 
        else
        {
            // 안드로이드일 경우, 
            if (ExtensionMethods.IsAndroid())
            {
                // 구글 로그인 진행 (GPGS 처리가 비동기므로, out 키워드가 아닌 콜백으로 결과 받음)
                yield return Timing.WaitUntilDone(_LoginGoogleFlow((result) => { loginResult = result; })
                    .CancelWith(gameObject));
            }
            // iOS 일 경우,
            else if (ExtensionMethods.IsIOS())
            {
                // PlayerPrefs.DeleteKey("lastLogin");
        
                // 둘중 하나만 결과가 넘어와도 하단의 loginResult == null 체크구문에 걸리기 때문에, 이렇게 작성.
                
                // 게임센터 로그인 진행 (GameCenter 처리가 비동기므로, out 키워드가 아닌 콜백으로 결과 받음)
                Timing.RunCoroutine(_LoginAppleIDFlow((result) => { loginResult = result; })
                    .CancelWith(gameObject));
        
                // 게스트 로그인 활성화 
                Timing.RunCoroutine(_LoginGuestFlow((result) => { loginResult = result; })
                    .CancelWith(gameObject));
            }
            // 기타 플랫폼은 일단 전부 게스트 로그인 
            else
            {
                loginResult = LoginGuest();
            }
        }

        // 뒤끝 로그인 결과 넘어올 때까지 대기 
        yield return Timing.WaitUntilTrue(() => loginResult != null);

        // 로그인 실패
        if (!loginResult.IsSuccess())
        {
            var statusCheck = loginResult.GetStatusCode();
            if (statusCheck == "401" || statusCheck == "403") // 401 = 점검, 403 = 차단
            {
                var errTitle = Localization.GetText(statusCheck == "401" ? "system_006" : "system_010");
                var errMsg = statusCheck == "401"
                    ? Localization.GetText("system_007")
                    : Localization.GetFormatText("system_011", loginResult.GetErrorCode());

                OkPopup.Alert(errTitle, errMsg, () => { Application.Quit(); });
                Time.timeScale = 0;
            }
            else
            {
                OkPopup.Alert("Failed Login", $"{loginResult.GetErrorCode()} - {loginResult.GetMessage()}", () => { });
            }

            // 플로우 더 이상 진행하지 못하게 막음
            while (true)
                yield return Timing.WaitForOneFrame;
        }

        // 로그인 액세스 토큰 업데이트
        Backend.BMember.RefreshTheBackendToken();

        // 다른 곳에서 유저 처리할 수 있도록 세팅
        ServerMyInfo.Instance.Init(loginResult);

        ServerMyInfo.Instance.GetMyInfo();
    }

    private static BackendReturnObject LoginGuest()
    {
        // 게스트 로그인 진행 
        var login = Backend.BMember.GuestLogin(DateTime.Now.ToLongDateString());
        if (!login.IsSuccess())
            return login;

        ULogger.Log("게스트 로그인 성공");

        // 게스트 ID 로그찍음
        BackEndManager.PrintGuestId();

        return login;
    }

    private static BackendReturnObject LoginCustom()
    {
        // 커스텀 로그인 진행 
        var login = Backend.BMember.CustomLogin(ServerMyInfo.Instance.ID, ServerMyInfo.Instance.Password)
            .CommonErrorCheck();
        if (login.IsSuccess())
        {
            // 우리 서버 처리 꼽사리 
            LoginCustomOurServer().Forget();
            
            return login;
        }

        // 로그인 실패 시, 커스텀 회원가입 진행 (인스펙터 값대로 설정)
        Backend.BMember.CustomSignUp(ServerMyInfo.Instance.ID, ServerMyInfo.Instance.Password);

        // 위의 커스텀 회원가입 진행 후, 커스텀 로그인 진행
        var bro = Backend.BMember.CustomLogin(ServerMyInfo.Instance.ID, ServerMyInfo.Instance.Password);
        
        // 우리 서버 처리 꼽사리 
        LoginCustomOurServer().Forget();
        
        // 뒤끝 결과 반환
        return bro;
    }
    
    private static async UniTaskVoid LoginCustomOurServer()
    {
        // 커스텀 로그인 진행 
        var req = new HTTPRequest(ServerUri.customLogin, HTTPMethods.Post);
        req.AddField("customId", ServerMyInfo.Instance.ID);
        AddCommonLoginFields(ref req);
        var rsp = await req.GetHTTPResponseAsync();

        if (!rsp.IsSuccess) return;
        
        // 공통 우리 서버 처리 
        CommonProcessOurServer(rsp);
    }
    
    private static void AddCommonLoginFields(ref HTTPRequest req)
    {
        req.AddField("user_device_os", SystemInfo.operatingSystem);
        req.AddField("user_device_model_no", SystemInfo.deviceModel);
    }

    private static void CommonProcessOurServer(HTTPResponse rsp)
    {
        var ourLoginRsp = rsp.ToServerRsp();
        
        // 엑세스 토큰 갱신 및 저장 처리 (성공하든 실패하든 다음 플로우에 지장 주지 않게 함)
        var accessToken = ourLoginRsp.data["access_token"].StringValue();
        RefreshAndSaveToken(accessToken).Forget();
        
        // 닉네임
        var nickname = ourLoginRsp.data["nick_name"]?.StringValue();
        if (nickname == null)
        {
            UpdateNickname().Forget();
        }
        
        // 국가 코드 
        var countryCode = ourLoginRsp.data["countryCode"].StringValue();
        if (countryCode == "-")
        {
            // 서브스레드 별도로 처리 
            var defaultCountryCode = RegionInfo.CurrentRegion.TwoLetterISORegionName;
            UpdateCountryCode(defaultCountryCode).Forget();
        }
    }

    private static async UniTaskVoid UpdateCountryCode(string defaultCountryCode)
    {
        var req = new HTTPRequest(ServerUri.updateCountryCode, HTTPMethods.Post);
        req.AddField("country", defaultCountryCode);
        var rsp = await req.GetHTTPResponseAsync();
        
        if (rsp.IsSuccess)
        {
            ULogger.Log($"{nameof(UpdateCountryCode)} 성공");
        }
        else
        {
            ULogger.Log($"{nameof(UpdateCountryCode)} 실패. {rsp.StatusCode.ToLookUpString()} : {rsp.Message}");
        }
    }

    private static async UniTaskVoid UpdateNickname()
    {
        var req = new HTTPRequest(ServerUri.updateNickname, HTTPMethods.Post);
        req.AddField("nickname", Backend.UserNickName);
        var rsp = await req.GetHTTPResponseAsync();
        
        if (rsp.IsSuccess)
        {
            ULogger.Log($"{nameof(UpdateNickname)} 성공");
        }
        else
        {
            ULogger.Log($"{nameof(UpdateNickname)} 실패. {rsp.StatusCode.ToLookUpString()} : {rsp.Message}");
        }
    }

    private static async UniTaskVoid RefreshAndSaveToken(string token)
    {
        // 토큰 갱신 
        var req = new HTTPRequest(ServerUri.refreshAccountToken, HTTPMethods.Post);
        req.AddField("token", token);
        var rsp = await req.GetHTTPResponseAsync();
            
        // 갱신 실패 시, 그냥 종료
        if (!rsp.IsSuccess)
        {
            ULogger.Log($"{nameof(RefreshAndSaveToken)} 실패. {rsp.StatusCode.ToLookUpString()} : {rsp.Message}");
            return;
        }
        
        ULogger.Log($"{nameof(RefreshAndSaveToken)} 성공");

        // 토큰 저장
        var refreshedAccessToken = rsp.ToServerRsp().data.StringValue(); 
        PlayerPrefs.SetString("access_token", refreshedAccessToken);
    }
}