﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Extensions;
using AppleAuth.Interfaces;
using AppleAuth.Native;
using BackEnd;
using Doozy.Engine.UI;
using Global;
using Global.Extensions;
using InGame.Data;
using JetBrains.Annotations;
using MEC;
using UI.Popup;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using Utils;

public partial class TitleFlow : MonoBehaviour
{
    [Header("APPLE GUEST 로그인")] [SerializeField]
    private GameObject gameAppleGuestLoginButton;

    private BackendReturnObject _loginGuestResult;
    
    private const string guestLoginType = "Guest";

    private IEnumerator<float> _LoginGuestFlow(Action<BackendReturnObject> callback)
    {
        // 첫 실행인지 체크 
        var lastLoginType = PlayerPrefs.GetString("lastLogin", string.Empty);
        if (string.IsNullOrEmpty(lastLoginType))
        {
            // 첫 실행이라면, 게스트 로그인 버튼 눌러서 실행
            ShowAppleGuestLoginButton(true);
        }
        // 로그인 기록이 있을 시,
        else if(lastLoginType == guestLoginType)
        {
            // 자동 로그인 상태 메시지
            yield return Timing.WaitUntilDone(_ShowLoadingStateText("system_032").CancelWith(gameObject));

            // 게스트 로그인 버튼 가리기
            ShowAppleGuestLoginButton(false);

            // 자동 게스트 로그인 진행
            AppleGuestLogin();
        }

        // 게스트 응답이 오기 전까지 통과 못함 
        while (_loginGuestResult == null)
            yield return Timing.WaitForOneFrame;

        // 최근 로그인 게스트로 설정
        PlayerPrefs.SetString("lastLogin", guestLoginType);

        // 게스트 로그인 버튼 가리기 
        ShowAppleGuestLoginButton(false);
        
        // AppleID 로그인 버튼 가리기 
        ShowAppleIDLoginButton(false);

        // 콜백 전달 
        callback?.Invoke(_loginGuestResult);
    }
    
    private void AppleGuestLogin()
    {
        _loginGuestResult = Backend.BMember.GuestLogin(DateTime.Now.ToLongDateString());
        
        if (_loginGuestResult.IsSuccess())
        {
            ULogger.Log($"게스트 로그인에 성공했습니다.");
        }
        else
        {
            ULogger.Log($"게스트 로그인에 실패했습니다. / {_loginGuestResult.GetErrorCode()} / {_loginGuestResult.GetStatusCode()} / {_loginGuestResult.GetMessage()}");
        }

        // 실패했으니 다시 게스트 버튼 활성화해서 보여줌 (재시도 유도)
        if (!_loginGuestResult.IsSuccess())
            ShowAppleGuestLoginButton(true);
    }

    private void ShowAppleGuestLoginButton(bool show, bool enable = true)
    {
        // 로딩 메시지 가리기 
        loadingStateText.gameObject.SetActive(!show);

        // 로그인 버튼 보여주기  
        gameAppleGuestLoginButton.SetActive(show);

        // 게스트 로그인 버튼 스프라이트 체인지
        gameAppleGuestLoginButton.GetComponent<UIButton>().Interactable = enable;
        gameAppleGuestLoginButton.GetComponent<ButtonSpriteSwapper>().Init(enable);
    }
    
    [PublicAPI]
    public void OnClickAppleGuestLogin()
    {
        // 버튼 켜놓되, 클릭못하게 함 
        ShowAppleGuestLoginButton(true, false);
        
        // 게스트 로그인을 시도했기 때문에, iOS로그인 버튼도 클릭 못하게 함.
        ShowAppleIDLoginButton(true, false);

        // 로그인 진행
        AppleGuestLogin();
    }
}