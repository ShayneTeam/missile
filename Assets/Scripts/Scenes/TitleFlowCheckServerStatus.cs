﻿using System.Collections;
using System.Collections.Generic;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{
    private bool successServerCheck;

    private IEnumerator<float> _CheckServerStatusFlow()
    {
        // 뒤끝 서버 체크 재귀 로직 
        CheckServerAndShowPopupRecursive();

        // 뒤끝 서버 체크 성공하기 전까지 무한 루프
        while (!successServerCheck) yield return Timing.WaitForOneFrame;    
    }

    private void CheckServerAndShowPopupRecursive()
    {
        var isSuccess = BackEndManager.Instance.ServerStatusCheck();

#if UNITY_EDITOR
        //isSuccess = false;  // TODO 강제 실패 테스트 
#endif

        if (isSuccess)
        {
            successServerCheck = true;
        }
        else
        {
            // 서버 접속 불가 팝업 (재귀) 
            YesOrNoPopup.Show("system_006", "system_007", "system_016", "system_017",
                CheckServerAndShowPopupRecursive, Application.Quit);
        }
    }
}