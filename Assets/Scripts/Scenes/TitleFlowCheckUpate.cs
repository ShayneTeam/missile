﻿using System.Collections;
using System.Collections.Generic;
using Doozy.Engine.UI;
#if UNITY_ANDROID
using Google.Play.AppUpdate;
using Google.Play.Common;
#endif
using MEC;
using Server;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{
    private bool successUpdateCheck;

    private IEnumerator<float> _CheckUpdateFlow()
    {
        // 뒤끝 서버 체크 재귀 로직 
        CheckUpdateRecursive();

        // 뒤끝 서버 체크 성공하기 전까지 무한 루프 
        while (!successUpdateCheck) yield return Timing.WaitForOneFrame;
    }

    private void CheckUpdateRecursive()
    {
        var isSuccess = BackEndManager.Instance.ServerVersionCheck();

#if UNITY_EDITOR
        // isSuccess = false;  // TODO 강제 실패 테스트 
#endif

        if (isSuccess)
        {
            successUpdateCheck = true;
        }
        else
        {
            // 업데이트 팝업 
            OkPopup.Show("system_008", "system_009", "system_018",
                UpdateApp);
        }
    }

    private void UpdateApp()
    {
        // StartCoroutine(CheckForUpdate());

        // one link
        Application.OpenURL("http://onelink.to/missilerpg2");
        Application.Quit();
    }

    IEnumerator CheckForUpdate()
    {
#if UNITY_ANDROID
        var appUpdateManager = new AppUpdateManager();

        var appUpdateInfoOperation = appUpdateManager.GetAppUpdateInfo();

        // Wait until the asynchronous operation completes.
        yield return appUpdateInfoOperation;

        if (appUpdateInfoOperation.IsSuccessful)
        {
            var appUpdateInfoResult = appUpdateInfoOperation.GetResult();

            ULogger.Log(
                $@"업데이트 존재! : {appUpdateInfoResult.AvailableVersionCode.ToString()} / 업데이트 나온 날짜 : {appUpdateInfoResult.ClientVersionStalenessDays.ToString()}");

            // Creates an AppUpdateOptions defining an immediate in-app
// update flow and its parameters.
            var appUpdateOptions = AppUpdateOptions.FlexibleAppUpdateOptions();

            var startUpdateRequest = appUpdateManager.StartUpdate(
                // The result returned by PlayAsyncOperation.GetResult().
                appUpdateInfoResult,
                // The AppUpdateOptions created defining the requested in-app update
                // and its parameters.
                appUpdateOptions);

            var popup = UIPopup.GetPopup("Ingame Update Popup");
            popup.Show();
            var ingameUpdatePopup = popup.GetComponent<IngameUpdatePopup>();

            while (!startUpdateRequest.IsDone)
            {
                ingameUpdatePopup.ProgressUpdate(startUpdateRequest.DownloadProgress, startUpdateRequest.BytesDownloaded);
                ULogger.Log($"download percent : {startUpdateRequest.DownloadProgress}");
                // For flexible flow,the user can continue to use the app while
                // the update downloads in the background. You can implement a
                // progress bar showing the download status during this time.
                yield return null;
            }

            var result = appUpdateManager.CompleteUpdate();
            yield return result;

            // Check AppUpdateInfo's UpdateAvailability, UpdatePriority,
            // IsUpdateTypeAllowed(), etc. and decide whether to ask the user
            // to start an in-app update.
        }
        else
        {
            // Log appUpdateInfoOperation.Error.
        }
#endif
        yield break;
    }
}