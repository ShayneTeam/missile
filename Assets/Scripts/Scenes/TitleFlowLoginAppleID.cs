﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Text;
using AppleAuth;
using AppleAuth.Enums;
using AppleAuth.Extensions;
using AppleAuth.Interfaces;
using AppleAuth.Native;
using BackEnd;
using BestHTTP;
using BestHTTP.Forms;
using Cysharp.Threading.Tasks;
using Doozy.Engine.UI;
using Global;
using Global.Extensions;
using InGame.Data;
using JetBrains.Annotations;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UIElements;
using Utils;

public partial class TitleFlow : MonoBehaviour
{
    [Header("APPLE ID 로그인")] [SerializeField]
    private GameObject gameAppleIDLoginButton;

    private bool _successLoginAppleID;
    private BackendReturnObject _loginAppleResult;

    private IAppleAuthManager _appleAuthManager;
    private const string AppleUserIdKey = "AppleUserId";

    private const string appleLoginType = "AppleID";

    private void Awake()
    {
        if (AppleAuthManager.IsCurrentPlatformSupported)
        {
            var deserializer = new PayloadDeserializer();
            _appleAuthManager = new AppleAuthManager(deserializer);
        }
    }

    private void Update()
    {
        if (_appleAuthManager != null)
            _appleAuthManager.Update();
    }

    private IEnumerator<float> _LoginAppleIDFlow(Action<BackendReturnObject> callback)
    {
        // 첫 실행인지 체크 
        var lastLoginType = PlayerPrefs.GetString("lastLogin", string.Empty);
        if (string.IsNullOrEmpty(lastLoginType))
        {
            // 첫 실행이라면, 애플ID 로그인 버튼 눌러서 진행
            ShowAppleIDLoginButton(true);
        }
        // 로그인 기록이 있을 시,
        else if(lastLoginType == appleLoginType)
        {
            // 자동 로그인 상태 메시지
            yield return Timing.WaitUntilDone(_ShowLoadingStateText("system_032").CancelWith(gameObject));

            // 애플ID 로그인 버튼 가리기
            ShowAppleIDLoginButton(false);

            // 자동 애플ID 로그인 진행
            // AppleIDLogin(); // slow Login

            LoginAppleIDToken(); // Token Login
            
            // 자동 애플ID 로그인 진행
            // AppleIDQuickLogin(); // fast Login

            // 마지막 토큰을 가지고 로그인
            // var LastCredentialToken = PlayerPrefs.GetString(AppleUserIdKey);
            // LogInFederationAppleId(LastCredentialToken);
        }

        // AppleID 응답이 오기 전까지 통과 못함 
        while (_loginAppleResult == null)
            yield return Timing.WaitForOneFrame;

        // 최근 로그인 AppleID로 설정
        PlayerPrefs.SetString("lastLogin", appleLoginType);

        // AppleID 로그인 버튼 가리기 
        ShowAppleIDLoginButton(false);
        
        // 게스트 로그인 버튼 가리기 
        ShowAppleGuestLoginButton(false);

        // 콜백 전달 
        callback?.Invoke(_loginAppleResult);
        
        // 우리 서버 토큰 로그인 시도
        LoginAppleWithAccessToken().Forget();
    }
    
    private void LoginAppleIDToken()
    {            
        var loginToken = Backend.BMember.LoginWithTheBackendToken();
        if (loginToken.IsSuccess())
        {
            // 토큰 로그인 성공 시, 종료
            _loginAppleResult = loginToken;
            return;
        }
        
        // 실패 시, 플랫폼 로그인 태움 
        AppleIDLogin();
    }


    private void ShowAppleIDLoginButton(bool show, bool enable = true)
    {
        // 로딩 메시지 가리기 
        loadingStateText.gameObject.SetActive(!show);

        // 로그인 버튼 보여주기  
        gameAppleIDLoginButton.SetActive(show);

        // AppleID 로그인 버튼 스프라이트 체인지
        gameAppleIDLoginButton.GetComponent<UIButton>().Interactable = enable;
        gameAppleIDLoginButton.GetComponent<ButtonSpriteSwapper>().Init(enable);
    }

    [PublicAPI]
    public void OnClickAppleIDLogin()
    {
        // 버튼 켜놓되, 클릭못하게 함 
        ShowAppleIDLoginButton(true, false);
        
        // iOS 로그인을 시도했기 때문에, Guest 로그인 버튼도 클릭 못하게 함.
        ShowAppleGuestLoginButton(true, false);

        // 로그인 진행
        AppleIDLogin();
    }

    private void AppleIDLogin()
    {
        var loginArgs = new AppleAuthLoginArgs(LoginOptions.IncludeEmail | LoginOptions.IncludeFullName);

        // 초기화가 되지 않았을때에 대한 예외처리.
        if (_appleAuthManager == null)
        {
            // 다시 보여줌
            ShowAppleIDLoginButton(true);
            return;
        }

        _appleAuthManager.LoginWithAppleId(
            loginArgs,
            credential =>
            {
                var appleIdCredential = credential as IAppleIDCredential;
                var identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken);
                ULogger.Log($"APPLE ID LOGIN Success! token : {identityToken}");
                PlayerPrefs.SetString(AppleUserIdKey, identityToken);

                LogInFederationAppleId(identityToken);
            },
            error =>
            {
                // 다시 보여줌
                ShowAppleIDLoginButton(true);
                var authorizationErrorCode = error.GetAuthorizationErrorCode();
                ULogger.LogWarning("Sign in with Apple failed " + authorizationErrorCode + " " + error);
            });
    }

    private void AppleIDQuickLogin()
    {
        var quickLoginArgs = new AppleAuthQuickLoginArgs();

        // Quick login should succeed if the credential was authorized before and not revoked
        _appleAuthManager.QuickLogin(
            quickLoginArgs,
            credential =>
            {
                // If it's an Apple credential, save the user ID, for later logins
                var appleIdCredential = credential as IAppleIDCredential;
                var identityToken = Encoding.UTF8.GetString(appleIdCredential.IdentityToken);
                ULogger.Log("APPLE ID QUICK LOGIN Success!");
                PlayerPrefs.SetString(AppleUserIdKey, credential.User);
                LogInFederationAppleId(identityToken);
            },
            error =>
            {
                // 다시 보여줌
                ShowAppleIDLoginButton(true);
                // If Quick Login fails, we should show the normal sign in with apple menu, to allow for a normal Sign In with apple
                var authorizationErrorCode = error.GetAuthorizationErrorCode();
                ULogger.LogWarning("Quick Login Failed " + authorizationErrorCode.ToString() + " " + error.ToString());
            });
    }

    private void LogInFederationAppleId(string token)
    {
        if (string.IsNullOrWhiteSpace(token))
        {
            ULogger.Log("애플 토큰이 넘어오지 않아서, 로그인 버튼을 다시 활성화 합니다.");
            ShowAppleIDLoginButton(true);
            return;
        }

        ULogger.Log($@"애플 Token : {token} 으로 로그인을 시도합니다.");
        _loginAppleResult = Backend.BMember.AuthorizeFederation(token, FederationType.Apple, "APPLEID");

        // 실패했으니 다시 애플ID 로그인 버튼 활성화해서 보여줌 (재시도 유도)
        if (!_loginAppleResult.IsSuccess())
        {
            ShowAppleIDLoginButton(true);
            ULogger.Log(
                $@"애플ID 로그인 실패 / {_loginAppleResult.GetErrorCode()} / {_loginAppleResult.GetStatusCode()} / {_loginAppleResult.GetMessage()} / {_loginGoogleResult.GetFlattenJSON()}");
        }

        var isDevelopmentType = ExtensionMethods.IsDev() ? isDevelopment.iosDev : isDevelopment.iosProd;
        var registerPush = Backend.iOS.PutDeviceToken(isDevelopmentType);
        if (registerPush.IsSuccess())
        {
            ULogger.Log("dev 토큰 푸쉬 등록에 성공했습니다.");
        }
        else
        {
            ULogger.Log(
                $@"DEV 토큰 푸쉬 등록 실패 실패 / {registerPush.GetErrorCode()} / {registerPush.GetStatusCode()} / {registerPush.GetMessage()} / {registerPush.GetFlattenJSON()}");
        }
    }
    
    
    #region Our Server

    private async UniTaskVoid LoginAppleWithAccessToken()
    {
        var accessToken = PlayerPrefs.GetString("access_token", string.Empty);

        // 토큰 저장 안 돼있다면
        if (string.IsNullOrEmpty(accessToken))
        {
            // 애플 로그인 태움 
            LoginAppleForOurServer();
            return;
        }

        // 요청 
        var req = new HTTPRequest(ServerUri.loginAccountByToken, HTTPMethods.Post);
        req.AddField("token", accessToken);
        var rsp = await req.GetHTTPResponseAsync();

        if (rsp.IsSuccess)
        {
            // 공통 우리 서버 처리 
            CommonProcessOurServer(rsp);
        }
        else
        {
            // 실패 시, 플랫폼 로그인 태움 
            LoginAppleForOurServer();
        }
    }

    private void LoginAppleForOurServer()
    {
        var token = PlayerPrefs.GetString(AppleUserIdKey, string.Empty);
        if (string.IsNullOrEmpty(token)) return;

        LogInFederationAppleForOurServer(token).Forget();
    }

    private async UniTaskVoid LogInFederationAppleForOurServer(string token)
    {
        var req = new HTTPRequest(ServerUri.authorizeFederation, HTTPMethods.Post);
        req.AddField("platform_type", "IOS");
        req.AddField("token", token);
        req.AddField("etc", "APPLEID");
        AddCommonLoginFields(ref req);
        // 토큰 field가 너무 길어 MultiPart로 설정되는 걸 방지 
        req.FormUsage = HTTPFormUsage.UrlEncoded;
        var rsp = await req.GetHTTPResponseAsync();

        if (rsp.IsSuccess)
        {
            ULogger.Log($"애플 페러데이션 인증에 성공했습니다. 토큰 : {token}");

            // 공통 우리 서버 처리 
            CommonProcessOurServer(rsp);
        }
        else
        {
            ULogger.Log($"애플 페러데이션 인증에 실패했습니다. 토큰 : {token} / {rsp.StatusCode.ToLookUpString()} / {rsp.Message}");
        }
    }

    #endregion
}