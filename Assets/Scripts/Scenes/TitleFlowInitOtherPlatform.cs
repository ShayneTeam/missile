﻿using System.Collections;
using System.Collections.Generic;
using BackEnd;
using Firebase.Analytics;
using Firebase.Crashlytics;
using Global;
using Global.Extensions;
using InGame.Controller;
using InGame.Data;
using InGame.Global;
using MEC;
using Sentry;
using Server;
using TapjoyUnity;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{

    private IEnumerator<float> _InitOtherPlatformFlow()
    {
        var backEndUserInDate = Backend.UserInDate;
        // 탭조이 유저 ID 설정
        Tapjoy.SetUserID(backEndUserInDate);
        
        // 델리게이트 설정 
        AdsMediator.Instance.CanAdRemove = () => UserData.My.UserInfo.GetFlag(UserFlags.CanAdRemove);

        // 테이블에 작성된 모든 상품 ID 정리 
        var allProduct = new List<string> { "dev.purchase_test.1" };    // 테스트용 productId
        var allIdxes = DataboxController.GetAllIdxes(Table.Shop, Sheet.iap);

        // 플랫폼에 따라 상품 패키지 초기화.
        var columnKey = ExtensionMethods.IsIOS() ? Column.ios : Column.google;
        foreach (var idx in allIdxes)
        {
            var google = DataboxController.GetDataString(Table.Shop, Sheet.iap, idx, columnKey);
            allProduct.Add(google);
        }
        
        // 영수증 검증 델리게이트. 뒤끝 로그인 돼있어야 함 
        UnityPurchaser.Instance.ValidateReceipt = BackEndManager.ValidateReceipt;
        
        // 뒤끝 임시공지로 원격 인앱 펜딩 테스트  
        UnityPurchaser.Instance.IsForcePendingMode = () => BackEndManager.Instance.ContainsContentsTempNotice("ForcePendingIAP");
        
        // 센트리 비활성 
        if (!FirebaseRemoteConfigController.enable_sentry_backend) SentrySdk.Close();
        
        // IAP 초기화. 미처리 영수증 날아옴
        UnityPurchaser.Instance.Initialize(allProduct,
            (success, productId) =>
            {
                if (!success)
                    return;
                
                // 미처리 영수증 처리 
                ShopController.RestoreProduct(productId);
            });
        
        yield break;
    }
}