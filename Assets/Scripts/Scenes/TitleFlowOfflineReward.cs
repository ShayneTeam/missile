﻿using System.Collections;
using System.Collections.Generic;
using Databox.Dictionary;
using Doozy.Engine.UI;
using Firebase.Analytics;
using InGame;
using InGame.Controller;
using InGame.Data;
using MEC;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{
    private bool _isDoneOfflineReward;

    private OrderedDictionary<string, int> _rewardBoxes;
    private double _rewardMineral;

    private IEnumerator<float> _OfflineRewardFlow()
    {
        // 오프라인 보상 획득 가능할 때만 획득 과정 진행
        if (OfflineRewardController.CanEarnRewards())
        {
            // 오프라인 보상 박스 가져오기 
            OfflineRewardController.GetRewards(out _rewardMineral, out _rewardBoxes);

            // 팝업 띄우기 
            OfflineRewardPopup.Show(_rewardMineral, _rewardBoxes, Earn, EarnAds);

            // 보상처리될 때까지 대기 
            while (!_isDoneOfflineReward) yield return Timing.WaitForOneFrame;
        }

        // 오프라인 보상 획득 후 부터 플레이 타임스탬프 기록 시작 (보상 획득하지 않았다면, 재접속 때 다시 오프라인 보상 팝업 보여줘야 하기 때문)
        OfflineRewardController.Instance.RegisterAutoStampLastPlayedTime();
    }

    private void Earn()
    {
        WebLog.Instance.AllLog("offline_earn", new Dictionary<string, object>{{"reward_mineral",_rewardMineral}});

        EarnRewards();
    }

    private void EarnAds()
    {
        WebLog.Instance.AllLog("offline_ads_earn", new Dictionary<string, object>{{"reward_mineral",_rewardMineral}});

        EarnRewards(2);
    }

    private void EarnRewards(int scale = 1)
    {
        // 미네랄 
        UserData.My.Resources.mineral += _rewardMineral * scale;
        
        // 박스 
        foreach (var box in _rewardBoxes)
        {
            BoxController.AddBox(box.Key, box.Value * scale);
        }
        
        // 오프라인 보상 플로우 종료 
        _isDoneOfflineReward = true;
    }
}