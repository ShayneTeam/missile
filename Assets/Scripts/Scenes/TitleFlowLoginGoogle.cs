﻿using System;
using System.Collections;
using System.Collections.Generic;
using BackEnd;
using BestHTTP;
using BestHTTP.Forms;
using Cysharp.Threading.Tasks;
using Doozy.Engine.UI;
using Global;
using Global.Extensions;
using InGame.Data;
using JetBrains.Annotations;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;
using UnityEngine.UIElements;
using Utils;

public partial class TitleFlow : MonoBehaviour
{
    [Header("로그인")] [SerializeField] private GameObject googleLoginButton;

    private bool _successLoginGoogle;
    private BackendReturnObject _loginGoogleResult;
    

    private IEnumerator<float> _LoginGoogleFlow(Action<BackendReturnObject> callback)
    {
#if UNITY_ANDROID
        // 구글 로그인 버튼 눌리기 전, 미리 Activate 시켜놓는 것 중요 
        // 또한, Init이 여러 번 호출되서도 안 됨 (구글 로그인 프리징의 원인)
        PlayGamesController.Init();
#endif

        // 첫 실행인지 체크 
        var lastLoginType = PlayerPrefs.GetString("lastLogin", string.Empty);
        
        // 뒤끝 5.7 대응용, SDK 업데이트 했기때문에, PlayerPrefs를 하나 더 써서 이 값이 0이라면 무조건 한번 더 구글로그인을 거치도록 함.
        var backEndSDKLoginIssueChecker = PlayerPrefs.GetInt("backEndSDKLoginIssueChecker", 0);
        
        if (string.IsNullOrEmpty(lastLoginType) || backEndSDKLoginIssueChecker == 0)
        {
            // 첫 실행이라면, 구글 로그인 버튼 눌러서 진행     
            ShowGoogleLoginButton(true);
        }
        // 로그인 기록이 있을 시,
        else
        {
            // 자동 로그인 상태 메시지
            yield return Timing.WaitUntilDone(_ShowLoadingStateText("system_032").CancelWith(gameObject));

            // 구글 로그인 버튼 가리기 
            ShowGoogleLoginButton(false);

            // 자동 구글 로그인 진행
            LoginGPGSToken();
        }

        // 구글 로그인 응답이 오기 전까지 통과 못함 
        while (_loginGoogleResult == null)
            yield return Timing.WaitForOneFrame;

        // 최근 로그인 구글로 설정 
        PlayerPrefs.SetString("lastLogin", "Google");
        
        // 뒤끝 5.7 이슈 대응용, 구글로그인을 1회 했다고 가정하고 PlayerPrefs 에 저장.
        PlayerPrefs.SetInt("backEndSDKLoginIssueChecker", 1);

        // 구글 로그인 버튼 가리기 
        ShowGoogleLoginButton(false);

        // 콜백 전달 
        callback?.Invoke(_loginGoogleResult);
        
        // 우리 서버 토큰 로그인 시도
        LoginWithAccessToken().Forget();
    }

    private void ShowGoogleLoginButton(bool show, bool enable = true)
    {
        // 로딩 메시지 가리기 
        loadingStateText.gameObject.SetActive(!show);

        // 로그인 버튼 보여주기  
        googleLoginButton.SetActive(show);

        // 구글 로그인 버튼 스프라이트 체인지
        googleLoginButton.GetComponent<UIButton>().Interactable = enable;
        googleLoginButton.GetComponent<ButtonSpriteSwapper>().Init(enable);
    }

    [PublicAPI]
    public void OnClickGoogleLogin()
    {
        // 버튼 켜놓되, 클릭못하게 함 
        ShowGoogleLoginButton(true, false);

        // 로그인 진행
        LoginGoogle();
    }

    private void LoginGPGSToken()
    {
        // 토큰 로그인 시도 
        var loginToken = Backend.BMember.LoginWithTheBackendToken();
        if (loginToken.IsSuccess())
        {
            // 토큰 로그인 성공 시, 종료
            _loginGoogleResult = loginToken;
            return;
        }
        
        // 실패 시, 플랫폼 로그인 태움 
        LoginGoogle();
    }

    private void LoginGoogle()
    {
#if UNITY_ANDROID
        // 구글 로그인 진행
        if (PlayGamesController.Authenticated)
        {
            var token = PlayGamesController.GetToken();
            LogInFederationGoogle(token);
        }
        else
        {
            PlayGamesController.Authenticate((success, token) =>
            {
                if (success)
                    LogInFederationGoogle(token);
                // 실패했으니 다시 구글 버튼 활성화해서 보여줌 (재시도 유도)
                else 
                    ShowGoogleLoginButton(true);
            });
        }
#endif
    }

    private void LogInFederationGoogle(string token)
    {
        _loginGoogleResult =
            Backend.BMember.AuthorizeFederation(token, FederationType.Google, "GPGS").CommonErrorCheck();
        
        if (_loginGoogleResult.IsSuccess())
        {
            ULogger.Log($"구글 페러데이션 인증에 성공했습니다. 토큰 : {token}");
        }
        else
        {
            ULogger.Log($"구글 페러데이션 인증에 실패했습니다. 토큰 : {token} / {_loginGoogleResult.GetErrorCode()} / {_loginGoogleResult.GetStatusCode()} / {_loginGoogleResult.GetMessage()}");
        }

        // 실패했으니 다시 구글 버튼 활성화해서 보여줌 (재시도 유도)
        if (!_loginGoogleResult.IsSuccess())
            ShowGoogleLoginButton(true);
    }

    #region Our Server

    private async UniTaskVoid LoginWithAccessToken()
    {
        var accessToken = PlayerPrefs.GetString("access_token", string.Empty);

        // 토큰 저장 안 돼있다면
        if (string.IsNullOrEmpty(accessToken))
        {
            // 구글 로그인 태움 
            LoginGoogleForOurServer();
            return;
        }

        // 요청 
        var req = new HTTPRequest(ServerUri.loginAccountByToken, HTTPMethods.Post);
        req.AddField("token", accessToken);
        var rsp = await req.GetHTTPResponseAsync();

        if (rsp.IsSuccess)
        {
            // 공통 우리 서버 처리 
            CommonProcessOurServer(rsp);
        }
        else
        {
            // 실패 시, 플랫폼 로그인 태움 
            LoginGoogleForOurServer();
        }
    }

    private void LoginGoogleForOurServer()
    {
#if UNITY_ANDROID
        // 구글 로그인 진행
        // (우리서버 구글 로그인은 별개의 스레드로 돌릴꺼라 직접 구글 로그인을 요청하지 않을꺼임
        // 이미 구글 로그인이 성공한 상태로 들어와야만 함)
        if (!PlayGamesController.Authenticated) return;
        
        var token = PlayGamesController.GetToken();
        LogInFederationGoogleForOurServer(token).Forget();
#endif
    }

    private async UniTaskVoid LogInFederationGoogleForOurServer(string token)
    {
        var req = new HTTPRequest(ServerUri.authorizeFederation, HTTPMethods.Post);
        req.AddField("platform_type", "Android");
        req.AddField("token", token);
        req.AddField("etc", "GPGS");
        AddCommonLoginFields(ref req);
        // 토큰 field가 너무 길어 MultiPart로 설정되는 걸 방지 
        req.FormUsage = HTTPFormUsage.UrlEncoded;
        var rsp = await req.GetHTTPResponseAsync();

        if (rsp.IsSuccess)
        {
            ULogger.Log($"LogInFederationGoogleForOurServer 성공. 토큰 : {token}");

            // 공통 우리 서버 처리 
            CommonProcessOurServer(rsp);
        }
        else
        {
            ULogger.Log($"LogInFederationGoogleForOurServer 실패. 토큰 : {token} / {rsp.StatusCode.ToLookUpString()} / {rsp.Message}");
        }
    }

    #endregion
}