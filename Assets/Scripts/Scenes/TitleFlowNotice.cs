﻿using System.Collections;
using System.Collections.Generic;
using BackEnd;
using Doozy.Engine.UI;
using Global.Extensions;
using InGame.Data;
using LitJson;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{

    private IEnumerator<float> _NoticeFlow()
    {
        BackEndManager.Instance.GetTempNotice();
        
        /*
        // 차트 리스트 받기
        var mBackendChartList = Backend.Chart.GetChartList();
        for (var i = 0; i < mBackendChartList.Rows().Count; i++)
        {
            var curRows = mBackendChartList.FlattenRows()[i];

            // selectedChartFileId N => 차트에 지정한 데이터가 있다. (없는경우 True N이 아닌 null 로 넘어옴.
            if (curRows["selectedChartFileId"] != null)
            {
                var chartName = curRows["chartName"].StringValue();
                // 버전 용도의 꼼수로 사용하여 받으려고 함. => 반드시 있어야 하고, 업데이트마다 반드시 달라져야 함.
                var chartExplain = curRows["chartExplain"].StringValue(); 
                var selectedChartFileId = curRows["selectedChartFileId"].LongValue();
                var loadLocalVersion = PlayerPrefs.GetString(chartName, string.Empty);

                // 서버와 설명(버전) 이 다름 = 받아야 함. / Editor 의 경우에도 받아야 함.
                if (loadLocalVersion != chartExplain || ExtensionMethods.IsUnityEditor())
                {
                    // 로컬에 저장
                    Backend.Chart.GetOneChartAndSave(selectedChartFileId.ToString(), chartName).GetReturnValuetoJSON();
                    PlayerPrefs.SetString(chartName, chartExplain);
                }
            }
        }

        // 공지사항 차트 불러옴
        var mLocalNoticeString = Backend.Chart.GetLocalChartData("Notice");
        if (!string.IsNullOrWhiteSpace(mLocalNoticeString))
        {
            var mLocalNoticeList = BackendReturnObject.Flatten(JsonMapper.ToObject(mLocalNoticeString)["rows"]);
            for (var i = 0; i < mLocalNoticeList.Count; i++)
            {
                var curNotice = mLocalNoticeList[i];
        
                var mBackEndNoticeClass = new ServerNoticeSchema
                {
                    country = curNotice["country"].StringValue(),
                    startTime = curNotice["startTime"].StringValue(),
                    endTime = curNotice["endTime"].StringValue(),
                    title = curNotice["title"].StringValue(),
                    context = curNotice["context"].StringValue(),
                    btn1Name = curNotice["btn1Name"].NullableStringValue(),
                    btn1Url = curNotice["btn1Url"].NullableStringValue(),
                    btn2Name = curNotice["btn2Name"].NullableStringValue(),
                    btn2Url = curNotice["btn2Url"].NullableStringValue(),
                };
            }
        }
*/
        yield return Timing.WaitForOneFrame;
    }
}