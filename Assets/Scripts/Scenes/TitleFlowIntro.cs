﻿using System.Collections;
using System.Collections.Generic;
using BackEnd;
using Global.Extensions;
using InGame.Data;
using JetBrains.Annotations;
using MEC;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{
	[Header("인트로")] 
	[SerializeField] private GameObject introImagesParent;
    [SerializeField] private List<GameObject> introCutImages;
				
    [SerializeField] private float eachImageDurationSec = 4f;
    [SerializeField] private float lastImageDurationSec = 10f;
				
				
    private CoroutineHandle _waiting;
    private int _currentImageIndex;
				
    private bool _isFinishedIntro;
				
    private IEnumerator<float> _IntroFlow()
    {
	    var isWatchedIntro = UserData.My.UserInfo.GetFlag(UserFlags.IsWatchedIntro);
	    if (ExtensionMethods.IsUnityEditor())
	    {
		    //isWatchedIntro = false;   // TODO. 강제 인트로 테스트
	    } 
	    // 이미 인트로 봤다면 스킵 
	    if (isWatchedIntro)
			yield break; 
	    
	    // 각 이미지는 꺼두기 
	    foreach (var image in introCutImages)
	    {
		    image.SetActive(false);
	    }

	    // 시작 
	    introImagesParent.SetActive(true);

	    // 한장씩 보여줌 
	    _waiting = Timing.RunCoroutine(_ShowCutImageRecursive(0).CancelWith(gameObject));

	    // 인트로 끝날 때까지 대기 
	    while (!_isFinishedIntro) yield return Timing.WaitForOneFrame;

	    // 인트로 재생 여부 저장 
	    UserData.My.UserInfo.SetFlag(UserFlags.IsWatchedIntro, true);
    }

    private IEnumerator<float> _ShowCutImageRecursive(int index)
    {
	    if (index < 0 || index >= introCutImages.Count)
		    yield break;

	    // 이미지 인덱스 저장 
	    _currentImageIndex = index;

	    // 이미지 보여주기 
	    introCutImages[index].SetActive(true);

	    // 마지막 이미지
	    if (index == introCutImages.Count - 1)
	    {
		    yield return Timing.WaitForSeconds(lastImageDurationSec);

		    // 인트로 종료 
		    _isFinishedIntro = true;
	    }
	    // 그외의 이미지 
	    else
	    {
		    yield return Timing.WaitForSeconds(eachImageDurationSec);

		    // 다음 이미지 
		    _waiting = Timing.RunCoroutine(_ShowCutImageRecursive(index + 1).CancelWith(gameObject));
	    }
    }
				
    [PublicAPI]
    public void OnClickIntroImage()
    {
	    // 기다리는걸 강제 스탑 
	    if (_waiting.IsRunning)
		    Timing.KillCoroutines(_waiting);

	    // 마지막 이미지 보여주고 있었다면 
	    if (_currentImageIndex == introCutImages.Count - 1)
	    {
		    // 인트로 종료
		    _isFinishedIntro = true;
	    }
	    // 그외의 이미지 
	    else
	    {
		    // 다음 이미지 바로 보여줌 
		    _waiting = Timing.RunCoroutine(_ShowCutImageRecursive(_currentImageIndex + 1).CancelWith(gameObject));
	    }
    }
}