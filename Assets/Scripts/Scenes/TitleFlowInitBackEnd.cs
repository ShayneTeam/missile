﻿using System.Collections;
using System.Collections.Generic;
using Global.Extensions;
using MEC;
using Server;
using UI.Popup;
using UnityEngine;

public partial class TitleFlow : MonoBehaviour
{
    private bool successInitBackend;

    private IEnumerator<float> _InitBackEndFlow()
    {
        if (BackEndManager.Instance.IsInitialized) yield break;

        // 뒤끝 초기화 재귀 로직 
        InitBackEndAndShowPopupRecursive();

        // 뒤끝 초기화 성공하기 전까지 무한 루프 
        while (!successInitBackend) yield return Timing.WaitForOneFrame;
    }

    private void InitBackEndAndShowPopupRecursive()
    {
        var backEndInit = BackEndManager.Instance.Initialize().CommonErrorCheck();
        var isSuccess = backEndInit.IsSuccess();

#if UNITY_EDITOR
        //isSuccess = false;  // TODO 강제 연결 실패 테스트 
#endif

        if (isSuccess)
        {
            ULogger.Log($@"뒤끝 초기화 성공");

            successInitBackend = true;
        }
        else
        {
            ULogger.Log($@"뒤끝 초기화 실패 / Errcode : {backEndInit.GetErrorCode()} / Msg : {backEndInit.GetMessage()} / StatusCode : {backEndInit.GetStatusCode()}");

            // 서버 접속 불가 팝업 (재귀) 
            YesOrNoPopup.Show("system_004", "system_005", "system_016", "system_017",
                InitBackEndAndShowPopupRecursive, Application.Quit);
        }
    }
}