#if UNITY_PURCHASING

using System;
using System.Collections.Generic;
using Global.Extensions;
using UnityEngine;
using UnityEngine.Purchasing;
using UnityEngine.Purchasing.Security;
using UnityEngine.UDP;

// ReSharper disable InconsistentNaming


namespace Global
{
    public class UnityPurchaser : MonoBehaviourSingleton<UnityPurchaser>, IStoreListener, IPurchaseListener
    {
        // Unity IAP objects
        private IStoreController _controller;

        private ITransactionHistoryExtensions transactionHistoryExtensions;
        private IGooglePlayStoreExtensions googlePlayStoreExtensions;

        private bool isGooglePlayStoreSelected;


        // 구매 콜백
        private Action<bool, string> callbackPurchase = delegate { };

        // 위임 함수 
        public ValidateReceiptDelegate ValidateReceipt = (receipt) => false;
        public IsForcePendingModeDelegate IsForcePendingMode = () => false;
        
        // 초기화 여부 
        private bool IsInitialized => _controller != null;

        // A purchase was already in progress when a new purchase was requested.
        // This is currently unique to Google Play.
        // Retry the new purchase after receiving a callback from either IStoreListener.ProcessPurchase. or IStoreListener.OnPurchaseFailed.
        public static bool IsExistingPurchasePending
        {
            get => Instance._isExistingPurchasePending;
            private set => Instance._isExistingPurchasePending = value;
        }

        private bool _isExistingPurchasePending;
        
        public void Initialize(List<string> productIds, Action<bool, string> callbackRestore = null)
        {
            if (IsInitialized)
                return;
            
            ULogger.Log("UnityIAPManager Initialize");

            var module = StandardPurchasingModule.Instance();
            var builder = ConfigurationBuilder.Instance(module);
            
            // UDP 이면, UDP 초기화
            if (module.appStore == AppStore.UDP)
            {
                // UDP 초기화
                StoreService.Initialize(new InitListener());
            }
            
            // 에디터일 시, 페이크 스토어 모드 
            if (ExtensionMethods.IsUnityEditor())
                module.useFakeStoreUIMode = FakeStoreUIMode.StandardUser;

            // 콜백 스위칭 
            callbackPurchase = callbackRestore;

            //
            isGooglePlayStoreSelected =
                Application.platform == RuntimePlatform.Android && module.appStore == AppStore.GooglePlay;
            
            ULogger.Log($@"결제모듈 초기화 완료! 현재 결제모듈 : {module.appStore}");

            // Product 초기화 
            foreach (var productId in productIds)
            {
                builder.AddProduct(productId, ProductType.Consumable,
                    new IDs { { productId, GooglePlay.Name }, { productId, AppleAppStore.Name } });
            }
            
            UnityPurchasing.Initialize(this, builder);

        }

        public static void Purchase(string productDefId, Action<bool, string> callback)
        {
            Instance.InternalPurchase(productDefId, callback);
        }

        private void InternalPurchase(string productDefId, Action<bool, string> callback)
        {
            // 컨트롤러 체크 
            if (!IsInitialized)
            {
                ULogger.LogError("Purchasing is not initialized");
                callback(false, productDefId);
                return;
            }

            // Product 가져옴
            var product = _controller.products.WithID(productDefId);
            if (product == null)
            {
                ULogger.LogError("product id is invalid" + productDefId);
                callback(false, productDefId);
                return;
            }

            // 콜백 스위칭 
            callbackPurchase = callback;
    
            // 구매 API 호출
            // IAP 3.0부터 전달된 페이로드는 적용되지 않음 (최종 영수증에 표시되지 않음)
            IsExistingPurchasePending = true;
            
            var module = StandardPurchasingModule.Instance();
            switch (module.appStore)
            {
                case AppStore.fake:
                case AppStore.GooglePlay:
                case AppStore.AppleAppStore:
                    _controller.InitiatePurchase(product, "payload is obsoleted");
                    break;
                case AppStore.UDP:
                    StoreService.Purchase(productDefId, "developerPayload", this);
                    break;
                default:
                    throw new ArgumentOutOfRangeException();
            }
        }
        
        public void Restore()
        {
            // 구글플레이에서는 쓰지 않을 함수 
            // iOS에서는 수동 Restore 처리가 있어야 하므로 필요 
            // 안드로이드는 IAP 초기화시, 따로 Restore을 호출하지 않아도 자동으로 ProcessPurchased으로 넘어옴
            // 앱 크래시가 나서 처리 못했거나, 구독형 아이템을 다시 정상 처리 시키는 목적 
            if (isGooglePlayStoreSelected)
            {
                googlePlayStoreExtensions.RestoreTransactions(OnTransactionsRestored);
            }
        }

        private static void OnTransactionsRestored(bool success)
        {
            ULogger.Log("Transactions restored." + success);
        }


        #region IStoreListener

        public void OnInitialized(IStoreController controller, IExtensionProvider extensions)
        {
            ULogger.Log("OnInitialized");

            _controller = controller;
            transactionHistoryExtensions = extensions.GetExtension<ITransactionHistoryExtensions>();
            googlePlayStoreExtensions = extensions.GetExtension<IGooglePlayStoreExtensions>();

            // Pass an obfuscated account ID. 
            // IAP 3.0 부터 developerPayload 공식 중단됨 
            // 대신 SetObfuscatedAccountId를 사용하라고 함 
            // 결제 시, 영수증에 이 Id가 같이 들어가고, 영수증 반환 시 같이 반환된다고 함
            //googlePlayStoreExtensions.SetObfuscatedAccountId(obfuscatedAccountId);
            
            // Pass an obfuscated profile ID.
            // 만약 게임에서 "1계정 : 다중 캐릭터"가 가능할 때, 사용
            // "1계정 : 1 캐릭터" 게임이라면, SetObfuscatedAccountId 만으로 커버 가능 
            //googlePlayStoreExtensions.SetObfuscatedProfileId(obfuscatedProfileId);


            ULogger.Log("Available items:");
            foreach (var item in controller.products.all)
            {
                if (item.availableToPurchase)
                {
                    ULogger.Log(string.Join(" - ",
                        new[]
                        {
                            item.metadata.localizedTitle,
                            item.metadata.localizedDescription,
                            item.metadata.isoCurrencyCode,
                            item.metadata.localizedPrice.ToString(),
                            item.metadata.localizedPriceString,
                            item.transactionID,
                            item.receipt
                        }));
                }
            }

            // 모든 프로덕트 로그 찍기
            LogProductDefinitions();
        }

        public void OnInitializeFailed(InitializationFailureReason error)
        {
            ULogger.Log("Billing failed to initialize!");
            switch (error)
            {
                case InitializationFailureReason.AppNotKnown:
                    ULogger.LogError("Is your App correctly uploaded on the relevant publisher console?");
                    break;
                case InitializationFailureReason.PurchasingUnavailable:
                    // Ask the user if billing is disabled in device settings.
                    ULogger.Log("Billing disabled!");
                    break;
                case InitializationFailureReason.NoProductsAvailable:
                    // Developer configuration error; check product metadata.
                    ULogger.Log("No products available for purchase!");
                    break;
            }
        }
        
        public void OnPurchase(PurchaseInfo purchaseInfo)
        {
            // The purchase has succeeded.
            // If the purchased product is consumable, you should consume it here.
            // Otherwise, deliver the product.
            ULogger.Log($@"{purchaseInfo.ProductId} / OnPurchase");
            ULogger.Log("Purchase OK: " + purchaseInfo.ProductId);
            ULogger.Log("OrderQueryToken: " + purchaseInfo.OrderQueryToken);
            ULogger.Log("StorePurchaseJsonString: " + purchaseInfo.StorePurchaseJsonString);

            // 구매 도중 에러 시, 구매 처리 테스트  
            if (IsForcePendingMode())
                return;  // 펜딩 처리 
            
            // 강제 Pending 하는 게 아닌 한, 다른 구매 처리 할 수 있게 false 시킴 
            IsExistingPurchasePending = false;
            
            // 소모 처리
            StoreService.ConsumePurchase(purchaseInfo, this);
        }

        public void OnPurchaseRepeated(string productId)
        {
            // throw new NotImplementedException();
        }
        
        public void OnPurchaseFailed(string message, PurchaseInfo purchaseInfo)
        {
            IsExistingPurchasePending = false;
            callbackPurchase?.Invoke(false, purchaseInfo.ProductId);   // 결제 실패 콜백
        }


        public void OnPurchaseConsume(PurchaseInfo purchaseInfo)
        {
            callbackPurchase?.Invoke(true, purchaseInfo.ProductId);   // 결제 성공 콜백
        }

        public void OnPurchaseConsumeFailed(string message, PurchaseInfo purchaseInfo)
        {
            ULogger.Log("Purchase failed: " + purchaseInfo.ProductId);

            // Detailed debugging information
            ULogger.Log("Store specific error code: " + transactionHistoryExtensions.GetLastStoreSpecificPurchaseErrorCode());
            if (transactionHistoryExtensions.GetLastPurchaseFailureDescription() != null)
            {
                ULogger.Log("Purchase failure description message: " +
                            transactionHistoryExtensions.GetLastPurchaseFailureDescription().message);
            }

            IsExistingPurchasePending = false;
            callbackPurchase?.Invoke(false, purchaseInfo.ProductId);
        }

        public void OnQueryInventory(Inventory inventory)
        {
        }

        public void OnQueryInventoryFailed(string message)
        {
        }

        public PurchaseProcessingResult ProcessPurchase(PurchaseEventArgs e)
        {
            ULogger.Log("Purchase OK: " + e.purchasedProduct.definition.id);
            ULogger.Log("Receipt: " + e.purchasedProduct.receipt);

            // 구매 도중 에러 시, 구매 처리 테스트  
            if (IsForcePendingMode())
                return PurchaseProcessingResult.Pending;    // 펜딩 처리 
            
            // 강제 Pending 하는 게 아닌 한, 다른 구매 처리 할 수 있게 false 시킴 
            IsExistingPurchasePending = false;
          
            // 페이크 스토어 체크  
            if (GetStoreAndPayload(e.purchasedProduct.receipt, out var store, out _) && store == "fake")
            {
                // 결제 성공 콜백
                callbackPurchase?.Invoke(true, e.purchasedProduct.definition.id);

                return PurchaseProcessingResult.Complete;
            }
            
            // 영수증 검증 처리 위임 
            var validateReceipt = ValidateReceipt(e.purchasedProduct);
            if (validateReceipt)
            {
                callbackPurchase?.Invoke(true, e.purchasedProduct.definition.id);   // 결제 성공 콜백
            }
            else
            {
                callbackPurchase?.Invoke(false, e.purchasedProduct.definition.id);  // 결제 실패 콜백   
            }
            
            return PurchaseProcessingResult.Complete;
        }


        public void OnPurchaseFailed(Product item, PurchaseFailureReason r)
        {
            ULogger.Log("Purchase failed: " + item.definition.id);
        
            // Detailed debugging information
            ULogger.Log("Store specific error code: " + transactionHistoryExtensions.GetLastStoreSpecificPurchaseErrorCode());
            if (transactionHistoryExtensions.GetLastPurchaseFailureDescription() != null)
            {
                ULogger.Log("Purchase failure description message: " +
                          transactionHistoryExtensions.GetLastPurchaseFailureDescription().message);
            }
        
            IsExistingPurchasePending = false;
            callbackPurchase?.Invoke(false, item.definition.id);
        }

        #endregion


        private static bool GetStoreAndPayload(string receipt, out string store, out string payload)
        {
            var receipt_wrapper = (Dictionary<string, object>)MiniJson.JsonDecode(receipt);
            if (!receipt_wrapper.ContainsKey("Store") || !receipt_wrapper.ContainsKey("Payload"))
            {
                ULogger.Log("The product receipt does not contain enough information");

                store = payload = null;

                return false;
            }

            store = (string)receipt_wrapper["Store"];
            payload = (string)receipt_wrapper["Payload"];

            return true;
        }


        #region Product 체크

        private void LogProductDefinitions()
        {
            var products = _controller.products.all;
            foreach (var product in products)
            {
                ULogger.Log($"id: {product.definition.id}\nstore-specific id: {product.definition.storeSpecificId}\ntype: {product.definition.type.ToString()}\nenabled: {(product.definition.enabled ? "enabled" : "disabled")}\n");
            }
        }

        public string GetProductLocalizedPriceText(string productDefId)
        {
            if (_controller == null)
                return "";

            var product = _controller.products.WithID(productDefId);

            return product != null ? product.metadata.localizedPriceString : "";
        }

        public decimal GetProductLocalizedPrice(string productDefId)
        {
            if (_controller == null)
                return 0;

            var product = _controller.products.WithID(productDefId);

            return product != null ? product.metadata.localizedPrice : 0;
        }

        public string GetProductLocalizedTitle(string productDefId)
        {
            if (_controller == null)
                return "";

            var product = _controller.products.WithID(productDefId);

            return product != null ? product.metadata.localizedTitle : "";
        }

        public string GetProductCurrency(string productDefId)
        {
            if (_controller == null)
                return "";

            var product = _controller.products.WithID(productDefId);

            return product != null ? product.metadata.isoCurrencyCode : "";
        }

        public bool IsAvailableProduct(string productDefId)
        {
            if (_controller == null)
                return false;

            var product = _controller.products.WithID(productDefId);

            return product != null && product.availableToPurchase;
        }

        #endregion
    }
    
    public delegate bool ValidateReceiptDelegate(Product productInfo);
    public delegate bool IsForcePendingModeDelegate();
}

/* 테스트 결제 영수증 
 * Purchase data: {
 * "orderId":"GPA.3383-7395-9127-30794",
 * "packageName":"com.lparty101.test",
 * "productId":"doki.coin_4",
 * "purchaseTime":1564721252769,
 * "purchaseState":0,
 * "developerPayload":"{\"developerPayload\":\"eyJhY2NvdW50SWQiOiI2MzcwMDMxODA0OCIsImRldmVsb3BlclBheWxvYWQiOiI2MzcwMDMxODA0\\nOCJ9\\n\",\"is_free_trial\":false,\"has_introductory_price_trial\":false,\"is_updated\":false,\"accountId\":\"63700318048\"}",
 * "purchaseToken":"ebkoanboaekkacnonakhkgjb.AO-J1OxiGVZFS0N1yW6vgJ_kYxsTiE_9VKB3MqTnZ-xOeWpqzYKSqT4BGFDepQcjWHW8RYZirPFQaW4d4tn7m8x1JlzGntRSNgB-VjS_w5vvXbI8V43nmmU"
 * }
*/

#endif // UNITY_PURCHASING