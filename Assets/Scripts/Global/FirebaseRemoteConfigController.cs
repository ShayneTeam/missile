﻿

namespace Global
{
    public class FirebaseRemoteConfigController : MonoBehaviourSingletonPersistent<FirebaseRemoteConfigController>
    {
        public static long inAppReviewStage;
        public static bool isOnWeblog;
        public static float saveToServerDelay;
        public static float saveToServerDelayNightMode;
        public static ulong gcInterval;
        public static ulong gcBudgetInGame;
        public static ulong gcBudgetMenu;
        public static string webLogUrl;
        public static string gmNickNameKeyword;
        public static string gmChatMessageColor;
        
        public static bool isOnDailyChip;
        public static bool isOnCameraOrthographicSize;
        
        public static string raidUrl;
        
        public static bool tutorialShow;
        
        public static float saveToServerDelayForOurServer;
        public static string server_url;

        public static bool enable_sentry_backend;
    }
}