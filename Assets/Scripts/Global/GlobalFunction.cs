﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Numerics;
using System.Text;
using Doozy.Engine.UI;
using MEC;
using UnityEngine;
using UnityEngine.UI;
using Random = UnityEngine.Random;
using Vector2 = UnityEngine.Vector2;
using Vector3 = UnityEngine.Vector3;

public class GlobalFunction : MonoBehaviour
{
    public static AnimationClip GetAnimationClip(string clipName, Animator animator)
    {
        AnimationClip unknownAnimationClip = null;
        foreach (var animationClip in animator.runtimeAnimatorController.animationClips)
        {
            if (animationClip.name == clipName)
            {
                unknownAnimationClip = animationClip;

                break;
            }
        }

        return unknownAnimationClip;
    }

    public static AnimationClip GetAnimationClipCheckContains(string clipName, Animator animator)
    {
        AnimationClip unknownAnimationClip = null;
        foreach (var animationClip in animator.runtimeAnimatorController.animationClips)
        {
            if (animationClip.name.Contains(clipName))
            {
                unknownAnimationClip = animationClip;

                break;
            }
        }

        return unknownAnimationClip;
    }


    // 0~99 주사위 돌림
    // 주사위가 chance 안에 들면 true
    public static bool Roll(int chance)
    {
        return Random.Range(0, 100) < chance;
    }

    public static bool RollFloat(float chance)
    {
        return Random.Range(0.01f, 100f) <= chance;
    }

    public static void AlphaBlend(GameObject unit, float alpha)
    {
        // 투명화 
        foreach (var renderer in unit.GetComponentsInChildren<Renderer>())
        {
            foreach (var material in renderer.materials)
            {
                material.shader = Shader.Find("Unlit/UnlitAlphaWithFade");
                var colorStrength = (material.color.r + material.color.g + material.color.b) / 3f;
                material.color = new Color(material.color.r, material.color.g, material.color.b, alpha * colorStrength);
            }
        }
    }

    public static void ShuffleArray<T>(T[] array)
    {
        int random1;
        int random2;

        T tmp;

        for (var index = 0; index < array.Length; ++index)
        {
            random1 = Random.Range(0, array.Length);
            random2 = Random.Range(0, array.Length);

            tmp = array[random1];
            array[random1] = array[random2];
            array[random2] = tmp;
        }
    }


    public static void ShuffleList<T>(List<T> list)
    {
        int random1;
        int random2;

        T tmp;

        for (var index = 0; index < list.Count; ++index)
        {
            random1 = Random.Range(0, list.Count);
            random2 = Random.Range(0, list.Count);

            tmp = list[random1];
            list[random1] = list[random2];
            list[random2] = tmp;
        }
    }




    #region 주사위 확률 

    public static void AddRoll(ref List<CommonRoll> rollList, string name, int chance)
    {
        var newRoll = new CommonRoll {Name = name, Chance = chance};
        rollList.Add(newRoll);
    }

    public static CommonRoll Roll(ref List<CommonRoll> rollList)
    {
        CommonRoll rollChosen = null;

        // 총합 구하기 
        var sumChance = 0;
        foreach (var roll in rollList)
        { 
            sumChance += roll.Chance;
        }

        // 
        var randRoll = Random.Range(0, sumChance);

        var current = 0;
        for (var i = 0; i < rollList.Count; ++i)
        {
            current += rollList[i].Chance;

            if (randRoll < current)
            {
                rollChosen = rollList[i];

                break;
            }
        }

        return rollChosen;
    }

    #endregion


    #region 시간 관련

    public static string TimeSecToString(int sec)
    {
        var minute = sec / 60;
        var second = sec % 60;

        string minuteStr;
        if (minute >= 10)
        {
            minuteStr = minute.ToString();
        }
        else
        {
            minuteStr = "0" + minute;
        }

        string secondStr;
        if (second >= 10)
        {
            secondStr = second.ToString();
        }
        else
        {
            secondStr = "0" + second;
        }

        return $"{minuteStr}:{secondStr}";
    }
    
    public static string TimeSecToStringWithHour(int sec)
    {
        var hours = sec / 60 / 60;
        var minutes = sec / 60 % 60;
        var seconds = sec % 60;

        string hoursStr;
        if (hours >= 10)
            hoursStr = hours.ToString();
        else
            hoursStr = "0" + hours;
        
        string minuteStr;
        if (minutes >= 10)
            minuteStr = minutes.ToString();
        else
            minuteStr = "0" + minutes;

        string secondStr;
        if (seconds >= 10)
            secondStr = seconds.ToString();
        else
            secondStr = "0" + seconds;

        return $"{hoursStr}:{minuteStr}:{secondStr}";
    }
    
    public static DateTime ToDateTime(long timestamp)
    {
        return new DateTime(timestamp * TimeSpan.TicksPerSecond);
    }

    #endregion


    public static void ChangeLayersRecursively(Transform trans, string name)
    {
        trans.gameObject.layer = LayerMask.NameToLayer(name);
        foreach (Transform child in trans)
        {
            ChangeLayersRecursively(child, name);
        }
    }


    //
    public static void SyncTransformWorldToUI(Transform world, Transform ui, float distance)
    {
        var uiPos = ui.position;
        var newWorldPos = uiPos;

        newWorldPos.z += distance;
        world.position = newWorldPos;
    }
    
    private static readonly Dictionary<Canvas, RectTransform> _cachedRectTransforms = new Dictionary<Canvas, RectTransform>(); 

    public static Vector2 WorldToCanvas(Vector3 worldPos, Camera worldCamera, Canvas targetCanvas)
    {
        if (worldCamera == null || targetCanvas == null)
            return Vector2.zero;
        
        // World -> Viewport -> Canvas
        var viewportPos = worldCamera.WorldToViewportPoint(worldPos);
        if (!_cachedRectTransforms.TryGetValue(targetCanvas, out var canvasRect))
        {
            canvasRect = targetCanvas.GetComponent<RectTransform>();
            _cachedRectTransforms[targetCanvas] = canvasRect;
        }
        var sizeDelta = canvasRect.sizeDelta;
            
        return new Vector2(
            ((viewportPos.x * sizeDelta.x) - (sizeDelta.x * 0.5f)),
            ((viewportPos.y * sizeDelta.y) - (sizeDelta.y * 0.5f)));
    }

    public static Vector2 WorldToScreen(Vector3 worldPos, Camera worldCamera)
    {
        if (worldCamera == null)
            return Vector2.zero;
        
        // World -> Screen
        return worldCamera.WorldToScreenPoint(worldPos);
    }
}

public static class DoubleExtensions
{
    // double 타입은 0이 324개 붙을 수 있음. 한 단위당 0이 4개므로, /4 = 81개. 총 81개의 단위 문자 처리 가능 
    private static readonly string[] _unitAlphabet = {
        "", "A", "B", "C", "D", "E", "F", "G", "H", "I", "J", "K", "L", "M", "N", "O", "P", "Q", "R", "S", "T", "U", "V", "W", "X", "Y", "Z",
        "ZA", "ZB", "ZC", "ZD", "ZE", "ZF", "ZG", "ZH", "ZI", "ZJ", "ZK", "ZL", "ZM", "ZN", "ZO", "ZP", "ZQ", "ZR", "ZS", "ZT", "ZU", "ZV", "ZW", "ZX", "ZY", "ZZ",
        "ZZA", "ZZB", "ZZC", "ZZD", "ZZE", "ZZF", "ZZG", "ZZH", "ZZI", "ZZJ", "ZZK", "ZZL", "ZZM", "ZZN", "ZZO", "ZZP", "ZZQ", "ZZR", "ZZS", "ZZT", "ZZU", "ZZV", "ZZW", "ZZX", "ZZY", "ZZZ",
    };
    
    public static string ToUnitString(this double doubleNum)
    {
        // 단위 갯수 알아내기
        var unitCount = 0;
        var finalUnitNum = doubleNum;
        while (finalUnitNum >= 1000)
        {
            finalUnitNum /= 1000;
            unitCount++;
        }
        
        // 소숫점 두 자리수까지 출력. 나머지 단위는 단위 문자로 출력 (지원되는 단위 이상일 경우, 단위 출력 안 함)
        if (unitCount == 0 || unitCount >= _unitAlphabet.Length)
            return finalUnitNum.ToString("0");
        
        // 999.999999 라는 숫자 일 때, ToString() 시, 자동 라운딩 되어 1000.00이 됨 
        // 999.99로 표시될 수 있도록 함
        finalUnitNum = Math.Truncate(100 * finalUnitNum) / 100;
        var numStr = finalUnitNum.ToString("0.00");
        return $"{numStr}{_unitAlphabet[unitCount]}";
    }
}

public static class IntExtensions
{
    private static readonly Dictionary<int, string> _lookup = new Dictionary<int, string>();
    private static readonly Dictionary<(int, string), string> _lookupFormat = new Dictionary<(int, string), string>();
    
    public static string ToUnitString(this int intNum)
    {
        return ((double)intNum).ToUnitString();
    }

    public static string ToLookUpString(this int intNum)
    {
        if (_lookup.TryGetValue(intNum, out var value)) return value;

        var str = intNum.ToString();
        _lookup[intNum] = str;
        return str;
    }
    
    public static string ToLookUpFormatString(this int intNum, string format)
    {
        if (_lookupFormat.TryGetValue((intNum, format), out var value)) return value;

        var str = intNum.ToString(format);
        _lookupFormat[(intNum, format)] = str;
        return str;
    }
}

public static class LongExtensions
{
    public static string ToUnitString(this long longNum)
    {
        return ((double)longNum).ToUnitString();
    }
}

public static class FloatExtensions
{
    public static string ToUnitOrDecimalString(this float num)
    {
        return num >= 1000f ? num.ToUnitString() : num.ToDecimalString();    
    }
    
    public static string ToUnitString(this float num)
    {
        return ((double)num).ToUnitString();
    }
    
    public static string ToDecimalString(this float floatNum)
    {
        string result;

        /*
         * 소숫점이 있다면 두 자리 수까지만 출력 
         */
        if (Math.Abs(floatNum - (int) floatNum) < 0.001f)
        {
            result = floatNum.ToString(CultureInfo.InvariantCulture);
        }
        else
        {
            result = floatNum.ToString("0.00");
        }

        return result;
    }
}


[Serializable]
public class AnimationEffect
{
    public GameObject[] effects;
}

public class CommonRoll
{
    public string Name;
    public int Chance;
}


public static class ImageExtensions
{
    public static void SetSpriteWithOriginSIze(this Image image, Sprite sprite)
    {
        image.sprite = sprite;
        image.rectTransform.sizeDelta = sprite.rect.size;
    }
}

public static class StringExtensions
{
    public static string SubstringNoLongerThanSource(this string source, int startIndex, int maxLength)
    {
        return source.Substring(startIndex, Math.Min(source.Length - startIndex, maxLength));
    }
    
    public static string Wrap(this string input, Color color)
    {
        var colorString = ColorUtility.ToHtmlStringRGBA(color);
        return input.Wrap(colorString);
    }

    // color argument should be in rrggbbaa format or match standard html color name
    public static string Wrap(this string input, string color)
    {
        return "<color=#" + color + ">" + input + "</color>";
    }
}

public static class ButtonExtensions
{
    // Color Tint와 Sprite Swap을 같이 쓰기 위한 방법 
    // Color Tint를 쓰되, Interactable이 바뀔 땐 Sprite Swap 해주기 
    // 1. Color Tint - disabled color를 스프라이트의 영향없도록 하얀색으로 바꿔줘야 함 
    // 2. Sprite Swap - disbaled sprite / selected sprite를 세팅해놔야 함 
    public static void SetInteractable(this Button button, bool interactable)
    {
        button.image.sprite = interactable ? button.spriteState.selectedSprite : button.spriteState.disabledSprite;

        button.interactable = interactable;
    }
}

public static class DateTimeExtensions
{
    public static long ToTimestamp(this DateTime dateTime)
    {
        return dateTime.Ticks / TimeSpan.TicksPerSecond;
    }
}

public static class UIPopupExtensions
{
    public static IEnumerator<float> WaitShow(this UIPopup popup)
    {
        var waitingPopupToOpen = true;
        popup.ShowBehavior.OnFinished.Action += go => { waitingPopupToOpen = false; };
        while (waitingPopupToOpen)
            yield return Timing.WaitForOneFrame;
    }
    public static IEnumerator<float> WaitHide(this UIPopup popup)
    {
        var waitingPopupToClose = true;
        popup.HideBehavior.OnFinished.Action += go => { waitingPopupToClose = false; };
        while (waitingPopupToClose)
            yield return Timing.WaitForOneFrame;
    }
}

public static class UIViewExtensions
{
    public static IEnumerator<float> _WaitShow(this UIView view)
    {
        var isFinishedToShow = false;
        
        // 이벤트 등록
        view.ShowBehavior.OnFinished.Action += FinishedShow;

        // 지역함수
        void FinishedShow(GameObject go)
        {
            isFinishedToShow = true;
            
            // 이벤트 해제
            view.ShowBehavior.OnFinished.Action -= FinishedShow;
        }

        // 무한 대기
        while (!isFinishedToShow) yield return Timing.WaitForOneFrame;    
    }
    
    public static IEnumerator<float> _WaitHide(this UIView view)
    {
        var isFinishedToHide = false;
        
        // 이벤트 등록
        view.HideBehavior.OnFinished.Action += FinishedHide;
        
        // 지역함수
        void FinishedHide(GameObject go)
        {
            isFinishedToHide = true;
            
            // 이벤트 해제
            view.ShowBehavior.OnFinished.Action -= FinishedHide;
        }

        // 무한 대기
        while (!isFinishedToHide) yield return Timing.WaitForOneFrame;    
    }
}