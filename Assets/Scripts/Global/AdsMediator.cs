﻿using System;
using BackEnd;
using Global.Extensions;
using GoogleMobileAds.Api;
using InGame.Data;
using IronSource.Scripts;
using UI.Popup;
using UnityEngine;


namespace Global
{
    public class AdsMediator : MonoBehaviourSingleton<AdsMediator>
    {
        public static event VoidDelegate OnWatched;

        // 외부에서 세팅
        public HasAdSkipDelegate CanAdRemove = () => false;

        private Action<bool> WatchCompleteCallBack = null;

        private bool _isInit;
        
        void OnApplicationPause(bool isPaused) {                 
            IronSourceController.Agent.onApplicationPause(isPaused);
        }
        
        void IronSource_Init()
        {
            // Ironsource 세팅, 무조건 리워드 동영상 보상 (30초) 만 존재하기 때문에, 관련 Delegate 만 세팅.
            IronSourceEvents.onRewardedVideoAdOpenedEvent += RewardedVideoAdOpenedEvent;
            IronSourceEvents.onRewardedVideoAdClickedEvent += RewardedVideoAdClickedEvent;
            IronSourceEvents.onRewardedVideoAdClosedEvent += RewardedVideoAdClosedEvent;
            IronSourceEvents.onRewardedVideoAvailabilityChangedEvent += RewardedVideoAvailabilityChangedEvent;
            IronSourceEvents.onRewardedVideoAdStartedEvent += RewardedVideoAdStartedEvent;
            IronSourceEvents.onRewardedVideoAdEndedEvent += RewardedVideoAdEndedEvent;
            IronSourceEvents.onRewardedVideoAdRewardedEvent += RewardedVideoAdRewardedEvent;
            IronSourceEvents.onRewardedVideoAdShowFailedEvent += RewardedVideoAdShowFailedEvent;
        }

        public void Init()
        {
            if (_isInit) return;
            
            var appKey = string.Empty;
            if (ExtensionMethods.IsAndroid())
                appKey = "1174523fd";
            else if(ExtensionMethods.IsIOS())
                appKey = "11fe8b905";
            
            IronSourceController.Agent.init(appKey);

            // ironSource 미디에이션 연동 테스트, 넣으면 미디에이션 초기화 성공했는지 알려줌.
            IronSourceController.Agent.validateIntegration();

            IronSource_Init();

            IronSourceController.Agent.shouldTrackNetworkState(true);
            IronSourceController.Agent.setAdaptersDebug(true);

            _isInit = true;
        }

        public void ShowVideoAds(Action<bool> callback)
        {
            WatchCompleteCallBack = callback;
            
            // 튜토리얼 무료버프
            if (TutorialPopup.Instance != null && TutorialPopup.IsPlaying &&
                !UserData.My.UserInfo.GetFlag(UserFlags.TutorialAdsComplete))
            {
                WatchCompleteCallBack(true);
                OnWatched?.Invoke();
                return;
            }

            // 광고 제거 가능하다면 광고 틀지 않음
            if (CanAdRemove.Invoke() || ExtensionMethods.IsUnityEditor())
            {
                MessagePopup.Show("info_312");
                
                WatchCompleteCallBack(true);
                OnWatched?.Invoke();
                return;
            }

            IronSourceController.Agent.showRewardedVideo();
        }


        //RowardedVideo 광고 보기가 열렸을 때 호출됩니다.
        ////활동의 포커스가 손실됩니다. 무거운 행동은 삼가주세요.
        /// //비디오 광고가 닫힐 때까지 기다립니다.
        void RewardedVideoAdOpenedEvent()
        {
            ULogger.Log("광고 보기가 열렸습니다.");
        }

        //보상을 받은 비디오 광고 보기가 닫힐 때 호출됩니다.
        ////이제 활동에 포커스가 다시 맞춰집니다.
        void RewardedVideoAdClosedEvent()
        {
            ULogger.Log("광고 보기가 닫혔습니다.");
        }

        //광고 가용성 상태에 변화가 있을 때 호출됩니다.
        //@param - available - 보상된 비디오를 사용할 수 있게 되면 값이 true로 변경됩니다.
        //그러면 show Rewarded Video()를 호출하여 비디오를 표시할 수 있습니다.
        //사용 가능한 비디오가 없으면 값이 false로 변경됩니다.
        void RewardedVideoAvailabilityChangedEvent(bool available)
        {
            //Change the in-app 'Traffic Driver' state according to availability.
            bool rewardedVideoAvailability = available;

            ULogger.Log($"IS / 광고를 볼 수 있는 상태인가? : {rewardedVideoAvailability.ToString()}");
        }

        //사용자가 비디오를 완료하면 호출되며 보상을 받아야 합니다.
        //서버 간 콜백을 사용하는 경우 이 이벤트를 무시하고 기다릴 수 있습니다.
        // ironSource 서버에서 콜백됩니다.
        // @param - 배치 - 보상 데이터가 들어 있는 배치 개체입니다.
        void RewardedVideoAdRewardedEvent(IronSourcePlacement placement)
        {
            ULogger.Log(
                $"IS 광고 시청이 완료되었습니다 / {placement.getPlacementName()} / {placement.getRewardAmount().ToString()} / {placement.getRewardName()}");

            WatchCompleteCallBack(true);
            // WatchCompleteCallBack = null;
            OnWatched?.Invoke();
        }

        //리워드된 비디오를 표시하지 못했을 때 호출됩니다.
        //@param 설명 - 문자열 - 오류에 대한 정보를 포함합니다.
        void RewardedVideoAdShowFailedEvent(IronSourceError error)
        {
            ULogger.Log($"IS 광고를 표시하지 못했습니다. / {error.getCode().ToString()} / {error.getDescription()} / {error.getErrorCode().ToString()}");
            WatchCompleteCallBack(false);
        }

        // ----------------------------------------------------------------------------------------
        // 참고: 아래 이벤트는 지원되는 일부 보상 비디오 광고 네트워크에 사용할 수 없습니다.
        // 빌드에 포함하도록 선택한 광고 네트워크당 사용 가능한 이벤트를 확인합니다.
        // 빌드에 포함된 모든 광고 네트워크에 등록되는 이벤트만 사용하는 것이 좋습니다.
        // ----------------------------------------------------------------------------------------

        //비디오 광고가 재생되기 시작하면 호출됩니다.
        void RewardedVideoAdStartedEvent()
        {
            ULogger.Log("IS 광고가 재생됩니다..");
        }

        //비디오 광고 재생이 완료되면 호출됩니다.
        void RewardedVideoAdEndedEvent()
        {
            ULogger.Log("IS 광고재생이 완료되었습니다..");
        }

        //비디오 광고를 클릭하면 호출됩니다.
        void RewardedVideoAdClickedEvent(IronSourcePlacement placement)
        {
            ULogger.Log(
                $"비디오 광고가 클릭되었습니다. {placement.getPlacementName()} / {placement.getRewardAmount().ToString()}/ {placement.getRewardName()}");
        }
    }

    public delegate bool HasAdSkipDelegate();
}