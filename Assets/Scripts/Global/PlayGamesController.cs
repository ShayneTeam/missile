﻿using System;
using System.Collections;
using Global.Extensions;
#if UNITY_ANDROID
using Google.Play.Review;
#endif
using GooglePlayGames;
using GooglePlayGames.BasicApi;
using GooglePlayGames.BasicApi.SavedGame;
using UnityEngine;

#if UNITY_ANDROID

namespace Global
{
    public class PlayGamesController : MonoBehaviourSingleton<PlayGamesController>
    {
        #region SignIn

        private static bool _isAuthenticating;

        public static bool Authenticated => Social.Active.localUser.authenticated;

        public static string UserId => Authenticated ? Social.Active.localUser.id : "";

        public static void Init()
        {
            // GPGS 플러그인 설정
            var config = new PlayGamesClientConfiguration.Builder()
                .RequestIdToken()
                .RequestServerAuthCode(false)
                .RequestEmail() // 이메일 권한을 얻고 싶지 않다면 해당 줄(RequestEmail)을 지워주세요.
                .Build();
            //커스텀 된 정보로 GPGS 초기화
            PlayGamesPlatform.InitializeInstance(config);
            //PlayGamesPlatform.DebugLogEnabled = true; // 디버그 로그를 보고 싶지 않다면 false로 바꿔주세요.
            //GPGS 시작.
            PlayGamesPlatform.Activate();
        }

        public static void Authenticate(Action<bool, string> callback)
        {
            if (Authenticated || _isAuthenticating)
            {
                ULogger.LogWarning("Ignoring repeated call to Authenticate().");
                callback(false, "");
                return;
            }
            
            _isAuthenticating = true;
            Social.localUser.Authenticate((success) =>
            {
                _isAuthenticating = false;
                if (success)
                {
                    var resultCode = PlayGamesPlatform.Instance.GetServerAuthCode();
                    ULogger.Log($"구글 로그인 성공! / {resultCode}");
                    
                    ULogger.Log("Local user's username is " + ((PlayGamesLocalUser)Social.localUser).userName);
                    ULogger.Log("Local user's id is " + ((PlayGamesLocalUser)Social.localUser).id);

                    callback(true, GetToken());
                }
                else
                {
                    // no need to show error message (error messages are shown automatically by plugin)
                    ULogger.LogWarning("Failed to sign in with Google Play Games.");

                    callback(false, string.Empty);
                }
            });
        }

        public static string GetToken()
        {
            if (Authenticated)
            {
                if (PlayGamesPlatform.Instance.localUser.authenticated)
                {
                    ULogger.Log("구글 인증되어있음 확인.");
                }
                else
                {
                    ULogger.Log("구글 인증 안되어있음.");
                }
                
                // 첫번째 방법
                var token = PlayGamesPlatform.Instance.GetIdToken();
                ULogger.Log($"====첫번째 토큰 : {token}");
                if (string.IsNullOrWhiteSpace(token))
                {
                    ULogger.Log("첫번째 방법의 토큰이 없습니다. 두번째 방법으로 시도합니다.");
                    
                    // 두번째 방법
                    token = ((PlayGamesLocalUser)Social.localUser).GetIdToken();
                    ULogger.Log($"====두번째 토큰 : {token}");
                }
                
                return token;
            }

            ULogger.Log("구글 로그인이 되어있지 않습니다. PlayGamesPlatform.Instance.localUser.authenticated :  fail");
            return null;
        }

        public static void SignOut()
        {
            PlayGamesPlatform.Instance.SignOut();
        }

        #endregion


        #region Review

        private ReviewManager _reviewManager;
        private PlayReviewInfo _playReviewInfo;

        public void RequestReview()
        {
            // 리뷰 매니저는 반드시 GPGS 초기화가 이루어진 후, 호출돼야 함
            // 안 그럼 초기화 안 되고, null 처리 됨
            if (_reviewManager == null)
            {
                // 에디터일 때 ReviewManager 초기화 시, 아래 예외 발생
                // 실기기일 때만 초기화돼야 함
                // Exception: Field currentActivity or type signature not found
                if (!ExtensionMethods.IsUnityEditor())
                {
                    _reviewManager = new ReviewManager();    
                }
                else
                {
                    return;    
                }
            }
            
            //
            StartCoroutine(RequestReviewFlow());
        }

        private IEnumerator RequestReviewFlow()
        {
            // 리뷰 요청 
            var requestFlowOperation = _reviewManager.RequestReviewFlow();
            yield return requestFlowOperation;
            if (requestFlowOperation.Error != ReviewErrorCode.NoError)
            {
                // Log error. For example, using requestFlowOperation.Error.ToString().
                ULogger.LogError(requestFlowOperation.Error.ToString());
                yield break;
            }

            _playReviewInfo = requestFlowOperation.GetResult();


            // 리뷰 요청 실행
            var launchFlowOperation = _reviewManager.LaunchReviewFlow(_playReviewInfo);
            yield return launchFlowOperation;
            _playReviewInfo = null; // Reset the object
            if (launchFlowOperation.Error != ReviewErrorCode.NoError)
            {
                // Log error. For example, using requestFlowOperation.Error.ToString().
                ULogger.LogError(launchFlowOperation.Error.ToString());
                yield break;
            }
            // The flow has finished. The API does not indicate whether the user
            // reviewed or not, or even whether the review dialog was shown. Thus, no
            // matter the result, we continue our app flow.
        }

        #endregion

        public static void ShowLeaderboard()
        {
            Social.ShowLeaderboardUI();
        }

        public static void ReportScore(int value)
        {
            if (!Authenticated)
                return;
            
            Social.ReportScore(value, GPGSIds.leaderboard_best_stage, (x)=>{});
        }
    }
}

#endif