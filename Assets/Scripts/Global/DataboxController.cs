﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Numerics;
using Databox;
using MEC;
using UnityEngine;
using Vector2 = UnityEngine.Vector2;

// DataboxObjectManager를 가지고 있기 위한 싱글톤 클래스 
// 인게임에서 Databox 타입에 의존하지 않도록, 기본 타입으로 변환해서 넘겨주는 역할까지 한다
namespace Global
{
    public class DataboxController : MonoBehaviourSingleton<DataboxController>
    {
        private DataboxObjectManager _managerAsset;

        private static List<CommonRoll> _rollList;

        public void Awake()
        {
            _managerAsset ??= LoadManagerAsset();
        }

        private void OnDestroy()
        {
        }

        #region Load

        public void Load(List<string> databoxNames)
        {
            _managerAsset ??= LoadManagerAsset();
            
            foreach (var databoxName in databoxNames)
            {
                _managerAsset.Load(databoxName);
            }
        }

        public IEnumerator<float> Load(Action<float> callbackEach)
        {
            var isDone = false;
            
            //  
            _managerAsset ??= LoadManagerAsset();
            
            // 지역함수로 이벤트 등록
            // 개별 로딩 될 때마다 로딩 진행률 반환
            void LocalCallbackEach() { callbackEach?.Invoke(_managerAsset.LoadingPercent); }

            // 모든 로딩 끝났으면, 이 코루틴 종료
            void LocalCallbackAll() { isDone = true; }
            
            // 로드 
            _managerAsset.LoadAll(LocalCallbackEach, LocalCallbackAll);
            
            // 
            while (!isDone) yield return Timing.WaitForOneFrame;
        }

        public static DataboxObjectManager LoadManagerAsset()
        {
            return Resources.Load<DataboxObjectManager>("Manager");
        }

        

        #endregion
    
    
    
        #region Save
    
        public static void Save(string databoxName)
        {
            var databoxObject = Instance._managerAsset.GetDataboxObject(databoxName);

            databoxObject.SaveDatabase();
        }
    
        #endregion
    
    

        #region Set

        private bool SetData<T>(string databoxName, string tableName, string entryIdx, string valueIdx, DataboxType data) where T : DataboxType
        {
            var databoxObject = _managerAsset.GetDataboxObject(databoxName);
        
            var successSetData = databoxObject.SetData<T>(tableName, entryIdx, valueIdx, data);
            if (!successSetData)
                databoxObject.AddData(tableName, entryIdx, valueIdx, data);

            // SetData 해보고, 없으면 AddData까지 하므로, 데이터셋은 항상 성공함 
            return true;
        }
    
        public static bool SetDataFloat(string tableName, string sheetName, string idx, string column, float value)
        {
            return Instance.SetData<FloatType>(tableName, sheetName, idx, column, new FloatType(value));
        }

        public static bool SetDataDouble(string tableName, string sheetName, string idx, string column, double value)
        {
            return Instance.SetData<DoubleType>(tableName, sheetName, idx, column, new DoubleType(value));
        }

        public static bool SetDataInt(string tableName, string sheetName, string idx, string column, int value)
        {
            return Instance.SetData<IntType>(tableName, sheetName, idx, column, new IntType(value));
        }

        public static bool SetDataString(string tableName, string sheetName, string idx, string column, string value)
        {
            return Instance.SetData<StringType>(tableName, sheetName, idx, column, new StringType(value));
        }
        
        public static bool SetDataBool(string tableName, string sheetName, string idx, string column, bool value)
        {
            return Instance.SetData<BoolType>(tableName, sheetName, idx, column, new BoolType(value));
        }

        public static bool SetDataDictionaryString(string tableName, string sheetName, string idx, string column, Dictionary<string, string> value)
        {
            return Instance.SetData<DictionaryStringType>(tableName, sheetName, idx, column, new DictionaryStringType(value));
        }

        #endregion


        #region Get

        private T GetData<T>(string databoxName, string tableName, string entryIdx, string valueIdx) where T : DataboxType
        {
            var databoxObject = _managerAsset.GetDataboxObject(databoxName);

            // 데이터 없다고 새로 넣지 않음
            // 그래야 다음 조회 시에도 null이 나오면서 DefaultValue로 설정됨 
            return databoxObject.GetData<T>(tableName, entryIdx, valueIdx, false);
        }

        private T GetDataSearchByAllTable<T>(string databoxName, string entryIdx, string valueIdx) where T : DataboxType
        {
            var databoxObject = _managerAsset.GetDataboxObject(databoxName);

            return databoxObject.GetDataSearchByAllTable<T>(entryIdx, valueIdx);
        }

        public static string GetConstraintsData(string idx, string defaultValue = "")
        {
            return GetDataString("Constraints", "constraints", idx, "value", defaultValue);
        }

        public static string GetLocalizationData(string key, string lang, string defaultValue = "")
        {
            var data = Instance.GetDataSearchByAllTable<StringType>("Localization", key, lang);
            return data?.Value ?? defaultValue;
        }

        public static bool GetDataBool(string tableName, string sheetName, string idx, string column, bool defaultValue = false)
        {
            var data = Instance.GetData<BoolType>(tableName, sheetName, idx, column);
            return data?.Value ?? defaultValue;
        }

        public static float GetDataFloat(string tableName, string sheetName, string idx, string column, float defaultValue = 0f)
        {
            var data = Instance.GetData<FloatType>(tableName, sheetName, idx, column);
            return data?.Value ?? defaultValue;
        }

        public static double GetDataDouble(string tableName, string sheetName, string idx, string column, double defaultValue = 0d)
        {
            var data = Instance.GetData<DoubleType>(tableName, sheetName, idx, column);
            return data?.Value ?? defaultValue;
        }

        public static int GetDataInt(string tableName, string sheetName, string idx, string column, int defaultValue = 0)
        {
            var data = Instance.GetData<IntType>(tableName, sheetName, idx, column);
            return data?.Value ?? defaultValue;
        }

        public static BigInteger GetDataBigInteger(string tableName, string sheetName, string idx, string column, BigInteger defaultValue)
        {
            var data = Instance.GetData<BigIntegerType>(tableName, sheetName, idx, column);
            return data?.Value ?? defaultValue;
        }

        public static string GetDataString(string tableName, string sheetName, string idx, string column, string defaultValue = "")
        {
            var data = Instance.GetData<StringType>(tableName, sheetName, idx, column);
            if (data != null && !string.IsNullOrEmpty(data.Value) && !string.IsNullOrWhiteSpace(data.Value))
                return data.Value;

            return defaultValue;
        }

        public static GameObject GetDataGameObject(string tableName, string sheetName, string idx, string column, GameObject defaultValue = null)
        {
            var data = Instance.GetData<ResourceType>(tableName, sheetName, idx, column);
            if (data != null)
                return data.Load() as GameObject;

            return defaultValue;
        }

        public static Vector2 GetDataVector2(string tableName, string sheetName, string idx, string column, Vector2 defaultValue)
        {
            var data = Instance.GetData<Vector2Type>(tableName, sheetName, idx, column);
            return data?.Value ?? defaultValue;
        }
    
        public static Dictionary<string, string> GetDataDictionaryString(string tableName, string sheetName, string idx, string column, Dictionary<string, string> defaultValue = null)
        {
            var data = Instance.GetData<DictionaryStringType>(tableName, sheetName, idx, column);
            return data?.Value ?? defaultValue;
        }
        
        public static List<string> GetDataStringList(string tableName, string sheetName, string idx, string column, List<string> defaultValue = null)
        {
            var data = Instance.GetData<StringListType>(tableName, sheetName, idx, column);
            return data?.Value ?? defaultValue;
        }

        public static ICollection<string> GetAllIdxes(string tableName, string sheetName)
        {
            var databoxObject = Instance._managerAsset.GetDataboxObject(tableName);
            return databoxObject.GetEntriesFromTable(sheetName).Keys;
        }

        public static IEnumerable<string> GetAllIdxesByGroup(string tableName, string sheetName, string groupColumn, string targetGroup)
        {
            var allIdxes = GetAllIdxes(tableName, sheetName);
            return allIdxes.Where(idx => GetDataString(tableName, sheetName, idx, groupColumn) == targetGroup);
        }

        public static string RandomPick(string tableName, string sheetName, string groupColumn, string targetGroup, string rarityColumn)
        {
            _rollList ??= new List<CommonRoll>();
            _rollList.Clear();
            
            var allIdxesSameGroup = GetAllIdxesByGroup(tableName, sheetName, groupColumn, targetGroup);
            foreach (var idx in allIdxesSameGroup)
            {
                // 레어리티 넣기 
                var rarity = GetDataInt(tableName, sheetName, idx, rarityColumn);
                GlobalFunction.AddRoll(ref _rollList, idx, rarity);
            }
            
            // 확률적으로 하나 뽑기
            return GlobalFunction.Roll(ref _rollList).Name;
        }
        
        public static string RandomPick(string tableName, string sheetName, string rarityColumn)
        {
            _rollList ??= new List<CommonRoll>();
            _rollList.Clear();
            
            var allIdxesSameGroup = GetAllIdxes(tableName, sheetName);
            foreach (var idx in allIdxesSameGroup)
            {
                // 레어리티 넣기 
                var rarity = GetDataInt(tableName, sheetName, idx, rarityColumn);
                GlobalFunction.AddRoll(ref _rollList, idx, rarity);
            }
            
            // 확률적으로 하나 뽑기
            return GlobalFunction.Roll(ref _rollList).Name;
        }

        public static string GetIdxByColumn(string tableName, string sheetName, string columnName, string column)
        {
            var allIdxes = GetAllIdxes(tableName, sheetName);
            foreach (var idx in allIdxes)
            {
                // 타겟한 컬럼 찾기 (단, 컬럼 타입이 StringType이어야 한다)
                var foundColumn = GetDataString(tableName, sheetName, idx, columnName);
                if (foundColumn == column) return idx; 
            }

            return "";
        }
        
        #endregion
    
    
        public static bool IdxExists(string tableName, string sheetName, string idx)
        {
            var databoxObject = Instance._managerAsset.GetDataboxObject(tableName);
        
            return databoxObject.EntryExists(sheetName, idx);
        }
    }
}