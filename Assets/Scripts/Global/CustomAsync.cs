﻿using System.Collections;
using System.Collections.Generic;
using InGame;
using InGame.Enemy;
using LitJson;
using UnityEngine;

namespace Global
{
    /*
 * 1. 내부의 비동기 처리 완료 여부 반환
 * 2. 비동기 처리 결과 데이터 반환 
 */
    public class CustomAsync
    {
        public bool IsDone;
        public bool IsSuccess;
    }
}