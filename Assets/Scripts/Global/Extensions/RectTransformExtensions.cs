﻿using UnityEngine;


public static class RectTransformExtensions
{
    public static void FromWorldPosition(this RectTransform rect, Vector3 worldPos, Camera worldCamera, Canvas targetCanvas)
    {
        rect.anchoredPosition = GlobalFunction.WorldToCanvas(worldPos, worldCamera, targetCanvas);
    }
}