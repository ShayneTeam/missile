﻿using UnityEngine;
 
public static class TransformExtensions
{
    public static void FromMatrix(this Transform transform, Matrix4x4 matrix)
    {
        transform.localScale = matrix.ExtractScale();
        transform.rotation = matrix.ExtractRotation();
        transform.position = matrix.ExtractPosition();
    }
    
    public static void FromMatrix(this Rigidbody2D rigidbody2D, Matrix4x4 matrix)
    {
        var rot = matrix.ExtractRotation();
        rigidbody2D.rotation = rot.eulerAngles.z;
        rigidbody2D.position = matrix.ExtractPosition();
    }
}