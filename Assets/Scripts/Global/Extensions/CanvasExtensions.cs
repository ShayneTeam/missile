﻿using UnityEngine;

public static class CanvasExtensions
{
    // Render Mode가 Screen Space - Camera 일 때, 스크린 좌표를 해당 캔버스 좌표로 변환
    public static Vector3 ConvertScreenPointToWorldPoint(this Canvas canvas, Vector3 screenPoint)
    {
        var battleCanvasRect = canvas.GetComponent<RectTransform>();
        var canvasCamera = canvas.worldCamera;

        RectTransformUtility.ScreenPointToWorldPointInRectangle(battleCanvasRect, screenPoint, canvasCamera, out var convertedMousePos);

        return convertedMousePos;
    }
}