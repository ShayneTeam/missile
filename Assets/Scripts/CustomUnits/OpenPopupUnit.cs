﻿using Doozy.Engine.UI;
using Ludiq;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace Bolt.CustomUnits
{
    [UnitTitle("Open Popup")]
    [UnitCategory("Doozy\\Popup")]
    public class OpenPopupUnit : Unit
    {
        [DoNotSerialize]
        [PortLabelHidden]
        public ControlInput enter { get; private set; }
        
        [DoNotSerialize]
        [PortLabelHidden]
        public ControlOutput exit { get; private set; }
        
        [DoNotSerialize]
        [PortLabel("Name")]
        public ValueInput name { get; private set; }
        
        
        protected override void Definition()
        {
        	enter = ControlInput(nameof(enter), Enter);
        	exit = ControlOutput(nameof(exit));
        
            name = ValueInput<string>(nameof(name), "");    
        
        	Succession(enter, exit);
        	
        	Requirement(name, enter);
        }

        private ControlOutput Enter(Flow flow)
        {
        	var popupName = flow.GetValue<string>(name);
        	
        	var popup = UIPopup.GetPopup(popupName);
            /*
             * 큐 쓰지 않고 바로 연다
             * 큐를 써야 하는 것들은 최초 게임 시작 때 이벤트, 공지 팝업 큐 쌓을 때나 필요하지 않나 싶다
             */
            popup.Show();	 
        
            return exit;
        }

    }
}