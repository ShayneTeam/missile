﻿using Doozy.Engine.UI;
using InGame;
using Ludiq;
using UI.Popup;
using UnityEngine;
// ReSharper disable InconsistentNaming

namespace Bolt.CustomUnits
{
    [UnitTitle("Show Message")]
    [UnitCategory("Doozy\\Popup")]
    public class ShowMessageUnit : Unit
    {
        [DoNotSerialize]
        [PortLabelHidden]
        public ControlInput enter { get; private set; }
        
        [DoNotSerialize]
        [PortLabelHidden]
        public ControlOutput exit { get; private set; }
        
        [DoNotSerialize]
        [PortLabel("Localization Key")]
        public ValueInput key { get; private set; }
        
        
        protected override void Definition()
        {
        	enter = ControlInput(nameof(enter), Enter);
        	exit = ControlOutput(nameof(exit));
        
            key = ValueInput<string>(nameof(key), "");    
        
        	Succession(enter, exit);
        	
        	Requirement(key, enter);
        }

        private ControlOutput Enter(Flow flow)
        {
        	var localizationKey = flow.GetValue<string>(key);
        	
            MessagePopup.Show(localizationKey);
        
            return exit;
        }

    }
}