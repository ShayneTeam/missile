﻿using System.Collections;
using Ludiq;
using UnityEngine;

namespace Bolt
{
    [UnitTitle("Coroutine")]
    [UnitShortTitle("Coroutine")]
    [UnitOrder(1)]
    public class CoroutineUnit : WaitUnit
    {
        [DoNotSerialize]
        [PortLabel("Coroutine")]
        public ValueInput coroutine { get; private set; }

        protected override void Definition()
        {
            base.Definition();

            coroutine = ValueInput<IEnumerator>(nameof(coroutine));

            Requirement(coroutine, enter);
        }

        protected override IEnumerator Await(Flow flow)
        {
            yield return flow.GetValue<IEnumerator>(this.coroutine);

            yield return exit;
        }
    }
}
