using UnityEngine;
using UnityEngine.UDP;

public class PurchaseListener : IPurchaseListener
{
    public void OnPurchase(PurchaseInfo purchaseInfo)
    {
        // The purchase has succeeded.
        // If the purchased product is consumable, you should consume it here.
        // Otherwise, deliver the product.
        
        Debug.Log($@"{purchaseInfo.ProductId} / OnPurchase");
        StoreService.ConsumePurchase(purchaseInfo, new PurchaseListener());
    }

    public void OnPurchaseFailed(string message, PurchaseInfo purchaseInfo)
    {
        Debug.Log($@"{purchaseInfo.ProductId} / OnPurchaseFailed");
        Debug.Log("Purchase Failed: " + message);
    }

    public void OnPurchaseRepeated(string productCode)
    {
        // Some stores don't support queryInventory.
        Debug.Log($@"{productCode} / OnPurchaseRepeated");
    }

    public void OnPurchaseConsume(PurchaseInfo purchaseInfo)
    {
        // The consumption succeeded.
        // You should deliver the product here.
        Debug.Log($@"{purchaseInfo.ProductId} / OnPurchaseConsume");
    }

    public void OnPurchaseConsumeFailed(string message, PurchaseInfo purchaseInfo)
    {
        // The consumption failed.
        Debug.Log($@"{purchaseInfo.ProductId} / OnPurchaseConsumeFailed");
    }

    public void OnQueryInventory(Inventory inventory)
    {
        // Querying inventory succeeded.
        Debug.Log($@"OnQueryInventory");
    }

    public void OnQueryInventoryFailed(string message)
    {
        // Querying inventory failed.
        Debug.Log($@"{message} / OnQueryInventoryFailed");
    }
}