using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UDP;

public class InitListener : IInitListener
{
    public void OnInitialized(UserInfo userInfo)
    {
        ULogger.Log("Initialization succeeded");
        // You can call the QueryInventory method here
        // to check whether there are purchases that haven’t be consumed.       
    }

    public void OnInitializeFailed(string message)
    {
        ULogger.Log("Initialization failed: " + message);
    }
}

