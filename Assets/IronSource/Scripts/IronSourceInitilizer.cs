﻿using UnityEngine;

public class IronSourceInitilizer
{
#if UNITY_IOS || UNITY_ANDROID
    [RuntimeInitializeOnLoadMethod]
    static void Initilize()
    {
        var developerSettings = Resources.Load<IronSourceMediationSettings>(IronSourceConstants.IRONSOURCE_MEDIATION_SETTING_NAME);
        if (developerSettings != null)
        {
#if UNITY_ANDROID
            string appKey = developerSettings.AndroidAppKey;
#elif UNITY_IOS
        string appKey = developerSettings.IOSAppKey;
#endif
            if (developerSettings.EnableIronsourceSDKInitAPI == true)
            {
                if (appKey.Equals(string.Empty))
                {
                    Debug.LogWarning("IronSourceInitilizer Cannot init without AppKey");
                }
                else
                {
                    IronSource.Scripts.IronSourceController.Agent.init(appKey);
                }

            }

            if (developerSettings.EnableAdapterDebug)
            {
                IronSource.Scripts.IronSourceController.Agent.setAdaptersDebug(true);
            }

            if (developerSettings.EnableIntegrationHelper)
            {
                IronSource.Scripts.IronSourceController.Agent.validateIntegration();
            }
        }
    }
#endif

}
