using System;
using InGame.Controller;
using NUnit.Framework;
using UnityEngine;

namespace Tests.Editor
{
    public class TestTime
    {
        // A Test behaves as an ordinary method
        [Test]
        public void TestEndTime()
        {
            Debug.Log("TestEndTime");
        
            var now = DateTime.Now;

            // 오늘의 0시로 맞춤 
            now = now.AddSeconds(-now.Second);
            now = now.AddMinutes(-now.Minute);
            now = now.AddHours(-now.Hour);

            // 0 ~ 24시간 까지 결과 잘 나오는 지 체크 
            for (var hour = 0; hour <= 48; hour++)
            {
                var endTime = DailyTimeController.GetEndTimeOf(now);
            
                Assert.True(now.ToTimestamp() < endTime.ToTimestamp());

                Debug.Log($"{now:g} 접속 시, 종료 시간 : {endTime:g} - 성공");

                // 시간 증가 
                now = now.AddHours(1);
            }
        }

        [Test]
        public void TestNextWeekDay()
        {
            Debug.Log("TestNextWeekDay");
        
            var now = DateTime.Now;

            // 오늘의 0시로 맞춤 
            now = now.AddSeconds(-now.Second);
            now = now.AddMinutes(-now.Minute);
            now = now.AddHours(-now.Hour);
        
            // 한국 기준. 초기화 시간에 접속 했을 때 상황
            now = now.AddHours(5);
        
            // 0 ~ 24시간 까지 결과 잘 나오는 지 체크 
            for (var day = 1; day <= 7; day++)
            {
                var endTime = DailyTimeController.GetEndTimeOf(now);
                var nextWeekDay = DailyTimeController.GetNextWeekDay(endTime, DayOfWeek.Monday);

                Assert.True(now.ToTimestamp() < nextWeekDay.ToTimestamp());

                Debug.Log($"{now:g} 접속 시, 종료 시간 : {nextWeekDay:g} - 성공");

                // 시간 증가 
                now = now.AddDays(1);
            }
        }
    
        [Test]
        public static void TestNextMonthDay()
        {
            Debug.Log("TestNextMonthDay");
        
            var now = DateTime.Now;

            // 오늘의 0시로 맞춤 
            now = now.AddSeconds(-now.Second);
            now = now.AddMinutes(-now.Minute);
            now = now.AddHours(-now.Hour);
        
            // 한국 기준. 초기화 시간에 접속 했을 때 상황
            now = now.AddHours(5);
        
            // 0 ~ 24시간 까지 결과 잘 나오는 지 체크 
            for (var day = 1; day <= 30; day++)
            {
                var endTime = DailyTimeController.GetEndTimeOf(now);
                var nextMonthDay = DailyTimeController.GetNextMonthDay(endTime, 1);

                Assert.True(now.ToTimestamp() < nextMonthDay.ToTimestamp());

                Debug.Log($"{now:g} 접속 시, 종료 시간 : {nextMonthDay:g} - 성공");

                // 시간 증가 
                now = now.AddDays(1);
            }
        }

        [Test]
        public void TestLimitTime()
        {
            // 4일 제한 
            Debug.Log("4일 제한");
            const long timestampLimitFourDays = 63771512400;
            TestTimestamp(timestampLimitFourDays);
        
            // 5일 제한 
            Debug.Log("5일 제한");
            const long timestampLimitFiveDays = 63771598800;
            TestTimestamp(timestampLimitFiveDays);
        }

        private static void TestTimestamp(long resetTimestamp)
        {
            var now = DateTime.Now;

            // 오늘의 0시로 맞춤 
            now = now.AddSeconds(-now.Second);
            now = now.AddMinutes(-now.Minute);
            now = now.AddHours(-now.Hour);

            // 한국 기준. 초기화 시간에 접속 했을 때 상황
            now = now.AddHours(5);

            // 0 ~ 24시간 까지 결과 잘 나오는 지 체크 
            for (var day = 1; day <= 7; day++)
            {
                Assert.True(now.ToTimestamp() >= resetTimestamp);

                Debug.Log($"{now:g} 접속 시, 데일리 리셋 - 성공");

                // 시간 증가 
                now = now.AddDays(1);
            }
        }
    }
}
