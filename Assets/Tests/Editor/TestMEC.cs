﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using BackEnd;
using MEC;
using NUnit.Framework;
using Server;
using UnityEngine;
using UnityEngine.TestTools;

namespace Tests.Editor
{
    public class TestMEC
    {
        private const int TestSec = 10;
        private const int ExpectedFrame = 60;

        // 딱 한 번만 초기화 하는 용 (EnterPlayMode 땐 호출 안 되는 곳)
        [SetUp]
        public void SetUp()
        {
            // 뒤끝 초기화 
            if (BackEndManager.Instance.IsInitialized) return;
            var backEndInit = BackEndManager.Instance.Initialize();
            Assert.True(backEndInit.IsSuccess());
        }

        [UnityTest]
        public IEnumerator ScaledTime()
        {
            // 기준 시간 가져오기
            var serverTimeOrigin = GetServerTime();

            // MEC 부하 주기
            //var handles = new List<CoroutineHandle>();
            //for (var i = 0; i < 10000; i++) handles.Add(Timing.RunCoroutine(_Overhead(), Segment.EditorUpdate));

            
            
            for (var i = 0; i < TestSec; i++)
            {
                serverTimeOrigin = GetServerTime();
                
                // EditMode 테스트는 yield return null 밖에 안 되므로, 수동으로 1초를 나타내기 위해, 30 프레임 yield return 시킴
                for (var frame = 0; frame < ExpectedFrame; frame++)
                {
                    var time = Time.time;        
                    yield return null;
                    var deltaTime = Time.time - time;
                    serverTimeOrigin = serverTimeOrigin.AddSeconds(deltaTime);
                }

                var serverTimeOriginMilliSec = serverTimeOrigin.Ticks / System.TimeSpan.TicksPerMillisecond;

                // 뒤끝 서버 시간 직접 받아오기 
                var serverTimeNew = GetServerTime();
                var serverTimeNewMilliSec = serverTimeNew.Ticks / System.TimeSpan.TicksPerMillisecond;

                // 둘의 차이가 크면 안 됨
                var diff = serverTimeNewMilliSec - serverTimeOriginMilliSec;
                var diffSec = diff / 1000f;
                Debug.Log($"시간 차이 : {diffSec}초");
                Assert.True(Math.Abs(diffSec) < 1);
            }
        }

        private static DateTime GetServerTime()
        {
            var result = Backend.Utils.GetServerTime();
            var time = result.GetReturnValuetoJSON()["utcTime"].ToString();
            return DateTime.Parse(time);
        }

        private IEnumerator<float> _Overhead()
        {
            // 일일히 코루틴 해제하는 거 귀찮으니, 적당히 돌리고 끝냄 
            for (var i = 0; i < TestSec * ExpectedFrame; i++)
            {
                yield return Timing.WaitForOneFrame;
            }
        }
    }
}