using System.Collections;
using BackEnd;
using Global;
using InGame.Controller;
using InGame.Data;
using MEC;
using NUnit.Framework;
using Server;
using UnityEngine.TestTools;

namespace Tests.Editor
{
	public class TestDailyProcess
	{
		// 딱 한 번만 초기화 하는 용 (EnterPlayMode 땐 호출 안 되는 곳)
		[SetUp]
		public void SetUp()
		{
			// 뒤끝 초기화 
			if (BackEndManager.Instance.IsInitialized) return;
			var backEndInit = BackEndManager.Instance.Initialize();
			Assert.True(backEndInit.IsSuccess());
		}
	
		[UnityTest]
		public IEnumerator TestDailyProcessWithEnumeratorPasses()
		{
			// 뒤끝 로그인
			var login = Backend.BMember.CustomLogin("id21", "pw21");
			Assert.True(login.IsSuccess());
	    
			// 뒤끝 시간 동기화
			var isSuccessSyncServerTime = ServerTime.Instance.GetServerTimeSync();
			Assert.True(isSuccessSyncServerTime);
	    
			// 데이터박스 로딩 
			var managerAsset = DataboxController.LoadManagerAsset();
			var load = Timing.RunCoroutine(DataboxController.Instance.Load((perfect) => { }), Segment.EditorUpdate);
			while (load.IsRunning) yield return null;
			foreach (var data in managerAsset.databoxContainers)
			{
				Assert.True(data.dataObject.IsLoaded);
			}
			
			// SendQueue 초기화 
			if (!SendQueue.IsInitialize) SendQueue.StartSendQueue(true, (exception) =>{ });
			Assert.True(SendQueue.IsInitialize);
	    
			// 유저 데이터 동기화 
			var isSuccessSyncUserData = UserData.My.SyncServerDataAllForHost(); 
			Assert.True(isSuccessSyncUserData);
	    
			// 데일리 프로세스
			var daily = Timing.RunCoroutine(BackEndManager.DailyProcess(), Segment.EditorUpdate);
			while (daily.IsRunning)
			{
				BackEnd.SendQueue.Poll();
				yield return null;
			}
			
			// SendQueue 종료
			BackEnd.SendQueue.StopSendQueue();
		}
	}
}
