﻿namespace SA.Android.GMS.CodeGen
{
    public static class AN_GamesIds
    {
        public const string AppId = "130303469895";

        public static class Achievements
        {
            public const string Beststage = "CgkIx9LEteUDEAIQAA";
        }

        public static class Leaderboards
        {
            public const string BestStage = "CgkIx9LEteUDEAIQBQ";
        }
    }
}
