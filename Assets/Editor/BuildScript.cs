using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net;
using System.Text;
using Newtonsoft.Json;
using NUnit.Framework.Internal;
using UnityEditor;
using UnityEditor.AddressableAssets;
using UnityEditor.AddressableAssets.Build;
using UnityEditor.AddressableAssets.Settings;
using UnityEditor.Build.Reporting;
using UnityEditor.Purchasing;
using UnityEngine;
using UnityEngine.Purchasing;

public static class BuildScript
{
    private static readonly string Eol = Environment.NewLine;

    private static readonly List<string> Secrets = new List<string>
    {
        "androidKeystorePass", "androidKeyaliasName", "androidKeyaliasPass"
    };

    // [MenuItem("Tools/CI/Build iOS Debug")]
    public static void BuildIOS()
    {
        // 1. Build Number Automatically Up
        AddBuildVersion(BuildTarget.iOS);
        GenericBuild(FindEnabledEditorScenes(), $"./Build/IOS/", BuildTarget.iOS, BuildOptions.None);
    }

    static (string cdnPath, string outPutPath) AddressableBuild(string addressableOption)
    {
        var cdnPath = string.Empty;
        var outputPath = string.Empty;

        // 미사일은 원격 CDN을 사용하지 않기 때문에, NewBuild 면 빌드를 새로 하지만 / 입력을 안하면 그냥 기존꺼 그대로 쓰면 됨.
        if (addressableOption == "NewBuild")
        {
            AddressableAssetSettings.CleanPlayerContent();
            AddressableAssetSettings.BuildPlayerContent();
        }
        // else
        // {
        //     var androidBinaryPath = Path.Combine(Application.dataPath.Substring(0, Application.dataPath.LastIndexOf('/')),
        //         ContentUpdateScript.GetContentStateDataPath(false));
        //     // var androidBinaryPath = Path.Combine(Application.dataPath, "AddressableAssetsData", "Android",
        //     //     "addressables_content_state.bin");
        //
        //     Console.WriteLine($@"어드레서블 Content Path : {androidBinaryPath}");
        //
        //     if (!File.Exists(androidBinaryPath))
        //     {
        //         Debug.LogError($"{androidBinaryPath} 없음.");
        //         return (string.Empty, string.Empty);
        //     }
        //
        //     // clean
        //     ContentUpdateScript.BuildContentUpdate(AddressableAssetSettingsDefaultObject.Settings, androidBinaryPath);
        // }

        // 미사일은 nas에 올리려는것이 아니기 때문에 (built-in) return 할 필요 없음. (빈값으로 줌)
        return (cdnPath, outputPath);
    }
    
    public static void ChatBuild()
    {
        var options = GetValidatedOptions();

        foreach (var keyValuePair in options)
        {
            Console.WriteLine($"커맨드라인 Key : {keyValuePair.Key} : 커맨드라인 Value : {keyValuePair.Value}");
        }

        // 시작시간.
        options["startTime"] = DateTime.Now.ToLongTimeString();

        // 플랫폼.
        var runningPlatform = options["runningPlatform"];
        BuildTarget BuildTargetEnum;

        if (runningPlatform == "aos")
        {
            BuildTargetEnum = BuildTarget.Android;
        }
        else if (runningPlatform == "ios")
        {
            BuildTargetEnum = BuildTarget.iOS;
        }
        else
        {
            Console.WriteLine($"지정되지 않은 플랫폼 타입. {runningPlatform}");
            return;
        }
        
        var apkVersion = options["appVersion"];
        if (!string.IsNullOrWhiteSpace(apkVersion) && Version.TryParse(apkVersion, out _))
        {
            PlayerSettings.bundleVersion = apkVersion;
        }

        var newBuildVersionCode = int.Parse(options["versionCode"]);

        if (newBuildVersionCode == 0)
            newBuildVersionCode = int.Parse(AddBuildVersion(BuildTargetEnum));
        else
        {
            PlayerSettings.Android.bundleVersionCode = newBuildVersionCode;
        }

        var customSymbol = options["customDefineSymbol"];
        var customDefine = customSymbol.Split(';');
        var originalDefinesSymbolString = PlayerSettings.GetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup);
        if (!string.IsNullOrWhiteSpace(customSymbol) && customDefine.Length > 0)
        {
            var allDefines = originalDefinesSymbolString.Split(';').ToList();
            allDefines.AddRange(customDefine.ToList());

            PlayerSettings.SetScriptingDefineSymbolsForGroup(
                EditorUserBuildSettings.selectedBuildTargetGroup,
                string.Join(";", allDefines.ToArray()));
        }

        var newPath = Path.Combine("build", $"{runningPlatform}_{DateTime.Now:yyyyMMdd}_V{newBuildVersionCode}");
        if (!Directory.Exists(newPath))
        {
            Console.WriteLine($@"폴더가 없습니다. 패스를 생성합니다. {newPath}");
            Directory.CreateDirectory(newPath);
        }

        // 빌드 정보 구성
        var buildPlayerInfo = new BuildPlayerOptions
        {
            scenes = FindEnabledEditorScenes(),
            locationPathName = Path.Combine(newPath, $"{DateTime.Now:yyyy_MM_dd_HH_mm_ss}"),
            target = BuildTargetEnum,
        };

        if (BuildTargetEnum == BuildTarget.Android)
        {
            PlayerSettings.keystorePass = "slaqmf2014";
            PlayerSettings.Android.keyaliasName = "missile";
            PlayerSettings.Android.keyaliasPass = "slaqmf2014";
            PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7 | AndroidArchitecture.ARM64;

            var targetArchitectures = options["targetArchitectures"];
            if (targetArchitectures == "x86")
            {
                PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARMv7;
            }
            else if (targetArchitectures == "x64")
            {
                PlayerSettings.Android.targetArchitectures = AndroidArchitecture.ARM64;
            }

            var buildExtension = options["buildExtension"];
            if (buildExtension == "aab")
            {
                EditorUserBuildSettings.buildAppBundle = true;
                buildPlayerInfo.locationPathName += ".aab";
            }
            else
            {
                EditorUserBuildSettings.buildAppBundle = false;
                buildPlayerInfo.locationPathName += ".apk";
            }

            var iapStore = options["iapStore"];
            UnityPurchasingEditor.TargetAndroidStore(iapStore == "UDP" ? AppStore.UDP : AppStore.GooglePlay);
        }

        PlayerSettings.SetStackTraceLogType(LogType.Assert, StackTraceLogType.ScriptOnly);
        PlayerSettings.SetStackTraceLogType(LogType.Error, StackTraceLogType.ScriptOnly);
        PlayerSettings.SetStackTraceLogType(LogType.Exception, StackTraceLogType.ScriptOnly);
        PlayerSettings.SetStackTraceLogType(LogType.Log, StackTraceLogType.ScriptOnly);
        PlayerSettings.SetStackTraceLogType(LogType.Warning, StackTraceLogType.ScriptOnly);

        if (options["developerBuild"] == "on")
        {
            buildPlayerInfo.options |= BuildOptions.Development;
        }
        else
        {
            // 릴리즈 빌드 시, DEV 디파인심볼이 없다면, 로그 남기지 않게 설정 
            if (!customDefine.Contains("DEV"))
            {
                PlayerSettings.SetStackTraceLogType(LogType.Assert, StackTraceLogType.None);
                PlayerSettings.SetStackTraceLogType(LogType.Error, StackTraceLogType.None);
                PlayerSettings.SetStackTraceLogType(LogType.Exception, StackTraceLogType.None);
                PlayerSettings.SetStackTraceLogType(LogType.Log, StackTraceLogType.None);
                PlayerSettings.SetStackTraceLogType(LogType.Warning, StackTraceLogType.None);
            }
        }

        try
        {
            // AddressableBuild
            var addressableBuild = AddressableBuild(options["addressableOption"]);

            // APK 빌드
            var report = BuildPipeline.BuildPlayer(buildPlayerInfo);

            var logPath = Path.Combine(Environment.CurrentDirectory, "build_log.log");
            BuildReportTool.ReportGenerator.CreateReport(logPath);

            var summary = report.summary;
            ReportSummary(summary);

            if (Application.isBatchMode)
            {
                SendChatBuildResult(summary.result, buildPlayerInfo, options, newPath, string.Empty);
                ExitWithResult(summary.result);
            }
        }
        catch (Exception ex)
        {
            SendChatBuildResult(BuildResult.Failed, buildPlayerInfo, options, newPath, ex.Message);
            ExitWithResult(BuildResult.Failed);
        }

        // restore symbol
        if (!string.IsNullOrWhiteSpace(customSymbol) && customDefine.Length > 0)
            PlayerSettings.SetScriptingDefineSymbolsForGroup(EditorUserBuildSettings.selectedBuildTargetGroup, originalDefinesSymbolString);
    }

    public static void SendChatBuildResult(BuildResult result, BuildPlayerOptions buildPlayerOptions,
        Dictionary<string, string> options, string newPath, string errMsg)
    {
        switch (result)
        {
            case BuildResult.Succeeded:
                Console.WriteLine("Build succeeded!");
                break;
            case BuildResult.Failed:
            case BuildResult.Cancelled:
            case BuildResult.Unknown:
            default:
                options["errMsg"] = errMsg;
                Console.WriteLine($"Build result is ${result}!");
                break;
        }

        options["endTime"] = DateTime.Now.ToLongTimeString();
        options["isSuccess"] = result.ToString();
        options["newPath"] = newPath;
        options["fullPath"] = Path.Combine(options["projectPath"], newPath);
        options["apkPath"] = buildPlayerOptions.locationPathName;
        options["apkFullPath"] = Path.Combine(options["projectPath"], buildPlayerOptions.locationPathName);

        var logPath = Path.Combine(Environment.CurrentDirectory, "build_log.log");
        if (File.Exists(logPath) && !File.Exists(Path.Combine(newPath, "build_log.log")))
        {
            File.Copy(logPath, Path.Combine(newPath, "build_log.log"));
        }

        var request = WebRequest.Create(options["resultUrl"]);
        request.Method = "POST";

        var postData = JsonConvert.SerializeObject(options);

        var byteArray = Encoding.UTF8.GetBytes(postData);

        // 커스텀 헤더값 같이 보냄
        request.ContentType = "application/json;charset=UTF-8";

        request.ContentLength = byteArray.Length;
        var dataStream = request.GetRequestStream();
        // Post Data 같이 전송
        dataStream.Write(byteArray, 0, byteArray.Length);
        dataStream.Close();

        var response = request.GetResponse();
        dataStream = response.GetResponseStream();
        var reader = new StreamReader(dataStream);

        // 데이터 읽어서 Json 화
        var responseFromServer = reader.ReadToEnd();
    }

    private static void ParseCommandLineArguments(out Dictionary<string, string> providedArguments)
    {
        providedArguments = new Dictionary<string, string>();
        var args = Environment.GetCommandLineArgs();

        Console.WriteLine(
            $"{Eol}" +
            $"###########################{Eol}" +
            $"#    Parsing settings     #{Eol}" +
            $"###########################{Eol}" +
            $"{Eol}"
        );

        // Extract flags with optional values
        for (int current = 0, next = 1; current < args.Length; current++, next++)
        {
            // Parse flag
            var isFlag = args[current].StartsWith("-");
            if (!isFlag) continue;
            var flag = args[current].TrimStart('-');

            // Parse optional value
            var flagHasValue = next < args.Length && !args[next].StartsWith("-");
            var value = flagHasValue ? args[next].TrimStart('-') : "";
            var secret = Secrets.Contains(flag);
            var displayValue = secret ? "*HIDDEN*" : "\"" + value + "\"";

            // Assign
            Console.WriteLine($"Found flag \"{flag}\" with value {displayValue}.");
            providedArguments.Add(flag, value);
        }
    }

    private static Dictionary<string, string> GetValidatedOptions()
    {
        ParseCommandLineArguments(out var validatedOptions);

        // if (!validatedOptions.TryGetValue("projectPath", out string _))
        // {
        //     Console.WriteLine("Missing argument -projectPath");
        //     EditorApplication.Exit(110);
        // }
        //
        // if (!validatedOptions.TryGetValue("buildTarget", out string buildTarget))
        // {
        //     Console.WriteLine("Missing argument -buildTarget");
        //     EditorApplication.Exit(120);
        // }
        //
        // if (!Enum.IsDefined(typeof(BuildTarget), buildTarget ?? string.Empty))
        // {
        //     EditorApplication.Exit(121);
        // }
        //
        // if (!validatedOptions.TryGetValue("customBuildPath", out string _))
        // {
        //     Console.WriteLine("Missing argument -customBuildPath");
        //     EditorApplication.Exit(130);
        // }
        //
        // const string defaultCustomBuildName = "TestBuild";
        // if (!validatedOptions.TryGetValue("customBuildName", out string customBuildName))
        // {
        //     Console.WriteLine($"Missing argument -customBuildName, defaulting to {defaultCustomBuildName}.");
        //     validatedOptions.Add("customBuildName", defaultCustomBuildName);
        // }
        // else if (customBuildName == "")
        // {
        //     Console.WriteLine($"Invalid argument -customBuildName, defaulting to {defaultCustomBuildName}.");
        //     validatedOptions.Add("customBuildName", defaultCustomBuildName);
        // }

        return validatedOptions;
    }

    private static void ExitWithResult(BuildResult result)
    {
        switch (result)
        {
            case BuildResult.Succeeded:
                Console.WriteLine("Build succeeded!");
                EditorApplication.Exit(0);
                break;
            case BuildResult.Failed:
                Console.WriteLine("Build failed!");
                EditorApplication.Exit(101);
                break;
            case BuildResult.Cancelled:
                Console.WriteLine("Build cancelled!");
                EditorApplication.Exit(102);
                break;
            case BuildResult.Unknown:
            default:
                Console.WriteLine("Build result is unknown!");
                EditorApplication.Exit(103);
                break;
        }
    }

    private static void ReportSummary(BuildSummary summary)
    {
        Console.WriteLine(
            $"{Eol}" +
            $"###########################{Eol}" +
            $"#      Build results      #{Eol}" +
            $"###########################{Eol}" +
            $"{Eol}" +
            $"Duration: {summary.totalTime.ToString()}{Eol}" +
            $"Warnings: {summary.totalWarnings.ToString()}{Eol}" +
            $"Errors: {summary.totalErrors.ToString()}{Eol}" +
            $"Size: {summary.totalSize.ToString()} bytes{Eol}" +
            $"{Eol}"
        );
    }

    private static string AddBuildVersion(BuildTarget buildTarget)
    {
        CheckBuildTarget(buildTarget);

        string oldBuildNumber;

        switch (buildTarget)
        {
            case BuildTarget.Android:
                oldBuildNumber = PlayerSettings.Android.bundleVersionCode.ToString();
                PlayerSettings.Android.bundleVersionCode = (int.Parse(oldBuildNumber) + 1);
                return PlayerSettings.Android.bundleVersionCode.ToString();
            case BuildTarget.iOS:
                oldBuildNumber = PlayerSettings.iOS.buildNumber;
                PlayerSettings.iOS.buildNumber = (int.Parse(oldBuildNumber) + 1).ToString();
                return PlayerSettings.iOS.buildNumber;
                break;
        }

        return "null";
    }

    private static void CheckBuildTarget(BuildTarget buildTarget)
    {
        if (buildTarget == BuildTarget.Android || buildTarget == BuildTarget.iOS)
            return;

        throw new Exception($"Is not supported Platform, {buildTarget}");
    }

    private static void GenericBuild(string[] scenes, string targetPath, BuildTarget buildTarget,
        BuildOptions buildOptions)
    {
        var buildTargetGroup = BuildPipeline.GetBuildTargetGroup(buildTarget);
        EditorUserBuildSettings.SwitchActiveBuildTarget(buildTargetGroup, buildTarget);
        BuildPipeline.BuildPlayer(scenes, targetPath, buildTarget, buildOptions);
    }

    private static string[] FindEnabledEditorScenes()
    {
        var EditorScenes = new List<string>();
        foreach (var scene in EditorBuildSettings.scenes)
        {
            if (!scene.enabled)
                continue;

            EditorScenes.Add(scene.path);
        }

        return EditorScenes.ToArray();
    }
}